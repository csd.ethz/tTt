/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "tttconvertexperiment.h"

/// Project
#include "tttio/fastdirectorylisting.h"

// Qt
#include <QMessageBox>
#include <QFileDialog>
#include <QSettings>
#include <QImageReader>
#include <QImageWriter>
#include <QProgressDialog>
#include <QMessageBox>
#include <QProcess>
#include <QElapsedTimer>

// STL
#include <cassert>

TTTConvertExperiment::TTTConvertExperiment(QWidget* parent /*= 0*/)
	: QWidget(parent)
{
	// Setup ui
	QSettings settings;
	m_ui.setupUi(this);
	m_ui.dteExperimentDate->setDate(QDate::currentDate());
	m_ui.lieDstFolder->setText(settings.value("lieDstFolder", QString()).toString());
	m_ui.lieUserName->setText(settings.value("lieUserName", QString()).toString());
	updateExperimentNameAndDirectory();

	// Adaption for release version (i.e. version not used within the lab)
	if(g_bReleaseVersion) {
		m_ui.lblTatXmlFound->setVisible(false);
		m_ui.grbPositions->setVisible(false);
		m_ui.grbConversion->setChecked(false);
		m_ui.grbAdvancedImport->setChecked(false);
		m_ui.grbSpecifyMmppIndirectly->setEnabled(false);
		m_ui.grbSpecifyMmppIndirectly->setVisible(false);	// Do not allow indirect specification of micrometer per pixel (this is hardware specific)
		m_ui.grbSpecifyMmppDirectly->setChecked(true);
		m_ui.grbSpecifyMmppDirectly->setCheckable(false);
		m_ui.grbSpecifyMmppDirectly->setTitle("");
		setWindowTitle(QString("TTT Converter - v%1 - %2").arg(CONVERTER_VERSION).arg(sizeof(int*) == 4 ? "32 Bit" : "64 Bit"));
	}
	else
		setWindowTitle(QString("TTT Experiment Converter - v%1 - %2").arg(CONVERTER_VERSION).arg(sizeof(int*) == 4 ? "32 Bit" : "64 Bit"));

	// Init variables
	m_foundImages = new FrmImportExperimentDataModel(this, m_ui.treeView);
	m_ui.treeView->setModel(m_foundImages);
	m_currentFilterAppliedToFolderNames = false;
	m_currentFilterAppliedToImageNames = false;
	m_scanBinFactorW0 = -1;
	m_inputFolderHasTatXml = false;
	m_currentFilterRestrictPositions = false;
	m_currentFilterRestrictPositionsStart = m_ui.spbStartPos->value();
	m_currentFilterRestrictPositionsStop = m_ui.spbStopPos->value();

	// Signals/slots
	connect(m_ui.clbLoad, SIGNAL(clicked()), this, SLOT(importSelectedImages()));
	connect(m_ui.lieFilter, SIGNAL(textChanged(const QString&)), this, SLOT(applyFilter()));
	connect(m_ui.chkApplyFilterToImages, SIGNAL(toggled(bool)), this, SLOT(applyFilter()));
	connect(m_ui.chkApplyFilterToFolderNames, SIGNAL(toggled(bool)), this, SLOT(applyFilter()));
	connect(m_ui.grbAdvancedImport, SIGNAL(toggled(bool)), m_foundImages, SLOT(emitDataChanged()));
	connect(m_ui.liePositionMarker, SIGNAL(textChanged(const QString&)), m_foundImages, SLOT(emitDataChanged()));
	connect(m_ui.lieTpMarker, SIGNAL(textChanged(const QString&)), m_foundImages, SLOT(emitDataChanged()));
	connect(m_ui.lieChannelMarker, SIGNAL(textChanged(const QString&)), m_foundImages, SLOT(emitDataChanged()));
	connect(m_ui.lieZIndexMarker, SIGNAL(textChanged(const QString&)), m_foundImages, SLOT(emitDataChanged()));
	connect(m_ui.lieUserName, SIGNAL(textChanged(const QString&)), this, SLOT(userNameChanged()));
	connect(m_ui.spbSetup, SIGNAL(valueChanged(int)), this, SLOT(setupNumberChanged(int)));
	connect(m_ui.lieDstFolder, SIGNAL(textChanged(const QString&)), this, SLOT(updateExperimentNameAndDirectory()));
	connect(m_ui.dteExperimentDate, SIGNAL(dateChanged(const QDate&)), this, SLOT(updateExperimentNameAndDirectory()));
	connect(m_ui.pbtSetDstFolder, SIGNAL(clicked()), this, SLOT(seletDestinationFolder()));
	connect(m_ui.pbtSelectInputFolder, SIGNAL(clicked()), this, SLOT(seletInputFolder()));
	connect(m_ui.grbSpecifyMmppDirectly, SIGNAL(clicked(bool)), this, SLOT(specifyDirectlyToggled(bool)));
	connect(m_ui.grbSpecifyMmppIndirectly, SIGNAL(clicked(bool)), this, SLOT(specifyIndirectlyToggled(bool)));
	connect(m_ui.pbtBpWpHelp, SIGNAL(clicked()), this, SLOT(showBpWpHelp()));
	connect(m_ui.grbPositions, SIGNAL(toggled(bool)), this, SLOT(applyFilter()));
	connect(m_ui.spbStartPos, SIGNAL(valueChanged(int)), this, SLOT(applyFilter()));
	connect(m_ui.spbStopPos, SIGNAL(valueChanged(int)), this, SLOT(applyFilter()));
	connect(m_ui.pbtHelp, SIGNAL(clicked()), this, SLOT(showHelp()));

	// Adjust column widths
	QHeaderView* header = m_ui.treeView->header();
	if(header)
		header->resizeSection(0, 500);
}

void TTTConvertExperiment::setCurrentFolder(QString folder)
{
	ChangeCursorObject cc;

	// Make sure path ends with '/'
	folder = QDir::fromNativeSeparators(folder);
	if(folder.right(1) != "/")
		folder += '/';
	m_currentFolder = folder;

	// Extract experiment name from input data to see if it contains a tat-xml
	m_experimentNameFromInputFolder = m_currentFolder.left(m_currentFolder.length() - 1);
	if(m_experimentNameFromInputFolder.lastIndexOf('/') >= 0)
		m_experimentNameFromInputFolder = m_experimentNameFromInputFolder.mid(m_experimentNameFromInputFolder.lastIndexOf('/') + 1);
	else
		m_experimentNameFromInputFolder = "";

	// Check for tat-xml
	QString tatXmlFileName = m_experimentNameFromInputFolder;
	if(tatXmlFileName.endsWith("_16bit"))
		tatXmlFileName = tatXmlFileName.left(tatXmlFileName.length() - 6);
	tatXmlFileName += "_TATexp.xml";
	m_inputFolderHasTatXml = QFile::exists(m_currentFolder + tatXmlFileName);
	if(m_inputFolderHasTatXml) {
		m_ui.lblTatXmlFound->setText("TAT-XML found: yes");
		m_ui.grbExperimentMetaInformation->setEnabled(false);
		m_ui.grbExperimentMetaInformation->setTitle("Experiment meta information (disabled: will be copied from tat-xml file)");
	}
	else {
		m_ui.lblTatXmlFound->setText("TAT-XML found: no - specify experiment meta information below");
		m_ui.grbExperimentMetaInformation->setEnabled(true);
		m_ui.grbExperimentMetaInformation->setTitle("Experiment meta information");
	}

	// Look for images in all sub folders and in root folder
	QHash<QString, QStringList> imagesInSubfolders; 
	QList<QString> imagesInRoot;
	QStringList directories = QDir(folder).entryList(QDir::AllDirs | QDir::NoDotDot);
	bool foundSubfoldersWithImages = false;
	QStringList imageExtensions;
	imageExtensions << ".jpg" << ".tif" << ".png";
	int totalImageCount = 0;
	for(int iFolder = 0; iFolder < directories.size(); ++iFolder) {
		QString curSubFolder = directories[iFolder];

		// If a tat-xml was found, skip folders that do not contain a position tag _p
		if(m_inputFolderHasTatXml && !curSubFolder.contains("_p"))
			continue;

		QStringList imagesInCurrentFolder = FastDirectoryListing::listFiles(QDir(folder + curSubFolder), imageExtensions);
		qSort(imagesInCurrentFolder);
		if(imagesInCurrentFolder.size()) {
			totalImageCount += imagesInCurrentFolder.size();
			if(curSubFolder == ".")
				imagesInRoot = std::move(imagesInCurrentFolder);
			else
				imagesInSubfolders[curSubFolder] = std::move(imagesInCurrentFolder);
		}
	}

	// Show found input data in GUI
	m_foundImages->setData(imagesInSubfolders, imagesInRoot);
	m_ui.clbLoad->setEnabled(totalImageCount > 0);
	m_ui.clbLoad->setText(QString("Convert %1 images").arg(totalImageCount));
	//m_ui.lblFoundImageData->setText(QString("Found image data in '%1':").arg(folder));

	// Apply filter
	m_currentFilter = "";
	applyFilter();

	// Update experiment name
	updateExperimentNameAndDirectory();
}

void TTTConvertExperiment::applyFilter()
{
	ChangeCursorObject cc;

	// If new filter contains old filter, hidden items do not need to be checked again (if filter settings are equal, nothing needs to be done)
	QString newFilter = m_ui.lieFilter->text();
	if(m_currentFilterRestrictPositions == m_ui.grbPositions->isChecked() 
		&& m_currentFilterRestrictPositionsStart == m_ui.spbStartPos->value()
		&& m_currentFilterRestrictPositionsStop == m_ui.spbStopPos->value()
		&& newFilter == m_currentFilter 
		&& m_currentFilterAppliedToFolderNames == m_ui.chkApplyFilterToFolderNames->isChecked() 
		&& m_currentFilterAppliedToImageNames == m_ui.chkApplyFilterToImages->isChecked())
		return;
	bool hiddenRowsRemainHidden = newFilter.contains(m_currentFilter);

	// Apply filter
	int totalImageCount = 0;
	for(int topRow = 0; topRow < m_foundImages->rowCount(); ++topRow) {
		QModelIndex topRowIndex = m_foundImages->index(topRow, 0);
		bool hideTopRow = false;
		if(topRow < m_foundImages->getSubFoldersCount()) {
			// TopRow is folder, check if it should be visible
			if(m_ui.grbPositions->isChecked()) {
				int topRowPosition = m_foundImages->data(m_foundImages->index(topRow, 1)).toInt();
				if(topRowPosition < m_ui.spbStartPos->value() || topRowPosition > m_ui.spbStopPos->value())
					hideTopRow = true;
			}

			if(!hideTopRow && m_ui.chkApplyFilterToFolderNames->isChecked()) {
				if(hiddenRowsRemainHidden && m_ui.treeView->isRowHidden(topRow, QModelIndex()))
					hideTopRow = true;
				else {
					QString rowName = m_foundImages->data(topRowIndex).toString();
					hideTopRow = !rowName.contains(newFilter, Qt::CaseSensitive);
				}
			}

			// If topRow is visible, process children
			if(!hideTopRow) {
				int childrenCount = m_foundImages->rowCount(topRowIndex);
				for(int childRow = 0; childRow < childrenCount; ++childRow) {
					// Check if child row should be visible
					QModelIndex childRowIndex = m_foundImages->index(childRow, 0, topRowIndex);
					bool hideChildRow = false;
					if(m_ui.chkApplyFilterToImages->isChecked()) {
						if(hiddenRowsRemainHidden && m_ui.treeView->isRowHidden(childRow, topRowIndex))
							hideChildRow = true;
						else {
							QString rowName = m_foundImages->data(childRowIndex).toString();
							hideChildRow = !rowName.contains(newFilter, Qt::CaseSensitive);
						}
					}

					// Hide/show child row
					m_ui.treeView->setRowHidden(childRow, topRowIndex, hideChildRow);
					if(!hideChildRow)
						++totalImageCount;
				}
			}
		}
		else {
			// TopRow is image, check if it should be hidden
			if(m_ui.grbPositions->isChecked())
				hideTopRow = true;
			else {
				if(m_ui.chkApplyFilterToImages->isChecked()) {
					if(hiddenRowsRemainHidden && m_ui.treeView->isRowHidden(topRow, QModelIndex()))
						hideTopRow = true;
					else {
						QString rowName = m_foundImages->data(topRowIndex).toString();
						hideTopRow = !rowName.contains(newFilter, Qt::CaseSensitive);
					}
				}
			}
			if(!hideTopRow)
				++totalImageCount;
		}

		// Hide/show top row
		m_ui.treeView->setRowHidden(topRow, QModelIndex(), hideTopRow);
	}

	// Filter updated
	m_currentFilter = newFilter;
	m_currentFilterAppliedToFolderNames = m_ui.chkApplyFilterToFolderNames->isChecked();
	m_currentFilterAppliedToImageNames = m_ui.chkApplyFilterToImages->isChecked();
	m_currentFilterRestrictPositions = m_ui.grbPositions->isChecked();
	m_currentFilterRestrictPositionsStart = m_ui.spbStartPos->value();
	m_currentFilterRestrictPositionsStop = m_ui.spbStopPos->value();

	// Update GUI
	m_ui.clbLoad->setEnabled(totalImageCount > 0);
	m_ui.clbLoad->setText(QString("Convert %1 images").arg(totalImageCount));
}

void TTTConvertExperiment::importSelectedImages()
{
	// Test
	QElapsedTimer t;
	t.start();

	// Preliminary checks
	QString expName = m_ui.lieExperimentName->text();
	QString expFolder = m_ui.lieExperimentFolder->text();
	if(expName.isEmpty() || expFolder.isEmpty()) {
		QMessageBox::information(this, "Invalid parameters", "Output experiment directory invalid. Please specify at least experiment date and destination folder.");
		return;
	}
	if(QDir(m_ui.lieDstFolder->text()) == QDir(m_ui.lieInputFolder->text()) || QDir(m_ui.lieExperimentFolder->text()) == QDir(m_ui.lieInputFolder->text()) ) {
		QMessageBox::information(this, "Invalid parameters", "Input and output directory must be different.");
		return;
	}

	// Bp/wp
	QHash<int, QPair<int, int>> bpWp = getBpWpSettings();
	if(!bpWp.contains(-1))
		return;

	// Check if destination directory already exists
	if(QDir(expFolder).exists()) {
		if(QMessageBox::question(this, "Confirm", QString("The destination folder (%1) already exists.\n\nUse it anyways?").arg(expFolder), QMessageBox::Yes | QMessageBox::No) != QMessageBox::Yes)
			return;
	}

	// Make directory
	if(!QDir(expFolder).exists() && !QDir(expFolder).mkpath(expFolder)) {
		QMessageBox::critical(this, "Error", QString("Cannot create output folder: %1.").arg(expFolder));
		return;
	}
	expFolder = QDir::fromNativeSeparators(expFolder);
	if(expFolder.right(1) != "/")
		expFolder += "/";

	// Create log file
	QString logFileName;
	if(m_ui.grbPositions->isChecked())
		logFileName = expFolder + QString("TTTExperimentConverterLog_Inst%1.txt").arg(QCoreApplication::applicationPid());
	else
		logFileName = expFolder + "TTTExperimentConverterLog.txt";
	QFile logFile(logFileName);
	if(!logFile.open(QIODevice::WriteOnly)) {
		QMessageBox::critical(this, "Error", QString("Cannot create log file: %1.").arg(logFile.errorString()));
		return;
	}
	QTextStream log(&logFile);

	// Write log file header
	log << "TTTExperimentConverter\n";
	log << "Time: " << QDateTime::currentDateTime().toString() << "\n";
	log << "Micrometer per pixel: " << (m_ui.grbSpecifyMmppDirectly->isChecked() ? "direct" : "factors") << "\n";
	log << "Input folder: " << m_ui.lieInputFolder->text() << "\n";
	log << "TAT-XML found: " << m_inputFolderHasTatXml << "\n";
	log << "Use position/time point/channel/... tags: " << (m_ui.grbAdvancedImport->isChecked() ? "yes" : "no") << "\n";
	log << "Restrict to positions: " << (m_ui.grbPositions->isChecked() ? "yes" : "no") << " - start: " << m_ui.spbStartPos->value() << "stop: " << m_ui.spbStopPos->value() << "\n";
	log << "Black point / white point:\n";
	for(auto it = bpWp.begin(); it != bpWp.end(); ++it) {
		log << "\tchannel " << it.key() << ": bp = " << it->first << ", wp = " << it->second << "\n";
	}
	log << "Starting conversion..\n";

	// Determine conversion settings
	m_scanBinFactorW0 = -1;	// Needs to be determined

	// Do conversion
	QStringList errors;
	int imageCounter = 0,
		imageCounterSuccess = 0;
	try {
		ChangeCursorObject cc(Qt::BusyCursor);

		// Count images to convert (for progress bar) and make sure for every time point with any images an image with w0 exists
		QHash<PictureIndex, int> foundW0Images;	// Contains for each position/time point at which images exist the number of w0 images
		int imagesToConvertTotal = 0;
		for(int topRow = 0; topRow < m_foundImages->rowCount(); ++topRow) {
			QModelIndex topRowIndex = m_foundImages->index(topRow, 0);
			if(topRow < m_foundImages->getSubFoldersCount()) {

				// TopRow is folder, process children (i.e. images in folder) if it is visible
				if(!m_ui.treeView->isRowHidden(topRow, QModelIndex())) {
					QString folderName = m_foundImages->data(topRowIndex).toString();
					int imageCountCurrentPosition = m_foundImages->rowCount(topRowIndex);
					for(int imageRow = 0; imageRow < imageCountCurrentPosition; ++imageRow) {
						// Add image of child row if child row is visible
						QModelIndex childRowNameIndex = m_foundImages->index(imageRow, 0, topRowIndex);
						if(!m_ui.treeView->isRowHidden(imageRow, topRowIndex)) {
							++imagesToConvertTotal;

							PictureIndex foundW0ImagesPi;
							foundW0ImagesPi.positionNumber = m_foundImages->data(m_foundImages->index(imageRow, 1, topRowIndex)).toInt();
							foundW0ImagesPi.timePoint = m_foundImages->data(m_foundImages->index(imageRow, 2, topRowIndex)).toInt();
							int ch = m_foundImages->data(m_foundImages->index(imageRow, 3, topRowIndex)).toInt();
							if(ch == 0)
								++foundW0Images[foundW0ImagesPi];
							else {
								if(!foundW0Images.contains(foundW0ImagesPi))
									foundW0Images[foundW0ImagesPi] = 0;
							}
						}
					}
				}
			}
			else {
				// TopRow is image, add it to mapper if it is visible
				if(!m_ui.treeView->isRowHidden(topRow, QModelIndex())) {
					++imagesToConvertTotal;

					PictureIndex foundW0ImagesPi;
					foundW0ImagesPi.positionNumber = m_foundImages->data(m_foundImages->index(topRow, 1)).toInt();
					foundW0ImagesPi.timePoint = m_foundImages->data(m_foundImages->index(topRow, 2)).toInt();
					int ch = m_foundImages->data(m_foundImages->index(topRow, 3)).toInt();
					if(ch == 0)
						++foundW0Images[foundW0ImagesPi];
					else {
						if(!foundW0Images.contains(foundW0ImagesPi))
							foundW0Images[foundW0ImagesPi] = 0;
					}
				}
			}
		}

		// Check if for any position/timepoint images exist but no w0 image
		for(auto it = foundW0Images.constBegin(); it != foundW0Images.constEnd(); ++it) {
			if(*it == 0) {
				PictureIndex badPi = it.key();
				QMessageBox::warning(this, "Note", QString("Experiment data not compatible with TTT: Found images at position %1 and time point %2, but none of them has channel 0. Note that in TTT channel indexing must always begin with 0 and for each position and time point that has images, an image with channel 0 must exist.\n\nPlease adjust your data accordingly before conversion.").arg(badPi.positionNumber).arg(badPi.timePoint));
				return;
			}
		}

		// Show progress bar
		QProgressDialog progressBar("Converting experiment..", QString(), 0, imagesToConvertTotal, this);
		progressBar.setWindowTitle("TTT Convert Experiment");
		progressBar.setWindowModality(Qt::ApplicationModal);
		progressBar.setMinimumDuration(0);
		progressBar.setValue(0);
		progressBar.setWindowFlags( (progressBar.windowFlags() | Qt::CustomizeWindowHint) & (~Qt::WindowContextHelpButtonHint));// Disable context-help button

		// TatExpXml information
		QVector<QHash<QString, QString>> tatXmlPositionData;		// First index: position number, hash-key: attribute, hash-value: value
		QVector<QHash<QString, QString>> tatXmlWavelengthData;		// First index: wavelength number, hash-key: attribute, hash-value: value

		// Create mapper and add all selected files
		for(int topRow = 0; topRow < m_foundImages->rowCount(); ++topRow) {
			QModelIndex topRowIndex = m_foundImages->index(topRow, 0);
			if(topRow < m_foundImages->getSubFoldersCount()) {

				// TopRow is folder, process children (i.e. images in folder) if it is visible
				if(!m_ui.treeView->isRowHidden(topRow, QModelIndex())) {
					QString folderName = m_foundImages->data(topRowIndex).toString();
					int imageCountCurrentPosition = m_foundImages->rowCount(topRowIndex);
					for(int imageRow = 0; imageRow < imageCountCurrentPosition; ++imageRow) {
						if(progressBar.wasCanceled()) {
							errors.push_back("Conversion was canceled.");
							break;
						}

						// Add image of child row if child row is visible
						QModelIndex childRowNameIndex = m_foundImages->index(imageRow, 0, topRowIndex);
						if(!m_ui.treeView->isRowHidden(imageRow, topRowIndex)) {
							QString imageName = m_foundImages->data(childRowNameIndex).toString();
							QString imageRelName = folderName + '/' + imageName;
							int pos = m_foundImages->data(m_foundImages->index(imageRow, 1, topRowIndex)).toInt();
							int tp = m_foundImages->data(m_foundImages->index(imageRow, 2, topRowIndex)).toInt();
							int ch = m_foundImages->data(m_foundImages->index(imageRow, 3, topRowIndex)).toInt();
							int z = m_foundImages->data(m_foundImages->index(imageRow, 4, topRowIndex)).toInt();
							if(pos >= 0 && tp >= 0 && ch >= 0 && z >= 0) {
								// Get bp/wp
								int bp, wp;
								if(bpWp.contains(ch)) {
									// Use specific bp/wp
									bp = bpWp[ch].first;
									wp = bpWp[ch].second;
								}
								else {
									// Use default
									bp = bpWp[-1].first;
									wp = bpWp[-1].second;
								}
								assert(bp <= wp && bp >= 0 && wp <= 0xFFFF);

								// Convert frame
								PictureIndex pi(pos, tp, ch, z);
								QString err = createConvertedFrameAndUpdateTatXmlInfo(imageRelName, expFolder, bp, wp, pi, expName, tatXmlPositionData, tatXmlWavelengthData);
								progressBar.setValue(++imageCounter);
								if(!err.isEmpty())
									errors.push_back(err);
								else
									++imageCounterSuccess;
							}
						}
					}
				}
			}
			else {
				if(progressBar.wasCanceled()) {
					errors.push_back("Conversion was canceled.");
					break;
				}

				// TopRow is image, add it to mapper if it is visible
				if(!m_ui.treeView->isRowHidden(topRow, QModelIndex())) {
					QString imageName = m_foundImages->data(topRowIndex).toString();
					int pos = m_foundImages->data(m_foundImages->index(topRow, 1)).toInt();
					int tp = m_foundImages->data(m_foundImages->index(topRow, 2)).toInt();
					int ch = m_foundImages->data(m_foundImages->index(topRow, 3)).toInt();
					int z = m_foundImages->data(m_foundImages->index(topRow, 4)).toInt();
					if(pos >= 0 && tp >= 0 && ch >= 0 && z >= 0) {
						// Get bp/wp
						int bp, wp;
						if(bpWp.contains(ch)) {
							// Use specific bp/wp
							bp = bpWp[ch].first;
							wp = bpWp[ch].second;
						}
						else {
							// Use default
							bp = bpWp[-1].first;
							wp = bpWp[-1].second;
						}
						assert(bp <= wp && bp >= 0 && wp <= 0xFFFF);

						PictureIndex pi(pos, tp, ch, z);
						//experiment->addImageMapping(pi, imageName);
						QString err = createConvertedFrameAndUpdateTatXmlInfo(imageName, expFolder, bp, wp, pi, expName, tatXmlPositionData, tatXmlWavelengthData);
						progressBar.setValue(++imageCounter);
						if(!err.isEmpty())
							errors.push_back(err);
						else
							++imageCounterSuccess;
					}
				}
			}
		}

		// Unless positions were selected: if source folder has tat-xml, copy additional folders and files (including tat-xml) to new folder, otherwise generate tat-xml
		if(!m_ui.grbPositions->isChecked()) {
			if(m_inputFolderHasTatXml) {
				// Simply copy all folders without position tag "_p" and files in root folder
				QFileInfoList files = QDir(m_currentFolder).entryInfoList(QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot);
				for(auto itFile = files.constBegin(); itFile != files.constEnd(); ++itFile) {
					if(itFile->isFile() || !itFile->fileName().contains("_p")) {
						log << "Copying additional file or directory: '" << itFile->absoluteFilePath() << "'\n";
						copyFileOrDir(*itFile, expFolder, errors);
					}
				}
			}
			else {
				// Generate: layout positions in a grid
				double microMeterPerPixel = getMicroMeterPerPixel();
				QString err = createGridLayout(tatXmlPositionData, tatXmlWavelengthData, microMeterPerPixel);
				if(!err.isEmpty())
					errors.push_back(err);

				// Write Tat Xml
				err = writeTatExpXml(expFolder, expName, tatXmlPositionData, tatXmlWavelengthData, microMeterPerPixel);
				if(!err.isEmpty())
					errors.push_back(err);
			}
		}
	}
	catch(ExceptionBase& e) {
		// Show error message 
		QMessageBox::critical(this, "Error", QString("Cannot convert experiment. Exception: %1 at %2").arg(e.what()).arg(e.where()));
	}

	// Show results message
	if(errors.isEmpty()) {
		QMessageBox::information(this, QString("Conversion completed."), QString("Conversion completed successfully: converted %1 images.\n\nElapsed time: %1 sec").arg(imageCounterSuccess).arg(t.elapsed()/1000));

		log << QString("Conversion completed successfully: converted %1 images.").arg(imageCounterSuccess);	
	}
	else {
		QMessageBox msg;
		msg.setText(QString("Conversion completed with errors: converted %1 images.").arg(imageCounterSuccess));
		msg.setIcon(QMessageBox::Warning);
		msg.setInformativeText(QString("%1 errors occurred during conversion.").arg(errors.size()));
		msg.setStandardButtons(QMessageBox::Ok);
		msg.setDefaultButton(QMessageBox::Ok);
		QString details = errors.join("\n");
		msg.setDetailedText(details);
		msg.exec();

		log << QString("Conversion completed with errors: converted %1 images.").arg(imageCounterSuccess);
		log << QString("Errors:\n") << details;
	}
}

int TTTConvertExperiment::mapImageNameToPositionNumber(const QString& imgName) const
{
	if(m_ui.grbAdvancedImport->isChecked()) {
		QString separator = m_ui.liePositionMarker->text();
		if(separator.length())
			return getNumberFromString(imgName, separator);
	}

	return -1;
}

int TTTConvertExperiment::mapImageNameToTimePoint(const QString& imgName) const
{
	if(m_ui.grbAdvancedImport->isChecked()) {
		QString separator = m_ui.lieTpMarker->text();
		if(separator.length())
			return getNumberFromString(imgName, separator);
	}

	return -1;
}

int TTTConvertExperiment::mapImageNameToChannel(const QString& imgName) const
{
	if(m_ui.grbAdvancedImport->isChecked()) {
		QString separator = m_ui.lieChannelMarker->text();
		if(separator.length())
			return getNumberFromString(imgName, separator);
	}

	return -1;
}

int TTTConvertExperiment::mapImageNameToZIndex(const QString& imgName) const
{
	if(m_ui.grbAdvancedImport->isChecked()) {
		QString separator = m_ui.lieZIndexMarker->text();
		if(separator.length())
			return getNumberFromString(imgName, separator);
	}

	return -1;
}

void TTTConvertExperiment::updateExperimentNameAndDirectory()
{
	// Set experiment name
	QString expName;
	if(m_inputFolderHasTatXml) {
		expName = m_experimentNameFromInputFolder;
	}
	else {
		QDate expData = m_ui.dteExperimentDate->date();
		expName = QString("%1%2%3%4%5").arg(expData.year() - 2000, 2, 10, QChar('0'))
			.arg(expData.month(), 2, 10, QChar('0'))
			.arg(expData.day(), 2, 10, QChar('0'))
			.arg(m_ui.lieUserName->text())
			.arg(m_ui.spbSetup->value());
	}
	m_ui.lieExperimentName->setText(expName);

	// Destination folder
	QString dstFolder = m_ui.lieDstFolder->text();
	if(!dstFolder.isEmpty()) {
		dstFolder = QDir::fromNativeSeparators(dstFolder);
		if(dstFolder.right(1) != "/")
			dstFolder += "/";
		dstFolder += expName;
		dstFolder = QDir::toNativeSeparators(dstFolder);
	}
	m_ui.lieExperimentFolder->setText(dstFolder);
}

void TTTConvertExperiment::seletDestinationFolder()
{
	// Open directory browser
	QString newFolder = QFileDialog::getExistingDirectory(this, "Select destination folder", m_ui.lieDstFolder->text());
	if(!newFolder.isEmpty()) {
		m_ui.lieDstFolder->setText(newFolder);

		// Remember
		QSettings settings;
		settings.setValue("lieDstFolder", newFolder);
	}
}

void TTTConvertExperiment::userNameChanged()
{
	// Remember
	QSettings settings;
	settings.setValue("lieUserName", m_ui.lieUserName->text());

	// Update experiment folder
	updateExperimentNameAndDirectory();
}

void TTTConvertExperiment::seletInputFolder()
{
	// Determine search folder
	QSettings settings;
	QString searchFolder = m_ui.lieInputFolder->text();
	if(searchFolder.isEmpty()) {
		searchFolder = settings.value("lieInputFolder", QString()).toString();
	}
	
	// Open directory browser
	QString newFolder = QFileDialog::getExistingDirectory(this, "Select input folder", searchFolder);
	if(!newFolder.isEmpty()) {
		m_ui.lieInputFolder->setText(newFolder);

		// Remember
		settings.setValue("lieInputFolder", newFolder);

		// List data
		setCurrentFolder(newFolder);
	}
}

QString TTTConvertExperiment::createConvertedFrameAndUpdateTatXmlInfo(const QString& inputFileName, const QString& expFolder, int bp, int wp, const PictureIndex& dstPictureIndex, const QString& expName, QVector<QHash<QString, QString>>& tatXmlPositionData, QVector<QHash<QString, QString>>& tatXmlWavelengthData)
{
	qDebug() << inputFileName << "bw: " << bp << " " << wp;

	if(dstPictureIndex.positionNumber < 0 || dstPictureIndex.timePoint < 0 || dstPictureIndex.channel < 0 || dstPictureIndex.zIndex < 0)
		return QString("Invalid destination image index: ") + dstPictureIndex.toString();

	// Open source image
	QString errString;
	QImage srcImage = openGrayScaleImage(m_currentFolder + inputFileName, bp, wp, &errString);
	if(!errString.isEmpty() || srcImage.isNull())
		return QString("Cannot open %1: %2").arg(m_currentFolder + inputFileName).arg(errString);
	srcImage = srcImage.convertToFormat(QImage::Format_Indexed8);

	// Determine m_scanBinFactorW0
	if(m_scanBinFactorW0 < 1 && dstPictureIndex.channel == 0) {
		switch(srcImage.width()) {
		case 1388:		//1388x1040
			//base size: scan factor 1.0
			m_scanBinFactorW0 = 1;
			break;
		case 694:		//694x520
			m_scanBinFactorW0 = 4;
			break;
		case 462:		//462x346
			m_scanBinFactorW0 = 9;
			break;
		case 346:		//346x260
			m_scanBinFactorW0 = 16;
			break;
		case 276:		//276x208
			m_scanBinFactorW0 = 25;
			break;
		case 2776:		//2776x2080
			m_scanBinFactorW0 = 2;
			break;
		case 4164:		//4164x3120
			m_scanBinFactorW0 = 3;
			break;
		default:
			m_scanBinFactorW0 = 1;
		}
	}

	// Update tat xml info
	if(dstPictureIndex.positionNumber >= tatXmlPositionData.size()) 
		tatXmlPositionData.resize(dstPictureIndex.positionNumber + 1);
	tatXmlPositionData[dstPictureIndex.positionNumber]["index"] = QString("%1").arg(dstPictureIndex.positionNumber, 4, 10, QChar('0'));
	if(dstPictureIndex.channel >= tatXmlWavelengthData.size()) 
		tatXmlWavelengthData.resize(dstPictureIndex.channel + 1);
	// Check if value is set, but conflicts with size of current image
	int curWidthForWl = tatXmlWavelengthData[dstPictureIndex.channel].value("width", QString("0")).toInt();
	int curHeightForWl = tatXmlWavelengthData[dstPictureIndex.channel].value("height", QString("0")).toInt();
	if((curWidthForWl > 0 && curWidthForWl != srcImage.width()) || (curHeightForWl > 0 && curHeightForWl != srcImage.height())) 
		return QString("%1: unexpected image size %2x%3 (expected: %4x%5)").arg(m_currentFolder + inputFileName).arg(srcImage.width()).arg(srcImage.height()).arg(curWidthForWl).arg(curHeightForWl); 
	// Update tat xml info if no value set yet
	if(curWidthForWl <= 0 || curHeightForWl <= 0) {
		tatXmlWavelengthData[dstPictureIndex.channel]["width"] = QString::number(srcImage.width());
		tatXmlWavelengthData[dstPictureIndex.channel]["height"] = QString::number(srcImage.height());
		tatXmlWavelengthData[dstPictureIndex.channel]["Name"] = QString("%1").arg(dstPictureIndex.channel, 2, 10, QChar('0'));
		tatXmlWavelengthData[dstPictureIndex.channel]["ImageType"] = "png";
	}

	// Create output folder
	if(!QDir(expFolder).exists() && !QDir(expFolder).mkpath(expFolder))
		return QString("Cannot create output folder: %1.").arg(expFolder);
	QString posFolder = expFolder + expName + QString("_p%1").arg(dstPictureIndex.positionNumber, 4, 10, QChar('0'));
	if(!QDir(posFolder).exists() && !QDir(posFolder).mkpath(posFolder))
		return QString("Cannot create output folder: %1.").arg(posFolder);

	// Save image
	QString newFileNameWithoutFileExtension = posFolder + QString("/%1_p%2_t%3_z%4_w%5").arg(expName) 
		.arg(dstPictureIndex.positionNumber, 4, 10, QChar('0')) 
		.arg(dstPictureIndex.timePoint, 5, 10, QChar('0')) 
		.arg(dstPictureIndex.zIndex, 3, 10, QChar('0')) 
		.arg(dstPictureIndex.channel, 2, 10, QChar('0'));
	QImageWriter writer(newFileNameWithoutFileExtension + ".png");
	if(!writer.write(srcImage)) 
		return QString("Cannot write file %1: %2").arg(newFileNameWithoutFileExtension + ".png").arg(writer.errorString());

	// Also copy xml file with image acquisition time, if it exists
	QString xmlFileName = inputFileName.left(inputFileName.lastIndexOf('.')) + ".xml";
	if(QFile::exists(m_currentFolder + xmlFileName)) {
		QFile xmlFile(m_currentFolder + xmlFileName);
		if(!xmlFile.copy(newFileNameWithoutFileExtension + ".xml"))
			return QString("Error: xml file '%1' with image meta information found, but cannot copy: %2").arg(xmlFileName).arg(xmlFile.errorString());
	}


	// Done
	return QString();
}

QImage TTTConvertExperiment::openGrayScaleImage(const QString& imageFileName, int bp /*= -1*/, int wp /*= -1*/, QString* outError /*= 0*/)
{
	QImage img;
	QString imageExtension = imageFileName.right(4).toLower();
	if(imageExtension == ".tif") {
		qDebug() << "Loading tiff " << imageFileName;

		// Use own tiff loader
		QFile device(imageFileName);
		if(device.open(QIODevice::ReadOnly)) {
			CatTiffHandler tiffLoader(bp, wp);
			tiffLoader.setDevice(&device);
			if(!tiffLoader.read(&img)) {
				if(outError)
					*outError = "Tiff import failed.";
				return QImage();
			}
			//// DEBUG
			//else {
			//	img.save(imageFileName.left(imageFileName.length() - 3) + "png");
			//}
		}
		else {
			if(outError)
				*outError = QString("Cannot open file: ") + device.errorString();
			return QImage();
		}
	}
	else if(imageExtension == ".png") {
		// Use own PNG loader
		int bitDepth, width, height;
		unsigned char* imageData = 0;
		int errCode = openPngGrayScaleImage(QDir::toNativeSeparators(imageFileName).toLatin1(), &imageData, bitDepth, width, height, true);	// Open background image, too

		// Check for error
		if(errCode || !imageData) {
			if(imageData)
				delete[] imageData;
			if(outError)
				*outError = QString("Cannot open file, error code: %1").arg(errCode);
			return QImage();
		}

		// Convert 
		if(bitDepth == 8) {
			// Just copy
			img = QImage(width, height, QImage::Format_Indexed8);
			if(img.isNull()) {
				if(outError)
					*outError = QString("Cannot allocate memory for image.");
				delete[] imageData;
				return QImage();
			}
			img.setColorTable(getColorTableGrayIndexed8());
			for(int y = 0; y < height; ++y)
				memcpy(img.scanLine(y), imageData + y*width, width);
		}
		else if(bitDepth == 16) {
			// Need to scale down to 8 bit
			img = QImage(width, height, QImage::Format_Indexed8);
			if(img.isNull()) {
				if(outError)
					*outError = QString("Cannot allocate memory for image.");
				delete[] imageData;
				return QImage();
			}
			img.setColorTable(getColorTableGrayIndexed8());

			// Determine bp and wp if necessary
			unsigned short* imageDataShort = reinterpret_cast<unsigned short*>(imageData);
			if(bp < 0 || wp < 0 || wp < bp) {
				bp = 0xFFFF;
				wp = 0;
				for(int y = 0; y < height; ++y) {
					unsigned short* curScanLine = imageDataShort + y*width;
					for(int x = 0; x < width; ++x) {
						bp = std::min(curScanLine[x], static_cast<unsigned short>(bp));
						wp = std::max(curScanLine[x], static_cast<unsigned short>(wp));
					}
				}
			}
			assert(wp >= bp);

			// Convert data
			float factor =  255.0f / (wp - bp);
			for(int y = 0; y < height; ++y) {
				unsigned char* curScanLineDst = img.scanLine(y);
				const unsigned short* curScanLineSrc = imageDataShort + y*width;
				for(int x = 0; x < width; ++x) {
					int curval = curScanLineSrc[x];
					curval = (curval - bp) * factor + 0.5f;
					curval = std::max(curval, 0);
					curval = std::min(curval, 255);
					curScanLineDst[x] = curval;
				}
			}
		}
		else if(bitDepth == 1) {
			// Scale up
			QImage tmp(imageData, width, height, QImage::Format_Mono);
			img = tmp.convertToFormat(QImage::Format_Indexed8);
			img.setColorTable(getColorTableGrayIndexed8());
		}
		else {
			if(outError)
				*outError = QString("Bitdepth not supported: %1").arg(bitDepth);
			delete[] imageData;
			return QImage();
		}
		delete[] imageData;
	}
	else {
		// Image is not tiff, so use QTs default implementation
		QImageReader imgReader(imageFileName);
		img = imgReader.read();
		if(img.isNull()) {
			if(outError)
				*outError= imgReader.errorString();
			return QImage();
		}
	}

	return img;
}

QVector<QRgb> TTTConvertExperiment::getColorTableGrayIndexed8()
{
	static QVector<QRgb> colorTableGrayIndexed8;
	if(colorTableGrayIndexed8.size() == 0) {
		colorTableGrayIndexed8.resize(256);
		for(unsigned int i = 0; i < colorTableGrayIndexed8.size(); ++i)
			colorTableGrayIndexed8[i] = qRgb(i,i,i);
	}

	return colorTableGrayIndexed8;
}

QString TTTConvertExperiment::writeTatExpXml(const QString& expFolder, const QString& expName, const QVector<QHash<QString, QString>>& positionData, const QVector<QHash<QString, QString>>& tatXmlWavelengthData, double microMeterPerPixel)
{
	// Open file
	QString tatExpXmlFileName = expFolder + expName + "_TATexp.xml";
	QFile file(tatExpXmlFileName);
	if(!file.open(QIODevice::WriteOnly)) 
		return QString("Cannot create file %1: %2").arg(tatExpXmlFileName).arg(file.errorString());

	// Write Header
	QTextStream s(&file);
	s << "<TATSettings>\n";
	s << "<TTTConvertExperimentVersion>\n";
	s << CONVERTER_VERSION << "\n";
	s << "</TTTConvertExperimentVersion>\n";

	// Position data
	int numNonEmptyPositions = 0;
	for(int iPos = 0; iPos < positionData.size(); ++iPos) {
		if(!positionData[iPos].isEmpty())
			++numNonEmptyPositions;
	}
	s << "\t<PositionCount count=\"" << numNonEmptyPositions << "\"/>\n";
	s << "\t<PositionData>\n";
	for(int iPos = 0; iPos < positionData.size(); ++iPos) {
		if(positionData[iPos].isEmpty())
			continue;
		s << "\t\t<PositionInformation>\n";
		s << "\t\t\t<PosInfoDimension";
		QList<QString> attributes = positionData[iPos].keys();
		qSort(attributes);
		for(int iAttr = 0; iAttr < attributes.size(); ++iAttr) {
			const QString& curAttr = attributes[iAttr];
			const QString val = positionData[iPos][curAttr];
			s << " " << curAttr << "=\"" << val << "\"";
		}
		s << " comments=\"\"";
		s << " />\n";
		s << "\t\t</PositionInformation>\n";
	}
	s << "\t</PositionData>\n";

	// Wavelength data
	int numNonEmptyWavelengths = 0;
	for(int iWl = 0; iWl < tatXmlWavelengthData.size(); ++iWl) {
		if(!tatXmlWavelengthData[iWl].isEmpty())
			++numNonEmptyWavelengths;
	}
	s << "\t<WavelengthCount count=\"" << numNonEmptyWavelengths << "\"/>\n";
	s << "\t<WavelengthData>\n";
	for(int iWl = 0; iWl < tatXmlWavelengthData.size(); ++iWl) {
		if(tatXmlWavelengthData[iWl].isEmpty())
			continue;
		s << "\t\t<WavelengthInformation>\n";
		s << "\t\t\t<WLInfo";
		QList<QString> attributes = tatXmlWavelengthData[iWl].keys();
		qSort(attributes);
		for(int iAttr = 0; iAttr < attributes.size(); ++iAttr) {
			const QString& curAttr = attributes[iAttr];
			const QString val = tatXmlWavelengthData[iWl][curAttr];
			s << " " << curAttr << "=\"" << val << "\"";
		}
		s << " />\n";
		s << "\t\t</WavelengthInformation>\n";
	}
	s << "\t</WavelengthData>\n";

	// Micrometer per pixel
	if(m_ui.grbSpecifyMmppDirectly->isChecked()) {
		s << "\t<MicrometerPerPixel value=\"" << microMeterPerPixel << "\"/>\n";
	}
	else {
		// Ocular factor (=objective maginification)
		int ocularFactor;
		switch(m_ui.cmbOcularFactor->currentIndex()) {
		case 0:
			ocularFactor = 4;
			break;
		case 1:
			ocularFactor = 5;
			break;
		case 2:
			ocularFactor = 10;
			break;
		case 3:
			ocularFactor = 20;
			break;
		case 4:
			ocularFactor = 40;
			break;
		case 5:
			ocularFactor = 63;
			break;
		case 6:
			ocularFactor = 100;
			break;
		default:
			ocularFactor = 5;
			break;
		}
		s << "\t<CurrentObjectiveMagnification value=\"" << ocularFactor << "\"/>\n";

		// TV Factor
		QString tvFactor;
		if(m_ui.optTVFactor040->isChecked())
			tvFactor = "0.4";
		else if(m_ui.optTVFactor050->isChecked())
			tvFactor = "0.5";
		else if(m_ui.optTVFactor063->isChecked())
			tvFactor = "0.63";
		else if(m_ui.optTVFactor070->isChecked())
			tvFactor = "0.70";
		else 
			tvFactor = "1.0";
		s << "\t<CurrentTVAdapterMagnification value=\"" << tvFactor << "\"/>\n";
	}

	// Cells and conditions (dummy entry for now)
	s << "\t<CellsAndConditions>\n";
	s << "\t\t<NumberOfCellTypes value=\"1\"/>\n";
	s << "\t\t<CellsAndConditions_CellTypes>\n";
	s << "\t\t\t<CNC_CTs_CellType>\n";
	s << "\t\t\t\t<PrimaryCell value=\"\"/>\n";
	s << "\t\t\t\t<Name value=\"\"/>\n";
	s << "\t\t\t\t<Species value=\"\"/>\n";
	s << "\t\t\t\t<Sex value=\"\"/>\n";
	s << "\t\t\t\t<Organ value=\"\"/>\n";
	s << "\t\t\t\t<Age value=\"\"/>\n";
	s << "\t\t\t\t<Purification value=\"\"/>\n";
	s << "\t\t\t\t<Comment value=\"\"/>\n";
	s << "\t\t\t</CNC_CTs_CellType>\n";
	s << "\t\t</CellsAndConditions_CellTypes>\n";
	s << "\t</CellsAndConditions>\n";

	// End of file
	s << "</TATSettings>\n";

	return QString();
}

QString TTTConvertExperiment::createGridLayout(QVector<QHash<QString, QString>>& tatXmlPositionData, const QVector<QHash<QString, QString>>& tatXmlWavelengthData, double microMeterPerPixel)
{
	// Check if have w0 information
	int w0ImageSizeX = 0,
		w0ImageSizeY = 0;
	if(tatXmlWavelengthData.size()) {
		w0ImageSizeX = tatXmlWavelengthData[0].value("width", QString("0")).toInt();
		w0ImageSizeY = tatXmlWavelengthData[0].value("height", QString("0")).toInt();
	}
	if(w0ImageSizeX < 1 || w0ImageSizeY < 1) 
		return QString("Error: image size for channel 0 is missing or invalid (%1x%2 pixels). Experiments must have images with channel 0.").arg(w0ImageSizeX).arg(w0ImageSizeY);

	// Calculate change of posX/posY between adjacent columns/rows (i.e. image size plus distance in micrometers)
	const int DELTA_PIXELS_ALONG_X = 100;
	const int DELTA_PIXELS_ALONG_Y = 100;
	double deltaX = (w0ImageSizeX + DELTA_PIXELS_ALONG_X) * microMeterPerPixel;
	double deltaY = (w0ImageSizeY + DELTA_PIXELS_ALONG_Y) * microMeterPerPixel;
	
	// Create grid
	const int NUM_WELLS_ALONG_X = 10;
	double curX = 0.0,
		curY = 0.0;
	for(int iPos = 0; iPos < tatXmlPositionData.size(); ++iPos) {
		if(tatXmlPositionData[iPos].isEmpty())
			continue;
		tatXmlPositionData[iPos]["posX"] = QString("%1").arg(curX);
		tatXmlPositionData[iPos]["posY"] = QString("%1").arg(curY);

		if((iPos + 1) % NUM_WELLS_ALONG_X == 0) {
			// New row
			curX = 0.0;
			curY += deltaY;
		}
		else {
			// New column
			curX += deltaX;
		}
	}

	// Success
	return QString();
}

void TTTConvertExperiment::specifyDirectlyToggled(bool on)
{
	if(m_ui.grbSpecifyMmppIndirectly->isChecked() != !on)
		m_ui.grbSpecifyMmppIndirectly->setChecked(!on);
}

void TTTConvertExperiment::specifyIndirectlyToggled(bool on)
{
	if(m_ui.grbSpecifyMmppDirectly->isChecked() != !on)
		m_ui.grbSpecifyMmppDirectly->setChecked(!on);
}

double TTTConvertExperiment::getMicroMeterPerPixel() 
{
	// Get from spinbox if set directly or calculate using factors
	if(m_ui.grbSpecifyMmppDirectly->isChecked()) {
		return m_ui.dsbMicroMeterPerPixel->value();
	}
	else {
		// If Scanbin factor not set, use default
		if(m_scanBinFactorW0 < 0)
			m_scanBinFactorW0 = 1;

		// Determine TVFactor
		double tvFactor = 1.0;
		if(m_ui.optTVFactor040->isChecked())
			tvFactor = 0.4;
		else if(m_ui.optTVFactor050->isChecked())
			tvFactor = 0.5;
		else if(m_ui.optTVFactor063->isChecked())
			tvFactor = 0.63;

		// Determine ocular factor
		double ocularFactor;
		switch(m_ui.cmbOcularFactor->currentIndex()) {
		case 0:
			ocularFactor = 5;
			break;
		case 1:
			ocularFactor = 10;
			break;
		case 2:
			ocularFactor = 20;
			break;
		case 3:
			ocularFactor = 40;
			break;
		case 4:
			ocularFactor = 63;
			break;
		case 5:
			ocularFactor = 100;
			break;
		default:
			ocularFactor = 5;
			break;
		}

		// Calculate mmpp
		const int BASE_ANGSTROOM_PER_PIXEL = 1075;
		double microMeterPerPixel = BASE_ANGSTROOM_PER_PIXEL;
		microMeterPerPixel *= 20.0 / ocularFactor;
		microMeterPerPixel /= tvFactor;
		switch (m_scanBinFactorW0) {
		case 1:
			microMeterPerPixel *= 3.0;
			break;
		case 2:
			microMeterPerPixel *= 1.5;
			break;
		case 3:
			//already ok in base value
			break;
		case 4:
			microMeterPerPixel *= 6.0;
			break;
		case 9:
			microMeterPerPixel *= 9.0;
			break;
		case 16:
			microMeterPerPixel *= 12.0;
			break;
		case 25:
			microMeterPerPixel *= 15.0;
			break;
		default:
			//scanning 1 assumed
			microMeterPerPixel *= 3.0;
		}
		microMeterPerPixel /= 10000.0;

		// Done
		qDebug() << "mmpp: " << microMeterPerPixel;
		return microMeterPerPixel;
	}
}

void TTTConvertExperiment::setupNumberChanged(int setupNumber)
{
	updateExperimentNameAndDirectory();
}

void TTTConvertExperiment::showBpWpHelp()
{
	QString msg = "Format:\nc[channel] b[bp] w[wp];c[channel] b[bp] w[wp];...\n\nExample:\nc1 b100 w1000; c3 b200 w2000\n\nSpecifies blackpoint/whitepoint for channel 1 to 100/1000 and for channel 3 to 200/2000.";
	QMessageBox::information(this, "Format Help", msg);
}

QHash<int, QPair<int, int>> TTTConvertExperiment::getBpWpSettings()
{
	QHash<int, QPair<int, int>> ret;

	// Check if bp/wp should not be changed
	if(!m_ui.grbConversion->isChecked()) {	
		ret.insert(-1, QPair<int, int>(0, 0xFFFF - 1));
		return ret;
	}

	// Get default setting
	int bp, wp;
	bp = m_ui.spbBlackPoint->value();
	wp = m_ui.spbWhitePoint->value();
	if(bp < 0 || wp > 0xFFFF || bp > wp) {
		QMessageBox::information(this, "Invalid parameter", QString("Invalid blackpoint / whitepoint settings: bp=%1, wp=%2.").arg(bp).arg(wp));
		return QHash<int, QPair<int, int>>();
	}
	ret.insert(-1, QPair<int, int>(bp, wp));

	// Parse string for individual channels
	QString s = m_ui.lieBpWpPerChannel->text();
	int pos = 0;
	while(pos < s.size()) {
		// Parse 'cX'
		bool ok = false;
		QString t;
		if(!nextBpWpToken(t, s, pos, "c"))
			return QHash<int, QPair<int, int>>();
		nextBpWpToken(t, s, pos);
		int ch = t.toInt(&ok);
		if(!ok) {
			QMessageBox::information(this, "Invalid parameter", QString("Cannot parse channel specific bp/wp: expected channel number, but found '%1' in column %2.").arg(t).arg(pos));
			return QHash<int, QPair<int, int>>();
		}

		// Parse 'bX'
		if(!nextBpWpToken(t, s, pos, "b"))
			return QHash<int, QPair<int, int>>();
		nextBpWpToken(t, s, pos);
		bp = t.toInt(&ok);
		if(!ok) {
			QMessageBox::information(this, "Invalid parameter", QString("Cannot parse channel specific bp/wp: expected blackpoint, but found '%1' in column %2.").arg(t).arg(pos));
			return QHash<int, QPair<int, int>>();
		}

		// Parse 'wX'
		if(!nextBpWpToken(t, s, pos, "w"))
			return QHash<int, QPair<int, int>>();
		nextBpWpToken(t, s, pos);
		wp = t.toInt(&ok);
		if(!ok) {
			QMessageBox::information(this, "Invalid parameter", QString("Cannot parse channel specific bp/wp: expected whitepoint, but found '%1' in column %2.").arg(t).arg(pos));
			return QHash<int, QPair<int, int>>();
		}

		// Store if valid
		if(bp < 0 || wp > 0xFFFF || bp > wp) {
			QMessageBox::information(this, "Invalid parameter", QString("Invalid blackpoint / whitepoint setting for channel %1: bp=%2, wp=%3.").arg(ch).arg(bp).arg(wp));
			return QHash<int, QPair<int, int>>();
		}
		if(ret.contains(ch)) {
			QMessageBox::information(this, "Invalid parameter", QString("Cannot parse channel specific bp/wp: setting for channel %1 already specified in column %2.").arg(ch).arg(pos));
			return QHash<int, QPair<int, int>>();
		}
		ret.insert(ch, QPair<int, int>(bp, wp));

		// Parse ';'
		nextBpWpToken(t, s, pos);
		if(!t.isEmpty() && t != ";") {
			QMessageBox::information(this, "Invalid parameter", QString("Cannot parse channel specific bp/wp: expected ';', but found '%1' in column %2.").arg(t).arg(pos));
			return QHash<int, QPair<int, int>>();
		}
	}

	return ret;
}

bool TTTConvertExperiment::nextBpWpToken(QString& t, const QString& s, int& pos, const QString& expectedToken)
{
	t = "";

	// Get next character, eating spaces
	QChar nextChar = 0;
	while(pos < s.length()) {
		nextChar = s[pos++];
		if(!nextChar.isSpace()) {
			t = nextChar;
			break;
		}
	}

	if(nextChar.isDigit()) {
		// Parse until no digit
		while(pos < s.length()) {
			nextChar = s[pos];
			if(nextChar.isDigit()) {
				++pos;
				t += nextChar;
			}
			else
				break;
		}
	}

	// Check if expectedToken was specified
	if(!expectedToken.isEmpty() && t != expectedToken) {
		QMessageBox::information(this, "Invalid parameter", QString("Cannot parse channel specific bp/wp: expected '%1', but found '%2' in column %3.").arg(expectedToken).arg(t).arg(pos));
		return false;
	}

	// Done
	return true;
}

void TTTConvertExperiment::copyFileOrDir(const QFileInfo& fileOrDir, QString newFolder, QStringList& errors)
{
	// Make sure, newFolder ends with '/'
	newFolder = QDir::fromNativeSeparators(newFolder);
	if(newFolder.right(1) != "/")
		newFolder += '/';

	// Create output directory
	if(!QDir(newFolder).exists() && !QDir(newFolder).mkpath(newFolder)) {
		errors.push_back(QString("Cannot create output folder: %1.").arg(newFolder));
		return;
	}

	// If fileOrDir is a file, just copy it, and if it is a directory, list directory contents and copy them recursively
	if(fileOrDir.isFile()) {
		// Copy file
		QFile sourceFile(fileOrDir.absoluteFilePath());
		QString dstFileName = newFolder + fileOrDir.fileName();
		if(!sourceFile.copy(dstFileName))
			errors.push_back(QString("Error: cannot copy file '%1': %2").arg(fileOrDir.absoluteFilePath()).arg(sourceFile.errorString()));
	}
	else if(fileOrDir.isDir()) {
		// List contents and copy
		QString oldDir = fileOrDir.absoluteFilePath();
		QString newDir = newFolder + fileOrDir.fileName();
		QFileInfoList files = QDir(oldDir).entryInfoList(QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot);
		for(auto itFile = files.constBegin(); itFile != files.constEnd(); ++itFile) {
			copyFileOrDir(*itFile, newDir, errors);
		}
	}
}

void TTTConvertExperiment::showHelp()
{
	const char* txt = "<h3>Instructions</h3>"
		"<p>Use this tool to convert image data from time-lapse microscopy experiments to enable analysis with TTT and QTFy. Options:<ul>"
		"<li><b>Input folder:</b> Click <i>Browse</i> to specify the input folder - it can contain individual images (jpg, png or tif) or sub-folders with images, e.g. one sub-folder per position (=field of view). The found images are shown in the table below.</li>"
		"<li><b>Filter (optional):</b> Only images/subfolders with matching names will be included in the conversion.</li>"
		"<li><b>Parse meta-information from image file names (optional):</b> Enable this option to parse meta-data for each image from its file name. E.g., specify <i>_t</i> as <i>Time point marker</i> so an image with filename <i>img_t123.png</i> will be assigned time point 123. The detected meta information is shown in the table above.</li>"
		"<li><b>Set 16-bit to 8-bit conversion blackpoint/whitepoint (optional):</b> Although TTT supports 16-bit images, TTT Experiment Converter automatically converts 16-bit images to 8-bit for better performance. Use this option to specify the black- and whitepoint for the conversion. Different black- and whitepoints can be used for each imaging channel.</li>"
		"<li><b>Experiment meta information:</b> Specify the acquisition date of the experiment, the initials of the person who acquired it and the setup number. This information is used to determine the experiment name (see next point). Additionally, you can specify the number of micro-meters per pixel.</li>"
		"<li><b>Destination folder:</b> Folder to save results in. Note that the experiment will be saved in a sub-folder with the experiment name as folder name (this is required by TTT).</li>"
		"</ul></p>";

	QMessageBox msgBox;
	msgBox.setWindowTitle("TTT Experiment Converter");
	msgBox.setText(txt);
	msgBox.exec();
}

