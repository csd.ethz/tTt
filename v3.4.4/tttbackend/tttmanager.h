/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TTTMANAGER_H
#define TTTMANAGER_H


// Project includes
#include "tttdata/track.h"
#include "tttdata/trackpoint.h"
#include "tttpositionmanager.h"
#include "exception.h"

// Qt includes
#include <QString>
#include <QPointF>
#include <QWidget>
#include <QHash>
#include <QDomDocument>

// Qt3 includes
#include <Q3Dict>

// Forward declarations
class TTTTracking;
class TTTMainWindow;
class TTTSymbolSelection;
class TTTCellProperties;
class TTTTreeMap;
class TTTTreeStyleEditor;
class TTTPositionLayout;
//class TTTPositionStatistics;
class TTTPositionDetails;
class TTTTextbox;
class TTTTracksOverview;
class TTTPositionXMLDisplay;
class TTTTreeMerging;
class TTTStatsMainWindow;				// ---------- Konstantin --------------------
class TTTStatistics;					// ---------- Konstantin --------------------
class TTTStatisticsPlot;				// ---------- Konstantin --------------------
class TTTStatsPlotPropertiesEditor;		// ---------- Konstantin --------------------
class TTTLogFileConverter;
class Experiment;
class PositionManagerVector;
class Tree;
class MiningResults;
struct BDisplay;
struct CellProperties;
class TTTAutoTracking;


///how many pictures should be loaded at the begin out of the selected pictures?
const int LOAD_ALL_SELECTED_PICTURES = -1; //== all selected

///the default output name of the TAT information file
///BS 2010/02/25 new: the filename is no longer just TATexp.xml, but contains the experiment name as well
///                   example: 090901PH2_TATexp.xml
const QString DEFAULT_TAT_FILENAME = "TATexp.xml";


/**
 * @author Bernhard Schauberger
 * 
 * Keeps the data of one externally assigned trackpoint
 * 
 */
struct MatlabRecord {
        QPointF coords;
	int tries;
	float integral;
	float mean_value;
};



///type mapping a timepoint to a MatlabRecord entry
typedef QMap<int, MatlabRecord> TYPE_tp_to_mlr;

///type which can hold a set of externally calculated tracks
///timepoint |-> cellnr |-> [coords_mat,tries,integral,mean]
typedef QMap<int, TYPE_tp_to_mlr> TYPE_externalTrackMap;


/**
	@author Bernhard
	
	This class handles the core machinery of displaying/tracking and is global for all positions.
	It contains the current timepoint, display settings, file arrays, tree, track, ...
	All timepoint changing events are managed here, thus no recursive signal problems can occur.
	(for this still necessary: semi-static inst and position identifier as private attribute)
	TTTManager also contains the central menu bar, which is inserted into both frmTracking and all frmMovies.
	
	Additionally:
	For each position there is one TTTPositionManager.
*/
class TTTManager : public QObject {

	Q_OBJECT

public:
		
	/**
	 * returns the static instance of this class; if it does not yet exist, it is created and initialized
	 * @return the (unique) TTTManager instance
	 */
	static TTTManager& getInst();

	~TTTManager();


#ifndef TREEANALYSIS	

	///pointers to all used forms
	TTTAutoTracking *frmAutoTracking;
	TTTTracking *frmTracking;
	TTTMainWindow *frmMainWindow;
	TTTSymbolSelection *frmSymbolSelection;
	TTTTreeStyleEditor *frmTreeStyleEditor;
	TTTCellProperties *frmCellProperties;
	TTTTreeMap *frmTreeMap;
	TTTPositionLayout *frmPositionLayout;
	//TTTPositionStatistics *frmPositionStatistics;
	TTTPositionDetails *frmPositionDetails;
	TTTTextbox *frmTextbox;
	TTTTracksOverview *frmTracksOverview;
	TTTPositionXMLDisplay *frmPositionXMLDisplay;
	TTTTreeMerging *frmTreeMerging;
	TTTLogFileConverter* frmLogFileConverter;

#endif	
	//TTTStatistics *frmStatistics; // ---------- Konstantin --------------------
	TTTStatistics *frmStaTTTsMain; // ---------- Laura --------------------
	TTTStatsMainWindow *frmStatsMainWindow; // ---------- Konstantin --------------------
	TTTStatisticsPlot *frmStatisticsPlot; // ---------- Konstantin --------------------
	TTTStatsPlotPropertiesEditor* frmTTTStatsPlotProperties; // ---------- Konstantin --------------------

	
	// Delete TTTManager instance
	void destroy();
	
	/**
	 * sets whether the new coordinate scheme should be used (experiment relative, not position dependent).
	 * this is only possible if the tat-xml file is present!
	 * @param _useNewPositions true, if the tat file was there; must be false otherwise
	 */
	void setUSE_NEW_POSITIONS(bool _useNewPositions) {
		use_NEW_POSITIONS = _useNewPositions;
	}
	
	/**
	 * @return whether the new experiment relative coordinate scheme is used
	 */
	bool USE_NEW_POSITIONS() const {
		return use_NEW_POSITIONS;
	}
	
	/**
	 * creates one instance of all used forms
	 */
	void createForms();

	/**
	 * establishes connections between forms (only between forms that TTTManager owns)
	 */
	void connectForms();

    // ---------- Konstantin --------------------
    /**
     * adds an experiment to the experiment hash
     * @param _key the unique experiment key (experiment base name)
     * @param _exp pointer to a Experiment instance
     */
    void addExperiment(const QString &_key, Experiment *_exp);

    // ---------- Konstantin --------------------
    /**
     * returns the Experiment instance for the desired experiment base name (&_key)
     * @param _key the unique experiment key (experiment base name)
     * @return a pointer to a Experiment instance
     */
    Experiment* getExperiment(const QString &_key) const;
	
	/**
	 * Adds a position manager to the current dictionary
	 * @param key the unique position key
	 * @param pm the position manager
	 */
#ifndef TREEANALYSIS
	void addPositionManager (int _key, TTTPositionManager *_pm) {
		if(TTTPositionManager* old = m_positionManagers.value(_key, 0))
			delete old;
		m_positionManagers.insert(_key, _pm);
	}
#endif

	/**
	 * @deprecated
	 *
	 * adds a position manager to the current dictionary
	 * @param _key the unique position key
	 * @param _pm the position manager
	 */
	void addPositionManager (const QString &_key, TTTPositionManager *_pm) {
#if TREEANALYSIS
		if(m_positionManagers.contains(_key))
			m_positionManagers.remove(_key);
		m_positionManagers.insert(_key, _pm);
#else
		addPositionManager(_key.toInt(), _pm);
#endif
	}

	/**
	 * Returns the PositionManager instance for the desired position
	 *
	 * @param _key the unique position key
	 * @return a pointer to a PositionManager instance or nullptr if position was not found.
	 */
	TTTPositionManager* getPositionManager (int _key) const {
#ifndef TREEANALYSIS
		return m_positionManagers.value(_key, nullptr);
#else
		throw ttt_Exception("Not implemented for staTTTs");
#endif
	}

	
	/**
	 * @deprecated 
	 *
	 * returns the PositionManager instance for the desired position
	 * NOTE: very important: must have constant access time
	 * @param _key the unique position key
	 * @return a pointer to a PositionManager instance
	 */
	TTTPositionManager* getPositionManager (const QString &_key) const {
#if TREEANALYSIS
		return m_positionManagers.value(_key, nullptr);
#else
		return getPositionManager(_key.toInt());
#endif
	}
	
	/**
	 * Set the current position.
	 * @param _key the unique key of the position to be set
	 */
	void setCurrentPosition(int key) {
		// Do nothing if position has not changed
		if(m_currentPositionKey == key)
			return;

		// Change position and emit signal
		int oldPos = m_currentPositionKey;
		m_currentPositionKey = key;
		emit currentPositionChanged (oldPos, key);
	}

	/**
	 * @deprecated 
	 *
	 * sets the current position
	 * @param _key the unique key of the position to be set
	 */
	void setCurrentPosition (const QString &_key) {
		setCurrentPosition(_key.toInt());
	}
	
	/**
	 * @return the current position key
	 */
	int getCurrentPosition() const {
		return m_currentPositionKey;
	}
	
	/**
	 * sets the base position (the position in which the tree was started)
	 * @param _key the unique key of the base position
	 */
	void setBasePosition (int _key) {
		m_basePositionKey = _key;
	}
	
	/**
	 * @return the base position key
	 */
	int getBasePosition() const	{
		return m_basePositionKey;
	}
	
	/**
	 * returns the PositionManager instance for the CURRENT position
	 * @return a pointer to the "current" PositionManager instance
	 */
	TTTPositionManager* getCurrentPositionManager() const {
		return getPositionManager(m_currentPositionKey);
	}
	
	/**
	 * returns the PositionManager instance for the BASE position (the one in which the tree was started)
	 * @return a pointer to the "base" PositionManager instance
	 */
	TTTPositionManager* getBasePositionManager() const {
		return getPositionManager(m_basePositionKey);
	}
	
	/**
	 * returns all position managers as they are
	 * @return a QDict instance; the values can be obtained via an iterator
	 */
	QHash<int, TTTPositionManager*>& getAllPositionManagers() {
#if TREEANALYSIS
		throw ttt_Exception("Not implemented for stattts!");
#else
		return m_positionManagers;
#endif
	}
	//Q3Dict<TTTPositionManager>& getAllPositionManagers();
	
	/**
	 * returns all position managers sorted alphabetically according to their index
	 * @return a pointer to a QPtrVector instance; the values can be obtained via an iterator and are passed in the sort order
	 */
	std::vector<TTTPositionManager*> getAllPositionManagersSorted() const;
	//PositionManagerVector* getAllPositionManagersSorted();
	
	/**
	 * sets whether the current folders do not contain pictures at all
	 * Necessary to be able to load ttt files without pics
	 * @param _loadOnlyTTTFiles true, if only ttt files (without pictures) should be loadable
	 */
	void setLoadTTTFilesOnly (bool _loadOnlyTTTFiles) {
		loadOnlyTTTFiles = _loadOnlyTTTFiles;
	}
	
	/**
	 * @return whether the current folders do not contain pictures at all (but ttt files should be loadable in spite)
	 */
	bool loadTTTFilesOnly() const {
		return loadOnlyTTTFiles;
	}
	
	/**
	 * sets a new global timepoint and sends a signal that the timepoint was changed
	 * this method calls the timepoint setting method for all positions
	 * @param _timepoint the timepoint of the movie
	 */
	void setTimepoint (int _timepoint, int _wavelength = -1, int _zIndex = -1);
	
	/**
	 * sets a new timepoint without sending the update signal
	 * this method calls the timepoint setting method for all positions
	 * @param _timepoint the timepoint of the movie
	 */
	void setTimepoint_withoutSignal (int _timepoint, int _wavelength = -1, int _zIndex = -1);
	
	/**
	 * clears the internal Track and TrackPoint fields
	 * necessary when for example the colony changed, but not the timepoint
	 */
	void clearFields();
	
	/**
	 * @return the current global timepoint (reads it directly from the current position)
	 */
	int getTimepoint() const {
		if(TTTPositionManager* tttpm = getCurrentPositionManager())
			return tttpm->getTimepoint();
		return -1;
	}
	
	/**
	 * returns a list of the tracks at the current timepoint
	 * @return a QIntDict containing all tracks that exist at the current timepoint
	 */
	Q3IntDict<Track>* getCurrentTracks();
	
	/**
	 * returns a list of the trackpoints at the current timepoint
	 * @return a QValueList containing all trackpoints (of different cells) that exist at the current timepoint
	 */
	Q3ValueList<TrackPoint>* getCurrentTrackPoints();
	
	/**
	 * sets the current tree
	 * @param _tree 
	 */
	void setTree (Tree *_tree)
		{tree = _tree;}
	
	/**
	 * returns the current Tree instance
	 * @return a pointer to the current tree instance
	 */
	Tree* getTree() const {
		return tree;
	}
	
	/**
	 * sets the current Track instance
	 * @param _track a Track pointer
	 */
	void setCurrentTrack (Track *_track)
		{currentTrack = _track;}
	
	/**
	 * @return a pointer to the current Track instance
	 */
	Track* getCurrentTrack() const {
		return currentTrack;
	}
	
	/**
	 * sets the nas drive
	 * @param _nasDrive the path to the network attached storage
	 */
	void setNASDrive (const QString &_nasDrive)
	{
		NASDrive = _nasDrive;
	}
	
	/**
	 * @return the path to the network attached storage (with a "/" at the end)
	 */
	const QString getNASDrive()
	{
		if (NASDrive.isEmpty() || NASDrive.right (1) == "/")
			return NASDrive;
		else
			return NASDrive + "/";
	}
	
	/**
	 * called when the cell editor (tree window) is closed
	 * sets the base position to nothing, unloads all images and resets all global variables to the state when tTt was started
	 * (but the experiment folder is kept)
	 */
	void resetBasePosition();
	
    /**
     * causes tTt to end: saves the global settings, asks for saving the current tree, closes all forms
     * @param _closeTTTPositionLayout whether TTTPositionLayout should be closed as well (default = true); must be false when it is the triggering form
     */
    void shutDownTTT (bool _closeTTTPositionLayout = true);

	/**
	 * called on program shutdown
	 * saves the current user settings and other stuff
	 */
	void saveGlobalSettings();
	
	/**
	 * starts the global tracking process, if it is not yet running
	 * NOTE this method is called by TTTTracking, while stopTracking() is called by TTTMovie (nested mode)
	 * @param noChangeTimePoint do not change current time point.
	 * @return success
	 */
	bool startTracking(bool noChangeTimePoint = false);
	
	/**
	 * stops the global tracking process, if it is running
	 * NOTE confer note at startTracking()
	 * @param _notifyTTTAutoTrackingWindow if TTTAutoTracking window should be notified
	 * @return success
	 */
	bool stopTracking(bool _notifyTTTAutoTrackingWindow = true);

	/**
	 * add trackpoint (in tracking mode only!)
	 * redirects to track->addMark(..) and sets TrackingStartFirstTimepoint if it has no valid value yet
	 */
	void addTrackpoint (int _timepoint, float _x, float _y, float _diameter, bool _setBackground, short int _mouseButtons, QString _position);
	
	/**
	 * @return whether tTt is currently in tracking mode
	 */
	bool isTracking() const {
		return Tracking;
	}
	
	/**
	 * @return the first trace of the currently tracked cell at the time when tracking was started
	 */
	int getTrackingStartFirstTimepoint() const {
		return TrackingStartFirstTimepoint;
	}
	
	/**
	 * @return whether the last finished tracking process was forward or backward 
	 *         (backward = the first existence timepoint of the tracked cell was set to an earlier timepoint)
	 */
	bool trackingWasForward() const;
	
	/**
	 * sets the stop reason of the last tracking process
	 * @param _tsr the track stop reason
	 */
	void setTrackStopReason (TrackStopReason _tsr)
		{stopReason = _tsr;}
	
	/**
	 * @return the cell stop reason of the last tracking process
	 */
	TrackStopReason getTrackStopReason() const {
		return stopReason;
	}
	
	/**
	 * sets the microscope ocular factor
	 * @param _of the ocular factor (5, 10 or 20)
	 */
	void setOcularFactor (int _of) {
		ocularFactor = _of;
	}
	
	/**
	 * sets the microscope TV adapter factor
	 * @param _tvf the TV adapter magnifying factor (e.g. 0.63 or 1.0)
	 */
	void setTVFactor (float _tvf) {
		tvFactor = _tvf;
	}
	
	/**
	 * sets both microscope factors at once
	 * @param _ocularFactor the ocular factor (5, 10 or 20)
	 * @param _tvFactor the TV adapter magnifying factor (either 0.63 or 1.0)
	 */
	void setDisplayFactors (int _ocularFactor, float _tvFactor) {
		ocularFactor = _ocularFactor;
		tvFactor = _tvFactor;
	}
	
	/**
	 * @return the ocular factor (5, 10 or 20)
	 */
	int getOcularFactor() const {
		return ocularFactor;
	}
	
	/**
	 * @return the TV adapter magnifying factor (either 0.63 or 1.0)
	 */
	float getTVFactor() const {
		return tvFactor;
	}
	
	/**
	 * sets whether the coordinate system is inverted
	 * @param _inverted true, if the coordinate system is inverted (positive axes are up and left); false, if it is normal (positive axes are down and right)
	 */
	void setInvertedCoordinateSystem (bool _inverted) {
		coordinateSystemInverted = _inverted;
	}
	
	/**
	 * @return sets whether the coordinate system is inverted (= true, if the coordinate system is inverted (positive axes are up and left); false, if it is normal (positive axes are down and right))
	 */
	bool coordinateSystemIsInverted() const {
		return coordinateSystemInverted;
	}

	/**
	 * sets the filename of the experiment comment
	 * @param _commentFN the filename of the file that contains the comment
	 */
	void setExperimentCommentFilename (const QString& _commentFN) {
		experimentCommentFilename = _commentFN;
	}
	
	/**
	 * @return the filename of the experiment comment
	 */
	QString getExperimentCommentFilename() const {
		return experimentCommentFilename;
	}

	/**
	 * @return if backward tracking mode is on, i.e. if the last tracking since program start was backwards
	 */
	bool inBackwardTrackingMode() const {
		return backwardTrackingMode;
	}
	
	
///following methods return the positions  according to their layout
	
	/**
	 * the position at the provided, experiment global, x/y coordinates
	 * its best called with the center point of a position, as then there is no overlap and thus no problem
	 * @WARNING if more than one position contains this point, a random one among these is chosen and returned!
	 * @param _absX 
	 * @param _absY 
	 * @return 
	 */
	TTTPositionManager* getPositionAt (float _absX, float _absY);
	
	/**
	 * Investigates all positions that contain the provided coordinate.
	 * In the middle of a position, the dictionary should contain only this position, but at the border of a position, the same point is contained in more than one position, 
	 *  and all these are returned
	 * @param _absX the abscisse of the point (experiment global)
	 * @param _absY the ordinate of the point (experiment global)
	 * @return a dict with all the positions that contain the provided point
	 */
	Q3IntDict<TTTPositionManager> getAllPositionsAt (float _absX, float _absY);
	
	TTTPositionManager* getPositionLeftOf (const QString &_key);
	
	TTTPositionManager* getPositionRightOf (const QString &_key);
	
	TTTPositionManager* getPositionTopOf (const QString &_key);
	
	TTTPositionManager* getPositionBottomOf (const QString &_key);
	
	/**
	 * 
	 * @param _key 
	 * @param _direction 0 = topleft, 1 = top, 2 = topright, 3 = left, 4 = right, 5 = bottomleft, 6 = bottom, 7 = bottomright
	 * @return 
	 */
	TTTPositionManager* getPositionSomewhereOf (const QString &_key, int _direction);
	
///end
	
	/**
	 * tests whether a given point is within a position frame
	 * @param _tttpm the position which should be tested if it contains the point
	 * @param _point the coordinates of the point 
	 * @return true => point is within the position's borders (including the frame); false => out
	 */
    bool pointIsInPosition (const TTTPositionManager *_tttpm, const QPointF _point);
	
	/**
	 * set the mining results object (usually in TTTTracking)
	 * @param _miningResults the mining results, parsed from external files
	 */
	void setMiningResults (MiningResults* _miningResults) {
		miningResults = _miningResults;
	}
	
	/**
	 * @return the mining results object
	 */
	MiningResults* getMiningResults() const {
		return miningResults;
	}
	
	/**
	 * sets the list of externally assigned tracks (currently from MATLAB)
	 * @param _extTracks the tracks
	 */
	void setExternalTracks (TYPE_externalTrackMap *_extTracks) {
		if (externalTracks)
			delete externalTracks;
		externalTracks = _extTracks;
	}
	
	/**
	 * @return the externally assigned tracks
	 */
	TYPE_externalTrackMap* getExternalTracks() const {
		return externalTracks;
	}
	
	void setShowExternalTracks ( bool on ) {	
		showExternalTracks = on;
	}
	
	bool getShowExternalTracks() const {	
		return showExternalTracks;
	}
	
	void setExperimentBasename(const QString& theValue)	{	
		experimentBasename = theValue;
	}
	
	QString getExperimentBasename() const {	
		return experimentBasename;
	}
	
	/**
	 * sets the display settings that should be stored in the RAM
	 * note: needs to be a deep copy, as maybe the movie window from which the data originate is closed!
	 */
	void setRAMDisplaySettings (const BDisplay &_rds);
	
	/**
	 * @return the display settings from the RAM
	 */
	const BDisplay& getRAMDisplaySettings() const;
	
	/**
	 * quits tTt, optionally displaying a QMessageBox with the provided message before closing
	 * @param _message the message to be displayed
	 * @param _returnCode the return code to be returned by main(): 0 is success, !=0 is erroneous
	 */
	void quitProgram (const QString &_message = "", int _returnCode = 0);

	/**
	 * sets whether tTt is in statistics mode
	 * @param _statisticsMode true -> statistics mode, false -> tracking mode
	 */
	void setStatisticsMode (bool _statisticsMode) {
		statisticsMode = _statisticsMode;
	}
	
	/**
	 * @return whether tTt is in statistics mode
	 */
	bool isInStatisticsMode() const	{
		return statisticsMode;
	}
	
	/**
	 * calculates the next free suffix for a ttt file
	 * @param _tttpm the position manager for which this function should be executed
	 * @param _suffixOnly whether only the suffix should be returned (== colony index)
	 * @return the complete filename, if _suffixOnly == false, or the colony index only (e.g. '-001'), if _suffixOnly == true
	 */
	QString getNextFreeFile (TTTPositionManager *_tttpm, bool _suffixOnly);
	
	/**
	* sets the ttt config document (normally read already in the main() procedure)
	*/
	void setTTTConfigDocument (QDomDocument &_dom);

	/**
	* returns the ttt config document
	*/
	QDomDocument& getTTTConfigDocument();

	/**
	* set the path of the tTt configuration XML
	*/
	void setTTTConfigPath (const QString &_tttConfigPath) {
		tttConfigPath = _tttConfigPath;
	}

	/**
	* returns the path of the tTt configuration XML
	*/
	const QString &getTTTConfigPath() {
		return tttConfigPath;
	}

	/**
	* sets the keyboard grabbing window (used e.g. in tracking mode)
	*/
	void setKeyboardGrabber (QWidget *_kg) {
		keyboardGrabber = _kg;
	}

	/**
	* @return the keyboard grabbing window (used e.g. in tracking mode)
	*/
	QWidget* getKeyboardGrabber() const	{
		return keyboardGrabber;
	}

	/**
	 * Reset cell properties for new trackpoints
	 */
	void resetTrackingCellProperties();

	/**
	 * Get tracking cell properties
	 */
	const CellProperties& getTrackingCellProperties() const;

	/**
	 * Set adherence property for new trackpoints
	 */
	void setTrackingCellAdherence(ADHERENCE_STATE _trackingAdherence);

	/**
	 * Set endomitosis property for new trackpoints
	 */
	void setTrackingEndomitosis(bool _on);

	/**
	 * Set differentiation property for new trackpoints
	 */
	void setTrackingDifferentiation(TissueType _trackingTissueType, CellGeneralType _trackingCellGeneralType, char _trackingCellLineage);

	/**
	 * Set wavelength property for new trackpoints
	 */
	void setTrackingWavelength(unsigned int _wavelength, bool _on);

	/**
	 * Get number of frames to skip after each new track point during tracking.
	 */
	int getFramesToSkipWhenTracking() const {
		return m_framesToSkipWhenTracking;
	}

public slots:

	/**
	 * displays the ttt customization form (modeless)
	 */
	void showCustomizeDialog();

	/**
	 * Redraw tracks (cell circles) in all open movie windows
	 */
	void redrawTracks();

	/**
	 * Set number of frames to skip after each new track point during tracking.
	 */
	void setFramesToSkipWhenTracking(int framesToSkipWhenTracking);

signals:
	
	/**
	 * emitted when setTimepoint() was called
	 * NOTE: the usual signal to connect to is emitted by TTTPositionManager, not TTTManager
	 *       Thus, take care what you connect!
	 * @param _currentTimepoint the current (new) timepoint
	 * @param _oldTimepoint the old timepoint
	 */
	void timepointSet (int _currentTimepoint, int _oldTimepoint);
	
	/**
	 * emitted when the current position pointer was changed
	 * @param _oldPosition the key of the old current position
	 * @param _newPosition the key of the new current position
	 */
	void currentPositionChanged (int _oldPosition, int _newPosition);

	/**
	 * Backward tracking mode has been enabled or disable.
	 * @param _on true if it is now on.
	 */
	void backwardTrackingModeSet(bool _on);
	
private:

	//the static global instance (unique in the complete program)
	static TTTManager *inst;
	
	// Private Constructors to prevent instatiation outside of getInst()
	TTTManager();	
	//~TTTManager();

	// Private Copy Constructor to prevent copying
	TTTManager(const TTTManager &);
	TTTManager& operator=(const TTTManager &);



	/**
	 * Inherit selected cell properties from mother cell when tracking  (and enable corresponding buttons in properties form)
	 */
	void inheritCellProperties();

	/**
	 * Change backward tracking mode
	 * @param _on true if it should be enabled
	 */
	void setBackwardTrackingMode(bool _on);
	
	///global flag:
	///whether the new experiment relative coordinates should be used (only if the tat-xml file is present) (=> true)
	///or not (=> false)
	///set in TTTMainWindow when the tat-xml file is read (if available => true)
	bool use_NEW_POSITIONS;
	
	///contains the basename of the experiment
	///e.g. "051104-2"
	QString experimentBasename;
	
	///contains the position specific data of all positions as dictionary
	///the key is the unique position identifier
	//Q3Dict<TTTPositionManager> positionManagers;
#if TREEANALYSIS
	QHash<QString, TTTPositionManager*> m_positionManagers;
#else
	QHash<int, TTTPositionManager*> m_positionManagers;
#endif
    // ---------- Konstantin --------------------
    /// contains the experiment specific data (like preincubation time)
    QHash<QString, Experiment*> experiments;

	///the current position key
	int m_currentPositionKey;
	
	//@deprecated has no special meaning anymore
	//
	///the base position key (the key of the position in which tracking was started)
	///necessary for frmTracking, Tree, TreeDisplay, ...
	int m_basePositionKey;
	
	///the tracks existing at the current timepoint (is updated on request with every timepoint change)
	Q3IntDict<Track> *currentTracks;
	
	///the track points existing at the current timepoint (is updated on request with every timepoint change)
	Q3ValueList<TrackPoint> *currentTrackPoints;
	
	///the current tree
	Tree *tree;
	
	///the current track
	Track *currentTrack;
	
	///the Network Attached Storage directory (mount point)
	///the default location can be obtained via SystemInfo, but the user can change this
	QString NASDrive;
	
	///whether tTt is in tracking mode currently
	bool Tracking;
	
	///the first trace of the current cell when tracking was started
	int TrackingStartFirstTimepoint;
	
	///the first trace of the current cell when tracking was finished (necessary for deciding the direction)
	int TrackingEndFirstTimepoint;

	///the cell stop reason of the last tracking process
	TrackStopReason stopReason;
	
	///contains the factor of the microscope tv adapter (either 0.63 or 1.0)
	float tvFactor;
	
	///contains the ocular factor of the microscope (5, 10 or 20)
	int ocularFactor;
	
	///whether the coordinate system is inverted 
	///true => the coordinate system is inverted (positive axes are up and left)
	///false => normal (positive axes are down and right)
	bool coordinateSystemInverted;
	
	///contains the filename of the experiment comment (is displayed in the statistics window)
	QString experimentCommentFilename;
	
	///whether the current folders do not contain pictures at all
	bool loadOnlyTTTFiles;
	
	///the mining results (assigned in TTTTracking, if necessary)
	MiningResults *miningResults;
	
	///the list of tracks that was assigned externally
	TYPE_externalTrackMap *externalTracks;
	
	///whether the externally assigned tracks should be shown
	bool showExternalTracks;
	
	///the display settings currently copied to the RAM; used for fast transfer of the display settings from one window to another
	BDisplay ramDisplaySettings;
	
	/**
	 * indicates whether we are in statistics or tracking mode for correct index in 
	 * TATXMLParser::startElement(...) and SystemInfo::getPositionIndex()
	 */
	bool statisticsMode;
	
    ///keeps the current ttt configurations (can be manipulated as well)
    QDomDocument tttConfigDOM;
	
    ///the path of the configuration document
    QString tttConfigPath;

    ///the widget that currently grabs the keyboard focus (only <>0 in tracking mode)
    QWidget *keyboardGrabber;

	// Selected CellProperties for newly added trackpoints in tracking mode (only adherence, differentiation and wavelenghts are used)
	CellProperties trackingCellProperties;

	// Backward tracking mode. Set to true only if the last tracking since the program has been started was backwards
	bool backwardTrackingMode;

	// Frames to skip after each new track point during tracking (default: 0)
	int m_framesToSkipWhenTracking;

//private methods
	
	/**
	 * sets the current tracks, according to the given timepoint
	 * @param _timepoint the timepoint for which the existing tracks should be read
	 */
	void setCurrentTracks (int _timepoint);

	/**
	 * sets the current track points, according to the given timepoint
	 * @param _timepoint the timepoint for which the existing track points should be read
	 */
	void setCurrentTrackPoints (int _timepoint);

	/**
	 * Synchronize tracking cell properties in all currently opened TTTMovie windows
	 */
	void sychronizeCellPropertiesInMovieWindows();
	
};

#endif
