/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef exception_h__
#define exception_h__

// Qt includes
#include <QString>

/**
 * Generic exception class
 * this class is a dublicate of TTTException class - to be replaced
 */
class ttt_Exception : public std::exception {
public:
	// Create exception with description in _descr
	ttt_Exception(const QString& _descr) : 
	  std::exception(_descr) { };

};

#endif // exception_h__