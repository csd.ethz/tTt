/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PICTUREARRAY_H
#define PICTUREARRAY_H

// Qt
#include <QList>
#include <QThread>
#include <QMutex>
#include <QSharedPointer>
#include <QAbstractListModel>

// Project
#include "picturecontainer.h"
#include "displaysettings.h"
#include "pictureindex.h"
#include "tttdata/systeminfo.h"
#include "tttdata/track.h"
#include "tttdata/trackpoint.h"
#include "tttgui/statusbar.h"




///very important:
///sets whether the complete QImage should be kept in memory
///true: keep complete image regardless of loading region; false: keep only the selected region (uses less memory, of course)
//const bool KEEPCOMPLETEIMAGES = false;


/////the number of pictures that should be loaded in each timer event
/////note: should be greater than at least the number of wavelenghts
/////NOTE: the number written here is NOT the actual number of pics loaded during each event, but rather the ATTEMPTS (due to missing wavelengths)
//const int PICTURES_TO_LOAD_PER_TIMER_EVENT = 25;


class QPaintDevice;
class TTTPositionManager;
class PictureView;
class ImageLoaderThread;



/**
@author Bernhard
	
	This class contains pointers to the pictures in memory (to PictureContainer objects) for one position
	There are many access functions, and there is absolutely no need for direct access
	to any PictureContainer object, PictureArray does it all.
	Most important functions are initPicture(), loadPictures() and drawPicture().
	A picture is always uniquely identified by a timepoint and a wavelength.
	It is implemented as QIntDict<PictureContainer>
*/
class PictureArray : public QObject 
{

	Q_OBJECT
	
public:
	
	
	/**
	 * constructs an empty picture array
	 * @param _positionManager the position manager
	 * @param _size the initial size of the picture array
	 * @param parent the parent QObject (usually 0)
	 * @param name the name of the picture array
	 */
	PictureArray (TTTPositionManager *_positionManager, unsigned int _size = 17, QObject *parent = 0, const char *name = 0);
	
	~PictureArray();
	
	///**
	// * creates a PictureContainer object (without actually loading the picture)
	// * @param _index the timepoint of the picture
	// * @param _wavelength the wavelength of the picture
	// * @param _filename the filename of the picture
	// * @param _loadingFlag whether the image should be marked as selected (to be loaded or unloaded)
	// * @param _defaultSize _defaultSize == size of the first image of the default wavelength, necessary to stretch all other pictures to this size
	// */
	//void initPicture (unsigned int _index, int _wavelength, QString _filename, bool _loadingFlag = false, QRect _defaultSize = QRect());

	/**
	 * Loads or unloads all pictures which have have been marked by a prior call to setLoading(). Errors are reported via signals.
	 * @param _loadAsynchronous whether the function should return immediately (true) or block until loading has completed (false, default). Ignored for unloading.
	 * @param _unload whether pictures are to be unloaded.
	 */
	void loadPictures (bool _loadAsynchronous = false, bool _unload = false);
	
	/**
	 * Load one picture. This always fails, if regular loading with threads is currently under progress!
	 * @param _index of picture to load
	 * @return true if image was already loaded or has been loaded successfully
	 */
	bool loadOnePicture(const PictureIndex& _index);

	///**
	// * loads the next picture that has the loading flag set
	// * @param _fromIndex the starting timepoint (from which the next, including this timepoint, is calculated)
	// * @param _wavelength the wavelength that the next picture must have
	// * @return a PictureIndex object with the timepoint/wavelength of the loaded picture (if succesful), -1 if there is no next picture and -2 if loading the picture failed
	// */
	//PictureIndex loadNextPicture (unsigned int _fromIndex = 0, int _wavelength = -1);
	
	/**
	 * returns the index of the first loaded picture within _waveLength
	 * @param _waveLength the wavelength for which the first picture should be returned; _waveLength == -1 means the complete range
	 * @return a PictureIndex object with the timepoint/wavelength of the first loaded picture (if succesful)
	 */
	PictureIndex getFirstLoadedPictureIndex (int _waveLength = -1) const;
	
	/**
	 * returns the index of the last loaded picture within _waveLength
	 * @param _waveLength the wavelength for which the last picture should be returned; _waveLength == -1 means the complete range
	 * @return a PictureIndex object with the timepoint/wavelength of the last loaded picture (if succesful)
	 */
	PictureIndex getLastLoadedPictureIndex (int _waveLength = -1) const;
	
	/**
	 * returns the index of the next/previous picture after _index that is in memory
	 * @param _pi the current picture index, the base for the next picture (Important: it is not required that an image exists at pi)
	 * @param _forward whether the next picture should be returned, otherwise the previous
	 * @param _waveLengthConstant if the next picture should have the same wavelength as _pi
	 * @param _zIndexConstant if the next picture should have the same z-index as _pi
	 * @return (-1), if there is no next picture, otherwise a PictureIndex with the timepoint/wavelength of the next loaded picture
	 */
	PictureIndex nextLoadedPicture (const PictureIndex &_pi, bool _forward = true, bool _waveLengthConstant = true, bool _zIndexConstant = true) const;
	
	/**
	 * tests whether a picture at the provided timepoint/wavelength exists
	 * note: this is equivalent to calling FileInfoArray::exists(), as long as both arrays refer to the same position
	 * @param _timepoint the timepoint to be tested
	 * @param _zIndex the z-index (must not be -1)
	 * @param _wavelength the wavelength to be tested (-1 => anyone is valid)
	 * @return true -> a picture exists (but is not necessarily loaded!)
	 */
	bool pictureExists (int _timepoint, int _zIndex, int _wavelength = -1);

	/**
	 * Apply background correction to image.
	 * @param pi PictureIndex of image to apply background correction to.
	 * @param bgPath path to background images. Use default location if empty.
	 * @return	0 if background correction was applied successfully
	 *			1 if background correction had already been applied before
	 *			2 if background image could not be opened
	 *			3 if picture index is invalid
	 *			4 if image was not loaded
	 *			5 if gain image could not be opened
	 *			6 if other error occurred
	 */
	int applyBackgroundCorrection(const PictureIndex& pi/*, const QString& bgPath = ""*/);
	
	/**
	 * Check if background correction was applied on image.
	 * @param pi PictureIndex of image
	 */
	bool backgroundCorrectionApplied(const PictureIndex& pi);

	///**
	// * @return the size of the array (not necessarily the number of pictures!)
	// */
	//unsigned int size() const;
	
	/**
	 * sets image _number to be loaded (default) or not loaded
	 * does NOT load the picture itself (this must be done with loadPictures())
	 * @param _timepoint the timepoint of the image to be loaded
	 * @param _zIndex the z-index
	 * @param _wavelength the wavelength of the image to be loaded
	 * @param _load whether the image should be loaded (true) or unloaded (false). If image already loaded/unloaded, nothing happens.
	 */
	void setLoading (unsigned int _timepoint, int _zIndex, int _wavelength, bool _load = true);
	
	/**
	 * returns whether the specified picture is selected to load/unload
	 * note: selection does NOT imply "should be loaded"!
	 * @param _timepoint the timepoint of the query picture
	 * @param _waveLength the wavelength of the query picture
	 * @return whether the loading flag is set for this picture
	 */
	bool getLoadingFlag (unsigned int _timepoint, int _zIndex, int _waveLength = -1) const;
	
	/**
	 * sets the selection flag for the specified picture
	 * @param _timepoint the timepoint of the picture to be set
	 * @param _waveLength the wavelength of the picture to be set
	 * @param _value the selection status
	 */
	void setSelection (unsigned int _timepoint, int _zIndex, int _wavelength, bool _value = true);
	
	/**
	 * returns whether the selection flag is set
	 * @param _timepoint the timepoint of the query picture
	 * @param _waveLength the wavelength of the query picture
	 * @return whether this picture is selected (true) or not (false)
	 */
	bool getSelectionFlag (unsigned int _timepoint, int _zIndex, int _waveLength = -1) const;
	
	/**
	 * inits the picture array
	 */
	void init();
	
        /* *
	 * draws the picture the specified picture
	 * @param _timepoint the timepoint of the picture to draw
	 * @param _wavelength the wavelength of the picture to draw
	 * @param _pd the current paint device (should exist!)
	 * @param _colorChannel the color channel (necessary for selecting trans colors and alpha blending)
	 * 			-2 == none, -1 == alpha blending, >= 0 => TransColorIndex, cf. BDisplay)
	 * @param _img the currently merged picture (necessary for overlays); into this the newly drawn is merged with the colorchannel
	 * @param _complete whether the complete image from memory should be drawn without regarding the current display settings
	 * @return whether drawing was successful
	 */
        //bool drawPicture (int _timepoint, int _wavelength, QPaintDevice *_pd, int _colorChannel = -2, QImage *_img = 0, bool _complete = false) const;
	
	/**
	 * returns the index within the field of LOADED pictures (necessary for frmMovie)
	 * the inversion of getPictureIndexLoaded()
	 * @WARNING: might be slow!!
	 * @param _picIndex the picture index of the query picture
	 * @return the ordinary number within the array of loaded pictures
	 */
	int getAbsIndexLoaded (PictureIndex _picIndex) const;
	
	///**
	// * returns the PictureIndex for an index, which specifies the index within all LOADED pictures
	// * the inversion of getAbsIndexLoaded()
	// * @WARNING: might be slow!!
	// * @param _absIndexLoaded the ordinary number within the array of loaded pictures
	// * @return a PictureIndex object containing the timepoint/wavelength of the desired picture
	// */
	//PictureIndex getPictureIndexLoaded (int _absIndexLoaded) const;
	
	/**
	 * returns the number of loaded pictures for the specified wavelength
	 * @param _waveLength the desired wavelength; if == -1, the number of all loaded pictures is returned
	 * @return the number of loaded pictures for the given wavelength
	 */
	int getLoadedPictures (int _waveLength = -1) const;
	
	/**
	 * @return the number of timepoints that have loaded pictures; works across all wavelengths
	 */
	int getMaxCountLoaded() const;
	
	/**
	 * returns whether the picture is loaded
	 * note: has nothing to do with "selection"
	 * @param _timepoint the query timepoint
	 * @param _zIndex the query zIndex; -1 means any
	 * @param _waveLength the query wavelength; -1 means any, note that either both _zIndex and _wavelength must be -1 or none of them!
	 * @return true, if the specified picture is in memory, otherwise false 
	 */
	bool isLoaded (int _timepoint, int _waveLength = -1, int _zIndex = -1) const;
	
	/**
	 * returns the number of loaded pictures at _timepoint
	 * @param _timepoint the query timepoint
	 * @return the number of pictures of different wavelengths that are loaded at the given timepoint
	 */
	int countLoaded (int _timepoint) const;
	
	/**
	 * returns the ordinary number of _timepoint within all loaded timepoints (regardless of the wavelength!)
	 * inversion of getTimePoint()
	 * @param _timepoint the query timepoint
	 * @return the number which gives the index of _timepoint within the range of loaded timepoints
	 */
	int getTimePointIndex (int _timepoint) const;
	
	/**
	 * returns the timepoint of _timepointIndex within all loaded timepoints (regardless of the wavelength!)
	 * inversion of getTimePointIndex()
	 * @param _timepointIndex the ordinary number of the query timepoint
	 * @return the _timepointIndex-th timepoint within all loaded timepoints
	 */
	int getTimePoint (int _timepointIndex) const;
	
	/**
	 * returns the red component of the pixel at position _x/_y in the picture at _timePoint & _waveLength
	 * @param _timePoint the query timepoint
	 * @param _zIndex the query z-index
	 * @param _waveLength the query wavelength
	 * @param _x the x position of the pixel
	 * @param _y the y position of the pixel
	 * @return if the picture does not exist, -1 is returned, otherwise a value between 0 and 255
	 */
	int readPixel (int _timePoint, int _zIndex, int _waveLength, int _x, int _y) const;
	
	/**
	 * @return the number of currently selected pictures
	 */
	int countSelectedPictures() const;
	
	/**
	 * returns the PictureContainer object for the desired timepoint and wavelength
	 * @param _timepoint the timepoint
	 * @param _zIndex (default: tries 1)
	 * @param _wavelength the wavelength (default: tries 0)
	 * @return a pointer to a PictureContainer object
	 */
	const PictureContainer* getPictureContainer (int _timepoint, int _zIndex = -1, int _wavelength = -1) const;
	
	/**
	 * unloads all currently loaded pictures
	 //* @param _loadSynchronous whether the pictures should be loaded by a timer and so keep the program handling to the user
	 //*                         (otherwise loading would be faster, but the user cannot do anything meanwhile)
	 --> Unloading must be asynchronous now
	 */
	void unloadAllPictures (/*bool _loadSynchronous = true*/);

	/**
	 * Get sorted list of truncated filenames
	 * @return const reference to sorted stringlist
	 */
	const QStringList& getTruncatedFileList() const {
		return fileNames;
	}
	
	/**
	 * Loads all pictures between the provided timepoints (Blocking Call!), stepping in interval steps.
	 * It can be specified if other pictures should be unloaded before
	 * @param _tp1 the start tp
	 * @param _tp2 the end tp
	 * @param _wl wavelength, set to -1 to load all wls
	 * @param _z z index
	 * @param _picture_interval the interval (default is 1, each picture)
	 * @param _unloadOthers whether other currently loaded pictures should be unloaded (default is false)
	 * @return success
	 */
	bool loadInterval (int _tp1, int _tp2, int _wl, int _z = 1, int _picture_interval = 1, bool _unloadOthers = false);
	
        /* *
	 * Sets whether the picture display should be smoothly scaled (slow) or normal (fast, default, usually good enough)
	 * @param _smoothScale true: slow, but more smooth scaling; false: fast scaling
	 */
        //void setSmoothScale (bool _smoothScale = true);

	///// setData() - Reimplemented from QAbstractItemModel
	//bool setData ( const QModelIndex& index, const QVariant& value, int role = Qt::EditRole );

	///// data() - Reimplemented from QAbstractItemModel
	//QVariant data ( const QModelIndex& index, int role = Qt::DisplayRole ) const;

	///// flags() - Reimplemented from QAbstractItemModel
	//Qt::ItemFlags flags ( const QModelIndex& index ) const;

	///// rowCount() - Reimplemented from QAbstractItemModel
	//int rowCount(const QModelIndex& _index) const {
	//	if(_index.isValid())
	//		return 0;
	//	return pictures.size();
	//}
	
	/**
	 * Get gain image.
	 * @param wl wavelength of gain image
	 * @param loadIfNecessary if image should be loaded if necessary
	 * @return pointer to gain image (must not be deleted by caller!) or 0
	 */
	const unsigned short* getGainImage(int wl, bool loadIfNecessary);

public slots:

	/**
	 * stops the loading timer
	 */
	void stopLoading();
	
signals:

	/**
	 * emitted when the (un)loading process for all pictures is completed
	 */
	void LoadingComplete();
	
	/**
	 * emitted when the (un)loading process for one picture is completed
	 * Note: the following parameters are not always set correctly.
	 * @param _timepoint the timepoint of the just (un)loaded picture 
	 * @param _wavelength the wavelength of the just (un)loaded picture
	 * @param _loaded whether the picture was loaded or unloaded
	 */
	void LoadedPicture (unsigned int _timepoint, int _wavelength, bool _loaded);
	
	/**
	 * emitted when an error occurred when attempting to (un)load a picture
	 * @param _timepoint the timepoint of the picture that was attempted to (un)load
	 * @param _wavelength the wavelength of the picture that was attempted to (un)load
	 */
	void ErrorLoading (unsigned int _timepoint, int _wavelength);
	
	/**
	 * emitted when the RAM is full with pictures or whatever
	 */
	void RAMFull();// (unsigned int _loadedPictures);
	
//private slots:
//
//	/**
//	 * this slot is called by LoadTimer and loads one picture (the next available)
//	 */
//	void loadTimerEvent();
	
private:
	
	///the position manager that is responsible for this movie window
	///is set in the constructor via TTTMainWindow
	TTTPositionManager *positionManager;
	
	/////this array contains pointers to PictureContainer objects
	/////the index is of type integer and represents the relative timepoint according to filename
	/////NOTE: it should always be initialized with the full possible size (not only with the loaded pictures)
	/////		because resizing is very costly!
	//// 01.03.2011-OH: changed pictures from Q3IntDict to QVector (we cannot use QHash as we have to be able to QUICKLY iterate over elements in SORTED order)
	////Q3IntDict<PictureContainer> Pictures;
	////QVector<PictureContainer*> pictures;

	// List of PictureContainers, sorted by TimePoint and WaveLength. Contains only valid entries (i.e. for which a file exists on the disk)
	QList<QSharedPointer<PictureContainer> > pictures;

	// Hash structure to map absolute (virtual) indices (see calcAbsIndex()) to pictures indices (="array indices"). Contains only entries
	// for which a PictureContainer (i.e. a valid pictures array index) exists! Important: need to be able to quickly find the next loaded
	// image for a given tp,z,wl even if no image exists there.
	QHash<qint64, int> indexHash;
	//std::unordered_map<PictureIndex, int> indexHash;

	// List of images to be loaded or unloaded, values are indexes of pictures array
	QList<int> imagesToBeLoadedOrUnloaded;

	// Sorted list of filenames but without base name (as files are displayed in TTTPositionLayout list)
	QStringList fileNames;
	
	
	///the number of loaded pictures
	unsigned int picturesLoaded;
	
	///contains the number of loaded pictures for each wavelength
	int countLoadedPictures [MAX_WAVE_LENGTH + 1];	
	
	///array: contains for each timepoint how many pictures there are loaded
	QByteArray loadedAtTimePoint;
	
	///contain the array index (not timepoint!) of the first/last loaded picture, or -1 if no image is loaded
	int firstLoadedPicture;
	int lastLoadedPicture;
		
	///the number of pictures set to be loaded (end - start; can differ)
	int LoadingCount;
	
	///contains the number of currently selected pictures
	int SelectionCounter;

	///picture loading threads
	QVector<ImageLoaderThread*> loadingThreads;

	///for thread-safe functions
	mutable QMutex loadPictureAbsMutex;
	mutable QMutex loadedAtTimePointMutex;
	QMutex getPictureForLoadingMutex;

	// Gain images for different wavelengths, only loaded on demand
	QHash<int, unsigned short*> m_gainImages;
	
	
//private methods

	/**
	 * Load pictures
	 */
	void initializePictures();
	
	/**
	 * if a picture was deleted from memory, this function resets the indices FirstLoadedPicture or LastLoadedPicture (depending on _first)
	 * @param _first whether the first timepoint or the last timepoint should be recalculated
	 */
	void updateBoundaries (bool _first);
	
	/**
	 * this function returns the (virtual) index of the picture 
	 * NOTE: this is necessary for EVERY access!
	 * @param _timepoint the timepoint of the picture
	 * @param _zIndex the zIndex of the picture
	 * @param _wavelength the wavelength of the picture
	 * @return the index within the internal picture array <code>Pictures</code>
	 */
	qint64 calcAbsIndex (qint64 _timepoint, qint64 _zIndex, qint64 _wavelength) const {
		if (_wavelength == -1) 
			_wavelength = 0;

		return (_timepoint * (MAX_ZINDEX+1) * (MAX_WAVE_LENGTH+1) + _zIndex * (MAX_WAVE_LENGTH+1) + _wavelength);
	}
	
	/**
	 * returns the timepoint of the picture with array index _absIndex
	 * @param _absIndex the index within the internal picture array <code>Pictures</code>
	 * @return the timepoint of the specified picture
	 */
	int calcTimePoint (qint64 _absIndex) const {
		//return (_absIndex / (MAX_WAVE_LENGTH + 1) + positionManager->getFirstTimePoint());
		return _absIndex / ((MAX_WAVE_LENGTH + 1) * (MAX_ZINDEX + 1));
	}
	
	/**
	 * returns the wavelength of the picture with array index _absIndex
	 * @param _absIndex the index within the internal picture array <code>Pictures</code>
	 * @return the wavelength of the specified picture
	 */
	int calcWavelength (qint64 _absIndex) const {
		return (_absIndex % (MAX_WAVE_LENGTH + 1));
	}

	/**
	 * Calc z-index.
	 * @param _absIndex the index within the internal picture array <code>Pictures</code>
	 * @return the z-index of the specified picture
	 */
	int calcZIndex (qint64 _absIndex) const {
		return (_absIndex / (MAX_WAVE_LENGTH+1)) % (MAX_ZINDEX+1);
	}
	
	/**
	 * @param _absIndex the index within the internal picture array <code>Pictures</code>
	 * @return a PictureIndex object constructed from the absolute index _index
	 */
	PictureIndex calcPI (qint64 _absIndex) const {
		return PictureIndex (calcTimePoint (_absIndex), calcZIndex(_absIndex), calcWavelength (_absIndex));
	}
	
	/**
	 * returns the index of the next/previous picture after _arrayIndex that is in memory
	 * @param _arrayIndex the current internal picture index, the base for the next picture
	 * @param _forward whether the next picture should be returned, otherwise the previous
	 * @param _zIndex if != -1, only indices to pictures with _zIndex will be returned
	 * @param _waveLength if != -1, only indices to pictures with _wavelength will be returned
	 * @return (-1), if there is no next picture, otherwise the internal index of the next loaded picture
	 */
	qint64 nextLoadedPictureArrayIndex(qint64 _arrayIndex, bool _forward = true, int _zIndex = -1, int _waveLength = -1) const;
	
	///**
	// * loads the picture with the internal array index _index
	// * @param _index the index within the internal picture array <code>Pictures</code>
	// * @return 0 if loading/deleting fails, 1 if picture was loaded, 2 if picture was deleted
	// */
	//int loadPictureAbs (unsigned int _index);

	/**
	 * Thread-safe version of the above function.
	 * The function is only thread-safe, if called with different parameters by each thread.
	 * @param _index the index within the internal picture array <code>Pictures</code>
	 * @param _load if image should be loaded or unloaded
	 * @return 0 if loading/deleting fails, 1 if picture was loaded, 2 if picture was deleted
	 */
	int loadPictureAbs_ThreadSafe (int _index, bool _load);

	/**
	 * Get index of next picture to load or unload and remove it from imagesToBeLoadedOrUnloaded.
	 * @return -1 if no image is left.
	 */
	int popImageForLoadingOrUnloading();

// friend classes
	friend class ImageLoaderThread;
};

/**
@author Oliver Hilsenbeck
	
	Thread class to support loading of pictures without interrupting the gui-thread (thus freezing the gui)
	and to load pictures faster on dual/quad-cores by using multi-threading (currently max. 2 threads supported).

	This class should be a private nested class of PictureArray, but unfortunately QT does not support signals for
	nested classes.
*/
class ImageLoaderThread : public QThread {
	Q_OBJECT

public:
	/**
	 * Constructor
	 * @param _parent the PictureArray to work with
	 */
	ImageLoaderThread(PictureArray* _parent);

	/**
	 * Set this to be the main loading thread. Waits for all others when finished and emits LoadingComplete() signal then.
	 */
	void setMainThread(const QVector<ImageLoaderThread*> _childThreads);

	/**
	 * Set params for the next loading. Must not be called while this thread is loading pictures.
	 */
	void setNextRunParams(bool _unload, bool _loadSynchronous);

	/**
	 * Cancel loading. Can be called by other threads to stop this thread safely
	 */
	void cancelLoading();

signals:
	// These signals will be connected to the corresponding signals in parrentPicArray (Note: _loaded is currently totally meaningless)
	void LoadedPicture (unsigned int _timepoint, int _wavelength, bool _loaded);
	void LoadingComplete();
	void RAMFull();

protected:
	/**
	 * Entry point for this thread. Reimplemented from QThread
	 */
	void run();

private:


	// If we are actually unloading pictures
	bool unload;

	// If loading is asynchronous -> means that gui thread waits for us -> means we should not emit too many signals
	bool loadSynchronous;

	// If loading should be cancelled immediately
	bool abortLoading;

	// The picture array
	PictureArray* parentPicArray;

	// If loading is currently in progress
	bool loadingInProgess;

	// If this is the main loading thread and child threads
	bool mainLoadingThread;
	QVector<ImageLoaderThread*> childThreads;
};


//inliners

inline int PictureArray::countSelectedPictures() const
{
	return SelectionCounter;
}
	
//inline unsigned int PictureArray::size() const
//{
//	return pictures.size();
//}


#endif
