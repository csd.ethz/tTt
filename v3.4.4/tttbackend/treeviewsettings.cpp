/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "treeviewsettings.h"


// Singleton definition
TreeViewSettings* TreeViewSettings::inst = 0;


TreeViewSettings::TreeViewSettings()
{
	// ToDO
}

TreeViewSettings* TreeViewSettings::getInstance()
{
	// This is the only place where a TreeViewSettings instance is created
	if(!inst)
		inst = new TreeViewSettings();
	return inst;
}

int TreeViewSettings::getMainTrackLinesDistance() const
{
	// ToDo:
	return 500;
}

int TreeViewSettings::getTreeLineWidth() const
{
	// ToDo:
	return 15;
}

int TreeViewSettings::getTrackLinesDistance() const
{
	// ToDo:
	return 1;
}

int TreeViewSettings::getTimeLineFontSize() const
{
	return 70;
}

int TreeViewSettings::getTimeLineLineWidth() const
{
	return 10;
}

QColor TreeViewSettings::getTimeLineFontColor() const
{
	return Qt::black;
}

QColor TreeViewSettings::getTimeLineLineColor() const
{
	return QColor(0xE0, 0xE0, 0xE0);
}

int TreeViewSettings::getCellCircleRadius() const
{
	return 35;
}

int TreeViewSettings::getCellCircleOutlineWidth() const
{
	return 15;
}

QColor TreeViewSettings::getCellCircleDefaultOuterColor() const
{
	return Qt::black;
}

QColor TreeViewSettings::getCellCircleDefaultInnerColor() const
{
	return Qt::white;
}

QColor TreeViewSettings::getCellCircleSelectedOuterColor() const
{
	return Qt::blue;
}

QColor TreeViewSettings::getCellCircleSelectedInnerColor() const
{
	return Qt::blue;
}

QColor TreeViewSettings::getCellCircleMouseHoverOuterColor() const
{
	return Qt::blue;
}


bool TreeViewSettings::getTrackLinesContinuous() const
{
	return false;
}

int TreeViewSettings::getTrackNumFontSize() const
{
	return 80;
}

QColor TreeViewSettings::getTrackNumColor() const
{
	return Qt::black;
}

int TreeViewSettings::getTreeNameFontSize() const
{
	return 70;
}

QColor TreeViewSettings::getTreeNameColor() const
{
	return Qt::black;
}

QColor TreeViewSettings::getTimeLineBackGroundColor() const
{
	return Qt::white;
}
