/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "displaysettings.h"

#include "tttmanager.h"


DisplaySettings::DisplaySettings()
{
	// 01.03.2011-OH: changed settings from Q3 IntDict<BDisplay> to QVector<BDisplay> -> no auto delete needed
	/*
	//make the QIntDict delete() its contents when DisplaySettings is destructed
	settings.setAutoDelete (true);
	Settings.setAutoDelete (false);
	*/

	//one additional for overlay frame
	settings.resize (MAX_WAVE_LENGTH + 2);
	
	// 01.03.2011-OH: changed settings from Q3 IntDict<BDisplay> to QVector<BDisplay> -> all elements have been
	// initialized at this point with default constructor already
	/*
	for (int i = 0; i < (int)settings.size(); i++)
		settings.insert (i, new BDisplay ());
	*/

	// 01.03.2011-OH: changed dummy from BDisplay* to BDisplay
	/*
	//init dummy
	dummy = new BDisplay();
	*/
}

DisplaySettings::~DisplaySettings()
{
	//Settings has AutoDelete set to true!
}

void DisplaySettings::setPictureSize (int _waveLength, int _width, int _height)
{
	if (_waveLength > -1)
		at (_waveLength).setCompleteSize (_width, _height);
	else 
		//all available wavelengths have to be set
		for (int i = 0; i < size(); i++)
			at (i).setCompleteSize (_width, _height);
}

void DisplaySettings::setLoadingRegion (int _waveLength, int _offsetX, int _offsetY, int _width, int _height)
{
//@todo
//	if (_waveLength > -1)
//		at (_waveLength).setLoadedRegion (_offsetX, _offsetY, _width, _height);
//	else
//		//all available wavelengths have to be set
//		for (int i = 0; i < size(); i++) {
//			if (! at (i).DisplayedRegionSet())
//				at (i).setDisplayedRegion (_offsetX, _offsetY, _width, _height);
//
//			at (i).setLoadedRegion (_offsetX, _offsetY, _width, _height);
//		}
}

