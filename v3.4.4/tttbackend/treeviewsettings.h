/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef treeviewsettings_h__
#define treeviewsettings_h__

// Qt
#include <QColor>

/**
 * @author Oliver Hilsenbeck
 *
 * Manages settings for TreeView classes
 */
class TreeViewSettings {

public:

	/**
	 * Get instance
	 */
	static TreeViewSettings* getInstance();

	/**
	 * @return width of tree lines 
	 */
	int getTreeLineWidth() const;

	/**
	 * @return distance between two tree lines (i.e. between lines for different wavelengths and main track line)
	 */
	int getTrackLinesDistance() const;

	/**
	 * @return distance between two adjacent tree lines (only main tree lines, not wavelength lines)
	 */
	int getMainTrackLinesDistance() const;

	/**
	 * @return TreeViewTimeLine font size (point size)
	 */
	int getTimeLineFontSize() const;

	/**
	 * @return TreeViewTimeLine font color
	 */
	QColor getTimeLineFontColor() const;

	/**
	 * @return line width of vertical TreeViewTimeLine lines
	 */
	int getTimeLineLineWidth() const;

	/**
	 * @return background color of timeline
	 */
	QColor getTimeLineBackGroundColor() const;

	/**
	 * @return color of vertical TreeViewTimeLine lines
	 */
	QColor getTimeLineLineColor() const;

	/**
	 * @return radius of TreeViewCellCircle in scene units
	 */
	int getCellCircleRadius() const;

	/**
	 * @return width of pen for TreeViewCellCircle outline
	 */
	int getCellCircleOutlineWidth() const;

	/**
	 * @return color of outline of TreeViewCellCircle 
	 */
	QColor getCellCircleDefaultOuterColor() const;

	/**
	 * @return color of inner area of TreeViewCellCircle 
	 */
	QColor getCellCircleDefaultInnerColor() const;

	/**
	 * @return color of outline of a selected TreeViewCellCircle 
	 */
	QColor getCellCircleSelectedOuterColor() const;

	/**
	 * @return color of inner area of a selected TreeViewCellCircle 
	 */
	QColor getCellCircleSelectedInnerColor() const;

	/**
	 * @return color of inner area of a TreeViewCellCircle when mouse is moving over it
	 */
	QColor getCellCircleMouseHoverOuterColor() const;

	/**
	 * @return if track lines should always be drawn as continuous lines, even if they contain gaps
	 */
	bool getTrackLinesContinuous() const;

	/**
	 * @return track number font size (point size)
	 */
	int getTrackNumFontSize() const;

	/**
	 * @return track number color
	 */
	QColor getTrackNumColor() const;

	/**
	 * @return tree name font size
	 */
	int getTreeNameFontSize() const;

	/**
	 * @return tree name color
	 */
	QColor getTreeNameColor() const;

private:

	/**
	 * Constructor, loads default settings
	 */
	TreeViewSettings();

	// Singleton instance
	static TreeViewSettings* inst;
};


#endif // treeviewsettings_h__
