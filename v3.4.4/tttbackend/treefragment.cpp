/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "treefragment.h"

// Qt
#include <QFile>
#include <QDataStream>
#include <QScopedPointer>

// Project
#include "tttexception.h"
#include "tttbackend/tools.h"
#include "tttbackend/tttexception.h"

// STL
#include <algorithm>
#include <cassert>


TreeFragment::TreeFragment( const QString& _fileNameWithPath, QHash<int, QList<QSharedPointer<TreeFragmentTrackPoint> > >& _trackpointsHash, bool _doNotSaveInHash)
{
	positionNumber = -1;

	// Open file
	QFile f(_fileNameWithPath);
	if(!f.open(QIODevice::ReadOnly))
		throw TTTException(QString("Cannot open file '%1' for reading: %2").arg(_fileNameWithPath).arg(f.errorString()));

	// Set name
	fileName = _fileNameWithPath.mid(_fileNameWithPath.lastIndexOf('/') + 1);

	// Set path
	path = _fileNameWithPath.left(_fileNameWithPath.lastIndexOf('/') + 1);

	// Set number
	// ToDo: This is a bit ugly
	treeFragmentNumber = fileName.mid(fileName.indexOf("_n") + 2, 5).toInt();

	// Open file depending on file extension
	if(_fileNameWithPath.right(3) == "csv") {
		// Load csv file 
		firstTimePoint = -1;
		maxTrackNumber = -1;
		numberOfTrackPoints = 0;

		// Parse header
		std::vector<int> additionalColumnIndexes;
		std::vector<int> indexMap(6, -1);	// Column indexes of position, timepoint, x, y, track number, confidence
		QTextStream in(&f);
		QString headerLine = in.readLine();
		QStringList header = headerLine.split(';');
		//qDebug() << header;
		for(int iCol = 0; iCol < header.size(); ++iCol) {
			header[iCol] = header[iCol].trimmed();
			QString headerLowerCase = header[iCol].toLower();
			if(headerLowerCase == "position")
				indexMap[0] = iCol;
			else if(headerLowerCase == "framenumber")
				indexMap[1] = iCol;
			else if(headerLowerCase == "centroidxglobal")
				indexMap[2] = iCol;
			else if(headerLowerCase == "centroidyglobal")
				indexMap[3] = iCol;
			else if(headerLowerCase == "tracknumber")
				indexMap[4] = iCol;
			else if(headerLowerCase == "confidence")
				indexMap[5] = iCol;
			else {
				additionalAttributes.push_back(header[iCol]);
				additionalColumnIndexes.push_back(iCol);
			}
		}
		//qDebug() << additionalAttributes;
		for(int i = 0; i < indexMap.size(); ++i) {
			
			// Require columns position, framenumber, centroidxglobal, centroidyglobal, tracknumber
			if(indexMap[i] < 0 && i != 5)		
				throw TTTException(QString("Error while reading file %1: columns missing in header.").arg(_fileNameWithPath));
		}

		// Parse remaining file
		while(!in.atEnd()) {
			QString line = in.readLine().trimmed();
			if(line.isEmpty())
				continue;
			QStringList columns = line.split(';');
			int position, timePoint, trackNumber;
			double x,y, confidence;
			for(int i = 0; i < indexMap.size(); ++i) {
				if(indexMap[i] >= columns.size())
					throw TTTException(QString("Error while reading file %1: columns missing in line: %2").arg(_fileNameWithPath).arg(line));
			}
			position = columns[indexMap[0]].toInt();
			if(positionNumber < 0)
				positionNumber = position;
			timePoint = columns[indexMap[1]].toInt();
			x = columns[indexMap[2]].toDouble();
			y = columns[indexMap[3]].toDouble();
			trackNumber = columns[indexMap[4]].toInt();
			if(indexMap[5] >= 0)
				confidence = columns[indexMap[5]].toDouble();
			else
				confidence = -1.0;

			// Get track
			QSharedPointer<TreeFragmentTrack>& track = tracks[trackNumber];
			if(track.isNull()) {
				// Create new track
				track = QSharedPointer<TreeFragmentTrack>(new TreeFragmentTrack(trackNumber, timePoint, timePoint, QHash<int, QSharedPointer<TreeFragmentTrackPoint> >(), treeFragmentNumber, this));
				
				// Check if parent exists
				if(trackNumber > 1) {
					int parentNumber = trackNumber / 2;
					QSharedPointer<TreeFragmentTrack>& trackParent = tracks[parentNumber];
					if(trackParent) {
						if(trackNumber % 2 == 0)
							trackParent->child1 = track.data();
						else
							trackParent->child2 = track.data();
					}
				}
				
				// Check if child exists
				int child1Index = trackNumber * 2,
					child2Index = trackNumber * 2 + 1;
				QSharedPointer<TreeFragmentTrack> trackChild1 = tracks.value(child1Index);
				if(trackChild1)
					track->child1 = trackChild1.data();
				QSharedPointer<TreeFragmentTrack> trackChild2 = tracks.value(child2Index);
				if(trackChild2)
					track->child2 = trackChild2.data();

				// Check if root
				if(trackNumber == 1)
					root = track;

				// Update maxTrackNumber
				if(maxTrackNumber == -1 || trackNumber > maxTrackNumber)
					maxTrackNumber = trackNumber;
			}
			// Insert trackpoint
			QSharedPointer<TreeFragmentTrackPoint>& trackPoint = track->trackPoints[timePoint];
			if(!trackPoint.isNull())
				throw TTTException(QString("File %1 has trackpoint %2 in track %3 more than once.").arg(_fileNameWithPath).arg(timePoint).arg(trackNumber));
			trackPoint = QSharedPointer<TreeFragmentTrackPoint>(new TreeFragmentTrackPoint(timePoint, position, x, y, confidence));
			++numberOfTrackPoints;
			track->startTp = std::min(track->startTp, timePoint);
			track->stopTp = std::max(track->stopTp, timePoint);
			trackPoint->setTreeFragmentTrack(track.data());

			// Get additional data
			trackPoint->additionalValues.resize(additionalColumnIndexes.size());
			for(int iAdditionalColumn = 0; iAdditionalColumn < additionalColumnIndexes.size(); ++iAdditionalColumn) {
				int additionalCol = additionalColumnIndexes[iAdditionalColumn];
				double d = std::numeric_limits<double>::quiet_NaN();
				if(additionalCol < columns.size()) {
					bool ok = false;
					d = columns[additionalCol].toDouble(&ok);
					if(!ok)
						d = std::numeric_limits<double>::quiet_NaN();
				}
				trackPoint->additionalValues[iAdditionalColumn] = d;
			}

			// Save it in hash
			if(!_doNotSaveInHash) {
				QList<QSharedPointer<TreeFragmentTrackPoint> >& hashList = _trackpointsHash[timePoint];
				hashList.append(trackPoint);
			}

			// Update firstTimePoint
			if(firstTimePoint == -1 || timePoint < firstTimePoint)
				firstTimePoint = timePoint;
		}

	}
	else {
		// Load trf file
		// Open stream
		QDataStream in(&f);
		in.setByteOrder(QDataStream::LittleEndian);
		in.setFloatingPointPrecision(QDataStream::SinglePrecision);

		// Check signature
		quint32 sig;
		in >> sig;
		if(sig != 0x72667274)
			throw TTTException(QString("File %1 is not a valid .trf-file (signature mismatch).").arg(_fileNameWithPath));

		// Check file version
		quint32 version;
		in >> version;
		if(version > TRF_FILE_VERSION)
			throw TTTException(QString("File %1 has newer file version (%2) than supported (%3).").arg(_fileNameWithPath).arg(version).arg(TRF_FILE_VERSION));

		// Number of tracks
		quint32 numTracks;
		in >> numTracks;

		positionNumber = -1;
		firstTimePoint = -1;

		// Read tracks
		int i = 0;
		//unsigned int lastTrackNumber = 0;
		numberOfTrackPoints = 0;
		maxTrackNumber = 1;
		while(i++ < numTracks && !in.atEnd() && in.status() == QDataStream::Ok) {
			// Track number
			quint64 tmp;
			in >> tmp;

			// We assume int to be sufficient
			unsigned int trackNumber = tmp;

			// Check if this track already exists
			if(tracks.contains(trackNumber)) 
				throw TTTException(QString("File %1 has track %2 more than once.").arg(_fileNameWithPath).arg(trackNumber));

			// Check for maxTrackNumber
			if(trackNumber > maxTrackNumber)
				maxTrackNumber = trackNumber;

			//// Check order
			//if(trackNumber <= lastTrackNumber)
			//	throw TTTException(QString("Track %1 of file %2 invalid or out of order.").arg(trackNumber).arg(_fileName));
			//lastTrackNumber = trackNumber;

			// Start and stop timepoints
			quint32 startTp, stopTp;
			in >> startTp >> stopTp;

			// Number of trackpoints
			quint32 numTrackPoints;
			in >> numTrackPoints;

			// Read trackpoints
			unsigned int lastTimePoint = 0;
			QHash<int, QSharedPointer<TreeFragmentTrackPoint> > trackPoints;
			for(int j = 0; j < numTrackPoints && !in.atEnd() && in.status() == QDataStream::Ok; ++j) {
				// Timepoint
				quint32 timePoint;
				in >> timePoint;
			
				// Trackpoints must be sorted by timepoints
				if(timePoint <= lastTimePoint)
					throw TTTException(QString("Trackpoints of track %1 of file %2 are not sorted or are invalid (1).").arg(trackNumber).arg(_fileNameWithPath));
				lastTimePoint = timePoint;

				// Store first timepoint
				if(firstTimePoint == -1)
					firstTimePoint = timePoint;
				// No timepoint can be smaller than first timepoint
				else if(timePoint < firstTimePoint)
					throw TTTException(QString("Trackpoints of track %1 of file %2 are not sorted or are invalid (2).").arg(trackNumber).arg(_fileNameWithPath));

				// Position number
				quint16 posNumber;
				in >> posNumber;

				// If this is first trackpoint of first track, set position number of treefragment
				if(trackNumber == 1 && j == 0)
					positionNumber = posNumber;

				// Coordinates
				float x, y;
				in >> x >> y;

				// Confidence
				float confidence;
				in >> confidence;

				// Create trackpoint
				TreeFragmentTrackPoint *newTrackPointTmp = new TreeFragmentTrackPoint(timePoint, posNumber, x, y, confidence);
				QSharedPointer<TreeFragmentTrackPoint> newTrackPoint(newTrackPointTmp);
				trackPoints.insert(timePoint, newTrackPoint);

				// Save it in hash
				if(!_doNotSaveInHash){
					if(!_trackpointsHash.contains(timePoint)) 
						_trackpointsHash.insert(timePoint, QList<QSharedPointer<TreeFragmentTrackPoint> >());

					_trackpointsHash[timePoint].append(newTrackPoint);
				}

				// Increase num of trackpoints
				++numberOfTrackPoints;
			}

			// Save memory
			trackPoints.squeeze();

			// Create Track
			TreeFragmentTrack *newTrackTmp = new TreeFragmentTrack(trackNumber, startTp, stopTp, std::move(trackPoints), treeFragmentNumber, this);
			QSharedPointer<TreeFragmentTrack> newTrack(newTrackTmp);
			tracks.insert(trackNumber, newTrack);

			// Check if parent node exists
			int parentNumber = trackNumber / 2;
			if(tracks.contains(parentNumber)) {
				if(trackNumber % 2 == 0)
					tracks[parentNumber]->setChild1(newTrackTmp);
				else
					tracks[parentNumber]->setChild2(newTrackTmp);
			}

			// Not looking for children, as tracks must be sorted by tracknumbers
			//// Check if child1 track exists
			//int child1Number = trackNumber * 2;
			//if(tracks.contains(child1Number))
			//	newTrackTmp->setChild1(tracks[child1Number].data());

			//// Check if child2 track exists
			//int child2Number = trackNumber * 2 + 1;
			//if(tracks.contains(child2Number))
			//	newTrackTmp->setChild2(tracks[child2Number].data());

			// Check if this is the root track
			if(trackNumber == 1)
				root = newTrack;
		}

		// Check for error
		if(in.status() != QDataStream::Ok || i != numTracks + 1)
			throw TTTException(QString("Error while reading .trf-file %1.").arg(_fileNameWithPath));
	}

	// Read if this fragment belongs to a tree already
	QString logFile = path + getLogFileName();

	// Read it if it exists
	if(QFile::exists(logFile)) {
		// If file exists but cannot be opened, throw an exception
		QFile f(logFile);
		if(!f.open(QIODevice::ReadOnly)) 
			throw TTTException(QString("Cannot open file '%1' for reading: %2").arg(_fileNameWithPath).arg(f.errorString()));

		// Just read string
		associatedTree = f.readAll();

		if(associatedTree.isEmpty()) {
			// Issue a warning
			qWarning() << "tTt Warning (TreeFragment::TreeFragment()): Found log file but log file appears to be empty: " << logFile;
		}
	}
}

TreeFragment::TreeFragment(const QString& _fileNameWithPath, int _treeFragmentNumber, int _positionNumber)
	: fileName(_fileNameWithPath),
	treeFragmentNumber(_treeFragmentNumber),
	positionNumber(_positionNumber),
	maxTrackNumber(-1),
	numberOfTrackPoints(0),
	firstTimePoint(-1)
{
	// Set filename
	fileName = _fileNameWithPath.mid(_fileNameWithPath.lastIndexOf('/') + 1);

	// Set path
	path = _fileNameWithPath.left(QDir::fromNativeSeparators(_fileNameWithPath).lastIndexOf('/') + 1);

	// Read if this fragment belongs to a tree already
	QString logFile = path + getLogFileName();

	// Read it if it exists
	if(QFile::exists(logFile)) {
		// If file exists but cannot be opened, throw an exception
		QFile f(logFile);
		if(!f.open(QIODevice::ReadOnly)) 
			throw TTTException(QString("Cannot open file '%1' for reading: %2").arg(_fileNameWithPath).arg(f.errorString()));

		// Just read string
		associatedTree = f.readAll();

		if(associatedTree.isEmpty()) {
			// Issue a warning
			qWarning() << "tTt Warning (TreeFragment::TreeFragment()): Found log file but log file appears to be empty: " << logFile;
		}
	}
}

int TreeFragment::getPositionNumber() const
{
	return positionNumber;
}

//QString TreeFragment::getName() const
//{
//	return fileName;
//}

TreeFragmentTrack* TreeFragment::getTrackByNumber( int _number ) const
{
	// Check if track with _number exists
	if(tracks.contains(_number))
		return tracks[_number].data();

	return 0;
}

QSharedPointer<Tree> TreeFragment::convertToTree(int _maxTimePoint) const
{
	// Create tree
	Tree* tmp = new Tree();
	QSharedPointer<Tree> retTree(tmp);

	// Convert and insert tracks
	retTree->reset();
	convertToTreeRec(retTree.data(), 0, root.data(), _maxTimePoint);

	// New tree, mark as modified
	retTree->setModified(true);

	return retTree;
}

void TreeFragment::convertToTreeRec( Tree* _tree, Track* _parentTrack, TreeFragmentTrack *_track, int _maxTimePoint ) const
{
	// Create track
	Track* newTrack = new Track(_parentTrack, _track->getTrackNumber() % 2 + 1, _maxTimePoint, _track->getFirstTimePoint());
	newTrack->setNumber(_track->getTrackNumber());

	// Add trackpoints
	const QHash<int, QSharedPointer<TreeFragmentTrackPoint> >& tps = _track->getTrackPoints();
	for(QHash<int, QSharedPointer<TreeFragmentTrackPoint> >::const_iterator it = tps.constBegin(); it != tps.constEnd(); ++it) {
		// Get current TreeFragmentTrackPoint
		TreeFragmentTrackPoint* curTp = it->data();

		// Get number of digits for positions
		int posDigits = Tools::getNumOfPositionDigits();
		if(posDigits == -1)
			posDigits = 4;

		// Convert position int to string
		QString pos = Tools::convertIntPositionToString(curTp->positionNumber);

		// Create TrackPoint
		TrackPoint* newTp = newTrack->addMark(curTp->timePoint, curTp->posX, curTp->posY, 0, false, 0, pos);
		if(!newTp)
			continue;

		newTp->confidence = curTp->confidence;
		newTp->CellDiameter = 25.0f;	// Default value by tTt..
	}

	// All track points must have different time points and track points must be sorted ascendingly by time point
#ifdef _DEBUG
	const auto& newTps = newTrack->getAllTrackpoints();
	int lastTimePoint = -1;
	for(int i = 0; i < newTps.size(); ++i) {
		if(newTps[i]) {
			assert(newTps[i]->getTimePoint() > lastTimePoint);
			lastTimePoint = newTps[i]->getTimePoint();
		}
	}
#endif

	// Add to tree
	_tree->insert(newTrack);

	// Add children
	if(TreeFragmentTrack* child = _track->getChild1())
		convertToTreeRec(_tree, newTrack, child, _maxTimePoint);
	if(TreeFragmentTrack* child = _track->getChild2())
		convertToTreeRec(_tree, newTrack, child, _maxTimePoint);
}

QString TreeFragment::getLogFileName() const
{
	// Extract index of file extension dot
	int i = fileName.lastIndexOf('.');
	if(i == -1)
		return "";

	// Extract part before that + log file extension
	return fileName.left(i) + ".log";
}

bool TreeFragment::writeAssociatedTreeToFile() const
{
	// Write if this fragment belongs to a tree already
	QString fileName = path + getLogFileName();
	QFile logFile(fileName);
	
	// Open
	if(!logFile.open(QIODevice::WriteOnly)) {
		qWarning() << "Could not open file '" << fileName << "' for writing: " << logFile.errorString();
		return false;
	}

	// Write data
	logFile.write(associatedTree.toLatin1());

	return true;
}

QList<ITrackPoint*> TreeFragment::getTrackPointsAtTimePoint( int _timePoint ) const
{
	QList<ITrackPoint*> ret;

	// Iterate over all tracks
	for(QHash<int, QSharedPointer<TreeFragmentTrack> >::const_iterator it = tracks.constBegin(); it != tracks.constEnd(); ++it) {
		TreeFragmentTrack* curTrack = it->data();

		// Try to get TrackPoint
		if(TreeFragmentTrackPoint* tp = curTrack->getTrackPointByTimePoint(_timePoint))
			ret.append(tp);
	}

	return ret;
}

void TreeFragment::setWeakPointer(QWeakPointer<TreeFragment> weakPointer)
{
	// Check if _sharedPointer actually refers to this object
	if(weakPointer.data() != this)
		return;

	// Iterate over all tracks
	for(QHash<int, QSharedPointer<TreeFragmentTrack> >::const_iterator it = tracks.constBegin(); it != tracks.constEnd(); ++it) {
		TreeFragmentTrack* curTrack = it->data();
		assert(curTrack);

		// Set shared pointer
		curTrack->treeFragmentWeakPointer = weakPointer;
	}
}

//void TreeFragment::clearSharedPointers() const
//{
//	// Iterate over all tracks
//	for(QHash<int, QSharedPointer<TreeFragmentTrack> >::const_iterator it = tracks.constBegin(); it != tracks.constEnd(); ++it) {
//		TreeFragmentTrack* curTrack = it->data();
//
//		// Unset shared pointer
//		curTrack->treeFragmentSharedPointer.clear();
//	}
//}

QVariant TreeFragment::getTreeProperty( QString _propName ) const
{
	// ToDo:
	return QVariant();
}

QVector<QString> TreeFragment::getAvailableProperties() const
{
	// ToDo:
	return QVector<QString>();
}

bool TreeFragment::prependFragment( TreeFragment* other, int trackNum, bool deleteOther )
{
	// Get track of other fragment and make sure it has no children and ends before this tree starts
	TreeFragmentTrack* trackToPrepend = other->getTrackByNumber(trackNum);
	if(!trackToPrepend || trackToPrepend->getChild1())
		return false;
	if(trackToPrepend->getLastTimePoint() >= firstTimePoint)
		return false;
	assert((trackToPrepend->getChild2() == nullptr));
	assert(root->getTrackNumber() == 1);
	assert(other->root->getTrackNumber() == 1);

	/**
	 * Move trackpoints of trackToPrepend into root.
	 */ 
	//qDebug() << "Appending " << other->treeFragmentNumber << " to " << treeFragmentNumber << " at " << firstTimePoint;
	auto trackpoints = trackToPrepend->getTrackPoints();
	for(auto it = trackpoints.begin(); it != trackpoints.end(); ++it) {
		assert(root->trackPoints.contains((*it)->getTimePoint()) == false);
		root->trackPoints.insert((*it)->getTimePoint(), *it);
		(*it)->track = root.data();
	}
	trackToPrepend->trackPoints.clear();
	
	/**
	 * Update track numbers in this tree fragment.
	 */
	int newMaxTrackNumber = maxTrackNumber;
	int newRootTrackNumber = trackNum;
	if(newRootTrackNumber != 1) {
		// Change track numbers and get maximum of new track numbers
		newMaxTrackNumber = std::max(root->setTrackNumber(newRootTrackNumber), newMaxTrackNumber);

		// Update keys in tracks
		QHash<int, QSharedPointer<TreeFragmentTrack>> tracksTmp;
		for(auto it = tracks.begin(); it != tracks.end(); ++it) {
			TreeFragmentTrack* curTrack = it->data();
			tracksTmp.insert(curTrack->getTrackNumber(), *it);
		}
		tracks = tracksTmp;
	}

	/**
	 * Move all tracks except trackToPrepend into this tree fragment.
	 */
	for(auto it = other->tracks.begin(); it != other->tracks.end(); ++it) {
		TreeFragmentTrack* curTrack = it->data();
		if(curTrack == trackToPrepend)
			continue;

		curTrack->treeFragmentWeakPointer = root->treeFragmentWeakPointer;
		curTrack->treeFragment = this;
		assert(!tracks.contains(it.key()));
		tracks.insert(it.key(), it.value());
	}
	
	
	/**
	 * Connect parent track of trackToPrepend with this root and update root (if necessary).
	 */
	TreeFragmentTrack* parentTrack = trackToPrepend->getMother();
	if(parentTrack) {
		// Root track number must have been changed then
		assert(newRootTrackNumber != 1);

		// Connect new parent
		if(parentTrack->child1 == trackToPrepend)
			parentTrack->child1 = root.data();
		else
			parentTrack->child2 = root.data();

		// Move root
		root = other->root;
		assert(root->getTrackNumber() == 1);
	}
	else {
		// Root track number must be still 1 then
		assert(newRootTrackNumber == 1);
	}

	/**
	 * Update all remaining attributes of this tree fragment.
	 */
	positionNumber = other->positionNumber;
	numberOfTrackPoints += other->numberOfTrackPoints;
	firstTimePoint = other->firstTimePoint;
	maxTrackNumber = std::max(newMaxTrackNumber, other->maxTrackNumber);
	
	/**
	 * Done.
	 */
	other->root.clear();
	other->tracks.clear();
	if(deleteOther)
		delete other;
	return true;
}

void TreeFragment::exportToFile( const QString& _fileName ) const
{
	// Open file
	QFile f(_fileName);
	if(!f.open(QIODevice::WriteOnly))
		throw TTTException(QString("Cannot open file for writing: %1").arg(_fileName));

	// Open stream
	QDataStream out(&f);
	out.setByteOrder(QDataStream::LittleEndian);
	out.setFloatingPointPrecision(QDataStream::SinglePrecision);

	// Signature
	out << (quint8)0x74 << (quint8)0x72 << (quint8)0x66 << (quint8)0x72;

	// File version
	out << (quint32)FILE_VERSION;

	// Number of tracks
	out << (quint32)getNumberOfTracks();

	// Recursively export tracks
	exportTrackToFile(out, root.data(), 1);
}

void TreeFragment::exportTrackToFile( QDataStream& _out, TreeFragmentTrack* _track, quint64 _trackNumber ) const
{
	// Tracknumber
	_out << _trackNumber;

	// First and last timepoint
	_out << (quint32)_track->getFirstTimePoint();
	_out << (quint32)_track->getLastTimePoint();

	// Number of trackpoints
	int numTPs = _track->getNumTrackPoints();
	_out << (quint32)numTPs;

	// Get trackpoints and sort by time point
	const QHash<int, QSharedPointer<TreeFragmentTrackPoint> >& tmpTps = _track->getTrackPoints();
	QList<QSharedPointer<TreeFragmentTrackPoint>> tps = tmpTps.values();
	std::sort(tps.begin(), tps.end(), [](QSharedPointer<TreeFragmentTrackPoint>& a, QSharedPointer<TreeFragmentTrackPoint>& b){return a->getTimePoint() < b->getTimePoint();});

	// Save trackpoints
	for(auto it = tps.constBegin(); it != tps.constEnd(); ++it) {
		TreeFragmentTrackPoint* tp = it->data();

		// Timepoint
		_out << (quint32)tp->getTimePoint();

		// Position number
		_out << (quint16)tp->getPositionNumber();

		// Coordinates
		_out << (float)tp->getX();
		_out << (float)tp->getY();

		// Confidence
		_out << (float)tp->getConfidenceLevel();
	}

	// Children
	if(_track->getChild1())
		exportTrackToFile(_out, _track->getChild1(), _trackNumber * 2);
	if(_track->getChild2())
		exportTrackToFile(_out, _track->getChild2(), _trackNumber * 2 + 1);
}

TreeFragmentTrack* TreeFragment::insertTrack(int trackNumber, int startTp, int stopTp, QHash<int, QSharedPointer<TreeFragmentTrackPoint>>&& trackPoints)
{
	QSharedPointer<TreeFragmentTrack>& track = tracks[trackNumber];
	if(!track.isNull())
		return 0;	// Already exists

	// Create new track
	track = QSharedPointer<TreeFragmentTrack>(new TreeFragmentTrack(trackNumber, startTp, stopTp, std::move(trackPoints), treeFragmentNumber, this));

	// Check if root
	if(trackNumber == 1)
		root = track;
	else {
		// Update child pointers of parent track
		QSharedPointer<TreeFragmentTrack> parent = tracks[trackNumber / 2];
		if(parent) {
			if(trackNumber % 2 == 0)
				parent->setChild1(track.data());
			else
				parent->setChild2(track.data());
		}
	}

	// Update maxTrackNumber
	if(maxTrackNumber == -1 || trackNumber > maxTrackNumber)
		maxTrackNumber = trackNumber;

	// Update firstTimePoint
	if(firstTimePoint == -1 || startTp < firstTimePoint)
		firstTimePoint = startTp;

	// Update numberOfTrackPoints
	numberOfTrackPoints += trackPoints.size();

	// Done
	return track.data();
}
