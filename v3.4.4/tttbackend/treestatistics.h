/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TREESTATISTICS_H
#define TREESTATISTICS_H

#include "tttdata/track.h"
#include "tttdata/tree.h"

#include <q3valuevector.h>

/**
	@author Bernhard Schauberger <tttcomputer@pc53339>
	
	This class holds simple tree statistics like cell counts.
*/

class TreeStatistics{

public:
	TreeStatistics();
	
	~TreeStatistics();
	
	/**
	 * calculates the statistics for the provided tree
	 * @param _tree a pointer to a Tree instance
	 * @return a new TreeStatistics object containing the stats for the provided tree
	 */
	static TreeStatistics calculate (const Tree *_tree);
	
	/**
	 * calculates the statistics for the provided tree, but only regards the generations and cell types that are provided by the parameters
	 * @param _tree a pointer to a Tree instance
	 * @param _generations the generations that should be regarded, provided as a boolean vector (index = generation, true -> regard it)
	 * @param _cellTypes the cell types that should be regarded for the calculation
	 * @param _maxStartingTP the timepoint before which a cell must have its first trace to be considered
	 * @param _maxEndingTP the timepoint before which a cell must have its last trace to be considered
	 * @return a new TreeStatistics object containing the stats for the provided tree
	 */
	static TreeStatistics calculate (const Tree *_tree, const Q3ValueVector<bool> &_generations, long _cellTypes, int _maxStartingTP, int _maxEndingTP);
	
	/**
	 * @return the total number of cells in the tree
	 */
	int getTotalCount()
		{return totalcount;}
	
	///getters for the cell numbers having the various stop reasons
	int getDividingCount()
		{return divcount;}
	
	int getApoptoticCount()
		{return apocount;}
	
	int getInterruptedCount()
		{return intercount;}
	
	int getLostCount()
		{return lostcount;}
	
	/**
	 * returns the number of cells for the given generation
	 * @param _generation the generation
	 * @return cell count in _generation
	 */
	int getGenerationCounts (int _generation);
	
	/**
	 * @return the number of generations (field size of generationCellcounts)
	 */
	int getGenerations()
		{return generationCellcounts.size();}
	
	/**
	 * adds up all single field values
	 * @param _ts the other TreeStatistics object
	 * @return a TreeStatistics object where each attribute has as value the sum of the same attribute of this and the second object
	 */
	TreeStatistics& operator+= (const TreeStatistics& _ts1);
	
private:
	
	///holds the number of cells for the different stop reasons
	int totalcount;
	int divcount;
	int apocount;
	int intercount;
	int lostcount;
	
	///holds the number of cells for each generation
	Q3ValueVector<int> generationCellcounts;
};

#endif
