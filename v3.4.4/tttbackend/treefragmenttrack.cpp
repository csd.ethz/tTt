/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "treefragmenttrack.h"

// Project
#include "tttdata/track.h"
#include "treefragment.h"

// STL
#include <cassert>


TreeFragmentTrack::TreeFragmentTrack(unsigned int _trackNumber, int _startTp, int _stopTp, QHash<int, QSharedPointer<TreeFragmentTrackPoint>>&& _trackPoints, int _treeFragmentNumber, TreeFragment* _treeFragment, TreeFragmentTrack* _child1 /*= 0*/, TreeFragmentTrack* _child2 /*= 0*/)
	: trackNumber(_trackNumber),
	startTp(_startTp),
	stopTp(_stopTp),
	treeFragmentNumber(_treeFragmentNumber),
	child1(_child1),
	child2(_child2),
	treeFragment(_treeFragment)
{	
	// Move track points
	trackPoints = std::move(_trackPoints);

	// Update statistics in TreeFragment
	if(treeFragment) {
		treeFragment->numberOfTrackPoints += trackPoints.size();
		if(startTp >= 0) {
			if(treeFragment->firstTimePoint == -1 || startTp < treeFragment->firstTimePoint)
				treeFragment->firstTimePoint = startTp;
		}
	}

	// Set parents of trackPoints
	for(QHash<int, QSharedPointer<TreeFragmentTrackPoint> >::const_iterator it = trackPoints.constBegin(); it != trackPoints.constEnd(); ++it)
		it->data()->setTreeFragmentTrack(this);
}

int TreeFragmentTrack::getFirstTimePoint() const
{
	//// Make sure we have trackpoints
	//if(trackPoints.isEmpty())
	//	return -1;

	//// First timepoint
	//return trackPoints[0]->timePoint;

	return startTp;
}

int TreeFragmentTrack::getLastTimePoint() const
{
	//// Make sure we have trackpoints
	//if(trackPoints.isEmpty())
	//	return -1;

	//// Last timepoint
	//return trackPoints[trackPoints.size() - 1]->timePoint;

	return stopTp;
}

TreeFragmentTrackPoint* TreeFragmentTrack::getTrackPointByTimePoint(int _timePoint) const
{
	// Look for trackpoint at _timePoint
	if(trackPoints.contains(_timePoint))
		return trackPoints[_timePoint].data();

	// Trackpoint not found
	return 0;
}

QSharedPointer<TreeFragmentTrackPoint> TreeFragmentTrack::getTrackPointByTimePointSharedPtr(int _timePoint) const
{
	return trackPoints.value(_timePoint, QSharedPointer<TreeFragmentTrackPoint>());
}

bool TreeFragmentTrack::deleteTrackPointByTimePoint(int _timePoint)
{
	// Check if exists
	if(!trackPoints.contains(_timePoint))
		return true;

	// Delete
	trackPoints.remove(_timePoint);

	// Update track statistics
	startTp = -1;
	stopTp = -1;
	for(auto it = trackPoints.constBegin(); it != trackPoints.constEnd(); ++it) {
		int tp = it.value()->timePoint;
		if(startTp == -1 || startTp > tp)
			startTp = tp;
		if(stopTp == -1 || stopTp < tp)
			stopTp = tp;
	}

	// Update TreeFragment statistics
	if(treeFragment) {
		treeFragment->numberOfTrackPoints -= 1;
		if(trackNumber == 1)
			treeFragment->firstTimePoint = startTp;
	}

	return true;
}

TreeFragmentTrackPoint* TreeFragmentTrack::getFirstTrackPoint() const
{
	// Get shared pointer (will return 0-pointer if startTp is not in hash)
	QSharedPointer<TreeFragmentTrackPoint> ret = trackPoints.value(startTp);
	return ret.data();
}

TreeFragmentTrackPoint* TreeFragmentTrack::getLastTrackPoint() const
{
	// Get shared pointer (will return 0-pointer if stopTp is not in hash)
	QSharedPointer<TreeFragmentTrackPoint> ret = trackPoints.value(stopTp);
	return ret.data();
}

TrackStopReason TreeFragmentTrack::getStopReason() const
{
	// Check if we have children
	if(child1 || child2)
		return TS_DIVISION;

	// Auto generated fragments currently have no other stop reasons
	return TS_NONE;
}

ITree* TreeFragmentTrack::getITree() const
{
	return treeFragment;
}

QVector<QString> TreeFragmentTrack::getAvailableProperties() const
{
	// ToDo:
	return QVector<QString>();
}

QVariant TreeFragmentTrack::getTrackProperty( QString _propName ) const
{
	// ToDo:
	return QVariant();
}

TreeFragmentTrack * TreeFragmentTrack::getMother() const
{
	if(treeFragment) 
		return treeFragment->getTrackByNumber(trackNumber / 2);
	return 0;
}

int TreeFragmentTrack::setTrackNumber( int newNum )
{
	trackNumber = newNum;
	if(child1)
		return std::max(child1->setTrackNumber(newNum * 2), child2->setTrackNumber(newNum * 2 + 1));
	
	return newNum;	
}

bool TreeFragmentTrack::insertTrackPoint(const QSharedPointer<TreeFragmentTrackPoint>& trackPoint)
{
	// If track has child tracks, which begin before or at the time point of the new trackPoint, it cannot be inserted
	if((child1 && child1->getFirstTimePoint() <= trackPoint->timePoint) ||
		(child2 && child2->getFirstTimePoint() <= trackPoint->timePoint))
		return false;

	// If track has parent that ends after or at the time point of the new trackPoint, it cannot be inserted
	if(trackNumber > 1 && trackPoint->timePoint < getFirstTimePoint()) {
		TreeFragmentTrack* mother = getMother();
		if(!mother)
			// Cannot check
			return false;
		if(mother->getLastTimePoint() >= trackPoint->timePoint)
			return false;
	}

	// ok, can insert/overwrite track point
	QSharedPointer<TreeFragmentTrackPoint>& dstRef = trackPoints[trackPoint->timePoint];
	bool increaseTrackPointCount = dstRef.isNull();
	dstRef = trackPoint;
	trackPoint->setTreeFragmentTrack(this);

	// Update track statistics
	if(startTp == -1 || startTp > trackPoint->timePoint)
		startTp = trackPoint->timePoint;
	if(stopTp == -1 || stopTp < trackPoint->timePoint)
		stopTp = trackPoint->timePoint;

	// Update TreeFragment statistics
	if(treeFragment) {
		if(increaseTrackPointCount)
			treeFragment->numberOfTrackPoints += 1;
		if(treeFragment->firstTimePoint == -1 || startTp < treeFragment->firstTimePoint)
			treeFragment->firstTimePoint = startTp;
	}

	return true;
}