/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MATRIX_H
#define MATRIX_H

#include <q3ptrvector.h>
#include <q3valuevector.h>
#include <qstring.h>
#include <qstringlist.h>

#include <fstream>
#include <typeinfo>


///the filename suffix for all stored matrices
///for tTt, such a matrix should lie in the position folder and be called [BaseName].[SUFFIX]
///e.g. 051104-2_p025.intensity_matrix
const QString MATRIX_SUFFIX = "intensity_matrix";


/**
	@author Bernhard <bernhard.schauberger@campus.lmu.de>
	Used to store a two-dimensional matrix of arbitrary size containing values of type <E>
	
	Note: for generic classes, the implementation needs to be in the header file.
*/

template<class E>
class Matrix {

public:
	Matrix();
	
	Matrix (int _width, int _height);
	
	~Matrix();
	
	bool isEmpty()
		{return emptyMatrix;}
	
	E getValue (int _x, int _y) const;
	
/*	void fillRow (int _row, QValueVector<E> _values);
	
	QValueVector<E> getRow (int _row) const;
	
	QValueVector<E> getCol (int _col) const;*/
	
	void setValue (int _x, int _y, E _value);
	
	E valueSum() const;
	
	void fillMatrix (QString _values);
	
	bool saveMatrix (QString _filename) const;
	
	bool readMatrix (QString _filename);
	
	void clear();
	
	static Matrix<E> readMatrixFile (QString _filename);
	
private:
	
	int width;
	int height;
	
	Q3ValueVector<Q3ValueVector<E> > data;
	
	bool emptyMatrix;
	
	void initMatrix (int _rows, int _cols);
};


template<class E>
Matrix<E>::Matrix()
{
	emptyMatrix = true;
	width = height = -1;
}

template<class E>
Matrix<E>::Matrix (int _width, int _height)
{
	initMatrix (_width, _height);
}

template<class E>
void Matrix<E>::initMatrix (int _width, int _height)
{
	width = _width;
	height = _height;
	data.clear();
	
	for (int i = 0; i < _width; i++) {
		Q3ValueVector<E> row (_height);
		for (int j = 0; j < _height; j++)
			row.push_back (E());
		
		data.push_back (row);
	}
	
	emptyMatrix = false;
}

template<class E>
Matrix<E>::~Matrix()
{
}

template<class E>
E Matrix<E>::getValue (int _x, int _y) const
{
	if (emptyMatrix)
		return E();
	
	if (_x >= 0 && _x < width)
		if (_y >= 0 && _y < height)
			return data.at (_x).at (_y);
	
	return E();
}

// template<class E>
// void Matrix<E>::fillRow (int _row, QValueVector<E> _values)
// {
// 	if (emptyMatrix)
// 		return;
// 	
// 	//no check on boundaries is performed!
// 	//assumes that the matrix has been initialized
// 	
// 	if (_row >= 0 && _row < rows) {
// 		for (int i = 0; i < (int)_values.size(); i++)
// 			data.at (_row) [i] = _values.at (i);
// 	}
// }
// 
// template<class E>
// QValueVector<E> Matrix<E>::getRow (int _row) const
// {
// 	if (emptyMatrix)
// 		return QValueVector<E>();
// 	
// 	if (_row >= 0 && _row < rows)
// 		return data.at (_row);
// }
// 
// template<class E>
// QValueVector<E> Matrix<E>::getCol (int _col) const
// {
// 	if (emptyMatrix)
// 		return QValueVector<E>();
// 	
// 	QValueVector<E> result (rows);
// 	if (_col >= 0 && _col < columns) {
// 		for (int i = 0; i < rows; i++) {
// 			result.insert (data.at (i).at (_col));
// 		}
// 	}
// 	return result;
// }

template<class E>
void Matrix<E>::setValue (int _x, int _y, E _value)
{
	if (emptyMatrix)
		return;
	
	if (_x >= 0 && _x < width)
		if (_y >= 0 && _y < height)
			data.at (_x) [_y] = _value;
}

template<class E>
E Matrix<E>:: valueSum() const
{
	if (emptyMatrix)
		return E();
	
	E sum = 0;
	for (int i = 0; i < width; i++) {
		for (int j = 0; j < height; j++) {
			sum += data.at (i).at (j);
		}
	}
	
	return sum;
}

template<class E>
void Matrix<E>::fillMatrix (QString _values)
{
	if (emptyMatrix)
		return;
	
	QStringList rows = QStringList::split (";", _values);
	
	int row = 0;
	for (QStringList::Iterator iter = rows.begin(); iter != rows.end(); ++iter, row++) {
		QStringList cols = QStringList::split (",", *iter);
		int col = 0;
		for (QStringList::Iterator iter2 = cols.begin(); iter2 != cols.end(); ++iter2, col++) {
			E val;
			if (typeid(E) == typeid(float))
				val = (*iter2).toFloat();
			else if (typeid(E) == typeid(int))
				val = (*iter2).toInt();
			
			setValue (row, col, val);
		}
        }
}

template<class E>
bool Matrix<E>::saveMatrix (QString _filename) const
{
	//write into a binary file
	std::ofstream saver (_filename, std::ios_base::binary | std::ios_base::out);
	
	if (! saver)
		return false;			//opening for output failed
	
	//writing a binary output (here int)
	//saver.write(reinterpret_cast<char const *>(&variable), sizeof(int));
	
	//write dimensions
	saver.write(reinterpret_cast<char const *>(&width), sizeof(int));
	saver.write(reinterpret_cast<char const *>(&height), sizeof(int));
	
	//write values
	E value;
	for (int i = 0; i < width; i++) {
		for (int j = 0; j < height; j++) {
			value = getValue (i, j);
			saver.write(reinterpret_cast<const char *>(&value), sizeof(E));
		}
	}
	
	saver.close();
	
	return true;
}

template<class E>
void Matrix<E>::clear()
{
	data.clear();
	emptyMatrix = true;
}

template<class E>
bool Matrix<E>::readMatrix (QString _filename)
{
	//read from binary file
	
	std::ifstream loader (_filename, std::ios_base::binary | std::ios_base::in);
	
	if (! loader)
		return false;			//opening for input failed
	
	//general statement for reading binary data (here int):
	//loader.read(reinterpret_cast<char *>(&variable), sizeof(int));
	
	
	//read dimensions and initialize matrix
	int r_width = 0, r_height = 0;
	loader.read (reinterpret_cast<char *>(&r_width), sizeof(int));
	loader.read (reinterpret_cast<char *>(&r_height), sizeof(int));
	initMatrix (r_width, r_height);
	
	//read values
	E value;
	for (int i = 0; i < r_width; i++) {
		for (int j = 0; j < r_height; j++) {
			loader.read(reinterpret_cast<char *>(&value), sizeof(E));
			setValue (i, j, value);
		}
	}
	
	loader.close();
	
	return true;
}

template<class E>
Matrix<E> Matrix<E>::readMatrixFile (QString _filename)
{
	Matrix<E> matrix();
	matrix.readMatrix (_filename);
	return matrix;
}



#endif
