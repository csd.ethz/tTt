/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "positionmanagervector.h"
//Added by qt3to4:
#include <Q3PtrCollection>

PositionManagerVector::PositionManagerVector()
 : Q3PtrVector<TTTPositionManager> ()
{}


PositionManagerVector::~PositionManagerVector()
{}

int PositionManagerVector::compareItems (Q3PtrCollection::Item _d1, Q3PtrCollection::Item _d2)
{
	//the return value must fulfill:
	//== 0, if _d1 == _d2
	//<  0, if _d1 < _d2
	//>  0, if _d1 > _d2
	
	TTTPositionManager *pm1 = (TTTPositionManager*)_d1;
	TTTPositionManager *pm2 = (TTTPositionManager*)_d2;
	
	if (pm1->positionInformation.getIndex() < pm2->positionInformation.getIndex())
		return -1;
	else if (pm1->positionInformation.getIndex() > pm2->positionInformation.getIndex())
		return 1;
	else
		return 0;
}

