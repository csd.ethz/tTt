/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "treeelementstyle.h"

#include "tools.h"
#include "tttio/xmlhandler.h"
#include "tttgui/graphicaid.h"


const char* TISSUETYPETEXTS[7] = {"TT Blood", "TT Stroma", "TT Endothelium", "TT Heart", "TT Nerve", "TT Endoderm", "TT Mesoderm"};
const char* GENERALTYPETEXTS[8] = {"GT Myeloid", "GT Lymphoid", "GT HSC", "GT Primitive", "GT Primary", "GT Cellline", "GT Neuro", "GT Glia"};
const char* LINEAGETEXTS[8] = {"Lineage Megakaryoid", "Lineage Erythroid", "Lineage Makrophage", "Lineage Neutrophil", "Lineage Eosinophil", "Lineage Basophil", "Lineage Mast", "Lineage Dendritic"};




TreeElementStyle::TreeElementStyle()
 : RTTI (0), key (0), visible (false), size (0), colorCount (1), positionX (0), positionY (0), symbol ("")
{
	//this element, constructed in such a way, is definitely not visible
	//initTexts();
	
	for (int i = 0; i < MAX_COLORS; i++)
		colors[i] = Qt::white;
}

TreeElementStyle::TreeElementStyle (const TreeElementStyle &_tes)
{
	//initTexts();
	*this = _tes;
}

TreeElementStyle::TreeElementStyle (int _rtti)
 : RTTI (_rtti)
{
	reset();		//set default values for this _rtti value
}

TreeElementStyle::TreeElementStyle (char _key)
 : key (_key)
{
	reset();		//set default values for this key value
}

TreeElementStyle::~TreeElementStyle()
{}

TreeElementStyle& TreeElementStyle::operator= (const TreeElementStyle &_tes)
{
	RTTI = _tes.RTTI;
	key = _tes.key;
	visible = _tes.visible;
	size = _tes.size;
	for (int i = 0; i < MAX_COLORS; i++)
		colors[i] = _tes.colors[i];
	positionX = _tes.positionX;
	positionY = _tes.positionY;
	symbol = _tes.symbol;
	displayedText = _tes.displayedText;
	
	return (*this);
}

void TreeElementStyle::setRTTI (int _rtti)
{
        if ((RTTI == 0) & (_rtti != 0)) {
		RTTI = _rtti;
		reset();
	}
}

void TreeElementStyle::setKey (char _key)
{
        if ((key == 0) & (_key != 0)) {
		key = _key;
		reset();
	}
}

void TreeElementStyle::setGrayscale()
{
	// //does not work sometimes??????
	//color = qGray (color.rgb());
	
	//int v = color.green();
	//color = QColor (v, v, v);
	for (int i = 0; i < MAX_COLORS; i++)
                colors[i] = GraphicAid::convertToGrayScale (colors[i]);
	
	switch (RTTI) {
		case RTTI_CELLSTATE_FREEFLOATING:
			size = 8;
			break;
		case RTTI_CELLSTATE_SPYHOP:
			size = 4;
			break;
		case RTTI_CELLSTATE_NORMAL:
			size = 1;
			break;
		case RTTI_HOURLINES:
			visible = false;
			break;
		case RTTI_DIVISIONLINE:
			size = 1;
			break;
		case RTTI_STOPREASON_APOPTOSIS:
			size = 20;
			break;
		case RTTI_STOPREASON_LOST:
			visible = false;
			break;
		case RTTI_TIMEPOINTLINE:
			visible = false;
			break;
/*		case RTTI_:
			size = ;
			break;*/
		default:
			;
	}
}

void TreeElementStyle::reset()
{
	colorCount = 1;
	
	if (RTTI > 0) {
		switch (RTTI) {
			case RTTI_CELLCIRCLE_FREE:
				visible = true;
				size = 2;
				colors[0] = CELLFREECOLOR;
				positionX = 0;
				symbol = "";
				displayedText = "Cell circles untracked";
				break;
			case RTTI_CELLCIRCLE_TRACKED:
				visible = true;
				size = 2;
				colors[0] = CELLCOLOR;
				positionX = 0;
				symbol = "";
				displayedText = "Cell circles tracked";
				break;
			case RTTI_CELLCIRCLE_SELECTION:
				visible = true;
				size = 2;
				colors[0] = CELLSELECTCOLOR;
				positionX = 0;
				symbol = "";
				displayedText = "Cell circles selected";
				break;
			case RTTI_DIVISIONLINE:
				visible = true;
				size = 2;
				colors[0] = HERITAGECOLOR;
				positionX = 0;
				symbol = "";
				displayedText = "Horizontal cell division line";
				break;
			case RTTI_STOPREASON_APOPTOSIS:
				visible = true;
				size = 10;
				colors[0] = CELLDEATHCOLOR;
				positionX = 0;
				symbol = "X";
				displayedText = "'Apoptosis' symbol";
				break;
			case RTTI_STOPREASON_LOST:
				visible = true;
				size = 10;
				colors[0] = CELLLOSTCOLOR;
				positionX = 0;
				symbol = "?";
				displayedText = "'Lost' symbol";
				break;
			case RTTI_STOPREASON_NONE:
				visible = false;
				size = 10;
				colors[0] = Qt::white;
				positionX = 0;
				symbol = "N";
				displayedText = "'TS_NONE' symbol";
				break;
			//MAX_WAVE_LENGTH
	//		case RTTI_WaveLength0:
			case RTTI_WAVELENGTH1:
			case RTTI_WAVELENGTH2:
			case RTTI_WAVELENGTH3:
			case RTTI_WAVELENGTH4:
			case RTTI_WAVELENGTH5:
			case RTTI_WAVELENGTH6:
			case RTTI_WAVELENGTH7:
			case RTTI_WAVELENGTH8:
			case RTTI_WAVELENGTH9:
				visible = true;
				size = 2;
				symbol = "";
				switch (RTTI) {
	/*				case RTTI_WaveLength0:
						visible = false;
						colors[0] = Qt::white;
						positionX = 0;
						break;*/
					case RTTI_WAVELENGTH1:
						colors[0] = WAVELENGTH1COLOR;
						positionX = -10;
						break;
					case RTTI_WAVELENGTH2:
						colors[0] = WAVELENGTH2COLOR;
						positionX = -8;
						break;
					case RTTI_WAVELENGTH3:
						colors[0] = WAVELENGTH3COLOR;
						positionX = -6;
						break;
					case RTTI_WAVELENGTH4:
						colors[0] = WAVELENGTH4COLOR;
						positionX = -4;
						break;
					case RTTI_WAVELENGTH5:
						colors[0] = WAVELENGTH5COLOR;
						positionX = -2;
						break;
					case RTTI_WAVELENGTH6:
						colors[0] = WAVELENGTH6COLOR;
						positionX = -12;
						break;
					case RTTI_WAVELENGTH7:
						colors[0] = WAVELENGTH7COLOR;
						positionX = -14;
						break;
					case RTTI_WAVELENGTH8:
						colors[0] = WAVELENGTH8COLOR;
						positionX = -16;
						break;
					case RTTI_WAVELENGTH9:
						colors[0] = WAVELENGTH9COLOR;
						positionX = -18;
						break;
					default:
						visible = false;
						colors[0] = Qt::white;
						positionX = 0;
				}
				displayedText = QString ("Wavelength %1 line").arg (RTTI - RTTI_WAVELENGTH0);
				break;
			case RTTI_CELLSTATE_NORMAL:
				visible = true;
				size = 2;
				colors[0] = CELLNORMALCOLOR;
				positionX = 0;
				symbol = "";
				displayedText = "Cell state: normal (adherent)";
				break;
			case RTTI_CELLSTATE_FREEFLOATING:
				visible = true;
				size = 2;
				colors[0] = CELLFLOATINGCOLOR;
				positionX = 0;
				symbol = "";
				displayedText = "Cell state: free floating";
				break;
			case RTTI_CELLSTATE_SPYHOP:
				visible = true;
				size = 2;
				colors[0] = CELLSPYHOPCOLOR;
				positionX = 0;
				symbol = "";
				displayedText = "Cell state: semi-adherent";
				break;
			case RTTI_CELLSPEED:
				visible = false;
				size = 0;
				colors[0] = Qt::white;
				positionX = 2;
				symbol = "";
				displayedText = "Cell speed (in heritage line)";
				break;
			case RTTI_SPEEDBAR:
				visible = false;
				size = 0;
				colors[0] = Qt::white;
				positionX = 0;
				symbol = "";
				displayedText = "Cell speed (right bar in tree)";
				break;
			case RTTI_CELLNUMBERS:
				visible = true;
				size = 10;
				colors[0] = CELLNUMBERCOLOR;
				positionX = 0;	//?? four directions?
				symbol = "";	//?? e.g. "Nr %1"
				displayedText = "Cell numbers";
				break;
			case RTTI_GENERATIONTIME:
				visible = false;
				size = 10;
				colors[0] = Qt::black;
				positionX = 0;	//?? y offset?
				symbol = "hm";	//allowed: hm (hours and minutes), h (only hours), m (in minutes)
				displayedText = "Generation time";
				break;
			case RTTI_ENDOMITOSIS:
				visible = true;
				size = 10;
				colors[0] = ENDOMITOSISCOLOR;
				positionX = 4;
				symbol = "OO";
				displayedText = "Endomitosis symbol";
				break;
			case RTTI_COLOCATION:
				visible = false;
				size = 2;
				colors[0] = COLOCATIONCOLOR;
				positionX = 0;
				symbol = "";
				displayedText = "Colocation line";
				break;
			case RTTI_TIMEPOINTLINE:
				visible = true;
				size = 2;
				colors[0] = TIMEPOINTCOLOR;
				positionX = 0;
				symbol = "";
				displayedText = "Timepoint line";
				break;
			case RTTI_DAYLINES:
				visible = true;
				size = 2;
				colors[0] = DAYSEPARATORCOLOR;
				positionX = 0;
				symbol = "";
				displayedText = "Day lines";
				break;
			case RTTI_HOURLINES:
				visible = true;
				size = 2;
				colors[0] = DAYSEPARATORCOLOR;
				positionX = 0;
				symbol = "";
				displayedText = "Hour lines";
				break;
			case RTTI_COMMENTASTERISK:
				visible = true;
				size = 10;
				colors[0] = COMMENTASTERISKCOLOR;
				positionX = 3;
				symbol = "*";
				displayedText = "Comment symbol";
				break;
			case RTTI_TIMESCALE_FIGURES:
				visible = true;
				size = 12;
				colors[0] = TIMESCALEFIGURECOLOR;
				positionX = 0;
				symbol = "";
				displayedText = "Timescale figures";
				break;
			case RTTI_CELLDISTANCE:
				visible = false;
				size = 10;
				colors[0] = CELLDISTANCECOLOR;
				positionX = 0;	//?? y offset?
				symbol = "pix";	//allowed: hm (hours and minutes), h (only hours), m (in minutes)
				displayedText = "Cell covered distance";
				break;
				
			case RTTI_TISSUETYPEBLOOD:
			case RTTI_TISSUETYPESTROMA:
			case RTTI_TISSUETYPEENDOTHELIUM:
			case RTTI_TISSUETYPEHEART:
			case RTTI_TISSUETYPENERVE:
			case RTTI_TISSUETYPEENDODERM:
			case RTTI_TISSUETYPEMESODERM:
				visible = false;
				size = 2;
				colors[0] = TISSUETYPECOLORS [RTTI - RTTI_TISSUETYPEBLOOD];
				positionX = +7;
				symbol = "";
				displayedText = TISSUETYPETEXTS [RTTI - RTTI_TISSUETYPEBLOOD];
				break;
			
			case RTTI_GENERALTYPEMYELOID:
			case RTTI_GENERALTYPELYMPHOID:
			case RTTI_GENERALTYPEHSC:
			case RTTI_GENERALTYPEPRIMITIVE:
			case RTTI_GENERALTYPEPRIMARY:
			case RTTI_GENERALTYPECELLLINE:
			case RTTI_GENERALTYPENEURO:
			case RTTI_GENERALTYPEGLIA:
				visible = false;
				size = 2;
				colors[0] = GENERALTYPECOLORS [RTTI - RTTI_GENERALTYPEMYELOID];
				positionX = +5;
				symbol = "";
				displayedText = GENERALTYPETEXTS [RTTI - RTTI_GENERALTYPEMYELOID];
				break;
	
	
			case RTTI_LINEAGEMEGAKARYOID:
			case RTTI_LINEAGEERYTHROID:
			case RTTI_LINEAGEMAKROPHAGE:
			case RTTI_LINEAGENEUTROPHIL:
			case RTTI_LINEAGEEOSINOPHIL:
			case RTTI_LINEAGEBASOPHIL:
			case RTTI_LINEAGEMAST:
			case RTTI_LINEAGEDENDRITIC:
				visible = false;
				size = 2;
				colors[0] = LINEAGECOLORS [RTTI - RTTI_LINEAGEMEGAKARYOID];
				positionX = +3;
				symbol = "";
				displayedText = LINEAGETEXTS [RTTI - RTTI_LINEAGEMEGAKARYOID];
				break;
	
			case RTTI_ENVIRONMENTNUCLEAR:
			case RTTI_ENVIRONMENTCELL:
				visible = false;
				size = 2;
				colorCount = 15;
				for (int i = 0; i < 16; i++)
					colors[i] = GraphicAid::mainColor (i);
				positionX = +4;
				symbol = "";
				displayedText = ADDONTEXTS [RTTI - RTTI_ENVIRONMENTNUCLEAR];
				break;
			
			case RTTI_CLUSTERID:
				visible = false;
				size = 10;
				colors[0] = Qt::black;
				positionX = -5;
				symbol = "";
				displayedText = "Cluster ID from TM file";
				break;
	
	
	/*		case RTTI_:
				visible = false;
				size = 0;
				colors[0] = Qt::white;
				positionX = 0;
				symbol = "";
				displayedText = "";
				break;*/
			default:
				//rtti value does not exist - mark this object as "unnecessary"
				RTTI = 0;
				visible = false;
				size = 0;
				colors[0] = Qt::white;
				positionX = 0;
				symbol = "";
				displayedText = "";
		}
	}
	else if (key > 0) {
		switch (key) {
			default:
				visible = false;
				size = 1;
				colors[0] = GraphicAid::mainColor ((uint)key);
				positionX = 2;
				symbol = "";
				displayedText = key;
		}
	}
}

/*QDataStream& operator<< (QDataStream& _str, const TreeElementStyle& _tes)
{
	//write data to stream
	
	_str << _tes.RTTI;
	_str << (Q_UINT8)_tes.key;
	_str << _tes.displayedText;
	if (_tes.visible)
		_str << (int)1;
	else
		_str << (int)0;
	
	_str << _tes.size;
	for (int i = 0; i < MAX_COLORS; i++) {
		GraphicAid::saveColor (_str, _tes.colors[i]);
	}
	_str << _tes.colorCount;
	_str << _tes.positionX;
	_str << _tes.positionY;
	_str << _tes.symbol;
	
	return _str;
}
*/

QDataStream& operator>> (QDataStream& _str, TreeElementStyle& _tes)
{
	_str >> _tes.RTTI;
	Q_UINT8 tmp;
	_str >> tmp;
	_tes.key = (char)tmp;
	_str >> _tes.displayedText;
	
	int visible = 0;
	_str >> visible;
	_tes.visible = (visible == 0 ? false : true);
	
	_str >> _tes.size;
	for (int i = 0; i < MAX_COLORS; i++) {
		GraphicAid::readColor (_str, _tes.colors[i]);
	}
	_str >> _tes.colorCount;
	_str >> _tes.positionX;
	_str >> _tes.positionY;
	_str >> _tes.symbol;

	return _str;
}

void TreeElementStyle::toDomElement (QDomDocument &_doc, QDomElement &_parent) const
{
	
	QDomElement de = _doc.createElement ("TreeElementStyle");
	
	de.setAttribute ("RTTI", RTTI);
	de.setAttribute ("Key", key);
	de.setAttribute ("DisplayedText", displayedText);
	de.setAttribute ("Visible", (visible ? 1 : 0));
	de.setAttribute ("Size", size);
	de.setAttribute ("ColorCount", colorCount);
	de.setAttribute ("PositionX", positionX);
	de.setAttribute ("PositionY", positionY);
	de.setAttribute ("Symbol", symbol);
	
	//for each color one attribute is created
	for (int i = 0; i < MAX_COLORS; i++) {
		de.setAttribute (QString ("index_%1").arg (i), i);
		de.setAttribute (QString ("color_%1").arg (i), GraphicAid::colorToString (colors[i]));
	}
	
	_parent.appendChild (de);
}

void TreeElementStyle::setFromXMLAttributes (const QXmlAttributes &_attrs)
{
	//set the values from the attributes
	
	QString tmp = "";
	
	tmp = XMLHandler::getAttributeValue (_attrs, "RTTI");
	RTTI = tmp.toInt();
	tmp = XMLHandler::getAttributeValue (_attrs, "Key");
	key = tmp.toInt();
	tmp = XMLHandler::getAttributeValue (_attrs, "DisplayedText");
	displayedText = tmp;
	tmp = XMLHandler::getAttributeValue (_attrs, "Visible");
	visible = tmp == "1";
	tmp = XMLHandler::getAttributeValue (_attrs, "Size");
	size = tmp.toInt();
	tmp = XMLHandler::getAttributeValue (_attrs, "ColorCount");
	colorCount = tmp.toInt();
	tmp = XMLHandler::getAttributeValue (_attrs, "PositionX");
	positionX = tmp.toInt();
	tmp = XMLHandler::getAttributeValue (_attrs, "PositionY");
	positionY = tmp.toInt();
	tmp = XMLHandler::getAttributeValue (_attrs, "Symbol");
	symbol = tmp;
	
	for (int i = 0; i < MAX_COLORS; i++) {
		
		tmp = XMLHandler::getAttributeValue (_attrs, QString ("index_%1").arg (i));
		if (! tmp.isEmpty()) {
			tmp = XMLHandler::getAttributeValue (_attrs, QString ("color_%1").arg (i));
			colors[i] = GraphicAid::stringToColor (tmp);
		}
	}
}

