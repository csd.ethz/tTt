/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef treefragmenttrackpoint_h__
#define treefragmenttrackpoint_h__

// Project
#include "itrackpoint.h"

// Qt
#include <QLineF>
#include <QVector>


// Forward declarations
class TreeFragmentTrack;


/**
 * Represent one trackpoint of a TreeFragmentTrack. As TreeFragmentTrack this class is read-only
 * Note: tTt has to be able to load a million or even more instances of this class at the same time, so every byte in here is expensive in terms of memory consumption
 */
class TreeFragmentTrackPoint : public ITrackPoint {
public:

	// Constructor
	TreeFragmentTrackPoint(int _timePoint, short _positionNumber, float _posX, float _posY, float _confidence = -1.0f) 
		: timePoint(_timePoint), positionNumber(_positionNumber), posX(_posX), posY(_posY), confidence(_confidence)
	{	
		track = 0;
	}

	// Timepoint
	int timePoint;

	// Position number
	short positionNumber;

	// Global micrometer coordinates
	float posX;
	float posY;

	// Confidence level
	float confidence;

	// Additional values (compare TreeFragment::additionalColumns)
	QVector<double> additionalValues;

	//// Number of treefragment this trackpoint belongs to
	//const int treeFragmentNumber;

	/**
	 * Set the TreeFragmentTrack this trackpoint belongs to, should only be called in the beginning
	 * @param _track pointer to TreeFragmentTrack instance
	 */
	void setTreeFragmentTrack(TreeFragmentTrack* _track) {
		track = _track;
	}

	/**
	 * @return pointer to ITrack instance this trackpoint belongs to, can be 0 
	 */
	ITrack* getITrack() const;

	/**
	 * @return pointer to TreeFragmentTrack instance this trackpoint belongs to, can be 0 
	 */
	TreeFragmentTrack* getTrack() const {
		return track;
	}

	/**
	 * @return x coordinate in global micrometer coordinates
	 */
	float getX() const {
		return posX;
	}

	/**
	 * @return y coordinate in global micrometer coordinates
	 */
	float getY() const {
		return posY;
	}

	/**
	 * @return timepoint
	 */
	int getTimePoint() const {
		return timePoint;
	}

	/**
	 * check for wavelength
	 * @param _waveLength the wavelength
	 * @return true if wavelength is set 
	 */
	bool checkForWavelength(int _waveLength) const {
		// TreeFragments currently contain no wavelength info
		return _waveLength == 0;
	}

	/**
	 * Return the number of the position where this trackpoint has been set
	 * @return number of position or -1 if not available
	 */
	int getPositionNumber() const {
		if(positionNumber)
			return positionNumber;
		else
			return -1;
	}

	/**
	 * @return confidence level of this trackpoint, i.e. a value in [0,1] or -1 if not available
	 */
	float getConfidenceLevel() const {
		return confidence;
	}

	/**
	 * Calculate euclidean distance to other trackpoint.
	 */
	float getDistanceTo(const TreeFragmentTrackPoint& other) {
		return QLineF(posX, posY, other.posX, other.posY).length();
	}

	/**
	 * @return addition bits for this tp
	 * e.g. used for special tracking keys
	 */
	virtual short int getAddonBits(int bitback) const {
		return -1;
	}

	/**
	 * Returns the value of the requested trackpoint property. Only
	 * values returned by getAvailableProperties
	 */
	virtual QVariant getTPProperty(QString propertyName) const {
		return QVariant();
	}

	/**
	 * Returns all available trackpoint properties
	 */
	virtual QVector<QString> getAvailableProperties() const;

private:

	// TreeFragmentTrack this trackpoint belongs to
	TreeFragmentTrack* track;

	friend class TreeFragment;
};


#endif // treefragmenttrackpoint_h__
