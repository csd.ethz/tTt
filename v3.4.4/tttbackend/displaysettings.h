/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef DISPLAYSETTINGS_H
#define DISPLAYSETTINGS_H

#include "tttdata/bdisplay.h"




class TTTManager;



/**
	@author Bernhard
	
	This class contains an array of BDisplay objects and provides access to it.\n
	Also holds various methods for the microscope settings access.
*/

class DisplaySettings
{

public:
	DisplaySettings();
	
	~DisplaySettings();
	
	/**
	 * Returns the display settings for the specified wavelength
	 * @param _wavelength 
	 * @return a BDisplay object containing these settings
	 */
	BDisplay& at (int _wavelength)
	{
		if (_wavelength >= 0 && _wavelength < settings.size()) 
			//return *(settings.find (_wavelength));
			return settings[_wavelength];
		else
			return dummy;
	}
	
	/**
	 * Returns the number of current settings
	 * @return the number of wavelengths that have settings stored here
	 */
	int size() const {
		return settings.size();
	}
	
	/**
	 * sets the complete size of the pictures for a certain wavelength
	 * note: assumes that all pictures of one wavelength are of the same size
	 * @param _waveLength the wavelength of which the pictures have the given size
	 * @param _width the width of all pictures of this wavelength
	 * @param _height the height of all pictures of this wavelength
	 */
	void setPictureSize (int _waveLength, int _width, int _height);
	
	/**
	* sets the loading region for all pictures of the given wavelength
	* note: assumes that all pictures of one wavelength are of the same size
	* @param _waveLength the wavelength for which all pictures should be set
	* @param _offsetX the left border of the loading region
	* @param _offsetY the top border of the loading region
	* @param _width the width of the loading region
	* @param _height the height of the loading region
	*/
	void setLoadingRegion (int _waveLength, int _offsetX, int _offsetY, int _width, int _height);
	
private:

	///the array for the settings
	//Q3 IntDict<BDisplay> settings;
	QVector<BDisplay> settings;
	
	///an empty display
	///necessary for returning a value in method "at" for not found display
	BDisplay dummy;
	
};

#endif
