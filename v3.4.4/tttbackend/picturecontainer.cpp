/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "picturecontainer.h"
#include "picturearray.h"
//#include "pictureview.h"
#include "tttmanager.h"
#include "tttbackend/tools.h"

#include <QImageReader>

// For Debugging only
//#ifdef DEBUGMODE
//#include <Windows.h>
//#endif


PictureContainer::PictureContainer (const QString& directory, const QString& _filename, const PictureIndex& _picIndex, bool _loadingFlag, QRect _defaultSize)
	:	
	m_path(directory),
	m_filename(_filename),
	Index(_picIndex),
	DefaultSize(_defaultSize)
{
	Selection = false;
	imageBackgroundCorrected = false;
	scalingFactorX = 1.0;
	scalingFactorY = 1.0;
}

PictureContainer::~PictureContainer()
{
	removeImage();
}

int PictureContainer::loadImage(bool load)
{
	
	// If LoadingFlag is false and a image is in memory, it is removed
	if (!load) {
		if (m_img.data) 
			return (removeImage() ? 0 : 2);		// 0 (no error) if unloading was succesful

		return 2;
	}

	// If there is still a picture in memory, it must be deleted first
	if (m_img.data) 
		return 2;			

	// Load the picture 
	m_img = Tools::openGrayScaleImageUnchanged(m_path + m_filename);
	if (!m_img.data) {
		// Check if we have run out of memory
		if(errno == ENOMEM) {
			// If this actually happens, we have a big problem and ttt will probably crash very soon due to
			// failed and unchecked allocations somewhere. Therefore this problem is avoided earlier in picturearray.
			return 1;
		}
		return 2;
	}
	assert(m_img.data);
	assert(m_img.type() == CV_8U || m_img.type() == CV_16U);

	// Compute scale factors
	if(m_img.cols != DefaultSize.width() || m_img.rows != DefaultSize.height()) {
		double factorX = (double)DefaultSize.width() / (double)m_img.cols,
			factorY = (double)DefaultSize.height() / (double)m_img.rows;

		scalingFactorX = factorX;
		scalingFactorY = factorY;
	}
	else {
		scalingFactorX = 1.0;
		scalingFactorY = 1.0;
	}

	// Newly loaded images are assumed to be never background corrected already
	imageBackgroundCorrected = false;

	// Success
	return 0; 		
}

bool PictureContainer::removeImage()
{
	m_img = cv::Mat();

    return true;
}

int PictureContainer::readPixel (int _x, int _y) const
{
	// Check if we have an image
	if(!m_img.data)
		return -1;

	// Check if we have scaling
	if(abs(scalingFactorX - 1.0) > 0.01 || abs(scalingFactorY - 1.0) > 0.01) {
		double tmpX = (double)_x / scalingFactorX,
			tmpY = (double)_y / scalingFactorY;

		// Apply scaling
		_x = (int)(tmpX + 0.5);
		_y = (int)(tmpY + 0.5);
	}

	if (_x < 0 || _x >= m_img.cols || _y < 0 || _y >= m_img.rows)
		return -1;

	assert(m_img.type() == CV_16U || m_img.type() == CV_8U);
	double pixelValue;
	if(m_img.type() == CV_8U)
		pixelValue = m_img.at<unsigned char>(_y, _x) / 255.0;
	else 
		pixelValue = m_img.at<unsigned short>(_y, _x) / 65535.0;

	assert(pixelValue >= 0.0 && pixelValue <= 1.0);

	// For backward compatibility, values must be in [0, 255] range
	return pixelValue * 255.0;

	//int pi = img.pixelIndex (_x, _y);
	//if (pi >= 0 && pi < 256)
	//	return qRed ((unsigned int)img.color (pi));
	//else
	//	return -1;
}

QImage PictureContainer::getImageForDisplay() const
{
	if(!m_img.data)
		return QImage();

	QImage imgReturn(m_img.cols, m_img.rows, QImage::Format_Indexed8);
	imgReturn.setColorTable(Tools::getColorTableGrayIndexed8());		
	unsigned char* dst = imgReturn.bits();
	int bpl = imgReturn.bytesPerLine();
	for(int y = 0; y < m_img.rows; ++y) {
		for(int x = 0; x < m_img.cols; ++x) {
			float pixelValue;
			if(m_img.type() == CV_8U)
				pixelValue = m_img.at<unsigned char>(y, x) / 255.0f;
			else 
				pixelValue = m_img.at<unsigned short>(y, x) / 65535.0f;

			dst[y * bpl + x] = pixelValue * 255.0f;
		}
	}

	return imgReturn;
}

bool PictureContainer::applyBackgroundCorrectionAndReNormalize(const unsigned short* backgroundData, const unsigned short* gainData)
{
	if(imageBackgroundCorrected)
		return true;
	if((!m_img.data))
		return false;

	// Apply correction
	cv::Mat correctedData = Tools::applyBackgroundCorrection(m_img, backgroundData, gainData, true);
	if(!correctedData.data)
		return false;
	m_img = correctedData;
	imageBackgroundCorrected = true;
	
	return true;
}

