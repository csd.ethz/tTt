/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "picturearray.h"

#include "tttpositionmanager.h"
#include "tttgui/pictureview.h"
#include "tttdata/userinfo.h"
#include "tttio/fastdirectorylisting.h"
#include "tttio/pngwriter.h"

// Qt
#include <QMutexLocker>

// STL
#include <cassert>
#include <algorithm>

PictureArray::PictureArray (TTTPositionManager *_positionManager, unsigned int _size, QObject *parent, const char *name)
	: QObject (parent, name)/*, pictures(_size, 0)*/
{
	positionManager = _positionManager;
	
	// 01.03.2011-OH: changed pictures from Q3IntDict<PictureContainer> to QVector<PictureContainer*>
	// -> we can init it with _size NULL pointers (see above) without losing performance
	//pictures.setAutoDelete (true);
	//pictures.resize (_size);

	picturesLoaded = 0;
	firstLoadedPicture = -1;
	lastLoadedPicture = -1;

	SelectionCounter = 0;

	// Create image loading threads (use as many as physical cores but not less than 1 ore more than 10), make last one the main thread
	int numThreads = qMax(SystemInfo::getNumPhysicalCores(), 1);
	numThreads = qMin(10, numThreads);
	loadingThreads.resize(numThreads);
	QVector<ImageLoaderThread*> childThreads;
	childThreads.resize(numThreads - 1);
	for(int i = 0; i < numThreads; ++i) {
		loadingThreads[i] = new ImageLoaderThread(this);
		if(i < childThreads.size())
			childThreads[i] = loadingThreads[i];
	}
	loadingThreads[loadingThreads.size() - 1]->setMainThread(childThreads);

	// Init pictures (creates containers only)
	initializePictures();
}

void PictureArray::initializePictures()
{
	// Get path
	QDir path = positionManager->getPictureDirectory();
	if(!path.exists())
		return;

	// Create list of file extensions
	QStringList extensions;
	extensions << ".jpg" << ".tiff" << ".tif" << ".png";

	// Get unsorted file list
	fileNames = FastDirectoryListing::listFiles(path, extensions);
	if(fileNames.isEmpty())
		return;

	// Sort it
	fileNames.sort();

	// Images directory
	QString picDir = positionManager->getPictureDirectory();
	if(picDir.right(1) != "/")
		picDir += "/";

	// Get WL0 size
	QRect defaultSize;
	for(QStringList::iterator it = fileNames.begin(); it != fileNames.end(); ++it) {
		int waveLength = FileInfoArray::CalcWaveLengthFromFilename (*it);

		// Find a w0 image
		if(waveLength == 0) {
			QImage tmpImg(picDir + *it);
			if(!tmpImg.isNull()) {
				defaultSize = QRect(0, 0, tmpImg.width(), tmpImg.height());
				positionManager->positionInformation.setImageRect(0, defaultSize);
				break;
			}
		}
	}

	// No w0 image found
	if(defaultSize.isNull()) {
		return;
	}

	// Make room in array
	pictures.reserve(fileNames.size());
	indexHash.reserve(fileNames.size());

	// Determine min and max timepoints
	int maxTimePoint = 0;
	int minTimePoint = 0xFFFFFF;

	// Iterate over them
	for(QStringList::iterator it = fileNames.begin(); it != fileNames.end(); ) {
		// Extract wl and tp from filename
		int waveLength = FileInfoArray::CalcWaveLengthFromFilename (*it);
		int timePoint = FileInfoArray::CalcTimePointFromFilename (*it);
		int zIndex = FileInfoArray::CalcZIndexFromFileName(*it);
		if(zIndex < 0)
			zIndex = 1;

		// Silently ignore invalid files
		if(waveLength < 0 || timePoint <= 0) {
			it = fileNames.erase(it);
			continue;
		}

		// Calc absolute index
		qint64 absIndex = calcAbsIndex(timePoint, zIndex, waveLength);

		// DEBUG TEST
		int wlD = calcWavelength(absIndex);
		int tpD = calcTimePoint(absIndex);
		int zTest = calcZIndex(absIndex);
		assert(wlD == waveLength && tpD == timePoint && zTest == zIndex);

		// Check if image already exists (i.e. we have more picture files with the same wl and tp)
		if(indexHash.contains(absIndex)) {
			it = fileNames.erase(it);
			continue;
		}

		// Get min and max timepoint
		maxTimePoint = qMax(maxTimePoint, timePoint);
		minTimePoint = qMin(minTimePoint, timePoint);

		// Set available wavelengths of this position
		positionManager->setWavelengthAvailable (waveLength);

		// Set image rect if this has not been done yet for wavelength
		if(!positionManager->positionInformation.isImageRectSet(waveLength)) {
			// For this we have to open the image, which is expensive, but necessary
			QImage tmpImg(picDir + *it);
			if(!tmpImg.isNull()) 
				positionManager->positionInformation.setImageRect(waveLength, QRect(0, 0, tmpImg.width(), tmpImg.height()));
		}

		// Create picture container
		PictureContainer* tmp = new PictureContainer ( picDir, *it, PictureIndex (timePoint, zIndex, waveLength), false, defaultSize);
		pictures.append(QSharedPointer<PictureContainer>(tmp));

		// Update indexHash
		int arrayIndex = pictures.size() - 1;
		indexHash.insert(absIndex, arrayIndex);

		// Truncate basename in fileNames list
		int i = it->lastIndexOf("_t");
		if(i >= 0)
			*it = it->mid(i + 2);

		// Go to next
		++it;
	}

	// Set timepoints
	positionManager->setExperimentTimepoints(minTimePoint, maxTimePoint);

	// Available WLs have been set
	positionManager->setAvailableWavelengthsSet (true);
}

PictureArray::~PictureArray()
{
/*	//no autoDelete is set => must be deleted manually!
	for (unsigned int i = 0; i < Pictures.size(); i++) 
		if (Pictures.find (i)) {
			delete Pictures.find (i);
			Pictures.insert (i, 0);
		}*/
	//// 01.03.2011-OH: changed pictures from Q3IntDict to QVector => no auto delete exists
	//for(unsigned int i = 0; i < pictures.size(); ++i) {
	//	if(pictures[i]) {
	//		delete pictures[i];
	//		pictures[i] = 0;
	//	}
	//}

	// Tell all threads to stop if running
	for(int i = 0; i < loadingThreads.size(); ++i) {
		if(loadingThreads[i]->isRunning())
			loadingThreads[i]->cancelLoading();
	}

	// Delete gain images
	for(auto it = m_gainImages.begin(); it != m_gainImages.end(); ++it) {
		delete[] it.value();
	}

	// Wait for threads
	for(int i = 0; i < loadingThreads.size(); ++i) {
		if(loadingThreads[i]->isRunning()) {
			if(!loadingThreads[i]->wait(200))
				loadingThreads[i]->terminate();
		}
	}

	// Delete threads
	for(int i = 0; i < loadingThreads.size(); ++i) {
		delete loadingThreads[i];
	}
}

PictureIndex PictureArray::getFirstLoadedPictureIndex (int _waveLength) const
{
	// Check if any image is loaded
	if(firstLoadedPicture < 0)
		return PictureIndex();

	if(_waveLength == -1)
		// Just return index to first loaded picture
		return PictureIndex (pictures[firstLoadedPicture]->getTimePoint(), pictures[firstLoadedPicture]->getZIndex(), pictures[firstLoadedPicture]->getWaveLength());
	else {
		// Return index to first loaded picture of given wavelength
		for (int i = firstLoadedPicture; i <= lastLoadedPicture; i++) {
			if (pictures[i]->isLoaded()) {
				if(pictures[i]->getWaveLength() == _waveLength)
					return PictureIndex(pictures[i]->getTimePoint(), pictures[i]->getZIndex(), _waveLength);
			}
		}
	}

	// Nothing found
	return PictureIndex();

	//if (_waveLength == -1)
	//	return PictureIndex (calcTimePoint (firstLoadedPicture), calcWavelength (firstLoadedPicture));
	//else {
	//	for (int i = FirstLoadedPicture; i < LastLoadedPicture; i++)
	//		if (pictures[i])
	//			if (pictures[i]->isLoaded())
	//				if (calcWavelength (i) == _waveLength)
	//					return PictureIndex (calcTimePoint (i), calcWavelength (i));
	//}
	//return PictureIndex();	//empty
}

PictureIndex PictureArray::getLastLoadedPictureIndex (int _waveLength) const
{
	// Check if any image is loaded
	if(lastLoadedPicture < 0)
		return PictureIndex();

	if(_waveLength == -1)
		// Just return index to last loaded picture
		return PictureIndex (pictures[lastLoadedPicture]->getTimePoint(), pictures[lastLoadedPicture]->getZIndex(), pictures[lastLoadedPicture]->getWaveLength());
	else {
		// Return index to first loaded picture of given wavelength
		for (int i = lastLoadedPicture; i >= firstLoadedPicture; --i) {
			if (pictures[i]->isLoaded()) {
				if(pictures[i]->getWaveLength() == _waveLength)
					return PictureIndex(pictures[i]->getTimePoint(), pictures[i]->getZIndex(), _waveLength);
			}
		}
	}

	// Nothing found
	return PictureIndex();

	//if (_waveLength == -1)	
	//	return PictureIndex (calcTimePoint (LastLoadedPicture), calcWavelength (LastLoadedPicture));
	//else {
	//	//???
	//	//for (int i = LastLoadedPicture; i > FirstLoadedPicture; i--)
	//	int i = LastLoadedPicture;
	//	while (i > FirstLoadedPicture) {
	//		if (pictures[i])
	//			if (pictures[i]->isLoaded())
	//				if (calcWavelength (i) == _waveLength)
	//					return PictureIndex (calcTimePoint (i), calcWavelength (i));
	//		i--;
	//	}
	//	
	//}
	//return PictureIndex();	//empty
}

void PictureArray::init()
{
	loadedAtTimePoint.resize (positionManager->getLastTimePoint() + 1);		//a bit more than necessary, but security first!
	
	for (int i = 0; i < loadedAtTimePoint.size(); i++)
		loadedAtTimePoint [i] = 0;
	
}

//void PictureArray::initPicture (unsigned int _timepoint, int _waveLength, QString _filename, bool _loadingFlag, QRect _defaultSize)
//{
//	//uint tp = _index;
//
//	//// Get index
//	//int tmpIndex = calcAbsIndex (tp, _waveLength);
//	//static bool reportedIndexError = false;
//	//if(tmpIndex < 0 || tmpIndex > pictures.size()) {
//	//	// If this error occurs, this could mean, that the log file for this position is damaged and 
//	//	// therefore ttt has incorrect values for firstTimePoint or LastTimePoint, causing pictures array to have the wrong size
//
//	//	if(!reportedIndexError) {
//	//		reportedIndexError = true;
//	//		QString message = "tTt has encountered a serious error: Experiment meta data seems to be corrupted.\n\nPlease close tTt and make sure that the log files in all positions are valid.";
//	//		QMessageBox::critical(0, "Error", message);
//
//	//		qWarning() << "tTt warning: log-file error.";
//	//	}
//	//	return ;
//	//}
//
//	//_index = tmpIndex;
//
//	int absIndex = calcAbsIndex (_timepoint, _waveLength);
//	int arrayIndex = indexHash.value(absIndex, -1);
//	
//	//if the picture at index _index is already allocated, it needs not to be done again
//	if (arrayIndex >= 0) {
//		if (pictures[arrayIndex]->getFilename() == _filename) {
//			//if (pictures[_index]->isLoaded()) {
//			//	pictures[_index]->setLoadingFlag (_loadingFlag);
//			//	return;			//this picture needs not to be initialized again!
//			//}
//
//			// Mark picture to be loaded/unloaded depending on _loadingFlag
//			if(_loadingFlag != pictures[arrayIndex]->getLoadingFlag()) {
//				pictures[arrayIndex]->setLoadingFlag(_loadingFlag);
//			}
//
//			if(pictures[arrayIndex]->getSelectionFlag() != _loadingFlag) {
//				pictures[arrayIndex]->setSelection (_loadingFlag);
//
//				if(_loadingFlag)
//					SelectionCounter++;
//			}
//
//			return;
//		}
//	}
//	
//	//a new PictureContainer is created with the filename and loading flag
//	#ifdef DEBUGMODE
//	//SystemInfo::report ("init: " + QString ("%1, %2").arg (tp).arg (_waveLength));
//	#endif
//	
//    //pictures.insert (_index, new PictureContainer ( _filename, PictureIndex (tp, _waveLength), _loadingFlag/*, &positionManager->getDisplays().at (_waveLength)*/, _defaultSize));
//	
//	if(arrayIndex >= 0) {
//		//// Remove picture first
//		//delete pictures[_index];
//		//pictures[_index] = 0;
//
//		// .. but this should not happen anyways
//		static bool loggedThis = false;
//		if(!loggedThis) {
//			loggedThis = true;
//			qWarning() << "tTt: PictureArray::initPicture Warning 1.";
//		}
//	}
//
//	PictureContainer* tmp = new PictureContainer ( _filename, PictureIndex (_timepoint, _waveLength), _loadingFlag, _defaultSize);
//	pictures.append(QSharedPointer<PictureContainer>(tmp));
//	arrayIndex = pictures.size() - 1;
//	indexHash.insert(absIndex, arrayIndex);
//	
//	if (_loadingFlag) {
//		pictures[arrayIndex]->setSelection (true);
//		SelectionCounter++;
//	}
//}

//int PictureArray::loadPictureAbs (unsigned int _index)
//{
//	//if the picture _index was not even initialized (no filename with this timepoint),
//	//there need not be any attempt to load it
//	PictureContainer* picContainer = pictures[_index];
//	if (!picContainer)
//		return 0;
//	
//	int tmp = picContainer->loadImage();
//	if (tmp == 0) {												//picture was either successfully loaded or unloaded
//		
//		if (picContainer->getSelectionFlag()) {
//			SelectionCounter--;
//			picContainer->setSelection (false);
//		}
//		
//		if (picContainer->getLoadingFlag()) {							//picture was loaded 
//			PicturesLoaded++;
//			if ((int)_index > LastLoadedPicture) 
//				LastLoadedPicture = _index;
//			if ((int)_index < FirstLoadedPicture) 
//				FirstLoadedPicture = _index;
//			countLoadedPictures [calcWavelength (_index)]++;
//			
//			//if (Pictures.find (_index)->getSelectionFlag())
//			//	SelectionCounter--;
//			
//			return 1;
//		}
//		else {												//picture was unloaded
//			PicturesLoaded--;	
//			if ((int)_index == FirstLoadedPicture) updateBoundaries(true);
//			if ((int)_index == LastLoadedPicture) updateBoundaries(false);
//			countLoadedPictures [calcWavelength (_index)]--;
//			
//			//if (Pictures.find (_index)->getSelectionFlag())
//			//	SelectionCounter--;
//			
//			return 2;
//		}
//	}
//	else if(tmp == 1) {
//		stopLoading();
//
//		// Loading failed becaue we have run out of memory
//		QMessageBox::critical (0, "Allocation failed", "A serious error occurred: Memory allocation failed.\n\nPlease restart tTt to prevent loss of data.");
//	}
//	
//	return 0;
//}

int PictureArray::loadPictureAbs_ThreadSafe (int _index, bool _load)
{
	// Get PictureContainer
	if(_index < 0 || _index >= pictures.size())
		return 0;
	PictureContainer* picContainer = pictures[_index].data();

	// Load image
	int tmp = picContainer->loadImage(_load);
	if (tmp == 0) {												
		//picture was either successfully loaded or unloaded

		// The following code must be executed by only one thread, as it contains non-atomic operations
		QMutexLocker locker(&loadPictureAbsMutex);

		// Deselect picture
		if (picContainer->getSelectionFlag()) {
			SelectionCounter--;
			picContainer->setSelection (false);
		}


		if (_load) {							
			// Picture was loaded 
			picturesLoaded++;

			if (_index > lastLoadedPicture) 
				lastLoadedPicture = _index;
			if (_index < firstLoadedPicture || firstLoadedPicture == -1) 
				firstLoadedPicture = _index;
			countLoadedPictures [picContainer->getWaveLength()]++;

			return 1;	
		}
		else {												
			// Picture was unloaded
			picturesLoaded--;	

			// Update first and last loaded picture
			if (_index == firstLoadedPicture) 
				updateBoundaries(true);
			if (_index == lastLoadedPicture) 
				updateBoundaries(false);
			countLoadedPictures [picContainer->getWaveLength()]--;

			return 2;	
		}

	}
	else if(tmp == 1) {
		stopLoading();

		// Loading failed becaue we have run out of memory
		//QMessageBox::critical (0, "Allocation failed", "A serious error occurred: Memory allocation failed.\n\nPlease restart tTt to prevent loss of data."); <- cannot create mesasgebox in non gui thread
		qWarning() << "tTt Warning (critical): Allocation failed in loadPictureAbs_ThreadSafe()";
	}

	return 0;
}


void PictureArray::loadPictures (bool _loadAsynchronous, bool _unload)
{
	if(imagesToBeLoadedOrUnloaded.size() == 0)
		return;

	// Option overwrites parameters
	if (_loadAsynchronous) {
		if (UserInfo::getInst().getStyleSheet().getLoadPicturesAsynchronous())
			_loadAsynchronous = false;
	}
	
	// Check if we only have one thread (single core) or are unloading pictures (very fast anyways, no multi threading needed) or only few images are selected
	if(loadingThreads.size() == 1 || _unload || imagesToBeLoadedOrUnloaded.size() < 5) {
		// Use one thread
		loadingThreads[0]->setNextRunParams(_unload, !_loadAsynchronous);
		loadingThreads[0]->start();
	}
	else {
		// Use all threads
		for(int i = 0; i < loadingThreads.size(); ++i) {
			loadingThreads[i]->setNextRunParams(_unload, !_loadAsynchronous);
			loadingThreads[i]->start();
		}
	}

	/*
	if(!loadingThread2 || _unload) {
		// Only one thread -> no problem
		loadingThread1->setNextRunParams(indexStart, indexEnd, _unload, !_loadAsynchronous);
		loadingThread1->start();
	}
	else {
		// To make this thread safe, we must divide the data in such a way,
		// that each thread operates on different timepoints (not only different indices)
		int middleTimePoint = (_startTimepoint + _endTimepoint) / 2;
		qint64 middleIndex = calcAbsIndex (middleTimePoint, MAX_ZINDEX, MAX_WAVE_LENGTH);

		// Init threads
		loadingThread1->setNextRunParams(indexStart, middleIndex, _unload, !_loadAsynchronous);
		loadingThread2->setNextRunParams(middleIndex+1, indexEnd, _unload, !_loadAsynchronous);

		// Start threads
		loadingThread1->start();
		loadingThread2->start();
	}
	*/ 

	// Wait for loading to complete
	if(!_loadAsynchronous) {
		for(int i = 0; i < loadingThreads.size(); ++i) 
			loadingThreads[i]->wait();
	}
}

void PictureArray::stopLoading()
{
	// Inform threads
	for(int i = 0; i < loadingThreads.size(); ++i)
		loadingThreads[i]->cancelLoading();
}

void PictureArray::setLoading (unsigned int _timepoint, int _zIndex, int _wavelength, bool _load)
{
	// Get wavelength and z-index range
	int startWL = _wavelength, 
		stopWL = _wavelength;
	if (_wavelength == -1) {
		startWL = 0;
		stopWL = MAX_WAVE_LENGTH;
	}
	int zStart = _zIndex,
		zStop = _zIndex;
	if(_zIndex == -1) {
		zStart = 0;
		zStop = MAX_ZINDEX;
	}
	
	// Set loading flags
	for(int zIndex = zStart; zIndex <= zStop; ++zIndex) {
		for (int curWl = startWL; curWl <= stopWL; curWl++) {
			// Map tp and wl to array index
			qint64 absIndex = calcAbsIndex (_timepoint, zIndex, curWl);
			int arrayIndex = indexHash.value(absIndex, -1);
		
			if (arrayIndex >= 0) {
				// Add image to array if not already loaded / unloaded
				bool loaded = pictures[arrayIndex]->isLoaded();
				if((_load && !loaded) || (!_load && loaded))
					imagesToBeLoadedOrUnloaded.push_back(arrayIndex);
			}
		}
	}
}

void PictureArray::setSelection (unsigned int _timepoint, int _zIndex, int _wavelength, bool _value)
{
	// Get wavelength and z-index range
	int startWL = _wavelength, 
		stopWL = _wavelength;
	if (_wavelength == -1) {
		startWL = 0;
		stopWL = MAX_WAVE_LENGTH;
	}
	int zStart = _zIndex,
		zStop = _zIndex;
	if(_zIndex != -1) {
		zStart = _zIndex;
		zStop = _zIndex;
	}

	// Set selection flags
	for(int zIndex = zStart; zIndex <= zStop; ++zIndex) {
		for (int curWl = startWL; curWl <= stopWL; curWl++) {
			// Map tp and wl to array index
			qint64 absIndex = calcAbsIndex (_timepoint, zIndex, curWl);
			int arrayIndex = indexHash.value(absIndex, -1);

			if (arrayIndex >= 0) {
				// Check if selection flag is different from current
				if(_value != pictures[arrayIndex]->getSelectionFlag()) {
					pictures[arrayIndex]->setSelection (_value);
					if (_value)
						SelectionCounter++;
					else
						SelectionCounter--;
				}
			}
		}
	}

	//unsigned int tmpIdx = _number;
	//bool oldValue = false;
	//
	//for (int i = start; i <= stop; i++) {
	//	_number = calcAbsIndex (tmpIdx, i);
	//	
	//	if (pictures[_number]) {
	//		oldValue = pictures[_number]->getSelectionFlag();
	//		if (_value != oldValue) {
	//			pictures[_number]->setSelection (_value);
	//			if (_value)
	//				SelectionCounter++;
	//			else
	//				SelectionCounter--;
	//		}
	//	}
	//}
}

bool PictureArray::getLoadingFlag (unsigned int _timePoint, int _zIndex, int _waveLength) const
{
	// Map tp and wl to array index
	qint64 absIndex = calcAbsIndex (_timePoint, _zIndex, _waveLength);
	int arrayIndex = indexHash.value(absIndex, -1);

	// Return loading flag if image exists
	if (arrayIndex >= 0) 
		return std::find(imagesToBeLoadedOrUnloaded.begin(), imagesToBeLoadedOrUnloaded.end(), arrayIndex) != imagesToBeLoadedOrUnloaded.end();
	
	return false;

	//if (pictures[_number])
	//	return pictures[_number]->getLoadingFlag();
	//else
	//	return false;
}

bool PictureArray::getSelectionFlag (unsigned int _timePoint, int _zIndex, int _waveLength) const
{
	// Map tp and wl to array index
	qint64 absIndex = calcAbsIndex (_timePoint, _zIndex, _waveLength);
	int arrayIndex = indexHash.value(absIndex, -1);

	// Return selection flag if image exists
	if (arrayIndex >= 0) 
		return pictures[arrayIndex]->getSelectionFlag();

	return false;

	//_number = calcAbsIndex (_number, _waveLength);
	//
	//if (pictures[_number])
	//	return pictures[_number]->getSelectionFlag();
	//else
	//	return false;
}

//bool PictureArray::drawPicture (int _timepoint, int _wavelength, QPaintDevice *_pd, int _colorChannel, QImage *_img, bool _complete) const
//{
//	int _index = calcAbsIndex (_timepoint, _wavelength);
//
//	if (_index > (int)Pictures.size())
//		return false;
//
//	if (Pictures.find (_index)) {
//		return Pictures.find (_index)->drawPixmap (_pd, _colorChannel, _img, _complete);
//	}
//	else
//		return false;		//drawing failed
//}

qint64 PictureArray::nextLoadedPictureArrayIndex(qint64 _arrayIndex, bool _forward, int _zIndex, int _waveLength) const
{
	// Determine step (direction)
	int step;
	if(_forward)
		step = 1;
	else
		step = -1;
	
	// Search (Note: do not use firstLoadedPicture and lastLoadedPicture as boundaries, as this function is used to update them!)
	for(qint64 curIndex = _arrayIndex + step; curIndex >= 0 && curIndex < pictures.size(); curIndex += step) {
		// Check if pic is loaded
		if (pictures[curIndex]->isLoaded()) {
			// Check wl if necessary
			if((_waveLength < 0 || _waveLength == pictures[curIndex]->getWaveLength()) &&
				(_zIndex < 0 || _zIndex == pictures[curIndex]->getZIndex()))
				return curIndex;

			/*
			if (_waveLength >= 0) {
				if (pictures[curIndex]->getWaveLength() == _waveLength)
					return curIndex;
			}
			else
				return curIndex;
				*/
		}
	}

	// Nothing found
	return -1;

	//int picIndex;
	//
	//if (_forward)
	//	picIndex = _arrayIndex + 1;
	//else
	//	picIndex = _arrayIndex - 1;
	//
	//while ((picIndex < (int)pictures.size()) && (picIndex >= 0))  {
	//	if (pictures[picIndex])
	//		if (pictures[picIndex]->isLoaded()) {
	//			if (_waveLengthConstant) {
	//				if (calcWavelength (picIndex) == calcWavelength (_arrayIndex))
	//					return picIndex;
	//			}
	//			else
	//				return picIndex;
	//		}
	//		
	//	//no picture found, try next one (forward or backward)
	//	if (_forward)
	//		++picIndex;
	//	else 
	//		--picIndex;
	//}

	//return -1;				//if there is no next/previous picture in memory, -1 is returned
}

PictureIndex PictureArray::nextLoadedPicture (const PictureIndex &_pi, bool _forward, bool _waveLengthConstant, bool _zIndexConstant) const
{
	// Check if pictures are loaded
	if(firstLoadedPicture == -1)
		return PictureIndex();

	// Get absolute index
	qint64 absoluteIndex = calcAbsIndex (_pi.TimePoint, _pi.zIndex, _pi.WaveLength);

	// Determine step
	int step;
	if(_forward)
		step = 1;
	else
		step = -1;

	// Determine minimal and maximal absolute index of loaded pictures
	qint64 maxAbsoluteIndex = calcAbsIndex(pictures[lastLoadedPicture]->getTimePoint(), pictures[lastLoadedPicture]->getZIndex(), pictures[lastLoadedPicture]->getWaveLength());
	qint64 minAbsoluteIndex = calcAbsIndex(pictures[firstLoadedPicture]->getTimePoint(), pictures[firstLoadedPicture]->getZIndex(), pictures[firstLoadedPicture]->getWaveLength());

	// Calc start index
	qint64 curIndex = absoluteIndex + step;

	// Check if start index is already at or behind min/max index
	if((curIndex >= maxAbsoluteIndex && _forward) || (curIndex <= minAbsoluteIndex && !_forward))
		return PictureIndex();

	// Picture index could be e.g. minAbsoluteIndex - 1 which is valid if _forward==true
	curIndex = qMax(curIndex, minAbsoluteIndex);
	curIndex = qMin(curIndex, maxAbsoluteIndex);

	// Find existing image
	for(; curIndex >= minAbsoluteIndex && curIndex <= maxAbsoluteIndex; curIndex += step) {
		// Check if a picture container exists for curIndex
		int arrayIndex = indexHash.value(curIndex, -1);
		if(arrayIndex >= 0) {
			// Check if pic is loaded
			if(pictures[arrayIndex]->isLoaded()) {
				// Check wavelength and z_index if necessary
				if((!_waveLengthConstant || pictures[arrayIndex]->getWaveLength() == _pi.WaveLength) &&
					(!_zIndexConstant || pictures[arrayIndex]->getZIndex() == _pi.zIndex))
					return PictureIndex(pictures[arrayIndex]->getTimePoint(), pictures[arrayIndex]->getZIndex(), pictures[arrayIndex]->getWaveLength());
				/*
				if(_waveLengthConstant) {
					if(pictures[arrayIndex]->getWaveLength() == _pi.WaveLength)
						return PictureIndex(pictures[arrayIndex]->getTimePoint(), pictures[arrayIndex]->getWaveLength());
				}
				else
					return PictureIndex(pictures[arrayIndex]->getTimePoint(), pictures[arrayIndex]->getWaveLength());
					*/
			}
			
			// Pic is not loaded or wl is incorrect, but now we can use nextLoadedPictureArrayIndex as we have a valid array index starting point
			arrayIndex = nextLoadedPictureArrayIndex(arrayIndex, _forward, _zIndexConstant ? _pi.zIndex : -1, _waveLengthConstant ? _pi.WaveLength : -1);
			
			// Check if something was found
			if(arrayIndex >= 0) 
				return PictureIndex(pictures[arrayIndex]->getTimePoint(), pictures[arrayIndex]->getZIndex(), pictures[arrayIndex]->getWaveLength());
			else
				return PictureIndex();
		}
	}

	// Nothing found
	return PictureIndex();

 //	int arrayIndex = calcAbsIndex (_pi.TimePoint, _pi.WaveLength);
	//int ret = nextLoadedPictureAbs (arrayIndex, _forward, _waveLengthConstant);
	//
	//if (ret == -1)
	//	return PictureIndex();		
	//else
	//	return PictureIndex (calcTimePoint (ret), calcWavelength (ret));
}

void PictureArray::updateBoundaries (bool _first)
{
	if(_first) {
		firstLoadedPicture = nextLoadedPictureArrayIndex(firstLoadedPicture, true);
	}
	else {
		if(lastLoadedPicture == -1)
			// Search from behind end
			lastLoadedPicture = nextLoadedPictureArrayIndex(pictures.size(), false);
		else
			lastLoadedPicture = nextLoadedPictureArrayIndex(lastLoadedPicture, false);
	}

	//if (_first)
	//	FirstLoadedPicture = nextLoadedPictureAbs (FirstLoadedPicture, true);
	//else
	//	LastLoadedPicture = nextLoadedPictureAbs (LastLoadedPicture, false);
}

//WARNING: might be slow!!
//if the wavelength in _picIndex == -1, any is meant
int PictureArray::getAbsIndexLoaded (PictureIndex _picIndex) const
{
	int counter = 0;
	bool anyWL = (_picIndex.WaveLength == -1);
	
	for (int i = 0; i < pictures.size(); i++)  {
		// Check if image has right timepoint
		if (_picIndex.TimePoint == pictures[i]->getTimePoint()) {
			// Check wl if necessary
			if (anyWL) {
				// Found match
				if (pictures[i]->isLoaded())
					return counter;
						
				//if (_picIndex.WaveLength < MAX_WAVE_LENGTH - 1)
				//	++_picIndex;
				//else {
				//	return -1;
				//}
			}
			else {
                if (_picIndex.WaveLength == pictures[i]->getWaveLength()) {
                    // found the picture with the matching wavelength
					if (pictures[i]->isLoaded()) 
						return counter;
					else {
                        // picture with tp/wl is not loaded
						return -1;
					}
				}
			}
		}
		
		if (pictures[i]->isLoaded()) 
				counter++;
			
	}
	
	// Not found
	return -1;
}

////WARNING: might be slow!!
//PictureIndex PictureArray::getPictureIndexLoaded (int _absIndexLoaded) const
//{
//	int counter = -1;
//	
//	int i = 0;
//	for ( ; i < (int)pictures.size(); i++) {
//		if (pictures[i])
//			if (pictures[i]->isLoaded())
//				counter++;
//		
//		if (counter == _absIndexLoaded)
//			break;
//	}
//	
//	return calcPI (i);
//}

int PictureArray::countLoaded (int _timepoint) const
{
	/*
	Why count manually when we have this information?

	int counter = 0;

	// Count images for given _timepoint
	for(int wl = 0; wl <= MAX_WAVE_LENGTH; ++wl) {
		int absIndex = calcAbsIndex (_timepoint, wl);
		int arrayIndex = indexHash.value(absIndex, -1);
		if(arrayIndex >= 0 && pictures[arrayIndex]->isLoaded())
			++counter;
	}

	return counter;
	*/

	// Return 0 if index is invalid
	if(_timepoint < 0 || _timepoint >= loadedAtTimePoint.size())
		return 0;

	return loadedAtTimePoint[_timepoint];

	////if _wavelength == -1, true is returned if any picture at that timepoint is loaded
	////otherwise true is only returned if the selected picture is loaded!
	//int idx = calcAbsIndex (_timepoint, 0);

	//// Check if index is < 0 (this probably means that _timepoint is before 
	//// the first timepoint of this position, so there cannot be any loaded pictures at _timepoint)
	//if(idx < 0)
	//	return 0;

	//int counter = 0;
	//for (int i = idx ; i <= idx + MAX_WAVE_LENGTH; i++) {
	//	if (pictures[i])
	//		if (pictures[i]->isLoaded())
	//			counter++;
	//}
	//return counter;
}

bool PictureArray::isLoaded (int _timepoint, int _wavelength, int _zIndex) const
{
	assert((_wavelength == -1 && _zIndex == -1) || (_wavelength != -1 && _zIndex != -1));

	// Check if any wavelength is ok
	if (_wavelength == -1 && _zIndex == -1)
		return (countLoaded (_timepoint) > 0);

	// Map to array index and check if image is loaded if exists
	qint64 absIndex = calcAbsIndex (_timepoint, _zIndex, _wavelength);
	int arrayIndex = indexHash.value(absIndex, -1);
	if(arrayIndex >= 0)
		return pictures[arrayIndex]->isLoaded();

	return false;

	//if (_wavelength == -1)
	//	return (countLoaded (_timepoint) > 0);
	//else {
	//	int idx = calcAbsIndex (_timepoint, _wavelength);
	//	if (pictures[idx])
	//		if (pictures[idx]->isLoaded())
	//			return true;	
	//}
	//return false;
}

int PictureArray::getLoadedPictures (int _waveLength) const
{
	if (_waveLength == -1)
		return picturesLoaded;
	else {
		//return the number of loaded pictures for _waveLength
		if (_waveLength <= MAX_WAVE_LENGTH)
			return countLoadedPictures [_waveLength];
		else
			return -1;
	}
}

int PictureArray::getMaxCountLoaded() const
{
	int cc = 0;
	
	//for (int i = 0; i < MAX_WAVE_LENGTH; i++) 
	//	if (countLoadedPictures [i] > _max) _max = countLoadedPictures [i];
	for (int i = positionManager->getFirstTimePoint(); i <= positionManager->getLastTimePoint() && i < loadedAtTimePoint.size(); i++)
		if (loadedAtTimePoint [i] > 0)
			cc++;
	
	return cc;
}

int PictureArray::getTimePointIndex (int _timepoint) const
{
	int counter = -1;
	int i = positionManager->getFirstTimePoint();
	for ( ; i < (long)loadedAtTimePoint.size(); i++)
		if (i <= _timepoint) {
			if (loadedAtTimePoint [i] > 0)
				counter++;
		}
		else
			break;
			
	return counter;
}

int PictureArray::getTimePoint (int _timepointIndex) const
{
	int counter = -1;
	for (int i = positionManager->getFirstTimePoint(); i <= positionManager->getLastTimePoint(); i++) {
		if (loadedAtTimePoint [i] > 0)
			counter++;
		if (counter == _timepointIndex)
			return i;
	}
	
	return -1;
}

int PictureArray::readPixel (int _timePoint, int _zIndex, int _waveLength, int _x, int _y) const
{
	qint64 idx = calcAbsIndex (_timePoint, _zIndex, _waveLength);
	int arrayIndex = indexHash.value(idx, -1);
	
	if (arrayIndex >= 0)
		return pictures[arrayIndex]->readPixel (_x, _y);
	else
		return -1;
}

const PictureContainer* PictureArray::getPictureContainer (int _timePoint, int _zIndex, int _waveLength) const
{
	if(_zIndex < 0)
		_zIndex = 1;
	qint64 idx = calcAbsIndex (_timePoint, _zIndex, _waveLength);
	int arrayIndex = indexHash.value(idx, -1);
	
	if (arrayIndex >= 0)
		return pictures[arrayIndex].data();
	else
		return 0;
}

void PictureArray::unloadAllPictures (/*bool _loadSynchronous*/)
{
	/*
	//set all loading flags to false
	int ftp = positionManager->getFirstTimePoint();
	int ltp = positionManager->getLastTimePoint();
	
	for (int tp = ftp; tp <= ltp; tp++)
		//setSelection (tp, -1, false);
		setLoading (tp, -1, -1, false);
		*/

	// Mark all loaded images to be unloaded
	for(int i = 0; i < pictures.size(); ++i) {
		if(pictures[i]->isLoaded())
			imagesToBeLoadedOrUnloaded.push_back(i);
	}
	
	// Execute unload
	loadPictures (false, true);
}

bool PictureArray::loadInterval (int _tp1, int _tp2, int _wl, int _z, int _picture_interval, bool _unloadOthers)
{
	if (_unloadOthers)
		unloadAllPictures (/*false*/);
	
	for (int tp = _tp1; tp <= _tp2; tp += _picture_interval)
		setLoading (tp, _z, _wl, true);
	
	loadPictures (false, false);

	return true;
}

//void PictureArray::setSmoothScale (bool /*_smoothScale*/)
//{
//	for (uint i = 0 ; i < Pictures.size(); i++)
//		if (Pictures.find (i))
//			Pictures.find (i)->setSmoothScale (_smoothScale);
//}

bool PictureArray::pictureExists (int _timepoint, int _zIndex, int _wavelength)
{
	assert(_zIndex > -1);
	if (_wavelength >= 0) {
		// Map to array index
		qint64 idx = calcAbsIndex (_timepoint, _zIndex, _wavelength);
		int arrayIndex = indexHash.value(idx, -1);
		return arrayIndex >= 0;
	}
	else {
		for (int wl = 0; wl <= MAX_WAVE_LENGTH; wl++) {
			// Map to array index
			qint64 idx = calcAbsIndex (_timepoint, _zIndex, wl);
			if (indexHash.value(idx, -1) >= 0)
				return true;
		}
	}
	
	return false;
}

bool PictureArray::loadOnePicture( const PictureIndex& _index )
{
	// Check if loading is in progress
	for(int i = 0; i < loadingThreads.size(); ++i) {
		if(loadingThreads[i]->isRunning())
			return false;
	}

	// Get array index
	qint64 absIndex = calcAbsIndex(_index.TimePoint, _index.zIndex, _index.WaveLength);
	int arrayIndex = indexHash.value(absIndex, -1);

	// Get container
	if(arrayIndex >= 0) {
		PictureContainer* pc = pictures[arrayIndex].data();

		// Check if it is already loaded
		if(pc->isLoaded())
			return true;

		// Load it
		if(pc->loadImage(true) == 0) {
			// Loading was successful
			++picturesLoaded;

			// Deselect picture if it is selected
			if(pc->getSelectionFlag()) {
				pc->setSelection(false);
				--SelectionCounter;
			}

			// Update array boundaries
			if (arrayIndex > lastLoadedPicture) 
				lastLoadedPicture = arrayIndex;
			if (arrayIndex < firstLoadedPicture || firstLoadedPicture == -1) 
				firstLoadedPicture = arrayIndex;

			// Update loadedAtTimePoint and countLoadedPictures
			loadedAtTimePoint[_index.TimePoint] = loadedAtTimePoint[_index.TimePoint] + 1;
			++countLoadedPictures [_index.WaveLength];

			return true;
		}
	}

	// Something went wrong
	return false;
}

int PictureArray::popImageForLoadingOrUnloading()
{
	QMutexLocker lock(&getPictureForLoadingMutex);

	// Check if we have anything to be loaded
	if(imagesToBeLoadedOrUnloaded.size() == 0)	
		return -1;
	
	// Take first element
	//int ret = imagesToBeLoadedOrUnloaded[imagesToBeLoadedOrUnloaded.size()-1];
	//imagesToBeLoadedOrUnloaded.pop_back();
	return imagesToBeLoadedOrUnloaded.takeFirst();
}

int PictureArray::applyBackgroundCorrection( const PictureIndex& pi/*, const QString& bgPath*/ /*= ""*/ )
{
	int zIndex = pi.zIndex;
	if(zIndex < 0)
		zIndex = 1;

	// Check if image exists
	qint64 absIndex = calcAbsIndex(pi.TimePoint, zIndex, pi.WaveLength);
	int arrayIndex = indexHash.value(absIndex, -1);
	if(arrayIndex < 0 || arrayIndex >= pictures.size())
		return 3;

	// Check if image is loaded
	PictureContainer* pc = pictures[arrayIndex].data();
	if(!pc->isLoaded())
		return 4;

	// Check if already done
	if(pc->backgroundCorrected())
		return 1;

	// Open gain image
	const unsigned short* gainImageData = getGainImage(pi.WaveLength, true);
	if(!gainImageData)
		return 5;

	// Open background image
	QString path = positionManager->getBackgroundDirectory() + "/" + pc->getFilename();
	unsigned short* backGroundImage = 0;
	int bgrBitDepth;
	unsigned int width, height;
	if(openPngGrayScaleImage(QDir::toNativeSeparators(path).toLatin1(), (unsigned char**)&backGroundImage, bgrBitDepth, width, height, true) != 0) {
		delete[] backGroundImage;
		return 2;
	}
	if(width != pc->getWidth() || height != pc->getHeight()) {
		delete[] backGroundImage;
		return 2;
	}

	// Run correction
	if(!pc->applyBackgroundCorrectionAndReNormalize(backGroundImage, gainImageData)) {
		delete[] backGroundImage;
		return 6;
	}

	// Success
	delete[] backGroundImage;

	return 0;
}

bool PictureArray::backgroundCorrectionApplied( const PictureIndex& pi )
{
	// Get array index and check value in PictureContainer
	qint64 absIndex = calcAbsIndex(pi.TimePoint, pi.zIndex, pi.WaveLength);
	int arrayIndex = indexHash.value(absIndex, -1);
	if(arrayIndex >= 0 && pictures[arrayIndex])
		return pictures[arrayIndex]->backgroundCorrected();
	return false;
}

const unsigned short* PictureArray::getGainImage( int wl, bool loadIfNecessary )
{
	// Check if exists
	unsigned short* data = m_gainImages.value(wl, 0);
	if(data)
		return data;

	// Does not exist
	if(!loadIfNecessary)
		return 0;

	//// Try to load
	//QString path = QString("%1/gain_w%2.png").arg(positionManager->getBackgroundDirectory()).arg(wl);
	//int bitDepth;
	//unsigned int width, height;
	//if(openPngGrayScaleImage(QDir::toNativeSeparators(path).toLatin1(), (unsigned char**)&data, bitDepth, width, height, true) != 0) {
	//	delete[] data;
	//	return 0;
	//}

	// Try to load
	QString path = QString("%1/gain_w%2.png").arg(positionManager->getBackgroundDirectory()).arg(wl, 2, 10, QChar('0'));
	int bitDepth;
	unsigned int width, height;
	if(openPngGrayScaleImage(QDir::toNativeSeparators(path).toLatin1(), (unsigned char**)&data, bitDepth, width, height, true) != 0) {
		delete[] data;
		return 0;
	}

	// Success
	m_gainImages.insert(wl, data);
	return data;
}



//bool PictureArray::setData( const QModelIndex& index, const QVariant& value, int role /*= Qt::EditRole */ )
//{
//	// Only CheckState events will be handled by us
//	if(role == Qt::CheckStateRole) {
//		// Get row and value
//		int row = index.row();
//		int val = value.toInt();
//
//		if(row < 0 || row >= pictures.size())
//			return false;
//
//		// Save checked state
//		if(val == Qt::Checked)
//			pictures[row]->setSelection(true);
//		else
//			pictures[row]->setSelection(false);
//
//		// Data has been changed successfully
//		//emit dataChanged(index, index);		// <-- this *seems* to be unneccesary (in qt 4.7.2) and takes like 2 seconds of time (causes whole list to be redrawn)..
//		emit itemSelectionChanged();
//
//		return true;
//	}
//
//	return false;
//}
//
//QVariant PictureArray::data( const QModelIndex& index, int role /*= Qt::DisplayRole */ ) const
//{
//	// Get row
//	unsigned int row = index.row();
//	if(row < 0 || row >= pictures.size())
//		return QVariant();
//
//	if(role == Qt::DisplayRole) {
//		return pictures[row]->getFilename();
//	}
//	else if(role == Qt::CheckStateRole) {
//		// Handle CheckState events for checkboxes
//		if(pictures[row]->getLoadingFlag())
//			return QVariant(Qt::Checked);
//		else
//			return QVariant(Qt::Unchecked);
//	} 
//	else if(role == Qt::TextColorRole) {
//		// Handle TextColorRole events to enable wl-specific coloring in listview
//
//		// Get pic name
//		QString picName = pictures[row]->getFilename();
//		if(!picName.isEmpty()) {
//			// Extract wl
//			int wl = FileInfoArray::CalcWaveLengthFromFilename(picName);
//
//			// Return color if wl is valid
//			if (wl > 0 && wl <= MAX_WAVE_LENGTH)
//				return QVariant(WAVELENGTHCOLORS [wl]);
//		}
//
//		// Return black as default
//		return QVariant(Qt::black);
//	}
//
//	// Have no value
//	return QVariant();
//}
//
//Qt::ItemFlags PictureArray::flags( const QModelIndex& index ) const
//{
//	// Indicate for all items that they can be checked
//	return Qt::ItemIsUserCheckable | Qt::ItemIsEnabled;
//}





void ImageLoaderThread::run()
{
	// To prevent very bad things from happening
	if(loadingInProgess)
		return;
	
	abortLoading = false;
	loadingInProgess = true;

	// Load images
	int actuallyLoadedPicsCount = 0;
	while(!abortLoading) {
		// Check if enough memory
		if(!unload && actuallyLoadedPicsCount % 10 == 0) {
			if(SystemInfo::shortOfMemory()) {
				//if there are less than MINIMUM_RAM MB of physical RAM free 
				// a signal is sent - then the receiver class has to decide what to do
				emit RAMFull();

				// Stop loading
				break;
			}
		}

		// Get next image
		int imageToLoad = parentPicArray->popImageForLoadingOrUnloading();
		if(imageToLoad == -1)
			break;
		int tp = parentPicArray->pictures[imageToLoad]->getTimePoint();

		// Try to load picture at index
		int loadStatus = parentPicArray->loadPictureAbs_ThreadSafe(imageToLoad, !unload);

		// Update parentPicArray->loadedAtTimePoint
		if(loadStatus == 1) {
			// Image was loaded
			QMutexLocker lock(&parentPicArray->loadedAtTimePointMutex);
			parentPicArray->loadedAtTimePoint[tp] = parentPicArray->loadedAtTimePoint[tp] + 1;
		}
		else if(loadStatus == 2) {
			// Image was deleted
			QMutexLocker lock(&parentPicArray->loadedAtTimePointMutex);
			parentPicArray->loadedAtTimePoint[tp] = parentPicArray->loadedAtTimePoint[tp] - 1;
		}

		// If picture was actually loaded..
		if(loadStatus > 0) {
			actuallyLoadedPicsCount++;

			// Make sure not to flood gui thread with LoadedPicture signals as signals
			// over threads are more expensive and can easily kill the gui-thread event handler.
			if(!loadSynchronous && (actuallyLoadedPicsCount % 25) == 0)
				emit LoadedPicture (tp, -1, true);
		}
	}

	/*
	// Iterate over indices
	for(int curIndex = startIndex; !abortLoading && curIndex <= stopIndex; curIndex++) {
		if(!unload && curIndex % 10 == 0) {
			if(SystemInfo::shortOfMemory()) {
				// Stop other thread
				if(other)
					other->cancelLoading();
				
				//if there are less than MINIMUM_RAM MB of physical RAM free (without swap!), 
				// a signal is sent - then the receiver class has to decide what to do
				emit RAMFull();
				
				// Stop loading
				break;
			}
		}

		// Try to load picture at index (will only be loaded if flagged as to-be-loaded in pic container)
		int loadStatus = parentPicArray->loadPictureAbs_ThreadSafe(curIndex);

		// Get timepoint for current index
		int tp = parentPicArray->calcTimePoint (curIndex);

		
		//parentPicArray->loadedAtTimePoint [tp] = (char)parentPicArray->countLoaded (tp);
		if(loadStatus == 1) {
			// Lock
			QMutexLocker lock(&parentPicArray->loadedAtTimePointMutex);

			// Image was loaded
			parentPicArray->loadedAtTimePoint[tp] = parentPicArray->loadedAtTimePoint[tp] + 1;
		}
		else if(loadStatus == 2) {
			// Lock
			QMutexLocker lock(&parentPicArray->loadedAtTimePointMutex);

			// Image was deleted
			parentPicArray->loadedAtTimePoint[tp] = parentPicArray->loadedAtTimePoint[tp] - 1;
		}

		// If picture was actually loaded..
		if(loadStatus > 0) {
			actuallyLoadedPicsCount++;

			// Make sure not to flood gui thread with LoadedPicture signals as signals
			// over threads are more expensive and can easily kill the gui-thread event handler.
			if(!loadSynchronous && (actuallyLoadedPicsCount % 25) == 0)
				emit LoadedPicture (tp, -1, true);
		}
	}
	*/

	// Make sure to emit this signal after the last picture has been loaded
	if(actuallyLoadedPicsCount > 0)
		emit LoadedPicture (-1, -1, true);

	// If this is the main loading thread
	if(mainLoadingThread) {
		// Wait for the others
		for(int i = 0; i < childThreads.size(); ++i)
			childThreads[i]->wait();

		// Now all pictures have been loaded
		emit LoadingComplete();
	}
	
	loadingInProgess = false;
	abortLoading = false;

	//// Debug
	//qDebug() << isMainThread << ": Finished.";
}

ImageLoaderThread::ImageLoaderThread( PictureArray* _parent)
{
	// Set params
	parentPicArray = _parent;
	loadingInProgess = false;
	abortLoading = false;
	unload = false;
	mainLoadingThread = false;
	loadSynchronous = false;

	// Connect signals to signals of pic array
	connect(this, SIGNAL (LoadingComplete()), _parent, SIGNAL(LoadingComplete()));
	connect(this, SIGNAL (LoadedPicture(unsigned int, int, bool)), _parent, SIGNAL(LoadedPicture(unsigned int, int, bool)));
	connect(this, SIGNAL (RAMFull()), _parent, SIGNAL(RAMFull()));
}

void ImageLoaderThread::setNextRunParams( bool _unload, bool _loadSynchronous)
{
	if(loadingInProgess)
		return;

	unload = _unload;
	loadSynchronous = _loadSynchronous;
}

void ImageLoaderThread::cancelLoading()
{
	abortLoading = true;
}

void ImageLoaderThread::setMainThread( const QVector<ImageLoaderThread*> _childThreads )
{
	// This is now the main loading thread
	mainLoadingThread = true;
	childThreads = _childThreads;
}
