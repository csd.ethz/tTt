/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef COMPILEFLAGS_H
#define COMPILEFLAGS_H

/**
	This document/class stores the compiler flags, telling which modules to include into the executable.
	This is very useful for generating different versions, including a custom set of features.
	Thus, for each feature/tool/method, there is (or should be) a flag setting whether it will be included
	  in the executable or not.
	To be able to manage that function here, each procedure/button/... that belongs to a module (exclusively) needs to
	  check before its execution whether its own flag is set to true!
	
	NOTE: 	There is a difference between not compiling a module or simply not executing or hiding, resp.!
		The first option is only available with preprocessor directives, the second, as used here, with boolean constants.
	
*/

///@ todo: read from XML file? rather not, because...
///but note: it is better to have features not compiled at all than offering the possibility to change the flags at runtime!


//tool to load pictures with a right mouse click on selected cells
const bool COMPFLAG_LOAD_PICTURES_POSITONS = true;

//context menu in the tree window on a set of selected cells
const bool COMPFLAG_CELL_CONTEXT_MENU = true;

//style editor in general, and below...
const bool COMPFLAG_STYLESETTINGS = true;
//...tree layout
const bool COMPFLAG_STYLESETTING_TREE_LAYOUT = true;
//...movie layout
const bool COMPFLAG_STYLESETTING_MOVIE_LAYOUT = true;
//...tracking key layout
const bool COMPFLAG_STYLESETTING_TRACKING_KEYS = true;
//...various settings
const bool COMPFLAG_STYLESETTING_VARIOUS = true;

//special export in movie window
const bool COMPFLAG_MOVIE_SPECIAL_EXPORT = true;

//picture information
const bool COMPFLAG_PICTURE_INFORMATION = true;

//symbols in movies
const bool COMPFLAG_MOVIE_SYMBOLS = true;

//custom zoom in movie
const bool COMPFLAG_CUSTOM_ZOOM_MOVIE = true;

//custom zoom in tree
const bool COMPLFAG_CUSTOM_ZOOM_TREE = true;

//speed display in tree window
const bool COMPFLAG_SPEED_DISPLAY = true;

//colocation display in tree window
const bool COMPFLAG_COLOCATION_DISPLAY = true;

//statistics (Bernhard's)
const bool COMPFLAG_SIMPLE_STATISTICS = true;

//tree map
const bool COMPFLAG_TREE_MAP = true;


/**
	@author Bernhard <bernhard.schauberger@campus.lmu.de>
	@warning currently not used, only the constants above
	@deprecated :-)
*/

class CompileFlags{
public:
    CompileFlags();

    ~CompileFlags();

};

#endif
