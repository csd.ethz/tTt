/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "symbolhandler.h"

SymbolHandler::SymbolHandler()
 : symbolIndex (0)
{
	for (int i = 0; i <= MAX_WAVE_LENGTH; i++)
                newIndices [i] = 0;
}

SymbolHandler::~SymbolHandler()
{
}

bool SymbolHandler::readSymbolFile (QString _filename, Tree *_tree)
{
	if (_filename.isEmpty())
		return false;
	
	QFile file (_filename);
	QDataStream reader;
	
	if (! file.open (QIODevice::ReadOnly))
		return false;
	
	reader.setDevice (&file);
	
	int version = 0;
	int wavelengths = 0;
	
	//read file version to know about the number of wavelengths
	reader >> version;
	
	switch (version) {
		case 12:
			wavelengths = 3;
			break;
		case TTTFileVersion:
			wavelengths = MAX_WAVE_LENGTH;
			break;
		default:
			wavelengths = MAX_WAVE_LENGTH;
	}
	
	
	//read header (number of symbols for each wavelength)
	for (int i = 0; i <= wavelengths; i++) {
                reader >> newIndices[i];
		//often the symbol files are corrupt and read strange numbers, then errors occur
                if ((newIndices[i] >= 0) && (newIndices[i] <= 10000))
                        wlSymbols[i].resize (newIndices[i] + 1);
		else
			return false;
	}
	
	//read symbol data from file
	int index = 0;
	for (int i = 0; i <= wavelengths; i++)
                for (	QVector<Symbol>::Iterator iter = (wlSymbols [i]).begin();
                                iter != (wlSymbols [i]).end();
				++iter) {
			
			reader >> *iter;
			
			//set unique index
			(*iter).setIndex (++index);
			
			//map track number to track pointer
			if (iter->getTrackNumber() > 0)
				iter->setTrack (_tree->getTrack (iter->getTrackNumber()));
			else
				iter->setTrack (0);
		}
	
	file.close();
	
	return true;
}
	
bool SymbolHandler::saveSymbolFile (QString _filename) const
{
	if (_filename.isEmpty())
		return false;
	
	QFile file (_filename);
	QDataStream writer;
	
	if (! file.open (QIODevice::WriteOnly)) 
		return false;		//opening the file for output failed
	
	//associate the stream with the file
	writer.setDevice (&file);
	
	//write ttt file version to know about the number of the wavelengths
	writer << TTTFileVersion;
	
	//write header (number of symbols for each wavelength)
	for (int i = 0; i <= MAX_WAVE_LENGTH; i++)
                writer << newIndices[i];
	
	//write symbol data to file
	for (int i = 0; i <= MAX_WAVE_LENGTH; i++)
                for (	QVector<Symbol>::ConstIterator iter = (wlSymbols [i]).constBegin();
                                iter != (wlSymbols [i]).constEnd();
				++iter) {
			
			if (iter->getSize() > 0)
				writer << *iter;
		}
	
	file.close();
	
	return true;
}

void SymbolHandler::addSymbol (Symbol _symbol, int _waveLength)
{
	//invalid symbol
	if (_symbol.getSize() == -1)
		return;
	
	if (_waveLength < 0 || _waveLength > MAX_WAVE_LENGTH)
		return;		//invalid wavelength
	
	int start = _waveLength;
	int stop = _waveLength;
	if (Symbols_In_All_WL) {
		start = 0;
		stop = MAX_WAVE_LENGTH;
	}
	
	for (int i = start; i <= stop; i++) {
	//resize necessary?
                if (newIndices [i] > 10000)
                        newIndices [i] = wlSymbols [i].size();
		
                if ((int)(wlSymbols [i]).size() <= newIndices [i])
                        (wlSymbols [i]).resize (newIndices [i] + 10);
		
                (wlSymbols [i]).append (_symbol);
		
                newIndices [i]++;
	}
}
	
QVector<Symbol> SymbolHandler::readSymbols (int _timePoint, int _waveLength) const
{
	
	if (_timePoint < 0 || _waveLength < 0 || _waveLength > MAX_WAVE_LENGTH)
                return QVector<Symbol>();		//invalid query
	
        QVector<Symbol> tmpV (0);
	
        for (	QVector<Symbol>::ConstIterator iter = (wlSymbols [_waveLength]).constBegin();
                        iter != (wlSymbols [_waveLength]).constEnd();
			++iter) {
		
		if ((iter->getSize() > -1) &
			(iter->getFirstTimePoint() <= _timePoint) &
			(iter->getLastTimePoint() >= _timePoint))
			
			 tmpV.append (*iter);
	}
	
	return tmpV;
}

void SymbolHandler::deleteAll()
{
	for (int i = 0; i <= MAX_WAVE_LENGTH; i++) {
                wlSymbols[i].clear();
                newIndices [i] = 0;
	}
}

void SymbolHandler::deleteSymbol (int _index)
{
/*	if (_waveLength < 0 || _waveLength > MAX_WAVE_LENGTH)
		return;*/
	
	
	for (int i = 0; i <= MAX_WAVE_LENGTH; i++)
                for (int j = 0; j < (int)wlSymbols[i].size(); j++)
                        if (wlSymbols[i].at (j).getIndex() == _index)
                                wlSymbols[i] [j] = Symbol();		//default symbol == none
	
	//WLSymbols[_waveLength].at (_index) = Symbol();		//default symbol == none
}

