/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TREEELEMENTSTYLE_H
#define TREEELEMENTSTYLE_H


#include "tttgui/graphicaid.h"

#include <qcolor.h>
#include <qstring.h>
#include <q3valuevector.h>
#include <qxml.h>
#include <qdom.h>


const int MAX_COLORS = 16;

///the RTTI values for each tree element (do NOT correspond to the usage constants defined in treedisplay.h)
///NOTE: as soon is there is a new value, the style file version needs to be updated and the loading routine needs
///       to distinguish between the old and new version
///+ default values for each element (are applied in reset(), depending on the RTTI value)
///note: groups should have neighboured RTTI values, as the display is sorted according to RTTI values
const int RTTI_CELLCIRCLE_FREE = 1001;
const QColor CELLFREECOLOR = QColor (200, 200, 200);			//~light gray
const int RTTI_CELLCIRCLE_TRACKED = 1002;
const QColor CELLCOLOR = QColor (100, 100, 100);			//~dark gray
const int RTTI_CELLCIRCLE_SELECTION = 1003;
const QColor CELLSELECTCOLOR = QColor (255, 220, 120);			//~yellow
const QColor CELLSELECTCOLORADDITIONAL = QColor (195, 187, 161);	//~something in between everything

const int RTTI_DIVISIONLINE = 1004;
const QColor HERITAGECOLOR = QColor (10, 120, 120);			//~light gray

const int RTTI_STOPREASON_APOPTOSIS = 1005;
const QColor CELLDEATHCOLOR = QColor (220, 10, 80);			//~red
const int RTTI_STOPREASON_LOST = 1006;
const QColor CELLLOSTCOLOR = QColor (50, 80, 250);			//~blue
const int RTTI_STOPREASON_NONE = 1007;


const int RTTI_CELLSTATE_NORMAL = 1014;
const QColor CELLNORMALCOLOR = Qt::gray;
const int RTTI_CELLSTATE_FREEFLOATING = 1015;
const QColor CELLFLOATINGCOLOR = Qt::red;
const int RTTI_CELLSTATE_SPYHOP = 1016;
const QColor CELLSPYHOPCOLOR = Qt::blue;

const int RTTI_CELLSPEED = 1017;
const int RTTI_SPEEDBAR = 1018;
const int RTTI_CELLNUMBERS = 1019;
const QColor CELLNUMBERCOLOR = Qt::black;

const int RTTI_GENERATIONTIME = 1020;
const int RTTI_ENDOMITOSIS = 1021;
const QColor ENDOMITOSISCOLOR = Qt::blue;

const int RTTI_COLOCATION = 1022;
const QColor COLOCATIONCOLOR = Qt::green;
const QColor COLOCATIONDOTCOLOR = Qt::red;

const int RTTI_TIMEPOINTLINE = 1023;
const QColor TIMEPOINTCOLOR = QColor (20, 255, 30);

const int RTTI_DAYLINES = 1024;
const int RTTI_HOURLINES = 1025;
const QColor DAYSEPARATORCOLOR = Qt::gray;

const int RTTI_COMMENTASTERISK = 1026;
const QColor COMMENTASTERISKCOLOR = Qt::magenta;

const int RTTI_TIMESCALE_FIGURES = 1027;
const QColor TIMESCALEFIGURECOLOR = DAYSEPARATORCOLOR;

const int RTTI_CELLDISTANCE = 1028;
const QColor CELLDISTANCECOLOR = Qt::black;

const int RTTI_TISSUETYPEBLOOD = 1030;
const int RTTI_TISSUETYPESTROMA = 1031;
const int RTTI_TISSUETYPEENDOTHELIUM = 1032;
const int RTTI_TISSUETYPEHEART = 1033;
const int RTTI_TISSUETYPENERVE = 1034;
const int RTTI_TISSUETYPEENDODERM = 1035;
const int RTTI_TISSUETYPEMESODERM = 1036;
const QColor TISSUETYPECOLORS[7] = {Qt::red, Qt::green, Qt::magenta, Qt::yellow, Qt::gray, Qt::cyan, Qt::blue};
extern const char* TISSUETYPETEXTS[];
const int RTTI_TISSUETYPES[7] = {RTTI_TISSUETYPEBLOOD, RTTI_TISSUETYPESTROMA, RTTI_TISSUETYPEENDOTHELIUM, RTTI_TISSUETYPEHEART, RTTI_TISSUETYPENERVE, RTTI_TISSUETYPEENDODERM, RTTI_TISSUETYPEMESODERM};

const int RTTI_GENERALTYPEMYELOID = 1037;
const int RTTI_GENERALTYPELYMPHOID = 1038;
const int RTTI_GENERALTYPEHSC = 1039;
const int RTTI_GENERALTYPEPRIMITIVE = 1040;
const int RTTI_GENERALTYPEPRIMARY = 1041;
const int RTTI_GENERALTYPECELLLINE = 1042;
const int RTTI_GENERALTYPENEURO = 1043;
const int RTTI_GENERALTYPEGLIA = 1044;
const QColor GENERALTYPECOLORS[8] = {Qt::red, Qt::green, Qt::magenta, Qt::yellow, Qt::gray, Qt::cyan, Qt::blue, Qt::darkBlue};
extern const char* GENERALTYPETEXTS[];
const int RTTI_GENERALTYPES[8] = {RTTI_GENERALTYPEMYELOID, RTTI_GENERALTYPELYMPHOID, RTTI_GENERALTYPEHSC, RTTI_GENERALTYPEPRIMITIVE, RTTI_GENERALTYPEPRIMARY, RTTI_GENERALTYPECELLLINE, RTTI_GENERALTYPENEURO, RTTI_GENERALTYPEGLIA};

const int RTTI_LINEAGEMEGAKARYOID = 1045;
const int RTTI_LINEAGEERYTHROID = 1046;
const int RTTI_LINEAGEMAKROPHAGE = 1047;
const int RTTI_LINEAGENEUTROPHIL = 1048;
const int RTTI_LINEAGEEOSINOPHIL = 1049;
const int RTTI_LINEAGEBASOPHIL = 1050;
const int RTTI_LINEAGEMAST = 1051;
const int RTTI_LINEAGEDENDRITIC = 1052;
const QColor LINEAGECOLORS[8] = {Qt::red, Qt::green, Qt::magenta, Qt::yellow, Qt::gray, Qt::cyan, Qt::blue, Qt::darkBlue};
extern const char* LINEAGETEXTS[];
const int RTTI_LINEAGES[8] = {RTTI_LINEAGEMEGAKARYOID, RTTI_LINEAGEERYTHROID, RTTI_LINEAGEMAKROPHAGE, RTTI_LINEAGENEUTROPHIL, RTTI_LINEAGEEOSINOPHIL, RTTI_LINEAGEBASOPHIL, RTTI_LINEAGEMAST, RTTI_LINEAGEDENDRITIC};
// 						B, T, NK,
// 						LTR, STR, MPP, 
// 						Whole_Bone_Marrow, Osteoblast, Endothelial, 
// 						AFT024, _2018, BFC, OP9, PA6


const int RTTI_ENVIRONMENTNUCLEAR = 1053;
const int RTTI_ENVIRONMENTCELL = 1054;

const int RTTI_CLUSTERID = 1055;

//MAX_WAVE_LENGTH
//note: RTTI should not change because of arithmetics in reset() (- 1008)!
const int RTTI_WAVELENGTH0 = 1059;
const int RTTI_WAVELENGTH1 = 1060;
const int RTTI_WAVELENGTH2 = 1061;
const int RTTI_WAVELENGTH3 = 1062;
const int RTTI_WAVELENGTH4 = 1063;
const int RTTI_WAVELENGTH5 = 1064;
const int RTTI_WAVELENGTH6 = 1065;
const int RTTI_WAVELENGTH7 = 1066;
const int RTTI_WAVELENGTH8 = 1067;
const int RTTI_WAVELENGTH9 = 1068;
const int RTTI_WAVELENGTHS[10] = {1059, RTTI_WAVELENGTH1, RTTI_WAVELENGTH2, RTTI_WAVELENGTH3, RTTI_WAVELENGTH4, RTTI_WAVELENGTH5
				  , RTTI_WAVELENGTH6, RTTI_WAVELENGTH7, RTTI_WAVELENGTH8, RTTI_WAVELENGTH9};
const QColor WAVELENGTH1COLOR = Qt::green;
const QColor WAVELENGTH2COLOR = Qt::red;
const QColor WAVELENGTH3COLOR = Qt::blue;
const QColor WAVELENGTH4COLOR = Qt::magenta;
const QColor WAVELENGTH5COLOR = Qt::yellow;
const QColor WAVELENGTH6COLOR = Qt::cyan;
const QColor WAVELENGTH7COLOR = Qt::gray;
const QColor WAVELENGTH8COLOR = Qt::darkGreen;
const QColor WAVELENGTH9COLOR = Qt::darkBlue;
const QColor WAVELENGTHCOLORS[10] = {Qt::white, WAVELENGTH1COLOR, WAVELENGTH2COLOR, WAVELENGTH3COLOR, WAVELENGTH4COLOR, WAVELENGTH5COLOR
                                     , WAVELENGTH6COLOR, WAVELENGTH7COLOR, WAVELENGTH8COLOR, WAVELENGTH9COLOR};


const QString ADDONTEXTS[2] = {"Environment Nuclear", "Environment Cell"};

const int RTTI_CELLRADIUS = 1099;

const int RTTI_FLUORESCENCE_DIAGRAM = 1100;

// const int RTTI_ = 10;
// const int RTTI_ = 10;

///these two values are necessary for looping through the RTTI values
///RTTI values should be a sequence, yet if not, no error occurs
const int TES_RTTI_MIN = 1001;
const int TES_RTTI_MAX = 1068;
const int TES_RTTI_MINUS = 1000;	///difference to absolute indices





/**
	@author Bernhard
	
	This class holds the style entries for one cell tree element (e.g. the cell circles, heritage lines, ...).
	It is completely managed by class StyleSheet, yet the reading functions are accessed in Tree::draw....
	It stores its values directly as XML entities.
	The mapping of tree element to TreeElementStyle object is realised via the RTTI attribute (cf. constants defined above)
	
	New additional usage:
	for key associations - now it can be initialized with a character (instead of an RTTI value)
*/

class TreeElementStyle{

public:
	///constructs an invisible tree element style with RTTI = 0
	TreeElementStyle();
	
	///copy constructor
	///@param _tes the element style to be copied
	TreeElementStyle (const TreeElementStyle &_tes);
	
	///constructs a tree style element with default values
	///@param _rtti the RTTI value for the element
	TreeElementStyle (int _rtti);
	
	///constructs a tree style element with default values for the given key
	///@param _key the key code of the element
	TreeElementStyle (char _key);
	
	~TreeElementStyle();
	
	///assignment operator
	TreeElementStyle &operator= (const TreeElementStyle &_tes);
	
	///sets the RTTI value for this element -------- only possible if RTTI == 0!
	///@param _rtti the RTTI value for which the default values are loaded
	void setRTTI (int _rtti);
	
	///returns the rtti value for this element
	int rtti() const
		{return RTTI;}
	
	/**
	 * sets the associated key value for this element
	 * @param _key the key code (e.g. 'Q')
	 */
	void setKey (char _key);
	
	/**
	 * @return the key value for this element (if it was associated with a key)
	 */
	char getKey()
		{return key;}
	
	///returns the displayed text
	QString getText() const
		{return displayedText;}
	
	///transforms the currently set color to grayscale
	void setGrayscale();
	
	///resets the default value for this element (depending on THIS' RTTI value)
	void reset();
	
	
	///getter and setter methods for all other attributes
	
	void setVisible (bool _visible)
		{visible = _visible;}
	bool isVisible() const
		{return visible;}
	
	void setSize (int _size)
		{size = _size;}
	int getSize() const
		{return size;}
	
	void setColor (int _index, QColor _color)
		{colors[_index] = _color;}
	QColor getColor(int _index) const
		{return colors[_index];}
	
	void setColorCount (int _colorCount)
		{colorCount = _colorCount;}
	int getColorCount()
		{return colorCount;}
	
	void setPositionX (int _positionX)
		{positionX = _positionX;}
	int getPositionX() const
		{return positionX;}
	
	void setPositionY (int _positionY)
		{positionY = _positionY;}
	int getPositionY() const
		{return positionY;}
	
	void setSymbol (const QString _symbol)
		{symbol = _symbol;}
	const QString getSymbol() const
		{return symbol;}
	
	/**
	 * Creates a QDomElement from this style element and adds it to the provided parent element in the XML document
	 * @param _doc the XML document
	 * @param _parent the parent XML element
	 */
	void toDomElement (QDomDocument &_doc, QDomElement &_parent) const;
	
	/**
	 * Sets the values of this element to the values stored in the provided attributes
	 * @param _attrs the XML attributes
	 */
	void setFromXMLAttributes (const QXmlAttributes &_attrs);
	
private:
	
	///which tree element does THIS style object belong to? (cf. RTTI value constants defined above)
	int RTTI;
	
	///the alternative (to RTTI) association: a key
	char key;
	
	///the text which is displayed to identify this element for the user (one for each RTTI value)
	QString displayedText;
	
	///whether this element is visible
	bool visible;
	
	///the size/thickness of the element
	int size;
	
	///the (outline) color of the element
	QColor colors[MAX_COLORS];
	
	///the number of colors (1 by default)
	///necessary for attributes whose different values deserve different colors
	int colorCount;
	
	///the position of the element (in relation to its associated cell's position)
	int positionX;
	int positionY;
	
	///the symbol (used for StopReason, EndoMitosis, ...)
	QString symbol;
	
	//NOTE: as soon as further elements are introduced, the style file version has to be changed (but be aware of XML)
	
	
	
	//friend QDataStream& operator<< (QDataStream&, const TreeElementStyle&);
	friend QDataStream& operator>> (QDataStream&, TreeElementStyle&);

};

//QDataStream& operator<< (QDataStream&, const TreeElementStyle&);
QDataStream& operator>> (QDataStream&, TreeElementStyle&);


#endif
