/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SYMBOLHANDLER_H
#define SYMBOLHANDLER_H

#include "tttdata/symbol.h"
#include "pictureindex.h"
#include "tttdata/tree.h"
#include "tttio/tttfilehandler.h"

#include <qstring.h>
#include <QVector>
#include <qfile.h>

#include <qdatastream.h>

///sets whether a symbol is applied for all wavelengths simultaneously (true) or not
///(then each wavelength can have different symbols even for the same cell)
///set to true for Hanna on 2007/06/26
const bool Symbols_In_All_WL = true;


/**
@author Bernhard

	This class provides functionality to add symbols (like arrows) to pictures
	and to store and read them with files
	For one experiment folder there is one symbol file (*.symbols)
*/

class SymbolHandler{

public:
	
	/**
	 * constructs a default symbol handler
	 */
	SymbolHandler();
	
	~SymbolHandler();
	
	/**
	 * reads the specified file from disk and puts the symbols into the internal array
	 * @param _filename the symbol filename
	 * @param _tree a Tree instance, is necessary for assigning tracks to symbols
	 * @return success
	 */
	bool readSymbolFile (QString _filename, Tree *_tree);
	
	/**
	 * saves the current symbol array to disk with the specified filename
	 * @param _filename the filename of the symbol file
	 * @return success
	 */
	bool saveSymbolFile (QString _filename) const;
	
	/**
	 * adds the provided Symbol instance to the vector of the specified _waveLength
	 * @param _symbol an instance of class Symbol
	 * @param _waveLength the wavelength to which this symbol should be assigned
	 */
	void addSymbol (Symbol _symbol, int _waveLength);
	
	/**
	 * finds and reports the symbols for the specified _timePoint and _waveLength
	 * @param _timePoint the timepoint for which the symbols are desired
	 * @param _waveLength the wavelength for which the symbols are desired
	 * @return a @ref QValueVector<Symbol> with the symbols that are visible for this timepoint and wavelength
	 */
        QVector<Symbol> readSymbols (int _timePoint, int _waveLength) const;
	
	/**
	 * deletes all symbols in RAM
	 * later storage of an empty array does not delete the file, but leaves it more or less empty
	 */
	void deleteAll();
	
	/**
	 * deletes the symbol with identifier _index
	 * @param _index the (unique) index of the symbol that should be deleted
	 */
	void deleteSymbol (int _index);
	
	/**
	 * increments the symbol index by 1
	 * @return the new index
	 */
	int createNewIndex()
                {return ++symbolIndex;}
	
        /**
         * @return a reference to the current symbol
         */
        Symbol& getCurrentSymbol()
                {return currentSymbol;}

        /**
         * sets the current symbol
         */
        void setCurrentSymbol (Symbol &_symbol)
                {currentSymbol = _symbol;}
private:

	///the structure containing the symbols, one for each wavelength
        QVector<Symbol> wlSymbols [MAX_WAVE_LENGTH + 1];
	
	///the indices for the new elements (last inserted + 1) for each wavelength
        int newIndices [MAX_WAVE_LENGTH + 1];
	
	///contains the index of the last inserted symbol (across all wavelengths)
        int symbolIndex;

        ///the current symbol (accessed by various forms/methods)
        Symbol currentSymbol;
};

#endif
