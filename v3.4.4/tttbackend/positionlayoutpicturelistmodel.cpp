/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
// Our header file
#include "positionlayoutpicturelistmodel.h"

// tTt headers
#include "treeelementstyle.h"
#include "fileinfoarray.h"

// Qt headers
#include <QApplication>
#include <QListView>


PositionLayoutPictureListModel::PositionLayoutPictureListModel( QListView* _lvwPictures ) : lvwPictures(_lvwPictures) 
{
	lvwPictures->setModel(this);
}


void PositionLayoutPictureListModel::setItemChecked( int _row, bool _checked /*= true*/ )
{
	// Create QValue for check state
	QVariant val;

	if(_checked) {
		// Hidden items cannot be checked
		if(lvwPictures->isRowHidden(_row))
			return;

		val = QVariant(Qt::Checked);
	}
	else
		val = QVariant(Qt::Unchecked);

	// Set check state
	QModelIndex ind = index(_row);
	if(setData(ind, val, Qt::CheckStateRole))
		emit dataChanged(ind, ind);
}

void PositionLayoutPictureListModel::setItemHidden( int _row, bool _hide /*= true*/ )
{
	// Uncheck item _hide = true
	if(_hide)
		setItemChecked(_row, false);

	// Hide/show element
	lvwPictures->setRowHidden(_row, _hide);
}

bool PositionLayoutPictureListModel::setData( const QModelIndex& index, const QVariant& value, int role /*= Qt::EditRole */ )
{
	// Only CheckState events will be handled by us
	if(role == Qt::CheckStateRole) {
		// Get row and value
		int row = index.row();
		int val = value.toInt();

		// Save checked state
		if(val == Qt::Checked)
			checkedItems.setBit(row, true);
		else
			checkedItems.setBit(row, false);

		// Data has been changed successfully
		//emit dataChanged(index, index);		// <-- this *seems* to be unneccesary (in qt 4.7.2) and takes like 2 seconds of time (causes whole list to be redrawn)..
		emit itemSelectionChanged();

		return true;
	}/* else if(role == Qt::DisplayRole) {
		QString val = value.toString();
	}*/
	
	//return QStringListModel::setData(index, value, role);
	return false;
}

QVariant PositionLayoutPictureListModel::data( const QModelIndex & index, int role /*= Qt::DisplayRole */ ) const
{
	if(role == Qt::DisplayRole) {
		unsigned int row = index.row();

		if(row < fileNames.size())
			return QVariant(fileNames[row]);
		else
			return QVariant();
	}
	else if(role == Qt::CheckStateRole) {
		// Handle CheckState events for checkboxes
		if(checkedItems.testBit(index.row()))
			return QVariant(Qt::Checked);
		else
			return QVariant(Qt::Unchecked);
	} 
	else if(role == Qt::TextColorRole) {
		// Handle TextColorRole events to enable wl-specific coloring in listview

		// Get pic name
		QString picName = index.data(Qt::DisplayRole).toString();
		if(!picName.isEmpty()) {
			// Extract wl
			int wl = FileInfoArray::CalcWaveLengthFromFilename(picName);

			// Return color if wl is valid
			if (wl > 0 && wl <= MAX_WAVE_LENGTH)
				return QVariant(WAVELENGTHCOLORS [wl]);
		}

		// Return black as default
		return QVariant(Qt::black);
	}

	// Have no value
	return QVariant();
}

Qt::ItemFlags PositionLayoutPictureListModel::flags( const QModelIndex& index ) const
{
	// Indicate for all items that they can be checked
	return Qt::ItemIsUserCheckable | Qt::ItemIsEnabled;
}

void PositionLayoutPictureListModel::setStringList ( const QStringList& _strings )
{
	// Parent function, notify that model is about to change
	beginResetModel();

	// Clear selection
	checkedItems.clear();

	// Make room in checked array
	checkedItems.resize(_strings.size());

	// Put entries into model
	fileNames = _strings;

	// Notify that model has changed
	endResetModel();
}

void PositionLayoutPictureListModel::setWaveLengthHidden( int _wl, bool _hide /*= true*/ )
{
	// Change cursor to wait cursor
	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));

	// Iterate over files
	for(int i = 0; i < fileNames.size(); ++i) {
		// Get wavelength
		int currentWl = FileInfoArray::CalcWaveLengthFromFilename (fileNames[i]);

		// Need to show/hide current wavelength
		if(currentWl == _wl) {
			setItemHidden(i, _hide);
		}
	}

	// Restore cursor
	QApplication::restoreOverrideCursor();
}

int PositionLayoutPictureListModel::getIndexOfLastVisibleItem() const
{
	for(int i = fileNames.size() - 1; i >= 0; --i) {
		if(!lvwPictures->isRowHidden(i))
			return i;
	}

	return -1;
}

int PositionLayoutPictureListModel::getIndexOfFirstVisibleItem() const
{
	for(int i = 0; i < fileNames.size(); ++i) {
		if(!lvwPictures->isRowHidden(i))
			return i;
	}

	return -1;
}

int PositionLayoutPictureListModel::getIndexOfLastCheckedItem() const
{
	for(int i = fileNames.size() - 1; i >= 0; --i) {
		if(checkedItems.testBit(i))
			return i;
	}

	return -1;
}

int PositionLayoutPictureListModel::getIndexOfFirstCheckedItem() const
{
	for(int i = 0; i < fileNames.size(); ++i) {
		if(checkedItems.testBit(i))
			return i;
	}

	return -1;
}
