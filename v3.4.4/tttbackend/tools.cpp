/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "tools.h"

// Project includes
#include "tttio/xmlhandler.h"
#include "tttmanager.h"
#include "tttpositionmanager.h"
#include "tttdata/userinfo.h"
#ifndef TREEANALYSIS
#include "tttgui/tttsplashscreen.h"
#else
#include "tttstatsgui/statttsplashscreen.h"
#endif
#include "tttbackend/picturearray.h"
#include "tttio/fastdirectorylisting.h"

// Qt includes
#include <QTextStream>
#include <QMessageBox>
#include <QProcess>

// Qt3 includes
#include <q3listview.h>


QTime Tools::measureTime (QTime (0, 0));
bool Tools::timingStarted (false);

// Define and init xd path
QString Tools::xdPath;

// Ohter static variables
int Tools::numDigitsForPosIndex = -1;
int Tools::numDigitsForWlIndex = -1;
int Tools::numDigitsForZIndex = -1;


Tools::Tools()
{
	int test = 0;
}

Tools::~Tools()
{
}

int Tools::floatEquals (float _number1, float _number2, int _places)
{
	float nr1 = (float)MathFunctions::Round (_number1, _places);
	float nr2 = (float)MathFunctions::Round (_number2, _places);
	
	float diff = nr1 - nr2;
	float tolerance = 1.0f / (float)pow (10, _places + 1);
	
	if (fabs (diff) <= tolerance)
		return 0; 		//numbers are equal within the given tolerance
	else if (diff < 0.0f)
		return 1;		//number 1 < number 2
	else
		return -1;		//number 1 > number 2
}

//double Tools::Round (double _number, int _places)
//{
//    double v[] = { 1, 10, 1e2, 1e3, 1e4, 1e5, 1e6, 1e7, 1e8 };  // possibly enlarge
//    return floor(_number * v[_places] + 0.5) / v[_places];
//}

QString Tools::readTextFile (QString _filename)
{
	//std::ifstream loader (_filename, std::ios_base::in);
	//
	//if (! loader)
	//	return QString();			//opening for input failed
	//
	//loader.seekg (0, std::ios_base::beg);
	//
	//QString result = "";
	//char zack = 0;
	//while (! loader.eof()) {
	//	loader.read (&zack, 1);
	//	result += zack;
	//}
	//
	//return result;

	// --------- Oliver -----------
	// reimplemented using qt4 objects for better performance

	// Open file
	QFile file(_filename);
	if(!file.open(QFile::ReadOnly))
		return QString();

	// Create string and reserve enough space for file
	QString result;
	qint64 size = file.size();
	result.reserve(size);

	// Read file
	QTextStream in(&file);
	while(1) {
		char ch;
		in >> ch;

		if(in.status() != QTextStream::Ok)
			break;

		result.append(ch);
	}

	return result;
}

bool Tools::writeTextFile (const QString &_filename, const QString &_content)
{
	if (_filename.isEmpty())
		return false;
	
	QFile file;
	
	file.setName (_filename);
	if (! file.open (QIODevice::WriteOnly)) 
		return false;		//opening the file for output failed
	
	//associate the stream with the file
        QTextStream writer;
	writer.setDevice (&file);
	
	writer << _content;
	
	file.close();
	
	return true;
}

int Tools::pow (int _base, int _exp)
{
	if (_exp < 0)
		return 0;
	
	int result = 1;
	for (int i = 1; i <= _exp; i++)
		result *= _base;
	
	return result;
}

QString Tools::getPositionNumberFromString(const QString &_posString)
{
	if (_posString.isEmpty())
		return "";

	if (_posString.find (POSITION_MARKER) < 0)
		return "";

	QString result = _posString.mid (_posString.findRev (POSITION_MARKER) + POSITION_MARKER.length());

	// 18.02.2011-OH: Support for 3 and 4 digit positions required -> check 4th character
	if(result.length() > 3 && result[3].isDigit())
		result = result.left (4);
	else
		result = result.left (3);

	return result;
}

const QString Tools::getPositionIndex (const QString &_filename)
{
	QString result = getPositionNumberFromString(_filename);
	
	if ( TTTManager::getInst().isInStatisticsMode()) {// -------------- Konstantin ---------------
		QString experimentName = _filename.mid(_filename.findRev("/") + 1);
		experimentName = experimentName.section(POSITION_MARKER, 0, 0);
		//result = QString("%1_%1").arg(experimentName).arg(result); // -------------- Konstantin ---------------
		result = QString("%1%2%3").arg(experimentName).arg(POSITION_MARKER).arg(result); // -------------- Konstantin ---------------
	}
	
	return result;
}


void Tools::startTimeMeasure()
{
	measureTime = QTime::currentTime();
	timingStarted = true;
}

int Tools::stopTimeMeasure()
{
	if (! timingStarted)
		return -1;
	
	QTime stopTime = QTime::currentTime();
	timingStarted = false;
	
	return measureTime.secsTo (stopTime);
}

bool Tools::readLogFile (TTTPositionManager *_tttpm, int &_ltp, int &_ftp, /*const QString &_pictureSuffix,*/ bool _showMessage)
{
	//create a new log file reader and read the file
	
	if (! _tttpm)
		return false;
	
	bool success = false;
	
	if (_ltp == 0)
		_ltp = -1;		//force setting in log file reader
	
	QString basename = _tttpm->getBasename();
	
	LogFileReader *logInput = new LogFileReader (_tttpm->getPictureDirectory() + "/" + basename + ".log", basename, _ltp, _ftp/*, _pictureSuffix*/);
	
	if ((! logInput->LogFileUpToDate() & (! TTTManager::getInst().loadTTTFilesOnly())) || (! logInput->LoadingSuccesful())) {
		if (_showMessage)
			QMessageBox::information (0, "Loading error", "Fsi file " + basename + ".log" + " does not correspond to picture files in directory \nPlease redo log file conversion \nPosition will not be available.", QMessageBox::Ok);
		
		_tttpm->lockPosition();
		success = false;
	}
	else {
		//associate the FileInfoArray for public access
		_tttpm->setExperimentTimepoints (_ftp, _ltp);
		_tttpm->setFileInfoArray (logInput->getArray());
		success = true;
	}

    //BS 2010/08/05
    //Datenkonsistenz ueberpruefen: erstes Bilddatum muss vor Systemdatum liegen!
    QDate sd = SystemInfo::getSystemTime().date();

	//OH: in some cases getFiles returns null
    //QDate fpd = _tttpm->getFiles()->getRealTime (_ftp, -1, true).date();
	const FileInfoArray* files = _tttpm->getFiles();
	if(files) {
		QDate fpd = files->getRealTime (_ftp, 1, -1, true).date();

		if (sd.daysTo (fpd) > 0) {
				QString botschaft = "Position error due to flawed pixel data (code " + QString ("%1").arg (0) + ").\n";
				QMessageBox::warning (0, "Loading position failed", botschaft);

				_tttpm->lockPosition();
				success = false;
		}
	}

	
	if (logInput)
		//now the LogFileReader is not needed anymore!
		delete logInput;

	return success;
}

void Tools::exp_collListViewItems (Q3ListViewItem *_lvi, bool _expand, bool _recursive)
{
	if (! _lvi)
		return;
	
	_lvi->setOpen (_expand);
	
	///@todo expands/collapses too much... (the current and all following children)
	
	if (_recursive) {
		if (_lvi->childCount() > 0) {
			
			Q3ListViewItemIterator iter (_lvi->firstChild());
			
			while (iter.current()) {
				Q3ListViewItem *item = iter.current();
				item->setOpen (_expand);
				
				++iter;
			}
		}
	}
}

void Tools::parseDOMDocumentIntoListview (QDomDocument &_domDoc, Q3ListView *_listView)
{
	if (! _listView)
		return;
	
	_listView->clear();
	
	for (int col = 0; col < _listView->columns(); col++)
		_listView->removeColumn (col);
	
	_listView->addColumn ("Element / Attribute");
	_listView->addColumn ("Value");
	
	Q3ListViewItem *lvi = new Q3ListViewItem (_listView);
	
	//allow the user to open the root item
	_listView->setRootIsDecorated (true);
	
	QDomElement docElem = _domDoc.documentElement();
	
	parseDOMElementIntoListviewItem (docElem, lvi);
	
	//expand the root item only
	lvi->setOpen (true);
}

void Tools::parseDOMElementIntoListviewItem (QDomElement &_domEl, Q3ListViewItem *_lvi)
{
	if (_domEl.isNull())
		return;
	
	QDomNodeList children = _domEl.childNodes();
	
	//write tag name and value
	_lvi->setText (0, _domEl.tagName());
	
	if (children.count() == 1) {
		//only write the text if exactly one text child is present, otherwise the text would be repeated
		QDomNode ele = children.item (0);
		//if this element has no children, then the text can be displayed
		if (ele.childNodes().count() == 0)
			_lvi->setText (1, _domEl.text());
	}
	else {
		_lvi->setText (1, "");
	}
	
	//loop over the tag's attributes
	QDomNamedNodeMap attrs = _domEl.attributes();
	
        for (int i = 0; i < attrs.count(); i++) {
		QDomNode n = attrs.item (i);
		QDomAttr a = n.toAttr();
		if (! a.isNull()) {
			(void) new Q3ListViewItem (_lvi, a.name(), a.value());
			
		}
	}
	
	//continue recursively with this node's children
	if (children.count() > 0) {
                for (int i = 0; i < children.count(); i++) {
			QDomNode n = children.item (i);
			QDomElement e = n.toElement();
			if (! e.isNull()) {
				parseDOMElementIntoListviewItem (e, new Q3ListViewItem (_lvi));
			}
		}
	}
	
}

QString Tools::extractValueFromDOMDocument (const QDomDocument &_dom, const QString &_elementName)
{
        //NOTE: does not work for multiple items with the same name!

        //works for the following configurations:
        //<tag attr="desired value" />
        //<tag>value</tag>

        QDomElement docElem = _dom.documentElement();

        //traverse all elements of the document whether its name corresponds to the provided one
        return extractValueFromDOMElement (docElem, _elementName);
}

QString Tools::extractValueFromDOMElement (const QDomElement &_domEl, const QString &_elementName)
{
        if (_domEl.isNull())
                return "";

        QDomNodeList children = _domEl.childNodes();

        if (_domEl.tagName() == _elementName) {

                //loop over the tag's attributes, until one non-zero is found
                QDomNamedNodeMap attrs = _domEl.attributes();

                for (int i = 0; i < attrs.count(); i++) {
                        QDomNode n = attrs.item (i);
                        QDomAttr a = n.toAttr();
                        if (! a.isNull()) {
                                return a.value();
                        }
                }

                //if nothing was found in the attributes: check if this node has only one child -> return its text
                if (children.count() == 1) {
                        //if exactly one text child is present, use its text
                        QDomNode ele = children.item (0);
                        //if this element has no children, it's O.K.
                        if (ele.childNodes().count() == 0)
                                return _domEl.text();
                }

        }
        else {
                //continue recursively with this node's children
                if (children.count() > 0) {
                        QString tmp = "";
                        for (int i = 0; i < children.count(); i++) {
                                QDomNode n = children.item (i);
                                QDomElement e = n.toElement();
                                if (! e.isNull()) {
                                        tmp = extractValueFromDOMElement (e, _elementName);
                                        if (tmp > "") {
                                            return tmp;
                                        }
                                }
                        }
                }
        }

        //nothing found
        return "";
}

bool Tools::setValueInDOMDocument (const QDomDocument &_dom, const QString &_elementName, const QString &_elementValue)
{
    //NOTE: does not work for multiple items with the same name! (simply takes the first encountered)

    //works for the following configurations:
    //<tag attr="desired value" />
    //<tag>value</tag>

    QDomElement docElem = _dom.documentElement();

	if(docElem.isNull())
		return false;

    //traverse all elements of the document whether its name corresponds to the provided one
    return setValueInDOMElement (docElem, _elementName, _elementValue   );
}

bool Tools::setValueInDOMElement (const QDomElement &_domEl, const QString &_elementName, const QString &_elementValue)
{
        if (_domEl.isNull())
                return false;

        QDomNodeList children = _domEl.childNodes();

        if (_domEl.tagName() == _elementName) {

                //loop over the tag's attributes, until one non-zero is found
                QDomNamedNodeMap attrs = _domEl.attributes();

                for (int i = 0; i < attrs.count(); i++) {
                        QDomNode n = attrs.item (i);
                        QDomAttr a = n.toAttr();
                        if (! a.isNull()) {
                                a.setValue (_elementValue);
                                return true;
                        }
                }

                //if nothing was found in the attributes: check if this node has only one child -> return its text
                if (children.count() == 1) {
                        //if exactly one text child is present, use its text
                        QDomNode ele = children.item (0);
                        //if this element has no children, it's O.K.
                        if (ele.childNodes().count() == 0) {
                                ele.setNodeValue (_elementValue);
                                return true;
                        }
                }

        }
        else {
                //continue recursively with this node's children
                if (children.count() > 0) {
                        for (int i = 0; i < children.count(); i++) {
                                QDomNode n = children.item (i);
                                QDomElement e = n.toElement();
                                if (! e.isNull()) {
                                        if (setValueInDOMElement (e, _elementName, _elementValue))
                                            return true;
                                }
                        }
                }
        }

        //element not found
        return false;
}

QString Tools::exportAllTreesOfPositionToImages (TTTPositionManager *_tttpm, bool _showMessage)
{
	//export a snapshot of all trees in this position
	//agenda:
	//- create a new TreeDisplay object in the background
	//- for each tree:
	//  - load each tree into the tree view
	//  - save complete tree in unstretched format to its corresponding filename

	QString folder;
	QString us;
	QString basename;
	QString basename_without_pos;
	
#ifndef TREEANALYSIS // -------------------- Konstantin --------------
	
	if (! _tttpm)
		return QString();
	
	_tttpm->initialize();
	
	TreeDisplay *td = new TreeDisplay (0);
	
	bool completeTree = true;
	bool unstretched = true;
	
	QString filename = "";
	
	//create subdirectory for current experiment
	folder = TTTManager::getInst().getNASDrive() + TREE_EXPORT_FOLDER + "/";
	us = UserInfo::getInst().getUsersign();
	
	QStringList tttFiles = _tttpm->getAllTTTFiles();
	
	Tree *tree = 0;
	for (QStringList::Iterator iter = tttFiles.begin(); iter != tttFiles.end(); ++iter)
	{
		tree = new Tree();
		
		int ftp = -1, ltp = -1, numTracks = -1;

                if (TTTFileHandler::readFile (*iter, tree, numTracks, ftp, ltp, &_tttpm->positionInformation) != TTT_READING_SUCCESS)
                        continue;
		
		td->init (tree, _tttpm);
		
		basename = _tttpm->getBasename();
		basename_without_pos = basename.left (basename.findRev (POSITION_MARKER));
		QString tree_folder = folder + us + "/" + basename_without_pos + "/" + basename;
		
		SystemInfo::checkNcreateDirectory (tree_folder, true, true);
		
		QString tree_filename_without_suffix = tree->getFilename();
		tree_filename_without_suffix = tree_filename_without_suffix.left (tree_filename_without_suffix.length() - 4);		//cut off .ttt suffix
		tree_filename_without_suffix = tree_filename_without_suffix.mid (tree_filename_without_suffix.findRev ("/") + 1);	//cut off path
		
		//filename = tree_folder + "/" + CurrentColonyName.left (CurrentColonyName.length() - 4) + "_" + QDateTime::currentDateTime().toString ("yyyyMMdd_hhmmss");
		QString filename = tree_folder + "/" + tree_filename_without_suffix + "_" + QDateTime::currentDateTime().toString ("yyyyMMdd_hhmmss");
		
		td->exportTree (filename + "-tree.jpg", completeTree, unstretched, true, tree_filename_without_suffix, true, "JPEG");
		
		if (tree)
			delete tree;
	}
	
	if (_showMessage) 
		Tools::displayMessageBoxWithOpenFolder("Please make sure to delete your exported trees after (ab)using them\nbecause they consume quite a lot of disk space on the net.", "Tree export completed.", folder);
		//QMessageBox::information (0, "Saving storage space", "Please make sure to delete your exported trees after (ab)using them\nbecause they consume quite a vast amount of storage on the net.", "Surely I will...");
	
	if (td)
		delete td;

#endif // -------------------- Konstantin --------------

	return folder + us + "/" + basename_without_pos;
}


#ifndef TREEANALYSIS 
IntensityData Tools::measureCellIntensity (const TrackPoint &_trackPoint, int _wavelength, int _zIndex, bool _measureBackground)
{


	TTTPositionManager *tttpm = 0;
	
	//new version (Bernhard, 2009/11/23):
	//the position where the trackpoint was set is stored, so we can use it directly
	//yet, for older ones, this is not stored, so we have to calculate the position
	// NOTE: set as comment for old movies without real positions
	QString position_key = _trackPoint.Position;
	if (position_key == "" || position_key == "000") {
		tttpm = TTTManager::getInst().getPositionAt (_trackPoint.X, _trackPoint.Y);
	}
	else {
		//position is stored in trackpoint
		tttpm = TTTManager::getInst().getPositionManager (position_key);
	}
	
	if (! tttpm)
		return IntensityData();
	
	if (! tttpm->isInitialized())
		tttpm->initialize();
	
	
	if (! tttpm->getFiles()->exists (_trackPoint.TimePoint, _zIndex, _wavelength)) {
		//picture at the desired timepoint with the desired wavelength does not exist
		return IntensityData();
	}
	
	int timePoint = _trackPoint.TimePoint;
	bool picLoaded = tttpm->getPictures()->isLoaded (timePoint, _wavelength, 1);
	if (! picLoaded) {
		tttpm->getPictures()->loadOnePicture(PictureIndex(timePoint, _zIndex, _wavelength));
		//tttpm->getPictures()->setLoading (timePoint, _zIndex, _wavelength, true);
		//tttpm->getPictures()->loadPictures (timePoint, timePoint, -1, false);
	}
	
	//cell intensity
	int PixelCount = 0;
	float MaxValueCell = 0;
	float MinValueCell = 255;
	float ValueIntegralCell = 0;
	//int SumOfSquaresCell = 0;
	//int StdDevCell = 0.0f;
	float AvgCell = 0.0f;
	int PixelColor = 0;
	
	//calculate picture coordinates
    QPointF middle (_trackPoint.X, _trackPoint.Y);
    middle = tttpm->getDisplays().at (_wavelength).calcTransformedCoords (middle, &tttpm->positionInformation);

    int radius = (int)(_trackPoint.CellDiameter / 2.0f);
	
	for (int x = (int)(middle.x() - radius); x <= (int)(middle.x() + radius); x++) {
		for (int y = (int)(middle.y() - radius); y <= (int)(middle.y() + radius); y++) {
			
			//check if point is inside the region of the cell (circle!) via Pythagoras
			int dx = x - (int)middle.x();
			int dy = y - (int)middle.y();
			if ((dx*dx + dy*dy) <= radius*radius) {
				PixelCount++;
				
				PixelColor = tttpm->getPictures()->readPixel (_trackPoint.TimePoint, _zIndex, _wavelength, x, y);
				
				if (PixelColor > MaxValueCell)
					MaxValueCell = PixelColor;
				if (PixelColor < MinValueCell)
					MinValueCell = PixelColor;							
				
				ValueIntegralCell += PixelColor;
				//SumOfSquaresCell += Tools::pow (PixelColor, 2);
			}
		}
	}
	
	if (PixelCount > 0)
		AvgCell = (float)ValueIntegralCell / (float)PixelCount;
	
	
	float MaxValueBackground = 0;
	float MinValueBackground = 255;
	float ValueIntegralBackground = 0;
	//SumOfSquaresBackground = 0;
	//StdDevBackground = 0.0f;
	float AvgBackground = 0.0f;
	
	if (_measureBackground && (_trackPoint.backgroundPoint().x() != -1.0f)) {
		//background intensity
		PixelCount = 0;
		
		//calculate picture coordinates
		middle.setX (_trackPoint.XBackground);
		middle.setY (_trackPoint.YBackground);
                middle = tttpm->getDisplays().at (_wavelength).calcTransformedCoords (middle, &tttpm->positionInformation);
		
		for (int x = (int)(middle.x() - radius); x <= (int)(middle.x() + radius); x++) {
			for (int y = (int)(middle.y() - radius); y <= (int)(middle.y() + radius); y++) {
				
				//check if point is inside the region of the cell (circle!) via Pythagoras
				int dx = x - (int)middle.x();
				int dy = y - (int)middle.y();
				if ((dx*dx + dy*dy) <= radius*radius) {
					PixelCount++;
					
					PixelColor = tttpm->getPictures()->readPixel (_trackPoint.TimePoint, _zIndex, _wavelength, x, y);
					
					if (PixelColor > MaxValueBackground)
						MaxValueBackground = PixelColor;
					if (PixelColor < MinValueBackground)
						MinValueBackground = PixelColor;							
					
					ValueIntegralBackground += PixelColor;
					//SumOfSquaresBackground += Tools::pow (PixelColor, 2);
				}
			}
		}
		if (PixelCount > 0)
			AvgBackground = (float)ValueIntegralBackground / (float)PixelCount;
	}
	
	return IntensityData (AvgCell, AvgBackground, MinValueCell, MaxValueCell, MinValueBackground, MaxValueBackground, ValueIntegralCell, ValueIntegralBackground);
}
#endif

QCursor Tools::createCircleCursor (int _diameter, QColor _color, int _thickness)
{
        int diam = _diameter;

        //there seems to be no restriction on cursor icon sizes... (neither Windows nor Linux)
        //thus, simply create a cursor with the desired width
        //add 2 to receive a nice circle; otherwise flattened at the right & bottom margin
        int width = diam + 2;

        //create pixmap with a transparency mask
        QPixmap mousePix (width, width);
        mousePix.fill (Qt::transparent);
        QPainter p (&mousePix);
        p.setPen (QPen (_color, _thickness));  //non-transparent
        p.drawArc (QRect (0, 0, width - 2, width - 2), 0, 5760);
        p.drawPoint (diam / 2, diam / 2);         //draw center point
        p.end();

        //set all pixels transparent but these on the circle line with the given radius
        QBitmap mask (width, width);
        mask.fill (Qt::color0); //transparent
        p.begin (&mask);
        p.setPen (QPen (Qt::color1, _thickness));  //non-transparent
        p.drawArc (QRect (0, 0, width - 2, width - 2), 0, 5760);
        p.drawPoint (diam / 2, diam / 2);         //draw center point
        p.end();

        mousePix.setMask (mask);

        //the hot spot is set in the middle automatically
        return QCursor (mousePix);
}


bool Tools::stringToInt(const QString& str, int& i, int base)
{
	bool ok;
	i = str.toInt(&ok, base);
	return ok;
}

void Tools::displayMessageBoxWithOpenFolder( QString _message, QString _caption, QString _folder, bool _folderIncludesFile, const QWidget* _parent )
{
	// Show messagebox with possibility to open folder
	QMessageBox msgBox;

	// Default caption
	if(_caption.isEmpty())
		_caption = "tTt";

	// Include message
	msgBox.setCaption(_caption);
	msgBox.setText(_message);

	// Add buttons
	QPushButton *okButton = msgBox.addButton("Ok", QMessageBox::AcceptRole);
	msgBox.setDefaultButton(okButton);
	QPushButton *openFolderButton = msgBox.addButton("Open Folder", QMessageBox::ActionRole);

	// Display messagebox
	msgBox.exec();

	// Open folder if desired
	if(msgBox.clickedButton() == openFolderButton) {
		QString nativeMovieFolder = QDir::toNativeSeparators(_folder);
		QString commandLineArguments;
		if(_folderIncludesFile)
			// Opens the folder and selects the file
			commandLineArguments = QString("/select,") + nativeMovieFolder;
		else
			// Just open the folder
			commandLineArguments = nativeMovieFolder;

		//QProcess::execute("Explorer.exe", QStringList(commandLineArguments));
		QProcess::execute(QString("Explorer.exe ") + commandLineArguments);
	}
}

int Tools::getNumOfPositionDigits()
{
	// Issue a warning if this is not set
	if(numDigitsForPosIndex == -1)
		qWarning() << "tTt warning: Tools::getNumOfPositionDigits() called, but numOfDigitsForPosIndex is not set.";

	return numDigitsForPosIndex;
}

void Tools::setNumOfPositionDigits( int _numDigits )
{
	// Issue a warning if this was already set
	if(numDigitsForPosIndex != -1)
		qWarning() << "tTt warning: Tools::setNumOfPositionDigits() called, but numOfDigitsForPosIndex is already set.";

	numDigitsForPosIndex = _numDigits;
}

QString Tools::convertIntPositionToString( int _posNumber )
{
	// Determine num digits
	int numDigits = numDigitsForPosIndex;
	if(numDigits == -1)
		numDigits = 4;		// assume 4

	// Convert
	return QString("%1").arg(_posNumber, numDigits, 10, QChar('0'));
}

int Tools::getNumOfWavelengthDigits()
{
	// Issue a warning if this is not set
	if(numDigitsForWlIndex == -1)
		qWarning() << "tTt warning: Tools::getNumOfWavelengthDigits() called, but numDigitsForWlIndex is not set.";

	return numDigitsForWlIndex;
}

void Tools::setNumOfWavelengthDigits( int _numDigits )
{
	// Issue a warning if this was already set
	if(numDigitsForWlIndex != -1)
		qWarning() << "tTt warning: Tools::setNumOfWavelengthDigits() called, but numDigitsForWlIndex is already set.";

	numDigitsForWlIndex = _numDigits;
}

int Tools::getNumOfZDigits()
{
	// Issue a warning if this is not set
	if(numDigitsForZIndex == -1)
		qWarning() << "tTt warning: Tools::getNumOfZDigits() called, but numDigitsForZIndex is not set.";

	return numDigitsForZIndex;
}

void Tools::setNumOfZDigits( int _numDigits )
{
	// Issue a warning if this was already set
	if(numDigitsForZIndex != -1)
		qWarning() << "tTt warning: Tools::setNumOfZDigits() called, but numDigitsForZIndex is already set.";

	numDigitsForZIndex = _numDigits;
}

cv::Mat Tools::applyBackgroundCorrection(const cv::Mat& originalImage, const unsigned short* backgroundImage, const unsigned short* gainImage, bool reNormalize)
{
	try {
		// Basic input checks
		if(!originalImage.data || !backgroundImage || !gainImage || (originalImage.type() != CV_8U && originalImage.type() != CV_16U) )
			return cv::Mat();

		// Conversion
		cv::Mat retImage;
		if(originalImage.type() == CV_8U)
			retImage = applyBackgroundCorrectionInternal<unsigned char>(originalImage, backgroundImage, gainImage);
		else
			retImage = applyBackgroundCorrectionInternal<unsigned short>(originalImage, backgroundImage, gainImage);
	
		// Done
		return retImage;
	}
	catch(const cv::Exception& e) {
		return cv::Mat();
	}
}

cv::Mat Tools::openGrayScaleImageNormalized(const QString& imagePath)
{
	try {
		// Load image
		cv::Mat img = cv::imread(imagePath.toStdString(), CV_LOAD_IMAGE_UNCHANGED);
		if(!img.data)
			return cv::Mat();

		// Convert to 1-channel
		if(img.channels() > 1)
			cv::cvtColor(img, img, CV_BGR2GRAY, 1);

		// Convert image to float
		cv::Mat imgDouble;
		img.convertTo(imgDouble, CV_32F);
		if(!imgDouble.data)
			return cv::Mat();

		// Normalize data
		double maxValue = (1 << (img.elemSize1() << 3)) - 1; 
		imgDouble /= maxValue;

		// Done
		assert(imgDouble.type() == CV_32FC1);
		return imgDouble;
	}
	catch(const cv::Exception& e) {
		return cv::Mat();
	}
}

cv::Mat Tools::openGrayScaleImageUnchanged(const QString& imagePath)
{
	try {
		// Load image
		cv::Mat img = cv::imread(imagePath.toStdString(), CV_LOAD_IMAGE_UNCHANGED);
		if(!img.data)
			return cv::Mat();

		// Convert to 1-channel if necessary
		if(img.channels() > 1)
			cv::cvtColor(img, img, CV_BGR2GRAY, 1);

		// Convert type if necessary
		if(img.type() != CV_8U && img.type() != CV_16U) {
			img.convertTo(img, CV_8U);
			if(!img.data)
				return cv::Mat();
		}

		// Done
		return img;
	}
	catch(const cv::Exception& e) {
		return cv::Mat();
	}
}
