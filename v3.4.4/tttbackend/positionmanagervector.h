/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef POSITIONMANAGERVECTOR_H
#define POSITIONMANAGERVECTOR_H

#include <q3ptrvector.h>
//Added by qt3to4:
#include <Q3PtrCollection>

#include "tttpositionmanager.h"

/**
	@deprecated

	DO NOT USE IN NEW CODE!

	@author Bernhard Schauberger
	This class is a QPtrVector of TTTPositionManagers and implements compareItems, which can be used for sorting the collection according to their position index.
*/

class PositionManagerVector : public Q3PtrVector<TTTPositionManager>
{

public:
	PositionManagerVector();
	
	~PositionManagerVector();
	
protected:
	
	int compareItems (Q3PtrCollection::Item d1, Q3PtrCollection::Item d2);
};

#endif
