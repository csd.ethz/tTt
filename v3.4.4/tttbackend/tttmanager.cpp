/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "tttmanager.h"

// STL
#include <algorithm>

// Project includes
//#include "positionmanagervector.h"
#include "tools.h"
#include "picturearray.h"

#ifndef TREEANALYSIS
#include "tttgui/tttpositionlayout.h"
#include "tttgui/ttttracking.h"
#include "tttgui/tttmainwindow.h"
#include "tttgui/tttsymbolselection.h"
#include "tttgui/tttcellproperties.h"
#include "tttgui/ttttreemap.h"
#include "tttgui/ttttreestyleeditor.h"
#include "tttgui/tttpositiondetails.h"
//#include "tttgui/tttpositionstatistics.h"
#include "tttgui/ttttextbox.h"
#include "tttgui/ttttracksoverview.h"
#include "tttgui/tttpositionxmldisplay.h"
#include "tttgui/tttgammaadjust.h"
#include "tttgui/tttmovie.h"
#include "tttgui/ttttreemerging.h"
#include "tttio/xmlhandler.h"
#include "tttgui/tttautotracking.h"
#include "tttgui/tttautotrackingtreewindow.h"
#include "tttgui/tttlogfileconverter.h"
#endif




TTTManager* TTTManager::inst (0);

#ifndef TREEANALYSIS
//TTTManager::TTTManager()
//	: frmTracking (0), frmMainWindow (0), frmSymbolSelection (0), frmTreeStyleEditor (0),
//	  frmCellProperties (0), frmTreeMap (0), frmPositionLayout (0), frmPositionStatistics (0), 
//          frmPositionDetails (0), frmTracksOverview (0), frmPositionXMLDisplay (0), frmStatistics (0),
//          frmTreeMerging (0), frmStatsMainWindow(0), frmStatisticsPlot(0), frmTTTStatsPlotProperties(0), frmAutoTracking(0), frmLogFileConverter(0)
TTTManager::TTTManager()
	: frmTracking (0), frmMainWindow (0), frmSymbolSelection (0), frmTreeStyleEditor (0),
	  frmCellProperties (0), frmTreeMap (0), frmPositionLayout (0), /*frmPositionStatistics (0), */
          frmPositionDetails (0), frmTracksOverview (0), frmPositionXMLDisplay (0), frmStaTTTsMain (0),
          frmTreeMerging (0), frmStatsMainWindow(0), frmStatisticsPlot(0), frmTTTStatsPlotProperties(0), 
		  frmAutoTracking(0), frmLogFileConverter(0)
#else
//TTTManager::TTTManager() : frmStatistics(0), frmStatsMainWindow(0), frmStatisticsPlot(0), frmTTTStatsPlotProperties(0)
TTTManager::TTTManager() : frmStaTTTsMain(0), frmStatsMainWindow(0), frmStatisticsPlot(0), frmTTTStatsPlotProperties(0)
#endif
{
	//initialize variables
	currentTracks = 0;
	currentTrackPoints = 0;
	trackingCellProperties.Set_FreeFloating = true;
	trackingCellProperties.Set_NonAdherent = true;
	trackingCellProperties.Set_Tissues = true;
	trackingCellProperties.Set_Wavelengths = true;
	m_currentPositionKey = -1;
	m_basePositionKey = -1;
	tree = 0;
	currentTrack = 0;
	NASDrive = "";
	Tracking = false;
	backwardTrackingMode = false;
	TrackingStartFirstTimepoint = 0;
	TrackingEndFirstTimepoint = 0;
	stopReason = TS_NONE;
	use_NEW_POSITIONS = false;
	tvFactor = 1.0f;
	ocularFactor = 5;
	miningResults = 0;
	externalTracks = 0;
	statisticsMode =  false;
	keyboardGrabber = 0;
	m_framesToSkipWhenTracking = 0;

	// Differentiation: none
	trackingCellProperties.tissueType = TT_NONE;
	trackingCellProperties.cellGeneralType = CGT_NONE;
	trackingCellProperties.cellLineage = 0;

	// Adherence: adherent
	trackingCellProperties.NonAdherent = false;
	trackingCellProperties.FreeFloating = false;

	// Endomitosis: off
	trackingCellProperties.EndoMitosis = false;
}

TTTManager::~TTTManager()
{
	if (tree)
		delete tree;
	
	//if (currentTrack)
	//	delete currentTrack;
	
	if (currentTracks)
		delete currentTracks;
	
	if (currentTrackPoints)
		delete currentTrackPoints;
	
#ifndef TREEANALYSIS
	if (frmMainWindow)
		delete frmMainWindow;
	
	if (frmTracking)
		delete frmTracking;
	
	if (frmTreeStyleEditor)
		delete frmTreeStyleEditor;
	
	if (frmSymbolSelection)
		delete frmSymbolSelection;
	
	if (frmCellProperties)
		delete frmCellProperties;
	
	if (frmTreeMap)
		delete frmTreeMap;
	
	if (frmPositionLayout)
		delete frmPositionLayout;
	
	//if (frmPositionStatistics)
	//	delete frmPositionStatistics;
	
	if (frmPositionDetails)
		delete frmPositionDetails;
	
	if (frmTracksOverview)
		delete frmTracksOverview;
	
	if (frmPositionXMLDisplay)
		delete frmPositionXMLDisplay;
	
	
	if (frmTreeMerging)
		delete frmTreeMerging;

	if(frmAutoTracking)
		delete frmAutoTracking;

	if(frmLogFileConverter)
		delete frmLogFileConverter;

#endif

	//if (frmStatistics)
	//	delete frmStatistics; // ------------------ Konstantin ---------------
	if (frmStaTTTsMain)
		delete frmStaTTTsMain; // ------------------ Konstantin ---------------
	
	if (miningResults)
		delete miningResults;
	
	if (externalTracks)
		delete externalTracks;

	if (frmStatsMainWindow)
		delete frmStatsMainWindow; // ------------------ Konstantin ---------------

	if (frmStatisticsPlot)
		delete frmStatisticsPlot; // ------------------ Konstantin ---------------

	if (frmTTTStatsPlotProperties) // ------------------ Konstantin ---------------
		delete frmTTTStatsPlotProperties;

	// Delete position managers
	for(auto it = m_positionManagers.begin(); it != m_positionManagers.end(); ++it)
		delete (*it);
}

TTTManager& TTTManager::getInst()
{
	if (! inst) {
		inst = new TTTManager();
		inst->createForms();
		inst->connectForms();
	}
	
	return *inst;
}

//void TTTManager::setCurrentPosition (const QString &_key)
//{
//	if (currentPositionKey == _key)
//		return;
//	
//	QString oldKey = currentPositionKey;
//	currentPositionKey = _key;
//	
//	emit currentPositionChanged (oldKey, _key);
//}

// ---------- Konstantin --------------------
void TTTManager::addExperiment(const QString &_key, Experiment *_exp)
{
        //if there is already an experiment with this key, delete this first
        experiments.remove(_key);

        if ( ! _exp )
                return;

        experiments.insert(_key, _exp);
}

// ---------- Konstantin --------------------
Experiment* TTTManager::getExperiment(const QString &_key) const
{
        return experiments[_key];
}

//void TTTManager::addPositionManager (const QString &_key, TTTPositionManager *_pm)
//{
//	//if there is already a position manager with this key, delete this first
//	TTTPositionManager *pm = m_positionManagers [_key.toInt()];
//	if (pm) {
//		m_positionManagers.remove (_key.toInt());
//	}
//	
//	if (! _pm)
//		return;
//	
//	//we must save memory! -> only created on demand
//	//_pm->createForms();
//	
//	m_positionManagers.insert (_key.toInt(), _pm);
//}
	
//TTTPositionManager* TTTManager::getPositionManager (const QString &_key) const
//{
///*	if (! positionManagers [_key])
//		throw new Exception*/
//	//return m_positionManagers [_key.toInt()];
//
//	return getPositionManager(_key.toInt());
//}

void TTTManager::createForms()
{	
	//NOTE: all forms that are independent of others should be created first
	//NOTE: if one form depends on another already in the constructor, the latter one
        //       must be created first...
	
#ifndef TREEANALYSIS
	if (frmMainWindow)
		//method already run
		return;
	
        ////create the menu bar...
        //MenuManager::getInst().createMenus();

        //independent forms (do not rely on the existence of others in their constructor)
	frmTreeStyleEditor = new TTTTreeStyleEditor();
	frmSymbolSelection = new TTTSymbolSelection();
	frmCellProperties = new TTTCellProperties();
	frmTreeMap = new TTTTreeMap();
	//frmPositionStatistics = new TTTPositionStatistics();
	frmPositionDetails = new TTTPositionDetails();
	frmTextbox = new TTTTextbox();
	frmTracksOverview = new TTTTracksOverview();
	frmPositionXMLDisplay = new TTTPositionXMLDisplay();
	frmMainWindow = new TTTMainWindow();

	frmTreeMerging = new TTTTreeMerging();
	frmLogFileConverter = new TTTLogFileConverter();
	
	//dependent forms
	frmPositionLayout = new TTTPositionLayout();
	frmTracking = new TTTTracking();
	frmAutoTracking = new TTTAutoTracking();
	
	////...and insert the necessary menus into the already existing forms
	//MenuManager::getInst().equipWithMenu (frmTracking);
	
#endif

	/*frmStatistics = 0;
	frmStatsMainWindow = 0;
	frmStatisticsPlot = 0;*/
	frmStaTTTsMain = 0;
	frmStatsMainWindow = 0;
	frmStatisticsPlot = 0;
}

std::vector<TTTPositionManager*> TTTManager::getAllPositionManagersSorted() const
{
	// Result
	std::vector<TTTPositionManager*> result(m_positionManagers.size());
	
	// Copy pointers
	int i = 0;
	for(auto it = m_positionManagers.constBegin(); it != m_positionManagers.constEnd(); ++it)
		result[i++] = (*it);

	// Sort
	std::sort(result.begin(), result.end(), [](TTTPositionManager* a, TTTPositionManager* b) { return a->positionInformation.getIndex() < b->positionInformation.getIndex(); });

	// Done
	return result;
}

//PositionManagerVector* TTTManager::getAllPositionManagersSorted()
//{
//	//copy pointers to the position managers into a QPtrVector
//	PositionManagerVector *result = new PositionManagerVector();
//	result->resize (positionManagers.size());
//	
//	Q3DictIterator<TTTPositionManager> iter (positionManagers);
//	uint cc = 0;
//	for ( ; iter.current(); ++iter) {
//		if (cc >= result->size())
//			result->resize (cc + 20);
//		
//		result->insert (cc, iter.current());
//		cc++;
//	}
//	
//	//sort the vector
//	result->sort();
//	
//	return result;
//}

void TTTManager::setTimepoint (int _timepoint, int _wavelength, int _zIndex)
{
	int oldtp = getTimepoint();
	
	if (oldtp == _timepoint)
		return;
	
	//set the same timepoint in all position managers
	//Q3DictIterator<TTTPositionManager> iter (getAllPositionManagers());
	//for ( ; iter.current(); ++iter) {
	//	iter.current()->setTimepoint (_timepoint, _wavelength);
	//}
	for(auto it = m_positionManagers.begin(); it != m_positionManagers.end(); ++it)
		(*it)->setTimepoint (_timepoint, _wavelength, _zIndex);
	
	clearFields();
	
	emit timepointSet (_timepoint, oldtp);
}

void TTTManager::setTimepoint_withoutSignal (int _timepoint, int _wavelength, int _zIndex)
{
	if (getTimepoint() == _timepoint)
		return;
	
	//Q3DictIterator<TTTPositionManager> iter (getAllPositionManagers());
	//for ( ; iter.current(); ++iter) {
	//	iter.current()->setTimepoint_withoutSignal (_timepoint, _wavelength);
	//}
	for(auto it = m_positionManagers.begin(); it != m_positionManagers.end(); ++it)
		(*it)->setTimepoint_withoutSignal (_timepoint, _wavelength, _zIndex);
	
	clearFields();
}

//int TTTManager::getTimepoint() const
//{
//	if (getCurrentPositionManager())
//		return getCurrentPositionManager()->getTimepoint();
//	else
//		return -1;
//}

void TTTManager::clearFields()
{
	if (currentTracks) {
		currentTracks->clear();
		delete currentTracks;
		currentTracks = 0;
	}
	if (currentTrackPoints) {
		currentTrackPoints->clear();
		delete currentTrackPoints;
		currentTrackPoints = 0;
	}
}

Q3IntDict<Track>* TTTManager::getCurrentTracks()
{
	if ((! currentTracks) || currentTracks->isEmpty())
		setCurrentTracks (getTimepoint());
	
	return currentTracks;
}

Q3ValueList<TrackPoint>* TTTManager::getCurrentTrackPoints()
{
	if ((! currentTrackPoints) || currentTrackPoints->empty())
		setCurrentTrackPoints (getTimepoint());
	
	return currentTrackPoints;
}

void TTTManager::setCurrentTracks (int _timepoint)
{
	if (tree)
		currentTracks = tree->timePointTracks (_timepoint);
}

void TTTManager::setCurrentTrackPoints (int _timepoint)
{
	if (tree)
		currentTrackPoints = tree->timePointTrackPoints (_timepoint);
}

bool TTTManager::trackingWasForward() const
{
	/**
	 * Tracking was forward if:
	 * - first time point of tracked cell after tracking is >= first time point before tracking
	 * - or if current time point is bigger than first time point of cell before tracking (second case can occur e.g. if user tracks
	 *	 a few track points before the first time point of an already tracked cell and then continues with forward tracking)
	 */
	return TrackingEndFirstTimepoint >= TrackingStartFirstTimepoint || getTimepoint() > TrackingStartFirstTimepoint;
}

bool TTTManager::startTracking(bool noChangeTimePoint)
{
#ifndef TREEANALYSIS
/*	if (! getCurrentTrack())
		return false;*/
	if (! frmTracking)
		return false;
	if (! frmCellProperties)
		return false;
	if (! tree)
		return false;

	// Get current position manager
	TTTPositionManager* curTTTPM = getCurrentPositionManager();
	if (!curTTTPM)
		return false;
	if (! curTTTPM->frmMovie)
		return false;
	if (Tracking)
		return false;
	
	if (curTTTPM->frmMovie->inStartingCellSelectionMode())
		//the movie window is still in a different mode and cannot be used for tracking
		return false;
	
	if (frmCellProperties)
		frmCellProperties->selectNothing();

	resetTrackingCellProperties();
	
	//if there is no cell/track currently selected, nothing can be tracked
	if (! getCurrentTrack()) {	
		if (tree->getTrackCount() <= 1) {
			//nothing yet tracked, the base cell is assumed
			setCurrentTrack (tree->getBaseTrack());
			if (! getCurrentTrack())
				//if assigning the base cell failed
				return false;
		}
		else
			return false;
	}

	// Inherit wavelength settings from last trackpoint/mother cell
	inheritCellProperties();
	
	//set tracking group box in TTTMovie to be displayed
	curTTTPM->frmMovie->tobMovie->setCurrentIndex (2);
	
	if (! curTTTPM->frmMovie->isVisible())
		curTTTPM->frmMovie->show();
	
	curTTTPM->frmMovie->raise();
	curTTTPM->frmMovie->activateWindow();
	
	/*
	frmCellProperties->show();
	frmCellProperties->raise();
	// Make sure window is on top and visible
	frmCellProperties->activateWindow();
	if(frmCellProperties->isMinimized())
		frmCellProperties->showNormal();
	*/

	if (getCurrentTrack()->tracked()) {
		//cell was already tracked before

		// Get first and last trace
		int lt = getCurrentTrack()->getLastTrace(),
			ft = getCurrentTrack()->getFirstTrace();

		if(backwardTrackingMode && ft < lt) {
			// Backward tracking mode and valid ft value -> use first trace
			if(!noChangeTimePoint)
				setTimepoint(ft);
		}
		else {
			if (getCurrentTrack()->getStopReason() == TS_NONE) {
				//tracking was interrupted before and is now continued

				if(!backwardTrackingMode || lt <= ft) {
					//set timepoint to the last trace of the current cell
					if(!noChangeTimePoint)
						setTimepoint (lt);
				}
			
				//display the cell properties at this timepoint
				frmCellProperties->setProperties (getCurrentTrack()->getTrackPoint (getCurrentTrack()->getLastTrace()).getPropObject());
			}
		}
	}
	else {
		//cell is not yet tracked
		if (getCurrentTrack()->getMotherTrack()) {
			//set the slider and picture in frmMovie to the earliest possible starting point of the track
			TTTPositionManager* tttpm = getCurrentPositionManager();
			if(!noChangeTimePoint && tttpm) {

				// OH-160322: just set time point to first trace (and make sure image is loaded)
				int ft = getCurrentTrack()->getFirstTrace();
				if(ft > 0) {
					PictureArray* pa = tttpm->getPictures();
					if(pa) {
						int wl = std::max(tttpm->getWavelength(),0);
						int zIndex = std::max(tttpm->getZIndex(),0);
						pa->loadOnePicture(PictureIndex(ft, zIndex, wl));
					}
					setTimepoint(ft);
				}

				/*
				PictureArray* pa = tttpm->getPictures();
				int wl = std::max(tttpm->getWavelength(),0);
				int zIndex = std::max(tttpm->getZIndex(),0);
				if(pa) {
					int newTimePoint = pa->nextLoadedPicture(PictureIndex(getCurrentTrack()->getFirstTrace(), zIndex, wl)).TimePoint;
					if(newTimePoint > 0)
						setTimepoint (newTimePoint);
				}
				*/
			}
		}
	}
	
	
	//store current first trackpoint to decide whether tracking was forward or backward
	TrackingStartFirstTimepoint = getCurrentTrack()->getTrackPointByNumber (1).TimePoint;

	/*
	if (TrackingStartFirstTimepoint == -1) {
		//the first cell was tracked and additionally was untracked before - use the current timepoint as a rough indicator instead
		TrackingStartFirstTimepoint = getTimepoint();
	}
	*/
	// 05.01.11 OH: Removed: caused severe bug when user goes into trackingmode at timepoint y and then goes
	// back some timepoints before actually starting to track -> stops tracking before reaching y -> ttt thinks tracking was backward ->
	// if stop mode is division, complete track gets deleted!

	
	Tracking = true;
	
	//now start tracking (in all forms!)
        //note: for the current movie form, tracking is started separately
	//Q3DictIterator<TTTPositionManager> iter (positionManagers);
 //   	for( ; iter.current(); ++iter )
 //   		if (iter.currentKey() != currentPositionKey)
 //   			if (iter.current()->frmMovie)
 //       			iter.current()->frmMovie->startTracking (false);

	for(auto it = m_positionManagers.begin(); it != m_positionManagers.end(); ++it) {
		if(it.key() != m_currentPositionKey && (*it)->frmMovie) {
			(*it)->frmMovie->startTracking (false);
		}
	}
	
	//start tracking in the movie window of the current position
	return curTTTPM->frmMovie->startTracking (true);
#else
	return false;
#endif

}

bool TTTManager::stopTracking(bool _notifyTTTAutoTrackingWindow)
{
#ifndef TREEANALYSIS
	if (! getCurrentTrack())
		return false;
	if (! frmTracking)
		return false;
	if (! frmCellProperties)
		return false;
	if (! tree)
		return false;
	if (! Tracking)
		return false;

	// Get current position manager
	TTTPositionManager* curTTTPM = getCurrentPositionManager();
	if (!curTTTPM)
		return false;
	if (! curTTTPM->frmMovie)
		return false;

	frmCellProperties->hide();
	
	//store new first trackpoint
	TrackingEndFirstTimepoint = getCurrentTrack()->getTrackPointByNumber (1).TimePoint;
	
	bool trackingOK = false;
	
	int ltp = curTTTPM->getLastTimePoint();
	int ftp = curTTTPM->getFirstTimePoint();
	
	if (trackingWasForward()) {
		// No backward tracking occurred
		setBackwardTrackingMode(false);

		if (getTrackStopReason() == TS_DIVISION) {
			
			if (! getCurrentTrack()->hasChildren()) {
				//case F(orward)1: new stop reason division and no kids present
				//        => OK, create the kids and set TS_DIVISION
				
				trackingOK = true;
				
				//2010/01/07: old method not anymore desired
				//old: timepoint of division was the last tracked timepoint of the current cell
				///int div_tp = getCurrentTrack()->getLastTrace();
				//new: timepoint of division is the current timepoint -> we additionally have to prune the current cell
				int div_tp = getTimepoint();

				//03.03.2011-OH Fix: Don't set div_tp to current timepoint if no trackpoint exists for this tp
				// as we may run into trouble if there is no trackpoint at "LastTrace" for any track
				//if(trackingWasForward())
				div_tp = std::min(div_tp, getCurrentTrack()->getLastTrace());
				
				getCurrentTrack()->setLastTrace (div_tp);
				getCurrentTrack()->cutOffTrackPoints();
				
				//create two new tracks for the children 1 and 2
				Track *tmp = new Track (getCurrentTrack(), 1, ltp, div_tp + 1);
				tmp->setNumber (getCurrentTrack()->getNumber() * 2);
				tree->insert (tmp);
				
				tmp = new Track (getCurrentTrack(), 2, ltp, div_tp + 1);
				tmp->setNumber (getCurrentTrack()->getNumber() * 2 + 1);
				tree->insert (tmp);
				
				getCurrentTrack()->setStopReason (TS_DIVISION);
				
			}
			else {
			
				//case F2: unchanged stop reason division and kids present
				//        - if stopTP > CT.firstTrace & stopTP < CT.kids.lastTrace
				//          => OK, shift children timepoint forward/backward
				//        - else
				//          => not OK
				
				int earliestKidStop = getCurrentTrack()->getChildTrack (1)->getLastTrace();
				earliestKidStop = QMIN (earliestKidStop, getCurrentTrack()->getChildTrack (2)->getLastTrace());
				
				if ((earliestKidStop == -1) ||
                                    ((getTimepoint() > getCurrentTrack()->getFirstTrace()) &
                                     (getTimepoint() < earliestKidStop))) {
					
					trackingOK = true;
					
					getCurrentTrack()->setLastTrace (getTimepoint());
					getCurrentTrack()->getChildTrack (1)->setFirstTrace (getTimepoint() + 1);
					getCurrentTrack()->getChildTrack (2)->setFirstTrace (getTimepoint() + 1);
					
					getCurrentTrack()->setStopReason (TS_DIVISION);
				}
				else
					QMessageBox::information (0, "Warning", "The stop reason of an existing cell can not\nbe set after existing children's last trace!");
				
			}
		}
		else {
			//case F3: other stop reason than division
			//        - if no kids present
			//          => OK, just set stop reason
			//        - if kids present
			//          => not OK
			
			if (! getCurrentTrack()->hasChildren()) {
				trackingOK = true;
				
				getCurrentTrack()->setStopReason (getTrackStopReason());
			}
			else {
				
				if (getTrackStopReason() == TS_NONE) {
					//tracking was interrupted => nothing is changed
					
					trackingOK = true;
				}
				else
					QMessageBox::information (0, "Warning", "You cannot the change the stop reason, if children exist.\nDelete them first.", "Ok");
				
			}
		}
		
	}
	else {
		//tracking was backward
		setBackwardTrackingMode(true);
		
		if (getCurrentTrack() == tree->getBaseTrack()) {
			
			if (getTrackStopReason() == TS_DIVISION) {
				//case B(ackward)1: base cell tracked and stop reason == division
				//	   => OK, create reverse division
				
				//agenda for creating a reverse division:
				//1)	old track 1 becomes new track 2 (new tracks are 1 and 3)
				//2) 	create 2 new tracks
				//3)	new track 1 starts at CurrentTimePoint - 1
				//	new tracks 2 & 3 start at CurrentTimePoint
				
				//1) shift track numbers (1 -> 2, cascading)
				tree->shiftTrackNumbers (tree->getBaseTrack(), 2);
				
				//2) create new basetrack and new track 3
				int tmpTimePoint = getTimepoint(); // + 1;	//+1 can cause problems if the timepoint does not exist
				if (getTimepoint() == ftp)
					tmpTimePoint++;		//the children must start later then the mother cell!
				
				//new base track
				Track *tmp = new Track (0, 0, ltp, tmpTimePoint);
				tmp->setNumber (1);
				tmp->setLastTrace (tmpTimePoint - 1);
				tmp->setChildTrack (1, tree->getTrack (2));
				tmp->setStopReason (TS_DIVISION);
				tree->insert (tmp);
				
				//new track 3
				tmp = new Track (tmp, 2, ltp, tmpTimePoint);
				tmp->setNumber (3);
				tree->insert (tmp);
				
				//3) set correct timepoint
				tree->getTrack (2)->setFirstTrace (tmpTimePoint);
				tree->getTrack (2)->setSecondsFromBegin (0);
				
				trackingOK = true;
			}
			else {
				//case B2: base cell tracked and stop reason != division
				//         - if no kids present
				//           => OK
				//         - if kids present
				//           => not OK
				
				if (! getCurrentTrack()->hasChildren()) {
					trackingOK = true;
					
					getCurrentTrack()->setStopReason (getTrackStopReason());
				}
				else {
					trackingOK = false;
				}
			}
			
			
		}
		else {
			//case B3: other cell than base cell tracked backwards
			//         => not OK
		}
	}
	
	//switch off tracking buttons in every movie window
	//Q3DictIterator<TTTPositionManager> iter (positionManagers);
 //   for( ; iter.current(); ++iter )
	//	if (iter.current()->frmMovie)
	//		iter.current()->frmMovie->switchTrackingButtons (false);
	for(auto it = m_positionManagers.begin(); it != m_positionManagers.end(); ++it) {
		if((*it)->frmMovie) {
			(*it)->frmMovie->switchTrackingButtons (false);
		}
	}
	
	//update display stuff in frmTracking
	frmTracking->stopCellTracking();
	
	Tracking = false;

	// Notify autotracking
	if(_notifyTTTAutoTrackingWindow && frmAutoTracking && frmAutoTracking->getTreeWindow()) {
		frmAutoTracking->getTreeWindow()->notifyManualTrackingFinished();
	}
	
	return trackingOK;
#else
	return false;
#endif
}

void TTTManager::resetBasePosition()
{
#ifndef TREEANALYSIS
	//unload all pictures
	//Q3DictIterator<TTTPositionManager> iter (positionManagers);
 //   	for( ; iter.current(); ++iter ) {
 //   		
 //   		//unload all pictures
 //   		if (iter.current()->getPictures())
 //   			iter.current()->getPictures()->unloadAllPictures(/*false*/);
	//	
	//	//delete all "P" signs in the position layout as there are no loaded picture anymore
	//	//NOTE: creates a SIGSEV signal
	//	if (iter.current()->posThumbnailInTTTPosLayout)
	//		iter.current()->posThumbnailInTTTPosLayout->markPicturesLoaded (false);
	//}

	for(auto it = m_positionManagers.begin(); it != m_positionManagers.end(); ++it) {
		if(PictureArray* pics = (*it)->getPictures())
			pics->unloadAllPictures();
		if((*it)->posThumbnailInTTTPosLayout)
			(*it)->posThumbnailInTTTPosLayout->markPicturesLoaded (false);
	}
#endif
	
	//commented on 2008/10/09 (by BS) as the picture loading sign can not be updated otherwise
	//setCurrentPosition ("");
	
	
	//reset base position
	setBasePosition (-1);
	
	clearFields();
	
#ifndef TREEANALYSIS
	frmPositionLayout->pbtLoadTTTFile->setEnabled (true);
	//frmPositionLayout->pbtNewColony->setEnabled (true);
#endif
	
}

void TTTManager::shutDownTTT (bool _closeTTTPositionLayout)
{
#ifndef TREEANALYSIS
        //close all forms (note: do not close TTTPositionLayout by default, as this is the calling form => endless loop...)
        if (_closeTTTPositionLayout)
                frmPositionLayout->close();

        if (frmSymbolSelection)
                frmSymbolSelection->close();

        //if (frmPositionStatistics)
        //        frmPositionStatistics->close();

        if (frmPositionDetails)
                frmPositionDetails->close();

        if (frmPositionXMLDisplay)
                frmPositionXMLDisplay->close();

        if (frmMainWindow)
                frmMainWindow->close();
#endif

#ifndef TREEANALYSIS
    if (frmTreeMerging)
            frmTreeMerging->close();

    if (frmTracking)
            frmTracking->close();

	if (frmLogFileConverter)
		frmLogFileConverter->close();
#endif

		saveGlobalSettings();
}

void TTTManager::saveGlobalSettings()
{
	#ifndef TREEANALYSIS
	
	//save user information

	//UserInfo::getInst().writeUserConfigFile();
	// ------- Oliver -------
	// User info is now saved when UserInfo object is being destroyed.
	// This ensures, that all window geometry information will be saved to disk.
	
	//save user specific style sheet
	UserInfo::getInst().saveUserStyleSheet();


	#endif
}

TTTPositionManager* TTTManager::getPositionAt (float _absX, float _absY)
{
	//scan through positions and see which ones contain the point
	//if two or more positions overlap there, take a random one
	
	float mmpp = TATInformation::getInst()->getWavelengthInfo (0).getMicrometerPerPixel();
	int width = (int)((float)TATInformation::getInst()->getWavelengthInfo (0).getWidth() * mmpp);
	int height = (int)((float)TATInformation::getInst()->getWavelengthInfo (0).getHeight() * mmpp);
	
	for(auto it = m_positionManagers.begin(); it != m_positionManagers.end(); ++it) {

		int left = (*it)->positionInformation.getLeft();
		int top = (*it)->positionInformation.getTop();
		if (coordinateSystemInverted) {
			if ((left >= _absX) &
				(top >= _absY) &
				(left - width <= _absX) &
				(top - height <= _absY)
				) {

					return (*it);

			}

		}
		else {
			if ((left <= _absX) &
				(top <= _absY) &
				(left + width >= _absX) &
				(top + height >= _absY)
				) {

					return (*it);

			}
		}
	}

    	
    return 0;
}

Q3IntDict<TTTPositionManager> TTTManager::getAllPositionsAt (float _absX, float _absY)
{
	//scan through positions and see which ones contain the point
	//add up all the positions and return them
	
	Q3IntDict<TTTPositionManager> result (6);		//usually, there are no more than 4 possible
	
	float mmpp = TATInformation::getInst()->getWavelengthInfo (0).getMicrometerPerPixel();
	int width = (int)((float)TATInformation::getInst()->getWavelengthInfo (0).getWidth() * mmpp);
	int height = (int)((float)TATInformation::getInst()->getWavelengthInfo (0).getHeight() * mmpp);
	
    for(auto it = m_positionManagers.begin(); it != m_positionManagers.end(); ++it) {
    		
    		int left = (*it)->positionInformation.getLeft();
    		int top = (*it)->positionInformation.getTop();
    		if (coordinateSystemInverted) {
                        if ((left >= _absX) &
                            (top >= _absY) &
                            (left - width <= _absX) &
                            (top - height <= _absY)
    			    ) {
    			    
    				result.replace (result.count(), (*it));
    			
    			}
    			
    		}
    		else {
                        if ((left <= _absX) &
                            (top <= _absY) &
                            (left + width >= _absX) &
                            (top + height >= _absY)
    			    ) {
    			    
    				result.replace (result.count(), (*it));
    			
    			}
    		}
    	}
    	
    	return result;
}

TTTPositionManager* TTTManager::getPositionLeftOf (const QString &_key)
{
	return getPositionSomewhereOf (_key, 3);
}

TTTPositionManager* TTTManager::getPositionRightOf (const QString &_key)
{
	return getPositionSomewhereOf (_key, 4);
}

TTTPositionManager* TTTManager::getPositionTopOf (const QString &_key)
{
	return getPositionSomewhereOf (_key, 1);
}

TTTPositionManager* TTTManager::getPositionBottomOf (const QString &_key)
{
	return getPositionSomewhereOf (_key, 6);
}

TTTPositionManager* TTTManager::getPositionSomewhereOf (const QString &_key, int _direction)
{
	//calc the center point of the given position, go left/... the width/height/both
	// and get the position at this point
	
	TTTPositionManager* tttpm = getPositionManager (_key);
	
	if (! tttpm)
		return 0;
	
	float mmpp = TATInformation::getInst()->getWavelengthInfo (0).getMicrometerPerPixel();
	int width = (int)((float)TATInformation::getInst()->getWavelengthInfo (0).getWidth() * mmpp);
	int height = (int)((float)TATInformation::getInst()->getWavelengthInfo (0).getHeight() * mmpp);
	
	if (coordinateSystemInverted) {
		width = - width;
		height = - height;
	}
    	
	int left = tttpm->positionInformation.getLeft();
	int top = tttpm->positionInformation.getTop();
	
	float x = left + width / 2;
	float y = top + height / 2;
	
	//_direction: 0 = topleft, 1 = top, 2 = topright, 3 = left, 4 = right, 5 = bottomleft, 6 = bottom, 7 = bottomright	
	switch (_direction) {
		case 0:
			x -= width;
			y -= height;
			break;
		case 1:
			y -= height;
			break;
		case 2:
			x += width;
			y -= height;
			break;
		case 3:
			x -= width;
			break;
		case 4:
			x += width;
			break;
		case 5:
			x -= width;
			y += height;
			break;
		case 6:
			y += height;
			break;
		case 7:
			x += width;
			y += height;
			break;
		default:
			;
	}
	
	return getPositionAt (x, y);
	
}

bool TTTManager::pointIsInPosition (const TTTPositionManager *_tttpm, const QPointF _point)
{
	if (! _tttpm)
		return false;
	
	float left = _tttpm->positionInformation.getLeft();
	float top = _tttpm->positionInformation.getTop();
	float mmpp = TATInformation::getInst()->getWavelengthInfo (0).getMicrometerPerPixel();
	int width = (int)((float)TATInformation::getInst()->getWavelengthInfo (0).getWidth() * mmpp);
	int height = (int)((float)TATInformation::getInst()->getWavelengthInfo (0).getHeight() * mmpp);
	float absX = _point.x();
	float absY = _point.y();
	
	if (coordinateSystemInverted) {
		if ((left >= absX) &
			(top >= absY) &
			(left - width <= absX) &
			(top - height <= absY)
			) {

				return true;

		}

	}
	else {
		if ((left <= absX) &
			(top <= absY) &
			(left + width >= absX) &
			(top + height >= absY)
			) {

				return true;

		}
	}
	
	return false;
}

void TTTManager::setRAMDisplaySettings (const BDisplay &_rds)
{
	ramDisplaySettings = _rds;
}

const BDisplay& TTTManager::getRAMDisplaySettings() const
{
	return ramDisplaySettings;
}

void TTTManager::showCustomizeDialog()
{
	if (! COMPFLAG_STYLESETTINGS)
		//module not available
		return;
	
	if (! (COMPFLAG_STYLESETTING_MOVIE_LAYOUT | COMPFLAG_STYLESETTING_TRACKING_KEYS | COMPFLAG_STYLESETTING_TREE_LAYOUT | COMPFLAG_STYLESETTING_VARIOUS))
		//module available, but none of its features
		return;
	
#ifndef TREEANALYSIS	//------------------ Konstantin --------------------
	if (frmTreeStyleEditor) {
		frmTreeStyleEditor->setStyleSheet (UserInfo::getInst().getRefStyleSheet());
		frmTreeStyleEditor->show();
	}
#endif                  //------------------ Konstantin --------------------
}

void TTTManager::quitProgram (const QString &_message, int _returnCode)
{
	if (! _message.isEmpty()) {
		QMessageBox::critical (0, "Fatal error", _message);
	}
	
	//all qApp methods do not work!!!??
/*	qApp->exit (_returnCode);
	qApp->quit();
	qApp->processEvents();*/
	
	exit (_returnCode);
}

QString TTTManager::getNextFreeFile (TTTPositionManager *_tttpm, bool _suffixOnly)
{
	//get highest colony index by scanning the existing ttt files in both old and new ttt file directory
	
	if (! _tttpm)
		return "";
	
	int highestColony = 0;

	QVector<QString> files_new = SystemInfo::listFiles (_tttpm->getTTTFileDirectory(), QDir::Files, "*" + TTT_FILE_SUFFIX, false);
	QVector<QString> files_old = SystemInfo::listFiles (_tttpm->getTTTFileDirectory (false, true), QDir::Files, "*" + TTT_FILE_SUFFIX, false);

	QVector<QString> files = Tools::joinValueVectors<QString> (files_new, files_old);

	for (QVector<QString>::const_iterator fileIter = files.constBegin(); fileIter != files.constEnd(); ++fileIter) {

		//full filename
		const QString text = (*fileIter);

		//get colony index
		int cpos = text.findRev (COLONY_MARKER);
		if (cpos > -1) {
			int colony = text.mid (cpos + 1, 3).toInt();
			if (colony > highestColony)
				highestColony = colony;
		}
	}
	
//old, from frmTracking	
/*	QString tmp = "";
	for (int i = 0; i < (int)lboColony->count(); i++) {
		if (! lboColony->text (i).isEmpty()) {
			tmp = lboColony->text (i); 
			tmp = tmp.mid (tmp.findRev (COLONY_MARKER));
			tmp = tmp.left (4);
			if (tmp.left (1) == COLONY_MARKER) 
				if (tmp.mid (1, 3).toInt() > highestColony)
					highestColony = tmp.mid (1, 3).toInt();
		}
	}*/
	
	QString suffix = QString().sprintf (COLONY_MARKER + "%03d", highestColony + 1);
	
	if (_suffixOnly) {
		return suffix;
	}
	else {
		return _tttpm->getTTTFileDirectory() + _tttpm->getBasename() + suffix + TTT_FILE_SUFFIX;
		
	}
}

void TTTManager::setTTTConfigDocument (QDomDocument &_dom)
{
        tttConfigDOM = _dom;
}

QDomDocument& TTTManager::getTTTConfigDocument()
{
        return tttConfigDOM;
}

void TTTManager::addTrackpoint( int _timepoint, float _x, float _y, float _diameter, bool _setBackground, short int _mouseButtons, QString _position )
{
	// Only call this function in tracking mode
	if(!Tracking) {
		qWarning() << "tTt Warning: addTrackpoint() called while not in tracking mode";
		return;
	}

	// Add track
	getCurrentTrack()->addMark(_timepoint, _x, _y, _diameter, _setBackground, _mouseButtons, _position);

	// If user pressed mouse buttons, this overwrites adherence settings
	if(_mouseButtons != 0) {
		trackingCellProperties.Set_FreeFloating = false;
		trackingCellProperties.Set_NonAdherent = false;
	}

	// Set properties
	trackingCellProperties.Set_Addon = true;
	getCurrentTrack()->setProperties(_timepoint, trackingCellProperties, true);

	// Reset trackingCellProperties
	if(_mouseButtons != 0) {
		trackingCellProperties.Set_FreeFloating = true;
		trackingCellProperties.Set_NonAdherent = true;
	}

	// If we have no first trace yet, this must be the first trace
	if(TrackingStartFirstTimepoint == -1)
		TrackingStartFirstTimepoint = _timepoint;
	else {
		// Check if tracking direction is backward
		if(_timepoint < TrackingStartFirstTimepoint)
			setBackwardTrackingMode(true);
		else
			setBackwardTrackingMode(false);
	}
}

void TTTManager::destroy()
{
	if(inst) {
		delete inst;
		inst = 0;
	}
}

void TTTManager::inheritCellProperties()
{
#ifndef TREEANALYSIS
	// If track has been tracked already, take wls from last trackpoint
	Track *currentTrack = getCurrentTrack();
	if(!currentTrack)
		return;

	// Get last timepoint
	int timepoint = currentTrack->getLastTrace();  //getTimepoint();

	// If not, take last trackpoint from mothertrack
	if(!currentTrack->tracked()) {
		if(currentTrack->getMotherTrack())  {
			currentTrack = currentTrack->getMotherTrack();
			timepoint = currentTrack->getLastTrace();
		}
		else
			// Not tracked yet and no mother track
			currentTrack = 0;
	}

	if(!currentTrack || timepoint == -1)
		return;

	// Get trackpoint
	const TrackPoint& tp = currentTrack->getTrackPoint(timepoint);

	// Check for dummy trackpoint
	if(tp.TimePoint == -1)
		return;

	// If all properties should be inherited
	bool inheritAll = frmTracking->getInheritAllProperties();

	// Iterate over inherited WLs
	bool dataChanged = false;
	const QVector<bool> *inheritedWLs = frmTracking->getInheritedWLs();
	for(unsigned int i = 0; i < inheritedWLs->size() && i < MAX_WAVE_LENGTH; ++i) {
		if(inheritAll || inheritedWLs->at(i)) {
			// Check if reference trackpoint has wavelength i + 1 set
			if(tp.WaveLength[i+1] != 0) {
				// Take over..
				trackingCellProperties.WaveLength[i+1] = 1;
				dataChanged = true;
			}
		}
	}

	// Inherit adherence
	if(inheritAll || frmTracking->getInheritAdherence()) {
		trackingCellProperties.FreeFloating = tp.FreeFloating;
		trackingCellProperties.NonAdherent = tp.NonAdherent;
		dataChanged = true;
	}

	// Inherit differentiation
	if(inheritAll || frmTracking->getInheritDifferentiation()) {
		trackingCellProperties.tissueType = static_cast<TissueType>(tp.tissueType);
		trackingCellProperties.cellGeneralType = static_cast<CellGeneralType>(tp.cellGeneralType);
		trackingCellProperties.cellLineage = tp.cellLineage;
		dataChanged = true;
	}

	// Keep movie window menus synchronized
	if(dataChanged)
		sychronizeCellPropertiesInMovieWindows();
#endif
}

void TTTManager::resetTrackingCellProperties()
{
	// Differentiation: none
	trackingCellProperties.tissueType = TT_NONE;
	trackingCellProperties.cellGeneralType = CGT_NONE;
	trackingCellProperties.cellLineage = 0;

	// Wavelengths: all off
	for(unsigned int i = 0; i < MAX_WAVE_LENGTH + 1; ++i)
		trackingCellProperties.WaveLength[i] = 0;

	// Adherence: adherent
	trackingCellProperties.NonAdherent = false;
	trackingCellProperties.FreeFloating = false;

	// Endomitosis: off
	trackingCellProperties.EndoMitosis = false;

	// Keep movie window menus synchronized
	sychronizeCellPropertiesInMovieWindows();
}

void TTTManager::setTrackingCellAdherence( ADHERENCE_STATE _trackingAdherence )
{
	// Set adherence
	switch(_trackingAdherence) {
	case AS_SPYHOP:
		trackingCellProperties.NonAdherent = true;
		trackingCellProperties.FreeFloating = false;
		break;
	case AS_ADHERENT:
		trackingCellProperties.NonAdherent = false;
		trackingCellProperties.FreeFloating = false;
		break;
	case AS_FREEFLOATING:
		trackingCellProperties.NonAdherent = true;
		trackingCellProperties.FreeFloating = true;
		break;
	default:
		trackingCellProperties.NonAdherent = false;
		trackingCellProperties.FreeFloating = true;
	}

	// Keep movie window menus synchronized
	sychronizeCellPropertiesInMovieWindows();
}

void TTTManager::setTrackingEndomitosis( bool _on )
{
	// Set endomitosis
	trackingCellProperties.EndoMitosis = _on;

	// Keep movie window menus synchronized
	sychronizeCellPropertiesInMovieWindows();
}

void TTTManager::setTrackingDifferentiation( TissueType _trackingTissueType, CellGeneralType _trackingCellGeneralType, char _trackingCellLineage )
{
	// Set differentiation
	trackingCellProperties.tissueType = _trackingTissueType;
	trackingCellProperties.cellGeneralType = _trackingCellGeneralType;
	trackingCellProperties.cellLineage = _trackingCellLineage;

	// Keep movie window menus synchronized
	sychronizeCellPropertiesInMovieWindows();
}

void TTTManager::setTrackingWavelength( unsigned int _wavelength, bool _on )
{
	// Only 0 to MAX_WAVE_LENGTH+1 allowed
	if(_wavelength > MAX_WAVE_LENGTH) {
		qWarning() << "setTrackingWavelength: invalid wavelength: " << _wavelength;
		return;
	}

	// Set wavelength
	trackingCellProperties.WaveLength[_wavelength] = _on ? 1 : 0;

	// Keep movie window menus synchronized
	sychronizeCellPropertiesInMovieWindows();
}

void TTTManager::sychronizeCellPropertiesInMovieWindows()
{
#ifndef TREEANALYSIS
	// Iterate over positions
	for(auto it = m_positionManagers.begin(); it != m_positionManagers.end(); ++it) {
		// Update properties in movie window
		TTTPositionManager *cur = (*it);
		if(cur && cur->frmMovie) 
			cur->frmMovie->setSelectedTrackingProperties(trackingCellProperties);
	}
#endif
}

const CellProperties& TTTManager::getTrackingCellProperties() const
{
	return trackingCellProperties;
}

void TTTManager::connectForms()
{
#ifndef TREEANALYSIS
	connect ( frmTreeStyleEditor, SIGNAL (styleChosen (StyleSheet &)), frmTracking, SLOT (applyStyleSheet (StyleSheet &)));
#endif
}

void TTTManager::setBackwardTrackingMode( bool _on )
{
	// Check if anything to do
	if(_on == backwardTrackingMode)
		return;

	// Change mode
	backwardTrackingMode = _on;
	emit backwardTrackingModeSet(_on);

#ifndef TREEANALYSIS	
	// Update display
	if(frmTracking)
		frmTracking->updateBackwardTrackingModeDisplay();
#endif
}

void TTTManager::redrawTracks()
{
#ifndef TREEANALYSIS	

	// Iterate over all positions
	for(auto it = m_positionManagers.begin(); it != m_positionManagers.end(); ++it) {
		TTTPositionManager* curtttpm = (*it);

		// Skip uninitialized positions
		if(!curtttpm || !curtttpm->isInitialized() || !curtttpm->frmMovie)
			continue;

		// Redraw tracks
		curtttpm->frmMovie->getMultiplePictureViewer()->drawAdditions();
	}

#endif
}

void TTTManager::setFramesToSkipWhenTracking( int framesToSkipWhenTracking )
{
	// Change value here
	m_framesToSkipWhenTracking = framesToSkipWhenTracking;

#ifndef TREEANALYSIS	
	// Change value in all movie windows
	for(auto it = m_positionManagers.begin(); it != m_positionManagers.end(); ++it) {
		TTTPositionManager* curtttpm = (*it);

		// Skip uninitialized positions
		if(!curtttpm || !curtttpm->isInitialized() || !curtttpm->frmMovie)
			continue;

		// Update spinbox
		if(curtttpm->frmMovie->spbSkipFrames != sender())
			curtttpm->frmMovie->spbSkipFrames->setValue(m_framesToSkipWhenTracking);
	}
#endif
}











