/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
// Include QtGlobal for Q_WS_WIN
#include <QtGlobal>

// Only compile if this is windows
#ifdef Q_WS_WIN

#include "windowstools.h"
#include <windows.h>
#include <Psapi.h>


unsigned long long WindowsTools::getFreePhysicalRam()
{
	// Get memory information from windows
	MEMORYSTATUSEX statex;
	statex.dwLength = sizeof (statex);
	GlobalMemoryStatusEx (&statex);

	//// TEST
	//PROCESS_MEMORY_COUNTERS pmc;
	//GetProcessMemoryInfo(GetCurrentProcess(), (PPROCESS_MEMORY_COUNTERS)&pmc, sizeof(PROCESS_MEMORY_COUNTERS_EX));

	// Extract requested information
	return statex.ullAvailPhys;
}

unsigned long long WindowsTools::getFreeVirtualRam()
{
	// Get memory information from windows
	MEMORYSTATUSEX statex;
	statex.dwLength = sizeof (statex);
	GlobalMemoryStatusEx (&statex);

	// Extract requested information
	return statex.ullAvailVirtual;
}

#endif