/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MININGRESULTS_H
#define MININGRESULTS_H


#include "tttdata/littletree.h"


#include <QMap>
#include <QString>
#include <QVector>


class MiningResultsXMLParser;


/**
	@author Bernhard Schauberger <bernhard.schauberger@campus.lmu.de>
	
	This class stores the results of the tree mining process (including the previous clustering).
	It provides methods to parse the input and result files.

*/

class MiningResults {

public:
	MiningResults();
	
	~MiningResults();
	
	void setTMInputFile (const QString _tmFilename);
	
	bool parseFiles();
	
	LittleTree* findLittleTree (const QString _treeFilename);
	
	/**
	 * @return whether the files were read correctly
	 */
	bool isRead() const
		{return parsed;}
	
	
private:
	
	///the filename of the selected treeminer input file, result file and explanation file
	QString treeMinerFilename;
	QString resultFilename;
	QString explanationFilename;
	
	///this map stores the mapping from a tree filename to its index in the treeminer file
	QMap<QString, int> filename_to_index;
	///...and the reverse map
	QMap<int, QString> index_to_filename;
	
	
	///the number of frequent/input trees
	int freqTreeCount;
	int inputTreeCount;
	
	///arrays for the original trees and the frequent subtrees
	//Q3 IntDict<LittleTree> trees;
	//Q3IntDict<LittleTree> freq_trees;
	QVector<LittleTree> trees;
	QVector<LittleTree> freq_trees;
	
	///whether the input files were parsed successfully
	bool parsed;
	
	
	friend class MiningResultsXMLParser;
};

#endif
