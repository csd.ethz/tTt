/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef positionlayoutpicturelistmodel_h__
#define positionlayoutpicturelistmodel_h__

// Includes
#include <QAbstractListModel>
#include <QBitArray>
#include <QDir>

// Forward declarations
class QListView;


/**
 * @author Oliver Hilsenbeck
 *
 * Manages and servers as model for the list view of pictures. Remembers checkstate for each item in the list.
 * Checkstates are stored in a bit vector with row numbers as indices, therefore the order of the items must not be altered. 
 * Items will always be ordered alphabetically in ascending order.
 */
class PositionLayoutPictureListModel : public QAbstractListModel {

	Q_OBJECT

public:
	/**
	 * Constructor
	 * @param _lvwPictures Pointer to QListView object
	 */
	PositionLayoutPictureListModel(QListView* _lvwPictures);

	/**
	 * Set List of picture strings
	 * @param _strings QStringList with picture names
	 */
	void setStringList ( const QStringList& _strings );

	/**
	 * Get const reference to list of picture strings
	 * @return QStringList with picture names
	 */
	const QStringList& stringList() const {
		return fileNames;
	}

	/**
	 * Show/Hide pictures with specified wavelength
	 * @param _wl wavelength to be shown/hidden
	 * @param _hide hide or show
	 */
	void setWaveLengthHidden(int _wl, bool _hide = true);

	/**
	 * See if item in specified row is checked
	 * @param _row the row index, beginning with 0 and including hidden rows
	 * @return true if the item is checked
	 */
	bool isItemChecked(int _row) const {
		return checkedItems.testBit(_row);
	}

	/**
	 * Set check state of item at specified row
	 * @param _row the row index, beginning with 0 and including hidden rows
	 * @param _checked if the item should be checked or unchecked
	 */
	void setItemChecked(int _row, bool _checked = true);

	/**
	 * Get the number of checked items
	 * @return number of checked items
	 */
	int numCheckedPictures() const {
		return checkedItems.count(true);
	}

	/**
	 * Get number of pictures
	 * @return number of pictures, including hidden ones
	 */
	int numPictures() const {
		return fileNames.size();
	}

	/**
	 * @return Index of last visible item in list or -1 if none exists
	 */
	int getIndexOfLastVisibleItem() const;

	/**
	 * @return Index of first visible item in list or -1 if none exists
	 */
	int getIndexOfFirstVisibleItem() const;

	/**
	 * @return Index of last checked item in list or -1 if none exists
	 */
	int getIndexOfLastCheckedItem() const;

	/**
	 * @return Index of first checked item in list or -1 if none exists
	 */
	int getIndexOfFirstCheckedItem() const;

	///**
	// * Get a StringList with all picture names, indices are same as row numbers
	// */
	//QStringList getPictureNames() {
	//	return stringList();
	//}

	/**
	 * Set if item with specified row index should be hidden
	 */
	void setItemHidden(int _row, bool _hide = true);

	/// setData() - Reimplemented from QAbstractItemModel
	bool setData ( const QModelIndex& index, const QVariant& value, int role = Qt::EditRole );

	/// data() - Reimplemented from QAbstractItemModel
	QVariant data ( const QModelIndex& index, int role = Qt::DisplayRole ) const;

	/// flags() - Reimplemented from QAbstractItemModel
	Qt::ItemFlags flags ( const QModelIndex& index ) const;

	/// rowCount() - Reimplemented from QAbstractItemModel
	int rowCount(const QModelIndex& _index) const {
		if(_index.isValid())
			return 0;
		return fileNames.size();
	}

signals:

	/**
	 * Emitted when checkstate of one item in the list changed
	 */
	void itemSelectionChanged();

private:
/// Private data

	/// The list of data contained in this model
	QStringList fileNames;

	/// Checked pictures, hidden items should be unchecked
	QBitArray checkedItems;

	/// The listview
	QListView* lvwPictures;
};

#endif // positionlayoutpicturelistmodel_h__