/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "miningresults.h"

#include "tttio/xmlhandler.h"
//Added by qt3to4:
#include <QTextStream>

MiningResults::MiningResults()
{
	treeMinerFilename = "";
	
	// 01.03.2011-OH: changed trees from Q3IntDict to QVector
	//trees.setAutoDelete (true);
	//trees.setAutoDelete (true);
	
	parsed = false;
}


MiningResults::~MiningResults()
{
}


bool MiningResults::parseFiles()
{
	
	if (treeMinerFilename.isEmpty())
		return false;
	
	//parse result file and build a LittleTree for each frequent subtree
	//==================================================================
	
	/* example result output file:
	 	...
		MINSUPPORT : 6 (1)
		1 - 6
		3 - 6
		5 - 6
		1 3  - 6
		1 3 -1 5  - 6
		1 5 3  - 6
		1 5 3 -1 -1 5  - 6
		F1 - 3 6
		...
	 *
	 * the relevant parts are between MINSUPPORT and F1 (both exclusive)
	 * format of these lines: tree - absolute frequency
	 * 
	 */
	
	QFile file (resultFilename);
	if (file.open (QIODevice::ReadOnly)) {

		QTextStream ts (&file);

		QString line;
		bool data = false;
		uint cc = 0;
		
		while (! ts.atEnd()) {
			line = ts.readLine();
			
			if (line.left (10) == "MINSUPPORT") {
				data = true;
			}
			else if (line.left (2) == "F1") {
				data = false;
			}
			else if (data) {
				int posDash = line.findRev (" - ");
				QString treeS = line.left (posDash);
				treeS = treeS.stripWhiteSpace();
				QString countS = line.mid (posDash + 3);
				//int count = countS.toInt();
				
				//LittleTree *lt = new LittleTree();
				LittleTree lt;

				//build tree from string
				if (lt.set (treeS)) {
					
					//lboFrequentSubtrees->insertItem (QString ("[%1] %2; abs.freq. = %3").arg (cc + 1).arg (treeS).arg (count));
					
					if (cc >= freq_trees.size())
						freq_trees.resize (cc * 2 + 1);
					
					// 01.03.2011-OH: changed trees from Q3IntDict to QVector
					//freq_trees.insert (cc, lt);
					freq_trees[cc] = lt;

					cc++;
					freqTreeCount = cc;
				}
				
			}
		}
		file.close();
	}
	
	
	//display input trees in TreeMiner format
	//=======================================
	file.setName (treeMinerFilename);
	if (file.open (QIODevice::ReadOnly)) {
	
		QTextStream ts (&file);
		
		QString line;
		uint cc = 0;
		
		while (! ts.atEnd()) {
			line = ts.readLine();
			
			//remove first 3 digits for parsing
			int pos = line.find (" ") + 1;
			uint treeID = line.mid (0, pos - 1).toInt();
			for (int i = 0; i < 2; i++)
				pos = line.find (" ", pos + 1);
			QString line2 = line.mid (pos + 1);
			
			LittleTree lt = LittleTree (line2, true);
			
			//each line is a data line -> input tree
			//if (lt) {
				
/*				line2 = QString ("[%1] ").arg (cc + 1) + line2;
				txtInputTrees->append (line2);*/
				
				if (treeID >= trees.size())
					trees.resize (treeID * 2 + 1);
				
				// 01.03.2011-OH: changed trees from Q3IntDict to QVector
				//trees.insert (treeID, lt);
				trees[treeID] = lt;
				
				cc++;
				inputTreeCount = cc;
			//}
		}
		file.close();
	}
	
	
	
	//read XML explanation file
	//=========================
	XMLHandler::readMiningExplanations (explanationFilename, this);
	
	parsed = true;
	
	return parsed;
}

void MiningResults::setTMInputFile (const QString _tmFilename)
{
	treeMinerFilename = _tmFilename;
	resultFilename = treeMinerFilename + "_results";
	explanationFilename = treeMinerFilename + "_expl.xml";
	
	trees.clear();
	freq_trees.clear();
	
	parsed = false;
}

LittleTree* MiningResults::findLittleTree (const QString _treeFilename)
{
	//cut off path from filename (only pure filename is needed, as for mining and tracking there might be different locations)
	QString tmpFilename = _treeFilename.mid (_treeFilename.findRev ('/') + 1);
	
	int number = filename_to_index [tmpFilename];

	if(number >= trees.size() || number < 0) {
		qWarning() << "tTt: MiningResults::findLittleTree - Error 1 - " << number << " - " << trees.size();
		return 0;
	}
	
	//return trees.find (number);
	return &(trees[number]);
}

