/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef changecursorobject_h__
#define changecursorobject_h__

/**
	@author Oliver Hilsenbeck 
	
	Simple class that changes the application cursor when created and restores it when being destroyed.
	This can be used to change the cursor and ensure that it will be changed back, even in case of exceptions.
*/

// Qt
#include <QApplication>
#include <QWidget>


class ChangeCursorObject {
public:

	/**
	 * Constructor, changes cursor
	 * @param _cursor the desired cursor, WaitCursor by default
	 */
	ChangeCursorObject( const QCursor& cursor = QCursor(Qt::WaitCursor), QWidget* widget = nullptr ) 
		: m_widget(widget), m_cursorRestored(false)
	{
		// Change cursor
		if(m_widget) {	
			m_oldCursor = m_widget->cursor();
			m_widget->setCursor(cursor);
		}
		else {
			QApplication::setOverrideCursor(cursor);
		}

		// Required in QT5 for cursor change to take effect
		QApplication::processEvents();
	}

	/**
	 * Destructor, restored cursor
	 */
	~ChangeCursorObject() {
		// Restore cursor
		restoreCursor();
	}

	/**
	 * Restore cursor, can be used to restore cursor before destruction of object.
	 */
	void restoreCursor() {
		if(m_cursorRestored)
			return;

		// Restore cursor
		if(m_widget)
			m_widget->setCursor(m_oldCursor);
		else
			QApplication::restoreOverrideCursor();

		m_cursorRestored = true;
	}

private:

	// Widget whose cursor was changed, can be nullptr
	QWidget* m_widget;

	// Old cursor of widget (only set if m_widget is set)
	QCursor m_oldCursor;

	// If old cursor has been restored already
	bool m_cursorRestored;
};


#endif // changecursorobject_h__
