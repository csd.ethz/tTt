/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "treestatistics.h"

TreeStatistics::TreeStatistics()
	: totalcount (0),  divcount (0),  apocount (0), intercount (0), lostcount (0)
{
	generationCellcounts.resize (0);
}


TreeStatistics::~TreeStatistics()
{}

TreeStatistics TreeStatistics::calculate (const Tree *_tree)
//NOTE: implemented twice due to speed reasons
{
	TreeStatistics treestat;
	
	if (! _tree)
		return treestat;
	
	treestat.generationCellcounts.resize (_tree->getNumberOfGenerations() + 1);
	
	Track *tmpTrack = 0;
	Q3IntDict<Track> tracks = _tree->getAllTracks (-1);
	for (uint i = 0; i < tracks.size(); i++) {
		tmpTrack = tracks.find (i);
		if (tmpTrack) {
			
			//count for stop reasons
			switch (tmpTrack->getStopReason()) {
				case TS_DIVISION:
					treestat.divcount++;
					break;
				case TS_APOPTOSIS:
					treestat.apocount++;
					break;
				case TS_LOST:
					treestat.lostcount++;
					break;
				case TS_NONE:
					treestat.intercount++;
					break;
				default:
					;
			}
			treestat.totalcount++;
			
			//count for generations
			int generation = tmpTrack->getGeneration();
			treestat.generationCellcounts [generation]++;
		}
	}
	
	return treestat;
}

TreeStatistics TreeStatistics::calculate (const Tree *_tree, const Q3ValueVector<bool> &_generations, long _cellTypes, int _maxStartingTP, int _maxEndingTP) //, int _maxLifetimeDiff)
{
	TreeStatistics treestat;
	
	if (! _tree)
		return treestat;
	
	treestat.generationCellcounts.resize (_tree->getNumberOfGenerations() + 1);
	
	Track *tmpTrack = 0;
	Q3IntDict<Track> tracks = _tree->getAllTracks (-1);
	int gen = -1;
	//long cellType = CT_NOCELLTYPE;
	
	for (uint i = 0; i < tracks.size(); i++) {
		tmpTrack = tracks.find (i);
		if (tmpTrack) {
		
			gen = tmpTrack->getGeneration();
			
                        if ((gen < 0) | (gen > (int)_generations.size()))
				continue;
			
			if (! _generations [gen])
				continue;
			
			if (! tmpTrack->subsumesCellTypes (_cellTypes))
				continue;
			
			if (tmpTrack->getFirstTrace() > _maxStartingTP)
				continue;
			
			if (tmpTrack->getLastTrace() > _maxEndingTP)
				continue;
			
			
			//count for stop reasons
			switch (tmpTrack->getStopReason()) {
				case TS_DIVISION:
					treestat.divcount++;
					break;
				case TS_APOPTOSIS:
					treestat.apocount++;
					break;
				case TS_LOST:
					treestat.lostcount++;
					break;
				case TS_NONE:	
					treestat.intercount++;
					break;
				default:
					;
			}
			treestat.totalcount++;
			
			//count for generations
			treestat.generationCellcounts [gen]++;
		}
	}
	
	return treestat;
}

int TreeStatistics::getGenerationCounts (int _generation)
{
        if ((_generation >= 0) & (_generation < (int)generationCellcounts.size()))
		return generationCellcounts [_generation];
	else
		return -1;
}

TreeStatistics& TreeStatistics::operator+= (const TreeStatistics& _ts1)
{
        int genmax = QMAX (_ts1.generationCellcounts.size(), generationCellcounts.size());
	
	uint oldsize = generationCellcounts.size();
	generationCellcounts.resize (genmax);
        for (int i = oldsize; i < generationCellcounts.size(); i++)
		generationCellcounts [i] = 0;
	
	totalcount += _ts1.totalcount;
	divcount += _ts1.divcount;
	apocount += _ts1.apocount;
	lostcount += _ts1.lostcount;
	intercount += _ts1.intercount;
	
        for (int i = 0; i < genmax; i++) {
		int tmp = 0;
		
		if (i < _ts1.generationCellcounts.size())
			tmp += _ts1.generationCellcounts [i];
		
		generationCellcounts [i] += tmp;
	}
	
	return *this;
}

