/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef treefragmenttrack_h__
#define treefragmenttrack_h__

// Qt
#include <QHash>
#include <QSharedPointer>

// Project
#include "itrack.h"
#include "treefragmenttrackpoint.h"

// Forward declarations
class TreeFragment;
class TreeFragmentTrackPoint;


/**
 * Represent one track of a TreeFragment. As TreeFragment this class is read-only (except for the child tracks)
 */
class TreeFragmentTrack : public ITrack {

public:

	/**
	 * Constructor
	 * @param _trackNumber the track number of this track in the tree
	 * @param _startTp first timepoint of this track
	 * @param _stopTp last timepoint of this track
	 * @param _trackPoints hashmap with all trackpoints for this track by timepoint, cannot be changed later. Constructor will set track for each TreeFragmentTrackPoint to this
	 * @param _treeFragmentNumber the unique number associated with this fragment
	 * @param _treeFragment the treeFragment this track belongs to
	 * @param _child1 optional: left child track
	 * @param _child2 optional: right child track
	 */
	TreeFragmentTrack(unsigned int _trackNumber, int _startTp, int _stopTp, QHash<int, QSharedPointer<TreeFragmentTrackPoint>>&& _trackPoints, int _treeFragmentNumber, TreeFragment* _treeFragment, TreeFragmentTrack* _child1 = 0, TreeFragmentTrack* _child2 = 0);

	// Insert a new track point
	bool insertTrackPoint(const QSharedPointer<TreeFragmentTrackPoint>& trackPoint);

	// Get number of trackpoints
	int getNumTrackPoints() const {
		return trackPoints.size();
	}

	// Get trackpoint by timepoint or nullptr if no trackpoint exists at _timePoint
	TreeFragmentTrackPoint* getTrackPointByTimePoint(int _timePoint) const;
	QSharedPointer<TreeFragmentTrackPoint> getTrackPointByTimePointSharedPtr(int _timePoint) const;

	// Delete trackpoint by timepoint (returns true if track point was deleted or did not exist in the first place)
	bool deleteTrackPointByTimePoint(int _timePoint);

	// Get first trackpoint or nullptr if not available
	TreeFragmentTrackPoint* getFirstTrackPoint() const;

	// Get last trackpoint or nullptr if not available
	TreeFragmentTrackPoint* getLastTrackPoint() const;

	// Get child track 1
	TreeFragmentTrack* getChild1() const {
		return child1;
	}

	// Get child track 2
	TreeFragmentTrack* getChild2() const {
		return child2;
	}

	/**
	 * @return mother track or 0 if not available
	 */
	TreeFragmentTrack *getMother() const;

	/**
	 * @return first timepoint of this track or -1 if not available
	 */
	int getFirstTimePoint() const;

	/**
	 * @return last timepoint of this track or -1 if this track contains no trackpoints or if not available
	 */
	int getLastTimePoint() const;

	/**
	 * @return the track number of this track or -1 if not available
	 */
	int getTrackNumber() const {
		return trackNumber;
	}

	/**
	 * @return tree of this track or 0 if not available
	 */
	TreeFragment* getTree() const {
		return treeFragment;
	}

	/**
	 * @return weak pointer to tree of this track.
	 */
	QWeakPointer<TreeFragment> getTreeWeakPointer() const {
		return treeFragmentWeakPointer;
	}

	/**
	 * @return tree of this track or 0 if not available
	 */
	ITree* getITree() const;

	/**
	 * Get a list of all available properties. The values in the
	 * returnes list can be used for the getTrackProperty method
	 * @return vector containing all available property names
	 */
	QVector<QString> getAvailableProperties() const;

	/**
	 * Access method to many (all?) track properties (=cell properties)
	 * Because of the QVariant return type many datatypes are supported.
	 * @parm _propName the name of the property
	 * @return the value of the requested property
	 */
	QVariant getTrackProperty(QString _propName) const;

	/**
	 * @return stop reason for this track
	 */
	TrackStopReason getStopReason() const;

	/**
	 * Reimplemented from ITrack
	 */
	int getSecondsForTimePoint(int timepoint) const
	{
		return -1;
	}

	// Set child track 1
	void setChild1(TreeFragmentTrack* _child1) {
		child1 = _child1;
	}

	// Set child track 2
	void setChild2(TreeFragmentTrack* _child2) {
		child2 = _child2;
	}

	// Get const reference to trackpoints
	const QHash<int, QSharedPointer<TreeFragmentTrackPoint> >& getTrackPoints() const {
		return trackPoints;
	}

	/**
	 * @return number of seconds between the very first timePoint and the given one.
	 */
	virtual int getAbsoluteSecondsForTP(int timePoint) const {
		return -1;
	}

private:

	/**
	 * Change track number, recursively updates numbers of child tracks.
	 * @return new max track number of this track or any of its children.
	 */
	int setTrackNumber(int newNum);

	// Trackpoints by timepoint
	QHash<int, QSharedPointer<TreeFragmentTrackPoint>> trackPoints;

	// Child tracks (note: all tracks are owned by TreeFragment)
	TreeFragmentTrack *child1;
	TreeFragmentTrack *child2;

	// Track number
	unsigned int trackNumber;

	// Start and stop timepoints
	int startTp;
	int stopTp;

	// Number of TreeFragment object this track belongs to
	int treeFragmentNumber;

	// TreeFragment object this track belongs to
	TreeFragment* treeFragment;

	// Same as treeFragment, but as shared pointer. Not necessarily set
	QWeakPointer<TreeFragment> treeFragmentWeakPointer;

	friend class TreeFragment;
};


#endif // treefragmenttrack_h__
