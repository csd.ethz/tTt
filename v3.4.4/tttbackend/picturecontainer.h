/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PICTURECONTAINER_H
#define PICTURECONTAINER_H

#include "displaysettings.h"
#include "pictureindex.h"
#include "tttdata/systeminfo.h"
#include "tttio/JPEGReader.h"

#include <qimage.h>
#include <qpaintdevice.h>
#include <qwidget.h>

// OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>



class PictureView;

/**
@author Bernhard
*/

/** This object contains all functions that are necessary for loading a
*	rectangular region of a picture (jpg, tiff) into a QPixmap-Object with 8 bit depth
*	in greyscale and displaying it in a handled context
*
*	This guarantees fast memory allocations and efficient pixel data access.
*
*/


class PictureContainer 
{

public:
	
	/**
	 * constructs a PictureContainer from the specified file and optionally the input frame
	 * @param _filename the filename of the picture
	 * @param _picIndex the timepoint and wavelength of the picture
	 * @param _loadingFlag whether the picture should be loaded (later, not immediately!)
	 * @param _bdisplay the pointer to a display settings object
         * // @param _keepComplete whether the complete image should be kept in memory
	 * @param _defaultSize the size of the images of the default wavelength
	 */
    PictureContainer(const QString& directory, const QString& _filename, const PictureIndex& _picIndex, bool _loadingFlag = false/*, BDisplay *_bdisplay = 0*/, QRect _defaultSize = QRect());
	
	~PictureContainer();
	
	/**
	 * loads the picture with the coordinates specified in the constructor
	 * @param load if image should be loaded or unloaded
	 * @return 0 if the image could be loaded (or unloaded), 1 if out of memory, 2 if other error (e.g. image already loaded or unloaded)
	 */
	int loadImage(bool load);
	
	/**
	 * removes the image from memory (but keeps this object alive)
	 * @return whether unloading was successful
	 */
	bool removeImage();
	
	/**
	 * @return the image raw data (type CV_8U or CV_16U)
	 */
    const cv::Mat& getImage() const
	{
		return m_img;
	}
	
    /**
     * @return a copy of the raw image as 8-bit gray scale QImage.
     */
    QImage getImageForDisplay() const;
	
	/**
	 * sets the selection flag
	 * @param _set whether this picture is selected (true) or not (false)
	 */
	void setSelection (bool _set) 
	{
		Selection = _set;
	}
	
	/**
	 * @return the selection flag
	 */
	bool getSelectionFlag() const
	{
		return Selection;
	}
	
	/**
	 * @return true if this picture is currently loaded (if there is a picture in memory)
	 */
	bool isLoaded() const
	{
		return m_img.data != 0;
	}
	
	/**
	 * @return the width of the picture (without scaling)
	 */
	int getWidth() const
	{
		return m_img.cols;
	}
	
	/**
	 * @return the height of the picture (without scaling)
	 */
	int getHeight() const
	{
		return m_img.rows;
	}
	
	/**
	 * @return the timepoint of this picture
	 */
	int getTimePoint() const
	{
		return Index.TimePoint;
	}

	/**
	 * @return z-index of picture
	 */
	int getZIndex() const 
	{
		return Index.zIndex;
	}
	
	/**
	 * @return the wavelength of this picture
	 */
	int getWaveLength() const
	{
		return Index.WaveLength;
	}
	
	/**
	 * @return the filename of this picture (without path)
	 */
	const QString getFilename() const
	{
		return m_filename;
	}
	
	/**
	 * returns the red component of the pixel at _x/_y
	 * @param _x the x coordinate of the pixel
	 * @param _y the y coordinate of the pixel
	 * @return a value between 0 and 255 (the red channel); or -1, if the picture is not loaded
	 */
	int readPixel (int _x, int _y) const;

	/**
	 * @return the x-scaling factor of this picture
	 */
	double getScalingFactorX() const 
	{
		return scalingFactorX;
	}

	/**
	 * @return the y-scaling factor of this picture
	 */
	double getScalingFactorY() const 
	{
		return scalingFactorY;
	}

	/**
	 * @return if background corrected.
	 */
	bool backgroundCorrected() const 
	{
		return imageBackgroundCorrected;
	}

	/**
	 * Apply background correction and re-scale values to [0,1] range
	 */
	bool applyBackgroundCorrectionAndReNormalize(const unsigned short* backgroundData, const unsigned short* gainData);
	
	
private:
	
	// Path
	QString m_path;

	// Filename without path
	QString m_filename;
	
	///contains the raw image data (format: CV_8U or CV_16U)
	cv::Mat m_img;
	
	///contains timepoint and wavelength of the picture
	PictureIndex Index;
	
	///necessary for the timescale to decide whether an image is selected to be (un)loaded
	bool Selection;
	
	///the size of the default wavelength pictures
	///all pictures of other wavelengths are stretched to this size
	///set in constructor
	QRect DefaultSize;
	
	// Scaling factor of this picture: when displayed, all pictures must be scaled to the "default size"
	// which is usually the size of all wavelength 0 pictures.
	double scalingFactorX;
	double scalingFactorY;
	
	// If this picture is background corrected
	bool imageBackgroundCorrected;
};

#endif
