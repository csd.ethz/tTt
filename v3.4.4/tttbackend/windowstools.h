/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
// Include QtGlobal for Q_WS_WIN
#include <QtGlobal>

#ifdef Q_WS_WIN

#ifndef WINDOWSTOOLS_H
#define WINDOWSTOOLS_H

/**
@author Oliver Hilsenbeck
	
	This class contains general functions that are specific for windows.
*/

class WindowsTools {
public:

	/**
	 * Returns the amount of free physical ram in bytes
	 */
	static unsigned long long getFreePhysicalRam();

	/**
	 * Returns the amount of free virtual ram in bytes
	 */
	static unsigned long long getFreeVirtualRam();
};



#endif

#endif