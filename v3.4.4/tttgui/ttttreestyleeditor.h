/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TTTTREESTYLEEDITOR_H
#define TTTTREESTYLEEDITOR_H

#include "ui_frmTreeStyleEditor.h"
#include "tttdata/systeminfo.h"
#include "tttdata/trackingkeys.h"
#include "tttdata/stylesheet.h"
#include "tttbackend/compileflags.h"

#include <qpushbutton.h>
#include <qcheckbox.h>
#include <q3frame.h>
#include <qlineedit.h>
#include <qspinbox.h>
#include <qcombobox.h>
#include <q3filedialog.h>
#include <qmessagebox.h>
#include <qcolordialog.h>
#include <qcolor.h>
#include <qtabwidget.h>

#include <qevent.h>
#include <qstring.h>
//Added by qt3to4:
#include <QCloseEvent>
#include <QWidget>
#include <QVector>

#include <Q3IntDict>

///the pixel distance to the border for the topleft button, which is dynamically created
const int offsetX = 10;
const int offsetY = 20;


class TTTTreeStyleEditor: public QWidget, public Ui::frmTreeStyleEditor {

Q_OBJECT

public:
        TTTTreeStyleEditor(QWidget *parent = 0);
	
	void closeEvent (QCloseEvent *);
	
	///sets the display to show the properties of the provided style sheet
	///@param _ss the StyleSheet object that should be displayed
	void setStyleSheet (StyleSheet &_ss);
	
	///displays the provided layout/style
	///@param _layout the layout vector
	///@param _picsPerRow the number of pictures per row
	///@param _ccc the cell circle color
	///@param _cnc the cell number color
	///@param _cct the cell circle outline thickness
	///@param _cpc the cell path color
	///@param _cnfs the cell number font size
	///@param _cpt the cell path thickness
	///@param _cp_ti the cell path time display interval
        void setMovieLayout (QVector<int> _layout, int _picsPerRow, QColor _ccc, QColor _cnc, int _cct, QColor _cpc, int _cnfs, int _cpt, int _cp_ti, QColor _wbc, int _wbfs);
	
	
signals:
	///emitted when the user clicked either "Apply" or "Close" to inform the parent widget about the choice
	void styleChosen (StyleSheet &);

	///emitted when the user clicks "close" to take over the settings
	void movieLayoutSet();
	
	
private slots:
	
	///take over the settings and apply the settings (
	void apply();
	
	///loads a previously save style file
	void load();
	
	///saves the current settings in a style file
	void save();
	
	///displays a color picker for the color of the current item
	void chooseColor();
	
	
//tree layout methods
	
	///sets all color values (and sizes) to fit a a grayscale tree
	void setTreeGrayscale();
	
	///restores the default settings (stored in the default constructor of a StyleSheet object)
	void setTreeDefault();
	
	///shows/hides the property frame
	///@param _show whether to show or to hide the frame
	void showTreePropertyFrame (bool _show);
	
	///saves the props for the current element and displays the properties saved in 
	/// (private attribute) styleSheet for the desired element
	///@param _name the name (displayed text) of the tree element to display
	void changeTreeElement (const QString &_name);
	
	
//movie layout methods
	
	///assigns the current ordinal number to the clicked button and increases the number
	void setMovieOrder();
	
	///resets the form to default layout
	void resetMovie();
	
	///updates the view if the user changed the number of pictures per row
	///@param _picsPerRow the new number of pictures per row
	void updateMovieRows (int _picsPerRow);
	
	///lets the user select a color for the cell circle
	void setMovieCCC();
	
	///lets the user select a color for the cell number
	void setMovieCNC();
	
	///sets the cell circle thickness
	///@param _thickness the thickness of the circle outline in pixel
	void setMovieCCT (int _thickness);
	
	///lets the user select a color for the cell path
	void setMovieCPC();
	
	///sets the cell number font size
        ///@param _size the cell number font size
	void setMovieCNFS (int _size);
	
        ///lets the user select a color for the wavelength box
        void setMovieWBC();

        ///sets the wavelength box font size
        ///@param the wavelength box font size
        void setMovieWBFS (int);

        ///sets the cell path thickness
	///@param _thickness the thickness of the line in pixel
	void setMovieCPT (int _thickness);
	
	///sets the interval in timepoints for which the timepoint should be displayed
	///@param _time_interval 
	void setMovieCP_TI (int _time_interval);

	/**
	 * sets if out of sync box should be shown
	 * @param _show
	 */
	void setShowOutOfSynxBox (bool _show);
	
	
//key association methods
	
	///shows/hides the property frame for the currently selected key
	///@param _show whether to show or to hide the frame
	void showKeyPropertyFrame (bool _show);
	
	///saves the props for the current key and displays the properties saved in 
	/// (private attribute) styleSheet for the desired key
	///@param _newKey the key code (displayed character) of the key to display
	void changeKeyElement (const QString &_newKey);
	

//various settings
	
	/**
	 * sets whether the user sign should be added to all ttt files automatically
	 * @param _add true => add it
	 */
	void setAddUserSignToTTTFiles (bool _add);
	
	/**
	 * sets whether the ttt file should be stored in the new folder system
	 * @param  _use true => use new system
	 */
	void setUseNewTTTFileFolderStructure (bool _use);
	
	/**
	 * whether the trees should be stored in an experiment global fashion, not corresponding to their base position
	 * @param _use true => use new system
	 */
	void setUseExperimentGlobalTrees (bool _use);
	
	/**
	 * sets the export wl for fluorescence export
	 * @param _ewl the wavelength (1 - Max_Wavelength)
	 */
	void setExportWavelength (int _ewl);
	
	/**
	 * whether the pictures should be loaded asynchronously (faster, but freezes tTt)
	 * @param _use true => load asynchronous (without timer, in for loop)
	 */
	void setLoadPicturesAsynchronous (bool _use);
	
	/**
	 * whether the number of trees should be displayed in each position in the layout window
	 * @param _use true -> display tree count (needs some calculation time)
	 */
	void setShowTreeCountInPositionLayout (bool _use);
	
	/**
	 * whether the first/all tracks in the movie window should be displayed with different colours
	 * @param _use true -> different colours, false -> only one colour
	 */
	void setUseColorsForAllTracks  (bool _use);

	/**
	* set number of pictures to be loaded when clicking on one of the buttons inside the movie
	* window to load a neighbor position
	* @param _num number of pics
	*/
	void setNumPicsToLoadByNextPosItem(int _val);

	/**
	 * if the current display settings (gamma etc) should be taken over when a new position is loaded via a MovieNextPosItem button
	 * @param _val true -> take over settings
	 */
	void setTakeOverDisplaySettingsNextPosItem(bool _val);
	
//startup settings
	
	
	/**
	 * following slots: set parameter in styleSheet after user click
	 * @param _use 
	 */
	void setStartUpMoveToFirstFluor (bool _use);
	void setStartUpBgCircleVisible (bool _use);
	void setStartUpFullPicture (bool _use);
	void setStartUpAlternateNormalBgTracking (bool _use);
	
	
private:
	
	///the internal style sheet that bears the current settings
	StyleSheet styleSheet;
	
//tree layout attributes

	///the currently displayed/edited tree element
	TreeElementStyle tesTree;
	
	///the currently displayed/edited key element
	TreeElementStyle tesKey;

//movie layout attributes

	///the increasing number that is assigned on mouse click to a button
	///increased with every click from 0 to MAX_WAVE_LENGTH
	int order;
	
	///the inverse mapping of currentLayout (wavelength |-> position), necessary for easier calculation of shifts
	Q3ValueVector<int> ordenador;
	
	///the array of buttons (are created dynamically according to MAX_WAVE_LENGTH)
	Q3IntDict<QPushButton> buttons;

//key association attributes


//private methods	
	
	///saves the current element (tes)
	void saveTreeElement();
	
	///displays the properties saved in (private attribute) styleSheet for the desired element
	///@param _name the name (displayed text) of the tree element to display
	void showTreeElement (const QString &_name);
	
	///saves the current key setting (tes)
	void saveKeyElement();
	
	///displays the properties saved in (private attribute) styleSheet for the desired key
	///@param _key the name (displayed character) of the key element to display
	void showKeyElement (const QString &_key);
	
	///displays the current values of the various settings tab
	void showVariousSettings();
	
	///displays the current settings for the movie layout
	void showMovieProperties();
	
	///displays the startup settings
	void showStartUpSettings();
	
};

#endif
