/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ttttest.h"

// Qt
#include <QMessageBox>

// Project
#include "tttbackend/tttmanager.h"

// OpenCV
#ifdef DEBUGMODE
//// Important: Never include openCV in Release mode
//#include "opencv2/video/tracking.hpp"
//#include "opencv2/core/core.hpp"

#endif



TTTTest::TTTTest( QWidget* _parent /*= 0*/ )
	: QWidget(_parent)
{
	ui.setupUi(this);

	// Signals/slots
	connect(ui.pbtThresholds, SIGNAL(clicked()), this, SLOT(thresholdsTest()));
	connect(ui.pbtErrorTestSingle, SIGNAL(clicked()), this, SLOT(errorTestSingle()));
	connect(ui.pbtTTTHist, SIGNAL(clicked()), this, SLOT(tttHistogram()));
	connect(ui.pbtTrfHist, SIGNAL(clicked()), this, SLOT(trfHistogram()));
	connect(ui.pbt3DThresh, SIGNAL(clicked()), this, SLOT(thresholdsTest3D()));
	connect(ui.pbtSpeedHist, SIGNAL(clicked()), this, SLOT(speedHist()));
	connect(ui.pbtDispl, SIGNAL(clicked()), this, SLOT(displacement()));
	connect(ui.pbtCoords, SIGNAL(clicked()), this, SLOT(expCoords()));
	connect(ui.pbtAbsDisp, SIGNAL(clicked()), this, SLOT(absDispl()));
	connect(ui.pbtTrfSpeedPlot, SIGNAL(clicked()), this, SLOT(trfSpeedPlot()));
	connect(ui.pbtTestPrediction, SIGNAL(clicked()), this, SLOT(testPrediction()));
	connect(ui.pbtKalmanStats, SIGNAL(clicked()), this, SLOT(generateKalmanStats()));
	connect(ui.pbtKalmanErr, SIGNAL(clicked()), this, SLOT(generateKalmanInitErrStats()));
	connect(ui.pbtSpeedTRFs, SIGNAL(clicked()), this, SLOT(exportSpeedOfTrfs()));
}

void TTTTest::thresholdsTest()
{
	say("Performing thresholds test..");

	// Get folders
	if(!getFolders()) {
		say("Invalid folders");
		return;
	}

	// List trf folders
	QStringList trfFolders = listDirectories(trfFolder);
	trfFolders.sort();
	if(trfFolders.isEmpty())
		return;

	// Read trees
	QList<QSharedPointer<Tree> > trees = readTTTFiles(tttFolder);
	if(trees.isEmpty())
		return;

	// Output for file to read by R
	QFile output(outFolder + "thresholds.txt");
	if(!output.open(QIODevice::WriteOnly)) {
		say(QString("Could not open file: ") + output.errorString());
		return;
	}
	QTextStream out(&output);

	// Header
	out << "num;size;err\r\n";

	// Iterate over trf folders
	for(int i = 0; i < trfFolders.size(); ++i) {
		// Check if cancel
		QApplication::processEvents();
		if(ui.pbtCancel->isChecked()) {
			say("Canceled");
			ui.pbtCancel->setChecked(false);
			return;
		}

		if(trfFolders[i].length() != 2)
			continue;

		if(trfFolders[i].right(1) != "/")
			trfFolders[i] += '/';


		// Read trf files
		QList<QSharedPointer<TreeFragment> > fragments = readTrfFiles(trfFolder + trfFolders[i]);
		if(fragments.isEmpty())
			return;

		// Add observation
		out << thrObservation(fragments, trees) << "\r\n";
	}
}

QList<QSharedPointer<TreeFragment> > TTTTest::readTrfFiles( QString _dir )
{
	// Out
	QList<QSharedPointer<TreeFragment> > trees;
	say(QString("Parsing %1 for trf files...").arg(_dir));

	// Check dir
	QDir dir(_dir);
	if(_dir.isEmpty() || !dir.exists() || _dir.right(1) != "/") {
		say("Error: Invalid directory");
		return trees;
	}

	// List files
	QStringList exts;
	exts << "*.trf";
	QStringList fileNames = dir.entryList(exts, QDir::Files);
	
	// Read tree fragments
	QHash<int, QList<QSharedPointer<TreeFragmentTrackPoint> > > dummy;
	for(QStringList::const_iterator it = fileNames.constBegin(); it != fileNames.constEnd(); ++it) {
		QApplication::processEvents();

		// Read tree
		TreeFragment *tmp = new TreeFragment(_dir + *it, dummy, true);
		QSharedPointer<TreeFragment> nextTree(tmp);
		//tmp->setSharedPointer(nextTree);

		// Add to list
		trees.append(nextTree);
	}

	// Return list
	say(QString("Read %1 files").arg(trees.size()));
	return trees;
}

QList<QSharedPointer<Tree> > TTTTest::readTTTFiles( QString _dir )
{
	// Out
	QList<QSharedPointer<Tree> > trees;
	say(QString("Parsing %1 for ttt files...").arg(_dir));

	// Check dir
	QDir dir(_dir);
	if(_dir.isEmpty() || !dir.exists() || _dir.right(1) != "/") {
		say("Error: Invalid directory");
		return trees;
	}

	// List files
	QStringList exts;
	exts << "*.ttt";
	QStringList fileNames = dir.entryList(exts, QDir::Files);

	// Get some pos information
	TTTPositionManager* tttpm = TTTManager::getInst().getBasePositionManager();
	if(!tttpm) {
		say("Error: Could not get tttpm");
		return trees;
	}

	// Read tree fragments
	for(QStringList::const_iterator it = fileNames.constBegin(); it != fileNames.constEnd(); ++it) {

		// Read tree
		int trackCount, firstTP, lastTP;
		Tree *curTree = new Tree();
		curTree->reset();
		if (TTTFileHandler::readFile (_dir + *it, curTree, trackCount, firstTP, lastTP, &tttpm->positionInformation, false, true) != TTT_READING_SUCCESS) {
			say(QString("Warning: Failed to read file " + *it));
			delete curTree;
			continue;
		}

		// Add to list
		QSharedPointer<Tree> nextTree(curTree);
		trees.append(nextTree);
	}

	// Return list
	say(QString("Read %1 files").arg(trees.size()));
	return trees;
}

void TTTTest::say( QString _what )
{
	ui.pteOut->appendPlainText(_what);
}

QStringList TTTTest::listDirectories( QString _dir )
{
	// Check dir
	QDir dir(_dir);
	if(_dir.isEmpty() || !dir.exists() || _dir.right(1) != "/") {
		say("Error: Invalid directory");
		return QStringList();
	}

	// List files
	return dir.entryList(QStringList(), QDir::Dirs | QDir::NoDot | QDir::NoDotDot | QDir::NoSymLinks);
}

bool TTTTest::getFolders()
{
	trfFolder = ui.lieTrfs->text();
	tttFolder = ui.lieTTT->text();
	outFolder = ui.lieOut->text();
	trfSingleFolder = ui.lieSingleTRFFolder->text();

	if(trfFolder.isEmpty() || tttFolder.isEmpty() || outFolder.isEmpty() || trfSingleFolder.isEmpty())
		return false;

	trfFolder = QDir::fromNativeSeparators(trfFolder);
	tttFolder = QDir::fromNativeSeparators(tttFolder);
	outFolder = QDir::fromNativeSeparators(outFolder);
	trfSingleFolder = QDir::fromNativeSeparators(trfSingleFolder);

	if(!QDir(outFolder).exists())
		return false;
	if(!QDir(tttFolder).exists())
		return false;
	if(!QDir(trfFolder).exists())
		return false;
	if(!QDir(trfSingleFolder).exists())
		return false;


	if(trfFolder.right(1) != "/")
		trfFolder += '/';
	if(tttFolder.right(1) != "/")
		tttFolder += '/';
	if(outFolder.right(1) != "/")
		outFolder += '/';
	if(trfSingleFolder.right(1) != "/")
		trfSingleFolder += '/';

	return true;
}

QString TTTTest::thrObservation( const QList<QSharedPointer<TreeFragment> >& _fragments, const QList<QSharedPointer<Tree> >& _trees )
{
	// Calculations
	float avgFrgSize = 0;
	for(int i = 0; i < _fragments.size(); ++i) {
		// Avg size
		avgFrgSize += _fragments[i]->getNumberOfTrackPoints();
	}

	// Calculations part 2
	avgFrgSize /= (float)_fragments.size();

	// Num errors
	int numErrors = countSeriousErrors(_fragments, _trees);
	say(QString("Detected %1 errors").arg(numErrors));

	return QString("%1;%2;%3").arg(_fragments.size()).arg(avgFrgSize).arg(numErrors);
}

void TTTTest::errorTestSingle()
{
	say("Performing error single trf folder test..");

	// Get folders
	if(!getFolders()) {
		say("Invalid folders");
		return;
	}

	// Read trees
	QList<QSharedPointer<Tree> > trees = readTTTFiles(tttFolder);
	if(trees.isEmpty())
		return;

	// Read trf files
	QList<QSharedPointer<TreeFragment> > fragments = readTrfFiles(trfSingleFolder);
	if(fragments.isEmpty())
		return;

	// Count errors
	int errors = countSeriousErrors(fragments, trees);
	say(QString("Detected %1 errors").arg(errors));
}

int TTTTest::countSeriousErrors( const QList<QSharedPointer<TreeFragment> >& _fragments, const QList<QSharedPointer<Tree> >& _trees )
{
	int errorCount = 0;

	// Threshold
	int errThreshold = ui.spbErrThresh->value();
	int frgThreshold = ui.spbFrgThreshold->value();

	// Iterate over trees
	for(int i = 0; i < _trees.size(); ++i) {
		Tree* curTree = _trees[i].data();

		// Iterate over tracks of tree
		Q3IntDict<Track> tracks = curTree->getAllTracks (-1);
		for (uint i = 0; i < tracks.size(); i++) {
			Track* tmpTrack = tracks.find (i);
			if (tmpTrack) {
				QSharedPointer<TreeFragmentTrack> curFrg;

				// Iterate over timepoints of track
				int startTp = tmpTrack->getFirstTimePoint();
				if(startTp <= 0)
					continue;

				for(int tp = startTp; tp <= tmpTrack->getLastTimePoint(); ++tp) {
					// Get current trackpoint 
					TrackPoint* trackPoint = tmpTrack->getTrackPointByTimePoint(tp);
					if(!trackPoint)
						continue;

					if(curFrg.isNull() || curFrg->getTrackPointByTimePoint(tp) == 0) {
						// Find new current fragment
						float dist;
						QSharedPointer<TreeFragmentTrack> tmp = getClosestTrack(trackPoint->point(), tp, _fragments, dist);

						if(!tmp.isNull() && dist <= frgThreshold)
							curFrg = tmp;
					}
					else {
						// Get trackpoint of fragment at current
						TreeFragmentTrackPoint* frgTrackPoint = curFrg->getTrackPointByTimePoint(tp);

						// Measure distance to current track
						float dist = QLineF(QPointF(frgTrackPoint->getX(), frgTrackPoint->getY()), trackPoint->point()).length();

						// Check if dist is bad
						if(dist >= errThreshold) {
							// Probably a error
							++errorCount;

							// Message
							QString msg = QString("Error detected at timepoint %1 for fragment %2 - ttt file %3").arg(tp).arg(frgTrackPoint->getTrack()->getTree()->getTreeFragmentNumber()).arg(curTree->getFilename());
							say(msg);

							// Reset fragment
							curFrg.clear();
						}
					}

				}
			}
		}
		
	}

	return errorCount;
}

QSharedPointer<TreeFragmentTrack> TTTTest::getClosestTrack( QPointF _pos, int _tp, const QList<QSharedPointer<TreeFragment> >& _fragments, float& _outDistance )
{
	QSharedPointer<TreeFragmentTrack> ret;
	float curMinDist = -1;

	// Fragments
	for(int i = 0; i < _fragments.size(); ++i) {
		// Tracks
		const QHash<int, QSharedPointer<TreeFragmentTrack> >& tracks = _fragments[i]->getAllTracks();
		for(QHash<int, QSharedPointer<TreeFragmentTrack> >::const_iterator it = tracks.constBegin(); it != tracks.constEnd(); ++it) {

			// Try to get trackpoint
			TreeFragmentTrackPoint* tp = it->data()->getTrackPointByTimePoint(_tp);
			if(tp) {
				// Calc distance
				float curDist = QLineF(QPointF(tp->getX(), tp->getY()), _pos).length();
				if(curMinDist == -1 || curDist < curMinDist) {
					curMinDist = curDist;
					ret = *it;
				}
			}
		}
	}

	_outDistance = curMinDist;
	return ret;
}

void TTTTest::tttHistogram()
{
	say("Performing ttt files histogram calculation..");

	// Get folders
	if(!getFolders()) {
		say("Invalid folders");
		return;
	}

	// Read trees
	QList<QSharedPointer<Tree> > trees = readTTTFiles(tttFolder);
	if(trees.isEmpty())
		return;

	// Output for file to read by R
	QFile output(outFolder + "ttt_size_histogram.txt");
	if(!output.open(QIODevice::WriteOnly)) {
		say(QString("Could not open file: ") + output.errorString());
		return;
	}
	QTextStream out(&output);
	out << "num\r\n";

	int numTrackPointsOfAllTrees = 0;

	// Iterate over tree
	for(int i = 0; i < trees.size(); ++i) {
		Tree* curTree = trees[i].data();

		int numTrackPoints = 0;

		// Iterate over tracks of tree
		Q3IntDict<Track> tracks = curTree->getAllTracks (-1);
		for (uint i = 0; i < tracks.size(); i++) {
			Track* tmpTrack = tracks.find (i);
			if (tmpTrack) {
				numTrackPoints += tmpTrack->getMarkCount();
				numTrackPointsOfAllTrees += numTrackPoints;
			}
		}

		out << numTrackPoints << "\r\n";
	}

	say(QString("In sum %1 trackpoints").arg(numTrackPointsOfAllTrees));
}

void TTTTest::trfHistogram()
{
	say("Performing trf files histogram calculation..");

	// Get folders
	if(!getFolders()) {
		say("Invalid folders");
		return;
	}

	// Read trees
	QList<QSharedPointer<TreeFragment> > fragments = readTrfFiles(trfSingleFolder);
	if(fragments.isEmpty())
		return;

	// Output for file to read by R
	QFile output(outFolder + "trf_size_histogram.txt");
	if(!output.open(QIODevice::WriteOnly)) {
		say(QString("Could not open file: ") + output.errorString());
		return;
	}
	QTextStream out(&output);
	out << "num\r\n";

	int numTrackPointsOfAllTrees = 0;

	// Iterate over tree
	for(int i = 0; i < fragments.size(); ++i) {
		TreeFragment* curTree = fragments[i].data();

		int numTrackPoints = curTree->getNumberOfTrackPoints();
		numTrackPointsOfAllTrees += numTrackPoints;

		out << numTrackPoints << "\r\n";
	}

	say(QString("In sum %1 trackpoints").arg(numTrackPointsOfAllTrees));
}

void TTTTest::thresholdsTest3D()
{
	//say("Performing 3D thresholds test..");
	//QHash<int, QList<QSharedPointer<TreeFragmentTrackPoint> > > tmp;
	//TreeFragment* fu = new TreeFragment("G:\\autotracking\\100922PH3\\17\\100922PH3_t00001_n00001.trf", tmp, false);
	//QSharedPointer<TreeFragment> * test = new QSharedPointer<TreeFragment>(fu);

	//QList<QSharedPointer<TreeFragment> > test2;
	//test2.append(*test);

	//delete test;

	//test2.clear();

	// Get folders
	if(!getFolders()) {
		say("Invalid folders");
		return;
	}

	// List trf folders
	QStringList trfFolders = listDirectories(trfFolder);
	trfFolders.sort();
	if(trfFolders.isEmpty())
		return;

	// Read trees
	QList<QSharedPointer<Tree> > trees = readTTTFiles(tttFolder);
	if(trees.isEmpty())
		return;

	// Output for file to read by R
	QFile output(outFolder + "thresholds3D.txt");
	if(!output.open(QIODevice::WriteOnly)) {
		say(QString("Could not open file: ") + output.errorString());
		return;
	}
	QTextStream out(&output);

	// Header
	out << "num;size;err\r\n";

	// Iterate over trf folders
	for(int i = 0; i < trfFolders.size(); ++i) {
		// Check if cancel
		QApplication::processEvents();
		if(ui.pbtCancel->isChecked()) {
			say("Canceled");
			ui.pbtCancel->setChecked(false);
			return;
		}

		if(trfFolders[i].length() != 5 || trfFolders[i].indexOf("_") == -1)
			continue;

		if(trfFolders[i].right(1) != "/")
			trfFolders[i] += '/';


		// Read trf files
		QList<QSharedPointer<TreeFragment> > fragments = readTrfFiles(trfFolder + trfFolders[i]);
		if(fragments.isEmpty())
			return;

		// Add observation
		out << thrObservation(fragments, trees) << "\r\n";
		fragments.clear();
	}
}

void TTTTest::speedHist()
{
	say("TRF Speed Hist Calculation started.");

	// Get folders
	if(!getFolders()) {
		say("Invalid folders");
		return;
	}

	// Read trees
	QList<QSharedPointer<TreeFragment> > fragments = readTrfFiles(trfSingleFolder);
	if(fragments.isEmpty())
		return;

	// Output for file to read by R
	QFile output(outFolder + "trf_speed_histogram.txt");
	if(!output.open(QIODevice::WriteOnly)) {
		say(QString("Could not open file: ") + output.errorString());
		return;
	}
	QTextStream out(&output);
	out << "s\r\n";

	//int numTrackPointsOfAllTrees = 0;

	// Iterate over tree
	for(int i = 0; i < fragments.size(); ++i) {
		TreeFragment* curTree = fragments[i].data();

		// Over cells
		const QHash<int, QSharedPointer<TreeFragmentTrack> >& tracks = curTree->getAllTracks();
		for(QHash<int, QSharedPointer<TreeFragmentTrack> >::const_iterator it = tracks.constBegin(); it != tracks.constEnd(); ++it) {
			out << avgSpeed(it->data()) << "\r\n";
		}

		
	}

	//say(QString("In sum %1 trackpoints").arg(numTrackPointsOfAllTrees));
}

float TTTTest::avgSpeed(TreeFragmentTrack* _track)
{
	float speedSum = 0;
	int counter = 0;

	// Get file info array
	const FileInfoArray* fia = TTTManager::getInst().getCurrentPositionManager()->getFiles();
	if(!fia) {
		qDebug() << "Warning 1";
		return -1;
	}

	//// Save speeds by timepoints in hash map
	//QHash<int, QList<float> > speeds;

	// Trackpoints
	//const QHash<int, QSharedPointer<TreeFragmentTrackPoint> >& trackpoints = _track->getTrackPoints();
	TreeFragmentTrackPoint* prevTP = 0;
	//for(QHash<int, QSharedPointer<TreeFragmentTrackPoint> >::const_iterator it = trackpoints.constBegin(); it != trackpoints.constEnd(); ++it) {
	for(int i = _track->getFirstTimePoint(); i <= _track->getLastTimePoint(); ++i) {
		TreeFragmentTrackPoint* curTp = _track->getTrackPointByTimePoint(i);
		if(!curTp) {
			qDebug() << "Warning 2";
			continue;
		}

		if(prevTP) {
			// Calc deltatime
			int seconds = fia->calcSeconds(prevTP->getTimePoint(), curTp->getTimePoint());
			if(seconds <= 0) {
				qDebug() << "Warning 3 - " << i;
				continue;
			}

			// Calc distance
			float dist = QLineF(QPointF(prevTP->posX, prevTP->posX), QPointF(curTp->posX, curTp->posX)).length();

			// Add to speed (micrometer / minute)
			speedSum += dist * 60 / seconds;
			counter++;
		}

		prevTP = curTp;
	}

	speedSum /= counter;
	return speedSum;
}

void TTTTest::displacement()
{
	say("TRF displ Hist Calculation started.");

	// Get folders
	if(!getFolders()) {
		say("Invalid folders");
		return;
	}

	// Read trees
	QList<QSharedPointer<TreeFragment> > fragments = readTrfFiles(trfSingleFolder);
	if(fragments.isEmpty())
		return;

	// Output for file to read by R
	QFile output(outFolder + "trf_disp_histogram.txt");
	if(!output.open(QIODevice::WriteOnly)) {
		say(QString("Could not open file: ") + output.errorString());
		return;
	}
	QTextStream out(&output);
	out << "x;y\r\n";

	//int numTrackPointsOfAllTrees = 0;

	// Iterate over tree
	for(int i = 0; i < fragments.size(); ++i) {
		TreeFragment* curTree = fragments[i].data();

		// Over cells
		const QHash<int, QSharedPointer<TreeFragmentTrack> >& tracks = curTree->getAllTracks();
		for(QHash<int, QSharedPointer<TreeFragmentTrack> >::const_iterator it = tracks.constBegin(); it != tracks.constEnd(); ++it) {

			TreeFragmentTrack* curCell = it->data();

			// Get first and last trackpoint
			TreeFragmentTrackPoint* first = curCell->getFirstTrackPoint();
			TreeFragmentTrackPoint* last = curCell->getTrackPointByTimePoint(curCell->getLastTimePoint());

			if(!first || !last) {
				qDebug() << "Warning 1";
				continue;
			}

			// Calc displacement
			float dx = last->posX - first->posX;
			float dy = last->posY - first->posY;

			out << dx << ";" << dy << "\r\n";
		}


	}
}

void TTTTest::expCoords()
{
	say("Export coordinates started.");

	// Get folders
	if(!getFolders()) {
		say("Invalid folders");
		return;
	}

	// Read trees
	QList<QSharedPointer<TreeFragment> > fragments = readTrfFiles(trfSingleFolder);
	if(fragments.isEmpty())
		return;

	// Output for file to read by R
	QFile output(outFolder + "trf_coords.txt");
	if(!output.open(QIODevice::WriteOnly)) {
		say(QString("Could not open file: ") + output.errorString());
		return;
	}
	QTextStream out(&output);
	out << "c;tp;x;y;xabs;yabs\r\n";

	//int numTrackPointsOfAllTrees = 0;

	int minLen = 100;

	// Iterate over tree
	int cellNumber = 1;
	for(int i = 0; i < fragments.size(); ++i) {
		TreeFragment* curTree = fragments[i].data();

		// Over cells
		const QHash<int, QSharedPointer<TreeFragmentTrack> >& tracks = curTree->getAllTracks();
		for(QHash<int, QSharedPointer<TreeFragmentTrack> >::const_iterator it = tracks.constBegin(); it != tracks.constEnd(); ++it) {
			QApplication::processEvents();

			TreeFragmentTrack* curCell = it->data();

			if(curCell->getLastTimePoint() - curCell->getFirstTimePoint() < minLen)
				continue;

			// Over trackpoints
			int counter = 0;
			for(int i = curCell->getFirstTimePoint(); i <= curCell->getLastTimePoint(); ++i) {
				TreeFragmentTrackPoint* curTp = curCell->getTrackPointByTimePoint(i);
				if(!curTp) {
					qDebug() << "Warning 2";
					continue;
				}

				out << cellNumber << ";" << ++counter /*curTp->getTimePoint()*/ << ";" << curTp->posX << ";" << curTp->posY << ";" << abs(curTp->posX) << ";" << abs(curTp->posY) << "\r\n";
				if(counter==100)
					break;
			}
			++cellNumber;
		}


	}
}

void TTTTest::absDispl()
{
	say("Export coordinates started.");

	// Get folders
	if(!getFolders()) {
		say("Invalid folders");
		return;
	}

	// Read trees
	QList<QSharedPointer<TreeFragment> > fragments = readTrfFiles(trfSingleFolder);
	if(fragments.isEmpty())
		return;

	// Output for file to read by R
	QFile output(outFolder + "trf_displacement.txt");
	if(!output.open(QIODevice::WriteOnly)) {
		say(QString("Could not open file: ") + output.errorString());
		return;
	}
	QTextStream out(&output);
	out << "c;tp;x;y;ax;ay\r\n";

	//int numTrackPointsOfAllTrees = 0;

	int minLen = 100;

	// Iterate over tree
	int cellNumber = 1;
	for(int i = 0; i < fragments.size(); ++i) {
		TreeFragment* curTree = fragments[i].data();

		// Over cells
		const QHash<int, QSharedPointer<TreeFragmentTrack> >& tracks = curTree->getAllTracks();
		for(QHash<int, QSharedPointer<TreeFragmentTrack> >::const_iterator it = tracks.constBegin(); it != tracks.constEnd(); ++it) {
			QApplication::processEvents();

			TreeFragmentTrack* curCell = it->data();

			if(curCell->getLastTimePoint() - curCell->getFirstTimePoint() < minLen)
				continue;

			// Over trackpoints
			int counter = 0;
			float sumX = 0;
			float sumY = 0;
			float absSumX = 0;
			float absSumY  = 0;
			TreeFragmentTrackPoint* prevTP = 0;
			for(int i = curCell->getFirstTimePoint(); i <= curCell->getLastTimePoint(); ++i) {
				TreeFragmentTrackPoint* curTp = curCell->getTrackPointByTimePoint(i);
				if(!curTp) {
					qDebug() << "Warning 2";
					continue;
				}

				if(prevTP) {

					// Calc abs distance
					//float dist = QLineF(QPointF(prevTP->posX, prevTP->posX), QPointF(curTp->posX, curTp->posX)).length();
					//dist = abs(dist);
					//float dx = abs(curTp->posX - prevTP->posX);
					//float dy = abs(curTp->posY - prevTP->posY);
					float dx = curTp->posX - prevTP->posX;
					float dy = curTp->posY - prevTP->posY;
					sumX += dx;
					sumY += dy;
					absSumX += abs(dx);
					absSumY += abs(dy);

					out << cellNumber << ";" << ++counter << ";" << sumX << ";" << sumY << ";" << absSumX << ";" << absSumY << "\r\n";
				
					// Test: Exactly 100 tps
					if(counter == 100)
						break;
				}

				prevTP = curTp;
			}
			++cellNumber;
		}


	}
}

void TTTTest::trfSpeedPlot()
{
	say("Trf speed plot started.");

	// Get folders
	if(!getFolders()) {
		say("Invalid folders");
		return;
	}

	// Get file info array
	const FileInfoArray* fia = TTTManager::getInst().getCurrentPositionManager()->getFiles();
	if(!fia) {
		qDebug() << "Warning 1";
		return;
	}

	// Read trees
	QList<QSharedPointer<TreeFragment> > fragments = readTrfFiles(trfSingleFolder);
	if(fragments.isEmpty())
		return;

	// Output for file to read by R
	QFile output(outFolder + "trf_speedPlot.txt");
	if(!output.open(QIODevice::WriteOnly)) {
		say(QString("Could not open file: ") + output.errorString());
		return;
	}
	QTextStream out(&output);
	out << "tp;sx;sy;asx;asy\r\n";

	//int numTrackPointsOfAllTrees = 0;

	int minLen = 10;

	// Iterate over trees
	int cellNumber = 1;
	for(int i = 0; i < fragments.size(); ++i) {
		TreeFragment* curTree = fragments[i].data();

		// Over cells
		const QHash<int, QSharedPointer<TreeFragmentTrack> >& tracks = curTree->getAllTracks();
		for(QHash<int, QSharedPointer<TreeFragmentTrack> >::const_iterator it = tracks.constBegin(); it != tracks.constEnd(); ++it) {
			QApplication::processEvents();

			TreeFragmentTrack* curCell = it->data();

			if(curCell->getLastTimePoint() - curCell->getFirstTimePoint() < minLen)
				continue;

			// Over trackpoints
			//int counter = 0;
			//float sumX = 0;
			//float sumY = 0;
			TreeFragmentTrackPoint* prevTP = 0;
			for(int i = curCell->getFirstTimePoint(); i <= curCell->getLastTimePoint(); ++i) {
				TreeFragmentTrackPoint* curTp = curCell->getTrackPointByTimePoint(i);
				if(!curTp) {
					qDebug() << "Warning 2";
					continue;
				}

				if(prevTP) {

					// Calc deltatime
					int seconds = fia->calcSeconds(prevTP->getTimePoint(), curTp->getTimePoint());
					if(seconds <= 0) {
						qDebug() << "Warning 3";
						continue;
					}

					// Calc abs distance
					//float dist = QLineF(QPointF(prevTP->posX, prevTP->posX), QPointF(curTp->posX, curTp->posX)).length();
					//dist = abs(dist);
					//float dx = abs(curTp->posX - prevTP->posX);
					//float dy = abs(curTp->posY - prevTP->posY);
					float dx = curTp->posX - prevTP->posX;
					float dy = curTp->posY - prevTP->posY;
					//sumX += dx;
					//sumY += dy;

					float speedX = dx * 60 / seconds;
					float speedY = dy * 60 / seconds;

					//out << cellNumber << ";" << ++counter << ";" << sumX << ";" << sumY << "\r\n";
					out << curTp->getTimePoint() << ";" << speedX << ";" << speedY << ";" << abs(speedX) << ";" << abs(speedY) << "\r\n";

					//// Test: Exactly 100 tps
					//if(counter == 100)
					//	break;
				}

				prevTP = curTp;
			}
			//++cellNumber;
		}


	}
}

void TTTTest::testPrediction()
{
	say("Performing error single trf folder test..");

	// Get folders
	if(!getFolders()) {
		say("Invalid folders");
		return;
	}

	// Simple Predictors
	QList<QSharedPointer<PredictorBase> > predictors;
	float f = 0;
	for(int i = 0; i < 11; ++i) {
		PredictorBase* newPred = new SimplePredictor(f);
		predictors.append(QSharedPointer<PredictorBase>(newPred));

		f += 0.1f;
	}

	// Kalman
	PredictorBase* newPred = new KalmanPredictor();
	predictors.append(QSharedPointer<PredictorBase>(newPred));
	

	// Output for file to read by R
	QFile output(outFolder + "prediction_test.txt");
	if(!output.open(QIODevice::WriteOnly)) {
		say(QString("Could not open file: ") + output.errorString());
		return;
	}
	QTextStream out(&output);
	out << "s00;s01;s02;s03;s04;s05;s06;s07;s08;s09;s10;k\r\n";
	//out << "K\r\n";

	// Read trees
	QList<QSharedPointer<Tree> > trees = readTTTFiles(tttFolder);
	if(trees.isEmpty())
		return;

	// Iterate over trees
	for(int i = 0; i < trees.size(); ++i) {
		Tree* curTree = trees[i].data();

		// Iterate over tracks of tree
		Q3IntDict<Track> tracks = curTree->getAllTracks (-1);
		for (uint i = 0; i < tracks.size(); i++) {
			Track* tmpTrack = tracks.find (i);
			if (tmpTrack) {
				// Do dat
				testPredictorsOnTrack(tmpTrack, predictors, out);
			}
		}
	}
}

void TTTTest::testPredictorsOnTrack( Track* _track, QList<QSharedPointer<PredictorBase> >& _predictors, QTextStream& _out )
{
	// Get first timepoint
	int startTp = _track->getFirstTimePoint();
	if(startTp <= 0) {
		say("Error 1");
		return;
	}

	// Get first trackpoint
	ITrackPoint* firstTrackPoint = _track->getTrackPointByTimePoint(startTp);
	if(!firstTrackPoint) {
		say("Error 2");
		return;
	}

	// Get first pos
	QPointF startPos = QPointF(firstTrackPoint->getX(), firstTrackPoint->getY());

	// Init predictors
	for(int i = 0; i < _predictors.size(); ++i)
		_predictors[i]->initialize(startPos);

	// Iterate over all following trackpoints
	int lastTimePoint = _track->getLastTimePoint();
	for(int curTP = startTp + 1; curTP <= lastTimePoint; ++curTP) {
		// Get current trackpoint
		ITrackPoint* curTrackPoint = _track->getTrackPointByTimePoint(curTP);
		if(!curTrackPoint) {
			continue;
		}
		
		// Get current position
		QPointF curPos = QPointF(curTrackPoint->getX(), curTrackPoint->getY());

		// Compare with prediction of predictors
		for(int j = 0; j < _predictors.size(); ++j) {
			QPointF prediction = _predictors[j]->predictNextPos();
			_predictors[j]->setCurrentPos(curPos);

			// Calc abs. distance
			float dist = QLineF(curPos, prediction).length();

			//// Square 
			//dist *= dist;

			// Add observation
			if(j > 0)
				_out << ";";
			_out << dist;
		}

		_out << "\r\n";
	}
}

void TTTTest::generateKalmanStats()
{
	say("Kalman stas");

	// Get folders
	if(!getFolders()) {
		say("Invalid folders");
		return;
	}

	// Predictor
	QSharedPointer<SimplePredictor> predictor(new SimplePredictor(0.5));

	// Output for file to read by R
	QFile output(outFolder + "kalman_test.txt");
	if(!output.open(QIODevice::WriteOnly)) {
		say(QString("Could not open file: ") + output.errorString());
		return;
	}
	QTextStream out(&output);
	out << "dPX;dPY;dVX;dVY\r\n";

	// Read trees
	QList<QSharedPointer<Tree> > trees = readTTTFiles(tttFolder);
	if(trees.isEmpty())
		return;

	// Iterate over trees
	for(int i = 0; i < trees.size(); ++i) {
		Tree* curTree = trees[i].data();

		// Iterate over tracks of tree
		Q3IntDict<Track> tracks = curTree->getAllTracks (-1);
		for (uint i = 0; i < tracks.size(); i++) {
			Track* tmpTrack = tracks.find (i);
			if (tmpTrack) {
				// Kalman stats
				kalmanStatsOnTrack(tmpTrack, predictor, out);
			}
		}
	}
}

void TTTTest::kalmanStatsOnTrack( Track* _track, QSharedPointer<SimplePredictor>& _predictor, QTextStream& _out )
{
	// Get first timepoint
	int startTp = _track->getFirstTimePoint();
	if(startTp <= 0) {
		say("Error 1");
		return;
	}

	// Get first trackpoint
	ITrackPoint* firstTrackPoint = _track->getTrackPointByTimePoint(startTp);
	if(!firstTrackPoint) {
		say("Error 2");
		return;
	}

	// Get first pos
	QPointF startPos = QPointF(firstTrackPoint->getX(), firstTrackPoint->getY());

	// Init predictor
	_predictor->initialize(startPos);

	// Iterate over all following trackpoints
	int counter = 0;
	QPointF lastPos;
	for(int curTP = startTp + 1; curTP <= _track->getLastTimePoint(); ++curTP) {
		// Get current trackpoint
		ITrackPoint* curTrackPoint = _track->getTrackPointByTimePoint(curTP);
		if(!curTrackPoint) {
			continue;
		}

		// Get current position
		QPointF curPos = QPointF(curTrackPoint->getX(), curTrackPoint->getY());

		// Compare with prediction of predictor
		QPointF prediction = _predictor->predictNextPos();

		// Only after more than the first 2 timepoints the predictor can estimate the speed
		if(++counter > 1) {
			// Difference of current position to predicted position
			float pdiffX = curPos.x() - prediction.x();
			float pdiffY = curPos.y() - prediction.y();

			// Difference of current speed to predicted (i.e. last) speed
			QPointF predSpeed = _predictor->getSpeed();
			QPointF currentSpeed = curPos - lastPos;
			float vdiffX = currentSpeed.x() - predSpeed.x();
			float vdiffY = currentSpeed.y() - predSpeed.y();

			_out << pdiffX << ";" << pdiffY << ";" << vdiffX << ";" << vdiffY;
			_out << "\r\n";
		}
		_predictor->setCurrentPos(curPos);

		lastPos = curPos;
			// Calc distances

		
	}
}

void TTTTest::generateKalmanInitErrStats()
{
	say("Kalman start error stats");

	// Get folders
	if(!getFolders()) {
		say("Invalid folders");
		return;
	}

	// Output for file to read by R
	QFile output(outFolder + "kalman_startErr.txt");
	if(!output.open(QIODevice::WriteOnly)) {
		say(QString("Could not open file: ") + output.errorString());
		return;
	}
	QTextStream out(&output);
	out << "posX;posY;vX;vY\r\n";

	// Read trees
	QList<QSharedPointer<Tree> > trees = readTTTFiles(tttFolder);
	if(trees.isEmpty())
		return;

	// Iterate over trees
	for(int i = 0; i < trees.size(); ++i) {
		Tree* curTree = trees[i].data();

		// Iterate over tracks of tree
		Q3IntDict<Track> tracks = curTree->getAllTracks (-1);
		for (uint i = 0; i < tracks.size(); i++) {
			Track* tmpTrack = tracks.find (i);
			if (tmpTrack) {
				
				// Iterate over trackpoints
				int startTp = tmpTrack->getFirstTimePoint();
				int stopTp = tmpTrack->getLastTimePoint();

				if(startTp && stopTp) {
					QPointF lastPost;
					for(int curTp = startTp; curTp <= stopTp; ++curTp) {
						// Get current trackpoint
						ITrackPoint* curTrackPoint = tmpTrack->getTrackPointByTimePoint(curTp);
						if(curTrackPoint) {
							QPointF curPos = QPointF(curTrackPoint->getX(), curTrackPoint->getY());

							// Calc speed
							if(!lastPost.isNull()) {
								QPointF speed = curPos - lastPost;

								out << "0;0;" << speed.x() << ";" << speed.y();
								out << "\r\n";
							}

							lastPost = curPos;
						}
					}

				}
			}
		}
	}
}

void TTTTest::exportSpeedOfTrfs()
{
	say("Trfs speed plot started.");

	// Get folders
	if(!getFolders()) {
		say("Invalid folders");
		return;
	}

	// Read trees
	QList<QSharedPointer<TreeFragment> > fragments = readTrfFiles(trfSingleFolder);
	if(fragments.isEmpty())
		return;

	// Output for file to read by R
	QFile output(outFolder + "trf_speeds.txt");
	if(!output.open(QIODevice::WriteOnly)) {
		say(QString("Could not open file: ") + output.errorString());
		return;
	}
	QTextStream out(&output);
	out << "tp;sx;sy;euc\r\n";

	//int numTrackPointsOfAllTrees = 0;

	int minLen = 10;

	// Iterate over trees
	int cellNumber = 1;
	for(int i = 0; i < fragments.size(); ++i) {
		TreeFragment* curTree = fragments[i].data();

		// Over cells
		const QHash<int, QSharedPointer<TreeFragmentTrack> >& tracks = curTree->getAllTracks();
		for(QHash<int, QSharedPointer<TreeFragmentTrack> >::const_iterator it = tracks.constBegin(); it != tracks.constEnd(); ++it) {
			QApplication::processEvents();

			TreeFragmentTrack* curCell = it->data();

			if(curCell->getLastTimePoint() - curCell->getFirstTimePoint() < minLen)
				continue;

			// Over trackpoints
			//int counter = 0;
			//float sumX = 0;
			//float sumY = 0;
			TreeFragmentTrackPoint* prevTP = 0;
			for(int i = curCell->getFirstTimePoint(); i <= curCell->getLastTimePoint(); ++i) {
				TreeFragmentTrackPoint* curTp = curCell->getTrackPointByTimePoint(i);
				if(!curTp) {
					qDebug() << "Warning 2";
					continue;
				}

				if(prevTP) {


					// Calc abs distance
					//float dist = QLineF(QPointF(prevTP->posX, prevTP->posX), QPointF(curTp->posX, curTp->posX)).length();
					//dist = abs(dist);
					//float dx = abs(curTp->posX - prevTP->posX);
					//float dy = abs(curTp->posY - prevTP->posY);
					float dx = curTp->posX - prevTP->posX;
					float dy = curTp->posY - prevTP->posY;
					//sumX += dx;
					//sumY += dy;


					//out << cellNumber << ";" << ++counter << ";" << sumX << ";" << sumY << "\r\n";
					out << curTp->getTimePoint() << ";" << dx << ";" << dy << ";" << sqrt(dx*dx + dy * dy) << "\r\n";

					//// Test: Exactly 100 tps
					//if(counter == 100)
					//	break;
				}

				prevTP = curTp;
			}
			//++cellNumber;
		}


	}
}

void SimplePredictor::initialize( const QPointF& _startPos )
{
	lastPos = _startPos;
	beforeLastPos = QPointF();
}

void SimplePredictor::setCurrentPos( const QPointF& _point )
{
	beforeLastPos = lastPos;
	lastPos = _point;
}

QPointF SimplePredictor::predictNextPos()
{
	if(beforeLastPos.isNull())
		return lastPos;

	// Calc difference
	QPointF diff = lastPos - beforeLastPos;

	// Apply factor
	diff *= factor;

	// Add to last pos
	return lastPos + diff;
}

QPointF SimplePredictor::getSpeed()
{
	// Check if needed variables are set
	if(beforeLastPos.isNull())
		return QPointF();

	// Calc speed
	return lastPos - beforeLastPos;
}

KalmanPredictor::KalmanPredictor()
{
#ifdef DEBUGMODE
	//// Create kalman filter (4 dimensions for state vector, 2 for measurement, 0 for control)
	//kalman = cvCreateKalman(4, 2, 0);

	//// State transition 4x4 matrix (row by row)
	//const float F[] = {1, 0, 0.5, 0, 0, 1, 0, 0.5, 0, 0, 1, 0, 0, 0, 0, 1};

	////const float F[] = {	1, 0, 1, 0, 
	////					0, 1, 0, 1, 
	////					0, 0, 1, 0, 
	////					0, 0, 0, 1};
	//memcpy(kalman->transition_matrix->data.fl, F, sizeof(F));

	//// Process noise 4x4 covariance matrix (row by row)
	//const float Q[] = {	20.3378399f, -1.4339325f, 19.4920905f, -0.8616682f,
	//					-1.4339325f, 25.2301357f, -0.7135724f, 22.9775666f,
	//					19.4920905f, -0.7135724f, 25.9722092f, -1.0268858f,
	//					-0.8616682f, 22.9775666f, -1.0268858f, 30.6803891f};
	//memcpy(kalman->process_noise_cov->data.fl, Q, sizeof(Q));

	//// Measurement 2x4 matrix (row by row)
	//const float H[] = {1, 0, 0, 0, 0, 1, 0, 0};
	//memcpy(kalman->measurement_matrix->data.fl, H, sizeof(H));

	//// Measurement 2x2 noise matrix (row by row)
	//const float R[] = {0.1f, 0, 0, 0.1f};
	//memcpy(kalman->measurement_noise_cov->data.fl, R, sizeof(R));


	//// Initial error estimate 4x4 matrix (row by row)
	//const float P[] = {	0.0827660371f,  0.0004379691f,  0.0054604609f, -0.004297663f,
	//					 0.0004379691f,  0.0825015033f, -0.0007271984f,  0.048037796f,
	//					 0.0054604609f, -0.0007271984f, 58.2483742424f,  0.579080950f,
	//					 -0.0042976628f,  0.0480377964f,  0.5790809503f, 63.319274861f};
	//memcpy(kalman->error_cov_post->data.fl, P, sizeof(P));

	//// choose random initial state (Bullshit?)
	////CvRandState rng; 
	////cvRandInit( &rng, 0, 1, -1, CV_RAND_UNI );
	////cvRandSetRange( &rng, 0, 1, 0 );
	////rng.disttype = CV_RAND_NORMAL; 
	////cvRand( &rng, m_pKalmanFilter->state_post ); 

	//// Measurement matrix
	//measurement = cvCreateMat( 2, 1, CV_32FC1 );

#endif
}

void KalmanPredictor::initialize( const QPointF& _startPos )
{
#ifdef DEBUGMODE
	//// Set initial state
	//float x[] = {_startPos.x(), _startPos.y(), 0, 0};
	//memcpy(kalman->state_post->data.fl, x, sizeof(x));
#endif
}

QPointF KalmanPredictor::predictNextPos()
{
//#ifdef DEBUGMODE
//	// Predict next pos
//	const CvMat* predState = cvKalmanPredict(kalman, 0);
//
//	// Extract position
//	QPointF pos(predState->data.fl[0], predState->data.fl[1]);
//
//	// DEBUG
//	CvMat* errCov = kalman->error_cov_pre;
//	float* f = errCov->data.fl;
//
//	return pos;
//#else
	return QPointF();
//#endif
}

void KalmanPredictor::setCurrentPos( const QPointF& _point )
{
#ifdef DEBUGMODE

	//// Put in cv matrix
	//measurement->data.fl[0] = _point.x();
	//measurement->data.fl[1] = _point.y();

	////// DEBUG
	////float f0x = kalman->state_post->data.fl[0],
	////	f0y = kalman->state_post->data.fl[1];

	//// Update kalman filter
	//cvKalmanCorrect(kalman, measurement);

	////// DEBUG
	////float f1x = kalman->state_post->data.fl[0],
	////	f1y = kalman->state_post->data.fl[1];

#endif
}

KalmanPredictor::~KalmanPredictor()
{
#ifdef DEBUGMODE
	//cvReleaseKalman(&kalman);

	//cvReleaseMat(&measurement);
#endif
}
