/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef GRAPHICAID_H
#define GRAPHICAID_H

#include "tttbackend/displaysettings.h"

#include <qcolor.h>
#include <qstring.h>
#include <QElapsedTimer>

// OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

/**
 *	@author Bernhard
 * 
 * 	This class holds several functions that help with graphical displays, e.g. a simple color run.
*/

const QColor mainColors[] = {	Qt::white, Qt::red, Qt::darkRed, Qt::green, Qt::darkGreen, Qt::blue, Qt::darkBlue,
                                Qt::cyan, Qt::darkCyan, Qt::magenta, Qt::darkMagenta, Qt::yellow, Qt::darkYellow,
                                Qt::gray, Qt::darkGray, Qt::lightGray};

///@todo include all icons as xpm graphics!

class GraphicAid{

public:
	GraphicAid();
	
	~GraphicAid();
	
	/**
	 * returns the next color (by RGB value) in a color run
	 * @param _color the current color
	 * @return a QColor object with the next color from the run mainColors[]
	 */
	static QColor next (QColor _color);
	
	/**
	 * returns the _index-th main qt color (modulo, if _index > 15)
	 * @param _index the array index
	 * @return a QColor object with the Qt color at _index
	 */
	static QColor mainColor (uint _index);
	
	/**
	 * saves a color to the provided data stream
	 * @param _writer the data stream (must be associated with a file already)
	 * @param _color the color to be saved
	 * @return the data stream (at the next position)
	 */
	static void saveColor (QDataStream &_writer, const QColor &_color);
	
	/**
	 * receives a color object from the provided stream
	 * @param _str the data stream (must be associated with a file already)
	 * @param _color the color object to be read
	 * @return the data stream (at the next position)
	 */
	static void readColor (QDataStream& _str, QColor &_color);
	
	/**
	 * Converts a color to a long value
	 * @deprecated
	 * @param _color the color
	 * @return the long value of the color
	 */
	static unsigned int colorToLong (const QColor &_color);
	
	/**
	 * recovers a color from a long value
	 * @deprecated
	 * @param _color the color as long value
	 * @return the color as QColor object
	 */
	static QColor longToColor (unsigned int _color);
	
	/**
	 * a color to string function; uses QColor::name()
	 * @param _color a QColor object
	 * @return the color's name
	 */
	static const QString colorToString (const QColor &_color);
	
	/**
	 * string to color
	 * @param _name the color in a format QColor can parse
	 * @return the color constructed from _name
	 */
	static QColor stringToColor (const QString &_name);
	
        /**
         * overlays the two provided pictures
         * expects both QImage objects to be in 32bpp mode
         * @param _img1 the source picture (the base to which the other one is merged)
         * @param _img2 the overlay picture (with alpha channel set)
         * @param _useAlpha if true, _img2 is overlayed over _img1 with the special alpha channel settings; if false, the colors of _img1 and _img2 are simply ORed together
         * //@return the altered _img1
         */
        static void overlay (QImage *_img1, const QImage *_img2, bool _useAlpha = false);

        /**
         * returns the provided 8bpp image converted into 32bpp
         * the original image is not altered
         * @param _img the QImage object to be converted (in bit mode < 32)
         * @return the converted image (now in 32 bits per pixel mode)
         */
        static QImage to32bpp (const QImage *_img);

        /**
         * converts _color to greyscale (formula see implementation) and returns the new color
         * @param _color the color to be converted
         * @return a QColor object containing r=g=b with the grayscale value of _color
         */
        static QColor convertToGrayScale(QColor _color);

        /**
         * adjusts the _img with the black/white point, contrast and gamma function set in the current internal BDisplay object
         * @param _img the image to be adjusted
         * @param alpha which alpha/colorchannel to use (cf. drawPixmap())
         * @return RGB or Format_Indexed8 (for 8 bit) QImage with applied display transformations (e.g. contrast enhancement and trans-color)
         */
        static QImage adjustImageGraphicsForDisplay (const cv::Mat& inputImage, const BDisplay &_settings);


		private:

		/**
		 *	Helper function of GraphicAid::adjustImageGraphicsForDisplay().
		 */
		static QImage adjust16BitImageGraphicsForDisplayInternal (const cv::Mat& inputImage, const BDisplay &_settings)
		{
			//QElapsedTimer t;
			//t.start();

			// Create output image
			QImage retImage(inputImage.cols, inputImage.rows, QImage::Format_ARGB32);

			// Normalization factor for input data to map to [0,1], i.e. 255 for 8 bit, 65535 for 16 bit, etc.
			float scaleFactor = (1 << (inputImage.elemSize1() << 3)) - 1;

			// Copy and transform data
			int alpha = _settings.Alpha >= 0 ? _settings.Alpha : 255;
			float bp = _settings.BlackPoint / 255.0;
			float wp = _settings.WhitePoint / 255.0;
			for(int y = 0; y < retImage.height(); ++y) {
				const unsigned short* srcRow = inputImage.ptr<unsigned short>(y);
				QRgb* dstRow = reinterpret_cast<QRgb*>(retImage.scanLine(y));
				for(int x = 0; x < retImage.width(); ++x) {
					// Transform value to [0,255] using black point and white point
					int val;
					float srcValNormalized = srcRow[x] / scaleFactor;
					if(srcValNormalized <= bp) {
						val = 0;
					}
					else if(srcValNormalized >= wp) {
						val = 255;
					}
					else {
						// Interpolate between closest values in _settings.GammaValues color table (which has
						// only 256 entries) to avoid binning effects when working with 16 bit images
						float grayLevel = srcValNormalized * 255.0f;
						int grayLevelFloor = grayLevel;
						int grayLevelCeil = std::ceil(grayLevel);
						if(grayLevelFloor != grayLevelCeil) {
							// Interpolate
							float w1 = 1.0f - (grayLevel - grayLevelFloor);
							float w2 = 1.0f - w1;
							val = _settings.GammaValues[grayLevelFloor] * w1 + _settings.GammaValues[grayLevelCeil] * w2;
							assert(val >= 0 && val <= 255);
						}
						else {
							// Value maps exactly to one 8-bit value -> no interpolation needed
							val = _settings.GammaValues[grayLevelFloor];
						}
					}


					// Some weird contrast enhancement
					if (_settings.Contrast != 100) {

						//steps:
						//map color value to interval [0, 1]
						//transform color
						//map result value to interval [blackpoint, whitepoint]
						float cvf = (float)val;
						float b = (float)_settings.Contrast / 10.0f;
						float a = 0.5f;
						//cvf must lie in the range between 0 (=blackpoint) and 1 (=whitepoint)
						//normalization function: y = m * x + t, where m := 1/(wp-bp) and t := 1-m*wp
						float m = 1.0f / ((float)_settings.BWDiff());
						float t = 1.0f - m * (float)_settings.WhitePoint;
						cvf = m * cvf + t;

						cvf = (1/(1+exp(b*(a-cvf))) - 1/(1+exp(b))) / (1/(1+exp(b*(a-cvf))/(1+exp(b))));

						//now the cvf value has to be remapped to the real region
						//the function is now: y = m * x + t, where m := wp-bp and t := bp
						m = (float)_settings.BWDiff();
						t = (float)_settings.BlackPoint;
						cvf = m * cvf + t;
						val = cvf;
					}

					//apply brightness
					if (_settings.Brightness != 1.0f) {
						float xb = 1.0f / _settings.Brightness;
						val = (int) ( (pow ( float (val) / 255.0f, xb)) * 255.0f);
					}

					//if any color translation is applied, a color run is introduced
					//current politics:
					//only the channel can be selected (red, green or blue)
					//this channel is set to (gray scale value), the other channels are set to 0
					if (_settings.TransColorIndex > -1 && _settings.TransColorIndex < 3) {
						if (_settings.TransColorIndex == 0)
							dstRow[x] = qRgba (val, 0, 0, alpha);
						if (_settings.TransColorIndex == 1)
							dstRow[x] = qRgba (0, val, 0, alpha);
						if (_settings.TransColorIndex == 2)
							dstRow[x] = qRgba (0, 0, val, alpha);
					}
					else
						dstRow[x] = qRgba(val, val, val, alpha);
				}
			}

			//qDebug() << "adjust16BitImageGraphicsForDisplayInternal(): " << t.elapsed();

			// Done
			return retImage;
		}

		// Faster code for 8-bit grayscale images
		static QImage adjust8BitImageGraphicsForDisplayInternal (const cv::Mat& inputImage, const BDisplay &_settings)
		{
			//QElapsedTimer t;
			//t.start();

			if(!inputImage.data)
				return QImage();

			// Create color table
			QVector<QRgb> colorTable(256);
			for(int color = 0; color < 256; ++color) {
				QRgb& c = colorTable[color];
				if(color < _settings.BlackPoint)
					c = qRgb (0, 0, 0);
				else if(color > _settings.WhitePoint)
					c = qRgb (255, 255, 255);
				else
					c = qRgb(_settings.GammaValues[color], _settings.GammaValues[color], _settings.GammaValues[color]);

				if (_settings.Contrast != 100) {
					int cv = qRed (c);

					//steps:
					//map color value to interval [0, 1]
					//transform color
					//map result value to interval [blackpoint, whitepoint]
					float cvf = (float)cv;
					float b = (float)_settings.Contrast / 10.0f;
					float a = 0.5f;
					//cvf must lie in the range between 0 (=blackpoint) and 1 (=whitepoint)
					//normalization function: y = m * x + t, where m := 1/(wp-bp) and t := 1-m*wp
					float m = 1.0f / ((float)_settings.BWDiff());
					float t = 1.0f - m * (float)_settings.WhitePoint;
					cvf = m * cvf + t;

					cvf = (1/(1+exp(b*(a-cvf))) - 1/(1+exp(b))) / (1/(1+exp(b*(a-cvf))/(1+exp(b))));

					//now the cvf value has to be remapped to the real region
					//the function is now: y = m * x + t, where m := wp-bp and t := bp
					m = (float)_settings.BWDiff();
					t = (float)_settings.BlackPoint;
					cvf = m * cvf + t;

					cv = (int)cvf;

					c = qRgb (cv, cv, cv);
				}

				//apply brightness
				if (_settings.Brightness != 1.0f) {
					float xb = 1.0f / _settings.Brightness;
					int cv = (int) ( (pow ( float (qRed (c)) / 255.0f, xb)) * 255.0f);
					c = qRgb (cv, cv, cv);
				}

				//if any color translation is applied, a color run is introduced
				//current politics:
				//only the channel can be selected (red, green or blue)
				//this channel is set to (gray scale value), the other channels are set to 0
				if ((_settings.TransColorIndex > -1) & (_settings.TransColorIndex < 3)) {
					if (_settings.TransColorIndex == 0)
						c = qRgb (qRed (c), 0, 0);
					if (_settings.TransColorIndex == 1)
						c = qRgb (0, qGreen (c), 0);
					if (_settings.TransColorIndex == 2)
						c = qRgb (0, 0, qBlue (c));
				}

				//apply alpha blending
				if (_settings.Alpha > 0) {
					c = qRgba (qRed (c), qGreen (c), qBlue (c), _settings.Alpha);
				}
			}

			// Create QImage (we need to make a deep copy, because otherwise Qt crashes randomly when
			// accessing the QImage later)
			QImage retImage(inputImage.cols, inputImage.rows, QImage::Format_Indexed8);
			if(retImage.isNull())
				return QImage();
			for(int y = 0; y < inputImage.rows; ++y) {
				const unsigned char* src = inputImage.data + y*inputImage.cols;
				unsigned char* dst = retImage.scanLine(y);
				memcpy(dst, src, inputImage.cols);
			}

			// Set color table
			retImage.setColorTable(colorTable);

			//qDebug() << "adjust8BitImageGraphicsForDisplayInternal(): " << t.elapsed();

			return retImage;
		}
};

#endif
