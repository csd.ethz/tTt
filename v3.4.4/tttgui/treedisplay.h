/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TREEDISPLAY_H
#define TREEDISPLAY_H

//own classes
#include "tttdata/tree.h"
#include "tttdata/track.h"
#include "myqcanvasellipse.h"
#include "myqcanvasline.h"
#include "myqcanvaslinespecial.h"
#include "myqcanvastext.h"
#include "tttdata/stylesheet.h"
#include "tttdata/trackingkeys.h"
#include "tttbackend/compileflags.h"

//GUI elements
#include <qwidget.h>


//functional modules
#include <qevent.h>
#include <qpoint.h>
#include <qpainter.h>
#include <qcolor.h>
#include <qpen.h>
#include <qbrush.h>
#include <qtimer.h>
#include <qsize.h>
#include <q3canvas.h>
#include <qmatrix.h>
#include <qcursor.h>
#include <q3popupmenu.h>
//Added by qt3to4:
#include <QContextMenuEvent>
#include <QMouseEvent>



///the minimum horizontal size for the tree
const int TREEWINDOWWIDTHMINIMUM = 1000;

///the width to which to which the tree should be scaled (when it is too broad) for tree bmp export
///necessary because a very broad tree does not look good after export when the complete tree should be visible
const int EXPORTTREEWIDTH = 6000;

///the width for the cell speed window (clips a part from the complete tree with TreeWindowWidthComplete)
const int SPEEDWINDOWWIDTH = 50;



///usage constants for the tree items (lines, circles, text) so that the drawing can be managed efficiently
const int LineUsageHeritage = 1;
const int LineUsageDivision = 2;
const int LineUsageWavelength = 3;
const int LineUsageSpeedCell = 4;
const int LineUsageSpeedBar = 5;
const int LineUsageColocation = 6;
const int LineUsageState = 7;
const int LineUsageTimepoint = 8;
const int LineUsageDaylines = 9;
const int LineUsageTissue = 10;
const int LineUsageGeneralType = 11;
const int LineUsageLineage = 12;
const int LineUsageAddOn = 13;
const int LineUsageFinishedMark = 14;

const int EllipseUsageCell = 101;

const int TextUsageCellNumber = 201;
const int TextUsageGenerationTime = 202;
const int TextUsageComment = 203;
const int TextUsageStopReason = 204;
const int TextUsageDaylines = 205;
const int TextUsageCellDistance = 206;
const int TextUsageEndomitosis = 207;
const int TextUsageTreeName = 208;
const int TextUsageClusterID = 209;


///z value constants for some objects (default == MyQCanvas... default)
///a higher z value object hides lower z value objects
///z values should be between 0 and 1
const float Z_ColocationLine = 0.5; 	//behind normal tree objects
const float Z_TimepointLine = 0.6;	//behind tree objects
const float Z_DayHourLine = 0.4;	//behind timepoint line and colocation lines



/*
	Definition:
	
	the logical coordinate system is spanned from (0, FirstTimePoint) to (TreeWindowWidth, LastTimePoint)
	the real coordinate system is the geometry of the displaying widget
	for display, the current clip part of the logical system is stretched to fit into the real one
	 -> a transformation of logical coordinates to real ones
	a mouse click is in real coordinates and for evaluation is transformed to logical ones
	on the other way, all cell positions are in logical coordinates and have to be transformed to real 
	 coordinates on demand
*/



/**
	@author Bernhard
	
	A widget for displaying the cell tree, including all necessary functions. It is based on a QCanvasView, which is 
	 designed to hold lots of objects like lines, circles, ...
	Every mouse position is specified in the receiving widget's coordinates, if not defined otherwise for any method.
*/

class TreeDisplay : public Q3CanvasView
{
	Q_OBJECT
	
public:
	TreeDisplay (QWidget *_container, QWidget *_parent = 0, const char *_name = 0);
	
	~TreeDisplay();
	
	/**
	 * initializes the tree display with a tree and the position manager to which the tree belongs 
	 * @param _tree the tree
	 * @param _tttpm the position manager of this tree (must be initialized)
	 */
	void init (Tree *_tree, TTTPositionManager *_tttpm);
	
	///possibly the user has selected a cell/track or a comment
	void contentsMouseReleaseEvent (QMouseEvent *);
	void contentsMousePressEvent (QMouseEvent *);
	void contentsMouseMoveEvent (QMouseEvent *);
	void contentsMouseDoubleClickEvent (QMouseEvent *);
	
	void contextMenuEvent (QContextMenuEvent *);
	
	///resizes the widget to the containers size
	void fit();
	
	///sets the current track
	void setCurrent (Track *_track);	
	
	///sets _timePoint to be in the displayed region
	void displayTimePoint(); // (int _timePoint);
	
	///exports the currently visible view as bmp-file
	///@param _filename the filename where the tree should be stored
	///@param _completeTree: 	whether the complete tree or just the visible part should be saved 
	///				(default == only the visible clip)
	///@param _unstretched whether the tree should be stretched before exporting, if it is very broad
	///@param _printIndex whether the tree name (position + colony) should be printed into the tree before exporting
	///@param _index the index to be printed (usually the colony name)
	///@param _redraw whether the tree should be redrawn (default = false)
	///@param _format the picture format (default = "BMP")
	void exportTree (QString _filename, bool _completeTree, bool _unstretched, bool _printIndex = false, const QString &_index = "", bool _redraw = false, const QString &_format = "BMP");
	
	///returns the canvas object
	///should not be altered from outside
	Q3Canvas* getCanvas()
		{return &canvas;}
	
	///returns the rectangle in which the selected cell circle is located
	///if _lastTrace == true, not the first trace is taken as y position, but the last trace (e.g. for apoptosis)
	QRect cellPosition (Track *_track, bool _lastTrace = false) const;
	
	///as above, but with coordinates within a TreeDisplay instance
	QRect cellPositionReal (Track *_track, bool _lastTrace = false) const;
	
	///returns the line drawing position of _track
	QRect linePosition (Track *_track) const;
	
	///returns the track that is under the mouse (_pos is the mouse position in widget coordinates)
	///recursive; at first call _track == BaseTrack
	Track* getPosTrack (QPoint _pos) const;
	
	///selects the cell at position _mousePos (_select == false deselects)
	///the return value is a pointer to the selected track
	///@param _mousePos in widget coordinates
	///@param _select whether the track should be selected or unselected
	///@param _multiple: whether the cell should be marked additionally
	Track* selectTrack (const QPoint &_mousePos, bool _select = true, bool _multiple = false);
	
	///selects the provided track
	///@param _mousePos in widget coordinates
	///@param _select whether the track should be selected or unselected
	///@param _multiple: whether the cell should be marked additionally
	///@param _updateView whether the canvas should be updated after the cell was (de)selected
	void selectTrack (Track *_track, bool _select = true, bool _multiple = false, bool _updateView = true);

	///deletes the selection marker of all currently selected tracks
	void deselectTrack();

	///getter & setter for the zoom factor
	void setZoomFactor (int _zoomFactor);
	int getZoomFactor() const
		{return zoomFactor;}
	
	///getter & setter for the currently displayed region
	///(the interactive change is handled in TreeDisplay)
	void setDisplayRegion (QRect _rect);
	QRect getDisplayRegion() const;
	
	///set the flag for showing colocation of cells
	void setShowColocation (bool _show, bool _allCells = false);

	///set the flag for showing the cell speed
	void setShowSpeed (bool _show);
	
	///checks whether the user clicked on an asterisk
	///@param _pos the (mouse) click position in real coordinates
	///if yes, the desired comment is returned in the signal commentSelected() and true is returned
	///@return false, if no comment was selected; true otherwise
	bool checkCommentSelection (QPoint _pos); // const;
	
	///checks whether the user clicked on an endomitosis symbol
	///if yes, it should be deleted
	///@param _pos the (mouse) click position in real coordinates
	///@return the Track pointer of the track to which the symbol belonged, if there was one; 0 otherwise
	Track* checkEndomitosisSelection (QPoint _pos);
	
	///returns the track under _pos (mostly the mouse position)
	Track *getTrack (QPoint _pos) const;
	
	///adapts the currently displayed region after the size of the displaying widget (Area) changed
	//void updateDisplayedRegion (QSize _oldSize);
	
	///return the size of the tree in pixels
	int getTreeWidth() const
		{return TreeWindowWidth;}
	int getTreeHeight() const
		{return TreeWindowHeight;}
	
	///changes between displaying the generation time for each cell or not
	void setShowGenerationTime (bool _show);
	
	///whether _track is currently selected
	///@return 0 <=> the track is not selected; 1 <=> the track is the main selected track; 2 <=> track selected, but not as main track
	int isTrackSelected (Track *_track) const;
	
	///returns all currently selected tracks, the main selected is the first entry (index 0), if included
	///@param _includeMain: whether the main selected track (== SelectedTrack) should be included
	Q3IntDict<Track> getSelectedTracks (bool _includeMain = true) const;
	
	/**
	 * @return the number of currently selected tracks
	 */
	int countSelectedTracks() const;
	
	///sets & gets the speed amplifier value
	void setSpeedAmplification (int _amp)
		{SpeedAmplifier = _amp;}
	int getSpeedAmplification() const
		{return SpeedAmplifier;}
	
	///sets the radius (in pixel) within which cells should be considered coexisting
	void setColocationRadius (int _radius) 
		{ColocationRadius = _radius;}
	int getColocationRadius() const
		{return ColocationRadius;}
	
	int getLWF() const
		{return LineWidthFactor;}
	
	///draws the current tree into TreePicture and TreeImage
	///only the specified track, its children (cascadeously) and all tracks
	/// of the same generation are drawn -> nothing above, as there is no change
	///@param _track: == 0 => all tracks are drawn
	///@param _drawTracks: whether the tracks should be drawn or just the memory QImage be updated
	///@param _speedOnly: == true => only the speed display in the right is drawn for the provided _track
	///@param _colocOnly: == true => only the colocalization for the provided track is drawn
	///@param _drawCascade whether the track cascade should be drawn, if _track is specified (otherwise really only the provided track is updated)
	void draw (Track *_track = 0, bool _drawTracks = true, bool _speedOnly = false, bool _colocOnly = false, bool _drawCascade = true);
	
	///deletes all objects on the canvas which fulfill the specifications of the parameters
	///NOTE you have to call getCanvas()->update() after clearing all desired objects
	///@param _speedOnly whether only the speed lines should be removed
	///@param _colocOnly whehher only the colocation lines should be removed
	void clear (bool _speedOnly = false, bool _colocOnly = false);
	
	///deletes all objects on the canvas that belong to the provided track and its children
	///@param _track the track for which all objects should be deleted
	///@param _deleteChildren whether the children's items should be deleted also
	///@param _speedOnly whether only the speed lines should be removed
	///@param _colocOnly whehher only the colocation lines should be removed
	void clear (Track *_track, bool _deleteChildren = true, bool _speedOnly = false, bool _colocOnly = false);
	
	///deletes the Track object from the tree and the display
	///@param _track the track reference
	///@param _noInteraction whether the user should not be asked about the _track's children
	///@return success
	bool deleteTrack (Track *_track, bool _noInteraction = false);
	
	///just updates the canvas (can be necessary from outside, if e.g. clear() was called)
	void update();
	
	/**
	 * Grabs the tree image between the specified timepoints (but with full size of the complete image)
	 * @param _topTP the first visible timepoint (-1 => experiment first timepoint)
	 * @param _bottom the last visible timepoint (-1 => experiment last timepoint)
	 * @return a QImage pointer that contains the desired tree region (always in full tree size, rest is blank)
	 */
	QImage* getTreeImage (int _topTP = -1, int _bottomTP = -1);

	void setDrawContinuousCellLines(bool _on);
	
	
signals:
	
	///emitted when the user clicked on an asterisk to see the corresponding comment
	///@param _comment the comment string
	///@param _position the position of the mouse click
	///@param _timepoint the timepoint of the click
	void commentSelected (QString, QPoint, int);
	
	///emitted when the user selects a cell via mouse
	///@param _track the selected track
	void trackSelected (Track *_track);
	
	///emitted when the user doubleclicks a cell in order to start tracking
	///@param _track the track on which was doubleclicked
	void trackDoubleclicked (Track *_track);
	
	///emitted when the tree was shifted (necessary for analog time scale shifting)
	///@param _shift the shift in canvas coordinates
	///@param _twhf contains the current height factor (seconds |-> pixel) to restore the shift in pixel
	void treeShifted (QPoint _shift, int _twhf);
	
	///emitted when the user clicks anywhere on the tree (-> no comment is selected)
	///if the user in fact clicked on an asterisk to see an comment, there is another signal sent by Tree
	void commentDeSelected();
	
public slots:
	
	///shifts the displayed region with _offset
	///@param _offset the shift in pixels
	void shiftDisplay (QPoint _offset);
	
	/**
	 * sets the center of the display to the provided point (with regard to its bounds)
	 * @param _newCenter the new center point of the view
	 */
	void setDisplayCenter (QPoint _newCenter);

	///called when the tree emits signal TreeSizeChanged
	void updateSize();

private slots:
	
	///called when an update in the Tree object was made so that Tree::init() was called
	///calculates the line width factor and the font size from the current tree window height factor
	///@param _twhf the current tree window height factor, as calculated by the assigned Tree object
	void calcFactors (int _twhf);
	
	///called when the QCanvasView fires the event contentsMoved()
	///sends a signal to a TimeScale object that it should move either
	///@param _newX the new x position (after the move was performed)
	///@param _newY the new y position
	void moveDisplay (int _newX, int _newY);
	
	/**
	 * tests whether there is exactly one cell selected, and if yes, emits the signal trackDoubleclicked(), thus 
	 *  triggering a cell tracking
	 */
	void triggerCellTracking();
	
	/**
	 * Shows a dialog, customized for the the currently selected cell(s), where the user can select to load pictures
	 * The idea is to load the pictures in which the cell has lately roamed to continue tracking,
	 * This can be done across several positions, for an arbitrary range of pictures and for one or more cells.
	 */
	void selectCellDwellingPictures();
	
private:

	///the canvas object on which the tree is painted
	Q3Canvas canvas;
	
	///the current tree (is set in init())
	Tree *tree;
	
	///the position manager to which the current tree belongs (is set in init())
	TTTPositionManager *treeTTTPM;
	
	///the widget in which THIS is inserted
	QWidget *Container;
	
	///if the middle mouse button is currently pressed
	bool MiddleButtonPressed;
	
	///the old mouse position (necessary for dragging the viewport)
	QPoint oldMousePos;
	
	///the zoom factor for the tree (in percent); default == 100
	int zoomFactor;
	
	///contains the rectangle that actually should be displayed 
	///in zoom mode 100, its size is exactly the size of the displaying widget
	///in zoom mode 200, its size is exactly half the size of the displaying widget
	///the viewport can be shifted with the middle mouse button
	///the coordinates are in pixel, not in seconds
	QRect DisplayRegion;
	
	///the factor by which the SecondsDisplayed is divided and set as penwidth when lines are drawn
	///calculated with the seconds between first and last timepoint (cf. calcFactors())
	int LineWidthFactor;
	
	///the font size for all texts in the tree
	///calculated with the seconds between first and last timepoint (cf. calcFactors())
	int FontSize;
	
	///the line used for displaying the timepoint
	///is set in displayTimePoint()
	MyQCanvasLine *timePointLine;
	
	///the radius of the circle which represents a cell
	int CellDiameterX;
	int CellDiameterY;			///dependent on the distance in seconds!
	
	///the width of the complete tree in memory
	///for each cell 20 pixels are reserved, yet the minimum width is 500
	long TreeWindowWidthComplete;
	
	///the height of the tree
	///it is calculated from the total seconds of the experiment and the factor TreeWindowHeightFactor, as
	///TreeWindowHeight = Seconds / TreeWindowHeightFactor
	long TreeWindowHeight;
	
	///the logical width for the graphical display
	///normally equal to TreeWindowWidthComplete, but if the speed bar is displayed on the right,
	/// it is reduced by its width
	///all drawing functions just consider this bound -> all tree drawing functions are inside
	int TreeWindowWidth;
	
	///the list of co-selected tracks (*does NOT contain the current track*)
	Q3IntDict<Track> selectedTracks;
	
	///the number of currently selected cells
	///== selectedTracks.size()
	int selectedTrackCount;
	
	///whether the cell colocation lines are displayed for all cells
	bool ShowColocationAll;
	
	///whether the cell colocation lines are displayed only for the active cell
	bool ShowColocationSingle;
	
	///the radius within which a cell next to another one should be considered colocated
	int ColocationRadius;
	
	///whether the cell speed is displayed (both directly in the tree and a side bar)
	bool ShowSpeed;
	
	///the value with which the calculated speed is multiplied
	int SpeedAmplifier;

	///if cell lines should be continuous (not only at tracked timepoints)
	bool drawContinuousCellLines;



//private methods


	///calculates the position in logical coordinates, including zoom
	///@param _pos the real (e.g. mouse) coordinates
	///@param _shift whether the provided point is a shift offset
	///@param _seconds whether the return value should be in seconds (-> without TreeWindowHeightFactor division)
	QPoint mapToLogical (const QPoint &_pos, bool _shift = false, bool _seconds = false) const;
	
	///calculates the real coordinates from the logical ones
	///@param _pos the logical coordinates
	///@param _shift if the _pos coordinate is a relative shift rather than an absolute position
	QPoint mapToReal (const QPoint &_pos, bool _shift = false) const;
	
	///draws the specified _track into _p (with all data, like speed,...)
	///@param _track the track to be drawn (does not affect any child tracks)
	///@param _speedOnly if only the speed display for _track should be displayed (does not draw anything else)
	///@param _colocOnly if only the colocation for _track should be displayed (does not draw anything else)
	void drawTrack (Track *_track, bool _speedOnly = false, bool _colocOnly = false);
	
	///draws _track and all child tracks into _p [recursive]
	///simply calls drawTrack(...) recursively
	///@param _track the first track to be drawn
	///@param _speedOnly if only the speed display for _track should be displayed (does not draw anything else)
	///@param _colocOnly if only the colocation for _track should be displayed (does not draw anything else)
	void drawTrackCascade (Track *_track, bool _speedOnly = false, bool _colocOnly = false);
	
	///returns the x coordinate for a track
	///@param _track the track for which the position is desired
	int getTrackXPosition (Track *_track) const;
	
	///deletes all objects on the canvas that have the usage specified
	///@param _usage the usage (cf. const declarations), whose objects should be removed (and only these are removed)
	void clear (int _usage);
	
	/**
	 * updates the size of the canvas and redraws the tree
	 * @param _max_width the maximum width of the tree
	 * @param _min_width the minimum width of the tree
	 * @param _draw whether the tree should be redrawn afterwards
	 */
	void updateSize (int _max_width, int _min_width, bool _draw = false);
	
};

#endif
