/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "positiondisplay.h"

// Qt
#include <QScrollBar>

// Project includes
//#include "tttbackend/positionmanagervector.h"
#include "tttbackend/tttmanager.h"
#include "tttbackend/picturearray.h"


// Min/max scaling
const float PositionDisplay::MIN_SCALE = 0.01f;		// 1%
const float PositionDisplay::MAX_SCALE = 20.0f;		// 2000%

PositionDisplay::PositionDisplay (QWidget *_parent)
	: QGraphicsView (_parent)
{
	// Init variables
	scrolling = false;
	drawThumbNailFrames = true;
	drawText = true;
	treeCountTreeFragments = false;
	initialized = false;
	thumbnailsTimePoint = 2;
	thumbnailsWaveLength = 0;

	// Enable anti aliasing
	setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);

	// Every PositionDisplay has its own scene
	setScene (new QGraphicsScene (this));

	// Set background color to gray
	scene()->setBackgroundBrush(QBrush(QColor(200, 200, 200)));

	// Make zoom like in google maps
	setTransformationAnchor(AnchorUnderMouse);

	// Align top, left
	setAlignment(Qt::AlignLeft | Qt::AlignTop);
}

void PositionDisplay::initialize(bool _drawThumbNailFrame, bool _exclusiveSelection, bool _updatePosManagers)
{
	// Init (or reset) variables
	exclusiveSelection = _exclusiveSelection;
	indexOfLastSelectedPosition = "";

	// If we already have thumbnails, delete them first
	for(QHash<QString, PositionThumbnail*>::iterator it = thumbnails.begin(); it != thumbnails.end(); ++it) {
		scene()->removeItem(*it);
		delete *it;
	}
	thumbnails.clear();

	// Create position thumbnails
	float currentZValue = 0;
	QHash<int, TTTPositionManager*>& allPositions = TTTManager::getInst().getAllPositionManagers();
	for (auto iter = allPositions.begin(); iter != allPositions.end(); ++iter) {
		// Check if pos is available
		if (! (*iter)->isAvailable())
			continue;

		// Create thumbnail and add it to scene
		PositionThumbnail *pt = new PositionThumbnail((*iter), _drawThumbNailFrame, this, currentZValue, thumbnailsTimePoint, thumbnailsWaveLength);
		scene()->addItem(pt);

		// Save in hashmap
		const QString index = (*iter)->positionInformation.getIndex();
		thumbnails.insert(index, pt);

		// Mark as loaded if pictures have been loaded
		if ((*iter)->getPictures())
			if ((*iter)->getPictures()->getLoadedPictures() > 0)
				pt->markPicturesLoaded (true);

		// Update position manager
		if(_updatePosManagers)
			(*iter)->posThumbnailInTTTPosLayout = pt;

		// Get next z-value
		currentZValue += PositionThumbnail::Z_IMAGE_DELTA;
	}

	// Update display
	updateDisplay();

	// Reset view transformation
	setTransform(QTransform());

	// Default zoom: 10%
	scale(0.1, 0.1);
	scalingFactor = 0.1f;
	emit zoomChanged(scalingFactor);

	initialized = true;
}

PositionThumbnail* PositionDisplay::getThumbnail( const QString& _index ) const
{
	// Check if thumbnail exists or return 0
	if(thumbnails.contains(_index))
		return thumbnails[_index];

	return 0;
}

void PositionDisplay::updateDisplay()
{
	// If this happens, the application has probably not really started yet
	if(!TATInformation::getInst()->wavelengthInformationExistsFor(0))
		return;

	// Get width and height
	const WavelengthInformation& wlInfo = TATInformation::getInst()->getWavelengthInfo (0);
	int widthPixels = wlInfo.getWidth();
	int heightPixels = wlInfo.getHeight();

	// Set default size if necessary (necessary if there is no tatxml)
	if(widthPixels <= 0)
		widthPixels = 1388;
	if(heightPixels <= 0)
		heightPixels = 1040;

	// Update all thumbnails
	for(QHash<QString, PositionThumbnail*>::iterator it = thumbnails.begin(); it != thumbnails.end(); ++it)
		(*it)->updateDisplay(widthPixels, heightPixels);
}

QList<TTTPositionManager*> PositionDisplay::getActivePositions() const
{
	// Find active positions
	QList<TTTPositionManager*> activePositions;
	for(QHash<QString, PositionThumbnail*>::const_iterator it = thumbnails.begin(); it != thumbnails.end(); ++it) {
		if((*it)->isSelected())
			activePositions.append((*it)->getPosManager());
	}

	return activePositions;
}

void PositionDisplay::setDrawThumbNailFrames( bool _on )
{
	if(drawThumbNailFrames != _on) {
		// Change and redraw thumbnails
		drawThumbNailFrames = _on;
		updateDisplay();
	}
}

void PositionDisplay::setTreeCountTreeFragments( bool _on )
{
	if(treeCountTreeFragments != _on) {
		treeCountTreeFragments = _on;		
		updateDisplay();
	}
}

void PositionDisplay::setDrawText( bool _on )
{
	if(drawText != _on) {
		// Change and redraw thumbnails
		drawText = _on;
		updateDisplay();
	}
}

void PositionDisplay::wheelEvent( QWheelEvent *_event )
{
	// Zoom
	scaleView(pow((double)2, _event->delta() / 400.0));
}

void PositionDisplay::scaleView( qreal _scaleFactor )
{
	// Check boundaries
	if(scalingFactor * _scaleFactor < MIN_SCALE) 
		_scaleFactor =  MIN_SCALE / scalingFactor;
	else if(scalingFactor * _scaleFactor > MAX_SCALE)
		_scaleFactor = MAX_SCALE / scalingFactor;

	// Set zoom
	scale(_scaleFactor, _scaleFactor);

	// Emit zoomChanged
	scalingFactor *= _scaleFactor;
	emit zoomChanged(scalingFactor);
}

bool PositionDisplay::selectPosition( const QString& _index )
{
	// Get thumbnail for position
	PositionThumbnail *pos = 0;
	if(thumbnails.contains(_index))
		pos = thumbnails[_index];

	// Position not found
	if(!pos)
		return false;

	// Check if position is already selected, nothing to do
	if(pos->isSelected())
		return true;

	// If exclusive, unmark old position
	if(exclusiveSelection && !indexOfLastSelectedPosition.isEmpty()) {
		if(thumbnails.contains(indexOfLastSelectedPosition))
			thumbnails[indexOfLastSelectedPosition]->setSelected(false);
	}

	// Mark position active
	pos->setSelected(true);

	// _index is now last selected
	indexOfLastSelectedPosition = _index;

	// Update display
	updateDisplay();

	return true;
}


bool PositionDisplay::deSelectPosition( const QString& _index )
{
	// Deselect all position if _index is ""
	if(_index.isEmpty()) {
		for(QHash<QString, PositionThumbnail*>::const_iterator it = thumbnails.begin(); it != thumbnails.end(); ++it) {
			if((*it)->isActive())
				(*it)->setSelected(false);
		}

		// Nothing selected
		indexOfLastSelectedPosition = "";

		return true;
	}

	// Get thumbnail for position
	PositionThumbnail *pos = 0;
	if(thumbnails.contains(_index))
		pos = thumbnails[_index];

	// Position not found
	if(!pos)
		return false;

	// Check if position is already deselected, nothing to do
	if(!pos->isSelected())
		return true;

	// Deselect position
	pos->setSelected(false);

	// Update display
	updateDisplay();

	return true;
}



void PositionDisplay::reportPositionDoubleClicked( const QString& _index )
{
	// Emit signal
	emit positionDoubleClicked(_index);
}

void PositionDisplay::reportPositionRightClicked( const QString& _index )
{
	// Emit signal
	emit positionRightClicked(_index);
}

void PositionDisplay::reportPositionLeftClicked( const QString& _index )
{
	// Emit signal
	emit positionLeftClicked(_index);
}

bool PositionDisplay::markPicturesLoaded( const QString& _index, bool _loaded /*= true*/ )
{
	// Get thumbnail for position
	PositionThumbnail *pos = 0;
	if(thumbnails.contains(_index))
		pos = thumbnails[_index];

	// Position not found
	if(!pos)
		return false;

	// Mark pictures loaded
	pos->markPicturesLoaded(_loaded);

	// Update
	updateDisplay();

	return true;
}

void PositionDisplay::mousePressEvent( QMouseEvent *_event )
{
	// Default implementation
	QGraphicsView::mousePressEvent(_event);

	// Start mouse navigation
	mouseMoveStartX = _event->globalX();
	mouseMoveStartY = _event->globalY();
	scrolling = true;
}

void PositionDisplay::mouseReleaseEvent( QMouseEvent *_event )
{
	// Default implementation
	QGraphicsView::mouseReleaseEvent(_event);

	// Stop mouse navigation
	scrolling = false;
}

void PositionDisplay::mouseMoveEvent( QMouseEvent *_event )
{
	// Default implementation
	QGraphicsView::mouseMoveEvent(_event);

	if(!scrolling)
		return;

	// Get mouse pos
	int x = _event->globalX(),
		y = _event->globalY();

	// Get delta
	int deltaX = mouseMoveStartX - x,
		deltaY = mouseMoveStartY - y;

	// Get new scrollbar positions
	int scrollX = horizontalScrollBar()->value() + deltaX,
		scrollY = verticalScrollBar()->value() + deltaY;

	// Set scrollbar values
	horizontalScrollBar()->setValue(scrollX);
	verticalScrollBar()->setValue(scrollY);

	// Set start positions
	mouseMoveStartX = x;
	mouseMoveStartY = y;
}

void PositionDisplay::clearView()
{
	// Reset variables
	scrolling = false;
	initialized = false;
	indexOfLastSelectedPosition = "";

	// Delete thumbnails
	for(QHash<QString, PositionThumbnail*>::iterator it = thumbnails.begin(); it != thumbnails.end(); ++it) {
		scene()->removeItem(*it);
		delete *it;
	}
	thumbnails.clear();
}

bool PositionDisplay::isPositionSelected( const QString& _index )
{
	PositionThumbnail* tn = thumbnails.value(_index, 0);
	if(tn)
		return tn->isSelected();

	return false;
}

QPointF PositionDisplay::convertGlobalToLocal( const QPointF& point ) const
{
	QPointF ret;

	// Get boundaries of coordinate system (in micrometers) and micrometer per pixel
	const QRect& coordinateBounds = TATInformation::getInst()->getCoordinateBounds();
	float mmpp = TATInformation::getInst()->getWavelengthInfo (0).getMicrometerPerPixel();

	// Convert global to local (where local is the coordinate system defined by the bounding rect around all positions)
	if(coordinateBounds.right() > coordinateBounds.left() && coordinateBounds.bottom() > coordinateBounds.top()) {
		if (TTTManager::getInst().USE_NEW_POSITIONS()) {

			//regard coordinate system bounds and whether the coordinate system is inverted
			if (TTTManager::getInst().coordinateSystemIsInverted()) {

				//subtract coordinates from the bottom right corner (which in fact is the top left)
				ret = QPointF(coordinateBounds.right() - point.x(), coordinateBounds.bottom() - point.y());
			}
			else {
				//subtract top left corner to account for a shifted coordinate origin
				ret = QPointF(point.x() - coordinateBounds.left(), point.y() - coordinateBounds.top());
			}
		}
	}
	else
		ret = QPointF(point.x(), point.y());

	// Transform to pixels
	ret /= mmpp;
	return ret;
}
