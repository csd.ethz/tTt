/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "treewindowmenu.h"

// Project
#include "tttautotrackingtreewindow.h"



TreeWindowMenu::TreeWindowMenu( TTTAutoTrackingTreeWindow* _parent )
	: QMenuBar(_parent)
{
	// Init variables
	treeWindow = _parent;

	// Create menus
	init();
}

void TreeWindowMenu::init()
{
	/**
	 * File menu
	 */
	fileMenu = addMenu("&File");
	fileSubMenuLoadTrees = fileMenu->addMenu("&Open");

	// Open tree
	actFile_OPEN_TREE = fileSubMenuLoadTrees->addAction(QIcon(":/ttt/ttticons/document-open-2.png"), "&Existing Tree(s)");
	actFile_OPEN_TREE->setShortcut (Qt::CTRL + Qt::Key_O);
	connect (actFile_OPEN_TREE, SIGNAL (triggered()), treeWindow, SLOT (openExistingTreeFiles()));

	// New tree
	actFile_NEW_TREE = fileSubMenuLoadTrees->addAction("&New Tree");
	actFile_NEW_TREE->setShortcut (Qt::CTRL + Qt::Key_N);
	connect (actFile_NEW_TREE, SIGNAL (triggered()), treeWindow, SLOT (addNewTree()));

	fileSubMenuLoadPictures = fileMenu->addMenu("Load &Pictures");

	//// Toggle load pictures selection
	//actFile_LOADING_PICTURES_TOGGLE_SELECTION = fileSubMenuLoadPictures->addAction("&Toggle Selection");
	//actFile_LOADING_PICTURES_TOGGLE_SELECTION->setShortcut (Qt::Key_F5);
	//actFile_LOADING_PICTURES_TOGGLE_SELECTION->setCheckable (true);     

	//// Select pictures
	//actFile_LOADING_PICTURES_SELECT = fileSubMenuLoadPictures->addAction("&Select");
	//actFile_LOADING_PICTURES_SELECT->setShortcut (Qt::Key_F6);

	//// Deselect pictures
	//actFile_LOADING_PICTURES_DESELECT = fileSubMenuLoadPictures->addAction("&Deselect");
	//actFile_LOADING_PICTURES_DESELECT->setShortcut (Qt::Key_F7);

	// Load selected pictures
	actFile_LOADING_PICTURES_LOAD = fileSubMenuLoadPictures->addAction("&Load Selected");
	connect (actFile_LOADING_PICTURES_LOAD, SIGNAL (triggered()), treeWindow, SLOT (loadImages()));
	actFile_LOADING_PICTURES_LOAD->setShortcut (Qt::Key_F8);

	// Unload selected pictures
	actFile_LOADING_PICTURES_UNLOAD = fileSubMenuLoadPictures->addAction("&Unload Selected");
	connect (actFile_LOADING_PICTURES_UNLOAD, SIGNAL (triggered()), treeWindow, SLOT (unloadImages()));
	actFile_LOADING_PICTURES_UNLOAD->setShortcut (Qt::Key_F9);

	// Load all pictures of current tree
	fileSubMenuLoadPictures->addSeparator();
	actFile_LOADING_PICTURES_CURTREE = fileSubMenuLoadPictures->addAction("&Load All for Current Tree");

	// Save tree
	actFile_SAVE_CURRENT_TREE = fileMenu->addAction(QIcon(":/ttt/ttticons/document-save-5.png"), "&Save Current Tree");
	actFile_SAVE_CURRENT_TREE->setShortcut (Qt::Key_F10);
	connect (actFile_SAVE_CURRENT_TREE, SIGNAL (triggered()), treeWindow, SLOT (saveCurrentTree()));
	actFile_SAVE_CURRENT_TREE->setEnabled(false);

	// Save tree as
	actFile_SAVE_CURRENT_TREE_AS = fileMenu->addAction("Save Current Tree &as..");
	connect (actFile_SAVE_CURRENT_TREE_AS, SIGNAL (triggered()), treeWindow, SLOT (saveCurrentTreeAs()));
	actFile_SAVE_CURRENT_TREE_AS->setEnabled(false);

	// Close tree
	actFile_CLOSE_CURRENT_TREE = fileMenu->addAction(QIcon(":/ttt/ttticons/Icon_dialog-close-2.png"), "&Close Current Tree");
	connect (actFile_CLOSE_CURRENT_TREE, SIGNAL (triggered()), treeWindow, SLOT (closeCurrentTree()));
	actFile_CLOSE_CURRENT_TREE->setEnabled(false);

	// Close all trees
	actFile_CLOSE_ALL_TREES = fileMenu->addAction("Close All &Trees");
	connect (actFile_CLOSE_ALL_TREES, SIGNAL (triggered()), treeWindow, SLOT (closeAllTrees()));
	actFile_CLOSE_ALL_TREES->setEnabled(false);

	/**
	 * Autotracking menu
	 */
	autotrackingMenu = addMenu("&Autotracking");

	// Open tool to connect fragments
	actAT_CONNECT_FRAGMENTS = autotrackingMenu->addAction("&Connect Tree Fragments");
	connect (actAT_CONNECT_FRAGMENTS, SIGNAL (triggered()), treeWindow, SLOT (connectTreeFragmentsTool()));

	// Assistant (Important: shortcuts are also handled in TTTMovie to make them work from there, too -> change TTTMovie too, if a shortcut is changed here)
	autotrackingSubMenuAssistant = autotrackingMenu->addMenu("&Assistant");
	actAT_ASSISTANT_PREV = autotrackingSubMenuAssistant->addAction(QIcon(":/ttt/ttticons/arrow-left-3.png"), "Go to &Previous");
	actAT_ASSISTANT_PREV->setShortcut(Qt::Key_Minus);
	connect (actAT_ASSISTANT_PREV, SIGNAL (triggered()), treeWindow, SLOT (assistantSelectPrevRegion()));
	actAT_ASSISTANT_NEXT = autotrackingSubMenuAssistant->addAction(QIcon(":/ttt/ttticons/arrow-right-3.png"), "Go to &Next");
	actAT_ASSISTANT_NEXT->setShortcut(Qt::Key_Plus);
	connect (actAT_ASSISTANT_NEXT, SIGNAL (triggered()), treeWindow, SLOT (assistantSelectNextRegion()));
	actAT_ASSISTANT_ACCEPT = autotrackingSubMenuAssistant->addAction(QIcon(":/ttt/ttticons/dialog-ok-apply-2.png"), "&Accept Current");
	connect (actAT_ASSISTANT_ACCEPT, SIGNAL (triggered()), treeWindow, SLOT (assistantAcceptCurRegion()));
	actAT_ASSISTANT_ACCEPT->setShortcut(Qt::Key_Enter);

	/**
	 * Tools menu
	 */
	toolsMenu = addMenu("&Tools");
	actTools_EXPORT_VIEW = toolsMenu->addAction("Export tree view");
	connect (actTools_EXPORT_VIEW, SIGNAL (triggered()), treeWindow, SLOT (exportView()));

	/**
	 * Help menu
	 */
	helpMenu = addMenu("&Help");

	// Actions
	actHelp_HELP = helpMenu->addAction(QIcon(":/ttt/ttticons/help-4.png"), "&Help");
	connect (actHelp_HELP, SIGNAL (triggered()), treeWindow, SLOT (help()));
}
