/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "myqcanvasellipse.h"

#include <iostream>
//Added by qt3to4:
#include <Q3PointArray>

int MyQCanvasEllipse::RTTI = 1006;

MyQCanvasEllipse::MyQCanvasEllipse (int _x, int _y, int _width, int _height, Q3Canvas* _canvas)
: Q3CanvasPolygonalItem (_canvas), mx (_x), my (_y), mwidth (_width), mheight (_height)
{
	setEllipse (_x, _y, _width, _height);
	setZ (1);
}

MyQCanvasEllipse::MyQCanvasEllipse (QRect _rect, Q3Canvas* _canvas)
: Q3CanvasPolygonalItem (_canvas)
{
	mx = _rect.x() + _rect.width() / 2;
	my = _rect.y() + _rect.height() / 2;
	mwidth = _rect.width();
	mheight = _rect.height();
	setEllipse (mx, my, mwidth, mheight);
	setZ (1);
}

void MyQCanvasEllipse::setEllipse (int _x, int _y, int _width, int _height)
{
	//create a pointarray that is an ellipse
	points.makeEllipse (_x - (int)((float)_width/2.0f), _y - (int)((float)_height/2.0f), _width, _height);
}

void MyQCanvasEllipse::drawShape (QPainter& _p)
{
	_p.drawPolygon (points);
}

Q3PointArray MyQCanvasEllipse::areaPoints() const
{
    Q3PointArray r;
    r.makeEllipse (mx, my, mwidth+3, mheight+3);
    r.resize(r.size()+1);
    r.setPoint(r.size()-1,int(x()),int(y()));
    return r;
}
