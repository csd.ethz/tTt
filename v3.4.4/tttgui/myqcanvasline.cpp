/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "myqcanvasline.h"

#include <iostream>
//Added by qt3to4:
#include <Q3PointArray>

int MyQCanvasLine::RTTI = 1005;

MyQCanvasLine::~MyQCanvasLine()
{
	hide();
	if (colors)
		delete colors;
}

void MyQCanvasLine::setPoints (int _x1, int _y1, int _x2, int _y2)
{
	//NOTE: the two end points are at the beginning of the array!
	points.resize (0);
	points.putPoints (0, 2, _x1, _y1, _x2, _y2);
	
	if (usePC) {
		setX (_x1);
		setY (_y1);
	}
	
	x1 = _x1;
	y1 = _y1;
	x2 = _x2;
	y2 = _y2;
}
	
void MyQCanvasLine::setPointColors (QColor _startColor, QColor _endColor)
{
	colorsUsed = true;
	if (! colors) {
		colors = new Q3ValueVector<QColor> (2);
		colors->push_back (_startColor);
		colors->push_back (_endColor);
	}
	else {	
		(*colors)[0] = _startColor;
		(*colors)[1] = _endColor;
	}
}

void MyQCanvasLine::addMiddlePoint (int _x, int _y)
{
	points.putPoints (points.size(), 1, _x, _y);
}

void MyQCanvasLine::addMiddlePoint (int _x, int _y, QColor _color)
{
	//@ todo stamp line in any direction (cf. also drawShape())
	//if ((_y > y2) || (_y < y1))
	//	return;

	// OTODO: This is just a dirty workaround:
	if (_y < y1)
		return;
	if(!stamped && _y > y2)
		return;
	
	if (! colors) {
		colors = new Q3ValueVector<QColor> (2);
		colors->push_back (Qt::black);
		colors->push_back (Qt::black);
	}
	
	colorsUsed = true;
	int index = points.size();
	points.putPoints (index, 1, _x, _y);
	//colors->push_back (_color);
	if (index >= (int)colors->size())
		colors->resize ((int)((float)index * 1.5f));
	
	(*colors) [index] = _color;
}

void MyQCanvasLine::setPoints (Q3PointArray _points)
{
	//deep copy, as points can be changed
	points = _points.copy();
}

void MyQCanvasLine::setRasterOp (QPainter::CompositionMode _rop)
{
	rop = _rop;
}

void MyQCanvasLine::reset (bool _onlyMiddlepoints)
{
	if (_onlyMiddlepoints)
		points.resize (2);
	else
		points.resize (0);
}

void MyQCanvasLine::drawShape (QPainter &_p)
{
	_p.save();
	
	//the end points are the first two pairs, regardless of the mode
	QPoint start = points.point (0);
	QPoint end = points.point (1);
	
        //_p.setRasterOp (rop);
        _p.setCompositionMode (rop);

	if (stamped) {
		//_p.drawLine ((int)start.x() - width_2, (int)start.y(), (int)start.x() + width_2, (int)start.y());
		//_p.drawLine ((int)end.x() - width_2, (int)end.y(), (int)end.x() + width_2, (int)end.y());
		int oldY = -1;
		for (int i = 2; i < points.size(); i++) {
			QPoint p = points.point (i);
			if (colorsUsed) {
				_p.setPen (QPen (colors->at (i), _p.pen().width()));
			}

			_p.setBrush(QBrush(_p.pen().color(), Qt::SolidPattern));

			if(continuousLine && oldY != -1)
				_p.drawRect ((int)p.x() - width_2, oldY, width_2*2, p.y() - oldY);
			else
				_p.drawLine ((int)p.x() - width_2, (int)p.y(), (int)p.x() + width_2, (int)p.y());

			//_p.drawLine ((int)p.x() - width_2, (int)p.y(), (int)p.x() + width_2, (int)p.y());

			oldY = p.y();
		}
	}
	else {	
		//// Set width
		//QPen oldPen = _p.pen();
		//QPen tmp(oldPen);
		//tmp.setWidth(2 * width_2);
		//_p.setPen(tmp);

		if (usePC)
			_p.drawLine ((int)x(), (int)y(), (int)x() + (x2 - x1), (int)y() + (y2 - y1));
		else
			_p.drawLine (start, end);
		
		if (colorsUsed) {
			_p.setPen (QPen (colors->at (0), 3));
			_p.drawPoint (start);
			_p.setPen (QPen (colors->at (1), 3));
			_p.drawPoint (end);
		}

		//// Old
		//_p.setPen(oldPen);
	}
	
	_p.restore();
}

Q3PointArray MyQCanvasLine::areaPoints() const
{
    Q3PointArray p(4);
    int xi = int(x());
    int yi = int(y());
    int pw = pen().width();
    int dx = QABS(x1-x2);
    int dy = QABS(y1-y2);
    pw = pw*4/3+2; // approx pw*sqrt(2)
    int px = x1<x2 ? -pw : pw ;
    int py = y1<y2 ? -pw : pw ;
    if ( dx && dy && (dx > dy ? (dx*2/dy <= 2) : (dy*2/dx <= 2)) ) {
        // steep
        if ( px == py ) {
            p[0] = QPoint(x1+xi   ,y1+yi+py);
            p[1] = QPoint(x2+xi-px,y2+yi   );
            p[2] = QPoint(x2+xi   ,y2+yi-py);
            p[3] = QPoint(x1+xi+px,y1+yi   );
        } else {
            p[0] = QPoint(x1+xi+px,y1+yi   );
            p[1] = QPoint(x2+xi   ,y2+yi-py);
            p[2] = QPoint(x2+xi-px,y2+yi   );
            p[3] = QPoint(x1+xi   ,y1+yi+py);
        }
    } else if ( dx > dy ) {
        // horizontal
        p[0] = QPoint(x1+xi+px,y1+yi+py);
        p[1] = QPoint(x2+xi-px,y2+yi+py);
        p[2] = QPoint(x2+xi-px,y2+yi-py);
        p[3] = QPoint(x1+xi+px,y1+yi-py);
    } else {
        // vertical
        p[0] = QPoint(x1+xi+px,y1+yi+py);
        p[1] = QPoint(x2+xi+px,y2+yi-py);
        p[2] = QPoint(x2+xi-px,y2+yi-py);
        p[3] = QPoint(x1+xi-px,y1+yi+py);
    }
    return p;
}

bool MyQCanvasLine::containsMiddlePointAt( int _x, int _y )
{
	// 0 and 1 contain no middle points
	return points.lastIndexOf(QPoint(_x, _y)) > 1;
}


