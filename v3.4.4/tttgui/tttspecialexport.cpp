/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "tttspecialexport.h"

#include "tttdata/userinfo.h"
#include "tttbackend/tools.h"
#include "tttmovie.h"

#include <QFile>
#include <QMessageBox>


TTTSpecialExport::TTTSpecialExport(TTTMovie *parent) : 
	QDialog(parent)
{
	setupUi(this);

	movieWindow = parent;

	// Slots
	connect(pbtClose, SIGNAL(clicked()), this, SLOT(close()));
	connect(pbtAddItem, SIGNAL(clicked()), this, SLOT(addItem()));
	connect(pbtRemoveItem, SIGNAL(clicked()), this, SLOT(removeItem()));
	connect(pbtRunSpecialExport, SIGNAL(clicked()), this, SLOT(performExport()));

	// Try to load previously saved data
	loadDataFromTxtFile();
}


void TTTSpecialExport::loadDataFromTxtFile()
{
	QString filename = UserInfo::getInst().getUserConfigDirectory() + "special_export.txt";
	
	// Check if file exists
	if(!QFile::exists(filename))
		return;

	// Open file
	QFile file (filename);
	if (! file.open (QIODevice::ReadOnly))
		return;


    QTextStream ts (&file);
	QString line;
	line = ts.readLine();

	//read file format
	QString format = line.stripWhiteSpace();
	if (format == "JPEG")
		optJPEG->setChecked(true);
	else if (format == "BMP")
		optBMP->setChecked(true);
	else if (format == "PNG")
		optPNG->setChecked(true);
	else {
		optBMP->setChecked(true);
	}

	// Parse rest of the file
	while (! ts.atEnd()) {
		line = ts.readLine();

		if (line.isEmpty())
			continue;

		QStringList entries = line.trimmed().split (";");

		int tp1 = entries [0].toInt();
		int tp2 = entries [1].toInt();
		int picture_interval = entries [2].stripWhiteSpace().toInt();

		int repeats = entries [3].stripWhiteSpace().toInt();
		QString wavelengths = entries [4].stripWhiteSpace();

		//// Center pos or follow cell
		//
		//QPointF center;
		//bool followCell = false;
		//int cellToFollow = 0;
		//if (posS.left (1) == "f") {
		//	cellToFollow = posS.mid (1).toInt();
		//	followCell = true;
		//}
		//else {
		QString posS = entries [5].stripWhiteSpace();
		QStringList centerPos = QStringList::split ("/", posS);
		QPoint center = QPoint (centerPos [0].toInt(), centerPos [1].stripWhiteSpace().toInt());
		//}

		// Zoom
		int zoomFactor = entries [6].stripWhiteSpace().toInt();

		// Show tree
		bool treeVisible = false;
		if (entries.count() >= 8)
			treeVisible = entries [7].stripWhiteSpace().toInt() == 1 ? true : false;


		//BS: new on 2009/01/28
		//tracks:first/all;blackpoint/whitepoint;gammaanchors1/2/3;

		//tracksVisible: 0 = normal tracks / 1 = all tracks at tp / 2 = first tracks / 3 = none
		int tracksVisible = 0;
		if (entries.count() >= 9)
			tracksVisible = entries [8].stripWhiteSpace().toInt();

		int inPosLayout = 0;
		if(entries.count() >= 10)
			inPosLayout = entries [9].stripWhiteSpace().toInt();

		QString strTracksVisible;
		if(tracksVisible == 0)
			strTracksVisible = "Normal";
		else if(tracksVisible == 1)
			strTracksVisible = "All";
		else if(tracksVisible == 2)
			strTracksVisible = "First";
		else if(tracksVisible == 3)
			strTracksVisible = "None";
		else
			continue;

		addLineToTable(tp1, tp2, picture_interval, repeats, wavelengths, center.x(), center.y(), zoomFactor, treeVisible, strTracksVisible, inPosLayout);
	}
}

bool TTTSpecialExport::saveTxtFile()
{
	// Get file name
	QString filename = UserInfo::getInst().getUserConfigDirectory() + "special_export.txt";

	// Open file
	QFile file (filename);
	if (! file.open (QIODevice::WriteOnly)) {
		QMessageBox::critical(this, "Error", QString("Cannot open file (%1) for writing.").arg(filename));
		return false;
	}
    QTextStream ts (&file);

	// Format
	if(optJPEG->isChecked())
		ts << "JPEG\n";
	else if(optPNG->isChecked())
		ts << "PNG\n";
	else
		ts << "BMP\n";

	// Iterate over lines
	for(int y = 0; y < tableLines->rowCount(); ++y) {
		// data of current line
		int tp1, tp2, picInterval, repeats, centerX, centerY, zoom, showTracks, showTree, inPosLayout;
		QString wavelengths;

		// Read data

		// Integers
		if(!Tools::stringToInt(getItemFromTable(y, 0), tp1) ||
			!Tools::stringToInt(getItemFromTable(y, 1), tp2) ||
			!Tools::stringToInt(getItemFromTable(y, 2), picInterval) ||
			!Tools::stringToInt(getItemFromTable(y, 3), repeats) ||
			!Tools::stringToInt(getItemFromTable(y, 5), centerX) ||
			!Tools::stringToInt(getItemFromTable(y, 6), centerY) ||
			!Tools::stringToInt(getItemFromTable(y, 7), zoom)) {
			QMessageBox::critical(this, "Error", QString("Invalid data in row %1. Skipping row.").arg(y));
			continue;
		}

		// Wavelengths
		wavelengths = getItemFromTable(y, 4);
		if(!validWaveLengths(wavelengths))
			continue;
	
		// Show tree
		QString strShowTree = getItemFromTable(y, 8).toUpper();
		if(strShowTree.isEmpty() || strShowTree == "FALSE" || strShowTree == "0")
			showTree = 0;
		else
			showTree = 1;
			
		// showTracks
		QString strShowTracks = getItemFromTable(y, 9);
		if(strShowTracks == "Normal")
			showTracks = 0;
		else if(strShowTracks == "First")
			showTracks = 2;
		else if(strShowTracks == "All")
			showTracks = 1;
		else 
			showTracks = 3;	// none

		// Pos layout
		QString strPosLayout = getItemFromTable(y, 10).toUpper();
		if(strPosLayout.isEmpty() || strPosLayout == "FALSE" || strPosLayout == "0")
			inPosLayout = 0;
		else
			inPosLayout = 1;

		// Save data
		ts << tp1 << ";" << tp2 << ";" << picInterval << ";" << repeats << ";" << wavelengths
			<< ";" << centerX << "/" << centerY << ";" << zoom << ";" << showTree << ";" << showTracks << ";" << inPosLayout << "\n";
	}

	return true;
}

bool TTTSpecialExport::validWaveLengths(const QString& wls)
{
	for(int i = 0; i < wls.length(); ++i) {
		if(wls[i] != ',' && !wls[i].isDigit()) {
			QMessageBox::critical(this, "Error", QString("Invalid character in wavelengths: '%1'. Use comma separated list.").arg(wls[i]));
			return false;
		}
	}
	return true;
}

QString TTTSpecialExport::getItemFromTable(int row, int col)
{
	QTableWidgetItem *item = tableLines->item(row, col);
	if(!item)
		return QString();

	return item->text();
}

void TTTSpecialExport::performExport()
{
	lblStatus->setText("Saving data..");

	if(!saveTxtFile())
		return;

	lblStatus->setText("Performing export, please wait..");

	// TTTMovie::runexport
	if(pbtWindowSize800x600->isChecked())
		movieWindow->runSpecialExport(this, 800, 600, chkTimeIntoCsv->isChecked());
	else
		movieWindow->runSpecialExport(this, -1, -1, chkTimeIntoCsv->isChecked());
}

void TTTSpecialExport::addItem()
{
	///data
	int tp1, tp2, picInterval, repeats, centerX, centerY, zoom, showTracks;
	QString wavelengths;

	///get input
	if(!Tools::stringToInt(lieStartTp->text(), tp1) ||
		!Tools::stringToInt(lieEndTp->text(), tp2) ||
		!Tools::stringToInt(lieInterval->text(), picInterval) ||
		!Tools::stringToInt(lieRepeats->text(), repeats) ||
		!Tools::stringToInt(liePosX->text(), centerX) ||
		!Tools::stringToInt(liePosY->text(), centerY) ||
		!Tools::stringToInt(lieZoom->text(), zoom)) {
			QMessageBox::critical(this, "Error", "Invalid data");
			return;
	}

	// Get wavelengths
	wavelengths = lieWavelengths->text();

	if(!validWaveLengths(wavelengths))
		return;

	// Get show tracks setting
	QString strShowTracks = cmbTracks->currentText();

	bool showTree = chkShowTree->isChecked();

	
	// Create new item
	addLineToTable(tp1, tp2, picInterval, repeats, wavelengths, centerX, centerY, zoom, showTree, strShowTracks, chkPosLayout->isChecked());
}


void TTTSpecialExport::addLineToTable(int tp1, 
			int tp2, 
			int picInterval, 
			int repeats, 
			QString wavelengths,
			int centerX, 
			int centerY, 
			int zoom, 
			bool showTree,
			QString showTracks, 
			bool inPosLayout)
{
	int nextRow = tableLines->rowCount();
	
	tableLines->insertRow(nextRow);

	addItemToTable(QString("%1").arg(tp1), nextRow, 0);
	addItemToTable(QString("%1").arg(tp2), nextRow, 1);
	addItemToTable(QString("%1").arg(picInterval), nextRow, 2);
	addItemToTable(QString("%1").arg(repeats), nextRow, 3);
	addItemToTable(QString("%1").arg(wavelengths), nextRow, 4);
	addItemToTable(QString("%1").arg(centerX), nextRow, 5);
	addItemToTable(QString("%1").arg(centerY), nextRow, 6);
	addItemToTable(QString("%1").arg(zoom), nextRow, 7);
	addItemToTable(showTree ? "True" : "False", nextRow, 8);
	addItemToTable(showTracks, nextRow, 9);
	addItemToTable(inPosLayout ? "True" : "False", nextRow, 10);
}


void TTTSpecialExport::addItemToTable(QString itemText, int row, int col)
{
     QTableWidgetItem *newItem = new QTableWidgetItem(itemText);
     tableLines->setItem(row, col, newItem);
}

void TTTSpecialExport::removeItem()
{
	int curRow = tableLines->currentRow();
	if(curRow >= 0)
		tableLines->removeRow(curRow);
}

void TTTSpecialExport::setStatusText(QString newStatus)
{
	lblStatus->setText(newStatus);
}