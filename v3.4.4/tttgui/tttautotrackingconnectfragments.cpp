/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "tttautotrackingconnectfragments.h"

// Project
#include "tttbackend/changecursorobject.h"
#include "tttbackend/tttmanager.h"
#include "tttautotracking.h"
#include "tttautotrackingtreewindow.h"

// QT
#include <QMessageBox>
#include <QProgressDialog>
#include <QMouseEvent>

// QWT
#include <qwt_plot_layout.h>

// STL
#include <deque>



TTTAutotrackingConnectFragments::TTTAutotrackingConnectFragments( TTTAutoTrackingTreeWindow* parent /*= 0*/ )
	: QDialog(parent)
{
	// Setup ui
	ui.setupUi(this);

	// Disable context-help button
	setWindowFlags( (windowFlags() | Qt::CustomizeWindowHint) & (~Qt::WindowContextHelpButtonHint));

	// Init variables
	m_histogram = new QwtPlotHistogram("Movement");
	m_histogram->attach(ui.qwtPlot);
	m_histogram->setBrush(QBrush(Qt::darkBlue));
	//m_plotPicker = new QwtPlotPicker(ui.qwtPlot->canvas());
	//m_plotPicker->setSelectionFlags();
	//m_plotPicker->setRubberBand(QwtPicker::CrossRubberBand);

	// Install event filter for plot
	ui.qwtPlot->canvas()->installEventFilter(this);

	// Signals/slots
	connect(ui.pbtUpdateHistogram, SIGNAL(clicked()), this, SLOT(updateHistogram()));
	connect(ui.pbtStart, SIGNAL(clicked()), this, SLOT(connectTreeFragments()));
	if(parent)
		connect(ui.pbtSaveFragments, SIGNAL(clicked()), parent, SLOT(saveTreeFragments()));
	else
		ui.pbtSaveFragments->setEnabled(false);
	//connect(m_plotPicker, SIGNAL(selected(const QPointF&)), this, SLOT(histogramClicked(const QPointF&)));

}

void TTTAutotrackingConnectFragments::updateHistogram()
{
	// Wait cursor
	ChangeCursorObject cc;

	// Get trees
	TTTAutoTracking* autoTracking = TTTManager::getInst().frmAutoTracking;
	if(!autoTracking)
		return;
	auto trees = autoTracking->getTreeFragments();

	// Create histogram
	QVector<QwtIntervalSample> data = createHistogram(trees);

	// Display histogram
	m_histogram->setSamples(data);
	ui.qwtPlot->replot();
}

QVector<QwtIntervalSample> TTTAutotrackingConnectFragments::createHistogram( const QHash<int, QList<QSharedPointer<TreeFragment>>>& trees )
{	
	/**
	 * Obtain list of all displacements in pixels/timepoint
	 */
	std::deque<float> values;

	// Get micrometer per pixel
	float mmpp = TATInformation::getInst()->getWavelengthInfo (0).getMicrometerPerPixel();

	// Minimum and maximum
	m_minValue = 0xFFFFFF;
	m_maxValue = 0;

	// Iterate over tree tracks
	for(auto it = trees.begin(); it != trees.end(); ++it) {
		for(auto it2 = it->begin(); it2 != it->end(); ++it2) {
			auto tracks = (*it2)->getAllTracks();
			for(auto itTracks = tracks.begin(); itTracks != tracks.end(); ++itTracks) {
				// Finally, get track
				TreeFragmentTrack* curTrack = itTracks->data();

				// Iterate over trackpoints
				TreeFragmentTrackPoint* lastTP = nullptr;
				for(int timepoint = curTrack->getFirstTimePoint(); timepoint < curTrack->getLastTimePoint(); ++timepoint) {
					TreeFragmentTrackPoint* curTP = curTrack->getTrackPointByTimePoint(timepoint);

					// Calculate displacement to last trackpoint
					if(curTP && lastTP) {
						// Calculate movement in micrometers
						float displacement = QLineF(curTP->posX, curTP->posY, lastTP->posX, lastTP->posY).length();

						// Convert if desired
						if(ui.chkConvertToPixels->isChecked())
							displacement /= mmpp;

						// Update statistics
						if(displacement < m_minValue)
							m_minValue = displacement;
						if(displacement > m_maxValue)
							m_maxValue = displacement;

						values.push_back(displacement);
					}

					// Remember last trackpoint
					lastTP = curTP;
				}	
			}
		}
	}
	if(m_maxValue < m_minValue) {
		QMessageBox::critical(this, "Error", "Error: no data found or data invalid.");
		return QVector<QwtIntervalSample>();
	}

	/**
	 * Calculate histogram.
	 */
	int numBins = ui.spbNumBins->value();
	QVector<QwtIntervalSample> histogram;
	float binWidth = (m_maxValue - m_minValue) / numBins;
	
	// Init histogram
	histogram.resize(numBins);
	for(int i = 0; i < numBins; ++i) 
		histogram[i] = QwtIntervalSample(0, m_minValue + i*binWidth, m_minValue + (i+1)*binWidth);

	// Fill histogram
	for(auto it = values.begin(); it != values.end(); ++it) {
		float curval = *it;
		
		// Find bin
		int i = 0;
		while(i < histogram.size()) {
			if(curval + 0.0001f >= histogram[i].interval.minValue() && curval - 0.0001f <= histogram[i].interval.maxValue())
				break;
			++i;
		}
		if(i >= histogram.size()) {
			QMessageBox::critical(this, "Error", "Internal error: histogram intervals invalid.");
			return histogram;
		}

		// Increase bin height
		histogram[i].value += 1.0;
	}

	return histogram;
}

//void TTTAutotrackingConnectFragments::histogramClicked( const QPointF& pos )
//{
//	// Update max migration per timepoint
//	ui.dsbMaxMovement->setValue(pos.x());
//}

bool TTTAutotrackingConnectFragments::eventFilter( QObject *obj, QEvent *event )
{
	// Handle mouse release event if we have valid histogram boundaries
	if(event->type() == QEvent::MouseButtonRelease && m_maxValue > m_minValue) {
		// Get position
		QMouseEvent *keyEvent = reinterpret_cast<QMouseEvent *>(event);
		QPoint pos = keyEvent->pos();
		QwtPlotCanvas* canvas = ui.qwtPlot->canvas();

		// Get distance of actual histogram to widget border and transform pos
		int dist = ui.qwtPlot->plotLayout()->canvasMargin(QwtPlot::yLeft) + canvas->lineWidth();
		pos.setX(pos.x() - dist);
		if(pos.x() < 0)
			return false;

		// Map to histogram interval
		QwtInterval xAxisInterval = ui.qwtPlot->axisInterval(QwtPlot::xBottom);
		float canvasWidth = canvas->width() - dist - 10;		// ToDo: 10 is empirical value, invalid if style of plot is changed..
		float histValue = m_minValue + (pos.x() / canvasWidth) * xAxisInterval.width();
		ui.dsbMaxMovement->setValue(histValue);
	}

	return false;
}

void TTTAutotrackingConnectFragments::connectTreeFragments()
{
	/**
	 * Note: do not forget to update TTTAutoTracking::trackpointsHashmap if you alter 
	 * this code in way such that trackpoints can actually be removed (or added).
	 */
	ChangeCursorObject cc;

	// Get trees
	TTTAutoTracking* autoTracking = TTTManager::getInst().frmAutoTracking;
	if(!autoTracking)
		return;
	auto& trees = autoTracking->getTreeFragments();

	// Count number of trees for progress bar
	int numTrees = 0;
	for(auto it = trees.begin(); it != trees.end(); ++it)
		numTrees += it->size();

	// Create progress bar
	QProgressDialog progessBar("Connecting tree fragments", "Cancel", 0, numTrees, this);
	progessBar.setWindowModality(Qt::ApplicationModal);
	progessBar.setMinimumDuration(0);

	/**
	 * For each tree, check if there is another tree that can be prepended to its root.
	 * If so, do that and clear the corresponding shared pointer in trees.
	 */
	int numErrors = 0;
	int numConnected = 0;
	int counter = 0;
	for(auto it = trees.begin(); it != trees.end() && !progessBar.wasCanceled(); ++it) {
		QList<QSharedPointer<TreeFragment>>& curList = *it;
		for(auto it2 = curList.begin(); it2 != curList.end() && !progessBar.wasCanceled(); ++it2) {
			// Update progress bar
			progessBar.setValue(++counter);

			// Shared pointers of tree fragments that were prepended to others are set to null
			if(it2->isNull())
				continue;
			TreeFragment* curFragment = it2->data();

			// Check if we can prepend any other tree fragment to the current one, until nothing is found
			while(1) {
				int trackNum;
				QSharedPointer<TreeFragment>* prependable = findPrependableTreeFragment(trees, curFragment, trackNum);
				if(!prependable)
					break;

				// Prependable tree fragment has been found
				say(QString("Prepending fragment %1 to root of fragment %2 at time point %3\n").arg((*prependable)->getTreeFragmentNumber()).arg(curFragment->getTreeFragmentNumber()).arg(curFragment->getFirstTimePoint()));

				// Try to prepend it
				if(curFragment->prependFragment(prependable->data(), trackNum, false)) {
					// Set shared pointer to null
					prependable->clear();
					++numConnected;
				}
				else {
					++numErrors;
					break;
				}
			}
		}
	}

	/**
	 * Remove all null shared pointers in trees.
	 */
	auto it = trees.begin();
	while(it != trees.end()) {
		QList<QSharedPointer<TreeFragment>>& curList = *it;

		// Remove null entries from curList
		auto it2 = curList.begin();
		while(it2 != curList.end()) {
			if(it2->isNull())
				it2 = curList.erase(it2);
			else
				++it2;
		}

		// List has become empty, remove it from trees
		if(curList.isEmpty())
			it = trees.erase(it);
		else
			++it;
	}

	// Done
	QString msg = QString("Finished connecting tree fragments.\n\nNumber of errors: %1\nNumber of connected trees: %2").arg(numErrors).arg(numConnected);
	QMessageBox::information(this, "Finished", msg);

	// Update number of fragments display and tracks in movie windows
	autoTracking->updateDisplay();
	TTTManager::getInst().redrawTracks();
}

QSharedPointer<TreeFragment>* TTTAutotrackingConnectFragments::findPrependableTreeFragment( QHash<int, QList<QSharedPointer<TreeFragment>>>& treeFragments, TreeFragment* fragment, int& outFoundTrack )
{
	// Get first timepoint and first trackpoint
	int firstTimePoint = fragment->getFirstTimePoint();
	TreeFragmentTrackPoint* firstTrackPoint = fragment->getRootNode()->getFirstTrackPoint();
	if(!firstTrackPoint)
		return nullptr;

	// Temporal and spatial search range
	int temporalSearchRange = ui.spbTemporalSearchRange->value();
	float spatialSearchRange = ui.dsbMaxMovement->value();

	// Get micrometer per pixel
	float mmpp = TATInformation::getInst()->getWavelengthInfo (0).getMicrometerPerPixel();
	bool convertToPixels = ui.chkConvertToPixels->isChecked();

	// Iterate over all trees and check if it contains a tree fragment that can be prepended to fragment
	for(auto it = treeFragments.begin(); it != treeFragments.end(); ++it) {
		QList<QSharedPointer<TreeFragment>>& curList = *it;
		for(auto it2 = curList.begin(); it2 != curList.end(); ++it2) {
			if(it2->isNull())
				continue;
			TreeFragment* curFragment = it2->data();

			// Iterate over tracks
			auto curTracks = curFragment->getAllTracks();
			for(auto itTrack = curTracks.begin(); itTrack != curTracks.end(); ++itTrack) {
				TreeFragmentTrack* curTrack = itTrack->data();

				// Skip if children
				if(curTrack->getChild1())
					continue;

				// Get last trackpoint
				TreeFragmentTrackPoint* lastTrackPointOfCurTrack = curTrack->getLastTrackPoint();
				if(!lastTrackPointOfCurTrack)
					continue;

				// Skip if outside temporal search range
				int temporalDiff = firstTimePoint - lastTrackPointOfCurTrack->getTimePoint();
				if(temporalDiff <= 0 || temporalDiff > temporalSearchRange)
					continue;

				// Check spatial search range
				float spatialDiff = lastTrackPointOfCurTrack->getDistanceTo(*firstTrackPoint);
				if(convertToPixels)
					spatialDiff /= mmpp;
				if(spatialDiff <= spatialSearchRange * temporalDiff) {
					// Found track
					outFoundTrack = curTrack->getTrackNumber();
					return &(*it2);
				}
			}
		}
	}

	return nullptr;
}

void TTTAutotrackingConnectFragments::say( const QString& what )
{
	ui.pteResults->setPlainText(ui.pteResults->toPlainText() + what);
}
