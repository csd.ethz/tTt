/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "positionthumbnail.h"

// Project
#include "tttbackend/tttmanager.h"
#include "tttbackend/tttpositionmanager.h"
#include "tttdata/userinfo.h"
#include "positiondisplay.h"
#include "tttgui/tttautotracking.h"
#include "tttbackend/tools.h"

// QT
#include <QGraphicsSceneMouseEvent>


// Z-Values: Make text appear in front of frame lines and frame lines in front of image
const float PositionThumbnail::Z_IMAGE_DELTA = 0.001f;
const float PositionThumbnail::Z_FRAME_DELTA_TO_IMAGE = 0.0001f;
const float PositionThumbnail::Z_TEXT_DELTA_TO_IMAGE = 0.0002f;

// Colors
const QColor PositionThumbnail::colorSelected = QColor(255, 0, 0);
const QColor PositionThumbnail::colorNotSelected = QColor(0, 255, 0); 


PositionThumbnail::PositionThumbnail(TTTPositionManager* _posManager, bool _drawFrame, PositionDisplay *_positionDisplay, float _zValue, int _timePoint, int _waveLength)
	: QGraphicsPixmapItem()
{
	// Init variables
	posManager = _posManager;
	positionDisplay = _positionDisplay;
	picturesLoaded = false;
	selected = false;
	midText = 0;
	thumbNailSizeX = 0;
	thumbNailSizeY = 0;
	line1 = 0;
	line2 = 0;
	locked = false;

	// Try to find first picture in position
	QString picfilename = _posManager->getPictureDirectory() + "/" + _posManager->getBasename (true) + QString("%1").arg(_timePoint, 5, 10, QChar('0'));
	int zLen = Tools::getNumOfZDigits();
	if(zLen > 0)
		picfilename += QString("_z%1").arg(1, zLen, 10, QChar('0'));
	int wLen = Tools::getNumOfWavelengthDigits();
	if(wLen <= 0)
		wLen = 2;
	picfilename += QString("_w%1").arg(_waveLength, wLen, 10, QChar('0'));

	// Check possible file endings
	if(QFile::exists(picfilename + ".jpg"))
		picfilename = picfilename + ".jpg";
	else if(QFile::exists(picfilename + ".tif"))
		picfilename = picfilename + ".tif";
	else if(QFile::exists(picfilename + ".png"))
		picfilename = picfilename + ".png";
	else
		picfilename = "";

	//if(QFile::exists(picfilename + "0.jpg"))
	//	picfilename = picfilename + "0.jpg";
	//else if(QFile::exists(picfilename + "00.jpg"))
	//	picfilename = picfilename + "00.jpg";
	//else if(QFile::exists(picfilename + "0.tif"))
	//	picfilename = picfilename + "0.tif";
	//else if(QFile::exists(picfilename + "00.tif"))
	//	picfilename = picfilename + "0.tif";
	//else if(QFile::exists(picfilename + "00.png"))
	//	picfilename = picfilename + "00.png";
	//else if(QFile::exists(picfilename + "0.png"))
	//	picfilename = picfilename + "0.png";
	//else
	//	picfilename = "";

	// Load first image
	QImage tmpImage;
	if(!picfilename.isEmpty())
		tmpImage = QImage(picfilename);
	
	
	if(!tmpImage.isNull()) {
		foundFirstImage = true;
		firstImage = QPixmap::fromImage(tmpImage);
	}
	else {
		foundFirstImage = false;

		// If no first image available create white image (use arbitrary size, will be scaled later)
		firstImage = QPixmap(10, 10);

		// Fill with white color
		firstImage.fill(Qt::white);
	}

	// Set first image
	setPixmap(firstImage);

	// Set z-value
	setZValue(_zValue);

	// Set tooltip to comment
	QString comment = posManager->positionInformation.getComment();
	if(!comment.isEmpty()) {
		setToolTip(comment);
	}
}

void PositionThumbnail::updateDisplay(int _width, int _height)
{
	//// If we have no tatxml, these parameters can be -1, then just use size of first picture
	//if(_width == -1 || _height == -1) {
	//	_width = firstImage.width();
	//	_height = firstImage.height();
	//}

	// Scale firstImage if necessary
	if(firstImage.width() != _width || firstImage.height() != _height) {
		firstImage = firstImage.scaled(_width, _height);
		setPixmap(firstImage);
	}

	// Remember them
	thumbNailSizeX = _width;
	thumbNailSizeY = _height;

	// Get boundaries of coordinate system (in micrometers)
	const QRect& coordinateBounds = TATInformation::getInst()->getCoordinateBounds();

	// Get left and top of this position
	QPointF pos = QPointF(posManager->positionInformation.getLeft(), posManager->positionInformation.getTop());

	// Convert global to local (where local is the coordinate system defined by the bounding rect around all positions)
	pos = positionDisplay->convertGlobalToLocal(pos);

	// Set position
	setPos(pos.x(), pos.y());

	// Delete old frame
	for(int i = 0; i < frame.size(); ++i) {
		scene()->removeItem(frame[i]);
		delete frame[i];
	}
	frame.clear();

	// Create frame
	if(positionDisplay->getDrawThumbNailFrames()) {
		//frame.append(new QGraphicsRectItem(left, top, _width, FRAME_WIDTH));						// Top
		//frame.append(new QGraphicsRectItem(left, top+_height-FRAME_WIDTH, _width, FRAME_WIDTH));	// Bottom
		//frame.append(new QGraphicsRectItem(left, top, FRAME_WIDTH, _height));						// Left
		//frame.append(new QGraphicsRectItem(left+_width-FRAME_WIDTH, top, FRAME_WIDTH, _height));	// Right
		frame.append(new QGraphicsRectItem(0, 0, _width, FRAME_WIDTH, this));						// Top
		frame.append(new QGraphicsRectItem(0, _height-FRAME_WIDTH, _width, FRAME_WIDTH, this));	// Bottom
		frame.append(new QGraphicsRectItem(0, 0, FRAME_WIDTH, _height, this));						// Left
		frame.append(new QGraphicsRectItem(_width-FRAME_WIDTH, 0, FRAME_WIDTH, _height, this));	// Right

		// Add frame to scene, set Z-value and color
		for(int i = 0; i < 4; ++i) {
			frame[i]->setZValue(zValue() + Z_FRAME_DELTA_TO_IMAGE);
			//scene()->addItem(frame[i]);

			// Color
			if(isSelected())
				frame[i]->setBrush(QBrush(colorSelected, Qt::SolidPattern));
			else
				frame[i]->setBrush(QBrush(colorNotSelected, Qt::SolidPattern));

			// Disable drawing of frame outline
			frame[i]->setPen(Qt::NoPen);
		}
	}

	// Delete old text
	if(midText) {
		scene()->removeItem(midText);
		delete midText;
		midText = 0;
	}

	// Create text
	if(positionDisplay->getDrawText()) {
		// Generate text
		QString text = posManager->positionInformation.getIndex();

		// Show tree count if desired
		if(!positionDisplay->getTreeCountTreeFragments()) {
			if(UserInfo::getInst().getStyleSheet().getShowTreeCountInPositionLayout())
				text += QString(" [%1]").arg(posManager->getTreeCount());
		}
		else {
			// Show fragment count
			int fragCount = TTTManager::getInst().frmAutoTracking->getTreeFragmentCount(posManager->positionInformation.getIndex());
			text += QString(" [%1]").arg(fragCount);
		}

		// Show if pictures are loaded
		if(picturesLoaded)
			text += " - P";

		// Create QGraphicsItem
		midText = new QGraphicsTextItem(text, this);

		// Set font
		midText->setFont(QFont("Arial", FONT_POINT_SIZE));

		// Calc position (middle of thumbnail)
		const QRectF sceneRect = midText->sceneBoundingRect();
		int posX = _width / 2 - sceneRect.width() / 2;
		int posY = _height / 2 - sceneRect.height() / 2;;
		midText->setPos(posX, posY);
	
		// Set color
		if(isSelected())
			midText->setDefaultTextColor(colorSelected);
		else
			midText->setDefaultTextColor(colorNotSelected);

		// Set z-value
		midText->setZValue(zValue() + Z_TEXT_DELTA_TO_IMAGE);

		// Add to scene
		//scene()->addItem(midText);
	}

	// Delete old lock lines
	if(line1) {
		delete line1;
		line1 = 0;
	}
	if(line2) {
		delete line2;
		line2 = 0;
	}

	// Indicate if position is locked
	if(locked) {
		// Create lines for cross
		line1 = new QGraphicsLineItem(0, 0, _width, _height, this);
		line2 = new QGraphicsLineItem(0, _height, _width, 0, this);

		// Set z-value
		line1->setZValue(zValue() + Z_FRAME_DELTA_TO_IMAGE);
		line2->setZValue(zValue() + Z_FRAME_DELTA_TO_IMAGE);

		// Set pen
		QPen pen(Qt::red);
		pen.setWidth(FRAME_WIDTH);
		line1->setPen(pen);
		line2->setPen(pen);

		//// Add to scene
		//scene()->addItem(line1);
		//scene()->addItem(line2);
	}

}

void PositionThumbnail::updateDisplay()
{
	// Check image sizes and call actual function
	if(thumbNailSizeX && thumbNailSizeY)
		updateDisplay(thumbNailSizeX, thumbNailSizeY);
}


void PositionThumbnail::markPicturesLoaded( bool _loaded /*= true*/ )
{
	if(_loaded != picturesLoaded) {
		picturesLoaded = _loaded;
		updateDisplay();
	}
}

void PositionThumbnail::lock()
{
	locked = true;
}

void PositionThumbnail::setSelected( bool _active /*= true*/ )
{
	selected = _active;
}

void PositionThumbnail::mouseReleaseEvent( QGraphicsSceneMouseEvent* _event )
{
	if(_event->button() == Qt::LeftButton) {
		// Delegate
		positionDisplay->reportPositionLeftClicked(posManager->positionInformation.getIndex());
	}
	else if(_event->button() == Qt::RightButton) {
		// Delegate
		positionDisplay->reportPositionRightClicked(posManager->positionInformation.getIndex());
	}
}

void PositionThumbnail::mouseDoubleClickEvent( QGraphicsSceneMouseEvent* _event )
{
	if(_event->button() == Qt::LeftButton) {
		// Delegate
		positionDisplay->reportPositionDoubleClicked(posManager->positionInformation.getIndex());
	}
}

void PositionThumbnail::mousePressEvent( QGraphicsSceneMouseEvent *_event )
{
	// Nothing (needs to be reimplemented in order to be able to receive mouse release and mouse double click events!)
}

void PositionThumbnail::setFirstImage( const QPixmap& _image )
{
	// Check image
	if(_image.isNull())
		return;

	// Set image
	firstImage = _image;
	setPixmap(firstImage);
}











//PositionThumbnail::PositionThumbnail (QWidget *_parent, const QString &_index, const QString _picFilename, const QString  &_comment, bool _drawFrame)
// : Q3Frame (_parent), index (_index), picFilename (_picFilename), locked (false), active (false), drawFrame (_drawFrame)
//{
//	//try to read picture file
//	if (! picFilename.isEmpty()) {
//		///@todo try to read picture from position manager (controlable via parameter)
//		
//		picAvailable = imgOriginal.load (picFilename);
//	}
//	else
//		picAvailable = false;
//	
//	setFrameStyle (Q3Frame::Sunken);
//	
//	picturesLoaded = false;
//	
//	if (! _comment.isEmpty())
//		QToolTip::add (this, _comment);
//}
//
//QImage* PositionThumbnail::getOriginalImage()
//{
//	if (! picAvailable)
//		return 0;
//	else
//		return &imgOriginal;
//}
//
//void PositionThumbnail::draw()
//{
//        //bitBlt (this, 0, 0, &pixScaled, 0, 0, pixScaled.width(), pixScaled.height(), QPainter::CompositionMode_Source);
//        QPainter p (this);
//        p.drawPixmap (0, 0, pixScaled, 0, 0, pixScaled.width(), pixScaled.height());
//        p.end();
//}
//
//void PositionThumbnail::setPicture (const QImage *_image)
//{
//	if (picAvailable)
//		return;
//	
//	picAvailable = (_image != 0);
//	
//	if (picAvailable)
//		imgOriginal = *_image;
//}
//
//void PositionThumbnail::lock()
//{
//	locked = true;
//	
//	resizeEvent (0);	//trigger a redraw
//}
//
//void PositionThumbnail::markActive (bool _active)
//{
//	active = _active;
//	
//	resizeEvent (0);	//trigger a redraw
//        //draw();
//        //paintEvent (new QPaintEvent (QRect (0, 0, pixScaled.width(), pixScaled.height())));
//        repaint();
//}
//
//void PositionThumbnail::paintEvent (QPaintEvent *)
//{
//	draw();
//}
//
//void PositionThumbnail::resizeEvent (QResizeEvent *)
//{
//	int width = this->geometry().width();
//	int height = this->geometry().height();
//	
//	if (picAvailable)
//		pixScaled.convertFromImage (imgOriginal.smoothScale (width, height));
//	else {
//		pixScaled.resize (this->geometry().width(), this->geometry().height());
//		pixScaled.fill();
//	}
//	
//	
//	//draw index in picture and a bounding box around
//	//draw index into all four corners, as an overlap of positions might hide some of the corners, and in the middle
//	QPainter p (&pixScaled);
//	p.setFont (QFont ("Arial", 15));
//	
//	if (active)
//		p.setPen (Qt::green);
//	else
//		p.setPen (Qt::red);
//	
//	int textwidth = 30;
//	
//	p.drawText (5, 15, index);					//top left
//	p.drawText (width - textwidth - 5, 15, index);			//top right
//	p.drawText (5, height, index);					//bottom left
//	p.drawText (width - textwidth - 5, height, index);		//bottom right
//	
//	//BS 2010/03/22 number of trees displayed for each position (if the user selected this feature)
//	bool showTreeCount = UserInfo::getInst().getStyleSheet().getShowTreeCountInPositionLayout();
//	bool tc_shown = false;
//	if (showTreeCount) {
//		//get number of trees from position manager
//		TTTPositionManager *tttpm = TTTManager::getInst().getPositionManager (index);
//		if (tttpm) {
//			int treecount = tttpm->getTreeCount();
//			QString index_treecount = index + QString (" [%1]").arg (treecount);
//			p.drawText ((width - textwidth) / 2, height / 2, index_treecount);	//middle
//			tc_shown = true;
//		}
//	}
//	
//	if (! tc_shown)
//		//only position index in the middle
//		p.drawText ((width - textwidth) / 2, height / 2, index);	//middle
//	
//	
//        if (drawFrame) {
//                if (active)
//                        p.setPen (QPen (Qt::green, 2));
//                else
//                        p.setPen (QPen (Qt::red, 2));
//
//                p.drawRect (1, 1, width - 2, height - 2);
//        }
//
//	//K. 2008/10/03 *******
//	//Konsti's first programme extension
//	// draw blue circle in the middle of thumbnail
///*	if (active){
//		int mx = width/2;
//		int my = height/2;
//		int radius = 15;
//		
//		p.setPen (Qt::blue);
//		p.drawEllipse(mx - radius, my - radius, radius*2, radius*2);
//	}*/
//	//************
//	
//	
//	if (locked) {
//		//draw red cross over picture
//		p.setPen (QPen (Qt::red, 5));
//		p.drawLine (0, 0, width, height);
//		p.drawLine (0, height, width, 0);
//	}
//	
//	if (picturesLoaded) {
//		//draw a P in the middle
//		int midx = width / 2;
//		int midy = height / 2;
//		
//		p.setPen (QPen (Qt::red, 1));
//		p.setFont (QFont ("Arial", 15));
//		p.drawText (midx, midy, "P");
//	}
//	
//	p.end();
//	
//}
//
//void PositionThumbnail::mouseReleaseEvent (QMouseEvent *_ev)
//{
//	switch (_ev->button()) {
//		case Qt::LeftButton:
//			emit mouseLeftClick (index, this);
//			break;
//		case Qt::RightButton:
//			emit mouseRightClick (index, this);
//			break;
//		default:
//			;
//	}
//}
//
//void PositionThumbnail::mouseDoubleClickEvent (QMouseEvent *)
//{
//	emit mouseDoubleClick (index, this);
//}
//
//void PositionThumbnail::mouseMoveEvent (QMouseEvent *_ev)
//{
//	emit mouseMove (index, _ev->x(), _ev->y(), this);
//}
//
//void PositionThumbnail::markPicturesLoaded (bool _loaded)
//{
//	picturesLoaded = _loaded;
//	
//	resizeEvent (0);	//trigger a redraw
//        //draw();
//        repaint();
//}
//
//
