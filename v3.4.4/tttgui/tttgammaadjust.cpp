/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "tttgammaadjust.h"

// Project includes
#include "propagatingqframe.h"
#include "tttdata/userinfo.h"
#include "tttbackend/tttmanager.h"
#include "tttpictureinformation.h"
#include "tttbackend/compileflags.h"

// Qt includes
#include <QShowEvent>
#include <QColorDialog>



TTTGammaAdjust::TTTGammaAdjust (QWidget *_parent)
        : QDialog (_parent)
{
        setupUi (this);

		// Disable context-help button and turn on Always on top
		setWindowFlags( (windowFlags() | Qt::CustomizeWindowHint | Qt::Popup /*| Qt::WindowStaysOnTopHint*/) & (~Qt::WindowContextHelpButtonHint));

        connect ( pbtClose, SIGNAL (clicked()), this, SLOT (close()));


        connect ( spbBrightness, SIGNAL (valueChanged (int)), this, SLOT (setBrightness (int)));
        connect ( spbContrast, SIGNAL (valueChanged (int)), this, SLOT (setContrast (int)));
        connect ( spbAlpha, SIGNAL (valueChanged(int)), this, SLOT (setAlpha (int)));
        connect ( pbtAutofit, SIGNAL (clicked()), this, SLOT (autoFit()));
        connect ( pbtReset, SIGNAL (clicked()), this, SLOT (resetDisplay()));
        connect ( pbtTransColor, SIGNAL (clicked()), this, SLOT (chooseTransColor()));
        connect ( pbtCopySettings, SIGNAL (clicked()), this, SLOT (exportDisplaySettings()));
        connect ( pbtPasteSettings, SIGNAL (clicked()), this, SLOT (importDisplaySettings()));
        connect ( pbtPictureInformation, SIGNAL (clicked()), this, SLOT (showPictureInformation()));
		connect ( pbtShowHideControls, SIGNAL (clicked()), this, SLOT (showHideControls()));

        displaySettings = 0;

		windowLayoutLoaded = false;
        gammaShifting = false;
        currentAnchorPoint = -1;
        spbContrast->setValue (100);
        spbBrightness->setValue (100);
		controlsHidden = false;

        pqFrame = new PropagatingQFrame (fraGraphics);

        connect ( pqFrame, SIGNAL (mousePressed (QMouseEvent*)), this, SLOT (handleFrameMousePressed (QMouseEvent *)));
        connect ( pqFrame, SIGNAL (mouseReleased (QMouseEvent*)), this, SLOT (handleFrameMouseReleased (QMouseEvent *)));
        connect ( pqFrame, SIGNAL (mouseMoved (QMouseEvent*)), this, SLOT (handleFrameMouseMoved (QMouseEvent *)));

        //apply compile flags
        if (! COMPFLAG_PICTURE_INFORMATION)
                pbtPictureInformation->hide();


}

TTTGammaAdjust::~TTTGammaAdjust()
{
}

//EVENT:
void TTTGammaAdjust::resizeEvent (QResizeEvent *)
{
        pqFrame->setGeometry (0, 0, fraGraphics->width(), fraGraphics->height());
        if (pqFrame->getImage()) {
                //delete the old image, a new one will be created when drawing the graphics
                delete pqFrame->getImage();
                pqFrame->setImage (0);
        }

        adaptDisplay();
}

//EVENT:
void TTTGammaAdjust::showEvent (QShowEvent *_ev)
{
	if(!_ev->spontaneous() && !windowLayoutLoaded) {
		// Load window layout
		UserInfo::getInst().loadWindowPosition(this, "TTTGammaAdjust");

		windowLayoutLoaded = true;
	}

	_ev->accept();
}

//EVENT:
void TTTGammaAdjust::closeEvent (QCloseEvent *_ev)
{
	UserInfo::getInst().saveWindowPosition(this, "TTTGammaAdjust");

	_ev->accept();
}

void TTTGammaAdjust::setDisplay (TTTPositionManager *_tttpm, int _wavelength)
{
        displaySettings = &_tttpm->getDisplays().at (_wavelength);
        wavelength = _wavelength;

		// Update window title
		QString newTitle = QString("Adjust Gamma - Pos: %1 - WL: %2").arg(_tttpm->positionInformation.getIndex()).arg(_wavelength);
		setCaption(newTitle);

        adaptDisplay();
}

//SLOT:
void TTTGammaAdjust::resetDisplay()
{
        displaySettings->reset();
        adaptDisplay();

        emit graphicsAdjusted (wavelength);
}

void TTTGammaAdjust::autoFit()
{
        //@todo autofit function... (note: does not make sense in BDisplay, as the current picture/clip region is not known there -> rather in Tools?)
//                        /**
//                         * automatically finds the best values for the gamma settings, depending on the current picture and view
//                         */
//                        void autofit();
        adaptDisplay();

        emit graphicsAdjusted (wavelength);
}

//SLOT:
void TTTGammaAdjust::chooseTransColor()
{
	//show popup for selecting a color
	QColor newColor = QColorDialog::getColor (displaySettings->TransColor, 0);

	//if the user cancelled the dialog, the color is invalid
	if (newColor.isValid()) {

		//currently, the selected color has to be transformed to either red, green or blue
		//then this channel is extracted to be displayed
		//-> much easier than a real run of colors

		//test which one of the channels scores most -> desired color channel
		int red = newColor.red();
		int green = newColor.green();
		int blue = newColor.blue();

		int index = -1;
		if ((red > green) & (red > blue))
			index = 0;
		if ((green > red) & (green > blue))
			index = 1;
		if ((blue > red) & (blue > green))
			index = 2;

		displaySettings->TransColor = newColor;
		displaySettings->TransColorIndex = index;

		adaptDisplay();
		emit graphicsAdjusted (wavelength);
	}

}

//SLOT:
void TTTGammaAdjust::exportDisplaySettings()
{
        //export the values
        TTTManager::getInst().setRAMDisplaySettings (*displaySettings);
}

//SLOT:
void TTTGammaAdjust::importDisplaySettings()
{
        //apply the values
        *displaySettings = TTTManager::getInst().getRAMDisplaySettings();

        adaptDisplay();

        emit graphicsAdjusted (wavelength);
}

void TTTGammaAdjust::setBrightness (int _brightness)
{
        if (! displaySettings)
                return;

        displaySettings->Brightness = _brightness / 100.0f;
        adaptDisplay();
        emit graphicsAdjusted (wavelength);
}

void TTTGammaAdjust::setContrast (int _contrast)
{
        if (! displaySettings)
                return;

        displaySettings->Contrast = _contrast;
        adaptDisplay();
        emit graphicsAdjusted (wavelength);
}

void TTTGammaAdjust::setAlpha (int _alpha)
{
        if (! displaySettings)
                return;

        displaySettings->Alpha = _alpha;
        adaptDisplay();
        emit graphicsAdjusted (wavelength);
}

void TTTGammaAdjust::adaptDisplay()
{
        //display all current settings in the BDisplay object
        //do not trigger a graphic update here - this is already done from outside

        if (! displaySettings)
                return;

        //1) draw gamma curve
        //-------------------

        QImage *img = pqFrame->getImage();

        if (! img) {
                img = new QImage (pqFrame->width(), pqFrame->height(), QImage::Format_ARGB32);
                img->fill (Qt::white);
                pqFrame->setImage (img);
        }

        img->fill (Qt::white);

        QPainter p (img);

        float frameWidth = pqFrame->width();
        float frameHeight = pqFrame->height();

        //black/white point lines
        p.setPen (QPen (GRAPHICS_COLOR, 2));

        int x = displaySettings->BlackPoint;
        x *= frameWidth;
        x /= 255;
        p.drawLine (x, 0, x, frameHeight);

        x = displaySettings->WhitePoint;
        x *= frameWidth;
        x /= 255;

        //float bw_width = (float) (x - start) / 255;
        float bw_width = frameWidth / 255.0f;

        p.drawLine (x, 0, x, frameHeight);

        //gamma function
        int tmp = frameHeight;
        int tmp2 = 0;
        int hula = 0;

        for (int i = displaySettings->BlackPoint; i < displaySettings->WhitePoint; i++) {

                hula = displaySettings->GammaValues [i];
                tmp2 = frameHeight - hula * frameHeight / 255;
                p.drawLine ((int) ((float)i * bw_width), tmp, (int) (((float)i + 1.0f) * bw_width), tmp2);

                for (int j = 0; j < GAMMA_ANCHOR_POINTS; j++)
                        if (i == displaySettings->GammaAnchor [j][0])
                                p.drawEllipse ((int) ((float)i * bw_width), tmp2, (int)bw_width * 3, (int)bw_width * 3);

                tmp = tmp2;
        }

        p.end();

        pqFrame->update();

        //2) display gamma values
        //-----------------------

        lblBWGamma->setText (QString ("Black: %1\nWhite: %2\nA1:%3\nA2:%4\nA3:%5")
                                        .arg (displaySettings->BlackPoint)
                                        .arg (displaySettings->WhitePoint)
                                        .arg (displaySettings->GammaAnchor[0][1])
                                        .arg (displaySettings->GammaAnchor[1][1])
                                        .arg (displaySettings->GammaAnchor[2][1])
                                        );

        //3) display other values
        //-----------------------

        spbBrightness->setValue (displaySettings->Brightness * 100);
        spbContrast->setValue (displaySettings->Contrast);
        spbAlpha->setValue (displaySettings->Alpha);
        pbtTransColor->setPaletteBackgroundColor (displaySettings->TransColor);

}

void TTTGammaAdjust::handleFrameMousePressed (QMouseEvent *_ev)
{
        switch (_ev->button()) {
                case Qt::LeftButton: {
//                        leftMouseDown = true;

                        if (_ev->modifiers() & Qt::ShiftModifier) {
                                gammaShifting = true;

                                float frameWidth = pqFrame->width();
                                float frameHeight = pqFrame->height();

                                int x = _ev->pos().x() * 255 / frameWidth;
                                if ( (x > displaySettings->BlackPoint) && (x < displaySettings->WhitePoint) ) {

                                        int y = (frameHeight - _ev->pos().y()) * 255 / frameHeight;

                                        int anchorIndex = -1;

                                        //decide which anchor point to move
                                        for (int i = 0; i < GAMMA_ANCHOR_POINTS; i++) {
                                                int anchorX = displaySettings->GammaAnchor [i][0];
                                                int anchorY = displaySettings->GammaAnchor [i][1];

                                                //is the user clicked point in reach of an anchor point?
                                                if ((x > anchorX - ANCHOR_DETECTION_RANGE && x < anchorX + ANCHOR_DETECTION_RANGE) &&
                                                        (y > anchorY - ANCHOR_DETECTION_RANGE && y < anchorY + ANCHOR_DETECTION_RANGE)) {

                                                        //yes - take it and break it
                                                        anchorIndex = i;
                                                        break;
                                                }
                                        }

                                        if (anchorIndex > -1 && anchorIndex < GAMMA_ANCHOR_POINTS)
                                                currentAnchorPoint = anchorIndex;		//an anchor point was selected
                                }
                        }

                        break;
                }
                default:
                        ;
        }
}

void TTTGammaAdjust::handleFrameMouseReleased (QMouseEvent *_ev)
{
        switch (_ev->button()) {
                case Qt::LeftButton:

                        if (gammaShifting) {
                                gammaShifting = false;
                                currentAnchorPoint = -1;

                        }
                        else {
                                int bp = _ev->pos().x() * 255 / pqFrame->width();
                                if (bp < displaySettings->WhitePoint) {
                                        displaySettings->BlackPoint = bp;
                                        displaySettings->adjustGammaValues();

                                        emit graphicsAdjusted (wavelength);
                                        adaptDisplay();
                                }
                        }

                        break;

                case Qt::RightButton: {

                        int wp = _ev->pos().x() * 255 / pqFrame->width();
                        if (wp > displaySettings->BlackPoint) {

                                displaySettings->WhitePoint = wp;
                                displaySettings->adjustGammaValues();

                                emit graphicsAdjusted (wavelength);
                                adaptDisplay();
                        }

                        break;
                }
                default:
                        ;
        }
}

void TTTGammaAdjust::handleFrameMouseMoved (QMouseEvent *_ev)
{
        if (gammaShifting) {

                //method for gamma shifting:
                //three anchor points exist between black and white point
                //the user can drag them with the mouse (the desired is taken with a nearby(!) click)
                //the gamma function is represents 256 values between black and white point
                // -> it has to be adapted also when the black/white point changes


                if (currentAnchorPoint > -1) {

                        float frameWidth = pqFrame->width();
                        float frameHeight = pqFrame->height();

                        int x = _ev->pos().x() * 255 / frameWidth;
                        if ( (x > displaySettings->BlackPoint) && (x < displaySettings->WhitePoint) ) {

                                int y = (frameHeight - _ev->pos().y()) * 255 / frameHeight;

                                displaySettings->GammaAnchor [currentAnchorPoint][1] = y;
                                displaySettings->adjustGammaValues();

                                adaptDisplay();

                                emit graphicsAdjusted (wavelength);
                        }

                }

        }
}

//SLOT:
void TTTGammaAdjust::showPictureInformation()
{
        ///@todo -> change to qdialog?

        TTTPictureInformation *frmPI = new TTTPictureInformation();

        frmPI->show();

        //delete frmPI;

}

void TTTGammaAdjust::showHideControls()
{
	if(controlsHidden) {
		// Show controls
		//lblBrightness->setVisible(true);
		//spbBrightness->setVisible(true);
		//lblContrast->setVisible(true);
		//spbContrast->setVisible(true);
		//lblAlpha->setVisible(true);
		//spbAlpha->setVisible(true);
		//pbtTransColor->setVisible(true);
		fraUpperControls->setVisible(true);
		lblBWGamma->setVisible(true);
		pbtPictureInformation->setVisible(true);
		pbtCopySettings->setVisible(true);
		pbtPasteSettings->setVisible(true);
		pbtReset->setVisible(true);
		pbtAutofit->setVisible(true);

		controlsHidden = false;
		pbtShowHideControls->setText("Hide Controls");
	}
	else {
		// Hide controls
		//lblBrightness->setVisible(false);
		//spbBrightness->setVisible(false);
		//lblContrast->setVisible(false);
		//spbContrast->setVisible(false);
		//lblAlpha->setVisible(false);
		//spbAlpha->setVisible(false);
		//pbtTransColor->setVisible(false);
		fraUpperControls->setVisible(false);
		lblBWGamma->setVisible(false);
		pbtPictureInformation->setVisible(false);
		pbtCopySettings->setVisible(false);
		pbtPasteSettings->setVisible(false);
		pbtReset->setVisible(false);
		pbtAutofit->setVisible(false);

		controlsHidden = true;
		pbtShowHideControls->setText("Show Controls");
	}
}


