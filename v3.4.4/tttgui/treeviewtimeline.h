/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef treeviewtimeline_h__
#define treeviewtimeline_h__

// Qt
#include <QGraphicsItem>
#include <QList>
#include <QGraphicsTextItem>

// Forward declarations
class TreeView;
class TTTPositionManager;
//class TreeViewSettings;

/**
 * Represents the timeline in an instance of the TreeView class.
 */
class TreeViewTimeLine : public QGraphicsRectItem {

public:

	/**
	 * Constructor
	 * @param _treeView the associated TreeView instance
	 */
	TreeViewTimeLine(TreeView *_treeView);

	/**
	 * Initialization. Calls setUpTimeLine().
	 */
	void initialize();
	
	/**
	 * (Re-)create time labels, line items and so on.
	 */
	void setUpTimeLine();

	/**
	 * Specify if real time should be displayed in time scale. Recreates all labels and updates display.
	 */
	void setShowRealTime(bool showRealTime);

	/**
	 * @return width
	 */
	int getWidth() const;

	/**
	 * Update display, for example after display settings or current position have changed
	 */
	void updateDisplay();

	/**
	 * Set current timepoint, drawing horizontal currentTimePointLine for provided _tp. Automatically calls updateDisplay().
	 * @param _tp the timepoint
	 */
	void setCurrentTimePoint(int _tp);

	/**
	 * Set current position, drawing indicators for loaded pictures.
	 * @param positionIndex index of position. Can be -1 to indicate no position.
	 */
	void setCurrentPosition(int positionIndex);

	/**
	 * Paint event. Overwritten from QGraphicsRectItem.
	 */
	void paint(QPainter* _painter, const QStyleOptionGraphicsItem* _option, QWidget* _widget = 0);

	/**
	 * Set highlighted time points range.
	 */
	void setHighlighting(int startTp = -1, int stopTp = -1);

	/**
	 * @Reimplemented
	 */
	void setVisible(bool visible);

	/**
	 * Show/hide vertical bar indicating the current time point.
	 */
	void setCurrentTimePointBarVisible(bool visible);

	// Width of timeline in scene units
	static const int TIME_LINE_DEFAULT_WIDTH = 300;

	// Height
	static const int TIME_LINE_HEIGHT = 50000;

	// Width of lines
	static const int TIME_LINES_LENGTH = 500000;

	// Top margin
	static const int TOP_MARGIN = 50;

protected:

	// Mouse events
	void mousePressEvent( QGraphicsSceneMouseEvent* event );
	void mouseReleaseEvent( QGraphicsSceneMouseEvent* event );

private:

	/**
	 * Update width. Changes width and notifies TreeView
	 * @param _width new width in scene units
	 */
	void updateWidth(int _width);

	///**
	// * Get label for time scale item.
	// */
	//QString getTimeScaleItemLabel(int tp, TTTPositionManager* tttpm);

	/**
	 * Add time label and line.
	 */
	void addTimeLabelAndLine(int timePoint, QString labelText, int& neededWidth);

	// The TreeView instance this timeline belongs to
	TreeView *m_treeView;

	// Labels
	QList<QGraphicsTextItem*> m_timeLabels;

	// Lines
	QList<QGraphicsRectItem*> m_timeLines;

	// Line for current timepoint
	QGraphicsRectItem* m_currentTimePointLine;

	//// Display settings
	//TreeViewSettings *displaySettings;

	// Initialized
	bool m_initialized;

	// Index of current position, can be -1 if not set
	int m_currentPositionIndex;

	// Start and stop time point for highlighted area
	int m_highlightedStart;
	int m_highlightedStop;

	// If real time should be displayed in scale
	bool m_showRealTime;

	// Timepoint labels distance in scene units (i.e. pixels) when zoom=100%
	static const int LABELS_DELTA = 250;
};


#endif // treeviewtimeline_h__
