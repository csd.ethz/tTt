/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TTTMAINWINDOW_H
#define TTTMAINWINDOW_H

#include "ui_TTTMainWindow.h"

// Qt includes
#include <QDir>


/**
@author Bernhard

	The class for the first GUI for selecting the necessary pictures
	(MainWindow is a pseudonym...)
*/
class TTTMainWindow: public QWidget, public Ui::frmTTTMainWindow {

	Q_OBJECT

public:
	
	TTTMainWindow(QWidget *parent = 0, const char *name = 0);
	
	~TTTMainWindow();
	
    ///loads the NAS directory, either from the tTt configuration file or the default NAS, if none exists
    ///is called in main(), as calling it from THIS' constructor is too early
    void loadNASDrive();

    ///loads the last selected directory and drive (before last program stop)
	bool loadLastDirectory();
	
	///shows all available drives and directories
	void displayDirectories (bool _loadDrives = false);
	
	void showEvent (QShowEvent *_ev);

	void closeEvent (QCloseEvent *_ev);

public slots:

	///loads the experiment from the currently selected folder, unless overwritePath is set
	void loadExperiment(const QString& overwritePath = "");

private slots:
	
	///sets the drive
	void selectDrive (const QString&);
	
	///sets the user selected directory and displays the relevant files in it
	void handleDirectory (QListWidgetItem*);
	
	///lets the user select a folder for the current nas drive, where a search for ttt files is performed
	void selectNASDrive();

	///nas drive was changed
	void lieNasDriveTextEdited(const QString& newFolder);

	/////loads positions from all experiment folders in the currently selected folder
	//void loadStatistics();

	///show help
	void showHelp();
	
private:

	///release version: check if nas folder is obviously incorrect, i.e. if top or contained folder is "TTTWorkFolder"
	QString checkNasFolder(const QString& nasFolder);
	bool checkNasInLoadExperiment;

	///release version: check if top-folder of nas folder contains provided example experiment and if so, select it automatically
	void checkForExampleExperimentInNasTopFolder();

	///check nas

	///the current directory as QDir
	QDir currentQDir;

	///window layout already loaded
	bool windowLayoutLoaded;

    /**
    * display the current NAS directory; if it does not exist, paint it red
    */
    void displayNASDirectory();
	
};

#endif
