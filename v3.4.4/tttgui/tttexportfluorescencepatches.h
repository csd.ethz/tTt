/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef tttexportfluorescencepatches_h__
#define tttexportfluorescencepatches_h__


#include "ui_frmExportFluorescencePatches.h"


class TTTExportFluorescencePatches : public QWidget {

	Q_OBJECT

public:

	/**
	 * Constructor.
	 */
	TTTExportFluorescencePatches(QWidget* parent = 0);

private slots:

	/**
	 * Position selection.
	 */
	void positionLeftClicked(const QString& index);
	void positionRightClicked(const QString& index);
	void selectAllPositions();
	void clearPositionSelection();

	/**
	 * Set export directory.
	 */
	void setDirectory();

	/**
	 * Run export.
	 */
	void runExport();

	/**
	 * Select single tree file.
	 */
	void selectSingleTreeFile();

private:

	// Update selected positions textbox
	void updateSelectedPositionsText();

	// GUI
	Ui::TTTExportFluorescencePatches m_gui;

};


#endif // tttexportfluorescencepatches_h__
