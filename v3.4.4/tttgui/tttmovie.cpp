/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "tttmovie.h"

// Project includes
#include "tttbackend/tttmanager.h"
#include "tttsymbolselection.h"
#include "moviemenubar.h"
#include "tttmovieexport.h"
#include "tttspecialexport.h"
#include "tttcelldivisionoverviewexport.h"
#include "tttregionselection.h"
#include "tttgammaadjust.h"
#include "ttttracking.h"
#include "tttpositionlayout.h"
#include "tttcellproperties.h"
#include "tttblank.h"
#include "ttttextbox.h"
#include "ttttracksoverview.h"
#include "tttcellcomment.h"
#include "tttio/logging.h"
#include "ttttreestyleeditor.h"
#include "pictureview.h"
#include "tttbackend/picturearray.h"
#include "tttgui/tttgammaadjust.h"
#include "tttplayerassistant.h"
#include "tttspecialeffects.h"
#include "tttautotracking.h"
#include "tttautotrackingtreewindow.h"

// QT includes
#include <QProcess>
#include <QStatusBar>


TTTMovie::TTTMovie (TTTPositionManager *_positionManager, QWidget *parent, const char *name)
	: QWidget(parent, name)
{
	setupUi (this);

	// Add minimize/maximize buttons
	setWindowFlags( windowFlags() | Qt::CustomizeWindowHint | Qt::WindowMinMaxButtonsHint);

	// Init variables
	positionManager = _positionManager;
	specialEffectsWindow = 0;
	bgrWaveLengths = new QButtonGroup(this);
	bgrOverlayWaveLengths = new QButtonGroup(this);
	showMousePos = false;
	DirectionForward = true;
	MoviePlaying = false;
	MovieLoop = false;
	MovieReverse = false;
	FirstExportTimePoint = -1;
	LastExportTimePoint = -1;
	adjustDisplayBox = true;
	DirectionLabelMousePressed = false;
	ButtonColor = pbtWaveLengthMulti->paletteBackgroundColor();
	Process_TimePoint_Set = true;
	Track_OldFirstTrace = -1;
	Track_OldLastTrace = -1;
	call_slidePicture = true;
	commentTimepoint = 0;
	call_setZoomOnspZoomFactor = true;
	windowLayoutLoaded = false;
	showExternalTracksMode = OFF;
	controlsHidden = false;
	currentIndex = -1;
	lastIndex = -1;

	// Create gamma window
	frmGammaAdjust = new TTTGammaAdjust(this);
	positionManager->frmGammaAdjust = frmGammaAdjust;

	// Create player
	frmPlayer = new TTTPlayerAssistant(this);


	setCaption (QString("%1 Movie Window").arg(positionManager->positionInformation.getIndex()));
	//setFocusPolicy (Qt::StrongFocus);

	MovieTimer = new QTimer (this);

	//create missing controls
	spbTrackSize = new QSpinBox (fraSpbTrackSize);
	spbTrackSize->setGeometry (0, 0, 60, 20);
	spbTrackSize->setMinValue (1);
	spbTrackSize->setMaxValue (500);
	spbTrackSize->setSuffix (" px");                //always pixel!
	spbTrackSize->setValue ((int)CELLCIRCLESIZE);
	connect ( spbTrackSize, SIGNAL (valueChanged (int)), this, SLOT (setTrackSize (int)));	

	//spbTimerInterval = new QSpinBox (fraSpbTimerInterval);
	//spbTimerInterval->setGeometry (0, 0, 70, 20);
	//spbTimerInterval->setMaxValue (10000);
	//spbTimerInterval->setMinValue (0);
	//spbTimerInterval->setValue (100);
	//spbTimerInterval->setLineStep (10);
	//spbTimerInterval->setSuffix (" ms");

	//spbZoomFactor = new QSpinBox (fraSpbZoomFactor);
	//spbZoomFactor->setGeometry (0, 0, 70, 20);
	//spbZoomFactor->setMaxValue (1000);
	//spbZoomFactor->setMinValue (25);
	//spbZoomFactor->setValue (100);
	//spbZoomFactor->setLineStep (25);
	//spbZoomFactor->setSuffix (" %");
	//spbZoomFactor->setEnabled (true);

	spbPathTPStart = new QSpinBox (fraSpbPathTPStart);
	spbPathTPStart->setGeometry (0, 0, 70, 20);
	spbPathTPStart->setMaxValue (10000);
	spbPathTPStart->setMinValue (1);
	spbPathTPStart->setValue (1);
	spbPathTPStart->setLineStep (1);
	spbPathTPStart->setEnabled (true);

	spbPathTPEnd = new QSpinBox (fraSpbPathTPEnd);
	spbPathTPEnd->setGeometry (0, 0, 70, 20);
	spbPathTPEnd->setMaxValue (10000);
	spbPathTPEnd->setMinValue (1);
	spbPathTPEnd->setValue (1);
	spbPathTPEnd->setLineStep (1);
	spbPathTPEnd->setEnabled (true);

	//spbTrackmarkDelay = new QSpinBox (fraSpbTrackmarkDelay);
	//spbTrackmarkDelay->setGeometry (0, 0, 70, 20);
	//spbTrackmarkDelay->setMaxValue (3000);
	//spbTrackmarkDelay->setMinValue (0);
	//spbTrackmarkDelay->setValue (0);
	//spbTrackmarkDelay->setLineStep (25);
	//spbTrackmarkDelay->setEnabled (true);
	//spbTrackmarkDelay->setSuffix (" ms");

	//create the picture displays
	//one additional frame is necessary for the overlay pictures	
	multiPicViewer = new MultiplePictureViewer (fraPictures, MAX_WAVE_LENGTH + 1, _positionManager);

	connect ( multiPicViewer, SIGNAL (pictureMousePositionSet (QPointF)), this, SLOT (displayMousePosition (QPointF)));
	connect ( multiPicViewer, SIGNAL (PictureProceedDemand (bool, int, bool)), this, SLOT (ProceedPicture (bool, int, bool)));
	connect ( multiPicViewer, SIGNAL (trackSelected (Track *, bool, bool)), this, SLOT (selectTrack (Track *, bool, bool)));
	connect ( multiPicViewer, SIGNAL (trackingInterrupted (Track *)), this, SLOT (stopTracking()));
	connect ( multiPicViewer, SIGNAL (wavelengthSelected(int)), this, SLOT (setCurrentWaveLength (int)));
	connect ( multiPicViewer, SIGNAL (trackPointSet (Track *, int)), this, SLOT (wrapTrackPointSet (Track *, int)));
	connect ( multiPicViewer, SIGNAL (cellSizeChanged (int)), this, SLOT (changeCellSize (int)));
	connect ( multiPicViewer, SIGNAL (radiusSelected(int)), this, SLOT (radiusSelected (int)));
	connect ( multiPicViewer, SIGNAL (unhandledKeyReleased (int, bool, bool)), this, SLOT (processPictureKey (int, bool, bool)));

	// Create menu
	menuBar = new MovieMenuBar(this);
	menuBar->setGeometry(QRect(0, 0, 5000, 21));
	multiPicViewer->setMovieMenuBar(menuBar);

	// Init spbSkipFrames
	spbSkipFrames->setValue(TTTManager::getInst().getFramesToSkipWhenTracking());
	connect(spbSkipFrames, SIGNAL(valueChanged(int)), &TTTManager::getInst(), SLOT(setFramesToSkipWhenTracking(int)));

	// Create statusbar
	statusBar = new QStatusBar(this);
	statusBar->setSizeGripEnabled(false);

	// Create statusbar controls
	spbZIndex = new QSpinBox(statusBar);
	spbZIndex->setMinimum(0);
	spbZIndex->setMaximum(999);
	spbZIndex->setValue(1);
	connect(spbZIndex, SIGNAL(valueChanged(int)), this, SLOT(setZIndex(int)));
	lblZIndex = new QLabel("Z-Index:", statusBar);
	QFont fontTmp;
	fontTmp.setBold(true);
	lblTracking = new QLabel("", statusBar);
	lblTracking->setFont(fontTmp);
	pbtZoom50 = new QPushButton("50%", statusBar);
	pbtZoom50->setMaximumWidth(50);
	pbtZoom100 = new QPushButton("100%", statusBar);
	pbtZoom100->setMaximumWidth(50);
	spbZoomFactor = new QSpinBox (statusBar);
	spbZoomFactor->setGeometry (0, 0, 70, 20);
	spbZoomFactor->setRange(25, 1000);
	spbZoomFactor->setValue (100);
	spbZoomFactor->setLineStep (25);
	spbZoomFactor->setSuffix (" %");
	pbtFullPicture = new QPushButton("Full Picture", statusBar);

	// Add status bar controls to statusbar
	statusBar->addPermanentWidget(lblZIndex);
	statusBar->addPermanentWidget(spbZIndex);
	statusBar->addPermanentWidget(lblTracking);
	statusBar->addPermanentWidget(pbtZoom50);
	statusBar->addPermanentWidget(pbtZoom100);
	statusBar->addPermanentWidget(spbZoomFactor);
	statusBar->addPermanentWidget(pbtFullPicture);

	// Connect statusbar controls
	connect ( pbtZoom50, SIGNAL (clicked()), this, SLOT (setZoom()));
	connect ( pbtZoom100, SIGNAL (clicked()), this, SLOT (setZoom()));
	connect ( spbZoomFactor, SIGNAL (valueChanged (int)), this, SLOT (setZoom()));
	connect ( pbtFullPicture, SIGNAL (clicked()), this, SLOT (setZoom()));

	// Finally, add status bar to window
	statusBar->setMaximumHeight(25);
	if(statusBar->layout()) {
		statusBar->layout()->setContentsMargins(8, 0, 8, 0);
		statusBar->layout()->setSpacing(3);
	}
	laoMainLayout->addWidget(statusBar);


	lblCellSizeInfo->setText (QString ("D: %1 \nA: %2 \nV: %3")
		.arg (CELLCIRCLESIZE * 2)
		.arg (CELLCIRCLESIZE * CELLCIRCLESIZE * Pi, 0, 'f', 0)
		.arg (CELLCIRCLESIZE * CELLCIRCLESIZE * CELLCIRCLESIZE * 4 / 3 * Pi, 0, 'f', 0));

	//lblMovieStatus->setText ("==>>");

	sldMovie->setMinValue (0);
	sldMovie->setMaxValue (0);

	pbtCellDivision->setEnabled (false);
	pbtCellApoptosis->setEnabled (false);
	pbtCellLost->setEnabled (false);
	pbtCellComment->setEnabled (false);
	pbtInterruptTracking->setEnabled (false);
	pbtEditBackground->setEnabled (false);
	pbtStartTracking->setEnabled (false);
	pbtEndoMitosis->setEnabled (false);
	pbtPropertySemiAdherent->setEnabled(false);
	pbtPropertyAdherent->setEnabled(false);
	//pbtPropertyNone->setEnabled(false);
	pbtPropertyFreeFloating->setEnabled(false);

	pbtWL1->setEnabled(false);
	pbtWL2->setEnabled(false);
	pbtWL3->setEnabled(false);
	pbtWL4->setEnabled(false);
	pbtWL5->setEnabled(false);
	pbtWL6->setEnabled(false);
	pbtWL7->setEnabled(false);
	pbtWL8->setEnabled(false);
	pbtWL9->setEnabled(false);
	pbtWL10->setEnabled(false);

	chkShowOrientationBox->setChecked (true);

	//connect commands ordered by the visual layout of their signal senders

	//global
	connect ( chkShowTracks, SIGNAL (clicked (bool)), this, SLOT (setShowTracks (bool)));
	connect ( chkShowTrackNumbers, SIGNAL (clicked (bool)), this, SLOT (setShowTrackNumbers (bool)));
	connect ( chkShowWavelengthBox, SIGNAL (toggled (bool)), this, SLOT (setShowWavelengthBox (bool)));
	connect ( pbtWaveLengthMulti, SIGNAL (toggled (bool)), this, SLOT (setMultipleMode (bool)));
	connect ( pbtOverlay, SIGNAL (toggled (bool)), this, SLOT (setOverlay (bool)));
	//connect ( pbtCenterCurrentTrack, SIGNAL (clicked()), multiPicViewer, SLOT (centerCurrentTrack()));
	connect ( chkShowOrientationBox, SIGNAL (toggled (bool)), multiPicViewer, SLOT (setOrientationBoxVisible (bool)));
	connect ( pbtGamma, SIGNAL (clicked()), this, SLOT (adjustGraphics ()));
	connect ( spbTrackSize, SIGNAL (valueChanged(int)), multiPicViewer, SLOT (setCircleMouseRadius (int)));
	connect ( chkHideLoadPicsBars, SIGNAL(toggled(bool)), this, SLOT(chkHideLoadPicsBarsToggled(bool)));


	//BS 2010/03/12 wavelength buttons are now created dynamically (due to possibly unlimited wavelength count)
	//connect ( bgrWaveLengths, SIGNAL (clicked (int)), this, SLOT (setWaveLength (int)));


	//movie player 
	//connect ( pbtPlay, SIGNAL (toggled(bool)), this, SLOT (play(bool)));
	connect ( MovieTimer, SIGNAL (timeout()), this, SLOT (ProceedPicture()));
	//connect ( chkPlayerReverse, SIGNAL (toggled(bool)), this, SLOT (setPlayerReverse(bool)));
	//connect ( chkPlayerLoop, SIGNAL (toggled(bool)), this, SLOT (setPlayerLoop(bool)));
	//connect ( sldMovie, SIGNAL (sliderMoved (int)), this, SLOT (slidePicture (int)));
	connect ( sldMovie, SIGNAL (valueChanged (int)), this, SLOT (slidePicture (int)));
	//connect ( spbTimerInterval, SIGNAL (valueChanged(int)), this, SLOT (setTimerInterval(int)));


	//display tab
	connect ( pbtShowBackground, SIGNAL (clicked (bool)), multiPicViewer, SLOT (setCellBackgroundDisplay (bool)));
	connect ( pbtAllCellsWhite, SIGNAL (toggled (bool)), multiPicViewer, SLOT (setAllCellsUniform (bool)));
	connect ( spbPathTPStart, SIGNAL (valueChanged (int)), multiPicViewer, SLOT (setCellPathStartTP (int)));
	connect ( spbPathTPEnd, SIGNAL (valueChanged (int)), multiPicViewer, SLOT (setCellPathEndTP (int)));
	//connect ( pbtAddCellPath, SIGNAL (clicked()), multiPicViewer, SLOT (addCellPath()));
	//connect ( pbtClearCellPaths, SIGNAL (toggled (bool)), multiPicViewer, SLOT (clearCellPaths()));
	connect ( pbtAddFluorescenceLine, SIGNAL (clicked()), multiPicViewer, SLOT (addFluorescenceLine()));
	//connect ( chkSmoothScale, SIGNAL (toggled (bool)), this, SLOT (toggleSmoothScale (bool)));
	//connect ( pbtShowBackground, SIGNAL (toggled (bool)), multiPicViewer, SLOT (setShowBackground (bool)));
	//connect ( pbtUseCellSize, SIGNAL (toggled (bool)), PictureFrames.find (i), SLOT (setUseCellSize (bool)));
	connect ( pbtEditBackground, SIGNAL (toggled (bool)), multiPicViewer, SLOT (setBackgroundTracking (bool)));

	//connect ( pbtCustomLayout, SIGNAL (clicked()), this, SLOT (customLayout()));
	//connect ( pbtHideControls, SIGNAL (toggled (bool)), this, SLOT (hideControls (bool)));
	connect ( pbtAddChildrenDistanceLine, SIGNAL (clicked()), this, SLOT (addChildrenDistanceLine()));
	connect ( pbtSetDisplayCircleSize, SIGNAL (toggled(bool)), this, SLOT (toggleOverwriteTrackRadiusForDisplay (bool)));
	connect ( spbDisplayCircleSize, SIGNAL (valueChanged(int)), this, SLOT ( setOverwriteTrackRadiusForDisplay(int)));


	//tracking tab
	connect ( pbtCellDivision, SIGNAL (clicked()), this, SLOT (stopTracking()));
	connect ( pbtCellApoptosis, SIGNAL (clicked()), this, SLOT (stopTracking()));
	connect ( pbtCellLost, SIGNAL (clicked()), this, SLOT (stopTracking()));
	connect ( pbtInterruptTracking, SIGNAL (clicked()), this, SLOT (stopTracking()));
	connect ( pbtCellComment, SIGNAL (clicked()), this, SLOT (editCellComment()));
	connect ( pbtEditBackground, SIGNAL (toggled (bool)), this, SLOT (setEditCellBackground (bool)));
	connect ( pbtEndoMitosis, SIGNAL (clicked (bool)), this, SLOT (setEndoMitosis (bool)));
	connect ( pbtShowProperties, SIGNAL (clicked()), this, SLOT (showPropertyForm()));
	connect ( pbtShowTracksOverview, SIGNAL (clicked()), this, SLOT (showTracksOverview()));
	//connect ( pbtCircleTrack, SIGNAL (toggled (bool)), multiPicViewer, SLOT (setCircleMouse (bool)));

	connect ( pbtWL1, SIGNAL(clicked(bool)), this, SLOT(setTrackingCellPropertiesWavelength(bool)) );
	connect ( pbtWL2, SIGNAL(clicked(bool)), this, SLOT(setTrackingCellPropertiesWavelength(bool)) );
	connect ( pbtWL3, SIGNAL(clicked(bool)), this, SLOT(setTrackingCellPropertiesWavelength(bool)) );
	connect ( pbtWL4, SIGNAL(clicked(bool)), this, SLOT(setTrackingCellPropertiesWavelength(bool)) );
	connect ( pbtWL5, SIGNAL(clicked(bool)), this, SLOT(setTrackingCellPropertiesWavelength(bool)) );
	connect ( pbtWL6, SIGNAL(clicked(bool)), this, SLOT(setTrackingCellPropertiesWavelength(bool)) );
	connect ( pbtWL7, SIGNAL(clicked(bool)), this, SLOT(setTrackingCellPropertiesWavelength(bool)) );
	connect ( pbtWL8, SIGNAL(clicked(bool)), this, SLOT(setTrackingCellPropertiesWavelength(bool)) );
	connect ( pbtWL9, SIGNAL(clicked(bool)), this, SLOT(setTrackingCellPropertiesWavelength(bool)) );
	connect ( pbtWL10, SIGNAL(clicked(bool)), this, SLOT(setTrackingCellPropertiesWavelength(bool)) );

	connect ( pbtPropertyAdherent, SIGNAL(clicked(bool)), this, SLOT(setTrackingCellPropertiesAdherence(bool)) );
	connect ( pbtPropertySemiAdherent, SIGNAL(clicked(bool)), this, SLOT(setTrackingCellPropertiesAdherence(bool)) );
	connect ( pbtPropertyFreeFloating, SIGNAL(clicked(bool)), this, SLOT(setTrackingCellPropertiesAdherence(bool)) );
	//connect ( pbtPropertyNone, SIGNAL(clicked(bool)), this, SLOT(setTrackingCellPropertiesAdherence(bool)) );



	connect ( positionManager, SIGNAL (timepointSet (int, int)), this, SLOT (setToTimepoint (int, int)));

	connect ( TTTManager::getInst().frmSymbolSelection, SIGNAL (symbolPositionDesired (Symbol)), multiPicViewer, SLOT (startSymbolPositionSelection(Symbol)));
	connect ( TTTManager::getInst().frmSymbolSelection, SIGNAL (symbolSet (Symbol)), this, SLOT (addSymbol (Symbol)));
	connect ( TTTManager::getInst().frmSymbolSelection, SIGNAL (symbolDelete()), this, SLOT (deleteSymbols()));



	if (! COMPFLAG_CUSTOM_ZOOM_MOVIE)
		spbZoomFactor->hide();

	// Init wavelength buttons
	bgrWaveLengths->setExclusive (! multiPicViewer->isInMultipleView());
	bgrOverlayWaveLengths->setExclusive (false);
	createWavelengthButtons (_positionManager);

	updateView();

	// To keep checkbox and menu btn synchronous (maybe ugly but well..)
	connect ( chkTrackVisibilityAllWavelengths, SIGNAL (clicked(bool)), menuBar->actView_TRACKS_SETFORALLWLS, SLOT (setChecked(bool)));
	connect ( pbtShowBackground, SIGNAL (clicked (bool)), menuBar->actView_TRACKS_BACKGROUND_TRACKS, SLOT (setChecked (bool)));

	// Export windows
	frmMovieExportDialog = new TTTMovieExport(this, positionManager->getFirstTimePoint(), positionManager->getLastTimePoint());
	frmSpecialMovieExportDialog = new TTTSpecialExport(this);
	divisionExportDialog = new TTTCellDivisionOverviewExport(this);



	// Tracking cell properties selection in menu
	setSelectedTrackingProperties(TTTManager::getInst().getTrackingCellProperties());


//#ifndef DEBUGMODE
//	// Hide test button
//	pbtDebugTest->setVisible(false);
//#else
//	// Connect test button
//	connect ( pbtDebugTest, SIGNAL (clicked()), this, SLOT (debugTest()));
//#endif
}

TTTMovie::~TTTMovie ()
{
	//if (menuBar)
	//	delete menuBar;		<-- OH: is our child, deleted automatically
	//if (statusBar)
	//	delete statusBar;	<-- OH: is our child, deleted automatically
	//if (spbTimerInterval)
	//	delete spbTimerInterval;	<-- OH: is our child's child, deleted automatically
	//if (spbTrackSize)
	//	delete spbTrackSize;		<-- OH: is our child's child, deleted automatically
	if(frmMovieExportDialog)
		delete frmMovieExportDialog;
	if(frmSpecialMovieExportDialog)
		delete frmSpecialMovieExportDialog;
	if(divisionExportDialog)
		delete divisionExportDialog;
	//if (frmGammaAdjust)
	//	delete frmGammaAdjust;		<-- OH: is our child, deleted automatically
}

//EVENT:
void TTTMovie::showEvent (QShowEvent *_ev)
{
	if(!_ev->spontaneous() && !windowLayoutLoaded) {
		// Load window layout
		UserInfo::getInst().loadWindowPosition(this, "TTTMovie");

		windowLayoutLoaded = true;

		//// Set this movie window to the active window, when it is showed first
		//// For some reason this is necessary to always receive FocusInEvents() 
		//setFocus(Qt::ActiveWindowFocusReason);  --> even that does not work, using ActivateWindow events now

		// Show movie window maximized
		showMaximized();

		// Display full picture
		setZoom (0);

		// Enable circle mouse pointer
		setCircleMousePointer(true);
	}

	//// Make position of this movie window to current position
	//TTTManager::getInst().frmPositionLayout->setPosition(positionManager->positionInformation.getIndex());
	//qDebug() << "showEvent";

	_ev->accept();
}

//EVENT: closeEvent()
void TTTMovie::closeEvent (QCloseEvent *_ev)
{
	stopPlayer();
	
    if (positionManager) {
		if (positionManager->frmRegionSelection)
			positionManager->frmRegionSelection->close();
	}

	if (frmGammaAdjust)
		frmGammaAdjust->close();

	if(frmPlayer)
		frmPlayer->close();

	if(specialEffectsWindow)
		specialEffectsWindow->close();

	// Save window layout
	UserInfo::getInst().saveWindowPosition(this, "TTTMovie");

	//closing the movie form does not quit the current analysis (the tree display form is the main form)
	_ev->accept();
}

////EVENT:
//void TTTMovie::focusInEvent (QFocusEvent *)
//{
//	//qDebug() << "focusInEvent()";
//
//	//set this position to be the current one
//	//TTTManager::getInst().setCurrentPosition (positionManager->positionInformation.getIndex());
//	TTTManager::getInst().frmPositionLayout->setPosition(positionManager->positionInformation.getIndex());
//}

//EVENT:
void TTTMovie::paintEvent (QPaintEvent *)
{
        //nothing...
}

//EVENT:
void TTTMovie::resizeEvent (QResizeEvent *)
{

        multiPicViewer->resize();

//        //the boxes with the different wavelengths have to be rearranged
//        layoutPictureFrames();
//
//	if (! adjustDisplayBox) {
//		//resizeEvent() was triggered from a manual size update because of a change of the display box
//		// in frmRegionSelection; so the display box need not be set!
//		adjustDisplayBox = true;
//		return;
//	}
//
//        adjustDisplayedRegion();
}

//EVENT:
void TTTMovie::mousePressEvent (QMouseEvent *ev)
{
	ev->ignore(); 
	//if (lblMovieStatus->hasMouse()) {
	//	DirectionLabelMousePressed = true;
	//	ev->accept();
	//}
}

//EVENT:
void TTTMovie::mouseMoveEvent (QMouseEvent *ev)
{
	ev->ignore();
}

////EVENT:
//void TTTMovie::mouseReleaseEvent (QMouseEvent *ev)
//{
//	ev->ignore();
//	if (lblMovieStatus->hasMouse() && DirectionLabelMousePressed) {
//		DirectionForward = ! DirectionForward;
//		if (DirectionForward)
//			lblMovieStatus->setText ("==>>");
//		else
//			lblMovieStatus->setText ("<<==");
//		ev->accept();
//	}
//}

//EVENT:
void TTTMovie::keyReleaseEvent (QKeyEvent *ev)
{
        //NOTE: the keys pressed during Tracking mode are handled in PictureView
//	if (! TTTManager::getInst().isTracking()) {
		
	QWidget* focusW = qApp->focusWidget();
        if (focusW && QString (focusW->className()) == "MyQSpinBox") {
                //send keyboard events rather to the spin box than to the movie player
                //QMessageBox::information (0, "test", "test2", "Ok");
        }
        else {
                switch (ev->key()) {
                        //note on the navigation keys: they have to be checked twice as PictureView only receives them if the user really clicked into it before
                        case Qt::Key_1:			//single picture backward
                                ProceedPicture (false, 0);
                                break;
                        case Qt::Key_3:			//single picture forward
                                ProceedPicture (true, 0);
                                break;
                        case Qt::Key_4:			//preceed 10%
                                ProceedPicture (false, 10);
                                break;
                        case Qt::Key_6:			//proceed 10%
                                ProceedPicture (true, 10);
                                break;
                        case Qt::Key_7:			//jump to first picture
                                ProceedPicture (false, -1);
                                break;
                        case Qt::Key_9:			//jump to last picture
                                ProceedPicture (true, -1);
                                break;
						case Qt::Key_Plus:
							// Make some shortcuts from TTTAutoTrackingTreeWindow also work from within movie window
							if(TTTAutoTracking* at = TTTManager::getInst().frmAutoTracking) {
								if(TTTAutoTrackingTreeWindow* tw = at->getTreeWindow())
									tw->assistantSelectNextRegion();
							}
							break;
						case Qt::Key_Minus:
							// Make some shortcuts from TTTAutoTrackingTreeWindow also work from within movie window
							if(TTTAutoTracking* at = TTTManager::getInst().frmAutoTracking) {
								if(TTTAutoTrackingTreeWindow* tw = at->getTreeWindow())
									tw->assistantSelectPrevRegion();
							}
							break;
						case Qt::Key_Enter:
							// Make some shortcuts from TTTAutoTrackingTreeWindow also work from within movie window
							if(TTTAutoTracking* at = TTTManager::getInst().frmAutoTracking) {
								if(TTTAutoTrackingTreeWindow* tw = at->getTreeWindow())
									tw->assistantAcceptCurRegion();
							}
							break;
                        //case Qt::Key_5:
                        //        multiPicViewer->centerCurrentTrack();
                        //        break;
						case Qt::Key_Alt:
							multiPicViewer->setOrientationBoxPosition (QCursor::pos(), true);
							break;
                        //case Qt::Key_Control:	//shift multiple wavelength orientation box
                        //        multiPicViewer->setOrientationBoxPosition (QCursor::pos(), true);
                        //        break;

                        ////------------------- Oliver -----------------------
                        ////hide controls (including tabs on the left) e.g. for movie export
                        //case Qt::Key_H:
                        //        //check if ctrl was pressed
                        //        if(ev->modifiers() & Qt::ControlModifier) {
                        //                hideShowControls();
                        //        }
                        //        break;
                        //--------------------Oliver---------------------
						// Now automatically done by menu
                        // Perform movie export
                        //case Qt::Key_E:
                        //        //check if ctrl was pressed
                        //        if(ev->modifiers() & Qt::ControlModifier)
                        //                exportMovie();
                        default:
                                processKey (ev->key(), (ev->modifiers() & Qt::ShiftModifier) == Qt::ShiftModifier, (ev->modifiers() & Qt::ControlModifier) == Qt::ControlModifier);
                }
        }
}

void TTTMovie::createWavelengthButtons (TTTPositionManager *_tttpm)
{
	if (! _tttpm)
		return;
	
	// Create layouts
	QHBoxLayout* hbl = new QHBoxLayout (grbWls);
	hbl->setSpacing(2);
	hbl->setContentsMargins(3, 2, 3, 2);
	QHBoxLayout* hblOverlay = new QHBoxLayout (grbOverlayWls);
	hblOverlay->setSpacing(2);
	hblOverlay->setContentsMargins(3, 2, 3, 2);
	
	// Create buttons for each wavelength, whether they are visible is decided later
	const QSet<int>& wls = _tttpm->getAvailableWavelengthsByRef();
	for (QSet<int>::const_iterator iter = wls.constBegin(); iter != wls.constEnd(); ++iter) {
		int wl = *iter;

		// Wl button
		QPushButton *wlbutton = new QPushButton (QString ("%1").arg (wl), nullptr, QString ("wlbutton_%1").arg (wl));
		wlbutton->setCheckable(true);
		wlbutton->setText (QString ("%1").arg (wl));
		wlbutton->setMaximumSize (20, 20);
		wlbutton->hide();
		bgrWaveLengths->addButton(wlbutton, wl);
		hbl->addWidget (wlbutton);

		// Overlay wl button
		QPushButton *wlbuttonOverlay = new QPushButton (QString ("%1").arg (wl), nullptr, QString ("wlbuttonoverlay_%1").arg (wl));
		wlbuttonOverlay->setCheckable(true);
		wlbuttonOverlay->setText (QString ("%1").arg (wl));
		wlbuttonOverlay->setMaximumSize (20, 20);
		wlbuttonOverlay->hide();
		bgrOverlayWaveLengths->addButton(wlbuttonOverlay, wl);
		hblOverlay->addWidget (wlbuttonOverlay);
	}
	
	// Add variable space at the end
	hbl->addStretch();
	hblOverlay->addStretch();
	
	connect ( bgrWaveLengths, SIGNAL (buttonClicked (int)), this, SLOT (setWaveLength (int)));
	connect ( bgrOverlayWaveLengths, SIGNAL (buttonClicked (int)), this, SLOT (overlayWlButtonClicked (int)));
	
	//BS 2010/05/10: show all buttons, regardless of loaded pictures (but only up to the highest available wl for this position)
    for (int i = 0; i <= QMIN ((int)MAX_WAVE_LENGTH, _tttpm->getMaxAvailableWavelength()); i++) {
		if (QAbstractButton* btn = bgrWaveLengths->button(i))
			btn->show();
		if (QAbstractButton* btn = bgrOverlayWaveLengths->button(i))
			btn->show();
	}

	// Enable button of current wavelength
	int currentWaveLength = positionManager->getWavelength();
	if( QAbstractButton* btn = bgrWaveLengths->button (currentWaveLength) ) {
		btn->setPaletteBackgroundColor (Qt::green);
		btn->setChecked(true);
	}
}

void TTTMovie::initDisplays() //PictureArray (PictureArray *_pictures)
{
        //positionManager->frmRegionSelection->setBox (positionManager->getDisplays().getDisplayedRegion(), Qt::white, 0.08f);
	
	//setZoom (100);

	//setZoom (0);
}

void TTTMovie::startRadiusSelection()
{
//@todo
//	if (PictureFrames.find (positionManager->getWavelength()))
//		PictureFrames.find (positionManager->getWavelength())->startRadiusSelectionMode();
}

////SLOT: play the movie (if _play is false the movie is stopped when running)
//void TTTMovie::play (bool _play)
//{	
//	
//	if (! _play)
//		MovieTimer->stop();
//	else 
//		if (! MoviePlaying)  
//			MovieTimer->start (TimerInterval);
//	
//	MoviePlaying = _play;
//	
//	if (! MoviePlaying)
//		pbtPlay->setOn (false);
//}

//SLOT: proceed one picture in the active direction
//called both by MovieTimer and by the single slide button
void TTTMovie::ProceedPicture(bool afterTrackpointSet)
{
	if (! positionManager->isInitialized())
		return;
	
        /// @todo
	//Currently, when moving backwards, the last visible is kept (just as forward), which is not equal to the forward one
	
	//if the movie has reached the end (front or back) and the reverse option is checked, the movie's direction has to be altered
	if (MoviePlaying && MovieReverse) {
		if (DirectionForward && (positionManager->getPictureIndex() == positionManager->getPictures()->getLastLoadedPictureIndex(positionManager->getWavelength()))) {
			DirectionForward = false;
			//lblMovieStatus->setText ("<<==");
		}
		else if ((! DirectionForward) && (positionManager->getPictureIndex() == positionManager->getPictures()->getFirstLoadedPictureIndex(positionManager->getWavelength()))) {
			DirectionForward = true;
			//lblMovieStatus->setText ("==>>");
		}
	}
		
	//get the index of the current picture, then ...
	PictureIndex tmp = positionManager->getPictureIndex();
	
	//... the next loaded picture of the current wavelength
	int step = 1;
	if(afterTrackpointSet)
		step = TTTManager::getInst().getFramesToSkipWhenTracking() + 1;
	for(int i = 0; i < step; ++i) {
		tmp = positionManager->getPictures()->nextLoadedPicture (tmp, DirectionForward);
		if(tmp.TimePoint == -1)
			// No loaded picture found
			break;
	}
	
	if (tmp.TimePoint == -1)
		if (positionManager->getTimepoint() < positionManager->getPictures()->getLastLoadedPictureIndex (-1).TimePoint)
			//there are further pictures, but not of that wavelength
			tmp = positionManager->getPictures()->nextLoadedPicture (positionManager->getPictureIndex(), DirectionForward, false, false);
	
	if (tmp.TimePoint != -1) {			//further pictures available
		
		//set global timepoint (local one is set indirectly)
		TTTManager::getInst().setTimepoint (tmp.TimePoint);
	}
	
	//if ProceedPicture() is called by the timer and the end/begin of the slides is reached,
	// the timer has to be switched off (if the loop mode is not set)
	//the same for keyboard sliding: if the player is not in loop mode and the end/begin of the 
	// slides is reached, there is no more sliding!
	if(MoviePlaying) {			// OH-23/10/11: Alls this makes only sense if MoviePlaying is true (i.e. player is running)
		if (DirectionForward) {
			if (positionManager->getPictureIndex() >= positionManager->getPictures()->getLastLoadedPictureIndex (positionManager->getWavelength())) {
				if (MovieLoop) 
					if (MovieReverse) {
						DirectionForward = false;
						//lblMovieStatus->setText ("<<==");
					}
					else {
						// OH-23/10/11: what is this good for? causes the program to crash
						/*
						PictureIndex pi = positionManager->getPictures()->getFirstLoadedPictureIndex (positionManager->getWavelength()) - 1;
						positionManager->setWavelength (pi.WaveLength);
						TTTManager::getInst().setTimepoint (pi.TimePoint);
						*/
						PictureIndex pi = positionManager->getPictures()->getFirstLoadedPictureIndex (positionManager->getWavelength());
						if(pi.TimePoint != -1)
							TTTManager::getInst().setTimepoint (pi.TimePoint);
						else
							stopPlayer();
					}
				else 
					//if (MoviePlaying) 
						stopPlayer();
						//play (false);
			}
		}
		else
			if (positionManager->getPictureIndex() <= positionManager->getPictures()->getFirstLoadedPictureIndex (positionManager->getWavelength())) {
				if (MovieLoop) 
					if (MovieReverse) {
						DirectionForward = true;
						//lblMovieStatus->setText ("==>>");
					}
					else {
						// OH-23/10/11: what is this good for? causes the program to crash
						/*
						PictureIndex pi = positionManager->getPictures()->getLastLoadedPictureIndex (positionManager->getWavelength()) + 1;
						positionManager->setWavelength (pi.WaveLength);
						TTTManager::getInst().setTimepoint (pi.TimePoint);
						*/
						PictureIndex pi = positionManager->getPictures()->getLastLoadedPictureIndex (positionManager->getWavelength());
						if(pi.TimePoint != -1)
							TTTManager::getInst().setTimepoint (pi.TimePoint);
						else
							stopPlayer();
					}
				else 
					//if (MoviePlaying) 
						stopPlayer();
			}
	}
	
	
	if (TTTManager::getInst().frmTracking)
		//delete the list of tracks excluded from Matlab transfer
		TTTManager::getInst().frmTracking->lieExcludedMatlabTracks->setText ("");

}

void TTTMovie::ProceedPicture (bool _direction, int _step, bool afterTrackpointSet)
{
	if (! positionManager->isInitialized())
		return;
	
	bool tmpD = DirectionForward;		//save current direction to restore it later
	
	switch (_step) {
		case -1:								//first/last picture
			if (_direction) {
				PictureIndex pi = positionManager->getPictures()->getLastLoadedPictureIndex();
				positionManager->setWavelength (pi.WaveLength);
				TTTManager::getInst().setTimepoint (pi.TimePoint);
			}
			else {
				PictureIndex pi = positionManager->getPictures()->getFirstLoadedPictureIndex();
				positionManager->setWavelength (pi.WaveLength);
				TTTManager::getInst().setTimepoint (pi.TimePoint);
			}
			break;
		case 0:									//one picture in _direction
			DirectionForward = _direction;
			
			ProceedPicture(afterTrackpointSet);
			DirectionForward = tmpD;
			break;
		default:								//_step is the percentual value
			PictureIndex tmp = positionManager->getPictureIndex();
			tmp.WaveLength = 0;
			for (int i = 0; i < (positionManager->getPictures()->getLoadedPictures() / _step); i++)
				tmp = positionManager->getPictures()->nextLoadedPicture (tmp, _direction);
			//if there is another valid loaded picture, jump to it
			if (tmp.TimePoint != -1) {
				//positionManager->setPictureIndex (tmp, true);
				positionManager->setWavelength (tmp.WaveLength);
				TTTManager::getInst().setTimepoint (tmp.TimePoint);
			}
	}

//@todo	wait until new trackmark can be set
//	if (TTTManager::getInst().isTracking()) {
//		//set delay until new trackmark can be set
//		PictureView *pv = PictureFrames.find (positionManager->getWavelength());
//		int trackmark_delay = spbTrackmarkDelay->value();
//		if ((pv) && (trackmark_delay > 0)) {
//			pv->setWaitSettingTrackmark (true);
//			QTimer::singleShot (trackmark_delay, pv, SLOT (activateTrackMarkSetting()));
//		}
//	}
	
//	//??? why here?
//	if (TTTManager::getInst().isTracking() && UseCellRadius)
//		PictureFrames.find (positionManager->getWavelength())->drawMouseCircle (PictureFrames.find (positionManager->getWavelength())->mapFromGlobal (QCursor::pos()));
}

////SLOT: toggle player reverse mode
//void TTTMovie::setPlayerReverse (bool _reverse)
//{
//	MovieReverse = _reverse;
//}

////SLOT: set the player to simple / loop mode
//void TTTMovie::setPlayerLoop (bool _loop)
//{
//	MovieLoop = _loop;
//}

////SLOT: set the timer interval
//void TTTMovie::setTimerInterval (int _interval)
//{
//	TimerInterval = _interval;
//	if (MoviePlaying) 
//		MovieTimer->changeInterval (TimerInterval);
//}

//SLOT: update display
void TTTMovie::updateView()
{
	if (! positionManager->isInitialized() || !positionManager->getPictures())
		return;
		
	/*qDebug() << "updateView() - " << positionManager->positionInformation.getIndex();*/

	if (positionManager->getPictures()->getLoadedPictures() == 0)
		return;
		
	lblFirstPicture->setNum (positionManager->getPictures()->getFirstLoadedPictureIndex().TimePoint);
		
	/*
	lblLastPicture->setNum (positionManager->getPictures()->lastLoadedPicture().TimePoint);
	lblCurrentTimePoint->setText (QString ("TP %1").arg (positionManager->getTimepoint()));
	*/
	updateLblLastPicture(positionManager->getTimepoint());
		
	//lblFirstPicture->setNum (1);
	//lblLastPicture->setNum (positionManager->getPictures()->getLoadedPictures (-1));
		
	//the sliders max and min value are only corresponding to the number of actually loaded pictures
	sldMovie->setMinValue (0);
	sldMovie->setMaxValue (positionManager->getPictures()->getMaxCountLoaded() - 1);
		
	//lblLoadedImages->setText (QString ("%1 images loaded").arg (positionManager->getPictures()->getLoadedPictures()));

	/*
	QString tmp = lblCurrentIndex->text();
	lblCurrentIndex->setText (tmp.replace (tmp.find ("of"), 100, QString ("of %1").arg (positionManager->getPictures()->getLoadedPictures())));
	*/
	lastIndex = positionManager->getPictures()->getLoadedPictures();
	updateStatusBarMessage();
		
	// Find first picture..
	setToTimepoint(positionManager->getTimepoint(), 0);

	/*
	if (positionManager->getTimepoint() > positionManager->getLastTimePoint() || positionManager->getTimepoint() < positionManager->getFirstTimePoint())
		//display first picture
		//necessary only on startup
                    //showPicture (positionManager->getPictures()->getFirstLoadedPictureIndex (-1).TimePoint, -1);
                    multiPicViewer->drawPictures (positionManager->getPictures()->getFirstLoadedPictureIndex (-1).TimePoint, -1);
	*/
}

//SLOT:
void TTTMovie::changeDisplayPosition (QPointF _shift)
{
	if (! positionManager->isInitialized())
		return;
	
	//set the displayed region depending on the zoom factor
	shiftPictures (-1, _shift);
}

//SLOT:
void TTTMovie::changeDisplaySize()
{
	//BS 2010/02/10: currently switched off (slot only triggered by TTTRegionSelection when changing the box size -> not necessary as direct movie window resizing is possible, too
	return;
	
	
	
	
/*	if (! positionManager->isInitialized())
		return;
	
	//the pictures do not have to be shifted!
	
	int x = 0, y = 0;
	
	//set the displayed region depending on the zoom factor
	//for zoom factors 50,100,100+ the form's size has to be changed!
	switch (positionManager->getDisplays().getZoomFactor ()) {
		case -1:
			//do not change the form's size - just the displayed region and the zoom factor
			//the zoom factor is automatically generated in picturecontainer
			positionManager->getDisplays().setDisplayedRegion (-1, ZoomBox.left(), ZoomBox.top(), ZoomBox.width(), ZoomBox.height());
			break;
		case 0:
			//do not change the form's size and also not the displayed region (full loaded region)
			//the zoom factor is automatically generated in picturecontainer
			break;
		case 50:
			//change the form's size to display the whole selected region with exactly 50% zoom
			//so the displayed region has to be set as selected, but the display needs only half the size!
			x = fraPictures->x() + ZoomBox.width() / 2;
			y = fraPictures->y() + ZoomBox.height() / 2;
		case 100:
			//change the form's size to display the whole selected region with exactly 100% zoom
			//so the displayed region has to be set as selected, and the display needs exactly the same size
			
			if (x == 0) {
				x = fraPictures->x() + ZoomBox.width();
				y = fraPictures->y() + ZoomBox.height();
			}
		default:
		
			int ZOOM = (positionManager->getDisplays().getZoomFactor ());
			
			if (ZOOM > 0 & ZOOM < 1000) {
				positionManager->getDisplays().setDisplayedRegion (-1, ZoomBox.left(), ZoomBox.top(), ZoomBox.width(), ZoomBox.height());
				
				if (x == 0) {
					x = fraPictures->x() + (ZoomBox.width() * ZOOM) / 100;
					y = fraPictures->y() + (ZoomBox.height() * ZOOM) / 100;
				}
		
				//x and y must be within the form's minimum and maximum size!
				x = QMIN (x, this->maximumSize().width());
				x = QMAX (x, this->minimumSize().width());
				y = QMIN (y, this->maximumSize().height());
				y = QMAX (y, this->minimumSize().height());
				
				adjustDisplayBox = false;		//is set back to true in resizeEvent()
				
				//NOTE: with resize(), automatically a paintEvent() is triggered;
				//      this makes a repaint() or update() command obsolete here
				this->resize (x, y);
			}
			
	}*/
}

//SLOT: adjust gamma, black/white point, ...
void TTTMovie::adjustGraphics()
{
    frmGammaAdjust->show();
}

//SLOT:
//@todo
//void TTTMovie::showPicture (int _timepoint, int _wavelength, bool _reloadAllInMultipleMode)
//{
//multiPicViewer->drawPictures (timePoint);
//
//	if (! positionManager->isInitialized())
//		return;
//
//	//SystemInfo::report ("showPicture() called", false, true);
//
//	if (_timepoint == -2)
//		_timepoint = positionManager->getTimepoint();
//	if (_timepoint < 0)
//		return;
//
//	//display all possible wavelengths for this timepoint (only loaded pictures!)
//	for (int i = 0; i <= MAX_WAVE_LENGTH; i++) {
//		if (bgrWaveLengths->find (i)) {
//			if (positionManager->getPictures()->isLoaded (_timepoint, i)) {
//				//indicate loaded wavelengths with a bold big font and a raised border
//				//bgrWaveLengths->find (i)->show();
//				bgrWaveLengths->find (i)->setFont (QFont ("Sans Serif", 12, QFont::Bold, false));
//				((QPushButton*)(bgrWaveLengths->find (i)))->setFlat (false);
//			}
//			else {
//				//not loaded -> italic small font, flat style (no border)
//				//bgrWaveLengths->find (i)->hide();
//				bgrWaveLengths->find (i)->setFont (QFont ("Sans Serif", 8, QFont::Normal, true));
//				((QPushButton*)(bgrWaveLengths->find (i)))->setFlat (true);
//			}
//		}
//	}
//
//	//showPicture() is called with _wavelength == -1 when tracking mode is started
//	if (_wavelength == -1)
//		_wavelength = positionManager->getWavelength();
//	if (_wavelength == -1) {
//		_wavelength = 0;
//		positionManager->setWavelength (_wavelength);
//	}
//
//	int _wlstart = _wavelength, _wlend = _wavelength;
//
//	if (MultipleMode)
//		if (_reloadAllInMultipleMode) {			//all wavelengths should be reloaded - default
//			_wlstart = 0;
//			_wlend = MAX_WAVE_LENGTH;
//		}
//
//	if (Overlay) {
//		_wlstart = 0;
//		_wlend = MAX_WAVE_LENGTH;
//		if (PictureFrames.find (MAX_WAVE_LENGTH + 1)) {
//			PictureFrames.find (MAX_WAVE_LENGTH + 1)->resetImg();
//		}
//	}
//
//	int drawReturn = 1;			//return value of PictureView::draw(), only evaluated for non-overlay mode
//
//	//note: counting downwards to enable overlaying pictures (for this, the wavelengths >0 have to be drawn first)
//	for (int i = _wlend; i >= _wlstart; i--) {
//
//		//only draw if the pictures of this wavelength should be shown and if there is a picture present at this timepoint
//		//NOTE: in normal (not overlay) mode, a picture (e.g. of wl 1) is not removed from sight, if there was one for
//		//        the last timepoint, but not now => more tranquility on the screen and a better orientation
//		//       but in overly mode, this is not desired, as the cells could be associated wrongly
//		//        => only really present pictures are displayed
//		if (PictureFrames.find (i)->getShow()) {
//
//			if (! Overlay) {
//
//                                //drawReturn = PictureFrames.find (i)->draw();
//                                PictureFrames.find (i)->draw();
//
//				//draw details for every picture only if they are not overlayed
//                                PictureFrames.find (i)->setCurrentSymbols (symbolHandler.readSymbols (_timepoint, i));
//                                if ((showCellPaths) & (! cellPathCells.isEmpty()))
//                                        PictureFrames.find (i)->drawCircles (calcCellPath(), QPainter::CompositionMode_Source, false);
//                                if (StartingCellSelectionMode)
//                                        PictureFrames.find (i)->drawCircles (selectedStartingCells, TrackSize, Qt::white, QPainter::CompositionMode_Source, false, true);
//
//                                PictureFrames.find (i)->setDrawWavelengthIndex (drawWavelengthIndex);
//                                PictureFrames.find (i)->drawAdditions();
//
////				PictureFrames.find (i)->drawIndex (drawWavelengthIndex);
////				PictureFrames.find (i)->drawOrientationBox();
////				PictureFrames.find (i)->displayGraphics();
////				PictureFrames.find (i)->setCurrentSymbols (symbolHandler.readSymbols (_timepoint, i));
////				PictureFrames.find (i)->drawCurrentSymbols();
////				if (/*(ShowTracks) &*/ (showCellPaths) & (! cellPathCells.isEmpty()))
////					//PictureFrames.find (i)->drawCircles (calcCellPath(), UserInfo::getInst().getStyleSheet().getCellPathThickness(), UserInfo::getInst().getStyleSheet().getCellPathColor(), Qt::CopyROP, true, false);
////                                        PictureFrames.find (i)->drawCircles (calcCellPath(), QPainter::CompositionMode_Source, false);
////
////				/*if (ShowTracks)*/
////				PictureFrames.find (i)->drawTracks (TrackSize, /*ShowTrackNumbers,*/ CellBackgroundDisplay, UseCellRadius, 0);
////
////				if (StartingCellSelectionMode)
////                                        PictureFrames.find (i)->drawCircles (selectedStartingCells, TrackSize, Qt::white, QPainter::CompositionMode_Source, false, true);
//
//                                PictureFrames.find (i)->update();
//                        }
//			else {
//				//the second parameter is the color channel
//				//0 == red, 1 == green, 2 == blue
//				//-1 == alpha 50%
//				PictureFrames.find (i)->draw (PictureFrames.find (MAX_WAVE_LENGTH + 1), i - 1);
//
//				//currently an overlay picture is created
//				//in full picture display mode, the ZoomX/Y of the DisplaySettings must be set also in the overlay frame
//				//for this purpose, just take the ZoomX/Y settings just set in the current picture frame
//				positionManager->getDisplays().at (MAX_WAVE_LENGTH + 1).ZoomX = positionManager->getDisplays().at (i).ZoomX;
//				positionManager->getDisplays().at (MAX_WAVE_LENGTH + 1).ZoomY = positionManager->getDisplays().at (i).ZoomY;
//			}
//
//		}
//	}
//
//	if (Overlay) {
//		if (PictureFrames.find (MAX_WAVE_LENGTH + 1)) {
//
//			//draw details only for last picture (overlayed pictures)
//                        PictureFrames.find (MAX_WAVE_LENGTH + 1)->draw();
//                        //PictureFrames.find (MAX_WAVE_LENGTH + 1)->repaint();
//
//                        PictureFrames.find (MAX_WAVE_LENGTH + 1)->setDrawWavelengthIndex (drawWavelengthIndex);
//                        //PictureFrames.find (MAX_WAVE_LENGTH + 1)->drawIndex (drawWavelengthIndex);
//                        //PictureFrames.find (MAX_WAVE_LENGTH + 1)->drawOrientationBox();
//                        //PictureFrames.find (MAX_WAVE_LENGTH + 1)->displayGraphics();
//
//                        if ((showCellPaths) & (! cellPathCells.isEmpty()))
//				//PictureFrames.find (MAX_WAVE_LENGTH + 1)->drawCircles (calcCellPath(), UserInfo::getInst().getStyleSheet().getCellPathThickness(), UserInfo::getInst().getStyleSheet().getCellPathColor(), Qt::CopyROP, true, false);
//                                PictureFrames.find (MAX_WAVE_LENGTH + 1)->drawCircles (calcCellPath(), QPainter::CompositionMode_Source, false);
//
//			//PictureFrames.find (MAX_WAVE_LENGTH + 1)->setCurrentSymbols (symbolHandler.readSymbols (_timepoint, MAX_WAVE_LENGTH + 1));
//			//PictureFrames.find (MAX_WAVE_LENGTH + 1)->drawCurrentSymbols();
//
// 			//if (ShowTracks)
//                        //PictureFrames.find (MAX_WAVE_LENGTH + 1)->drawTracks (TrackSize, /*ShowTrackNumbers,*/ CellBackgroundDisplay, UseCellRadius, 0);
//                        PictureFrames.find (MAX_WAVE_LENGTH + 1)->drawAdditions();
//		}
//	}
//
//	//set current Picture_Index
//	//commented by BS on 2010/02/05
//	//positionManager->setTimepoint_withoutSignal (_timepoint, _wavelength);
//
//	if (Overlay || (drawReturn == 1)) {
//		//everything went fine while drawing
//
//		call_slidePicture = false;
//		sldMovie->setValue (positionManager->getPictures()->getTimePointIndex (_timepoint));
//		call_slidePicture = true;
//
//		//display picture x of y (loaded ones)
//		lblCurrentIndex->setText (QString ("N.%1 of %2").arg (1 + positionManager->getPictures()->getAbsIndexLoaded (PictureIndex (_timepoint, _wavelength)))
//														.arg (positionManager->getPictures()->getLoadedPictures (-1)));
//		lblCurrentTimePoint->setText (QString ("TP %1").arg (_timepoint));
//
//		//@todo
//		//if the user retracks a cell and changes the properties, then the latest set properties should be overtaken for the
//		// next timepoints, rather than displaying the old set ones
//		//maybe it is easier to forward the newly set properties to each timepoint ahead than dealing with this functionality
//		if (TTTManager::getInst().getCurrentTrack()) {
//			if (TTTManager::getInst().isTracking()) {
//				//update the cell property form (if there are already properties set for the currently tracked track)
//				TTTManager::getInst().frmTracking->showCurrentProperties();
//			}
//
//			//display fluorescence intensities (absolute values for cell and background); for A.H. and others
//			if (TTTManager::getInst().getCurrentTrack()->backgroundTracked()) {
//				//with a tracked background, the cell is automatically tracked in any fluorescent wavelength
//				int wl = UserInfo::getInst().getStyleSheet().getExportWavelength();
//				showCellIntegrals (TTTManager::getInst().getCurrentTrack(), positionManager->getTimepoint(), wl);
//			}
//		}
//	}
//
//}

void TTTMovie::showCellIntegrals (Track *_track, int _timepoint, int _zIndex, int _wavelength)
{
	if (! _track)
		return;
	if (! _track->aliveAtTimePoint (_timepoint))
		return;
	
	if (! positionManager->isInitialized())
		return;
	
	if (! positionManager->getPictures()->isLoaded (_timepoint, _wavelength, 1))
		return;
	
	TrackPoint tp = _track->getTrackPoint (_timepoint, false);
        if ((tp.CellDiameter > 0.0f) & (tp.X != -1.0f) & (tp.XBackground != -1.0f)) {
		int ValueIntegralCell = 0;
		int ValueIntegralBackground = 0;
		int dx = 0, dy = 0;
		int PixelColor = 0;
		
		//the trackpoint coordinates need to be mapped to picture coordinates!
		
                QPointF tmp = positionManager->getDisplays().at (_wavelength).calcTransformedCoords (QPointF (tp.X, tp.Y), &positionManager->positionInformation);
		int tp_X = (int)tmp.x();
		int tp_Y = (int)tmp.y();
                int radius = (int)(tp.CellDiameter / 2.0f);
		
		for (	int x = tp_X - radius; x <= tp_X + radius; x++) {
			for (	int y = tp_Y - radius; y <= tp_Y + radius; y++) {
							
				//check if point is inside the region of the cell (circle!) via Pythagoras
				dx = x - tp_X;
				dy = y - tp_Y;
				if ((dx*dx + dy*dy) <= radius*radius) {
					PixelColor = positionManager->getPictures()->readPixel (_timepoint, _zIndex, _wavelength, x, y);
					ValueIntegralCell += PixelColor;
				}
			}
		}
						
                tmp = positionManager->getDisplays().at (_wavelength).calcTransformedCoords (QPointF (tp.XBackground, tp.YBackground), &positionManager->positionInformation);
		tp_X = (int)tmp.x();
		tp_Y = (int)tmp.y();
		for (	int x = tp_X - radius; x <= tp_X + radius; x++) {
			for (	int y = tp_Y - radius;	y <= tp_Y + radius; y++) {
				
				//check if point is inside the region of the cell (circle!) via Pythagoras
				dx = x - tp_X;
				dy = y - tp_Y;
				if ((dx*dx + dy*dy) <= radius*radius) {
					PixelColor = positionManager->getPictures()->readPixel (_timepoint, _zIndex, _wavelength, x, y);
					ValueIntegralBackground += PixelColor;
				}
			}
		}
		
		//display values in labels
		lblCellIntegral->setText (QString ("Cell abs.\n%1").arg (ValueIntegralCell));
		lblBackgroundIntegral->setText (QString ("Bkgr. abs.\n%1").arg (ValueIntegralBackground));
	}
	
}

bool TTTMovie::startTracking (bool _isMainMovieWindow)
{

    //start tracking mode
    //*******************
    switchTrackingButtons (true);

    stopPlayer();
    DirectionForward = true;
    //chkPlayerReverse->setChecked (false);
    //chkPlayerLoop->setChecked (false);


    Track_OldFirstTrace = TTTManager::getInst().getCurrentTrack()->getFirstTrace();
    Track_OldLastTrace = TTTManager::getInst().getCurrentTrack()->getLastTrace();
    Track_OldFirstTrackPoint = TTTManager::getInst().getCurrentTrack()->getTrackPointByNumber (1).TimePoint;

    return multiPicViewer->startTracking (_isMainMovieWindow);

}

void TTTMovie::switchTrackingButtons (bool _enable)
{
	if (_enable) {
		lblCurrentTrackNumber->setText (QString ("<font color=\"red\">Tracking \n %1</font>").arg (TTTManager::getInst().getCurrentTrack()->getNumber()));
		lblTracking->setText (QString ("<font color=\"red\">Tracking \n %1</font>").arg (TTTManager::getInst().getCurrentTrack()->getNumber()));
	}
	else {
		if (pbtEditBackground->isOn())
			pbtEditBackground->toggle();

		lblCurrentTrackNumber->setText ("");
		lblTracking->setText("");
		
//@todo
//		for (int i = 0; i < (int)PictureFrames.size(); i++)
//			if (PictureFrames.find (i))
//				PictureFrames.find (i)->stopTracking();
	
	}
	
	pbtCellDivision->setEnabled (_enable);
	pbtCellApoptosis->setEnabled (_enable);
	pbtCellLost->setEnabled (_enable);
	pbtCellComment->setEnabled (_enable);
	pbtInterruptTracking->setEnabled (_enable);
	pbtEditBackground->setEnabled (_enable);
	pbtEndoMitosis->setEnabled (_enable);
	pbtPropertySemiAdherent->setEnabled(_enable);
	pbtPropertyAdherent->setEnabled(_enable);
	//pbtPropertyNone->setEnabled(_enable);
	pbtPropertyFreeFloating->setEnabled(_enable);

	pbtWL1->setEnabled(_enable);
	pbtWL2->setEnabled(_enable);
	pbtWL3->setEnabled(_enable);
	pbtWL4->setEnabled(_enable);
	pbtWL5->setEnabled(_enable);
	pbtWL6->setEnabled(_enable);
	pbtWL7->setEnabled(_enable);
	pbtWL8->setEnabled(_enable);
	pbtWL9->setEnabled(_enable);
	pbtWL10->setEnabled(_enable);
}

//SLOT:
bool TTTMovie::stopTracking()
{
	//stop tracking mode
	//******************
	
	TrackStopReason reason = TS_NONE;
	
	if (sender() == pbtCellDivision)
		reason = TS_DIVISION;
	else if (sender() == pbtCellApoptosis)
		reason = TS_APOPTOSIS;
	else if (sender() == pbtCellLost)
		reason = TS_LOST;
	else if (sender() == pbtInterruptTracking)
		//note: an existing stop reason is not deleted
		reason = TS_NONE;
	
//@todo
//	//if Escape was pressed in the picture view
//	if (PictureFrames.find (positionManager->getWavelength()))
//		if (sender() == PictureFrames.find (positionManager->getWavelength()))
//			reason = TS_NONE;
	
	return stopTracking (reason);
}

bool TTTMovie::stopTracking (int _reason)
{
	TrackStopReason reason = (TrackStopReason)_reason;

	switchTrackingButtons (false);

	TTTManager::getInst().setTrackStopReason (reason);

	multiPicViewer->stopTracking();

	return TTTManager::getInst().stopTracking();
}

//SLOT:
bool TTTMovie::stopTrackingAndContinueWithDaughter()
{
	// Get cell number of current track
	int curCellNumber = -1;
	if(TTTManager::getInst().getCurrentTrack()) 
		curCellNumber = TTTManager::getInst().getCurrentTrack()->getTrackNumber();
	if(curCellNumber <= 0)
		return false;

	// Check if AutoTrackingTreeWindow is open
	TTTAutoTrackingTreeWindow* atTreeWindow = 0;
	if(TTTManager::getInst().frmAutoTracking && TTTManager::getInst().frmAutoTracking->getTreeWindow())
		atTreeWindow = TTTManager::getInst().frmAutoTracking->getTreeWindow();

	// Stop tracking and assign cell division (unless we are not tracking, then just try to select daughter cell)
	bool wasTracking = false;
	bool wasTrackingInAutoTrackingMode = false;
	if(TTTManager::getInst().isTracking()) {
		wasTracking = true;
		if(!stopTracking(TS_DIVISION))		// TODO: do not call this in when auto-tracking is active (deletes all trackpoints after division) - use correct function in AT window
			return false;
		if(atTreeWindow)
			// If manual tracking was started from auto-tracking window, special care has to be taken
			wasTrackingInAutoTrackingMode = atTreeWindow->manualTrackingWasInAutoTrackingMode();
	}

	// Select daughter cell
	Track* daughterCell = 0;
	if(!wasTrackingInAutoTrackingMode) {
		// Select in TTTManager
		Tree* curTree = TTTManager::getInst().getTree();
		if(!curTree)
			return false;
		daughterCell = curTree->getTrack(curCellNumber*2);
		if(!daughterCell)
			return false;
		if(TTTManager::getInst().frmTracking)
			TTTManager::getInst().frmTracking->selectTrack(daughterCell);
	}
	else {
		// Select in TTTAutoTrackingTreeWindow
		Tree* curTree = atTreeWindow->getCurrentTree();
		if(!curTree)
			return false;
		daughterCell = curTree->getTrack(curCellNumber*2);
		if(!daughterCell)
			return false;

		// Simulate cell click event
		atTreeWindow->cellSelected(curTree, daughterCell);
	}

	// If tracking mode was off, we are done
	if(!wasTracking)
		return true;

	// Start tracking again
	if(!wasTrackingInAutoTrackingMode) {
		if(TTTManager::getInst().getCurrentTrack() != daughterCell)	// Check that selecting cell really worked
			return false;
		return TTTManager::getInst().startTracking();
	}
	else {
		if(atTreeWindow->getCurrentTrack() != daughterCell)
			return false;
		return atTreeWindow->startManualTracking(); 
	}
}


bool TTTMovie::selectOtherCell(int code)
{
	// Get cell number of current track
	int curCellNumber = -1;
	if(TTTManager::getInst().getTree() && TTTManager::getInst().getCurrentTrack()) 
		curCellNumber = TTTManager::getInst().getCurrentTrack()->getTrackNumber();
	if(curCellNumber <= 0)
		return false;

	// Determine cell number of cell to select
	int newCellNumber;
	switch(code) {
	case  0:
		// Sister
		if(curCellNumber % 2 == 0)
			newCellNumber = curCellNumber + 1;
		else
			newCellNumber = curCellNumber - 1;
		break;
	case 1:
		// Mother
		newCellNumber = curCellNumber / 2;
		break;
	case 2:
		// Daughter 1
		newCellNumber = curCellNumber * 2;
		break;
	case 3:
		// Daughter 2
		newCellNumber = curCellNumber * 2 + 1;
		break;
	default:
		return false;
	}

	// Check if AutoTrackingTreeWindow is open
	TTTAutoTrackingTreeWindow* atTreeWindow = 0;
	if(TTTManager::getInst().frmAutoTracking && TTTManager::getInst().frmAutoTracking->getTreeWindow())
		atTreeWindow = TTTManager::getInst().frmAutoTracking->getTreeWindow();

	// Stop tracking (without assigning/changing cell fate)
	bool wasTracking = false;
	bool wasTrackingInAutoTrackingMode = false;
	if(TTTManager::getInst().isTracking()) {
		wasTracking = true;
		if(!stopTracking(TS_NONE))
			return false;
		if(atTreeWindow)
			// If manual tracking was started from auto-tracking window, special care has to be taken
			wasTrackingInAutoTrackingMode = atTreeWindow->manualTrackingWasInAutoTrackingMode();
	}

	// Check if new cell exists
	Tree* curTree = 0;
	Track* newCell = 0;
	if(!wasTrackingInAutoTrackingMode) 
		curTree = TTTManager::getInst().getTree();
	else 
		curTree = atTreeWindow->getCurrentTree();
	if(curTree)
		newCell = curTree->getTrackByNumber(newCellNumber);
	if(!newCell)
		return false;

	// Select new cell
	if(!wasTrackingInAutoTrackingMode) {
		// Select in TTTManager
		if(TTTManager::getInst().frmTracking)
			TTTManager::getInst().frmTracking->selectTrack(newCell);
	}
	else {
		// Select in TTTAutoTrackingTreeWindow
		atTreeWindow->cellSelected(curTree, newCell);	// simulate cell click event
	}

	// If tracking mode was off, we are done
	if(!wasTracking) 
		return true;

	// Start tracking again
	if(!wasTrackingInAutoTrackingMode) {
		if(TTTManager::getInst().getCurrentTrack() != newCell)	// Check that selecting cell really worked
			return false;
		return TTTManager::getInst().startTracking();
	}
	else {
		if(atTreeWindow->getCurrentTrack() != newCell)
			return false;
		return atTreeWindow->startManualTracking(); 
	}
}

//SLOT:
void TTTMovie::setToTimepoint (int _newTimepoint, int /*_oldTimepoint*/)
{
	// Check if initialized and _newTimepoint is valid
	if (! positionManager->isInitialized() || _newTimepoint == -1)
		return;

	//// Test: Do not update display, if widget is not visible
	//QRegion visible = visibleRegion();
	//if(visible.isEmpty())
	//	return;
	//QVector<QRect> tmp = visible.rects();

	// Actually change timepoint by redirecting to multiPicViewer
	multiPicViewer->setToTimepointWavelength (_newTimepoint, -1);

	// Update slide
	call_slidePicture = false;
	sldMovie->setValue (positionManager->getPictures()->getTimePointIndex (_newTimepoint));
	call_slidePicture = true;

	// Update currentIndex and lastIndex
	currentIndex = 1 + positionManager->getPictures()->getAbsIndexLoaded (PictureIndex (_newTimepoint, positionManager->getWavelength(), positionManager->getZIndex()));
	lastIndex = positionManager->getPictures()->getLoadedPictures(-1);
	
	// Update label right of slider and status bar message
	updateStatusBarMessage();
	updateLblLastPicture(_newTimepoint);

	// Use repaint to directly call paintEvent() on the movie window (with all PictureViews) without using the event queue (this is 
	// required because of a weird problem observed on Windows8 Server where, when e.g. pressing the 0 button, 
	// key events come in faster than paint events, thereby causing the complete movie window to freeze until
	// the key is released again
	repaint();
}

//SLOT:
void TTTMovie::setShowTracks (bool _on, bool _forceForAllTracks)
{
	// Keep check box and menu button synchronous
	menuBar->actView_TRACKS_TRACKS->setChecked(_on);
	chkShowTracks->setChecked(_on);

	bool setForAll = chkTrackVisibilityAllWavelengths->isChecked() || _forceForAllTracks;
	
	// Get start and end WL
	int startWL, endWL;
	if (setForAll) {
		startWL = 0;
        endWL = MAX_WAVE_LENGTH + 1;
	}
	else {
		startWL = positionManager->getWavelength();
		endWL = startWL;
	}
	
    multiPicViewer->setShowTracks (_on, startWL, endWL);
}

//SLOT:
void TTTMovie::setShowTrackNumbers (bool _on)
{
	// Keep check box and menu button synchronous
	menuBar->actView_TRACKS_TRACKNUMBERS->setChecked(_on);
	chkShowTrackNumbers->setChecked(_on);

	bool setForAll = chkTrackVisibilityAllWavelengths->isChecked();
	
	int startWL = positionManager->getWavelength();
	int endWL = positionManager->getWavelength();
	
	if (setForAll) {
		startWL = 0;
		endWL = 6;
	}
	
    multiPicViewer->setShowTrackNumbers (_on, startWL, endWL);
}

//SLOT:
void TTTMovie::setShowWavelengthBox (bool _shown)
{
        bool setForAll = chkTrackVisibilityAllWavelengths->isChecked();

        int startWL = positionManager->getWavelength();
        int endWL = positionManager->getWavelength();

        if (setForAll) {
                startWL = 0;
                endWL = MAX_WAVE_LENGTH + 1;
        }

        multiPicViewer->setShowWavelengthBox (_shown, startWL, endWL);

        if (TTTManager::getInst().frmTracking) {
                if (TTTManager::getInst().frmTracking->treeView) {
                        StyleSheet *ss = & UserInfo::getInst().getRefStyleSheet();

                        ss->setWavelengthVisibility (_shown);
                }
        }
}

//SLOT:
void TTTMovie::setZoom()
{
	
	//if (!positionManager->frmRegionSelection->isVisible())
	//	positionManager->frmRegionSelection->show();
	
	if (sender() == pbtZoom50)
                setZoom (50);
	else if (sender() == pbtZoom100)
                setZoom (100);
	else if (sender() == spbZoomFactor) {
		if (call_setZoomOnspZoomFactor) {
                        if (multiPicViewer->getCurrentZoom() != spbZoomFactor->value()) {
				setZoom (spbZoomFactor->value());
			}
		}
	}
	else if (sender() == pbtFullPicture)
		setZoom (0);
}

//SLOT:
void TTTMovie::setZoom (int _zoomPercent, float _zoomX, float _zoomY, bool /*_forceSetting*/)
{
        int wavelength = positionManager->getWavelength();

        if (wavelength < 0 || wavelength > MAX_WAVE_LENGTH)
                return;

        switch (_zoomPercent) {
                case 0:
                        //the full loaded region is displayed

                        //BS 2010/03/29 due to Dirk's wish, 100% is set in the spinbox - if the user begins to zoom with mouse wheel again, there is no "sudden deep jump"
                        setZoomSpinboxValue (50, false);
                        multiPicViewer->setZoom (0);
                        break;
                default:
                        //note: automatically triggered endless signal loop is stopped in slot setZoom()
                        setZoomSpinboxValue (_zoomPercent, false);

                        multiPicViewer->setZoom (_zoomPercent, _zoomX, _zoomY);

                        break;
        }

}

//SLOT:
void TTTMovie::slidePicture (int _timePointIndex)
{	
	if (! positionManager->isInitialized())
		return;
	
	//call_slidePicture == false if slidePicture() was called by showPicture() by setting the slider value
	if (call_slidePicture) {
		int tp = positionManager->getPictures()->getTimePoint (_timePointIndex);
		
		if (tp != positionManager->getTimepoint())
			//set global timepoint (local one is set indirectly)
			TTTManager::getInst().setTimepoint (tp);
		
	}
}

//SLOT:
void TTTMovie::shiftPictureSlot (int _triggeredBy, QPointF _shift)
{
	shiftPictures (_triggeredBy, _shift);
}

QPointF TTTMovie::shiftPictures (int _triggeredBy, QPointF _shift)
{
	//NOTE: _shift is provided with screen coordinates (pixel)

//@todo
//	for (int i = 0; i < (int)PictureFrames.size(); i++) {
//		PictureFrames.find (i)->setCurrentSymbols (symbolHandler.readSymbols (positionManager->getTimepoint(), i));
//
//		_shift = PictureFrames.find (i)->shift (_shift);
//
//		//the picture itself is drawn automatically in ->shift(), but not the tracks!
//		if (PictureFrames.find (i)->getShow()) {
//			PictureFrames.find (i)->drawTracks (TrackSize, /*ShowTrackNumbers,*/ CellBackgroundDisplay, UseCellRadius, 0);
//		}
//	}
//
//	//if _triggeredBy == -1, the slot was called because the display box in frmRegionSelection was changed
//	// so there is no need to update it; but if one of the pictures was grabbed and shifted, also the display
//	// box has to be updated
//	//the user can also have clicked on "center cell"; then the region box has to be updated, too
//	if (_triggeredBy != -1) {
//
//		positionManager->frmRegionSelection->getCurrentBox()->shift ((int)_shift.x(), (int)_shift.y());
//                //positionManager->frmRegionSelection->repaint();
//                positionManager->frmRegionSelection->drawBox();
//
//	}
//
//	if ((! _shift.isNull()) & Overlay)
//		showPicture (positionManager->getTimepoint(), MAX_WAVE_LENGTH + 1);
	
	//the actual shift
	return _shift;
}

void TTTMovie::setWaveLength (int _waveLength, bool _show, bool _callShowPicture)
{
        multiPicViewer->setWaveLength (_waveLength, _show, _callShowPicture);

    //display the index of the loaded picture
		/*
	lblCurrentIndex->setText (QString ("N.%1 of %2").arg (1 + positionManager->getPictures()->getAbsIndexLoaded (positionManager->getPictureIndex())).arg (positionManager->getPictures()->getLoadedPictures (-1)));	
	*/

	currentIndex = 1 + positionManager->getPictures()->getAbsIndexLoaded (positionManager->getPictureIndex());
	lastIndex = positionManager->getPictures()->getLoadedPictures (-1);
	updateStatusBarMessage();
	
	//lblCurrentTimePoint->setText (QString ("TP %1").arg (positionManager->getTimepoint()));
	updateLblLastPicture(positionManager->getTimepoint());
}

//SLOT:
void TTTMovie::setWaveLength (int _waveLength)
{
//@todo

	if (! positionManager->isInitialized())
		return;
	
	if (! bgrWaveLengths->button(_waveLength))
		return;
	
	//whether this _waveLength should be switched on or off
	bool setOn = bgrWaveLengths->button (_waveLength)->isOn();
	
	
	//demark button
        if ((! multiPicViewer->isInMultipleView()) && (! multiPicViewer->isInOverlayMode()))
                if (bgrWaveLengths->button (positionManager->getWavelength()))
                        bgrWaveLengths->button (positionManager->getWavelength())->setPaletteBackgroundColor (ButtonColor);

        if (setOn) {
                //mark button in green
                if (bgrWaveLengths->button (_waveLength))
                        bgrWaveLengths->button (_waveLength)->setPaletteBackgroundColor (Qt::green);
        }
        else {
                if (multiPicViewer->isInMultipleView() || multiPicViewer->isInOverlayMode())
                        if (bgrWaveLengths->button (_waveLength))
                                bgrWaveLengths->button (_waveLength)->setPaletteBackgroundColor (ButtonColor);
        }
	
	setWaveLength (_waveLength, setOn, true);
}

//SLOT:
void TTTMovie::setCurrentWaveLength (int _index)
{
	if (! sender())
		return;
	
	if (_index == positionManager->getWavelength())
		return;
	
	if (_index > positionManager->getMaxAvailableWavelength())
		return;
	
        //unmark current wavelength button
        if (multiPicViewer->isInMultipleView())
		bgrWaveLengths->button (positionManager->getWavelength())->setPaletteBackgroundColor (ButtonColor);
	
	//mark new wavelength button in green
	bgrWaveLengths->button (_index)->setPaletteBackgroundColor (Qt::green);
	
//        //display current grahpic adjustment values for the new active wavelength
//	spbBrightness->setValue ((int)(positionManager->getDisplays().at (_index).Brightness * 100.0f));
//	spbContrast->setValue (positionManager->getDisplays().at (_index).Contrast);
	
	positionManager->setWavelength (_index);
}

//SLOT:
void TTTMovie::setMultipleMode (bool _on)
{
        if (_on == multiPicViewer->isInMultipleView())
		return;
	
	
	//if there are more wavelengths selected and multiple mode is switched off, the one with the current wavelength is kept
	
	bgrWaveLengths->setExclusive (! _on);

	//if the user changed from multiple to single mode, all button states have to be set to off
	//only the current is set to on
        if (! _on && multiPicViewer->isInMultipleView())
		for (int i = 0; i <= positionManager->getMaxAvailableWavelength(); i++) {
			if (bgrWaveLengths->button (i)) {
				bgrWaveLengths->button (i)->setPaletteBackgroundColor (ButtonColor);
				if (bgrWaveLengths->button (i)->isOn())
					bgrWaveLengths->button (i)->toggle();
			}
		}
	
        multiPicViewer->setMultipleMode (_on);
	
        if (! _on)
		if (bgrWaveLengths->button (positionManager->getWavelength()))
			if (! bgrWaveLengths->button (positionManager->getWavelength())->isOn())
				bgrWaveLengths->button (positionManager->getWavelength())->toggle();
	
	//switch orientation box on/off
        if (! _on)
                multiPicViewer->setOrientationBoxVisible (false);
	else
                multiPicViewer->setOrientationBoxVisible (chkShowOrientationBox->isOn());
}

//SLOT:
void TTTMovie::setOverlay (bool _on, bool _callShowPicture)
{
	//if there are more wavelengths selected and overlay mode is switched off, the one with the current wavelength is kept
//	if (! _on)
//		WaveLengthsSelected = 1;
	
	bgrWaveLengths->setExclusive (! _on);

	//if the user changed from overlay to non overlay mode, all button states have to be set to off
	//only the current is set to on
        if ((! _on) && multiPicViewer->isInOverlayMode())
		for (int i = 0; i <= positionManager->getMaxAvailableWavelength(); i++) {
			if (bgrWaveLengths->button (i)) {
				bgrWaveLengths->button (i)->setPaletteBackgroundColor (ButtonColor);
				if (bgrWaveLengths->button (i)->isOn())
					bgrWaveLengths->button (i)->toggle();
			}
		}
	
        if (! _on)
		if (bgrWaveLengths->button (positionManager->getWavelength()))
			if (! bgrWaveLengths->button (positionManager->getWavelength())->isOn())
				bgrWaveLengths->button (positionManager->getWavelength())->toggle();
	
	
        multiPicViewer->setOverlay (_on, _callShowPicture);

}

//SLOT:
void TTTMovie::continueLoading (bool )
{
/*	if (_start) {
		positionManager->getPictures()->loadPictures ();
		
		connect ( positionManager->getPictures(), SIGNAL (LoadingComplete()), pbtContinueLoading, SLOT (toggle()));
	}
	else {
		disconnect ( positionManager->getPictures(), SIGNAL (LoadingComplete()), pbtContinueLoading, 0);
		
		positionManager->getPictures()->stopLoading();
		//updateView();
	}*/
}

//SLOT:
void TTTMovie::setTrackSize (int _radius)
{
	//	TrackSize = _radius;
	multiPicViewer->setCircleMouseRadius (_radius);

	//update the cell size information box
	//contains: D (diameter), A (surface), V (volume)

	//note: the zoom needs to be included into the calculation
	int zoom = multiPicViewer->getCurrentZoom();
	_radius = (int)((float)_radius / ((float)zoom / 100.0f));

	lblCellSizeInfo->setText (QString ("D: %1 \nA: %2 \nV: %3")
		.arg (_radius * 2)
		.arg (_radius * _radius * Pi, 0, 'f', 0)
		.arg ((float)(_radius * _radius * _radius) * 4.0f / 3.0f * Pi, 0, 'f', 0));

	//	showPicture (positionManager->getTimepoint(), -1);
}

//SLOT:
void TTTMovie::selectTrack (Track *_track, bool _startTracking, bool _normalTrack)
{
        if (! _track)
		return;


        if (_normalTrack) {
                Process_TimePoint_Set = false;
                TTTManager::getInst().frmTracking->selectTrack (_track);
                Process_TimePoint_Set = true;

                pbtStartTracking->setEnabled (true);

                spbPathTPStart->setValue (_track->getFirstTrace());
                spbPathTPEnd->setValue (_track->getLastTrace());

                //update view (paint selected track in red)
                //showPicture (positionManager->getTimepoint(), -1);
                multiPicViewer->drawAdditions();

                if (_startTracking) {
                        //start tracking process for the just selected cell
                        TTTManager::getInst().frmTracking->handleCellDoubleClick (_track);
                }
        }
        else {
                //an external Matlab track was selected
                //store the cell number into the line edit of excluded track numbers in TTTTracking

                QString text = TTTManager::getInst().frmTracking->lieExcludedMatlabTracks->text();
                QString nr = QString ("%1;").arg (_track->getNumber());

                if (text.find (nr) < 0)
                        //only if the track number is not yet present, add it
                        TTTManager::getInst().frmTracking->lieExcludedMatlabTracks->setText (text + nr);
        }
}

////SLOT:
//void TTTMovie::resetGraphics()
//{
////        spbBrightness->setValue ((int) (positionManager->getDisplays().at (positionManager->getWavelength()).Brightness * 100.0f));
////        spbContrast->setValue (positionManager->getDisplays().at (positionManager->getWavelength()).Contrast);
//        multiPicViewer->resetGraphics();
//}

////SLOT:
//void TTTMovie::exportOptions()
//{
//        //
//}	


//SLOT:
void TTTMovie::setMovieTimeboxPosition (int _pos)
{
	if (TTTManager::getInst().frmTracking) {
		if (TTTManager::getInst().frmTracking->treeView) {
			StyleSheet *ss = & UserInfo::getInst().getRefStyleSheet();
                        //mapping: (left to right, top to bottom)
			//0 -> 0
			//1 -> 11
			//2 -> 12
			//3 -> 13
			//4 -> 21
			//...
			//9 -> 33
			
			int position = 0;
			if (_pos == 0)
				position = 0;
			else if (_pos < 4) 
				position = _pos + 10;
			else if (_pos < 7)
				position = _pos + 20 - 3;
			else if (_pos < 10)
				position = _pos + 30 - 6;
			
			if (ss)
				ss->setTimeboxPosition (position);
		}
	}
}

////SLOT:
//void TTTMovie::setShowOrientationBox (bool _show)
//{
//	//switch orientation box on/off
//	for (int i = 0; i < (int)PictureFrames.size(); i++)
//		if (PictureFrames.find (i))
//			PictureFrames.find (i)->setOrientationShow (_show);
//
//}

//SLOT:
void TTTMovie::displayMousePosition (QPointF _pos)
{
        //the coordinates are already global => no more transformations

	/*
	float mmpp = TATInformation::getInst()->getWavelengthInfo (0).getMicrometerPerPixel();
	lblMousePosition->setText (QString ("X: %1/Y: %2; um/pix=%3").arg (_pos.x()).arg (_pos.y()).arg (mmpp, 0, 'f', 3));
	*/

	mousePosX = _pos.x();
	mousePosY = _pos.y();
	showMousePos = true;
	updateStatusBarMessage();

	//@todo: show cross pointer in other wavelengths and other movie windows
	//...
	
}

//SLOT:
void TTTMovie::wrapTrackPointSet (Track *_track, int _index)
{
	/*
	CellProperties props;
	props.EndoMitosis = EndoMitosis_Selected;
	*/

	// Disable endomitosis
	if (pbtEndoMitosis->isOn()) {
		pbtEndoMitosis->toggle();

		TTTManager::getInst().setTrackingEndomitosis(false);
	}

	/*
	if (TTTTracking* frmTmp = TTTManager::getInst().frmTracking)
		frmTmp->addCellProperties (_track, _index, props);
	*/
}

//SLOT:
void TTTMovie::setEditCellBackground (bool _on)	
{
	if (_on)
		pbtEditBackground->setPaletteBackgroundColor (Qt::red);
	else
		pbtEditBackground->setPaletteBackgroundColor (ButtonColor);

        multiPicViewer->setBackgroundTracking (_on);
}

//SLOT:
void TTTMovie::changeCellSize (int _add)
{
	//set new size - all updates are performed automatically
	spbTrackSize->setValue (spbTrackSize->value() + _add);
}

//SLOT:
void TTTMovie::radiusSelected (int _radius)
{
	emit RadiusSelected (_radius);
}

//SLOT:
void TTTMovie::showSymbolForm()
{
	if (! TTTManager::getInst().frmSymbolSelection)
		return;
	
	//set timepoints
	TTTManager::getInst().frmSymbolSelection->setTimePoints (positionManager->getTimepoint(), positionManager->getTimepoint());
	
	TTTManager::getInst().frmSymbolSelection->show();
	TTTManager::getInst().frmSymbolSelection->raise();
}
	
//SLOT:
void TTTMovie::addSymbol (Symbol _symbol)
{
        _symbol.setPosition (positionManager->getSymbolHandler().getCurrentSymbol().getPosition());
        _symbol.setIndex (positionManager->getSymbolHandler().createNewIndex());
        positionManager->getSymbolHandler().addSymbol (_symbol, positionManager->getWavelength());
}

//SLOT:
void TTTMovie::deleteSymbols()
{
        positionManager->getSymbolHandler().deleteAll();
	
        multiPicViewer->drawAdditions();
}

//SLOT:
void TTTMovie::setEndoMitosis (bool _on)
{
	/*
	EndoMitosis_Selected = _on;
	
	if (TTTManager::getInst().isTracking() & _on) {
		CellProperties props;
		props.EndoMitosis = EndoMitosis_Selected;
		
		if (TTTManager::getInst().getCurrentTrack())
			if (positionManager->getTimepoint() > 0)
				if (TTTManager::getInst().frmTracking) {
					
					if (TTTManager::getInst().frmTracking->addCellProperties (TTTManager::getInst().getCurrentTrack(), positionManager->getTimepoint(), props)) {
						if (pbtEndoMitosis->isOn())
							pbtEndoMitosis->toggle();
					}
				}
	}
	*/

	TTTManager::getInst().setTrackingEndomitosis(_on);
}

//SLOT:
void TTTMovie::showPropertyForm()
{
	if (TTTManager::getInst().frmCellProperties)
		TTTManager::getInst().frmCellProperties->show();
}

//SLOT:
void TTTMovie::processPictureKey (int _key, bool _shift, bool _ctrl)
{
	//other keys
	processKey (_key, _shift, _ctrl);
}

//SLOT:
void TTTMovie::hideShowControls()
{
	// Get current size
	QSize picSize = size();

	//hide / unhide controls
	if (controlsHidden) {
		//unhide controls

		menuBar->actView_HIDECONTROLS->setText ("Hide controls");

		tobMovie->show();

		//bgrZoom->show();
		statusBar->show();
		chkShowTracks->show();
		chkShowTrackNumbers->show();
		chkShowWavelengthBox->show();
		chkTrackVisibilityAllWavelengths->show();
		grbView->show();
		grbTracking->show();

		//lblMousePosition->show(); // ------------------- Konstantin ----------------
		//lblCurrentIndex->show(); // ------------------- Konstantin ----------------
		//lblCurrentTimePoint->show(); // ------------------- Konstantin ----------------

		pbtGamma->show();
	}
	else {
		//hide controls

		menuBar->actView_HIDECONTROLS->setText ("Show controls");

		tobMovie->hide();

		//bgrZoom->hide();
		statusBar->hide();
		chkShowTracks->hide();
		chkShowTrackNumbers->hide();
		chkShowWavelengthBox->hide();
		chkTrackVisibilityAllWavelengths->hide();
		grbView->hide();
		grbTracking->hide();

		//lblMousePosition->hide(); // ------------------- Konstantin ----------------
		//lblCurrentIndex->hide(); // ------------------- Konstantin ----------------
		//lblCurrentTimePoint->hide(); // ------------------- Konstantin ----------------

		pbtGamma->hide();
	}

	controlsHidden = !controlsHidden;

	// Apply changes and tell picture area to resize
	QApplication::processEvents();
	multiPicViewer->resize();
}

//SLOT:
void TTTMovie::applyStyleSheetMovie (StyleSheet &_ss)
{
	if (TTTManager::getInst().frmTracking) {
                multiPicViewer->applyStyleSheet (_ss);
	}
}

void TTTMovie::runSpecialExport (TTTSpecialExport *exportWindow, int sizeX, int sizeY, bool timeBoxIntoCsvFile )
{
	if (! positionManager)
		return;
	if (! positionManager->getFiles())
		return;
	
	QString usersign = UserInfo::getInst().getUsersign();
	if (usersign.isEmpty())
		return;

	//if (QMessageBox::question (this, "Special export", "Run special export?", QMessageBox::Yes | QMessageBox::No) != QMessageBox::Yes)
	//	return;

	QString date = QDateTime::currentDateTime().toString ("yyyyMMdd_hhmmss");

	//BS 2010/03/22 nas folder has to exist, is checked at ttt startup
	QString folder = TTTManager::getInst().getNASDrive() + MOVIE_EXPORT_FOLDER;
	folder += "/" + positionManager->getBasename() + usersign;
	folder += "/" + date;
	folder += "/";
	SystemInfo::checkNcreateDirectory (folder, true, true);

	//switch off the orientation box, as it would disturb true grayscale pictures
	multiPicViewer->setOrientationBoxVisible(false);

	chkTrackVisibilityAllWavelengths->setChecked (true);
	//setMovieTimeboxPosition (spbTimeboxPosition->value());

	bool drawWavelengthIndex = UserInfo::getInst().getStyleSheet().getWavelengthVisibility();
	//drawWavelengthIndex = true;
	//int RelativeTimePoint = spbExportTPRelative->value();

	int width = fraPictures->width();
	int height = fraPictures->height();
	QPixmap pix (width, height);
	QPixmap pix2 (width, height);
	//draw the tree left of the movie
	QPixmap union_pic (width * 2 + 2, height);



	//run export
	//==========

	//the user can specify display wishes within a file, which is located in the configuration folder
	//read these dates from a file
	//file format:
	//tp 1;tp 2;tp interval;stack repeat;visible wl's;center x/y;zoom factor %(-1=full picture);tree visible;tracks:first/all;blackpoint/whitepoint;gammaanchors1/2/3;overlay:0/1/nothing=0

	QString filename = UserInfo::getInst().getUserConfigDirectory() + "special_export.txt";
	
	// Check if file exists
	if(!QFile::exists(filename)) {
		QMessageBox::information(this, "Error", QString("Cannot find input file (%1)").arg(filename));
		return;
	}

	// Open file
	QFile file (filename);
	if (! file.open (QIODevice::ReadOnly)) {
		QMessageBox::information(this, "Error", QString("Cannot open input file (%1)").arg(filename));
		return;
	}

	multiPicViewer->showHideScrollbars(true);
	multiPicViewer->setHideOutOfSyncBox(true);
	if(!controlsHidden)
		hideShowControls();

	qApp->processEvents();

	if(sizeX != -1 && sizeY != -1)
		setPictureSize(sizeX, sizeY);


    QTextStream ts (&file);
	QString line;

	QPointF oldCenter;
	int oldZoomFactor = multiPicViewer->getCurrentZoom();

	TTTManager::getInst().frmPositionLayout->hide();
	TTTBlank *tttb = new TTTBlank (this);

	// Open timebox file
	QString timeBoxFileName;
	timeBoxFileName = folder + "timebox.csv";
	QFile timeBoxFile(timeBoxFileName);
	if(timeBoxIntoCsvFile) {
		if(!timeBoxFile.open(QIODevice::WriteOnly))
			QMessageBox::critical(this, "Error", "Cannot open timebox text file for writing.");
	}
	QTextStream timeBox(&timeBoxFile);

	//read file format
	line = ts.readLine();
	QString format = line.stripWhiteSpace();
	QString extension = "";
	if (format == "JPEG")
		extension = ".jpg";
	else if (format == "BMP")
		extension = ".bmp";
	else if (format == "PNG")
		extension = ".png";
	else {
		format = "BMP";
		extension = ".bmp";
	}

	int lineCounter = 0,
		exportCount = 0;

	// Parse rest of the file
	int RelativeTimePoint = -1;
	while (! ts.atEnd()) {
		line = ts.readLine();

		if (line.isEmpty())
			continue;

		++lineCounter;

		/**
		 * Get settings and load images if needed.
		 */

		QStringList entries = line.trimmed().split (";");
		int tp1 = entries [0].toInt();
		if(RelativeTimePoint < 0)
			RelativeTimePoint = tp1;
		int tp2 = entries [1].toInt();
		int picture_interval = entries [2].stripWhiteSpace().toInt();

		exportWindow->setStatusText("Loading pictures..");
		qApp->processEvents();

		//check if all necessary pictures are loaded between these timepoints
		//if not, load them automatically; if the memory is full, unload previous ones
		if(tp2 > tp1)
			positionManager->getPictures()->loadInterval (tp1, tp2, -1, 1, picture_interval, true);
		else
			positionManager->getPictures()->loadInterval (tp2, tp1, -1, 1, picture_interval, true);

		int repeats = entries [3].stripWhiteSpace().toInt();
		QString wavelengths = entries [4].stripWhiteSpace();

		// Center pos or follow cell
		QString posS = entries [5].stripWhiteSpace();
		QPointF center;
		bool followCell = false;
		int cellToFollow = 0;
		if (posS.left (1) == "f") {
			cellToFollow = posS.mid (1).toInt();
			followCell = true;
		}
		else {
			QStringList centerPos = QStringList::split ("/", posS);
			center = QPointF (centerPos [0].toInt(), centerPos [1].stripWhiteSpace().toInt());
		}

		// Zoom
		int zoomFactor = entries [6].stripWhiteSpace().toInt();

		// Show tree
		bool treeVisible = false;
		if (entries.count() >= 8)
			treeVisible = entries [7].stripWhiteSpace().toInt() == 1 ? true : false;


		//BS: new on 2009/01/28
		//tracks:first/all;blackpoint/whitepoint;gammaanchors1/2/3;

		//tracksVisible: 0 = normal tracks / 1 = all tracks at tp / 2 = first tracks / 3 = none
		int tracksVisible = 0;
		if (entries.count() >= 9)
			tracksVisible = entries [8].stripWhiteSpace().toInt();

		// Position layout
		int posLayout = 0;
		if(entries.count() >= 10)
			posLayout = entries [9].stripWhiteSpace().toInt();

		//if(posLayout > 0) {
		//	// Update current zoom
		//	oldZoomFactor = TTTManager::getInst().frmPositionLayout->positionView->getCurrentScaling() * 100;
		//}

		int blackpoint = -1, whitepoint = -1;
		if (entries.count() >= 11) {
			QString tmp = entries [10].stripWhiteSpace();
			QStringList bpwp = QStringList::split ("/", tmp);
			blackpoint = bpwp [0].stripWhiteSpace().toInt();
			whitepoint = bpwp [1].stripWhiteSpace().toInt();
		}

		int gammaYValues[GAMMA_ANCHOR_POINTS] = {-1, -1, -1};
		if (entries.count() >= 12) {
			QString tmp = entries [11].stripWhiteSpace();
			QStringList gammas = QStringList::split ("/", tmp);
			for (int i = 0; i < GAMMA_ANCHOR_POINTS; i++)
				gammaYValues [i] = gammas [i].stripWhiteSpace().toInt();
		}

		bool overlay = false;
		if (entries.count() >= 13)
			overlay = entries [12].stripWhiteSpace().toInt() == 1 ? true : false;

		bool fullPicture = false;

		QStringList wls;
		if (! wavelengths.isEmpty()) {
			//hide all wavelengths
			for (int i = 0; i <= MAX_WAVE_LENGTH; i++) {
				setWaveLength (0, false, false);
			}

			//show desired ones
			wls = QStringList::split (",", wavelengths);

			bool multiple = wls.count() > 1;
			setMultipleMode (multiple);

			for ( QStringList::Iterator iter = wls.begin(); iter != wls.end(); ++iter) {
				int wl = (*iter).toInt();
				setWaveLength (wl, true, false);
			}
		}
		if (overlay != multiPicViewer->isInOverlayMode()) {
			 multiPicViewer->setOverlay (overlay, false);
		}
		//wait 2 seconds until the wavelength layout and stuff is completely loaded
		tttb->exec();



		float tp_count = (float)positionManager->getFiles()->getExistingTimepointsBetween (tp1, tp2) / (float)picture_interval;

		//get to the desired location in no more than 15 steps
                tp_count = QMIN ((int)tp_count, 15);

		if (zoomFactor == 0) {
			//full picture -> take center of the complete picture
			center = positionManager->getDisplays().at (positionManager->getWavelength()).calcPictureCenter (true);
			fullPicture = true;
		}

		if (oldCenter.isNull())
			oldCenter = center;

		if (gammaYValues [0] > -1) {
			for (int i = 0; i < GAMMA_ANCHOR_POINTS; i++)
				positionManager->getDisplays().at (positionManager->getWavelength()).GammaAnchor[i][1] = gammaYValues [i];
		}

		//if (blackpoint > -1 && whitepoint > -1) {
		//	setBlackWhitePoint (blackpoint, whitepoint);
		//}

		float zoom_add = ((float)zoomFactor - (float)oldZoomFactor) / (tp_count - 1);
		float x_add = ((float)center.x() - (float)oldCenter.x()) / (tp_count - 1);
		float y_add = ((float)center.y() - (float)oldCenter.y()) / (tp_count - 1);

		QPointF pos (oldCenter);
		int zoom = oldZoomFactor;
		int cc = 0;

		
		int tp = tp1;
		int step;
		bool bFinished = false;
		
		// Run forward or backward?
		if(tp2 > tp1)
			step = picture_interval;
		else
			step = -picture_interval;

		//for (int tp = tp1; tp <= tp2; tp += picture_interval) {
		do {
			exportWindow->setStatusText(QString("Exporting frame %1 of %2 in item %3").arg(tp - tp1).arg(tp2 - tp1).arg(lineCounter));
			QApplication::processEvents();

			//run from timepoint 1 to timepoint 2 in picture steps and modify the x/y location and zoom as desired

			if (positionManager->getFiles()->exists (tp, 1, 0)) {
				cc++;

				if (cc < tp_count) {
					zoom = (int)((float)zoom + zoom_add);
				}

				if (followCell) {
					if (TTTManager::getInst().getTree()) {

						Track *track = TTTManager::getInst().getTree()->getTrack (cellToFollow);
						if (track) {
							TrackPoint t_p = track->getTrackPoint (tp);
							if (t_p.X != -1.0f) {
								pos.setX (t_p.X);
								pos.setY (t_p.Y);
								center = pos;
							}
						}
					}
				}
				else {
					if (cc < tp_count) {
						pos.setX ((int)((float)pos.x() + x_add));
						pos.setY ((int)((float)pos.y() + y_add));
					}
				}

				//std::cout << pos.x() << "/" << pos.y() << "; " << zoom << "; zF = " << zoomFactor << std::endl;

				
				if (fullPicture) {
					setPictureDisplay (tp, pos, 0,  tracksVisible);

					////calculate the real zoom factor
					//zoomFactor = (int)(positionManager->getDisplays().at (positionManager->getWavelength()).ZoomX * 100.0f);
				}
				else
					setPictureDisplay (tp, pos, zoom,  tracksVisible);

				if(posLayout == 0) {
					//grab the display with everything on and in it
					//fraPictures contains all PictureView-instances currently displayed, so all wavelengths are grabbed
					pix = QPixmap::grabWidget (fraPictures);
				}
				else {
					// Take picture from position layout to see all positions nicely aligned
					// ToDo: All this is currently a bit ugly (changing TTTMovie settings and then take image from position layout etc.)
					TTTPositionLayout* frmPosLayout = TTTManager::getInst().frmPositionLayout;

					// Change timepoint, wavelength
					int wl = 0;
					if(!wls.isEmpty())
						wl = wls[0].toInt();
					frmPosLayout->positionView->setThumbnailsTimePoint(tp);
					frmPosLayout->positionView->setThumbnailsWavelength(wl);
					frmPosLayout->positionView->initialize(false, true, true);

					// Change zoom
					float desiredScaling = zoom / 100.0f;
					float curScaling = frmPosLayout->positionView->getCurrentScaling();
					if(curScaling > 0 && desiredScaling > 0)
						frmPosLayout->positionView->scaleView(desiredScaling / curScaling);

					// Change position
					QPointF newPosScene = frmPosLayout->positionView->convertGlobalToLocal(pos);
					frmPosLayout->positionView->centerOn(newPosScene);

					// Hide scrollbars
					frmPosLayout->positionView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
					frmPosLayout->positionView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

					// Process events
					QApplication::processEvents();

					// Grab pos layout
					pix = QPixmap::grabWidget(frmPosLayout->positionView);

					// Unhide scrollbars
					frmPosLayout->positionView->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
					frmPosLayout->positionView->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
				}

				if(!timeBoxIntoCsvFile)
					printMovieExportTime (RelativeTimePoint, positionManager->getTimepoint(), &pix);
				else
					timeBox << getMovieExportTimeString(RelativeTimePoint, positionManager->getTimepoint()) << "\n";

				if (treeVisible) {
					//grab tree (until the current timepoint)
					pix2.convertFromImage (TTTManager::getInst().frmTracking->treeView->getTreeImage (-1, tp)->smoothScale (width, height));

					//merge both pictures in one big slide
					QPainter p (&union_pic);

					p.drawPixmap (0, 0, pix2);
					p.drawPixmap (width + 2, 0, pix);
					p.setPen (QPen (Qt::black, 2));
					p.drawLine (width, 0, width, height);

					p.end();

					//save the picture as often as desired
					int appendix = 0;
					QString picture_filename = "";
					for (int rep = 1; rep <= repeats; rep++) {
						appendix = rep;

						//test if a picture with this name already exists - if yes, increase repetition number
						picture_filename = folder + QString().sprintf (positionManager->getBasename() + "_%06d%04d" + extension, positionManager->getTimepoint(), appendix);
						while (QFile::exists (picture_filename)) {
							appendix++;
							picture_filename = folder + QString().sprintf (positionManager->getBasename() + "_%06d%04d" + extension, positionManager->getTimepoint(), appendix);
						}

						union_pic.save (picture_filename, format);
					}
					//union_pic.save (folder + QString().sprintf ("%05d.bmp", positionManager->getTimepoint()), "BMP");
				}
				else {
					//save only the movie as often as desired
					//int appendix = 0;
					QString picture_filename = "";
					for (int rep = 1; rep <= repeats; rep++) {
						//appendix = rep;

						//test if a picture with this name already exists - if yes, increase repetition number
						picture_filename = folder + QString().sprintf (positionManager->getBasename() + "_%06d" + extension, exportCount);
						//while (QFile::exists (picture_filename)) {
						//	appendix++;
						//	picture_filename = folder + QString().sprintf (positionManager->getBasename() + "_%06d%04d" + extension, exportCount, appendix);
						//}

						pix.save (picture_filename, format);
						exportCount++;
					}

				}


			}
			tp += step;

			// Check if finished
			if(tp1 < tp2)
				bFinished = tp > tp2;
			else
				bFinished = tp < tp2;

		} while(!bFinished);

		oldCenter = center;
		oldZoomFactor = zoomFactor;
	}

	positionManager->getPictures()->unloadAllPictures();

	TTTManager::getInst().frmPositionLayout->show();

	multiPicViewer->showHideScrollbars(false);
	multiPicViewer->setHideOutOfSyncBox(false);
	if(controlsHidden)
		hideShowControls();

	exportWindow->setStatusText(QString("Export completed to: %1").arg(folder));

	// Display message box
	Tools::displayMessageBoxWithOpenFolder("Export completed.", "", folder, false, exportWindow);
}

////SLOT:
//void TTTMovie::runSpecialExport_OLD (bool _start)
//{
////@todo
//        if (! _start)
//		return;
//	
//	if (! positionManager)
//		return;
//	if (! positionManager->getFiles())
//		return;
//	
//	QString usersign = UserInfo::getInst().getUsersign();
//	if (usersign.isEmpty())
//		return;
//
//	if (QMessageBox::question (this, "Special movie export", "Is the screen-saver and monitor powersave really switched off?", "Yes", "No", QString::null, 0, 1) == 1)
//		//screen saver not switched off
//		return;
////*	if (QMessageBox::question (this, "Special movie export", "Are all necessary pictures (note wavelengths!) loaded?", "Yes", "No", QString::null, 0, 1) == 1)
////		//not all pictures loaded
////		return;*/
////
////*	setPictureDisplay (960, QPointF(), 100, "0,1", false);
////	return;*/
//
//
//	QString date = QDateTime::currentDateTime().toString ("yyyyMMdd_hhmmss");
//
//	//BS 2010/03/22 nas folder has to exist, is checked at ttt startup
//	QString folder = TTTManager::getInst().getNASDrive() + MOVIE_EXPORT_FOLDER;
//	folder += "/" + positionManager->getBasename() + usersign;
//	folder += "/" + date;
//	folder += "/";
//	SystemInfo::checkNcreateDirectory (folder, true, true);
//
//	//switch off the orientation box, as it would disturb true grayscale pictures
//	multiPicViewer->setOrientationBoxVisible(false);
//
//	chkTrackVisibilityAllWavelengths->setChecked (true);
//	setMovieTimeboxPosition (spbTimeboxPosition->value());
//
//	bool drawWavelengthIndex = UserInfo::getInst().getStyleSheet().getWavelengthVisibility();
//	//drawWavelengthIndex = true;
//	int RelativeTimePoint = spbExportTPRelative->value();
//
//	int width = fraPictures->width();
//	int height = fraPictures->height();
//	QPixmap pix (width, height);
//	QPixmap pix2 (width, height);
//	//draw the tree left of the movie
//	QPixmap union_pic (width * 2 + 2, height);
//
//
//
//	//run export
//	//==========
//
//	//the user can specify display wishes within a file, which is located in the configuration folder
//	//read these dates from a file
//	//file format:
//	//tp 1;tp 2;tp interval;stack repeat;visible wl's;center x/y;zoom factor %(-1=full picture);tree visible;tracks:first/all;blackpoint/whitepoint;gammaanchors1/2/3;overlay:0/1/nothing=0
//
//	QString filename = UserInfo::getInst().getUserConfigDirectory() + "special_export.txt";
//	//QString filename = "/home/bernhard/gsf/special_export.txt";
//	QFile file (filename);
//	if (! file.open (QIODevice::ReadOnly))
//		return;
//
//        QTextStream ts (&file);
//
//	QString line;
//
//	QPointF oldCenter;
//	int oldZoomFactor = multiPicViewer->getCurrentZoom();
//
//	TTTManager::getInst().frmPositionLayout->hide();
//	TTTBlank *tttb = new TTTBlank (this);
//
//
//	//read file format
//	line = ts.readLine();
//	QString format = line.stripWhiteSpace();
//	QString extension = "";
//	if (format == "JPEG")
//		extension = ".jpg";
//	else if (format == "BMP")
//		extension = ".bmp";
//	else if (format == "PNG")
//		extension = ".png";
//	else {
//		format = "BMP";
//		extension = ".bmp";
//	}
//
//
//	while (! ts.atEnd()) {
//		line = ts.readLine();
//
//		if (line.isEmpty())
//			continue;
//
//		QStringList entries = QStringList::split (";", line.stripWhiteSpace());
//
//		int tp1 = entries [0].toInt();
//		int tp2 = entries [1].toInt();
//		int picture_interval = entries [2].stripWhiteSpace().toInt();
//
//		//check if all necessary pictures are loaded between these timepoints
//		//if not, load them automatically; if the memory is full, unload previous ones
//		positionManager->getPictures()->loadInterval (tp1, tp2, picture_interval, true);
//
//		int repeats = entries [3].stripWhiteSpace().toInt();
//		QString wavelengths = entries [4].stripWhiteSpace();
//		QString posS = entries [5].stripWhiteSpace();
//		QPointF center;
//		bool followCell = false;
//		int cellToFollow = 0;
//		if (posS.left (1) == "f") {
//			cellToFollow = posS.mid (1).toInt();
//			followCell = true;
//		}
//		else {
//			QStringList centerPos = QStringList::split ("/", posS);
//			center = QPointF (centerPos [0].toInt(), centerPos [1].stripWhiteSpace().toInt());
//		}
//		int zoomFactor = entries [6].stripWhiteSpace().toInt();
//
//		bool treeVisible = false;
//		if (entries.count() >= 8)
//			treeVisible = entries [7].stripWhiteSpace().toInt() == 1 ? true : false;
//
//
//		//BS: new on 2009/01/28
//		//tracks:first/all;blackpoint/whitepoint;gammaanchors1/2/3;
//
//		//tracksVisible: 0 = normal tracks / 1 = all tracks at tp / 2 = first tracks / 3 = none
//		int tracksVisible = 0;
//		if (entries.count() >= 9)
//			tracksVisible = entries [8].stripWhiteSpace().toInt();
//
//		int blackpoint = -1, whitepoint = -1;
//		if (entries.count() >= 10) {
//			QString tmp = entries [9].stripWhiteSpace();
//			QStringList bpwp = QStringList::split ("/", tmp);
//			blackpoint = bpwp [0].stripWhiteSpace().toInt();
//			whitepoint = bpwp [1].stripWhiteSpace().toInt();
//		}
//
//		int gammaYValues[GAMMA_ANCHOR_POINTS] = {-1, -1, -1};
//		if (entries.count() >= 11) {
//			QString tmp = entries [10].stripWhiteSpace();
//			QStringList gammas = QStringList::split ("/", tmp);
//			for (int i = 0; i < GAMMA_ANCHOR_POINTS; i++)
//				gammaYValues [i] = gammas [i].stripWhiteSpace().toInt();
//		}
//
//		bool overlay = false;
//		if (entries.count() >= 12)
//			overlay = entries [11].stripWhiteSpace().toInt() == 1 ? true : false;
//
//		bool fullPicture = false;
//
//		if (! wavelengths.isEmpty()) {
//			//hide all wavelengths
//			for (int i = 0; i <= MAX_WAVE_LENGTH; i++) {
//				setWaveLength (0, false, false);
//			}
//
//			//show desired ones
//			QStringList wls = QStringList::split (",", wavelengths);
//
//			bool multiple = wls.count() > 1;
//			setMultipleMode (multiple);
//
//			for ( QStringList::Iterator iter = wls.begin(); iter != wls.end(); ++iter) {
//				int wl = (*iter).toInt();
//				setWaveLength (wl, true, false);
//			}
//		}
//		if (overlay != multiPicViewer->isInOverlayMode()) {
//			 multiPicViewer->setOverlay (overlay, false);
//		}
//		//wait 2 seconds until the wavelength layout and stuff is completely loaded
//		tttb->exec();
//
//
//
//		float tp_count = (float)positionManager->getFiles()->getExistingTimepointsBetween (tp1, tp2) / (float)picture_interval;
//
//		//get to the desired location in no more than 15 steps
//                tp_count = QMIN ((int)tp_count, 15);
//
//		if (zoomFactor == 0) {
//			//full picture -> take center of the complete picture
//			center = positionManager->getDisplays().at (positionManager->getWavelength()).calcPictureCenter (true);
//			fullPicture = true;
//		}
//
//		if (oldCenter.isNull())
//			oldCenter = center;
//
//		if (gammaYValues [0] > -1) {
//			for (int i = 0; i < GAMMA_ANCHOR_POINTS; i++)
//				positionManager->getDisplays().at (positionManager->getWavelength()).GammaAnchor[i][1] = gammaYValues [i];
//		}
//
//		//if (blackpoint > -1 && whitepoint > -1) {
//		//	setBlackWhitePoint (blackpoint, whitepoint);
//		//}
//
//		float zoom_add = ((float)zoomFactor - (float)oldZoomFactor) / (tp_count - 1);
//		float x_add = ((float)center.x() - (float)oldCenter.x()) / (tp_count - 1);
//		float y_add = ((float)center.y() - (float)oldCenter.y()) / (tp_count - 1);
//
//		QPointF pos (oldCenter);
//		int zoom = oldZoomFactor;
//		int cc = 0;
//
//		for (int tp = tp1; tp <= tp2; tp += picture_interval) {
//			//run from timepoint 1 to timepoint 2 in picture steps and modify the x/y location and zoom as desired
//
//			if (positionManager->getFiles()->exists (tp, 0)) {
//				cc++;
//
//				if (cc < tp_count) {
//					zoom = (int)((float)zoom + zoom_add);
//				}
//
//				if (followCell) {
//					if (TTTManager::getInst().getTree()) {
//
//						Track *track = TTTManager::getInst().getTree()->getTrack (cellToFollow);
//						if (track) {
//							TrackPoint t_p = track->getTrackPoint (tp);
//							if (t_p.X != -1.0f) {
//								pos.setX (t_p.X);
//								pos.setY (t_p.Y);
//								center = pos;
//							}
//						}
//					}
//				}
//				else {
//					if (cc < tp_count) {
//						pos.setX ((int)((float)pos.x() + x_add));
//						pos.setY ((int)((float)pos.y() + y_add));
//					}
//				}
//
//				//std::cout << pos.x() << "/" << pos.y() << "; " << zoom << "; zF = " << zoomFactor << std::endl;
//
//				if (fullPicture) {
//					setPictureDisplay (tp, pos, 0, "", overlay, tracksVisible);
//
//					////calculate the real zoom factor
//					//zoomFactor = (int)(positionManager->getDisplays().at (positionManager->getWavelength()).ZoomX * 100.0f);
//				}
//				else
//					setPictureDisplay (tp, pos, zoom, "", overlay, tracksVisible);
//
//
//				//grab the display with everything on and in it
//				//fraPictures contains all PictureView-instances currently displayed, so all wavelengths are grabbed
//				pix = QPixmap::grabWidget (fraPictures);
//
//				printMovieExportTime (RelativeTimePoint, positionManager->getTimepoint(), &pix);
//
//				if (treeVisible) {
//					//grab tree (until the current timepoint)
//					pix2.convertFromImage (TTTManager::getInst().frmTracking->TreeView->getTreeImage (-1, tp)->smoothScale (width, height));
//
//					//merge both pictures in one big slide
//					QPainter p (&union_pic);
//
//					p.drawPixmap (0, 0, pix2);
//					p.drawPixmap (width + 2, 0, pix);
//					p.setPen (QPen (Qt::black, 2));
//					p.drawLine (width, 0, width, height);
//
//					p.end();
//
//					//save the picture as often as desired
//					int appendix = 0;
//					QString picture_filename = "";
//					for (int rep = 1; rep <= repeats; rep++) {
//						appendix = rep;
//
//						//test if a picture with this name already exists - if yes, increase repetition number
//						picture_filename = folder + QString().sprintf (positionManager->getBasename() + "_%06d%04d" + extension, positionManager->getTimepoint(), appendix);
//						while (QFile::exists (picture_filename)) {
//							appendix++;
//							picture_filename = folder + QString().sprintf (positionManager->getBasename() + "_%06d%04d" + extension, positionManager->getTimepoint(), appendix);
//						}
//
//						union_pic.save (picture_filename, format);
//					}
//					//union_pic.save (folder + QString().sprintf ("%05d.bmp", positionManager->getTimepoint()), "BMP");
//				}
//				else {
//					//save only the movie as often as desired
//					int appendix = 0;
//					QString picture_filename = "";
//					for (int rep = 1; rep <= repeats; rep++) {
//						appendix = rep;
//
//						//test if a picture with this name already exists - if yes, increase repetition number
//						picture_filename = folder + QString().sprintf (positionManager->getBasename() + "_%06d%04d" + extension, positionManager->getTimepoint(), appendix);
//						while (QFile::exists (picture_filename)) {
//							appendix++;
//							picture_filename = folder + QString().sprintf (positionManager->getBasename() + "_%06d%04d" + extension, positionManager->getTimepoint(), appendix);
//						}
//
//						pix.save (picture_filename, format);
//					}
//
//				}
//
//
//			}
//		}
//
//		oldCenter = center;
//		oldZoomFactor = zoomFactor;
//	}
//
//	positionManager->getPictures()->unloadAllPictures();
//
//	TTTManager::getInst().frmPositionLayout->show();
//}

void TTTMovie::setPictureDisplay (int _timepoint, QPointF _pos, int _zoom, /*QString _wavelengths, bool _overlay,*/ int _firstAllTracks)
{
//@todo: only necessary for exports
	//TTTManager::getInst().setTimepoint_withoutSignal (_timepoint);
	TTTManager::getInst().setTimepoint (_timepoint);

	//if (! _wavelengths.isEmpty()) {
//		//@todo hide other wavelengths!
//
//		QStringList wls = QStringList::split (",", _wavelengths);
//
//		bool multiple = wls.count() > 1;
//
//		setMultipleMode (multiple);
//
//		for ( QStringList::Iterator iter = wls.begin(); iter != wls.end(); ++iter) {
//			int wl = (*iter).toInt();
//			setWaveLength (wl, true, false);
//		}
//
//
//	}

	//if (_overlay != Overlay) {
	//	setOverlay (_overlay, false);
	//}

	////display all/first tracks, if necessary
	//if (_firstAllTracks >= 1 && _firstAllTracks <= 2) {
	//	bool firstTracks = (_firstAllTracks == 1);

	//	positionManager->readAllTrackPoints (positionManager->getTTTFileDirectory(), firstTracks, positionManager->getTimepoint(), true);
	//	positionManager->readAllTrackPoints (positionManager->getTTTFileDirectory (false, true), firstTracks, positionManager->getTimepoint(), true, true);
	//}
	//else
	//	positionManager->getAllTrackPoints().clear();

	//if(_firstAllTracks != -1) {
	//	if (_firstAllTracks == 3)
	//		//display no tracks at all
	//		setShowTracks (false);
	//	else
	//		setShowTracks (true);
	//}

	setZoom (_zoom);	//showPicture() indirectly called here

	if (_zoom != 0)
		multiPicViewer->centerPosition (_pos);
//	else
//		showPicture();
}

//SLOT:
void TTTMovie::addChildrenDistanceLine()
{
	//measure the distances of the current cell's children over their lifetime
	//for each timepoint, one line is added
	
	if (! TTTManager::getInst().frmTextbox)
		return;
	
	if (TTTManager::getInst().frmTextbox->getUsage() == TFUNone) {
		TTTManager::getInst().frmTextbox->setUsage (TFUChildrenDistances);
		TTTManager::getInst().frmTextbox->setCaption ("Children distances");
		
		//add header line to textbox
		QString line = "Tree name;Cell numbers;Timepoint;Seconds;Distances";
		TTTManager::getInst().frmTextbox->addLine (line);
		
		TTTManager::getInst().frmTextbox->show();
	}
	
	Track *track = TTTManager::getInst().getCurrentTrack();
	
	if (! track)
		return;
	
	if (! track->hasChildren())
		return;
	
	Track *kid1 = track->getChildTrack (1);
	Track *kid2 = track->getChildTrack (2);
	
	if ((! kid1->tracked()) || (! kid2->tracked()))
		return;
	
	
	//now we know that both children exist and are really tracked
	//thus we add one line for each timepoint until the first cell of the two either divides or dies or whatever
	
	
	//get tree file name
	QString filename = TTTManager::getInst().getTree()->getFilename();
	filename = filename.mid (filename.findRev ("/") + 1);		//trim to filename without path
	filename = filename.left (filename.length() - TTT_FILE_SUFFIX.length());		//cut off suffix .ttt
	
	QString cellNumbers = QString ("%1/%2").arg (kid1->getNumber()).arg (kid2->getNumber());
	
	uint startTP = kid1->getFirstTrace();
	uint endTP = QMIN (kid1->getLastTrace(), kid2->getLastTrace());
	
	for (uint timepoint = startTP; timepoint <= endTP; timepoint++) {
		TrackPoint tp1 = kid1->getTrackPoint (timepoint);
		TrackPoint tp2 = kid2->getTrackPoint (timepoint);
		
		if (tp1.TimePoint > -1 && tp2.TimePoint > -1) {
			QString line = filename + ";";
			
			line += cellNumbers + ";";
			line += QString ("%1;").arg (timepoint);
			uint seconds = TTTManager::getInst().getBasePositionManager()->getFiles()->calcSeconds (startTP, timepoint);
			line += QString ("%1;").arg (seconds);
			
			float distance = tp1.distanceTo (tp2);
			line += QString ("%1;").arg (distance);
			
			TTTManager::getInst().frmTextbox->addLine (line);
		}
	}
}

//SLOT:
void TTTMovie::showTracksOverview()
{
	if (! TTTManager::getInst().frmTracksOverview)
		return;			//something went terribly wrong...
	
	//load default data -> @todo: user setting
	TTTManager::getInst().frmTracksOverview->setPositionManager (positionManager);
	TTTManager::getInst().frmTracksOverview->showFluorescence();
	TTTManager::getInst().frmTracksOverview->show();
}

void TTTMovie::initForms()
{
	//NOTE:
	//some of the parameters can be 0!
	
	if (TTTManager::getInst().frmTracking) {
		
		connect ( pbtStartTracking, SIGNAL (toggled (bool)), TTTManager::getInst().frmTracking, SLOT (startCellTracking (bool)));
		
                connect ( multiPicViewer, SIGNAL (trackPointSet (Track *, int)), TTTManager::getInst().frmTracking, SLOT (drawTree (Track *)));
//		for (int i = 0; i < (int)PictureFrames.size(); i++)
//			connect ( PictureFrames.find (i), SIGNAL (TrackPointSet (Track *, int)), TTTManager::getInst().frmTracking, SLOT (drawTree (Track *)));
	}
	
	if (positionManager->frmRegionSelection) {
		//BS 2010/02/10: currently not available due to graphical adaptions and former problems
		//connect ( positionManager->frmRegionSelection, SIGNAL (boxSizeChanged (/*RectBox **/)), this, SLOT (changeDisplaySize()));
                connect ( positionManager->frmRegionSelection, SIGNAL (boxPositionChanged (QPointF/*RectBox **/)), this, SLOT (changeDisplayPosition (QPointF)));
	}
	
	if (TTTManager::getInst().frmTreeStyleEditor) {
                connect (TTTManager::getInst().frmTreeStyleEditor, SIGNAL (styleChosen (StyleSheet &)), this, SLOT (applyStyleSheetMovie (StyleSheet &)));
	}
}

void TTTMovie::setTimePoints (int _firstTP, int _lastTP)
{
	// Nothing currently. Could remember this for export or so..
}

void TTTMovie::adjustDisplayedRegion (int /*_waveLength*/)
{
//@todo: adjust frame in frmRegionSelection


//quite simple: setZoom() does it all for us
//setZoom (Zoom, -1, -1, true);
	
// 	bool _done = false;
// 	
// 	if (_waveLength == -1)
// 		_waveLength = positionManager->getWavelength();
// 	
// 	//adjust ZoomBox according to the currently selected Zoom
// 	switch (Zoom) {
// 		case -1:
// 			//no change has to be made - the selected region stays constant, only the zoom factor changes automatically
// 			break;
// 		case 0:
// 			//no change has to be made - the full loaded region is displayed, only the zoom factor changes automatically
// 			break;
// 		case 50:
// 			//the selected region has to be set to double the size of the display
// 			//ZoomBox.resize (fraPictures->width() * 2, fraPictures->height() * 2);
// 			ZoomBox.resize (PictureFrames.find (_waveLength)->width() * 2, 
// 							PictureFrames.find (_waveLength)->height() * 2);
// 			if (positionManager->frmRegionSelection)
// 				positionManager->frmRegionSelection->repaint();
// 			_done = true;
// 		case 100:
// 			//the selected region has to be set to exactly the size of the display
// 			//ZoomBox.resize (fraPictures->width(), fraPictures->height());
// 			if (! _done) {
// 				ZoomBox.resize (PictureFrames.find (_waveLength)->width(), 
// 								PictureFrames.find (_waveLength)->height());
// 				if (positionManager->frmRegionSelection)
// 					positionManager->frmRegionSelection->repaint();
// 				_done = true;
// 			}
// 		default:
// 			
// 			if (Zoom > 0 & Zoom < 1000) {
// 				if (! _done) {
// 					ZoomBox.resize (PictureFrames.find (_waveLength)->width() * 100 / Zoom, 
// 									PictureFrames.find (_waveLength)->height() * 100 / Zoom);
// 					if (positionManager->frmRegionSelection)
// 						positionManager->frmRegionSelection->repaint();
// 					_done = true;
// 				}
// 				
// 				//common part (for zoom factors == 50,100,200,400,800)
// 				positionManager->getDisplays().setDisplayedRegion (_waveLength, ZoomBox.left(), ZoomBox.top(), ZoomBox.width(), ZoomBox.height());
// 			}
// 			
// 	}
}

void TTTMovie::processKey (int _keyCode, bool /*_shift*/, bool _ctrl) 
{
        QPointF shift;
//	bool shift_set = false;
//	float key_shift_offset = 20.0f;
	
	switch (_keyCode) {
//		case Qt::Key_F2:
//			if (TTTManager::getInst().frmTracking)
//				TTTManager::getInst().frmTracking->pbtTrackCell->animateClick();
//			break;
		//case Qt::Key_D:
		//case Qt::Key_A:
		//case Qt::Key_L:
		//	if (TTTManager::getInst().isTracking()) {
		//		if (_ctrl) {
		//			//track stop reason: set it according to the pressed key (only in tracking mode)
		//			TrackStopReason reason = TS_NONE;
		//			switch (_keyCode) {
		//				case Qt::Key_D:
		//					reason = TS_DIVISION;
		//					break;
		//				case Qt::Key_A:
		//					reason = TS_APOPTOSIS;
		//					break;
		//				case Qt::Key_L:
		//					reason = TS_LOST;
		//					break;
		//				default:
		//					;
		//			}
		//			
		//			if (reason != TS_NONE)
		//				stopTracking (reason);
		//			
		//		}
		//	}
		//	
		//	break;
		default:
			
			return;
	}
}

//SLOT:
void TTTMovie::editCellComment()
{
        editCellComment (TTTManager::getInst().getCurrentTrack(), positionManager->getTimepoint());
}

void TTTMovie::editCellComment (Track *_track, int _timepoint)
{

        if (! TTTManager::getInst().isTracking())
                return;
        if (! _track)
                return;
        if ((_timepoint < positionManager->getFirstTimePoint()) || (_timepoint > positionManager->getLastTimePoint()))
                return;


        TTTCellComment frmCellComment;
        connect ( &frmCellComment, SIGNAL (commentSaved (QString)), this, SLOT (saveCellComment (QString)));

        frmCellComment.setComment (_track->getComment (_timepoint));

        //note: as the form is in tracking mode, a PictureView instance
        //		 grabs the keyboard input
        //		-> this has to be interrupted until the comment form is closed

        //steal keyboard grab from grabbing PictureView
        if (TTTManager::getInst().getKeyboardGrabber()) {
                TTTManager::getInst().getKeyboardGrabber()->releaseKeyboard();
        }

        //show modal comment form
        frmCellComment.exec();

        //give keyboard grab back
        if (TTTManager::getInst().getKeyboardGrabber()) {
                TTTManager::getInst().getKeyboardGrabber()->grabKeyboard();
        }
}

//SLOT:
void TTTMovie::saveCellComment (QString _comment)
{
        if (! TTTManager::getInst().getCurrentTrack())
                return;

        //the currently entered comment is stored
        if (! _comment.isEmpty())
                TTTManager::getInst().getCurrentTrack()->setComment (_comment, positionManager->getTimepoint());
}

////SLOT:
////@Deprecated
//void TTTMovie::displayAllTracks (bool _on)
//{
//        if (! positionManager->isInitialized())
//                return;
//
//        if (! TTTManager::getInst().frmMainWindow)
//                return;
//
//		if(_on) {
//			const char* warning = "Please use the buttons in the menu under \"View -> Tracks\" or the corresponding shortcuts (these buttons will be removed in the next version):\n\n"
//				"\"Show all Tracks of this Position\" or F5 instead of \"All tracks...\"\n"
//				"\"Show all Tracks of all Positions\" or F6 instead of \"Really all!\"\n"
//				"\"First Trackpoints only\" or F7 instead of \"First Tracks...\"";
//			QMessageBox::information(this, "Warning", warning);
//		}
//
//		menuBar->actView_TRACKS_FIRST_TRACKS->setChecked(pbtDisplayAllFirstTracks->isOn());
//		menuBar->actView_TRACKS_POSITION_TRACKS->setChecked(pbtDisplayAllTracks->isOn());
//		menuBar->actView_TRACKS_ALL_POSITIONS_TRACKS->setChecked(pbtDisplayReallyAllTracks->isOn());
//
//		setShowExternalTracks(_on);
//
//		return;
//
//  //      bool firstTracks = false;
//  //      bool reallyAll = false;
//  //      //if (sender() != 0) {
//  //      //        firstTracks = (sender() == pbtDisplayAllFirstTracks);
//  //      //}
//		//firstTracks = pbtDisplayAllFirstTracks->isOn();
//  //      reallyAll = pbtDisplayReallyAllTracks->isOn();
//
//  //      ////if this slot was called by the jpg export method, the local setting has to be overridden
//  //      //switch (ExportTracks) {
//  //      //        case 1:
//  //      //                firstTracks = true;
//  //      //                break;
//  //      //        case 2:
//  //      //                firstTracks = false;
//  //      //                break;
//  //      //        case 3:
//  //      //                break;
//  //      //        default:
//  //      //                ;
//  //      //}
//
//  //      multiPicViewer->displayAllTracks (_on, firstTracks, reallyAll);
//}

void TTTMovie::setZoomSpinboxValue (int _zoom, bool _triggerCascade)
{

        call_setZoomOnspZoomFactor = _triggerCascade;

        spbZoomFactor->setValue (_zoom);

        call_setZoomOnspZoomFactor = true;
}

//SLOT
void TTTMovie::setCircleMousePointer (bool _on)
{
        multiPicViewer->setCircleMouse (_on);
}



void TTTMovie::setPictureSize(int x, int y)
{
	// Get fraPcitures and window size
	QSize sizeFraPictures = fraPictures->size(),
			windowSize = size();

	// Get size difference of fraPictures and complete window
	QSize delta(windowSize.width() - sizeFraPictures.width(), windowSize.height() - sizeFraPictures.height());

	// Calc new size of complete window
	QSize newSize(delta.width() + x, delta.height() + y);

	// Resize window
	resize(newSize);
}


void TTTMovie::toggleOverwriteTrackRadiusForDisplay(bool _on)
{
	// Enable/disable spin box for radius selection
	spbDisplayCircleSize->setEnabled(_on);

	// Tell pic viewer
	multiPicViewer->toggleOverwriteTrackRadiusForDisplay(_on, spbDisplayCircleSize->value());
}


void TTTMovie::setOverwriteTrackRadiusForDisplay(int _radius)
{
	// Tell pic viewer
	multiPicViewer->toggleOverwriteTrackRadiusForDisplay(pbtSetDisplayCircleSize->isChecked(), _radius);
}

void TTTMovie::launchMovieExportDialog()
{
	// Open dialog
	frmMovieExportDialog->show();
}

//SLOT:
void TTTMovie::specialExport()
{
	//TTTSpecialExport *frmTTTSpecialExport = new TTTSpecialExport(this);
	//frmTTTSpecialExport->exec();
	//delete frmTTTSpecialExport;

	// Open dialog
	frmSpecialMovieExportDialog->show();
}

void TTTMovie::launchDivisionExportDialog()
{
	// Open dialog
	divisionExportDialog->show();
}

void TTTMovie::debugTest()
{
	multiPicViewer->debugTest();

}

//void TTTMovie::exportMovieClicked()
//{
//	// Adjust size selected ?
//	bool autoAdjustSize = chkMovieExportAutoSize->isChecked();
//
//	// Desired picture format
//	QString imageFormat;
//	if (sender() == pbtJPGExport /*|| sender() == menuBar->actExport_MOVIE_FORMAT_JPG*/ || sender() == 0)
//		imageFormat = "JPEG";
//	else if (sender() == pbtBMPExport /*|| sender() == menuBar->actExport_MOVIE_FORMAT_BMP*/)
//		imageFormat = "BMP";
//	else /*if (sender() == pbtPNGExport || sender() == menuBar->actExport_MOVIE_FORMAT_PNG)*/
//		imageFormat = "PNG";
//
//	// Check if complete picture should be exported
//	bool exportComplete = sender() == pbtExportCompleteAllTracks || sender() == pbtExportCompleteFirstTracks;
//
//	// ---> Enum?
//	//if(exportComplete) {
//	//	// Check if show all tracks is currently enabled
//	//	if(positionManager->getAllTrackPoints().count() == 0)
//	//		disableShowAllTracksWhenFinished = true;
//
//	//	if (sender() == pbtExportCompleteAllTracks || _completeAll)
//	//		//show all tracks at the current timepoint
//	//		multiPicViewer->displayAllTracks (true, false, false);
//	//	else if (sender() == pbtExportCompleteFirstTracks || _completeFirst)
//	//		//show first tracks from all colonies
//	//		multiPicViewer->displayAllTracks (true, true, false);
//	//}
//}

QSize TTTMovie::getPictureDisplaySize() const
{
	return fraPictures->size();
}

void TTTMovie::showLogFilePath()
{
	// Get path
	QString path = Logging::getLogFilePath();

	// Check if we have path
	if(!path.isEmpty()) {
		// Display path
		QString msg = QString("Path to log file:\n\n%1").arg(QDir::toNativeSeparators(path));
		Tools::displayMessageBoxWithOpenFolder(msg, "Log file", path, true, this);
	}
	else 
		QMessageBox::critical(this, "Error", "Log file path is not set.");
}

void TTTMovie::setShowExternalTracks( bool _on )
{
	//// @DEPRECATED
	//// Keep old buttons synchronous
	//// Check if this function was called from within the menu
	//if(sender() == menuBar->actView_TRACKS_FIRST_TRACKS || sender() == menuBar->actView_TRACKS_ALL_POSITIONS_TRACKS || sender() == menuBar->actView_TRACKS_POSITION_TRACKS) {
	//	pbtDisplayAllFirstTracks->setChecked(menuBar->actView_TRACKS_FIRST_TRACKS->isChecked());
	//	pbtDisplayAllTracks->setChecked(menuBar->actView_TRACKS_POSITION_TRACKS->isChecked());
	//	pbtDisplayReallyAllTracks->setChecked(menuBar->actView_TRACKS_ALL_POSITIONS_TRACKS->isChecked());
	//}


	// Check if no external tracks should be displayed
	if(!menuBar->actView_TRACKS_POSITION_TRACKS->isChecked() && !menuBar->actView_TRACKS_ALL_POSITIONS_TRACKS->isChecked()) {

		// We implicitly enable display of tracks from this position, if user enabled first tracks while everything else is disabled
		if(sender() == menuBar->actView_TRACKS_FIRST_TRACKS && _on) {
			menuBar->actView_TRACKS_POSITION_TRACKS->setChecked(true);
		}
		else {
			// Disable external tracks
			showExternalTracksMode = OFF;
			positionManager->clearExternalTrees();

			// Update display
			multiPicViewer->drawPictures(positionManager->getTimepoint(), -1);
			multiPicViewer->drawAdditions();

			return;
		}
	}

	// Read external tracks
	bool readTrees = showExternalTracksMode == OFF;

	// Set showExternalTracksMode correctly
	bool firstTracks = menuBar->actView_TRACKS_FIRST_TRACKS->isChecked();
	if(menuBar->actView_TRACKS_ALL_POSITIONS_TRACKS->isChecked()) {
		if(firstTracks)
			showExternalTracksMode = FIRST_ALL_POS;
		else
			showExternalTracksMode = ALL_ALL_POS;
	}
	else if(menuBar->actView_TRACKS_POSITION_TRACKS->isChecked()) {
		if(firstTracks)
			showExternalTracksMode = FIRST_CUR_POS;
		else
			showExternalTracksMode = ALL_CUR_POS;
	}

	// Read external tracks
	if(readTrees)
		positionManager->readExternalTrees();

	// Update display
	multiPicViewer->drawPictures(positionManager->getTimepoint(), -1);
	multiPicViewer->drawAdditions();
}



void TTTMovie::printMovieExportTime (int _relativeTimepoint, int _currentTimepoint, QPixmap *_pix)
{
	if (! _pix)
		return;
	
	QString timeString = "";
	
	//former way, now free choice of the user
/*	//the font size depends on the display size to be of equal height in all exports
	//the rectangle size depends on the current display(!) width
	int fontWidth = 40;
	if (fraPictures->geometry().width() > 1000)
		fontWidth = 30;
	else if (fraPictures->geometry().width() > 500)
		fontWidth = 20;
	else if (fraPictures->geometry().width() > 100)
		fontWidth = 10;*/
	
	//int fontWidth = exportTimeFontSize;
	//bool fontBold = (chkExportTimeBold->isChecked());
	int fontWidth = 10;
	bool fontBold = false;	

	timeString = getMovieExportTimeString(_relativeTimepoint, _currentTimepoint);

	
	if (_relativeTimepoint > _currentTimepoint) 
		timeString = "- " + timeString;
	
	
	QPainter p;
	////the painter is set to paint on the first available picture frame 
	////(instance of PictureView), because on fraPictures it would be covered
	////needs to be calculated every timepoint because the number of pictures can change between timepoints
	//int index = 0;
	//for ( ; index < (int)PictureFrames.size(); index++) 
	//	if (PictureFrames.find (index)->getShow())
	//		break;
	
	int xpos = -1, ypos = -1;
	
	//set y position
	switch (UserInfo::getInst().getStyleSheet().getTimeboxPosition() / 10) {
		case 0:				//invisible
			break;
		case 1:				//y: top
// 			ypos = 0;
			// --------------Oliver------------
			if(chkShowWavelengthBox->isChecked())
				ypos = 20;
			else
				ypos = 0;
			break;
		case 2:				//y: middle
			ypos = _pix->height() / 2 - 20;
			break;
		case 3:				//y: bottom
			ypos = _pix->height() - 40;
			break;
		default:
			;
	}
	
	//set x position
	switch (UserInfo::getInst().getStyleSheet().getTimeboxPosition() % 10) {
		case 0:				//invisible
			break;
		case 1:				//x: left
			xpos = 0;
			break;
		case 2:				//x: middle
			xpos = _pix->width() / 2 - fontWidth * 8;
			break;
		case 3:				//x: right
			xpos = _pix->width() - fontWidth * 8;
			break;
		default:
			xpos = 0;
	}
	
	if (xpos >= 0 & ypos >= 0) {
//		if (! exportComplete) {
			//@ todo - what was the problem?
			//maybe create a transparent QPaintDevice above the PictureFrames to draw within
			
//			p.begin (PictureFrames.find (index));  // ------------------ Konstantin: comment out and make if {} else{} ----------------

			//----------------Oliver-----------------
			// Render into _pix instead of any PictureFrame
// 			if (Overlay) {
// 				if (PictureFrames.find (MAX_WAVE_LENGTH + 1)) 
// 					p.begin (PictureFrames.find (MAX_WAVE_LENGTH + 1));
// 			}
// 			else
// 				p.begin (PictureFrames.find (index));

			
			p.begin(_pix);
			//--> ----------------Oliver-----------------

			//p.begin (fraPictures);
			
			int tw = fontWidth * 8;
			int th = fontWidth * 2;


			switch (UserInfo::getInst().getStyleSheet().getTimeboxBackground()) {
				case 0:
					//transparent - nothing special
					break;
				case 1:
					//black background - draw black box
					p.save();
					p.setBrush (QBrush (Qt::black));
					p.drawRect (xpos, ypos, tw, th);
					p.restore();
					break;
				default:
					;
			}
			
			p.setFont (QFont ("Arial", fontWidth, fontBold ? QFont::Bold : QFont::Normal));
			p.setPen (QPen (Qt::white, fontWidth));
			
			//prints the time string and fits it into the provided rectangle
			// (x,y,width,height)
			//p.drawText (xpos, ypos, fontWidth * 8, 35, Qt::AlignJustify | Qt::SingleLine, timeString);
			p.drawText (xpos, ypos, tw, th, Qt::AlignJustify | Qt::SingleLine, timeString);
			
			p.end();
//		}
	}
}

QString TTTMovie::getMovieExportTimeString(int _relativeTimepoint, int _currentTimepoint)
{
	int secsPassed = 0;
	int days = 0, hours = 0, minutes = 0;
	//add time in top left corner			
	//the time is calculated in relation to the timepoint specified in the spinbox
	if (_relativeTimepoint <= _currentTimepoint) {
		secsPassed = positionManager->getFiles()->calcSeconds (_relativeTimepoint, _currentTimepoint);
	}
	else {
		secsPassed = positionManager->getFiles()->calcSeconds (_currentTimepoint, _relativeTimepoint);
	}
	qDebug() << _relativeTimepoint << " - " << _currentTimepoint << ": " << secsPassed;
	days = secsPassed / 86400;
	secsPassed %= 86400;
	hours = secsPassed / 3600;
	secsPassed %= 3600;
	minutes = secsPassed / 60;
	secsPassed %= 60;
	return QString ().sprintf ("%01d - %02d:%02d:%02d", days, hours, minutes, secsPassed);
}

void TTTMovie::updateLblLastPicture(int _timepoint)
{
	if( positionManager && positionManager->getPictures())
		lblLastPicture->setText(QString("%1/%2").arg(_timepoint).arg((positionManager->getPictures()->getLastLoadedPictureIndex().TimePoint)));
	else
		lblLastPicture->setText("?(error)");
}

void TTTMovie::updateStatusBarMessage()
{
	QString statusMessage;

	// Generate index information part of status message
	if(currentIndex != -1 && lastIndex != -1) 
		statusMessage = QString("No %1 of %2").arg(currentIndex).arg(lastIndex);

	// Generate mouse position part
	if(showMousePos) {
		if(!statusMessage.isEmpty())
			statusMessage.append("; ");

		// Add position string
		float mmpp = TATInformation::getInst()->getWavelengthInfo (0).getMicrometerPerPixel();
		statusMessage.append(QString ("X: %1 - Y: %2; um/pix = %3").arg (mousePosX).arg (mousePosY).arg (mmpp, 0, 'f', 3));
	}

	// Show message
	statusBar->showMessage(statusMessage);
}

void TTTMovie::setTrackingCellPropertiesDifferentiation( int _properites )
{
	/**
	 * _properties:
	 * always 0 (nothing selected) or a number with 4 digits, where 1st digit contains tissue type (as 
	 * described in the file format specification), 2nd digit contains general type
	 * and last 2 digits contain lineage
	 */

	// Properties with default values
	TissueType tissueType = TT_NONE;
	CellGeneralType cellGeneralType = CGT_NONE;
	char cellLineage = 0;

	// Check _properites
	if(_properites <= 9999 && (_properites >= 1000 || _properites == 0) ) {
		// Decode _properties
		tissueType = (TissueType)(_properites / 1000);
		_properites %= 1000;
		cellGeneralType = (CellGeneralType)(_properites / 100);
		_properites %= 100;
		cellLineage = _properites;
	}
	else {
		qWarning() << "tTt - warning: invalid _properties in TTTMovie::setTrackingCellPropertiesDifferentiation: " << _properites;
	}

	// Set properties in TTTManager
	TTTManager::getInst().setTrackingDifferentiation(tissueType, cellGeneralType, cellLineage);
}

void TTTMovie::setTrackingCellPropertiesAdherence( bool _on )
{
	// Just handle on events
	if(!_on)
		return;

	// Adherence
	ADHERENCE_STATE adherence = AS_NONE;

	// Set adherence
	QObject *source = sender();
	if(source == menuBar->actTracking_PROPERTY_ADHERENCE_ADHERENT || source == pbtPropertyAdherent) {
		adherence = AS_ADHERENT;

		// Keep buttons and menu synchronous
		pbtPropertyAdherent->setChecked(true);
		menuBar->actTracking_PROPERTY_ADHERENCE_ADHERENT->setChecked(true);
	}
	else if(source == menuBar->actTracking_PROPERTY_ADHERENCE_FREEFLOATING || source == pbtPropertyFreeFloating) {
		adherence = AS_FREEFLOATING;

		// Keep buttons and menu synchronous
		pbtPropertyFreeFloating->setChecked(true);
		menuBar->actTracking_PROPERTY_ADHERENCE_FREEFLOATING->setChecked(true);
	}
	else if(source == menuBar->actTracking_PROPERTY_ADHERENCE_SEMIADHERENT || source == pbtPropertySemiAdherent) {
		adherence = AS_SPYHOP;

		// Keep buttons and menu synchronous
		pbtPropertySemiAdherent->setChecked(true);
		menuBar->actTracking_PROPERTY_ADHERENCE_SEMIADHERENT->setChecked(true);
	}
	else if(source == menuBar->actTracking_PROPERTY_ADHERENCE_NONE) {
		adherence = AS_NONE;

		// Keep buttons and menu synchronous
		//pbtPropertyNone->setChecked(true);
		pbtPropertySemiAdherent->setEnabled(false);
		pbtPropertyAdherent->setEnabled(false);
		pbtPropertyFreeFloating->setEnabled(false);

		menuBar->actTracking_PROPERTY_ADHERENCE_NONE->setChecked(true);
	}
	else 
		qWarning() << "setTrackingCellPropertiesAdherence: Invalid sender.";

	// Set properties in TTTManager
	TTTManager::getInst().setTrackingCellAdherence(adherence);
}

void TTTMovie::setSelectedTrackingProperties( const CellProperties& _props )
{
	// Adherence
	if(_props.NonAdherent && _props.FreeFloating) {
		menuBar->actTracking_PROPERTY_ADHERENCE_FREEFLOATING->setChecked(true);
		pbtPropertyFreeFloating->setChecked(true);
	}
	else if(!_props.NonAdherent && !_props.FreeFloating) {
		menuBar->actTracking_PROPERTY_ADHERENCE_ADHERENT->setChecked(true);
		pbtPropertyAdherent->setChecked(true);
	}
	else if(_props.NonAdherent && !_props.FreeFloating) {
		menuBar->actTracking_PROPERTY_ADHERENCE_SEMIADHERENT->setChecked(true);
		pbtPropertySemiAdherent->setChecked(true);
	}
	else {
		menuBar->actTracking_PROPERTY_ADHERENCE_NONE->setChecked(true);
		//pbtPropertyNone->setChecked(true);
	}

	// Differentiation
	switch(_props.tissueType) {
	case Blood:
		switch(_props.cellGeneralType) {
		case Myeloid:
			switch(_props.cellLineage) {
			case 1:
				menuBar->actTracking_PROPERTY_DIFF_BLOOD_MYLOID_MEGA->setChecked(true);
				break;
			case 2:
				menuBar->actTracking_PROPERTY_DIFF_BLOOD_MYLOID_ERY->setChecked(true);
				break;
			case 3:
				menuBar->actTracking_PROPERTY_DIFF_BLOOD_MYLOID_MAKRO->setChecked(true);
				break;
			case 4:
				menuBar->actTracking_PROPERTY_DIFF_BLOOD_MYLOID_NEUTRO->setChecked(true);
				break;
			case 5:
				menuBar->actTracking_PROPERTY_DIFF_BLOOD_MYLOID_EOSINO->setChecked(true);
				break;
			case 6:
				menuBar->actTracking_PROPERTY_DIFF_BLOOD_MYLOID_BASO->setChecked(true);
				break;
			case 7:
				menuBar->actTracking_PROPERTY_DIFF_BLOOD_MYLOID_MAST->setChecked(true);
				break;
			case 8:
				menuBar->actTracking_PROPERTY_DIFF_BLOOD_MYLOID_DENDRITIC->setChecked(true);
				break;
			default:
				menuBar->actTracking_PROPERTY_DIFF_BLOOD_MYLOID_NONE->setChecked(true);
			}
			break;
		case Lymphoid:
			switch(_props.cellLineage) {
			case 1:
				menuBar->actTracking_PROPERTY_DIFF_BLOOD_LYMPHOID_B->setChecked(true);
				break;
			case 2:
				menuBar->actTracking_PROPERTY_DIFF_BLOOD_LYMPHOID_T->setChecked(true);
				break;
			case 3:
				menuBar->actTracking_PROPERTY_DIFF_BLOOD_LYMPHOID_NK->setChecked(true);
				break;
			default:
				menuBar->actTracking_PROPERTY_DIFF_BLOOD_LYMPHOID_NONE->setChecked(true);
			}
			break;
		case HSC:
			switch(_props.cellLineage) {
			case 1:
				menuBar->actTracking_PROPERTY_DIFF_BLOOD_HSC_LTR->setChecked(true);
				break;
			case 2:
				menuBar->actTracking_PROPERTY_DIFF_BLOOD_HSC_STR->setChecked(true);
				break;
			case 3:
				menuBar->actTracking_PROPERTY_DIFF_BLOOD_HSC_MPP->setChecked(true);
				break;
			case 4:
				menuBar->actTracking_PROPERTY_DIFF_BLOOD_HSC_CMP->setChecked(true);
				break;
			case 5:
				menuBar->actTracking_PROPERTY_DIFF_BLOOD_HSC_GMP->setChecked(true);
				break;
			case 6:
				menuBar->actTracking_PROPERTY_DIFF_BLOOD_HSC_MEP->setChecked(true);
				break;
			default:
				menuBar->actTracking_PROPERTY_DIFF_BLOOD_HSC_NONE->setChecked(true);
			}
			break;
		case Primitive:
			menuBar->actTracking_PROPERTY_DIFF_BLOOD_PRIMITIVE->setChecked(true);
			break;
		default:
			menuBar->actTracking_PROPERTY_DIFF_BLOOD_NONE->setChecked(true);
		}
		break;
	case Stroma:
		switch(_props.cellGeneralType) {
		case Primary:
			switch(_props.cellLineage) {
			case 1:
				menuBar->actTracking_PROPERTY_DIFF_STROMA_PRIM_WBM->setChecked(true);
				break;
			case 2:
				menuBar->actTracking_PROPERTY_DIFF_STROMA_PRIM_OB->setChecked(true);
				break;
			case 3:
				menuBar->actTracking_PROPERTY_DIFF_STROMA_PRIM_EC->setChecked(true);
				break;
			default:
				menuBar->actTracking_PROPERTY_DIFF_STROMA_PRIM_NONE->setChecked(true);
			}
			break;
		case CellLine:
			switch(_props.cellLineage) {
			case 1:
				menuBar->actTracking_PROPERTY_DIFF_STROMA_CELLLINE_AFT->setChecked(true);
				break;
			case 2:
				menuBar->actTracking_PROPERTY_DIFF_STROMA_CELLLINE_2018->setChecked(true);
				break;
			case 3:
				menuBar->actTracking_PROPERTY_DIFF_STROMA_CELLLINE_BFC->setChecked(true);
				break;
			case 4:
				menuBar->actTracking_PROPERTY_DIFF_STROMA_CELLLINE_OP9->setChecked(true);
				break;
			case 5:
				menuBar->actTracking_PROPERTY_DIFF_STROMA_CELLLINE_PA6->setChecked(true);
				break;
			default:
				menuBar->actTracking_PROPERTY_DIFF_STROMA_CELLLINE_NONE->setChecked(true);
			}
			break;
		default:
			menuBar->actTracking_PROPERTY_DIFF_STROMA_NONE->setChecked(true);
		}
		break;
	case Endothelium:
		menuBar->actTracking_PROPERTY_DIFF_ENDOTHELIUM->setChecked(true);
		break;
	case Heart:
		menuBar->actTracking_PROPERTY_DIFF_HEART->setChecked(true);
		break;
	case Nerve:
		switch(_props.cellGeneralType) {
		case Neuro:
			menuBar->actTracking_PROPERTY_DIFF_NERVE_NEURO->setChecked(true);
			break;
		case Glia:
			menuBar->actTracking_PROPERTY_DIFF_NERVE_GLIA->setChecked(true);
			break;
		default:
			menuBar->actTracking_PROPERTY_DIFF_NERVE_NONE->setChecked(true);
		}
		break;
	case Endoderm:
		menuBar->actTracking_PROPERTY_DIFF_ENDODERM->setChecked(true);
		break;
	case Mesoderm:
		menuBar->actTracking_PROPERTY_DIFF_MESODERM->setChecked(true);
	default:
		menuBar->actTracking_PROPERTY_DIFF_TISSUE_NONE->setChecked(true);
	}

	// Wavelengths
	for(int i = 1; i < MAX_WAVE_LENGTH+1 && i <= menuBar->actTracking_PROPERTY_WLS.size(); ++i) {
		// Menu actions
		menuBar->actTracking_PROPERTY_WLS[i-1]->setChecked(_props.WaveLength[i] != 0);

		// Buttons
		if(i >= 1 && i <= 10) {
			switch(i) {
			case 1:
				pbtWL1->setChecked(_props.WaveLength[i] != 0);
				break;
			case 2:
				pbtWL2->setChecked(_props.WaveLength[i] != 0);
				break;
			case 3:
				pbtWL3->setChecked(_props.WaveLength[i] != 0);
				break;
			case 4:
				pbtWL4->setChecked(_props.WaveLength[i] != 0);
				break;
			case 5:
				pbtWL5->setChecked(_props.WaveLength[i] != 0);
				break;
			case 6:
				pbtWL6->setChecked(_props.WaveLength[i] != 0);
				break;
			case 7:
				pbtWL7->setChecked(_props.WaveLength[i] != 0);
				break;
			case 8:
				pbtWL8->setChecked(_props.WaveLength[i] != 0);
				break;
			case 9:
				pbtWL9->setChecked(_props.WaveLength[i] != 0);
				break;
			case 10:
				pbtWL10->setChecked(_props.WaveLength[i] != 0);
				break;
			}
		}
	}


	// Endomitosis
	pbtEndoMitosis->setChecked(_props.EndoMitosis);
}

void TTTMovie::setTrackingCellPropertiesWavelength( bool _on )
{
	int wavelength = -1;


	// Find sender to identify wavelength
	QObject *source = sender();

	// First check fixed buttons
	if(source == pbtWL1) {
		wavelength = 1;
		menuBar->actTracking_PROPERTY_WLS[0]->setChecked(_on);
	}
	else if(source == pbtWL2)  { 
		wavelength = 2;
		menuBar->actTracking_PROPERTY_WLS[1]->setChecked(_on);
	}
	else if(source == pbtWL3)  { 
		wavelength = 3;
		menuBar->actTracking_PROPERTY_WLS[2]->setChecked(_on);
	}
	else if(source == pbtWL4)  {
		wavelength = 4;
		menuBar->actTracking_PROPERTY_WLS[3]->setChecked(_on);
	}
	else if(source == pbtWL5)  {
		wavelength = 5;
		menuBar->actTracking_PROPERTY_WLS[4]->setChecked(_on);
	}
	else if(source == pbtWL6)  {
		wavelength = 6;
		menuBar->actTracking_PROPERTY_WLS[5]->setChecked(_on);
	}
	else if(source == pbtWL7)  {
		wavelength = 7;
		menuBar->actTracking_PROPERTY_WLS[6]->setChecked(_on);
	}
	else if(source == pbtWL8)  {
		wavelength = 8;
		menuBar->actTracking_PROPERTY_WLS[7]->setChecked(_on);
	}
	else if(source == pbtWL9)  {
		wavelength = 9;
		menuBar->actTracking_PROPERTY_WLS[8]->setChecked(_on);
	}
	else if(source == pbtWL10) {
		wavelength = 10;
		menuBar->actTracking_PROPERTY_WLS[9]->setChecked(_on);
	}
	else {
		// Then check menu
		for(int i = 0; i < menuBar->actTracking_PROPERTY_WLS.size(); ++i) {
			if(menuBar->actTracking_PROPERTY_WLS[i] == source) {
				wavelength = i + 1;
				break;
			}
		}

		// Synchronize buttons
		if(wavelength <= 10 && wavelength > 0) {
			switch(wavelength) {
			case 1:
				pbtWL1->setChecked(_on);
				break;
			case 2:
				pbtWL2->setChecked(_on);
				break;
			case 3:
				pbtWL3->setChecked(_on);
				break;
			case 4:
				pbtWL4->setChecked(_on);
				break;
			case 5:
				pbtWL5->setChecked(_on);
				break;
			case 6:
				pbtWL6->setChecked(_on);
				break;
			case 7:
				pbtWL7->setChecked(_on);
				break;
			case 8:
				pbtWL8->setChecked(_on);
				break;
			case 9:
				pbtWL9->setChecked(_on);
				break;
			case 10:
				pbtWL10->setChecked(_on);
				break;
			default:
				;
			}
		}
	}

	// If found sender, set wavelength
	if(wavelength != -1)
		TTTManager::getInst().setTrackingWavelength(wavelength, _on);
}

void TTTMovie::setExternalTracksMode( DisplayExternalTracksMode _mode )
{
	showExternalTracksMode = _mode;

	if(_mode != OFF)
		positionManager->readExternalTrees();
}

//void TTTMovie::autoTrackingButtonClicked()
//{
//	// Delegate to TTTManager and hide button
//
//	pbtProposeTreeFragment->hide();
//}

//void TTTMovie::activateProposeTreeFragmentBtn( const QString& _buttonText )
//{
//	// Set text and make visible
//	pbtProposeTreeFragment->setCaption(_buttonText);
//	pbtProposeTreeFragment->setVisible(true);
//}

void TTTMovie::chkHideLoadPicsBarsToggled( bool _on )
{
	// Pass to multi pic viewer
	multiPicViewer->showPositionButtons(!_on);
}

bool TTTMovie::event( QEvent* _e )
{
	// Check for WindowActivate events
	if(_e->type() == QEvent::WindowActivate) {
		// Set corresponding position to be the current one
		TTTManager::getInst().frmPositionLayout->setPosition(positionManager->positionInformation.getIndex());
	}

	return QWidget::event(_e);
}

void TTTMovie::showMoviePlayer()
{
	// Just show dialog (dialog is created in constructor)
	frmPlayer->show();
}

void TTTMovie::startPlayer(bool _loop, bool _reverse, unsigned int _interval )
{
	MovieReverse = _reverse;
	MovieLoop = _loop;

	if (! MoviePlaying)  
		MovieTimer->start (_interval);

	MoviePlaying = true;
}

void TTTMovie::stopPlayer()
{
	MovieTimer->stop();
	MoviePlaying = false;
}

void TTTMovie::showSpecialEffectsDialog()
{
	// Create window
	if(!specialEffectsWindow)
		specialEffectsWindow = new TTTSpecialEffects(this);

	// Show window
	specialEffectsWindow->show();
}

bool TTTMovie::useWlForOverlay( int wl )
{
	// Test if corresponding button exists and is checked
	if(QAbstractButton* btn = bgrOverlayWaveLengths->button(wl))
		return btn->isChecked();

	return false;
}

void TTTMovie::overlayWlButtonClicked( int id )
{
	// Just redraw images
	multiPicViewer->drawPictures();
	multiPicViewer->drawAdditions();
}

void TTTMovie::setZIndex( int newZIndex )
{
	// Update z-index
	positionManager->setZIndex(newZIndex);

	// Redraw images
	multiPicViewer->drawPictures();
	multiPicViewer->drawAdditions();
}
