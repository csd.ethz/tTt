/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TTTPOSITIONLAYOUT_H
#define TTTPOSITIONLAYOUT_H

#include "ui_frmPositionLayout.h"

// Qt
#include <QDir>
#include <QHash>

// Qt3
#include <q3buttongroup.h>
#include <q3intdict.h>

// Project
#include "tttdata/systeminfo.h"


// Forward declarations
class PositionThumbnail;
class PositionLayoutPictureListModel;
class TTTVersionInfo;
class TTTPositionManager;
class PositionDisplay;
class StyleSheet;
class TTTPositionLayoutMovieExport;
//class TTTBackgroundCorrection;
class TTTExportFluorescencePatches;


class TTTPositionLayout: public QWidget, public Ui::FrmPositionLayout {

Q_OBJECT

public:
	
	TTTPositionLayout(QWidget *parent = 0, const char *name = 0);

	~TTTPositionLayout();

	/**
	 * sets the experiment directory (called only once by TTTMainWindow)
	 * @param _expDir the experiment QDir object
	 * @param _positionCount then number of positions in the current experiment folder
	 * @param _fastLoad whether the loading should be fast (is achieved by not loading the thumbnails)
	 */
	void setExperimentQDir (const QDir &_expDir, int _positionCount, bool _fastLoad);
	
	void resizeEvent (QResizeEvent *);
	
	void closeEvent (QCloseEvent *);

	void showEvent (QShowEvent *_ev);
	
	/**
	 * sets both microscope factors to avoid updating the size twice
	 * @param _ocularFactor the ocular factor
	 * @param _tvFactor the tv adapter factor
	 */
	void setFactors (int _ocularFactor, float _tvFactor);
	
	//PositionThumbnail* getCurrentThumbnail() const
	//	{
	//		return currentThumbnail;
	//	}
	
	/**
	 * Reads the dimension of the first image of all wavelengths
	 * @param _fromListView whether the picture filenames should be taken from this form's listview; if false, the picture files from the folder are read
	 * @param _posIndex the index of the position to be read; if "", the current position is taken
	 * @return the suffix (jpg, tiff, ...) of the first picture of wavelength 0; if reading failed it is set to ""
	 */
	QString getImageSize (bool _fromListView = true, QString _posIndex = "");
	
	///initializes the loading region with the complete original image size
	///@param _posIndex the index of the position for which this should happen
	void initLoadingRegion (const QString &_posIndex);
	
	///**
	// * Initializes the provided position manager (reads log file, sets timepoints, ...)
	// * For details of the initialization, please refer to the definition.
	// * @param _tttpm the position manager to be initializes
	// * @param _pictureSuffix the file suffix of the picture files
	// * @return success
	// */
	//bool initPositionManager (TTTPositionManager *_tttpm, const QString &_pictureSuffix);

	/**
	 * Marks pictures that have been selected in listview for loading
	 * @return if _markSelectedPicsToBeLoaded is true the number of pictures that were marked for loading is returned 
	 */
	int markSelectedPicturesForLoading();

	/**
	 * Mark pictures loaded for current position (indicated by a "P" on the thumbnail for this position) 
	 * @param _loaded whether mark should be added or removed
	 * @return true if position has been marked which will only happen if no errors occurred and - if 
	 *			_loaded=true - PictureArray::getLoadedPictures() returns not 0.
	 */
	bool markPicturesLoadedForCurrentPosition(bool _loaded = true);

	/**
	 * Returns the thumbnail corresponding to the provided position index, 0 if it does not exist
	 * @param _index the position index
	 * @return the thumbnail for the provided index, or 0 if it does not exist
	 */
	PositionThumbnail* getThumbnail (const QString& _index) const;

	///**
	// * Initialize current position and initialize images of current position
	// */
	//bool initCurrentPositionAndImages();

	/**
	 * @return the experiment directory
	 */
	QDir getExperimentDir() const {
		return experimentQDir;
	}

	/**
	 * Check if log file in position folders exist and, if not, offer to open TTTLogFileConverter
	 */
	void checkIfLogFilesExist();

	/**
	 * Completely re-initialize position display, e.g. after positions have been set as beeing not available
	 */
	void reInitializePositionDisplay();
	
public slots:

	/**
	 * Selects the specified position in position layout and changes current position here
	 * as well as in TTTManager (global current position) and base position to it. Initializes it if necessary.
	 * @param _posIndex the new position index
	 * @param _initPosition if this position should also be initialized (picture array created, tracking window opened and so on)
	 * @return true if successful
	 */
	bool setPosition(QString _posIndex/*, bool _initPosition = false*/);

	/**
	 * tTt Options were changed, connected to TTTTreeStyleEditor
	 * @param _ss ignored
	 */
	void styleChosen (StyleSheet &_ss);

	/**
	 * Show wizard to export image patches of all cells.
	 */
	void exportCellImagePatches();


private slots:

	// @Deprecated: use "bool setPosition (const QString &_index)" instead
	/////sets the position with the provided index to be the current (should only be called by PositionDisplay)
	/////@param _index the index of the position to be shown
	/////@param _lastIndex the index of position that was selected before
	//void setPosition (const QString &_index, const QString &_lastIndex);
	
	
	///loads all selected images into memory, reads the log file and stores
	///@param _initPosition whether the initParameters() method should be called (not necessary if this function is called by loadTTTFile() or addNewColony())
	void loadImages (bool _initPosition = true);
	
	///fetches information about picture size, number of selected pictures, RAM usage
	/// and displays it; all happens for the current picture(s)
	///called by a left click or a space press on the image list
	void displayInfoAboutSelectedPics ();
	
	///counts the selected pictures
	int countSelected();
	
	///displays/removes the selected wavelength (<code>sender()</code>-evaluation)
	void showWaveLength();
	
	///displays or hides all files of the selected wavelength in the file list (depending on button state)
	void selectWaveLength();
	
	// /unchecks all files of the selected wavelength in the file list
	//void deselectWaveLength();
	
	///sets a check on all files of wavelength [default] for each file/timepoint where
	/// another wavelength is selected
	void select0ForEachX();
	
	///selects the desired pictures (interval, first, last, ...) 
	void selectionHandler();
	
	///opens the form for selecting the loading region (modal!)
	void openRegionSelection();
	
	///reads the selected tTt-file from disk and finally calls LoadImages()
	void loadTTTFile();
	
	///**
	// * called when the user clicks on "new colony"
	// * adds a new colony instead of loading an old one
	// */
	//void addNewColony();

	///**
	// * Open window for background correction.
	// */
	//void showBackgroundCorrectionWindow();
	
	/**
	 * called by positionView
	 * displays the movie window of the provided position
	 * @param _index the index of the position to be shown
	 */
	void showMovieWindow (const QString &_index);
	
	/**
	 * called by positionView
	 * shows additional information for the provided position
	 * @param _index the index of the position
	 */
	void showPositionInformation (const QString &_index);
	
	/* *
	 * same as above, but via button call -> position still needs to be determined...
	 */
// 	void showPositionInformation();
	
	///sets the ocular factor
	//void setOcularFactor (int);
	void setOcularFactor (const QString&);
	
	///sets the tv adapter scaling factor
    void setTVAdapterFactor (const QString &_factor); //int);
	
	/////called when the position statistics window is closed
	/////sets the displayed picture of the current position (which was loaded in the statistics form) via the provided thumbnail object
	//void updateDisplayedPicture (PositionThumbnail *);
	
	///**
	// * displays the statistics window
	// */
	//void showStatisticsWindow();
	
	/**
	 * sets the coordinate system to be inverted
	 * @param _inverted true, if the coordinate system is inverted (positive axes are up and left); false, if it is normal (positive axes are down and right)
	 */
	void setInvertedCoordinateSystem (bool);
	
	/**
	 * Converts all colonies in all positions which have incorrect coordinates due to the incorrectly inverted coordinate system
	 * Only works if coordinate system is actually inverted
	 * HANDLE WITH CARE!
	 */
	void convertIncorrectCoordinates();

	/**
	 * Special movie export in position layout, for exporting movies where all positions are visible
	 */
	void posLayoutMovieExport();
	
	/**
	 * converts all ttt files of this experiment to a Jan Krumsiek readable format
	 * for details see implementation
	 */
	void exportAllTTTFiles();
	
	/**
	 * sets whether the frame around each position thumbnail should be drawn or not
	 * @param _draw true => frame is drawn; false => frame is not drawn
	 */
	void toggleDrawThumbnailFrame (bool _draw);

	/**
	 * sets whether the text on each position thumbnail should be drawn or not
	 * @param _draw true => text is drawn; false => text is not drawn
	 */
	void toggleDrawThumbnailText (bool _draw);

	/**
	 * displays a form with the xml content of the first picture in the current position (if a position is selected)
	 */
	void showPositionXMLDisplay();
	
	/**
	 * displays the ttt customization dialog
	 */
	void showStyleEditor();
	
	/**
	 * refreshes (redraws) the complete position layout
	 * also refreshes the tree count
	 */
	void refreshLayout();
	
	/**
	 * shows a dialog for merging trees
	 */
	void mergeTrees();
	
	/**
	 * creates snapshots of all trees in this experiment 
	 */
	void createSnapshotsOfAllTrees();

	/**
	 * Show TTTLogFileConverter image
	 */
	void showLogFileConverterWindow();
	
	// --------------- oliver --------------------------
	// --> not needed currently
	///**
	//* convert Dirk's trees, see implementation for details
	//*/
	//void convertDirksTrees();
	

	/**
	 * Unload all pictures in all positions
	 */
	void unloadAllPictures();

	/**
	 * Open dialog to load pictures from multiple positions at once
	 */
	void loadPicsFromMultiplePositions();

	/**
	 * Zoom in position display was changed
	 */
	void zoomChanged(float _scale);
	
	/**
	 * Update thumbnails, reloads thumbnails i.e. after change of timepoint or position.
	 */
	void updateThumbNails();

	/**
	 * Export position layout.
	 */
	void exportPosLayout();

private:
	
	/////the widget containing the thumbnails of the positions
	//PositionDisplay *positionView;
	
	///the experiment folder
	QDir experimentQDir;
	
	///contains the experiment basename (the text before "_p"; without path)
	QString experimentBasename;
	
	// /the class for reading the log file for the current folder
	//LogFileReader *logInput;

	/// Picture list model/manager
	PositionLayoutPictureListModel* pictureListModel;
	
	
//the following attributes concern just the current position
	
	///the internal currently selected position
	///does not necessarily match with the global current position
	QString currentPosition;
	
	///holds the basename of the currently selected position
	QString currentBasename;
	
	///the current directory as QDir
	QDir currentQDir;
	
	///the file extension of the picture files (jpg, jpeg, tif, tiff, ...)
	QString suffix;
	
	///contains for each wavelength whether it should be displayed in the file list
	bool ShowWaveLength [MAX_WAVE_LENGTH + 1];
	
	/////the current selection (marks) of the file listview
	/////necessary to store in order to reconstruct the checked pictures even when they are switched off view
	//QBitArray itemsChecked;
	
	///the maximal number of images that fit into memory
	int maxMemImages;
	
	///the number of wavelengths that have pictures, but are hidden by the user (0 => all visible)
	int wavelengthsHidden;
	
	///contains the wavelength buttons, the index for a button is the wavelength it represents
	Q3IntDict<QPushButton> wavelengthButtons;
	
	/////the current thumbnail (set after a user click on a position)
	//PositionThumbnail *currentThumbnail;
	
	///whether the sizes of the images in the current position were already loaded
	bool image_sizes_set;
	
	///contains the button groups for each wavelength
	///map: wavelength -> button group
	///necessary for creating each group only once, thus avoiding overhead and name conflicts
	Q3IntDict<Q3ButtonGroup> wavelengthButtonGroups;
	
	///window layout already loaded
	bool windowLayoutLoaded;

	///window for showing tTt changelog
	TTTVersionInfo *versionWindow;

	/////whether current position has been initialized already
	//bool cuurentPositionInitialized;	// removed: causes problem if tracking window is closed, pic of same position is loaded -> position initialized = true causes errors..

	/// Hash of QLables for size infos (in pixels) of image files of different wavelengths (keys are wavelengths)
	QHash<int, QLabel*> imageSizeLabels;

	// Movie exporter
	TTTPositionLayoutMovieExport* movieExporter;

	//// Background correction window
	//TTTBackgroundCorrection* frmBackgroundCorrection;

	// Export image patches window
	TTTExportFluorescencePatches* frmExportImagePatches;

//private methods	
	
	///displays the ttt files in the current folder and on the NAS drive
	///@param _onlyFrmTracking whether the colonies should be loaded only into the colony list in the cell editor (true) or only in THIS' colony list (false, default)
	void showTTTFiles (bool _onlyFrmTracking = false);
	
	/////displays the files in the current directory
	/////@param _setWaveLengthBoxes whether the visibility of the wavelength option buttons should be updated (only necessary if a new folder/position was selected)
	/////@param _waveLength the wavelength for which the update should take place
	/////@param _show the action (show [==true] or hide [==false]) for the specified wavelength
	//void displayFiles (bool _setWaveLengthBoxes = true, int _waveLength = -1, bool _show = false);

	///displays the files in the current directory
	///@param _setWaveLengthBoxes whether the visibility of the wavelength option buttons should be updated (only necessary if a new folder/position was selected)
	void displayFiles (bool _setWaveLengthBoxes = true);
	
	///updates the display of the positions window
	void updatePositionDisplay();

	///(un)checks the files with _waveLength
	void selectWaveLength (int _waveLength, bool _select = true);
	
	///initializes parameters like first/last timepoint, and proceeds them to all other forms
	///@param _addColony whether a new colony should be loaded if there is no ttt file yet loaded
	///@return success
	bool initCurrentPosition (bool _addColony);
	
	/**
	 * creates a new wavelength selection button group
	 * @param _wavelength the wavelength for which this should be done
	 * @param _wavelengthsTillNow the wavelength buttons created until now (needed for the y offset of the group boxes)
	 * @return the newly created group (normally not necessary as Qt manages the parent-child connections)
	 */
	Q3ButtonGroup* createNewWavelengthGroup (int _wavelength, int _wavelengthsTillNow);
	
	/**
	 * deletes all wavelength buttons (created by the method @see createNewWavelengthGroup)
	 */
	void hideWavelengthButtons();
	
	/**
	 * displays the size information for all available wavelengths
	 */
	void displayImageSizes();
	
	/**
	 * searches for ttt files in the given folder and displays them, either in the colony combobox here or in the colony listbox in frmTracking
	 * @param _folder the folder to be searched
	 * @param _onlyFrmTracking whether the colonies should be displayed in frmTracking only
	 * @param _mark a string to be added before each colony (usable as folder mark)
	 * @return whether any file was found (true) or none (false)
	 */
	bool searchAndDisplayTTTFiles (const QString &_folder, bool _onlyFrmTracking, const QString &_mark);


	friend TTTPositionLayoutMovieExport;
};



#endif
