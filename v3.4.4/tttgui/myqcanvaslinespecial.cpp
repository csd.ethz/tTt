/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "myqcanvaslinespecial.h"


void MyQCanvasLineSpecial::addMiddlePoint (int _x, int _y, int _width)
{
	if ((_y > y2) || (_y < y1))
		return;
	
        int index = points.size();
	
	points.putPoints (index, 1, _x, _y);
	
        if (index >= widths.size())
		widths.resize ((uint)((float)index * 1.5f));
	
	widths [index] = _width;
}

void MyQCanvasLineSpecial::addMiddlePoint (int _x, int _y, int _width, QColor _color)
{
	//@ todo stamp line in any direction (cf. also drawShape())
	if ((_y > y2) || (_y < y1))
		return;
	
	if (! colors) {
		colors = new Q3ValueVector<QColor> (2);
		colors->push_back (Qt::black);
		colors->push_back (Qt::black);
	}
	
	colorsUsed = true;
        int index = points.size();
	points.putPoints (index, 1, _x, _y);
	//colors->push_back (_color);
	if (index >= colors->size())
		colors->resize ((uint)((float)index * 1.5f));
	
	(*colors) [index] = _color;
	
	if (index >= widths.size())
		widths.resize ((uint)((float)index * 1.5f));
	
	widths [index] = _width;
}

void MyQCanvasLineSpecial::drawShape (QPainter &_p)
{
	_p.save();
	
	//the end points are the first two pairs, regardless of the mode
	QPoint start = points.point (0);
	QPoint end = points.point (1);
	
        //_p.setRasterOp (rop);
        _p.setCompositionMode (rop);

	if (stamped) {
		//_p.drawLine ((int)start.x() - width_2, (int)start.y(), (int)start.x() + width_2, (int)start.y());
		//_p.drawLine ((int)end.x() - width_2, (int)end.y(), (int)end.x() + width_2, (int)end.y());
                for (int i = 2; i < points.size(); i++) {
			QPoint p = points.point (i);
			if (colorsUsed)
				_p.setPen (QPen (colors->at (i), _p.pen().width()));
			int tmpwidth = widths.at (i);
			_p.drawLine ((int)p.x() - tmpwidth, (int)p.y(), (int)p.x() + tmpwidth, (int)p.y());
		}
	}
	else {	
		if (usePC)
			_p.drawLine ((int)x(), (int)y(), (int)x() + (x2 - x1), (int)y() + (y2 - y1));
		else
			_p.drawLine (start, end);
		
		if (colorsUsed) {
			_p.setPen (QPen (colors->at (0), 3));
			_p.drawPoint (start);
			_p.setPen (QPen (colors->at (1), 3));
			_p.drawPoint (end);
		}
	}
	
	_p.restore();
}

