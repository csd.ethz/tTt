/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef autotrackingtreewindow_h__
#define autotrackingtreewindow_h__

#include "ui_frmAutoTrackingTreeWindow.h"

// Qt
#include <QWidget>
#include <QSharedPointer>
#include <QStatusBar>
#include <QStringListModel>
#include <QAbstractTableModel>

// STL
#include <cassert>

// Project
#include "tttbackend/treefragment.h"

// Forward declarations
class Tree;
class TTTAutoTracking;
class AutoTrackingCellCircle;
class TreeWindowMenu;
class ITrackPoint;
class TTTAutotrackingConnectFragments;
class TTTMovie;
class TTTAutoTrackingTreeWindowFragmentsTableModel;

/**
 * Tree window
 */
class TTTAutoTrackingTreeWindow : public QWidget {

	Q_OBJECT

public:
	
	/**
	 * Constructor
	 * @param _parent the parent widget
	 */
	TTTAutoTrackingTreeWindow(TTTAutoTracking* _frmAutoTracking, QWidget *_parent = 0);

	/**
	 * Add the specified tree to the tree display
	 * @param _newTree the tree to be displayed
	 * @param _fragments list of fragments used for _newTree (can be empty)
	 * @param _fileName path and filename of tree or empty string if tree is new
	 */
	void addTree(QSharedPointer<Tree> _newTree, const QList<QSharedPointer<TreeFragment> >& _fragments = QList<QSharedPointer<TreeFragment> >(), QString _fileName = "");

	/**
	 * Called when user has finished tracking manually after it has been started from here
	 */
	void notifyManualTrackingFinished();

	/**
	 * @return if the user has selected a cell in any tree (so that only this cell should be displayed in movie window)
	 */
	bool trackIsSelected() const;

	/**
	 * Return cell circle objects for displaying e.g. in TTTMovie, but only for the currently selected track and possible successor tracks.
	 * If no track is selected, an empty list is returned. Function is called by TTTAutoTracking::getCellCirclesForDisplay().
	 * @param _timePoint the timepoint
	 * @param _tttpm used to transform global to local coordinates
	 * @param _wlIndex index of wavelength
	 * @return list of AutoTrackingCellCircle instances that should be displayed (caller must make sure to delete them properly!)
	 */
	QList<AutoTrackingCellCircle*> getCellCirclesForDisplay(int _timePoint, TTTPositionManager* _tttpm, int _wlIndex) const;

	/**
	 * Fragment or tree to append to current track has been selected while not in manual tracking mode.
	 * Appends the tree or fragment to the currently selected track.
	 * @param _tree the selected tree (should be 0 if _fragment is not)
	 * @param _fragment the selected fragment (should be 0 if _tree is not)
	 * @param _trackPoint the trackpoint associated with the circle the user clicked on
	 */
	void treeOrTreeFragmentToAppendSelected(QSharedPointer<Tree> _tree, QSharedPointer<TreeFragment> _treeFragment, const ITrackPoint* _trackPoint); 

	/**
	 * Fragment or tree to append to current track has been selected while in manual tracking mode.
	 * Stops manual tracking mode and appends the tree or fragment to the current track
	 * as defined by TTTManager::getCurrentTrack().
	 * @param _tree the selected tree (should be 0 if _fragment is not)
	 * @param _fragment the selected fragment (should be 0 if _tree is not)
	 * @param _trackPoint the trackpoint associated with the circle the user clicked on
	 */
	void treeOrTreeFragmentToAppendSelectedDuringManualTracking(QSharedPointer<Tree> _tree, QSharedPointer<TreeFragment> _fragment, const ITrackPoint* _trackPoint);
	
	/**
	 * Save the provided tree.
	 * @param _tree the tree to save
	 * @param _saveAs if user should be asked for a new file name (even if tree is not new)
	 * @return true if successful
	 */
	bool saveTree(ITree* _tree, bool _saveAs = false);

	/**
	 * Check if last manual tracking was done in auto tracking mode.
	 * @return if last manual tracking was in auto tracking mode, i.e. if a tree in this window was tracked manually.
	 */
	bool manualTrackingWasInAutoTrackingMode() const {
		return m_lastManualTrackingInAutoTrackingMode;
	}

	/**
	 * Get current tree.
	 * @return pointer to currently selected tree or nullptr if none is selected.
	 */
	Tree* getCurrentTree() const {
		if(selectedTree && selectedTree->theTree)
			return selectedTree->theTree.data();
		else
			return nullptr;
	}

	/**
	 * Get current track.
	 * @return pointer to currently selected track or nullptr if none is selected.
	 */
	Track* getCurrentTrack() const {
		return selectedTrack;
	}


public slots:

	/**
	 * Change the current timepoint
	 * @param _timePoint the new time point
	 * @param _oldTimePoint (ignored)
	 */
	void setTimePoint(int _timePoint, int _oldTimePoint = 0);

	/**
	 * Display dialog to select existing ttt file(s) to open.
	 */
	void openExistingTreeFiles();

	/**
	 * Attempt to close the specified tree. If it has been modified, the user is asked first. If the user cancels
	 * or if the provided tree is not being displayed, no tree is closed and false is returned.
	 * @param _tree the tree that should be closed
	 * @param closeWithoutAsking if true, tree will be closed even if modified, discarding all changes (use with caution).
	 * @return true if tree was closed
	 */
	bool closeTree(ITree* _tree, bool closeWithoutAsking = false);

	/**
	 * Create a new tree and add it to display.
	 */
	void addNewTree();

	/**
	 * Opens a dialog that lets the user save all currently loaded tree fragments.
	 */
	void saveTreeFragments();

	/**
	 * Attempt to close all currently opened trees. If any of them has been modified, the user is asked first. If the user cancels,
	 * the tree is not closed and false is returned
	 * @return true if all trees were closed
	 */
	bool closeAllTrees();

	/**
	 * Start manual tracking of current cell, e.g. after user clicked on button to 
	 * start manual tracking.
	 */
	bool startManualTracking();

protected:

	// Key release
	void keyReleaseEvent (QKeyEvent *_event);

	// Close event
	void closeEvent (QCloseEvent *_event);

private slots:

	/**
	 * Loaded tree fragments have changed.
	 */
	void loadedTreeFragmentsChanged()
	{
		updateTreeFragmentsTable();
	}

	/**
	 * Save tree button in treeView clicked
	 * @param _tree pointer to the corresponding tree
	 */
	void saveTreeClicked(ITree* _tree);

	/**
	 * Tree left clicked
	 * @param _tree pointer to the corresponding tree
	 */
	void treeLeftClicked(ITree* _tree);

	/**
	 * User has selected a cell in treeView
	 * @param _tree pointer to tree the cell belongs to
	 * @param _track pointer to track that was clicked
	 */
	void cellSelected(ITree* _tree, ITrack* _track);

	/**
	 * User has deselected a cell in treeView
	 * @param _tree pointer to tree the cell belongs to
	 * @param _track pointer to track that was clicked
	 */
	void cellDeSelected(ITree* _tree, ITrack* _track);

	///**
	// * User changed image in pictureView
	// */
	//void imageChanged(int _timePoint, int _waveLength, int _zIndex);

	///**
	// * User changed zoom in pictureView
	// */
	//void picViewZoomChanged(int _zoomPercent);

	/**
	 * User clicked on cell division button
	 */
	void cellDivisionSelected();

	/**
	 * User clicked on cell death button
	 */
	void cellDeathSelected();
	
	/**
	 * User clicked on lost button
	 */
	void lostSelected();

	/**
	 * User clicked on remove cell fate button.
	 */
	void removeCellFate();

	/**
	 * Cut Tree at currentTimePoint (which will be the first time point of the new tree)
	 * @return true if successful
	 */
	bool cutTree();

	/**
	 * Apply search range specified in spinbox
	 */
	void applySearchRange();

	/**
	 * Close current tree
	 */
	void closeCurrentTree();

	/**
	 * Save current tree
	 */
	void saveCurrentTree();

	/**
	 * Save current tree as
	 */
	void saveCurrentTreeAs();

	/**
	 * Display help message
	 */
	void help();

	/**
	 * Currently selected position has changed.
	 * @param _oldPosition index of old position.
	 * @param _newPosition index of current position.
	 */
	void currentPositionChanged(int _oldPosition, int _newPosition);

	/**
	 * Show if backward tracking mode is currently on.
	 */
	void updateBackwardTrackingModeDisplay();

	/**
	 * Open movie window for current position.
	 */
	void openMovieWindow();

	/**
	 * Go to the first position of the currently selected tree.
	 */
	void goToFirstPosOfTree();

	/**
	 * Value of timepoint slider changed.
	 */
	void timePointSliderChanged(int value);

	/**
	 * Set value of sptFirstTp to first time point of current position.
	 */
	void imageLoadingSetFirstTp();

	/**
	 * Set value of spbLastTp to first time point of current position.
	 */
	void imageLoadingSetLastTp();

	/**
	 * Load selected images.
	 */
	void loadImages();

	/**
	 * Unload selected images.
	 */
	void unloadImages();

	/**
	 * Open wizard to connect tree fragments.
	 */
	void connectTreeFragmentsTool();

	/**
	 * User clicked on timeline.
	 */
	void timeLineClicked(int timepoint, Qt::MouseButton button);

	/**
	 * Loading or unloading of image completed.    
	 */
	void loadingCompleted();

	/**
	 * Select next region with low confidence for tree editing assistant.
	 */
	void assistantSelectNextRegion(bool directionForward = true);

	/**
	 * Select previous region with low confidence for tree editing assistant.
	 */
	void assistantSelectPrevRegion();

	/**
	 * Accept current region (i.e. set confidence to 1 for all tps in it) and go
	 * to next region.
	 */
	void assistantAcceptCurRegion();

	/**
	 * Swap selected cell with nearest neighbor at current time point, i.e. cut both tracks and append
	 * each cut off part to the other track.
	 */
	void swapCells();

	/**
	 * Tree view height factor changed.
	 */
	void treeViewHeightFactorChanged(int newValue);

	/**
	 * Tree view width factor (i.e. the distance betweeen tracks of the highest generation specified by KEY_TREEVIEW_TRACKLINEDISTANCE) changed.
	 */ 
	void treeViewWidthFactorChanged(int newValue);

	/**
	 * Show real time checkbox changed.
	 */
	void showRealTimeChanged(bool newValue);

	/**
	 * Draw continuous track lines changed.
	 */
	void continuousTrackLinesChanged(bool newValue);

	/**
	 * Timeline font size changed.
	 */ 
	void timeLineFontSizeChanged(int newValue);

	/**
	 * Size of track lines changed.
	 */ 
	void treeViewTrackLineWidthChanged(int newValue);

	/**
	 * Size of cell circles changed.
	 */ 
	void treeViewCellCircleRadiusChanged(int newValue);

	///**
	// * Wavelength selection box (un-)checked (by user or by code).
	// */
	//void wlSelectionBoxChecked(bool _on);

	///**
	// * Wavelength interval box (un-)checked (by user or by code).
	// */
	//void wlIntervalBoxChecked(bool _on);
	

	/**
	 * Show/hide tree fragments in tree fragments table depending on whether or not they have been used to create trees.
	 */
	void showHideUsedTreeFragmentsInTable();

	/**
	 * Tree fragment in fragment table was double clicked: use it to create a new tree.
	 */
	void treeFragmentDoubleClicked(const QModelIndex& index);

	/**
	 * Export current tree view.
	 */
	void exportView();

	/**
	 * Debug: randomly create a huge tree for visualization.
	 */
	void debugCreateTree();

private:

	/**
	 * Descriptor for trees that are currently being displayed/edited
	 */
	struct TreeDescriptor {
		// Pointer to Tree object
		QSharedPointer<Tree> theTree;

		// List of fragments that have been added to this tree
		QList<QSharedPointer<TreeFragment> > usedFragments;

		// Path of .ttt file for this tree, empty for new unsaved trees, without filename
		QString path;

		// Filename of .ttt file, without path
		QString fileName;
	};

	/**
	 * Ask user if he wants to save changes made to the tree with provided index in currentTrees (e.g. to open a new tree or close the window).
	 * Make sure to reset noToAllAtivated and yesToAllActiaved before calling this function.
	 * @param _index index of tree in currentTrees
	 * @param _yesAndNoToAllButton if 'yes to all' and 'no to all' buttons should be added. Useful when calling this function in a loop. 
	 * @return false if user decided to cancel or in case of error. True if changes were saved successfully or user decided to not save the changes. 
	 */
	bool saveChangesToTree(int _index, bool _yesAndNoToAllButton = false);

	/**
	 * Find array index of descriptor for provided tree in currentTrees list
	 * @param _tree the tree
	 * @return a valid index of currentTrees if _tree was found or -1
	 */
	int getIndexOfTree(ITree* _tree);

	/**
	 * Set current (i.e. selected tree)
	 * @param _index index of tree to select in currentTrees, if it is -1, no tree will be selected
	 * @return true if successful
	 */
	bool setSelectedTree(int _index);

	/**
	 * Update information about the currently selected tree
	 */
	void updateCurrentTreeInformation();

	/**
	 * Append specified tree fragment to specified track
	 * @param _frg the fragment to append
	 * @param _track the track to append it to
	 * @param _trackNumberToAppend number of the track of _frg that is to be appended, necessary if tree starts before _track's last trace
	 * @param _allowOverwriteOfTrackPoints if it should be allowed to overwrite existing trackpoints (i.e. if appending an overlapping fragment shoud be allowed)
	 * @return true if successful
	 */
	bool appendTreeFragmentToTrack(QSharedPointer<TreeFragment> _frg, Track* _track, int _trackNumberToAppend, bool _allowOverwriteOfTrackPoints = false);

	/**
	 * Append specified track of provided tree to provided track. If _tree begins after the last trackpoint of _track, it will
	 * simply be appended. If _tree begins before the last track point of _track and the current time point is after the last
	 * track point of _track, _tree will be cut at the last time point of _track and the cut part will be appended to _track.
	 * If the current time point is before the last time point of _track, _track will be cut at the current time point and _tree
	 * will be appended at the current time point (_tree will also be cut if it begins before current time point).
	 * @param _tree the tree to append
	 * @param _trackNumberToAppend number of the track of _tree to appended, necessary if tree starts before _track's last trace
	 * @param _track the track to append it to
	 * @param _allowOverwriteOfTrackPoints if it should be allowed to overwrite existing trackpoints (i.e. if appending an overlapping fragment shoud be allowed)
	 * @param _insertionTimePoint time point (inclusively) at which tree should be inserted. Set to last track point of _track before current time point if not specified.
	 * @return true if successful
	 */
	bool appendTreeToTrack(QSharedPointer<Tree> _tree, int _trackNumberToAppend, Track* _track, bool _allowOverwriteOfTrackPoints = false, int _insertionTimePoint = -1);

	/**
	 * Select specified cell fate for current track if not in manual tracking mode
	 */
	void setCellFate(TrackStopReason _cellFate);

	/**
	 * Checks if a tree is currently selected and displays message box if not
	 * @return true if tree is selected
	 */
	bool checkIfTreeIsSelected();
	
	/**
	 * Attempt to save the tree with provided index in currentTrees. If tree has been saved, its modified state will be set to false and
	 * the display name in treeView will be updated. If the tree has not been saved before, the user will always be asked for a filename
	 * @param _index index of tree in currentTrees
	 * @param _askForFilename whether the user should always be asked for the filename of the tree
	 * @return true if tree was saved successfully
	 */
	bool saveTreeInternal(int _index, bool _askForFilename = false);

	/**
	 * Cut the provided _track creating a new tree starting with the provided _timePoint and add it to display (if desired).
	 * @param _trackToCut track to cut
	 * @param _timePoint the time point to cut at, must be within lifetime of _track. Will be first track point of new tree
	 * @param _addNewTreeToDisplay if new tree should be added to display
	 * @return shared pointer to newly created tree, is null if not successful
	 */
	QSharedPointer<Tree> cutTreeInternal(Track* _trackToCut, int _timePoint, bool _addNewTreeToDisplay = true);

	/**
	 * Update window title
	 */
	void updateWindowTitle();

	/**
	 * Add wavelength selection button to tbtWls.
	 * @param _row row index.
	 * @param _col column index.
	 * @param _wl wavelength number, will be used as button text.
	 */
	void addWlButton(int _row, int _col, int _wl);

	/**
	 * Update wavelength buttons. (Re-)creates buttons for wavelength selection for loading images.
	 */
	void updateWlButtons();

	/**
	 * Update display of current timepoint (real time etc.).
	 */
	void updateDisplayOfCurrentTimepoint();

	/**
	 * Change the currently selected track. Automatically updates track information.
	 * Corresponding tree is selected automatically.
	 * @param newTrack newly selected track, can be nullptr to deselect the current cell.
	 * @return true if successful.
	 */
	bool setSelectedCell(ITrack* trackToSelect);

	/**
	 * Load or unload currently selected images.
	 * @param load true if images should be loaded, false if unloaded.
	 */
	void loadImagesInternal(bool load);

	/**
	 * Update enabled/disabled states of all ui controls to edit trees (e.g. to set cell fates). 
	 * Should be called e.g. after a track has been selected/deselected.
	 */
	void updateControlsTreeEdit();

	/**
	 * Update enabled/disabled states of all ui controls that depend on the number of opened trees.
	 */
	void updateControlsDependingOnNumTrees();

	/**
	 * Update enabled/disabled states of assistant ui controls.
	 */
	void updateControlsAssistant();

	/**
	 * Clears/fills/updates lists of trees and tree fragments (possibleTreeFragmentSuccessors and possibleTreeSuccessors) 
	 * that can be appended to the selected track.
	 */
	void refreshPossibleSuccessors();

	///**
	// * Add tree fragments to display (if chkShowSuccessors is checked).
	// * @param fragments list of tree fragments to display.
	// */
	//void displayTreeFragments(const QList<QSharedPointer<TreeFragment>>& fragments);
	
	/**
	 * Remove tree fragments from display.
	 * @param fragments list of tree fragments to remove from display.
	 */
	void removeTreeFragmentsFromDisplay(const QList<QSharedPointer<TreeFragment>>& fragments);

	/**
	 * Helper of saveTreeFragments(). Checks if there are existing trf files and asks user if they can be deleted. Returns
	 * false if they cannot be deleted (because of user choice or because there are .log files). Returns true if no .trf files
	 * were found or if existing .trf files were deleted successfully.
	 */
	bool handleExisingTrfFiles(const QString& _pathName);

	/**
	 * Helper of handleExisintTrfFiles(). Returns true if the specified directory contains
	 * .log files.
	 */
	bool containsLogFiles(const QString& _pathName);

	/**
	 * Helper of handleExisintTrfFiles(). Lists all .trf files in specified directory and
	 * appends them to _fileList. Optionally prepends a prefix to all found files.
	 */
	void listTrfFiles(const QString& _pathName, QStringList& _fileList, const QString& _prefix = "");

	/**
	 * During saving of tree fragments: generate filename for next tree fragment.
	 * @param minTp start tp of fragment.
	 */
	QString getNextTreeFilename(int minTp) const;

	/**
	 * Set state of tree editing assistant.
	 */
	void setTreeEditingAssistantState(QWeakPointer<TreeDescriptor> _tree, int _trackNum, int _startTp, int _stopTp);

	/**
	 * Reset state of tree editing assistant.
	 */
	void resetTreeEditingAssistant();

	/**
	 * Find next root.
	 */
	Track* assistantFindNextRootTrack(Track* start);

	/**
	 * Remove cell division of provided track.
	 */
	void removeCellDivision(Track* track);

	/**
	 * Find low confidence region in given track.
	 * @param _track
	 * @param _direction search direction: true for forward, false for backward.
	 * @param _lowerTp lower bound for search. If region is found, lower bound of found region will be stored here.
	 * @param _higherTp upper bound for search. If region is found, upper bound of found region will be stored here.
	 * @return true if regions was found.
	 */
	bool findLowConfidenceRegion(const Track* _track, bool _direction, int& _lowerTp, int& _higherTp, float _confidenceThreshold);

	/**
	 * Update display of tree fragments.
	 */
	void updateTreeFragmentsTable();

	/**
	 * Load images
	 * @param _timePoint the starting time point
	 * @param _loadBefore images to load before
	 * @param _loadAfter images to load after
	 * @param _wl the wavelength to load
	 * @return true if successful (does not necessarily mean that all images were actually loaded)
	 */
	static bool preloadImages(TTTPositionManager* tttpm, unsigned int _timePoint, unsigned int _loadBefore, unsigned int _loadAfter, unsigned int _wl = 0);

	/**
	 * Check if the tree that belongs to the provided trackpoint starts before the provided timepoint
	 * @param _trackPoint pointer to the trackpoint
	 * @param _timePoint timepoint
	 * @return true if the first timepoint of the associated tree is before _timePoint, false if not or if an error occurred
	 */
	static bool treeStartsBeforeTimePoint(const ITrackPoint* _trackPoint, int _timePoint);

	// Menubar
	TreeWindowMenu* menuBar;

	// Current time point
	int currentTimePoint;

	// Current position
	int currentPosition;

	// Currently displayed trees
	QList<QSharedPointer<TreeDescriptor> > currentTrees;

	// Currently selected tree, i.e. tree on which the user has last clicked or first tree (not using
	// index to currentTrees, since index would have to be updated every time currentTrees.size changes)
	QSharedPointer<TreeDescriptor> selectedTree;

	// Currently selected track
	Track* selectedTrack;

	// If tree has been modified
	bool treeModified;

	// Statusbar and status bar controls
	QStatusBar* statusBar;
	QLabel* lblBackwardTrackingMode;			// Statusbar widget: Backward tracking mode on or off
	QPushButton* pbtStatusBarOpenMovieWindow;	// Statusbar widget: Open movie window of current position
	QPushButton* pbtStatusBarGoToFirstPos;		// Statusbar widget: Select position where current tree starts

	// StringList model for used fragments list
	QStringListModel* usedFragmentsModel;

	// GUI
	Ui::FrmAutoTrackingTreeWindow ui;

	// TTTAutoTracking window
	TTTAutoTracking* frmAutoTracking;

	//// Currently edited track and corresponding tree descriptor, i.e. the selected track for
	//// which the user can find successor TreeFragments. If this is set, it is equal to the selected
	//// track, but the selected track can also be set if this is not set.
	//Track* currentlyEditedTrack;
	//QSharedPointer<TreeDescriptor> currentlyEditedTree;

	// Old tree in TTTManager
	Tree* oldTTTManagerTree;
	Track* oldTTTManagerTrack;

	//// Currently displayed fragments and trees that are possible followers of currentlyEditedTrack
	//QList<QSharedPointer<TreeFragment> > possibleTreeFragmentSuccessors;
	//QList<QSharedPointer<Tree> > possibleTreeSuccessors;

	// Currently displayed wavelength selection buttons with wl as key
	QHash<int, QPushButton*> wlButtons;

	// Connect tree fragments tool
	TTTAutotrackingConnectFragments* m_connectTreesTool;

	// During saving of tree fragments: Number of exported tree fragments.
	int numExported;

	// Flags used by saveChangesToTree(). Indicate if user clicked 'yes to all' or 'no to all'
	bool yesToAllAtivated;
	bool noToAllAtivated;

	// Tree editing assistant: tree currently used by assistant
	QWeakPointer<TreeDescriptor> assistantCurrentTree;

	// Tree editing assistant: track currently used by assistant 
	int assistantCurrentTrack;

	// Tree editing assistant: current temporal region (no region selected if any value is < 1)
	int assistantStartTp;
	int assistantStopTp;

	// If last manual tracking was done in auto tracking mode (i.e. a tree in this window was tracked) or independently from autotracking
	bool m_lastManualTrackingInAutoTrackingMode;

	// Model for fragments overview
	TTTAutoTrackingTreeWindowFragmentsTableModel* m_fragmentsOverviewModel;

	// Temporal range to look for starting fragments int timepoint
	static const int FRAGMENTS_TEMPORAL_SEARCH_RANGE = 50;

	// Indexes of toolbox pages
	static const int TOOLBOX_INDEX_LOADIMAGES = 0;

	// Menubar is tightly connected to this class
	friend TreeWindowMenu;
	friend TTTMovie;
};



/**
 * Model for table of tree fragments. Holds a list of QWeakPointer instances to the 
 * QSharedPointer instances of tree fragments owned by TTTAutoTracking. List can be sorted.
 */
class TTTAutoTrackingTreeWindowFragmentsTableModel : public QAbstractTableModel {

	Q_OBJECT

public:

	/**
	 * Constructor.
	 */
	TTTAutoTrackingTreeWindowFragmentsTableModel(QObject* parent = 0)
		: QAbstractTableModel(parent)
	{}

	/**
	 * Set shown fragments.
	 */
	void setFragments(const QHash<int, QList<QSharedPointer<TreeFragment> > >& fragments);

	/**
	 * Get list of shown fragments.
	 */
	const QList<QWeakPointer<TreeFragment> >& getFragments() const
	{
		return m_fragments;
	}

	/**
	 * Reimplemented.
	 */
	int	columnCount ( const QModelIndex& parent = QModelIndex() ) const
	{
		return NUM_COLUMNS;
	}
	int	rowCount ( const QModelIndex& parent = QModelIndex() ) const
	{
		return m_fragments.size();
	}
	QVariant data ( const QModelIndex& index, int role = Qt::DisplayRole ) const;
	QVariant headerData ( int section, Qt::Orientation orientation, int role = Qt::DisplayRole ) const;
	void sort ( int column, Qt::SortOrder order = Qt::AscendingOrder );

	/**
	 * Internal sort function, does not notify view about model changes.
	 */
	void sortInternal ( int column, Qt::SortOrder order = Qt::AscendingOrder );

private:


	/**
	 * List of all fragments.
	 */
	QList<QWeakPointer<TreeFragment> > m_fragments;


	/*
	 *	Header indexes.
	 */
	static const int NUM_COLUMNS = 4;
	enum HEADERS {
		HLength = 0,
		HPos = 1,
		HStartTp = 2,
		//HStopTp = 3,
		HNR = 3
	};
};


#endif // autotrackingtreewindow_h__
