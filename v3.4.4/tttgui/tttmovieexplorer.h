/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef tttmovieexplorer_h__
#define tttmovieexplorer_h__

#include "ui_frmMovieExplorer.h"

// Qt
#include <QWidget>
#include <QHash>
#include <QXmlStreamAttributes>
#include <QProgressDialog>


/**
 * Movie explorer
 *
 * Gives an overview of all TATEXP.XML files in a given directory
 */
class TTTMovieExplorer : public QWidget {

	Q_OBJECT

public:

	/**
	 * Constructor
	 * @paramt _selectable if true, there are checkboxes for each experiment and a "Load Experiment" button,
	 *                     you can connect to the loadExperiment(QMap<QString,QString>) signal to get notified
	 */
	TTTMovieExplorer(QWidget* _parent = 0, bool _selectable = false);

	/**
	 * Sets the folder
	 */
	void setFolder(QString _folder);

	/**
	 * Gets the currently selected folder
	 */
	QString getFolder() const;

	/**
	 * Sets if the search for xml files is done recursively
	 */
	void setRecursive(bool _recursive);

	/**
	 * Gets the current value of recursive
	 */
	bool getRecursive() const;


signals:

	/**
	 * Signal emitted when the user clicks the "Load Experiments" button
	 * The QStringList contains the folders with logFiles for the experiment
	 */
	void loadExperiments(QStringList _selectedExperimentFolders);

public slots:

	/**
	 * Parse all files in the currently selected directory
	 * or (if the recursive checkbox is checked) also in all
	 * subdirectories
	 */
	void parseFiles();


private slots:

	/**
	 * Called if the user clicks "LoadExperiments"
	 * This methode emits the loadExperiment signal
	 */
	void loadExperiments();

	/**
	 * Select a folder
	 */
	void selectFolder();

	/**
	 * Find text
	 */
	void findText();

private:

	/**
	 * Initialize, i.e. get the keys and the display names and set the column headers accordingly
	 */
	void initialize();

	/**
	 * Clear current contents
	 */
	void clearContents();

	/**
	 * Parse a single file
	 * @param _fileName filename (complete with path)
	 * @param _rowIndex index of row into which parsed data should be put
	 */
	void parseSingleFile(const QString& _fileName, int _rowIndex);

	/**
	 * Helper function to parse the attributes of a xml element
	 * @param _curRow current row
	 * @param _key key of the xml element
	 * @param _attributes the attributes
	 * @param _keyCount hashmap with counter of found keys
	 */
	void parseAttributes(int _curRow, const QString& _key, const QXmlStreamAttributes& _attributes, QHash<QString, int>& _keyCount);

	/**
	 * Helper function which goes through the directory _dir and returns a
	 * list of all files ending with one of the extensions in _extensions.
	 * It also takes the current status of the recursive checkBox into account.
	 *
	 * @return QMap, Key: name of the found file, Value: folder of the file
	 */
	QMap<QString, QString> findFiles(const QString& _dir, const QStringList& _extensions) const;

	/**
	 * Helper function which goes through the directory _dir and returns a
	 * number of all files ending with one of the extensions in _extensions
	 * in all the _dir subfolders.
	 * It is used to return the number of tracked trees per experiment
	 *
	 * @return int, number of files with extensions in _extensions
	 */
	int TTTMovieExplorer::findNumberOfTrees(const QString& _dir, const QStringList& _extensions);

	/**
	 * Get default settings
	 * @param _keys string list with keys (e.g. 'TATSettings.CellsAndConditions.Flask.value0', compare entriesToDisplay)
	 * @param _displayNames corresponding column names (e.g. 'Flask')
	 */
	static void getDefaultSettings(QStringList& _keys, QStringList& _displayNames);

	// Hash table with keys for entries from the xml files that should be displayed (e.g. 'TATSettings.CellsAndConditions.Flask.value0') and
	// the corresponding row numbers as values
	QHash<QString, int> entriesToDisplay;

	// Items found in findText() and current index
	QList<QTableWidgetItem *> foundItems;
	int foundItemsIndex;
	QString oldSearchTerm;

	// Selectable? is the user able to select experiments and click Load Experiments?
	const bool selectable;

	// Initialized?
	bool initialized;

	// GUI
	Ui::frmMovieExplorer ui;

};


#endif // tttmovieexplorer_h__
