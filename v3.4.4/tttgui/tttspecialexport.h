/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TTT_SPECIALEXPORT
#define TTT_SPECIALEXPORT

#include "ui_frmSpecialExport.h"

#include <QDialog>


class TTTMovie;


/**
@author Oliver Hilsenbeck
*	
*	Little gui to help users perform special exports
*/

class TTTSpecialExport : public QDialog, public Ui::frmSpecialExport {

Q_OBJECT

public:
	TTTSpecialExport(TTTMovie *parent);

	///set text of status label
	void setStatusText(QString newStatus);

private:
	///populate gui with data from txt file
	void loadDataFromTxtFile();

	///save data from gui to txt file
	bool saveTxtFile();

	///add line to table
	void addLineToTable(int tp1, 
			int tp2, 
			int picInterval, 
			int repeats, 
			QString wavelengths,
			int centerX, 
			int centerY, 
			int zoom, 
			bool showTree,
			QString showTracks,
			bool inPosLayout);

	///add a single item
	void addItemToTable(QString itemText, int row, int col);

	///get a single item
	QString getItemFromTable(int row, int col);

	///check wavelengths string
	bool validWaveLengths(const QString& wls);

private slots:
	///perform special export
	void performExport();

	///add item
	void addItem();

	///remove item
	void removeItem();

private:
	TTTMovie* movieWindow;
};

#endif