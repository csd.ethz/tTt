/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef loadpicsfrommorepositions_h__
#define loadpicsfrommorepositions_h__

// Project includes
#include "ui_frmLoadPicsFromMorePositions.h"

// Qt includes
#include <QDialog>
#include <QList>

// Forward declarations
class PositionDisplay;


/**
 * Load pictures from arbitrary positions.
 * Based on code of TTTLoadDwellingPictures
 */
class TTTLoadPicsFromMorePositions : public QDialog {

Q_OBJECT

public:
	// Constructor
	TTTLoadPicsFromMorePositions(QWidget *_parent);
	
	// Events
	void resizeEvent (QResizeEvent *);
	void closeEvent (QCloseEvent *);
	void showEvent (QShowEvent *_ev);

private slots:
	
	/**
	 * starts the loading process for the selected pictures/positions
	 * called by a click on the load pictures button
	 */
	void startLoading();
	
	/**
	 * starts the unloading process for the selected pictures/positions
	 * called by a click on the unload pictures button
	 */
	void startUnloading();

	/**
	 * called by positionView, when a left-click is performed
	 * activates the selected positions (pictures are loaded for this one)
	 * @param _index the index of the position to be shown
	 */
	void activatePosition (const QString& _index);
	
	/**
	 * called by positionView, when a right-click is performed
	 * deactivates the selected positions (pictures are not loaded for this one)
	 * @param _index the index of the position to be shown
	 */
	void deactivatePosition (const QString& _index);

	/**
	 * Position in combobox selected
	 */
	void comboboxSelectionChanged(const QString& _text);
	
	
private:

	/**
	 * Get wavelengths specified by user
	 * @param _wls vector to save wavelength numbers
	 */
	void getWLs(QVector<unsigned int>& _wls);

	/**
	 * Update lblSelectedPositions
	 */
	void updateSelectedPositionsLabel();

	/**
	 * Init combobox
	 */
	void initSelectionComboBox();
	
	/////the widget containing the thumbnails of the positions
	//PositionDisplay *positionView;

	// Selected position
	QList<QString> selectedPositions;

	///window layout already loaded
	bool windowLayoutLoaded;

	Ui::frmLoadPicsFromMorePositions ui;
};


#endif // loadpicsfrommorepositions_h__