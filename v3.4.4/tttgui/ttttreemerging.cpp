/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ttttreemerging.h"

#include "tttbackend/tttmanager.h"
#include "tttio/tttfilehandler.h"
#include "tttbackend/tools.h"

#include <q3filedialog.h>
#include <qpushbutton.h>
#include <qradiobutton.h>
#include <qspinbox.h>
#include <qlineedit.h>
#include <qcheckbox.h>
#include <q3groupbox.h>
#include <qlabel.h>

#include <time.h> 
#include <stdlib.h>

TTTTreeMerging::TTTTreeMerging(QWidget *parent, const char *name)
    :QWidget(parent, name)
{
        setupUi (this);

	connect ( pbtChooseNewTree, SIGNAL (clicked()), this, SLOT (chooseTreeName()));
	connect ( pbtChooseTreeA, SIGNAL (clicked()), this, SLOT (chooseTreeName()));
	connect ( pbtChooseTreeB, SIGNAL (clicked()), this, SLOT (chooseTreeName()));
	
	connect ( rdbIntoNewTree, SIGNAL (toggled (bool)), this, SLOT (selectMergingMode (bool)));
	connect ( rdbTreeAIntoB, SIGNAL (toggled (bool)), this, SLOT (selectMergingMode (bool)));
	connect ( rdbTreeBIntoA, SIGNAL (toggled (bool)), this, SLOT (selectMergingMode (bool)));
	
	connect ( pbtMerge, SIGNAL (clicked()), this, SLOT (mergeTrees()));
	connect ( pbtClose, SIGNAL (clicked()), this, SLOT (close()));
	connect ( pbtTest, SIGNAL (clicked()), this, SLOT (specialMerging()));
	connect ( pbtRandomProperties, SIGNAL (clicked()), this, SLOT (randomlyAddCellAttributes()));
	connect ( pbtCutOffEnds, SIGNAL (clicked()), this, SLOT (cutOffEnds()));
	
	rdbIntoNewTree->setChecked (true);
	chkCreateBackups->setChecked (true);
	chkDeleteOldTree->setChecked (false);
	
	spbCellTreeFrom->setMinValue (1);
	spbCellTreeInto->setMinValue (1);
}

void TTTTreeMerging::chooseTreeName()
{
	bool allowNewFiles = false;
	
	if (sender() == pbtChooseNewTree)
		allowNewFiles = true;
	
	QString filename = "";
	
	QString sugdir = "";
	if (TTTManager::getInst().getCurrentPositionManager())
		sugdir = TTTManager::getInst().getCurrentPositionManager()->getTTTFileDirectory();
	
	
	if (allowNewFiles)
		filename = Q3FileDialog::getSaveFileName (sugdir, "Tree files (*.ttt)", this, 0, "Choose a non-existing file");
	else
		filename = Q3FileDialog::getOpenFileName (sugdir, "Tree files (*.ttt)", this, 0, "Choose a tree file");
	
	if (filename.isEmpty())
		//user cancelled the action 
		return;
	
	if (sender() == pbtChooseNewTree)
		lieNewTree->setText (filename);
	else if (sender() == pbtChooseTreeA) {
		lieTreeA->setText (filename);
	}
	else if (sender() == pbtChooseTreeB) {
		lieTreeB->setText (filename);
	}
	
}

void TTTTreeMerging::selectMergingMode (bool _on)
{
	if (_on) {
		if (sender() == rdbIntoNewTree) {
			grbIntoOptions->hide();
			grbNewTreeOptions->show();
			
			mergingMode = MM_INTO_NEW_TREE;
		}
		else if (sender() == rdbTreeAIntoB) {
			grbIntoOptions->show();
			grbNewTreeOptions->hide();
			
			lblOfTree->setText ("of tree A into cell");
			lblIntoTree->setText ("of tree B");
			
			mergingMode = MM_A_INTO_B;
		}
		else if (sender() == rdbTreeBIntoA) {
			grbIntoOptions->show();
			grbNewTreeOptions->hide();
			
			lblOfTree->setText ("of tree B into cell");
			lblIntoTree->setText ("of tree A");
			
			mergingMode = MM_B_INTO_A;
		}
		
		
		if ((mergingMode == MM_B_INTO_A) || (mergingMode == MM_A_INTO_B)) {
			//load trees A and B and set its cell number range
			
			int maxCellFrom = 1000, maxCellInto = 1000;
			
			QString treeAFilename = lieTreeA->text();
			QString treeBFilename = lieTreeB->text();
			
			if ((! treeAFilename.isEmpty()) && (! treeBFilename.isEmpty())) {
				//read trees A and B
				int trackCount;
				
				TTTPositionManager *tttpmA = 0, *tttpmB = 0;
				Tree *treeA = new Tree(), *treeB = new Tree();
				int firstTP = -1;
				int lastTPA = -1, lastTPB = -1;
				
				QString index = Tools::getPositionIndex (treeAFilename);
				tttpmA = TTTManager::getInst().getPositionManager (index);
				if (tttpmA) {
                                        if (TTTFileHandler::readFile (treeAFilename, treeA, trackCount, firstTP, lastTPA, &tttpmA->positionInformation) == TTT_READING_SUCCESS)
						//loading tree A succeeded
						maxCellFrom = treeA->getMaxPossibleTracks();
				}
				
				index = Tools::getPositionIndex (treeBFilename);
				tttpmB = TTTManager::getInst().getPositionManager (index);
				if (tttpmB) {
					firstTP = -1;
                                        if (TTTFileHandler::readFile (treeBFilename, treeB, trackCount, firstTP, lastTPB, &tttpmB->positionInformation) == TTT_READING_SUCCESS)
						//loading tree B succeeded
						maxCellInto = treeB->getMaxPossibleTracks();
				}
				
				if (mergingMode == MM_B_INTO_A)
					qSwap (maxCellFrom, maxCellInto);
				
				spbCellTreeFrom->setMaxValue (maxCellFrom);
				spbCellTreeInto->setMaxValue (maxCellInto);
			}
		}
	}
}

void TTTTreeMerging::mergeTrees()
{
	bool deleteOldTree = chkDeleteOldTree->isChecked();
	bool createBackups = chkCreateBackups->isChecked();
	
	int cellTreeFrom = spbCellTreeFrom->value();
	int cellTreeInto = spbCellTreeInto->value();
	
	QString treeAFilename = lieTreeA->text();
	QString treeBFilename = lieTreeB->text();
	QString newTreeName = lieNewTree->text();
	
	
	TTTPositionManager *tttpmA = 0, *tttpmB = 0;
	Tree *treeA = new Tree(), *treeB = new Tree();
	int firstTP = -1;
	int lastTPA = -1, lastTPB = -1;
	
	bool error = false;
	
	if (treeAFilename.isEmpty() || treeBFilename.isEmpty()) {
		//tree names not given - error
		error = true;
	}
	
	if (! error) {
		//read trees A and B
		int trackCount;
		
		QString index = Tools::getPositionIndex (treeAFilename);
		tttpmA = TTTManager::getInst().getPositionManager (index);
		if (tttpmA) {
                        if (TTTFileHandler::readFile (treeAFilename, treeA, trackCount, firstTP, lastTPA, &tttpmA->positionInformation) != TTT_READING_SUCCESS)
				//loading tree A failed
				error = true;
		}
		
		index = Tools::getPositionIndex (treeBFilename);
		tttpmB = TTTManager::getInst().getPositionManager (index);
		if (tttpmB) {
			firstTP = -1;
                        if (TTTFileHandler::readFile (treeBFilename, treeB, trackCount, firstTP, lastTPB, &tttpmB->positionInformation) != TTT_READING_SUCCESS)
				//loading tree B failed
				error = true;
		}
		
		if ((lastTPA != lastTPB)) {
			//two trees from different experiments cannot (currently) be merged
			error = true;
		}
	}
	
	
	if (error) {
		QMessageBox::information (this, "tTt error", "Please specify two existing trees from the same experiment to be merged.", "Ok");
	}
	else {
		if (createBackups) {
			//create backups, if desired; add random numbers for unification
			TTTFileHandler::saveFile (treeAFilename + "_backup_before_merge" + QString ("%1").arg (rand()), treeA, firstTP, lastTPA, treeA->getFileVersion());
			TTTFileHandler::saveFile (treeBFilename + "_backup_before_merge" + QString ("%1").arg (rand()), treeB, firstTP, lastTPB, treeB->getFileVersion());
		}
		
		if (mergingMode == MM_INTO_NEW_TREE) {
			error = false;
			if (newTreeName.isEmpty()) {
				//new tree name not given - error
				QMessageBox::information (this, "tTt error", "Please specify a filename for the new tree", "Ok");
				error = true;
			}
			else {
				if (QFile::exists (newTreeName)) {
					//new tree file already exists - error
					QMessageBox::information (this, "tTt error", "New tree file already exists!\nPlease specify a non-existing filename for the new tree", "Ok");
					error = true;
				}
			}
			
			if (! error) {
				//now we can work...
				
				//create a new tree by joining treeA and treeB
				Tree *treeNew = new Tree();
				
				Track *baseTrackNew = new Track (0, 0, lastTPA, -1);
				baseTrackNew->setNumber (1);
				treeNew->insert (baseTrackNew);
				
				Track::mergeTrees (treeA, treeB, 1, 1, treeNew);
				
				//save the newly created tree with the provided filename
				bool success = TTTFileHandler::saveFile (newTreeName, treeNew, firstTP, lastTPA);
				
				if (success && deleteOldTree) {
					QFile::remove (treeAFilename);
					QFile::remove (treeBFilename);
				}
				
				if (treeNew)
					delete treeNew;
			}
			else
				error = false;
		}
		else if (mergingMode == MM_A_INTO_B) {
			bool success = Track::mergeTrees (treeA, treeB, cellTreeFrom, cellTreeInto, false);
			
			//save the modified tree B (A was merged into it)
			if (success)
				success = TTTFileHandler::saveFile (treeBFilename, treeB, firstTP, lastTPB);
			else
				error = true;
				
			if (success && deleteOldTree) {
				QFile::remove (treeAFilename);
			}
		}
		else if (mergingMode == MM_B_INTO_A) {
			bool success = Track::mergeTrees (treeB, treeA, cellTreeFrom, cellTreeInto, false);
			
			//save the modified tree A (B was merged into it)
			if (success)
				success = TTTFileHandler::saveFile (treeAFilename, treeA, firstTP, lastTPA);
			else
				error = true;
			
			if (success && deleteOldTree) {
				QFile::remove (treeBFilename);
			}
		}
	}
	
	if (error) {
		QMessageBox::information (this, "Merging problem", "Merging the two trees failed due to an unknown reason.\nPlease contact the programmer.", "Ok");
	}
	
	if (treeA)
		delete treeA;
	if (treeB)
		delete treeB;
	
}

void TTTTreeMerging::specialMerging()
{
	// Open trees
	/*
	QString treeAFilename = lieTreeA->text();
	QString treeBFilename = lieTreeB->text();
	QString newTreeName = lieNewTree->text();
	if(treeAFilename.isEmpty() || treeBFilename.isEmpty() || newTreeName.isEmpty()) {
		QMessageBox::critical(this, "Error", "Pleas specify Tree A, Tree B and result tree name.");
		return;
	}
	*/
	// DEBUG

	//QString treeAFilename = "D:/scd/MethodsPaper/Trees/Source/111115AF6_p0004-001AF Hiwi.ttt";
	//QString treeBFilename = "D:/scd/MethodsPaper/Trees/Source/111115AF6_p0004-001AF SH.ttt";
	//QString newTreeName = "D:/scd/MethodsPaper/Trees/Result/_p0004_Hiwi_INTO_SH.ttt";

	QString treeAFilename = "D:/scd/MethodsPaper/Trees/Source/111115AF6_p0004-001PH PH.ttt";
	QString treeBFilename = "D:/scd/MethodsPaper/Trees/Result/_p0004_Hiwi_INTO_SH.ttt";
	QString newTreeName = "D:/scd/MethodsPaper/Trees/Result/ALL_MERGED.ttt";
	QString index = Tools::getPositionIndex (treeAFilename);
	TTTPositionManager* tttpmA = TTTManager::getInst().getPositionManager (index);
	index = Tools::getPositionIndex (treeBFilename);
	TTTPositionManager* tttpmB = TTTManager::getInst().getPositionManager (index);
	if(!tttpmA  || !tttpmB) {
		QMessageBox::critical(this, "Error", "Invalid positions");
		return;
	}
	QSharedPointer<Tree> treeA(new Tree());
	QSharedPointer<Tree> treeB(new Tree());
	treeA->reset();
	treeB->reset();
	int numTracksA, firstTimePointA, lastTimePointA, numTracksB, firstTimePointB, lastTimePointB;
	if(TTTFileHandler::readFile(treeAFilename, treeA.data(), numTracksA, firstTimePointA, lastTimePointA, &tttpmA->positionInformation, false, false) != TTT_READING_SUCCESS) {
		QMessageBox::critical(this, "Error", "Cannot open tree 1");
		return;
	}
	if(TTTFileHandler::readFile(treeBFilename, treeB.data(), numTracksB, firstTimePointB, lastTimePointB, &tttpmB->positionInformation, false, false) != TTT_READING_SUCCESS) {
		QMessageBox::critical(this, "Error", "Cannot open tree 1");
		return;
	}

	// For every track in B check if A contains additional track points or daughter cells
	std::vector<Track*> tracksStack;
	tracksStack.push_back(treeB->getBaseTrack());
	while(tracksStack.size()) {
		// Take next track
		Track* curTrack = tracksStack.back();
		tracksStack.pop_back();
		if(!curTrack)
			continue;

		// Check if A contains track with same number that has more track points or children that B does not have
		Track* curTrackInA = treeA->getTrack(curTrack->getTrackNumber());
		bool curTrackChanged = false;
		if(curTrackInA) {
			bool AHasChildren = curTrackInA->hasChildren();
			bool BHasChildren = curTrack->hasChildren();
			if(curTrackInA->getLastTrace() > curTrack->getLastTrace() || (AHasChildren && !BHasChildren)) {
				curTrackChanged = true;
				int cutTimePoint = curTrack->getLastTrace();
				if(cutTimePoint == -1) {
					// curTrack is obviously empty so take first trace of curTrackInA
					cutTimePoint = curTrackInA->getFirstTrace();
				}
				if(cutTimePoint > 0) {
					QSharedPointer<Tree> tmpTree = curTrackInA->cutTrack(cutTimePoint);
					if(tmpTree) {
						QString err;
						if(!curTrack->appendTree(tmpTree.data(), true, &err))
							qWarning() << "Warning: cannot append track with number " << curTrackInA->getTrackNumber() << " - error: " << err;
					}
					else {
						qWarning() << "Warning: cannot cut track number " << curTrackInA->getTrackNumber();
					}
				}
				else
					qWarning() << "Warning: cannot get cut time point for track " << curTrackInA->getTrackNumber();
			}
			else if(curTrack->getStopReason() == TS_NONE && !BHasChildren && curTrackInA->getStopReason() != TS_NONE) 
				curTrack->setStopReason(curTrackInA->getStopReason());
		}

		// Add children to stack
		if(!curTrackChanged) {
			if(Track* child1 = curTrack->getChild1())
				tracksStack.push_back(child1);
			if(Track* child2 = curTrack->getChild2())
				tracksStack.push_back(child2);
		}
	}

	// Save tree B
	if(!TTTFileHandler::saveFile(newTreeName, treeB.data(), firstTimePointB, lastTimePointB))
		QMessageBox::critical(this, "Error", "Cannot save result tree");
}

void TTTTreeMerging::randomlyAddCellAttributes()
{
	// Open trees
	/*
	QString treeAFilename = lieTreeA->text();
	QString treeBFilename = lieTreeB->text();
	QString newTreeName = lieNewTree->text();
	if(treeAFilename.isEmpty() || treeBFilename.isEmpty() || newTreeName.isEmpty()) {
		QMessageBox::critical(this, "Error", "Pleas specify Tree A, Tree B and result tree name.");
		return;
	}

	QString index = Tools::getPositionIndex (treeAFilename);
	*/
	// DEBUG

	//QString treeAFilename = "D:/scd/MethodsPaper/Trees/Source/111115AF6_p0004-001AF Hiwi.ttt";
	//QString treeBFilename = "D:/scd/MethodsPaper/Trees/Source/111115AF6_p0004-001AF SH.ttt";
	//QString newTreeName = "D:/scd/MethodsPaper/Trees/Result/_p0004_Hiwi_INTO_SH.ttt";

	//QString treeAFilename = "D:/scd/MethodsPaper/Trees/Result/ALL_MERGED.ttt";
	//QString newTreeName = "D:/scd/MethodsPaper/Trees/Result/ALL_MERGED_EDITED.ttt";
	QString index = "0009";
	QString treeAFilename = lieTreeA->text();
	QString newTreeName = lieNewTree->text();
	if(QMessageBox::question(this, "merge", QString("Add attributes to %1 and store as %2?").arg(treeAFilename).arg(newTreeName), QMessageBox::Yes | QMessageBox::No) != QMessageBox::Yes)
		return;

	TTTPositionManager* tttpmA = TTTManager::getInst().getPositionManager (index);
	if(!tttpmA) {
		QMessageBox::critical(this, "Error", "Invalid position");
		return;
	}
	QSharedPointer<Tree> treeA(new Tree());
	treeA->reset();
	int numTracksA, firstTimePointA, lastTimePointA;
	if(TTTFileHandler::readFile(treeAFilename, treeA.data(), numTracksA, firstTimePointA, lastTimePointA, &tttpmA->positionInformation, false, false) != TTT_READING_SUCCESS) {
		QMessageBox::critical(this, "Error", "Cannot open tree 1");
		return;
	}

	// For every track in A add random attributes (also to all children) if they are not there yet
	qsrand(time(NULL));
	std::vector<Track*> tracksStack;
	tracksStack.push_back(treeA->getBaseTrack());
	while(tracksStack.size()) {
		// Take next track
		Track* curTrack = tracksStack.back();
		tracksStack.pop_back();
		if(!curTrack)
			continue;
		TrackPoint* lastTrackPoint = curTrack->getTrackPointByTimePoint(curTrack->getLastTrace());

		// Only consider cells with cell number >= 4
		if(curTrack->getFirstTrace() > 0 && lastTrackPoint && curTrack->getNumber() >= 4) {
			// Wavelengths
			for(int wl = 1; wl <= 5; ++wl) {
				if(lastTrackPoint->WaveLength[wl])
					continue;

				// Add wl with probability 10%
				int r = qrand() % 100;
				if(r < 10) {
					// Get start time point
					int lifeTime = std::max(curTrack->getLastTrace() - curTrack->getFirstTrace(), 1);
					int startTimePoint = curTrack->getFirstTrace() + qrand() % lifeTime;

					CellProperties props = lastTrackPoint->getPropObject();
					props.WaveLength[wl] = 1;
					std::vector<Track*> tracksToChange;
					tracksToChange.push_back(curTrack);
					while(tracksToChange.size()) {
						// Take next track
						Track* curTrackToChange = tracksToChange.back();
						tracksToChange.pop_back();

						// Change track
						int curStartTimePoint = std::max(startTimePoint, curTrackToChange->getFirstTrace());
						for(int tp = curStartTimePoint; tp <= curTrackToChange->getLastTrace(); ++tp)
							curTrackToChange->setProperties(tp, props, false, true, true);

						// Add children to stack
						if(Track* child1 = curTrackToChange->getChild1())
							tracksToChange.push_back(child1);
						if(Track* child2 = curTrackToChange->getChild2())
							tracksToChange.push_back(child2);
					}
				}
			}

			// Set adherence states (semi-adherent or free floating)
			if(!lastTrackPoint->NonAdherent && !lastTrackPoint->FreeFloating) {
				for(int i = 0; i < 2; ++i) {
					// Add wl with probability 10%
					int r = qrand() % 100;
					if(r < 10) {
						// Get start time point
						int lifeTime = std::max(curTrack->getLastTrace() - curTrack->getFirstTrace(), 1);
						int startTimePoint = curTrack->getFirstTrace() + qrand() % lifeTime;

						CellProperties props = lastTrackPoint->getPropObject();
						if(i == 0) {
							props.NonAdherent = 1;
							props.FreeFloating = 0;
						}
						else {
							props.NonAdherent = 1;
							props.FreeFloating = 1;
						}
						std::vector<Track*> tracksToChange;
						tracksToChange.push_back(curTrack);
						while(tracksToChange.size()) {
							// Take next track
							Track* curTrackToChange = tracksToChange.back();
							tracksToChange.pop_back();

							// Change track
							int curStartTimePoint = std::max(startTimePoint, curTrackToChange->getFirstTrace());
							for(int tp = curStartTimePoint; tp <= curTrackToChange->getLastTrace(); ++tp)
								curTrackToChange->setProperties(tp, props, false, true, true);

							// Add children to stack
							if(Track* child1 = curTrackToChange->getChild1())
								tracksToChange.push_back(child1);
							if(Track* child2 = curTrackToChange->getChild2())
								tracksToChange.push_back(child2);
						}

						break;
					}
				}
			}
		}

		// Add children to stack
		if(Track* child1 = curTrack->getChild1())
			tracksStack.push_back(child1);
		if(Track* child2 = curTrack->getChild2())
			tracksStack.push_back(child2);
	}

	// Save tree
	if(!TTTFileHandler::saveFile(newTreeName, treeA.data(), firstTimePointA, lastTimePointA))
		QMessageBox::critical(this, "Error", "Cannot save result tree");
}

void TTTTreeMerging::cutOffEnds(int maxTimePoint)
{
	// Open trees
	/*
	QString treeAFilename = lieTreeA->text();
	QString treeBFilename = lieTreeB->text();
	QString newTreeName = lieNewTree->text();
	if(treeAFilename.isEmpty() || treeBFilename.isEmpty() || newTreeName.isEmpty()) {
		QMessageBox::critical(this, "Error", "Pleas specify Tree A, Tree B and result tree name.");
		return;
	}

	QString index = Tools::getPositionIndex (treeAFilename);
	*/
	// DEBUG

	//QString treeAFilename = "D:/scd/MethodsPaper/Trees/Source/111115AF6_p0004-001AF Hiwi.ttt";
	//QString treeBFilename = "D:/scd/MethodsPaper/Trees/Source/111115AF6_p0004-001AF SH.ttt";
	//QString newTreeName = "D:/scd/MethodsPaper/Trees/Result/_p0004_Hiwi_INTO_SH.ttt";

	// For Fig1 of methods paper:
	//QString treeAFilename = "D:/Users/Mitz/Dropbox/NatMehodsPaper/Figure1/Trees/Result/ALL_MERGED_EDITED.ttt";
	//QString newTreeName = "D:/Users/Mitz/Dropbox/NatMehodsPaper/Figure1/Trees/Result/ALL_MERGED_EDITED_CUT.ttt";
	//QString index = "0004";

	// For reviewer package of methods paper:
	//QString treeAFilename = "D:/Users/Mitz/Dropbox/NatMehodsPaper/Backups/131103/111115AF6_p0003-001AF.ttt";
	//QString newTreeName = "D:/scd/MethodsPaper/Tmp/111115AF6_p0003-001AF.ttt";
	//QString index = "0003";
	QString treeAFilename = "D:/Users/Mitz/Dropbox/NatMehodsPaper/Backups/131103/111115AF6_p0004-001AF.ttt";
	QString newTreeName = "D:/scd/MethodsPaper/Tmp/111115AF6_p0004-001AF.ttt";
	QString index = "0004";

	TTTPositionManager* tttpmA = TTTManager::getInst().getPositionManager (index);
	if(!tttpmA) {
		QMessageBox::critical(this, "Error", "Invalid position");
		return;
	}
	QSharedPointer<Tree> treeA(new Tree());
	treeA->reset();
	int numTracksA, firstTimePointA, lastTimePointA;
	if(TTTFileHandler::readFile(treeAFilename, treeA.data(), numTracksA, firstTimePointA, lastTimePointA, &tttpmA->positionInformation, false, false) != TTT_READING_SUCCESS) {
		QMessageBox::critical(this, "Error", "Cannot open tree 1");
		return;
	}

	// For every track in A add random attributes (also to all children) if they are not there yet
	qsrand(time(NULL));
	std::vector<Track*> tracksStack;
	tracksStack.push_back(treeA->getBaseTrack());
	while(tracksStack.size()) {
		// Take next track
		Track* curTrack = tracksStack.back();
		tracksStack.pop_back();
		if(!curTrack)
			continue;

		// Cut track if necessary
		bool trackChanged = false;
		if(curTrack->getLastTrace() > maxTimePoint) {
			// Cut off end and discard it
			QSharedPointer<Tree> tmpTree = curTrack->cutTrack(maxTimePoint+1);
			curTrack->setStopReason(TS_NONE);
			if(!tmpTree) 
				qWarning() << "Warning: could not cut track " << curTrack->getTrackNumber();
		}

		// Add children to stack
		if(!trackChanged) {
			if(Track* child1 = curTrack->getChild1())
				tracksStack.push_back(child1);
			if(Track* child2 = curTrack->getChild2())
				tracksStack.push_back(child2);
		}
	}

	// Save tree
	if(!TTTFileHandler::saveFile(newTreeName, treeA.data(), firstTimePointA, lastTimePointA))
		QMessageBox::critical(this, "Error", "Cannot save result tree");
}




//#include "ttttreemerging.moc"
