/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TTTSPLASHSCREEN_H
#define TTTSPLASHSCREEN_H


#include "ui_frmSplashScreen.h"
#include "tttdata/systeminfo.h"

#include <qtimer.h>
#include <qstring.h>
#include <qlabel.h>
#include <qdir.h>
#include <qfile.h>
#include <QTextStream>
#include <qmessagebox.h>
//Added by qt3to4:
#include <QMouseEvent>
#include <QDialog>
#include <QDate>

#ifndef DEBUGMODE
const int ShowSplasherTime = 5;		//seconds
#else
const int ShowSplasherTime = 1;		//seconds
#endif


const QString Version = "3.4 Build 4";
const QString internalVersion = "3.4.4";
//date of changing to 2.3.11: 02.02.2010
//date of changing to 2.5.4:  24.03.2010
//date of changing to 2.5.5:  29.03.2010
//date of changing to 2.5.6:  15.04.2010
//date of changing to 2.5.7:  27.04.2010
//date of changing to 2.5.8:  03.05.2010
//date of changing to 2.5.9:  10.05.2010
//date of changing to 2.6.0:  09.06.2010
//date of changing to 3.0.0:  29.06.2010 -> aim: compilable in Windows and Linux
//date of changing to 3.0.1:  30.08.2010
//date of changing to 3.0.2:  01.09.2010
//date of changing to 3.0.3:  06.09.2010
//date of changing to 3.0.4:  09.09.2010
//date of changing to 3.0.5:  20.09.2010
//date of changing to 3.0.6:  30.09.2010
//date of changing to 3.0.7:  14.10.2010
//date of changing to 3.0.8:  11.11.2010
//date of changing to 3.0.9:  03.12.2010
//date of changing to 3.1.0:  31.12.2010
//date of changing to 3.1.1:  09.01.2011
//date of changing to 3.1.2:  21.02.2011
//date of changing to 3.1.3:  04.04.2011
//date of changing to 3.1.5:  01.08.2011
//date of changing to 3.1.6:  02.08.2011
//date of changing to 3.1.7:  
//date of changing to 3.1.8:  23.10.2011
//date of changing to 3.1.9:  03.12.2011
//date of changing to 3.2.0:  03.12.2011
//date of changing to 3.2.1:  02.04.2012
//date of changing to 3.2.2:  31.05.2012
//date of changing to 3.2.3:  22.07.2012
//date of changing to 3.2.4:  31.07.2012
//date of changing to 3.2.5:  04.09.2012 (internal beta)
//date of changing to 3.2.5:  29.09.2012
//date of changing to 3.2.6:  02.12.2012
//date of changing to 3.2.7:  04.02.2013
//date of changing to 3.2.8:  25.03.2013
//date of changing to 3.2.9:  03.06.2013
//date of changing to 3.3.0:  19.07.2013 (3.3.1 in change log file)
//date of changing to 3.3.1:  06.10.2013
//date of changing to 3.3.2:  28.11.2013
//date of changing to 3.3.4:  01.04.2014
//date of changing to 3.3.5:  11.04.2014
//date of changing to 3.3.6:  27.05.2014
//date of changing to 3.3.7:  12.08.2014
//date of changing to 3.3.8:  27.10.2014
//date of changing to 3.3.9:  10.01.2015
//date of changing to 3.4.0:  18.03.2015
//date of changing to 3.4.1:  22.06.2015
//date of changing to 3.4.2:  27.07.2015

//date of changing to 3.4.3:  14.04.2015	(paper revision version)
//date of changing to 3.4.4:  01.06.2016	(initial publicly available version)


//long version string, contains if 32 or 64 bit version (definition in cpp file, initialization in TTTSplashScreen constructor)
extern QString tttVersionLong;

/**
@author Bernhard
*	
*	The class for the splash screen GUI
*/

class TTTSplashScreen: public QDialog, public Ui::frmSplashScreen {

Q_OBJECT

public:
    TTTSplashScreen(QWidget *parent = 0, const char *name = 0);
    
protected:
    void mouseReleaseEvent (QMouseEvent *);

	//virtual void mousePressEvent (QMouseEvent * _event);
};

#endif
