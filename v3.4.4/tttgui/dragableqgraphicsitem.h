/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef DRAGABLEQGRAPHICSITEM_H
#define DRAGABLEQGRAPHICSITEM_H

#include "callbackitem.h"

#include <QGraphicsItem>
#include <QRectF>
#include <QPainter>
#include <QString>

enum ItemShape {DGIS_Ellipse = 0, DGIS_Line = 1};

const QColor LINECOLOR = Qt::red;
const QString GRAPHICS_ADJUST_MIME_DATA_TEXT = "gamdt";

class DragableQGraphicsItem : public QGraphicsItem, public CallbackItem
{
public:
        ///stores the currently dragged item (set here, queried in ClickableQGraphicsView)
        static DragableQGraphicsItem *draggedItem;


        DragableQGraphicsItem (ItemShape _shape);

        QRectF boundingRect() const;

        void paint (QPainter *_painter, const QStyleOptionGraphicsItem *_option, QWidget *_widget);

        /**
         * sets the end position of the line (only useful if shape == Line)
         */
        void setLineLength (float _length);

        /**
         * sets the radius of the ellipse (only useful if shape == Ellipse)
         */
        void setEllipseRadius (float _radius);

        /**
         * called by ClickableQGraphicsView when the item was dropped in there
         */
        void wasDropped (QPointF _scenePos);

protected:

        void mousePressEvent (QGraphicsSceneMouseEvent *);
        void mouseMoveEvent (QGraphicsSceneMouseEvent *);
        void mouseReleaseEvent (QGraphicsSceneMouseEvent *);

private:

        ItemShape shape;

        float lineLength;
        float ellipseRadius;

        ///the item position before the dragging started
        QPointF positionBeforeDrag;
};

#endif // DRAGABLEQGRAPHICSITEM_H
