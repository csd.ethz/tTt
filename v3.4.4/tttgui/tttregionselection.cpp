/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "tttregionselection.h"

#include "tttbackend/tttpositionmanager.h"
//Added by qt3to4:
#include <QMouseEvent>
#include <Q3ValueList>
#include <QContextMenuEvent>
#include <QKeyEvent>
#include <Q3PopupMenu>
#include <QCloseEvent>
#include <QPaintEvent>

#include "tttdata/userinfo.h"

TTTRegionSelection::TTTRegionSelection (TTTPositionManager *_positionManager, QWidget *parent, const char *name)
    :QDialog (parent, name)
{
        setupUi (this);

	positionManager = _positionManager;
	
	posMouseX = posMouseY = 0;
	ShowCells = false;

        img = 0;
        BoundingBox = 0;
        CurrentBox = 0;
		windowLayoutLoaded = false;
}

void TTTRegionSelection::init (QRect &_scale, float _zoom, QWidget *, const char *)
{
	Scale = _scale;
	ZoomFactor = _zoom;
	posMouseX = posMouseY = 0;
	ConsiderBounds = false;
	
	//calculate the form size according to zoom factor
	setGeometry (QRect (x(), y(), int(Scale.width() * ZoomFactor), int(Scale.height() * ZoomFactor)));
        img = new QImage (this->width(), this->height(), QImage::Format_ARGB32);
        img->fill (Qt::white);

	ShowCells = false;
	
	connect ( pbtApply, SIGNAL (clicked()), this, SLOT (close()));
}

TTTRegionSelection::~TTTRegionSelection()
{
	if (CurrentBox)
		delete CurrentBox;

        if (BoundingBox)
                delete BoundingBox;

        if (img)
                delete img;
}

void TTTRegionSelection::setBox (const QRect &_rect, QColor _color, float _zoom) //(RectBox *_box)
{
        //CurrentBox = _box;
        if (CurrentBox)
                delete CurrentBox;

        CurrentBox = new RectBox();
        CurrentBox->initRectBox (img, _rect, _color, _zoom);
}

void TTTRegionSelection::setBoundingBox (const RectBox &/*_boundingBox*/, bool /*_consider*/)
{
//        //BoundingBox = _boundingBox;
//        if (BoundingBox)
//                delete BoundingBox;
//
//        BoundingBox = new RectBox();
//        BoundingBox->initRectBox (img, _boundingBox.box(), _boundingBox.color(), _boundingBox.zoom());
//
//	ConsiderBounds = _consider;
}

void TTTRegionSelection::setBoundingRect (QRect _boundingRect, QColor _color, float _zoom, bool _consider)
{
        if (BoundingBox)
                delete BoundingBox;

        BoundingBox = new RectBox();
        BoundingBox->initRectBox (img, _boundingRect, _color, _zoom);

	ConsiderBounds = _consider;
}

void TTTRegionSelection::setScale (QRect &_scale)
{
	Scale = _scale;
	setGeometry (QRect (x(), y(), int(Scale.width() * ZoomFactor), int(Scale.height() * ZoomFactor)));
}

void TTTRegionSelection::paintEvent (QPaintEvent *)
{
//	if (CurrentBox->initialized())
//		CurrentBox->draw();
//
//        if (BoundingBox->initialized())
//                BoundingBox->draw();
//
	if (ShowCells)
		showCells ();

        //copy the contents of img onto this
        QPainter p (this);
        p.drawImage (0, 0, *img);
        p.end();
}

//SLOT: also called from context menu "reset box"
void TTTRegionSelection::resetCurrentBox()
{
	// !!! problem with menu graphics
	
        CurrentBox->reset();

        drawBox();
}

//EVENT: right mouse click opens the context menu
void TTTRegionSelection::contextMenuEvent (QContextMenuEvent * /*_ev*/)
{
	//the normal right click is used for resizing the box!
	//so the context menu is opened only if the control button is pressed additionally
//	if ((ev->state() == (Qt::ControlModifier & Qt::RightButton) )) return;
//
//	Q3PopupMenu *ohyeah = new Q3PopupMenu (this);
//	ohyeah->insertItem ("Reset box to original size", this, SLOT (resetCurrentBox()));
//	ohyeah->exec (QCursor::pos());
//	if (ohyeah) {
//		delete ohyeah;
//		ohyeah = 0;
//	}
}

//EVENT: mouse moved causes the current frame to be shifted, if a mouse button is pressed
void TTTRegionSelection::mouseMoveEvent (QMouseEvent *ev)
{
	if (mouseButton && CurrentBox) {
		//shift shape box according to mouse button and position
		
		//delete the old box (draw() is executed with an XOr-Pen!)
                //CurrentBox->draw();
		
		int xDiff =  (int)((ev->x() - posMouseX)  / ZoomFactor);
		int yDiff =  (int)((ev->y() - posMouseY)  / ZoomFactor);
		//qDebug ("xDiff = %x, yDiff = %x", xDiff, yDiff);
		
		//if there exists a bounding rect within the current box should stay, the box is cut
		// if it would exceed beyond the edge
		if (ConsiderBounds)
                        if (BoundingBox->initialized()) {
				//if (BoundingBox->box().intersect (CurrentBox->box()) != CurrentBox->box())
				//	CurrentBox->truncate();
				if (mouseButton != 2)
                                        if ( (BoundingBox->left() > CurrentBox->left() + xDiff) && (xDiff < 0) )
						xDiff = 0;
				
				if (mouseButton != 1)
                                        if ( (BoundingBox->right() < CurrentBox->right() + xDiff) && (xDiff > 0) )
						xDiff = 0;
				
				if (mouseButton != 2)
                                        if ( (BoundingBox->top() > CurrentBox->top() + yDiff) && (yDiff < 0) )
						yDiff = 0;
				
				if (mouseButton != 1)
                                        if ( (BoundingBox->bottom() < CurrentBox->bottom() + yDiff) && (yDiff > 0) )
						yDiff = 0;
			}
		
		
		switch (mouseButton) {
			case 1:			//left -> shift upper left edge
				CurrentBox->shiftTopLeft (xDiff, yDiff);
				emit boxSizeChanged();
				break;
			case 2:			//right -> shift lower right edge
				CurrentBox->shiftBottomRight (xDiff, yDiff);
				emit boxSizeChanged();
				break;
			case 3:			//middle -> shift complete box
				CurrentBox->shift (xDiff, yDiff);
                                emit boxPositionChanged (QPointF (xDiff, yDiff));//CurrentBox);
				break;
			default:
				;
		}
		
				
		
		
		posMouseX = ev->x();
		posMouseY = ev->y();

                drawBox();
        }
	ev->accept();
}

//EVENT: mouse released causes the shift process of the current frame to stop
void TTTRegionSelection::mouseReleaseEvent (QMouseEvent *ev)
{
	mouseButton = 0;
	ev->accept();
}

void TTTRegionSelection::drawBox()
{
        img->fill (Qt::white);
        CurrentBox->draw();
        repaint();
}

//EVENT: mouse pressed causes the shift process of the current frame to start
void TTTRegionSelection::mousePressEvent (QMouseEvent *ev)
{
	if (ev->button() == Qt::LeftButton)  mouseButton = 1;
	else if (ev->button() == Qt::RightButton)  mouseButton = 2;
	else if (ev->button() == Qt::MidButton)  mouseButton = 3;
	else mouseButton = 0;
	
	if (mouseButton) {
		posMouseX = ev->x();
		posMouseY = ev->y();
	}
	ev->accept();
}

//EVENT: when the user presses left/right/up/down keys, the current frame is shifted pixel by pixel
//what is shifted, depends on the additional keys like Shift or Control
void TTTRegionSelection::keyPressEvent (QKeyEvent *ev)
{
	//if (ev not used) this is important! Setting ignore() proceeds the key event to the parent widget
	ev->ignore();
	
	switch (ev->key()) {
		case Qt::Key_Escape:
			ev->accept();
			close();
			break;
		default:
			;	
	}
	
}

//EVENT: the form is not closed but only hidden
void TTTRegionSelection::closeEvent (QCloseEvent *ev)
{
        //do not actually close the form - just hide it

	hide();
        ev->ignore();


	// Save window layout
	UserInfo::getInst().saveWindowPosition(this, "TTTRegionSelection");
}

//EVENT:
void TTTRegionSelection::showEvent (QShowEvent *_ev)
{
	if(!_ev->spontaneous() && !windowLayoutLoaded) {
		// Load window layout
		UserInfo::getInst().loadWindowPosition(this, "TTTRegionSelection");

		windowLayoutLoaded = true;
	}

	_ev->accept();
}

void TTTRegionSelection::resizeEvent (QResizeEvent *)
{
        //adopt drawing pixmap to new size

        //not necessary!
        //img.resize (this->width(), this->height());
}

void TTTRegionSelection::showCells ()
{

	if (! ShowCells)
		return;
	
        //BS QPainter
        //return;

	Q3ValueList<TrackPoint> *ctp = TTTManager::getInst().getCurrentTrackPoints();
	
	if (! ctp)
		return;
	
        QPainter p (img);
	
	TrackPoint dummy;
	for (	Q3ValueList<TrackPoint>::iterator iter = ctp->begin(); 
			iter != ctp->end();
			++iter) {
		
		dummy = *iter; 
		if (dummy.TimePoint > -1) {
			p.drawPoint ((int)(dummy.X * ZoomFactor), (int)(dummy.Y * ZoomFactor));
		}
	}
	
	p.end();

        repaint();
}

//#include "tttregionselection.moc"
