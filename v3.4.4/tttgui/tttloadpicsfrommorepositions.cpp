/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "tttloadpicsfrommorepositions.h"

// Project includes
#include "tttdata/userinfo.h"
#include "positiondisplay.h"
#include "tttpositionlayout.h"
#include "tttbackend/tttexception.h"
#include "tttmovie.h"
#include "tttbackend/tttpositionmanager.h"
#include "tttbackend/tttmanager.h"
#include "tttbackend/picturearray.h"
#include "tttbackend/changecursorobject.h"

// Qt includes
#include <QResizeEvent>
#include <QShowEvent>
#include <QCloseEvent>
#include <QStringList>
#include <QVector>
#include <QMessageBox>
#include <QProgressDialog>

// Qt3
#include <Q3DictIterator>



TTTLoadPicsFromMorePositions::TTTLoadPicsFromMorePositions( QWidget *_parent ) : QDialog(_parent)
{
	ui.setupUi(this);

	// Init variables
	windowLayoutLoaded = false;

	// Init controls
	connect ( ui.pbtCancel, SIGNAL (clicked()), this, SLOT (close()));
	connect ( ui.pbtLoad, SIGNAL (clicked()), this, SLOT (startLoading()));
	connect ( ui.pbtUnload, SIGNAL (clicked()), this, SLOT (startUnloading()));
	connect ( ui.cboPositions, SIGNAL (currentIndexChanged(const QString&)), this, SLOT (comboboxSelectionChanged(const QString&)));

	// Init position layout
	//positionView = new PositionDisplay (ui.grbPositionLayout);
	connect ( ui.positionView, SIGNAL (positionLeftClicked (const QString&)), this, SLOT (activatePosition (const QString&)));
	connect ( ui.positionView, SIGNAL (positionRightClicked (const QString&)), this, SLOT (deactivatePosition (const QString&)));

	ui.positionView->initialize(true, false);

	// Create position thumbnails
	//for (Q3DictIterator<TTTPositionManager> iter (TTTManager::getInst().getAllPositionManagers()); iter.current(); ++iter) {
	//	// Check if pos is available
	//	if (! iter.current()->isAvailable())
	//		continue;

	//	// Create thumbnail
	//	QString picfilename = iter.current()->getPictureDirectory() + "/" + iter.current()->getBasename (true) + "00001_w0.jpg";
	//	PositionThumbnail *pt = new PositionThumbnail (positionView->viewport(), iter.current()->positionInformation.getIndex(), picfilename, iter.current()->positionInformation.getComment(), true);

	//	// Mark as loaded if pictures have been loaded
	//	if (iter.current()->getPictures())
	//		if (iter.current()->getPictures()->getLoadedPictures() > 0)
	//			pt->markPicturesLoaded (true);

	//	// Add to position view
	//	positionView->addChildLocal (pt, iter.current()->positionInformation.getLeft(), iter.current()->positionInformation.getTop());
	//	
	//	//if (iter.current()->positionThumbnail)
	//	//	iter.current()->backupPositionThumbnail = iter.current()->positionThumbnail;
	//	//iter.current()->positionThumbnail = pt;
	//}

	// Set position display settings
	//positionView->setFactors (TTTManager::getInst().getOcularFactor(), TTTManager::getInst().getTVFactor());
	//positionView->setInverted (TTTManager::getInst().coordinateSystemIsInverted());
	//positionView->refresh();

	ui.lblSelectedPositions->setText("");

	// Init selection combobox
	initSelectionComboBox();
}

void TTTLoadPicsFromMorePositions::startLoading()
{
	QProgressDialog *progessBar = 0;
	int numSelectedPositions;
	bool bError = false;

	// Change to "wait" cursor
	ChangeCursorObject c;

	try {
		// Get selected position
		const QList<TTTPositionManager*> positions = ui.positionView->getActivePositions();
		numSelectedPositions = positions.size();

		// Check if any positions have been selected
		if(numSelectedPositions == 0)
			throw TTTException("No positions selected.");

		// Get timepoints
		int startTP = ui.lieStartTp->text().toUInt();
		int endTP = ui.lieStopTp->text().toUInt();

		// Get interval
		int interval = ui.spbInterval->value();

		// Check timepoints
		if(startTP < 1 || (endTP - startTP) <= 0 )
			throw TTTException("Invalid timepoints. Timepoints must be non-negative numbers and stop timepoint has to be bigger than start timepoint.");

		// Get wavelengths
		QVector<unsigned int> wls;
		getWLs(wls);

		// Get position layout window
		TTTPositionLayout* posLayout = TTTManager::getInst().frmPositionLayout;
		if(!posLayout)
			throw TTTException("Internal error (1)");

		// Show progress bar
		progessBar = new QProgressDialog("Loading pictures..", "Cancel", 0, numSelectedPositions, this);
		progessBar->setWindowModality(Qt::ApplicationModal);
		progessBar->setMinimumDuration(0);

		// Iterate over selected positions
		int counter = 0;
		for(QList<TTTPositionManager*>::const_iterator it = positions.begin(); it != positions.end(); ++it) {
			// Get position manager
			TTTPositionManager* posManager = *it;

			// Update progress bar
			progessBar->setValue(counter++);
			QApplication::processEvents();
			if(progessBar->wasCanceled())
				break;

			// Get position key
			const QString posKey = posManager->positionInformation.getIndex();
			if(posKey.isEmpty())
				throw TTTException("Internal error (2)");

			// Change to position in position layout and initialize it
			if(!posLayout->setPosition(posKey/*, true*/))
				throw TTTException("Internal error (3)");

			//// Make sure pictures have been initialized
			//posLayout->markSelectedPicturesForLoading();

			// Mark pictures as to be loaded
			for(QVector<unsigned int>::const_iterator wlIt = wls.constBegin(); wlIt != wls.constEnd(); ++wlIt ) {
				for(int curTP = startTP; curTP <= endTP;) {
					if(posManager->getPictures()->pictureExists(curTP, 1, *wlIt)) 
						posManager->getPictures()->setLoading(curTP, 1, *wlIt, true);

					// Use interval for wavelength 0
					if(*wlIt == 0) 
						curTP += interval;
					else
						++curTP;
				}
			}

			// Load them..
			posManager->getPictures()->loadPictures(false);

			// Make sure to indicate that pictures have been loaded
			posLayout->markPicturesLoadedForCurrentPosition(true);

			// Open movie window
			if(posManager->frmMovie)
				posManager->frmMovie->show(); 
		}
	}
	catch(const TTTException& e) {
		bError = true;
		QMessageBox::critical(this, "Error", "An error occurred:\n" + e.what());
	}

	// Close progress bar
	if(progessBar)
		progessBar->setValue(numSelectedPositions);

	// Close window if everything worked
	if(!bError)
		close();
}

void TTTLoadPicsFromMorePositions::startUnloading()
{
	bool bError = false;

	// Change to "wait" cursor
	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));

	try {
		// Get selected position
		const QList<TTTPositionManager*> positions = ui.positionView->getActivePositions();

		// Check if any positions have been selected
		if(positions.size() == 0)
			throw TTTException("No positions selected.");

		// Get timepoints
		int startTP = ui.lieStartTp->text().toUInt();
		int endTP = ui.lieStopTp->text().toUInt();

		// Check timepoints
		if(startTP < 1 || (endTP - startTP) <= 0 )
			throw TTTException("Invalid timepoints. Timepoints must be non-negative numbers and stop timepoint has to be bigger than start timepoint.");

		// Get wavelengths
		QVector<unsigned int> wls;
		getWLs(wls);

		// Iterate over selected positions
		int counter = 0;
		for(QList<TTTPositionManager*>::const_iterator it = positions.begin(); it != positions.end(); ++it) {
			// Get position manager
			TTTPositionManager* posManager = *it;

			// Get position key
			const QString posKey =  posManager->positionInformation.getIndex();
			if(posKey.isEmpty())
				throw TTTException("Internal error (2)");


			// Check if picture array for this position exists 
			if(!posManager->getPictures())
				continue;

			// Mark pics as to be unloaded
			for(QVector<unsigned int>::const_iterator wlIt = wls.constBegin(); wlIt != wls.constEnd(); ++wlIt ) {
				for(int curTP = startTP; curTP <= endTP; curTP++) {
					if(posManager->getPictures()->pictureExists(curTP, 1, *wlIt)) 
						posManager->getPictures()->setLoading(curTP, 1, *wlIt, false);	
				}
			}

			// Unload them..
			posManager->getPictures()->loadPictures(false, true);

			// Check if there are no pictures left in memory
			if (posManager->getPictures()->getLoadedPictures() == 0) {
				if(PositionThumbnail *tn = TTTManager::getInst().frmPositionLayout->getThumbnail(posKey))
					tn->markPicturesLoaded(false);

				posManager->frmMovie->close();
			}
		}
	}
	catch(const TTTException& e) {
		bError = true;
		QMessageBox::critical(this, "Error", "An error occurred:\n" + e.what());
	}

	// Restore mouse pointer
	QApplication::restoreOverrideCursor();	

	// Close window if everything worked
	if(!bError)
		close();
}


void TTTLoadPicsFromMorePositions::activatePosition (const QString& _index)
{
	if(ui.positionView->selectPosition(_index)) {
		if(!selectedPositions.contains(_index))
			selectedPositions.append(_index);
	}

	updateSelectedPositionsLabel();
}

void TTTLoadPicsFromMorePositions::deactivatePosition (const QString& _index)
{
	if(ui.positionView->deSelectPosition(_index))
		selectedPositions.removeOne(_index);

	updateSelectedPositionsLabel();
}


void TTTLoadPicsFromMorePositions::closeEvent (QCloseEvent *_ev)
{
	// Save window layout
	UserInfo::getInst().saveWindowPosition(this, "TTTLoadPicsFromMorePositions");

	// Close form
	accept();

	// Accept event
	_ev->accept();
}

void TTTLoadPicsFromMorePositions::showEvent (QShowEvent *_ev)
{
	// Load window layout
	if(!_ev->spontaneous() && !windowLayoutLoaded) {
		UserInfo::getInst().loadWindowPosition(this, "TTTLoadPicsFromMorePositions");
		windowLayoutLoaded = true;
	}

	_ev->accept();
}

void TTTLoadPicsFromMorePositions::resizeEvent (QResizeEvent *)
{
	//ui.positionView->fit();
}

void TTTLoadPicsFromMorePositions::getWLs( QVector<unsigned int>& _wls )
{
	// Help string
	const QString wlHelp("\n\nSpecify wavelengths as csv-list. For example enter '0,2,3' to load pictures of wavelengths 0, 2 and 3.");

	// Get wavelength string
	QString wlString = ui.lieWLs->text();
	if(wlString.isEmpty()) 
		throw TTTException("No wavelengths specified." + wlHelp);

	// Split string
	QStringList wlList = wlString.split(',', QString::SkipEmptyParts);

	// Convert to int
	for(QStringList::const_iterator it = wlList.constBegin(); it != wlList.constEnd(); ++it) {
		bool ok;
		_wls.append((*it).toUInt(&ok));

		if(!ok)
			throw TTTException("Invalid wavelength: " + *it + wlHelp);
	}

	// Make sure we have something
	if(_wls.size() == 0)
		throw TTTException("Invalid wavelengths." + wlHelp);
}

void TTTLoadPicsFromMorePositions::updateSelectedPositionsLabel()
{
	// Sort positions
	qSort(selectedPositions.begin(), selectedPositions.end());

	// Update text
	QString newText;
	for(int i = 0; i < selectedPositions.size(); ++i) {
		if(i)
			newText += ", ";
		newText += selectedPositions[i];
	}
	ui.lblSelectedPositions->setText(newText);
}

void TTTLoadPicsFromMorePositions::initSelectionComboBox()
{
	// Clear
	ui.cboPositions->clear();

	// Iterate over all positions and generate entries
	QStringList entries;
	entries.append("");
	QHash<int, TTTPositionManager*>& allPositions = TTTManager::getInst().getAllPositionManagers();
	for (auto iter = allPositions.begin(); iter != allPositions.end(); ++iter) {
		// Check if pos is available
		if (! (*iter)->isAvailable())
			continue;

		QString nextEntry = QString("%1 - %2").arg((*iter)->positionInformation.getIndex()).arg((*iter)->positionInformation.getComment());
		entries.append(nextEntry);
	}

	// Sort entries
	entries.sort();

	// Add to list
	ui.cboPositions->addItems(entries);
}

void TTTLoadPicsFromMorePositions::comboboxSelectionChanged( const QString& _text )
{
	// Extract index
	int i = _text.indexOf('-');
	if(i == -1)
		return;
	QString index = _text.left(i - 1);

	// Select / deselect position
	if(selectedPositions.contains(index)) {
		deactivatePosition(index);
	}
	else {
		activatePosition(index);
	}
}
