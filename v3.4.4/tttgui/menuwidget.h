/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MENUWIDGET_H
#define MENUWIDGET_H

#include <qmenubar.h>

/**
	@author Bernhard <bernhard.schauberger@campus.lmu.de>
	
	The use of this class is only to distinguish between widgets that can be equipped with a custom menu bar and those
	 that cannot.
	The first widgets need to inherit additionally from this class.
*/

class MenuWidget {
public:
	
	MenuWidget();
	
	~MenuWidget();
	
	/**
	 * assigns the local menu bar attribute to the externally created menu bar
	 * @param _menuBar the (externally created) QMenuBar item
	 */
	virtual void setMenuBar (QMenuBar *_menuBar);

protected:
	
	///the menu bar of this window (is assigned in setMenuBar())
	QMenuBar *menuBar;
	
};

#endif
