/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "tttexportfluorescencepatches.h"

// Project
#include "tttbackend/changecursorobject.h"
#include "tttgui/positiondisplay.h"
#include "tttdata/userinfo.h"
#include "tttbackend/tttmanager.h"
#include "tttbackend/tttpositionmanager.h"
#include "tttbackend/tools.h"
#include "tttbackend/picturearray.h"
#include "tttbackend/tools.h"

// QT
#include <QFileDialog>
#include <QProgressDialog>



TTTExportFluorescencePatches::TTTExportFluorescencePatches( QWidget* parent /*= 0*/ )
	: QWidget(parent)
{
	// Setup gui
	m_gui.setupUi(this);

	// Signals/slots
	connect(m_gui.pbtSelectAllPositions, SIGNAL(clicked()), this, SLOT(selectAllPositions()));
	connect(m_gui.pbtClearPositions, SIGNAL(clicked()), this, SLOT(clearPositionSelection()));
	connect(m_gui.positionView, SIGNAL(positionLeftClicked(const QString&)), this, SLOT(positionLeftClicked(const QString&)));
	connect(m_gui.positionView, SIGNAL(positionRightClicked(const QString&)), this, SLOT(positionRightClicked(const QString&)));
	connect(m_gui.pbtSetTargetDir, SIGNAL(clicked()), this, SLOT(setDirectory()));
	connect(m_gui.pbtStartExport, SIGNAL(clicked()), this, SLOT(runExport()));
	connect(m_gui.pbtSelectTree, SIGNAL(clicked()), this, SLOT(selectSingleTreeFile()));

	// Init gui
	ChangeCursorObject cc;
	m_gui.positionView->initialize(true, false);
	m_gui.lblSelectedPositions->setText("");
	m_gui.lieTargetDir->setText(UserInfo::get(UserInfo::KEY_TTTEXPORTFLUORESCENCEPATCHES_TARGETDIR, "").toString());
	TTTPositionManager* tttpm = TTTManager::getInst().getCurrentPositionManager();
	if(tttpm && tttpm->getLastTimePoint() > 1)
		m_gui.spbStopTp->setValue(tttpm->getLastTimePoint());
}

void TTTExportFluorescencePatches::positionLeftClicked(const QString& index)
{
	m_gui.positionView->selectPosition(index);

	// Display selected positions
	updateSelectedPositionsText();
}

void TTTExportFluorescencePatches::positionRightClicked(const QString& index)
{
	m_gui.positionView->deSelectPosition(index);

	// Display selected positions
	updateSelectedPositionsText();
}

void TTTExportFluorescencePatches::selectAllPositions()
{
	// Select all positions
	ChangeCursorObject cc;
	auto positions = TTTManager::getInst().getAllPositionManagers();
	for(auto it = positions.begin(); it != positions.end(); ++it) {
		QString posIndex = it.value()->positionInformation.getIndex();
		m_gui.positionView->selectPosition(posIndex);
	}

	// Display selected positions
	updateSelectedPositionsText();
}

void TTTExportFluorescencePatches::clearPositionSelection()
{
	// Clear selection
	m_gui.positionView->deSelectPosition("");
	m_gui.positionView->updateDisplay();

	// Display selected positions
	updateSelectedPositionsText();
}

void TTTExportFluorescencePatches::setDirectory()
{
	// Change selection
	QString newFolder = QFileDialog::getExistingDirectory(this, "Select export folder", m_gui.lieTargetDir->text());
	if(!newFolder.isEmpty()) {
		m_gui.lieTargetDir->setText(newFolder);
		UserInfo::set(UserInfo::KEY_TTTEXPORTFLUORESCENCEPATCHES_TARGETDIR, newFolder);
	}
}

void TTTExportFluorescencePatches::updateSelectedPositionsText()
{
	// Update text
	auto selectedPositions = m_gui.positionView->getActivePositions();
	QString positionsText = "";
	for(auto it = selectedPositions.begin(); it != selectedPositions.end(); it++){
		if(positionsText.length())
			positionsText += ", ";
		positionsText += (*it)->positionInformation.getIndex();
	}
	m_gui.lblSelectedPositions->setText(positionsText);
}

void TTTExportFluorescencePatches::runExport()
{
	// Check if only a single tTt file and/or cell branch should be used
	QString singleTTTFile;
	if(m_gui.grbExportOnlySpecifiedTree->isChecked()) {
		singleTTTFile = m_gui.lieSingleTttFile->text();
		if(!QFile::exists(singleTTTFile)) {
			QMessageBox::critical(this, "Error", "Cannot find specified tTt file.");
			return;
		}
	}
	int singleCellBranch = -1;
	if(m_gui.grbExportOnlySpecifiedCellBranch->isChecked()) {
		singleCellBranch = m_gui.spbSingleCell->value();
	}

	// Get positions
	QList<TTTPositionManager*> selectedPositions;
	if(singleTTTFile.isEmpty()) {
		// Selected ones
		selectedPositions = m_gui.positionView->getActivePositions();
		if(selectedPositions.size() == 0) {
			QMessageBox::information(this, "Note", "Export not possible:\n\nNo position selected.");
			return;
		}
	}
	else {
		// Only of specified tree
		QString posNumber = Tools::getPositionNumberFromString(singleTTTFile);
		TTTPositionManager* tttpm = TTTManager::getInst().getPositionManager(posNumber);
		if(tttpm && !tttpm->isInitialized() && !tttpm->initialize())
			tttpm = 0;
		if(!tttpm) {
			QMessageBox::critical(this, "Error", "Cannot open position for specified tTt file.");
			return;
		}		
		selectedPositions.push_back(tttpm);
	}

	// Create destination folder
	QString folder = m_gui.lieTargetDir->text();
	if(folder.isEmpty()) {
		QMessageBox::information(this, "Note", "Export not possible:\n\nNo destination folder specified.");
		return;
	}
	if(!QDir(folder).exists()) {
		if(!QDir(folder).mkpath(folder)) {
			QMessageBox::information(this, "Note", "Export not possible:\n\nCannor create destination folder.");
			return;
		}
	}
	UserInfo::set(UserInfo::KEY_TTTEXPORTFLUORESCENCEPATCHES_TARGETDIR, folder);
	folder = QDir::fromNativeSeparators(folder);
	if(folder.right(1) != "/")
		folder += "/";

	// Display progress bar
	QProgressDialog progessBar("Exporting image patches..", "Cancel", 0, selectedPositions.size(), this);
	progessBar.setWindowModality(Qt::ApplicationModal);
	progessBar.setMinimumDuration(0);
	progessBar.setValue(0);
	QApplication::processEvents();

	// Settings and stuff
	int zIndex = m_gui.spbZIndex->value();
	int wl = m_gui.spbWl->value();
	int patchSize = m_gui.spbImagePatchSize->value();
	int patchSizeDiv2 = patchSize / 2;
	bool doBackground = m_gui.chkApplyBackgroundCorrection->isChecked();
	QString expName = TTTManager::getInst().getExperimentBasename();

	// Process one position after another
	int counter = 0;
	int exportedCells = 0;
	auto allPositions = TTTManager::getInst().getAllPositionManagers();
	QStringList errors;
	for(auto itPos = selectedPositions.begin(); itPos != selectedPositions.end(); ++itPos) {
		// Abort if canceled
		progessBar.setValue(counter++);
		if(progessBar.wasCanceled())
			break;

		// List trees starting in position
		TTTPositionManager* tttpm = *itPos;
		if(!tttpm || tttpm->isLocked())
			continue;
		if(!tttpm->isInitialized()) {
			if(!tttpm->initialize())
				continue;
		}
		QStringList tttFiles;
		if(singleTTTFile.isEmpty())
			tttFiles = tttpm->getAllTTTFiles();
		else
			tttFiles.push_back(singleTTTFile);

		// First/last time point of position
		int firstTimePoint = std::max(m_gui.spbStartTp->value(), tttpm->getFirstTimePoint());
		int lastTimePoint = std::min(m_gui.spbStopTp->value(), tttpm->getLastTimePoint());
		

		// Process trees
		for(int iFile = 0; iFile < tttFiles.size(); ++iFile) {
			// Open current file
			Tree curTree;
			curTree.reset();
			int ftp = -1, ltp = -1, numTracks = -1;
			if(TTTFileHandler::readFile(tttFiles[iFile], &curTree, numTracks, ftp, ltp, &tttpm->positionInformation, false, true) != TTT_READING_SUCCESS) {
				errors << QString("Cannot open tree %1").arg(tttFiles[iFile]);
				continue;
			}

			// Process cells
			QVector<int> tracksToProcess;
			if(singleCellBranch < 0) {
				tracksToProcess.push_back(1);
			}
			else {
				int tmp = singleCellBranch;
				while(tmp > 0) {
					tracksToProcess.push_front(tmp);
					tmp /= 2;
				}
			}
			while(!tracksToProcess.isEmpty()) {
				// Get current cell
				int curTrackNumber = tracksToProcess.back();
				tracksToProcess.pop_back();
				ITrack* curTrack = curTree.getTrackByNumber(curTrackNumber);
				if(!curTrack)
					continue;

				// Add children
				if(singleCellBranch < 0) {
					if(curTrack->getChild1())
						tracksToProcess.push_back(curTrackNumber*2);
					if(curTrack->getChild2())
						tracksToProcess.push_back(curTrackNumber*2+1);
				}

				// Check if cell alive during time range if needed
				if(m_gui.chkOnlyAliveCells->isChecked()) {
					if(curTrack->getFirstTimePoint() > firstTimePoint || curTrack->getLastTimePoint() < lastTimePoint)
						continue;
				}

				// Process track points
				int maxTpCurTrack = std::min(lastTimePoint, curTrack->getLastTimePoint());
				int minTpCurTrack = std::max(firstTimePoint, curTrack->getFirstTimePoint());
				bool cellCounterIncreased = false;
				for(int curTimePoint = minTpCurTrack; curTimePoint <= maxTpCurTrack; ++curTimePoint) {
					ITrackPoint* curTrackPoint = curTrack->getTrackPointByTimePoint(curTimePoint);
					if(!curTrackPoint)
						continue;

					// Get position of current track point
					int position = curTrackPoint->getPositionNumber();
					TTTPositionManager* tttpmCurTrackPoint = allPositions.value(position, 0);
					if(!tttpmCurTrackPoint || tttpmCurTrackPoint->isLocked())
						continue;
					if(!tttpmCurTrackPoint->isInitialized()) {
						if(!tttpmCurTrackPoint->initialize())
							continue;
					}

					// Check if image of wl exists at timepoint/position
					PictureArray* pics = tttpmCurTrackPoint->getPictures();
					if(pics->pictureExists(curTimePoint, zIndex, wl)) {
						// Load image if necessary
						if(!pics->isLoaded(curTimePoint, wl, zIndex)) {
							if(!pics->loadOnePicture(PictureIndex(curTimePoint, zIndex, wl))) {
								errors << QString("Cannot load image: position=%1 timepoint=%2").arg(tttpmCurTrackPoint->positionInformation.getIndex()).arg(curTimePoint);
								continue;
							}
						}

						// Get image data
						const PictureContainer* picContainer = pics->getPictureContainer(curTimePoint, zIndex, wl);
						if(!picContainer) {
							errors << QString("Cannot access image container: position=%1 timepoint=%2").arg(tttpmCurTrackPoint->positionInformation.getIndex()).arg(curTimePoint);
							continue;
						}

						// Apply background correction
						if(doBackground && !picContainer->backgroundCorrected()) {
							int r = pics->applyBackgroundCorrection(PictureIndex(curTimePoint, zIndex, wl));
							if(r != 0) {
								errors << QString("Cannot calculate background: position=%1 timepoint=%2 wl=%3 error=%4").arg(tttpmCurTrackPoint->positionInformation.getIndex()).arg(curTimePoint).arg(wl).arg(r);
								continue;
							}
						}

						// Get image data as 8 bit grayscale QImage
						const QImage imageData = picContainer->getImageForDisplay();
						if(imageData.isNull()) {
							errors << QString("Image data not set: position=%1 timepoint=%2").arg(tttpmCurTrackPoint->positionInformation.getIndex()).arg(curTimePoint);
							continue;
						}
						const unsigned char* imageRawData = imageData.bits();
						int bpl = imageData.bytesPerLine();
						int imageSizeX = imageData.width();
						int imageSizeY = imageData.height();

						// Create image patch
						QImage imagePatch(patchSize, patchSize, QImage::Format_Indexed8);
						imagePatch.setColorTable(Tools::getColorTableGrayIndexed8());
						imagePatch.fill(0);
						unsigned char* dataDest = imagePatch.bits();
						int bplDest = imagePatch.bytesPerLine();

						// Determine area to extract
						float xGlobal = curTrackPoint->getX(),
							yGlobal = curTrackPoint->getY();
						QPointF posLocalF = tttpmCurTrackPoint->getDisplays().at (wl).calcTransformedCoords(QPointF(xGlobal, yGlobal), &tttpmCurTrackPoint->positionInformation);
						QPoint posLocal = posLocalF.toPoint();
						int xStart = std::min(imageSizeX-1, std::max(0, posLocal.x() - patchSizeDiv2));
						int xStop = std::min(imageSizeX-1, std::max(0, posLocal.x() + patchSizeDiv2));
						xStop = std::min(xStop, xStart + patchSize - 1);
						int yStart = std::min(imageSizeY-1, std::max(0, posLocal.y() - patchSizeDiv2));
						int yStop = std::min(imageSizeY-1, std::max(0, posLocal.y() + patchSizeDiv2));
						yStop = std::min(yStop, yStart + patchSize - 1);

						// Copy data
						for(int iY = 0; iY <= (yStop-yStart); ++iY) {
							int y = iY + yStart;
							if(y >= imageSizeY)
								break;
							for(int iX = 0; iX <= (xStop-xStart); ++iX) {
								int x = iX + xStart;
								if(x >= imageSizeX)
									break;
								dataDest[iY*bplDest + iX] = imageRawData[y*bpl + x];
							}
						}

						// Save file
						if(!cellCounterIncreased) {
							++exportedCells;
							cellCounterIncreased = true;
						}
						QString fileName = folder + QString("%1_p%2_c%3_t%4.png").arg(expName).arg(tttpmCurTrackPoint->positionInformation.getIndex()).arg(exportedCells, 5, 10, QChar('0')).arg(curTimePoint, 5, 10, QChar('0'));
						if(!imagePatch.save(fileName)) {
							errors << QString("Cannot save image (%1).").arg(fileName);
							continue;
						}
					}
				}
			}
		}

		// Unload all images from position
		if(tttpm) {
			PictureArray* pics = tttpm->getPictures();
			if(pics) {
				pics->unloadAllPictures();
			}
		}
	}
	progessBar.setValue(counter++);

	// Write errors to log file
	for(auto it = errors.begin(); it != errors.end(); ++it)
		qWarning() << QString("Error during fluorescence export: %1").arg(*it);

	// Done
	Tools::displayMessageBoxWithOpenFolder(QString("Export completed successfully.\n\nExported %1 cells (%2 errors).").arg(exportedCells).arg(errors.size()), "Done", folder, false, this);
}

void TTTExportFluorescencePatches::selectSingleTreeFile()
{
	// Suggest super directory of all ttt files
	QString sugg;
	const TTTPositionManager* tttpm = TTTManager::getInst().getCurrentPositionManager();
	if(tttpm)
		sugg = tttpm->getTTTFileDirectory(true);
	QString treeFile = QFileDialog::getOpenFileName(this, "Select tree", sugg, "tTt Files (*.ttt)");
	if(!treeFile.isEmpty())
		m_gui.lieSingleTttFile->setText(treeFile);
}
