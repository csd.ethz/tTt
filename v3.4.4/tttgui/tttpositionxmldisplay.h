/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TTTPOSITIONXMLDISPLAY_H
#define TTTPOSITIONXMLDISPLAY_H

#include "ui_frmPositionXMLDisplay.h"


class TTTPositionXMLDisplay: public QWidget, public Ui::frmPositionXMLDisplay {

Q_OBJECT

public:
	TTTPositionXMLDisplay(QWidget *parent = 0, const char *name = 0);
	
	/**
	 * loads the xml file for the provided position index, timepoint and wavelength
	 * @param _posIndex the position index
	 * @param _timepoint the timepoint
	 * @param _wavelength the wavelength
	 */
	void loadImageXML (int _posIndex, const int _timepoint, const int _wavelength);
	
	/**
	 * @see loadImageXML() above, uses the current values
	 */
	void loadImageXML();
	
	/**
	 * displays the information of the tat xml file
	 * note: needs to be done only once, thus all other calls to this method are cancelled internally
	 */
	void loadTATXML();
	
	/**
	 * initializes the form (loads all available positions)
	 * this cannot be done in the constructor as the positions are only known later
	 */
	void init();


	void closeEvent (QCloseEvent *);

	void showEvent (QShowEvent *_ev);
	
public slots:
	
	/**
	 * sets the position index to be displayed in its combobox
	 * automatically adds timepoint 1 and wavelength 0 in their respective boxes, too, and loads the corresponding xml document (if existing)
	 * @param _posIndex the index of the position
	 */
	void setPositionIndex (int _posIndex);

	/**
	 * Overloaded.
	 */
	void setPositionIndexString(const QString& _posIndex) {
		setPositionIndex(_posIndex.toInt());
	}
	
private slots:
	
	/**
	 * sets the timepoint after the user pressed return in its edit field
	 */
	void setTimepoint();
	
	/**
	 * sets the wavelength after the user pressed return in its edit field
	 */
	void setWavelength();
	
	/**
	 * completely expands the currently selected item (listview depends on the signalling button)
	 */
	void expandItem();
	
	/**
	 * completely collapses the currently selected item (listview depends on the signalling button)
	 */
	void collapseItem();
	
private:
	
	bool tatXML_loaded;
	
	///the currently selected position, timepoint and wavelength for the image meta xml display
	int positionIndex;
	int timepoint;
	int wavelength;

	///window layout already loaded
	bool windowLayoutLoaded;
	
//methods
	
	
};

#endif
