/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MULTIPLEPICTUREVIEWER_H
#define MULTIPLEPICTUREVIEWER_H


// Project includes
#include "mycircle.h"
#include "tttdata/symbol.h"
#include "tttdata/cellpathcell.h"

// Qt includes
#include <QTimer>
#include <QPointF>
#include <QVector>
#include <QCursor>
#include <QTimer>

// Forward declarations
class TTTPositionManager;
class StyleSheet;
class QPaintDevice;
class MovieMenuBar;
class Track;
class QMouseEvent;
class PictureView;
class MovieNextPosItem;



/**
 * handles multiple instances of PictureViewer
 * only serves as a manager, is not a displayed element itself
 */
class MultiplePictureViewer : public QObject
{
        Q_OBJECT

public:
        MultiplePictureViewer (QWidget *_containingFrame, int _wlCount, TTTPositionManager *_tttpm);

        void drawPictures (int _timepoint = -1, int _wavelength = -1);

        void resize();

        /**
         * sets the magenta orientation rectangle to the provided position
         * @param _pos normally in picture coordinates (pixel, topleft = 0/0), but...
         * @param _isaQCursorPos = true tells to transform the screen mouse coordinates to picture coords before (default = false)
         */
        void setOrientationBoxPosition (QPointF _pos, bool _isaQCursorPos = false);


        bool isInMultipleView() const
                {return multipleView;}

        bool isInOverlayMode() const
                {return overlayPictures;}

        /**
         * @return whether the form is in the mode to select more colony starts at once
         */
        bool inStartingCellSelectionMode() const
                {return startingCellSelectionMode;}

        /**
         * centers the view on the provided position
         * @param _newCenter provided in absolute coordinates
         */
        void centerPosition (QPointF _newCenter) const;

        ///sets whether track circles should be displayed
        void setShowTracks (bool _show, int _startWL, int _endWL);

        ///sets whether the track numbers should be displayed (for the provided wavelengths)
        void setShowTrackNumbers (bool _on, int _startWL, int _endWL);

        ///sets whether the wavelength index is visible (for the provided wavelengths)
        void setShowWavelengthBox (bool _show, int _startWL, int _endWL);

        void setWaveLength (int _waveLength, bool _show, bool _callShowPicture = true);

		/**
		 * Enable/Disable drawing of previous trackpoints (special effect)
		 */
		void setShowPreviousTrackPoints(bool _on, int _numPrevTrackPointsToDraw, int _widthOfTails, bool _drawPrevTrackPointsTransparencyEffect, bool _drawPrevTrackPointsOnlyOfSelectedCellBranch);

		/**
		 * Enable/Disable drawing of segmentation (special effect)
		 */
		void setDrawSegmentation(bool _on);

        /**
         * returns the current zoom factor
         */
        int getCurrentZoom() const;

        /**
         * sets the current timepoint (and z-index, wavelength) and updates the views accordingly
         */
        void setToTimepointWavelength (int _newTimeopint, int _newZIndex, int _newWavelength = -1);

        /**
         * draws all additional graphical elements (tracks, ...)
         * for each of them, the current visibility is evaluated (happens in PictureView)
         */
        void drawAdditions();

        /**
         * returns whether the circle mouse pointer is currently in use
         */
        bool circleMouseCursorUsed() const
                {return circleMouseOn;}

        /**
         * returns the radius of the circle mouse cursor (not dependent on its current usage)
         */
        int getCircleMouseRadius() const
                {return circleMouseRadius;}

        /**
         * starts the tracking process
         * @param _isMainTrackingInstance whether this is the main tracking instance, thus grabbing all keyboard events etc.
         */
        bool startTracking (bool _isMainTrackingInstance);

        /**
         * stops the tracking process for all views
         */
        bool stopTracking();

        /**
         * updates the view/layout/... to reflect the provided style sheet
         */
        void applyStyleSheet (const StyleSheet &_ss);

//        /**
//         * causes the currently tracking PictureView instance that holds the keyboard grab to give it up
//         */
//        void releaseKeyboard();
//
//        /**
//         * re-allows the currently tracking PictureView instance to take the keyboard grab
//         */
//        void grabKeyboard();

        /**
         * returns the cell paths for the current timepoint
         */
        QVector<MyCircle> getCellPaths() const
                {return cellPaths;}

        /**
         * @return whether this is in starting cell selection mode
         */
        bool isInStartingCellSelectionMode() const
                {return startingCellSelectionMode;}

        /**
         * @return the currently selected colony starting cells
         */
        QVector<MyCircle> getStartingCells() const
                {return selectedStartingCells;}

		/**
		 * Hide / unhide scrollbars in alle pictureviews
		 * @param _hide true if hide, false if show
		 */
		void showHideScrollbars(bool _hide);

		/**
		 * Get WaveLengthsSelected
		 * @return WaveLengthsSelected
		 */
		unsigned int getWaveLengthsSelected() const {
			return WaveLengthsSelected;
		}

		/**
		 * Renders the complete picture at the specified wavelength with all
		 * additions (the complete scene) currently visible into _pd
		 * @param _pd the target paint device (needs to have correct size already!)
		 * @param _waveLength the desired wavelength
		 * @param left left scene coordinate of area to export
		 * @param top top scene coordinate of area to export
		 * @param w width of area to export in scene units
		 * @param h height of area to export in scene units
		 * @return true if successful
		 */
		bool exportPicture(QPaintDevice *_pd, int _waveLength, int left, int top, int w, int h) const;

		/**
		* Set if track size for display should be overwritten by the value of spbDisplayCircleSize
		* Connected with toggle event of pbtSetDisplayCircleSize
		* @param _on toggle on or off
		* @param _trackRadius the radius that should be used in pixels, ignored if _on is false,
		* must be > 0 if _on is true
		*/
		void toggleOverwriteTrackRadiusForDisplay(bool _on, int _trackRadius = -1);

		/**
		* Returns if saved cell radius should be overwritten by radius set for display of tracks
		* @return true or false ;)
		*/
		bool overwriteTrackRadiusForDisplay() const {
			return trackRadiusForDisplay != -1;
		}

		/**
		* Returns the radius that should be used for drawing tracks, if saved cell radius should not be used or -1
		* @return radius in pixels
		*/
		int getTrackRadiusForDisplay() const {
			return trackRadiusForDisplay;
		}
	
		/**
		 * Set TTTMovie menu bar, so we can directly access it from within this class
		 * @param _movieMenu pointer to menu object
		 */
		void setMovieMenuBar(MovieMenuBar* _movieMenu) {
			movieMenu = _movieMenu;
		}

		/**
		 * Orientation box currently visible
		 */
		bool getOrientationBoxVisible() const {
			return showOrientationBox;
		}

		/**
		 * Hide out of sync box, regardless of user settings (e.g. for movie export)
		 * @param _show true if visible
		 */
		void setHideOutOfSyncBox(bool _show) {
			hideOutOfSyncBox = _show;
		}

		/**
		 * @return if out of sync box is hidden
		 */
		bool getHideOutOfSyncBox() const {
			return hideOutOfSyncBox;
		}


		/**
		 * Show or hide position buttons
		 * @param _show true if show
		 */
		void showPositionButtons(bool _show);

		/**
		 * Get special effect color for track with given track number. If no color is assigned yet, for cells
		 * with even cell numbers the parent cell's color is taken over if available, for uneven cell numbers
		 * a new color is assigned randomly
		 */
		QColor getSpecialEffectTrackColor(int trackNumber);

		/**
		 * Activate/deactivate marking of active PictureView.
		 */
		void setMarkActivePictureView(bool on);

		// For debugging purposes only
		void debugTest();

public slots:


        void addFluorescenceLine();

        void exportPics();

        void setZoom (int _zoomPercent, float _zoomX = -1.0f, float _zoomY = -1.0f);

        void setCircleMouse (bool _on);

        void setCircleMouseRadius (int _radius);

        void setOrientationBoxVisible (bool _visible);

        void setOverlay (bool _on, bool _updatePictures);

        /**
         * called when the user changes a setting in frmGammaAdjust
         * @param _wavelength the wavelength for which the setting was changed
         */
        void adjustGraphics (int _wavelength);

        /**
         *
         */
        void setSymbolPosition (QPointF);

        /**
         * starts the symbol position selection mode for the current wavelength
         * the user can choose a position with the mouse
         */
        void startSymbolPositionSelection (Symbol);

        /**
         * sets whether the background tracks should be visible or not
         */
        void setCellBackgroundDisplay (bool);

        /**
         * called when the user clicks on a picture in single background selection mode
         * @param _ the position of the click (the background track position) in absolute coordinates
         */
        void setSingleBackground (QPointF);

        /**
         * both: start/end
         * sets the timepoints between which the next added cell path should be visible
         * @param _? the timepoint (between 1 and 10,000)
         */
        void setCellPathStartTP (int _startTp);
        void setCellPathEndTP (int _endTp);


        ///adds the cell path for the current cell to the already displayed ones
        void addCellPath();

        /**
         * clears all displayed life paths (also from display)
         */
        void clearCellPaths();

        ///starts the start cell selection mode, where the user can select some colony originating cells at once
        ///@see createColonies
        void selectStartCells (bool _on);

        ///takes the start cell selections to create the desired amount of colonies
        ///@see selectStartCells()
        void createColonies();

        ///centers the current track to the wavelength display center if the life cycle of the track
        ///contains the current timepoint
        void centerCurrentTrack();

		///centers on current track or any precursor of it that is alive at the current time point
		///returns false if no cell was found alive at current time point or if cell is in a different
		///position
		bool centerCurrentTrackIncludingPrecursors();

		///centers the specified track to the wavelength display center if the life cycle of the track
		///contains the specified timepoint
		void centerTrack(const ITrack *_track, int _timepoint = -1);

        ///toggles if all cells/tracks should be displayed in the same color
        ///(no distinguishing between active and inactive cell)
        void setAllCellsUniform (bool);

  //      ///displays all tracks for the current timepoint that are stored in the current experiment folder
  //      ///all *.ttt files are read and existing trackpoints are painted in different colors (for each file)
		/////@Deprecated: use functions setDisplayFirstTracks(), setDisplayAllTracksOfCurPos() and setDisplayAllTracksOfAllPos() instead
  //      void displayAllTracks (bool _show, bool _firstOnly, bool _reallyAll);

		/////display only the first trackpoints for each track
		//void setDisplayFirstTracks(bool _on);

		/////display all tracks from current position
		//void setDisplayAllTracksOfCurPos(bool _on);

		/////display all tracks from all positions
		//void setDisplayAllTracksOfAllPos(bool _on);

//        //lets the user select a color to which all white points in the fluorescent pictures should be translated
//        //(better display and contrast)
//        void setTransColor();

        /////sets whether the background tracks should be shown (true) or not (false)
        //void setShowBackground (bool);

        ///sets whether the currently tracked positions are backgrounds (true) or normal tracks (false)
        void setBackgroundTracking (bool);

        ///sets whether the display is in multiple mode (true) or not (false)
        void setMultipleMode (bool);

		///called e.g. when the user clicks on any displayed wavelength (which is the parameter)
		void setWavelength (int);

		/**
		 * called when the circle mouse pointer should be updated, e.g. after turning tracking mode on or off.
		 * @param _trackingMode true if tracking mode will be on now, false if off.
		 */
		void updateCircleMousePointer(bool _trackingMode);

signals:

        //most of the signals are just propagated from PictureView
        //thus, for a documentation, see the signals there (most of them have the same name)

        void pictureMousePositionSet (QPointF _picGlobalPos);
        void PictureProceedDemand (bool, int, bool afterTrackpointSet);
        void wavelengthSelected (int);
        void cellSizeChanged (int);
        void trackSelected (Track *, bool, bool);
        void trackingInterrupted (Track *);
        void trackPointSet (Track *, int);
        void radiusSelected (int);
        void unhandledKeyReleased (int, bool, bool);

private slots:

        void setScrollBarValues (int _callingIndex, int _x, int _y);

        void propagateAbsoluteMousePosition (QPointF _absoluteGlobalPosition);

        ///used for different purposes
        /// - to select the position of a colony originating cell
        /// -
        ///@param _ev: the originating mouse event
        ///@param _pos: the coordinates of the mouse click, transformed to picture coordinates
        void handlePictureMouseRelease (QMouseEvent *_ev, QPointF _pos);

        ///called when a new (fluorescence) picture was displayed (not the old one repainted)
        void notifyPictureUpdate();

        ///lets the mouse cursor blink (called by a timer)
        void blinkMouse();

        ///deletes the specified symbol (wavelength & index)
        void deleteSymbol (int, int);

        ///deletes the colony starting cell with _index
        void deleteColonyStartingCell (int _index);


private:

		///initializes the "maps of positions" buttons in the picture views of this multiple pic viewer
		void initMapsOfPositions();

        ///the QWidget in which all PictureView instances are embedded (THIS is never visible, as it is no widget itself)
        QWidget *containingFrame;

        ///the array of PictureView objects (one for each wavelength + one for the overlay frame)
        QVector<PictureView*> picViews;

        ///the position manager for this position
        TTTPositionManager *positionManager;

        ///the number of wavelengths
        int wlCount;

        ///the index of the overlay PictureView object (necessary for drawing in there)
        int overlayIndex;

        ///whether more than one wavelength is possibly visible
        bool multipleView;

		///if orientation box is currently visible
		bool showOrientationBox;

        ///whether pictures should be overlayed
        bool overlayPictures;

        ///the number of wavelengths currently displayed
        unsigned int WaveLengthsSelected;

        ///whether the background of the cells rather than the cell itself should be tracked
        bool editCellBackground;

        //helps preventing an endless scroll loop
        bool processScrollViews;

        ///the radius of the circle mouse pointer
        int circleMouseRadius;

        ///the circle mouse cursor (is created anew for each radius change) and its color
        QCursor circleCursor;
		QColor circleCursorColor;

        //whether the mouse is in circle mode
        bool circleMouseOn;

        ///whether the form is in the mode where the user can select multiple colony origins at once
        bool startingCellSelectionMode;

        ///the selected colony starting cells
        QVector<MyCircle> selectedStartingCells;

        ///the timepoints between which the next added cell path should be visible (are set to track data each time a cell is selected)
        int cellPathTPStart;
        int cellPathTPEnd;

        ///contains a vector of cells for which the cell path should be displayed
        QVector<CellPathCell> cellPathCells;

        ///the (already calculated) cell paths for the currently set cells, valid for the current timepoint
        ///re-calculated with each timepoint change
        QVector<MyCircle> cellPaths;

        ///timer for any purposes
        QTimer universalTimer;

		///radius set for display of tracks if saved radius should not be used or -1
		int trackRadiusForDisplay;

		///menubar of movie window
		MovieMenuBar* movieMenu;

		///hide out of synx box
		bool hideOutOfSyncBox;

		// Track cirlce colors for special effects
		QHash<int, QColor> specialEffectsTracksColors;


//private methods

        /**
         * layout the picture views according to the current settings (active wavelength, multiple mode, overlay)
         */
        void layout (bool _multipleView, bool _updateView = true);

        /**
         * calculates the cell path for the currently selected cells that should have a path
         * @return a vector of MyCircle objects that store the various parameters for each point
         */
        QVector<MyCircle> calcCellPaths();

        /**
         * clears all colony starting cells (also from display)
         */
        void clearStartingCells();

};

#endif // MULTIPLEPICTUREVIEWER_H
