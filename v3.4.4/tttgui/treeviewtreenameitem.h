/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef treeviewtreenameitem_h__
#define treeviewtreenameitem_h__

// Qt
#include <QGraphicsTextItem>

// Forward declarations
class TreeViewTreeItem;
//class TreeViewSettings;


/**
 * Display tree name or description in a TreeView and can be used to move the tree also
 */
class TreeViewTreeNameItem : public QGraphicsTextItem {

public:

	/**
	 * Constructor
	 * @param _treeViewTreeItem the TreeViewTreeItem instance this text belongs to
	 * @param _text the text to display
	 */
	TreeViewTreeNameItem(TreeViewTreeItem* _treeViewTreeItem, const QString& _text);

	/**
	 * Update display (reflect appearance settings, update position)
	 */
	void updateDisplay();

	// Distance of tree name from middle (i.e. center of first track cell)
	static const int TREE_NAME_DIST_TO_CENTER = 100;

protected:

	// Mouse pressed (needs to be reimplemented in order to be able to receive mouse release events!)
	void mousePressEvent(QGraphicsSceneMouseEvent *_event);

	// Mouse button released event
	void mouseReleaseEvent(QGraphicsSceneMouseEvent* _event);

	// Mouse moved
	void mouseMoveEvent(QGraphicsSceneMouseEvent* _event);

	// Mouse hovering in
	void hoverEnterEvent(QGraphicsSceneHoverEvent* _event);

	// Mouse hovering out
	void hoverLeaveEvent(QGraphicsSceneHoverEvent* _event);

private:

	// The TreeViewTreeItem instance this text belongs to
	TreeViewTreeItem* treeViewTreeItem;

	//// Display settings
	//TreeViewSettings *displaySettings;

	// Tree movement
	bool movingTree;

};


#endif // treeviewtreenameitem_h__
