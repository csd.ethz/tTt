/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "movienextpositem.h"

// tTt
#include "../tttbackend/tttpositionmanager.h"
#include "../tttbackend/tttmanager.h"
#include "tttmovie.h"
#include "tttpositionlayout.h"
#include "tttdata/userinfo.h"
#include "tttbackend/picturearray.h"
#include "tttautotracking.h"
#include "tttautotrackingtreewindow.h"

// QT
#include <QGraphicsSceneHoverEvent>
#include <QBrush>
#include <QDebug>


// Width of buttons
const int MovieNextPosItem::WIDTH = 10;

// Distance from movie picture
const int MovieNextPosItem::BAR_DISTANCE_PIC = 2;

// Number of timepoints to load before and after the current, when neighbor pos is opened
const int MovieNextPosItem::TPS_TO_LOAD_BEFORE = 5;
//const int MovieNextPosItem::TPS_TO_LOAD_AFTER = 3;	// now user stylesheet setting "NumPicsToLoadByNextPosItem"

// Color sheme
const QColor MovieNextPosItem::ACTIVE_COLOR = QColor(0xAF,0xAF,0xFF);//QColor(0,0,255);
const QColor MovieNextPosItem::INACTIVE_COLOR = QColor(0xCF,0xCF,0xFF);

MovieNextPosItem::MovieNextPosItem( const QString& _posKey )
	: QGraphicsRectItem(), posKey(_posKey)
{
	setAcceptHoverEvents(true);

	setBrush(INACTIVE_COLOR);

	setToolTip("Load Position " + _posKey);
}


void MovieNextPosItem::hoverEnterEvent(QGraphicsSceneHoverEvent* _event)
{
	setBrush(ACTIVE_COLOR);
}

void MovieNextPosItem::hoverLeaveEvent(QGraphicsSceneHoverEvent* _event)
{
	setBrush(INACTIVE_COLOR);
}

void MovieNextPosItem::mousePressEvent( QGraphicsSceneMouseEvent* _event )
{
	// Get position manager for posKey
	TTTPositionManager* posManager = TTTManager::getInst().getPositionManager (posKey);
	TTTPositionManager* curPosManager = TTTManager::getInst().getCurrentPositionManager();

	if(posManager && curPosManager) {
		// Change to "wait" cursor
		QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));

		// Get current pic index
		PictureIndex curPI = curPosManager->getPictureIndex();
		if(curPI.TimePoint == -1) {
			qWarning() << "MovieNextPosItem::mousePressEvent(): Error 2 - " << posKey;
			QApplication::restoreOverrideCursor();
			return;
		}

		// Get position layout winodow
		TTTPositionLayout* posLayout = TTTManager::getInst().frmPositionLayout;
		if(posLayout) {
			// Change position and initialize
			if(!posLayout->setPosition(posKey/*, true*/)) {
				qWarning() << "MovieNextPosItem::mousePressEvent(): Error 3 - " << posKey;
				QApplication::restoreOverrideCursor();
				return;
			}

			//// Make sure pictures have been initialized
			//posLayout->markSelectedPicturesForLoading();


			/*
			Not done anymore, if there is no picture, user will notice anyways ;)

			// Check if any picture exists at the current timepoint in the new position
			if(!posManager->getPictures()->pictureExists(curPI.TimePoint, curPI.WaveLength)) {
				// Then check wavelength 0
				if(!posManager->getPictures()->pictureExists(curPI.TimePoint, 0)) {
					QApplication::restoreOverrideCursor();
					QMessageBox::information(posManager->frmMovie, "Note", "Cannot open position as there seems to be no picture for the current timepoint in that position.");
					return;
				}
				else {
					// Found w0 pic -> change wl to load to 0
					curPI.WaveLength = 0;
				}
			}
			*/

			// Stop tracking
			bool wasInTrackingMode = false;
			if(TTTManager::getInst().isTracking()) {
				curPosManager->frmMovie->stopTracking();
				wasInTrackingMode = true;
			}

			// Get number of timepoints to load after current
			int tpsToLoadAfter = UserInfo::getInst().getStyleSheet().getNumPicsToLoadByNextPosItem();
			if(tpsToLoadAfter < 1)
				tpsToLoadAfter = 1;

			// Try to load pictures from curTP - TPS_TO_LOAD_BEFORE to curTP + tpsToLoadAfter (or vice versa if in backward tracking mode)
			int startTP, endTP;
			if(!TTTManager::getInst().inBackwardTrackingMode()) {
				startTP = curPI.TimePoint - TPS_TO_LOAD_BEFORE;
				endTP = curPI.TimePoint + tpsToLoadAfter;
			}
			else {
				startTP = curPI.TimePoint - tpsToLoadAfter;
				endTP = curPI.TimePoint + TPS_TO_LOAD_BEFORE;
			}


			startTP = std::max(startTP, posManager->getFirstTimePoint());
			endTP = std::min(endTP, posManager->getLastTimePoint());

			if(startTP<1)
				startTP = 1;

			// Mark pics as to be loaded
			int lastTpWithPic = -1;
			for(int curTP = startTP; curTP <= endTP; curTP++) {
				if(posManager->getPictures()->pictureExists(curTP, curPI.zIndex, curPI.WaveLength)) {
					posManager->getPictures()->setLoading(curTP, curPI.zIndex, curPI.WaveLength, true);
					lastTpWithPic = curTP;
				}
			}
		
			// Load them..
			if(lastTpWithPic != -1)
				posManager->getPictures()->loadPictures(false);

			// Make sure to indicate that pictures have been loaded
			posLayout->markPicturesLoadedForCurrentPosition(true);

			if(posManager->frmMovie) {
				// Take over display settings
				if( UserInfo::getInst().getStyleSheet().gettakeOverDisplaySettingsNextPosItem() ) {
					// Copy settings for each wavelength
					DisplaySettings& oldPosSettings = curPosManager->getDisplays();
					DisplaySettings& newPosSettings = posManager->getDisplays();
					for(int wl = 0; wl < oldPosSettings.size() && wl < newPosSettings.size(); ++wl) {
						newPosSettings.at(wl) = oldPosSettings.at(wl);
					}
				}

				// Make sure new movie window is now on top
				posManager->frmMovie->show();
				posManager->frmMovie->raise();
				if(posManager->frmMovie->isMinimized())
					posManager->frmMovie->showMaximized();
				//posManager->frmMovie->activateWindow();
				//if(posManager->frmMovie->isMinimized())
				//	posManager->frmMovie->showNormal();

				// Exceptionally, we call setToTimepoint directly from here. Don't do that anywhere else.
				QApplication::processEvents();
				posManager->frmMovie->setToTimepoint(curPI.TimePoint, -1);

				// Enable tracking mode again
				if(wasInTrackingMode) {
					// Check if manual tracking was done in auto tracking mode
					TTTAutoTracking* at = TTTManager::getInst().frmAutoTracking;
					if(at && at->getTreeWindow() && at->getTreeWindow()->manualTrackingWasInAutoTrackingMode())
						at->getTreeWindow()->startManualTracking();
					else
						TTTManager::getInst().startTracking();
				}
			}
		}

		//TTTManager::getInst().setTimepoint(curPI.TimePoint-1);
		//posManager->setTimepoint(curPI.TimePoint-1, -1, true);
		//posManager->setTimepoint(curPI.TimePoint, -1, true);

		// Restore mouse pointer
		QApplication::restoreOverrideCursor();	
	}
	else
		qWarning() << "MovieNextPosItem::mousePressEvent(): Error 1 - " << posKey;
}

QRect MovieNextPosItem::transformRectToPixel( QRect _rect, int _myLeft, int _myTop, float _mmpp )
{
	QRect ret = QRect((_rect.left() - _myLeft)/_mmpp, (_rect.top() - _myTop)/_mmpp, _rect.width()/_mmpp, _rect.height()/_mmpp);
	return ret;
}



