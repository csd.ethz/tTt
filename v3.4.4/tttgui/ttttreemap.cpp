/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ttttreemap.h"
//Added by qt3to4:
#include <QResizeEvent>
#include <QMouseEvent>

TTTTreeMap::TTTTreeMap(QWidget *parent, const char *name)
    :QWidget(parent, name)/*, completeImage (0), image (0)*///, MiddleMouseDown (false), factorX (0.0f), factorY (0.0f)
{
        setupUi (this);

	cv = new Q3CanvasView (this);
	cv->setGeometry (0, 0, width(), height());
	cv->setHScrollBarMode (Q3ScrollView::AlwaysOff);
	cv->setVScrollBarMode (Q3ScrollView::AlwaysOff);
}

TTTTreeMap::~TTTTreeMap()
{
/*	if (image)
		delete image;*/
}

void TTTTreeMap::setCanvas (Q3Canvas *_canvas)
{
	cv->setCanvas (_canvas);
	
	QMatrix wm;
	float factorX = (float)width() / (float)cv->canvas()->width();
	float factorY = (float)height() / (float)cv->canvas()->height();
	wm.scale (factorX, factorY);
	cv->setWorldMatrix (wm);
}

void TTTTreeMap::resizeEvent (QResizeEvent *)
{
	if (! cv->canvas())
		return;
	
	cv->setGeometry (0, 0, this->geometry().width(), this->geometry().height());
	
	QMatrix wm;
	float factorX = (float)width() / (float)cv->canvas()->width();
	float factorY = (float)height() / (float)cv->canvas()->height();
	wm.scale (factorX, factorY);
	cv->setWorldMatrix (wm);
	
/*	drawTree (0);
	
	if (! completeImage)
		return;*/
	
	//adapt zoom factor of rectbox (== size of display for the tree : real tree size)
//	CurrentRegion.draw();
/*	factorX = (float)width() / (float)cv.canvas()->width();
	factorY = (float)height() / (float)cv.canvas()->height();*/
/*	factorX = (float)width() / (float)completeImage->width();
	factorY = (float)height() / (float)completeImage->height();*/
//	CurrentRegion.setZoomFactor (factorX, factorY);
//	CurrentRegion.draw();
}
// 
// void TTTTreeMap::paintEvent (QPaintEvent *)
// {
// 	tmr.singleShot (0, this, SLOT (drawTimerEvent()));
// 	
// /*	if (image) {
// 		bitBlt (this, 0, 0, image);
// 		tmr.singleShot (0, this, SLOT (drawTimerEvent()));
// 	}*/
// }    
// 
// void TTTTreeMap::drawTimerEvent()
// {
// //	CurrentRegion.draw();
// }
// 
void TTTTreeMap::mousePressEvent (QMouseEvent *ev)
{
	switch (ev->button()) {
		case Qt::MidButton:
			MiddleMouseDown = true;
			//oldMousePos = ev->pos();
			break;
		default:
			;
	}
}

// void TTTTreeMap::mouseMoveEvent (QMouseEvent *ev)
// {
// /*	if (! completeImage)
// 		return;*/
// 	
// 	if (MiddleMouseDown) {
// 		//shift box and emit signal to adapt other views
// 		
// //		CurrentRegion.draw();
// 		QPoint shift = ev->pos() - oldMousePos;
// 		shift.setX ((int)((float)shift.x() / factorX)); 
// 		shift.setY ((int)((float)shift.y() / factorY)); 
// 		CurrentRegion.shift (shift.x(), shift.y(), 0, 0, cv.canvas()->width(), cv.canvas()->height());
// 		//CurrentRegion.shift (shift.x(), shift.y(), 0, 0, completeImage->width(), completeImage->height());
// //		CurrentRegion.draw();
// 		
// 		emit displayedRegionShifted (shift);
// 		
// 		oldMousePos = ev->pos();
// 	}
// }
// 
void TTTTreeMap::mouseReleaseEvent (QMouseEvent *ev)
{
	//@todo set display in frmTracking to center currently clicked point
	QMatrix wm = cv->worldMatrix();
	switch (ev->button()) {
		case Qt::MidButton:
			MiddleMouseDown = false;
			//find out position in canvas
			if (wm.isInvertible()) {
				QPoint r =  wm.invert().map (ev->pos());
				//set canvas in TTTTracking/TreeDisplay to center this very point
				emit displayedRegionSet (r);
			}
			break;
		default:
			;
	}
}

void TTTTreeMap::setRectBox (QRect, int, int) //(QRect _displayedRegion, int _treeWidth, int _treeHeight)
{	
	//note: zoom factor is set to real value in resizeEvent()
/*	CurrentRegion.initRectBox (this, _displayedRegion, CurrentRegionColor, 1.0f);
	
	factorX = (float)width() / (float)_treeWidth;
	factorY = (float)height() / (float)_treeHeight;
	CurrentRegion.setZoomFactor (factorX, factorY);*/
}
// 
//SLOT:
void TTTTreeMap::setDisplayedRegion (QRect) // (QRect _displayedRegion)
{
//	CurrentRegion.draw();
//	CurrentRegion.setBox (_displayedRegion);
//	CurrentRegion.draw();
}

//SLOT:
void TTTTreeMap::shiftDisplayedRegion (QPoint, int) //(QPoint _shift, int _twhf)
{
/*	if (! completeImage)
		return;*/
	//if (_twhf == 0)
	//	return;
	
//	CurrentRegion.draw();
//	CurrentRegion.shift (_shift.x(), _shift.y() / _twhf, 0, 0, cv.canvas()->width(), cv.canvas()->height());
	//CurrentRegion.shift (_shift.x(), _shift.y() / _twhf, 0, 0, completeImage->width(), completeImage->height());
//	CurrentRegion.draw();
}
// 
// //void TTTTreeMap::drawTree (QPixmap *_pixmap)
// void TTTTreeMap::drawTree (QImage *_image)
// {
// 	//return;
// 	
// /*	if (_pixmap)
// 		pixmap = _pixmap;
// 	
// 	if (! pixmap)
// 		return;
// 	
// 	QImage ximg = pixmap->convertToImage();		
// 	
// 	if (image)
// 		delete image;
// 	image = new QImage (this->width(), this->height(), 32);
// 	
// 	*image = ximg.scale (this->width(), this->height());
// 	
// 	if (image)
// 		bitBlt (this, 0, 0, image);*/
// 
// 	
// 	
// 	
// 	
// // 	if (_image)
// // 		completeImage = _image;
// // 	
// // 	if (! completeImage)
// // 		return;
// // 	
// // 	if (image)
// // 		delete image;
// // 	image = new QImage (this->width(), this->height(), 32);
// // 	
// // 	*image = completeImage->scale (this->width(), this->height());
// // 	
// // 	if (image)
// // 		bitBlt (this, 0, 0, image);
// 
// }

//#include "ttttreemap.moc"
