/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ttttracking.h"

// Project includes
#include "tttbackend/tttmanager.h"
#include "tttdata/stylesheet.h"
#include "treedisplay.h"
#include "timescale.h"
#include "trackingmenubar.h"
#include "ttttreemap.h"
#include "tttregionselection.h"
#include "tttpositionlayout.h"
#include "tttcellproperties.h"
#include "ttttreemerging.h"
#include "tttbackend/treestatistics.h"
#include "tttsplashscreen.h"
#include "ttttracksoverview.h"
#include "ttttextbox.h"
#include "tttsymbolselection.h"
#include "tttio/fastdirectorylisting.h"
#include "myqchecklistitem.h"
#include "ttttreestyleeditor.h"
#include "tttbackend/miningresults.h"
#include "tttversioninfo.h"
#include "tttbackend/picturearray.h"
#include "statusbar.h"
#include "positionthumbnail.h"
#include "tttgui/tttautotracking.h"
#include "tttbackend/changecursorobject.h"
#include "tttmovieexplorer.h"
#include "tttconvertoldtrees.h"


// For debugging only
//#ifdef DEBUGMODE
#include "ttttest.h"
//#endif

// Qt includes
#include <QResizeEvent>
#include <QCloseEvent>
#include <QStatusBar>
#include <QTextStream>
#include <QKeyEvent>
#include <QPaintEvent>
#include <QShowEvent>
#include <QLabel>
#include <QSpinBox>
#include <QSettings>
#include <QProcess>
#include <QFileDialog>
#include <QProgressDialog>

// STL
#include <algorithm>

// Qt3 includes
#include <Q3ListBox>
#include <Q3ListView>
#include <Q3GridLayout>
#include <Q3HBoxLayout>

// Debug information variable defined in main.cpp
extern int g_debugInformation;


TTTTracking::TTTTracking(QWidget *parent, const char *)
    : QWidget (parent), timeScale (0),
     TrackCount (0), ShowSpeed (false),
     ShowColocation (false)
{
    setupUi (this);

	inheritedWLs = 0;
	movieExplorer = 0;
	convertOldTreesWindow = 0;

	timeScale = new TimeScale (fraTimeScale, this);
	//fileHandler = new TTTFileHandler ();
	
	treeView = new TreeDisplay (fraCellTree, this);

	// Create menu
	menuBar = new TrackingMenuBar(this);
	menuBar->setGeometry(QRect(0, 0, 5000, 21));

	// Create statusbar
	statusBar = new QStatusBar(this);
	statusBar->setSizeGripEnabled(false);

	// Backward tracking mode label
	lblBackwardTrackingMode = new QLabel("", statusBar);
	statusBar->addPermanentWidget(lblBackwardTrackingMode);
	updateBackwardTrackingModeDisplay();

	// Create permanent message label
	permanentStatusMessage = new QLabel();
	resetPermanentStatusbarMessage();
	statusBar->addPermanentWidget(permanentStatusMessage);

	// Create status bar buttons
	pbtStatusBarOpenMovieWindow = new QPushButton("Movie Window", statusBar);
	pbtStatusBarOpenMovieWindow->setToolTip("Open movie window for current position");
	statusBar->addPermanentWidget(pbtStatusBarOpenMovieWindow);
	connect ( pbtStatusBarOpenMovieWindow, SIGNAL (clicked()), this, SLOT (openMovieWindow()));

	pbtStatusBarGoToFirstPos = new QPushButton("First Position of Tree", statusBar);
	pbtStatusBarGoToFirstPos->setToolTip("Go to position in which the current tree starts");
	pbtStatusBarGoToFirstPos->setEnabled(false);
	statusBar->addPermanentWidget(pbtStatusBarGoToFirstPos);
	connect ( pbtStatusBarGoToFirstPos, SIGNAL (clicked()), this, SLOT (goToFirstPosOfTree()));

	// Set statusbar layout
	statusBar->setMaximumHeight(25);
	if(statusBar->layout()) {
		statusBar->layout()->setContentsMargins(8, 0, 8, 0);
		statusBar->layout()->setSpacing(3);
	}
	mainLayout->addWidget(statusBar);
	
	connect ( treeView, SIGNAL (treeShifted (QPoint, int)), timeScale, SLOT (shiftTimeScale (QPoint)));
	//connect ( timeScale, SIGNAL (contentsMoving (int, int)), TreeView, SLOT (moveDisplay (int, int)));
	
	//connect ( TreeView, SIGNAL (treeShifted (QPoint, int)), frmTreeMap, SLOT (shiftDisplayedRegion (QPoint, int)));
	connect ( treeView, SIGNAL (trackSelected (Track *)), this, SLOT (selectTrack (Track *)));
	connect ( treeView, SIGNAL (trackDoubleclicked (Track *)), this, SLOT (handleCellDoubleClick (Track *)));
	connect ( treeView, SIGNAL (commentDeSelected ()), fraComment, SLOT (hide()));
	connect ( treeView, SIGNAL (commentSelected (QString, QPoint, int)), this, SLOT (showComment (QString, QPoint, int)));
	
	//connect ( frmTreeMap, SIGNAL (displayedRegionShifted (QPoint)), TreeView, SLOT (shiftDisplay (QPoint)));
	connect ( TTTManager::getInst().frmTreeMap, SIGNAL (displayedRegionSet (QPoint)), treeView, SLOT (setDisplayCenter (QPoint)));
	
	

	
	lvwCurrentTreeItems->clear();
	
        spbZoomFactor = new QSpinBox (fraSpbZoomFactor);
	spbZoomFactor->setGeometry (0, 0, 70, 20);
	spbZoomFactor->setMaxValue (1000);
	spbZoomFactor->setMinValue (20);
	spbZoomFactor->setLineStep (10);
	spbZoomFactor->setValue (100);
	spbZoomFactor->setSuffix (" %");
	
	
        spbLoadInterval = new QSpinBox (fraSpbLoadInterval);
	spbLoadInterval->setGeometry (0, 0, 70, 20);
	spbLoadInterval->setMaxValue (1000);
	spbLoadInterval->setMinValue (1);
	spbLoadInterval->setValue (1);
	
	
//	pbtTrackCell->hide();
        menuBar->actTracking_START->setEnabled (false);

//	pbtDeleteAllTrackPoints->hide();
//	pbtDeleteStopReason->hide();
        menuBar->actTracking_DELETE_TRACKPOINTS->setEnabled (false);
        menuBar->actTracking_DELETESTOPREASON->setEnabled (false);
	
	fraComment->hide();
	
	
	//connect calls ordered by tabwidget associations (see ui for details)
	
	//[out of tabs]
//	connect ( pbtClose, SIGNAL (clicked()), this, SLOT (close()));
	//connect ( pbtAddColony, SIGNAL (clicked()), this, SLOT (addColony()));
	connect ( sldTimePoint, SIGNAL (valueChanged (int)), this, SLOT (setTimePoint (int)));
	connect ( sldTimePoint, SIGNAL (sliderMoved (int)), this, SLOT (setTimePoint (int)));
//	connect ( lboColony, SIGNAL (mouseButtonClicked (int, QListBoxItem *, const QPoint &)), this, SLOT (handleColonyClick (int, QListBoxItem *, const QPoint &)));
	//connect ( pbtSetColony, SIGNAL (clicked()), this, SLOT (selectColony()));
	//connect ( pbtLoadLastAutosave, SIGNAL (clicked()), this, SLOT (loadLastAutosavedColony()));
	connect ( pbtComment, SIGNAL (clicked()), fraComment, SLOT (hide()));
	
	//cell data
//	connect ( pbtTrackCell, SIGNAL (toggled (bool)), this, SLOT (startCellTracking (bool)));
//	connect ( pbtDeleteTrack, SIGNAL (clicked()), this, SLOT (deleteTrack()));
//	connect ( pbtDeleteStopReason, SIGNAL (clicked()), this, SLOT (deleteStopReason()));
//	connect ( pbtCellComment, SIGNAL (clicked()), this, SLOT (editCellComment()));
//	connect ( pbtDeleteAllTrackPoints, SIGNAL (clicked()), this, SLOT (deleteAllTrackPoints()));
	connect ( pbtCellChooseProperties, SIGNAL (clicked()), this, SLOT (showCellProperties()));
	connect ( pbtCellSetProperties, SIGNAL (clicked()), this, SLOT (setCellProperties()));
	
	//tree display
	connect ( spbZoomFactor, SIGNAL (valueChanged (int)), this, SLOT (drawTree()));
	connect ( spbZoomFactor, SIGNAL (valueChanged (int)), this, SLOT (updateTimeScale()));


	connect ( pbtShowColocation_All, SIGNAL (toggled (bool)), this, SLOT (showColocation (bool)));
	connect ( pbtShowColocation_Cell, SIGNAL (toggled (bool)), this, SLOT (showColocation (bool)));
	connect ( pbtSelectColocationRadius, SIGNAL (clicked()), this, SLOT (selectCoLocationRadius()));
	connect ( scrColocationRadius, SIGNAL (valueChanged (int)), this, SLOT (setColocationRadius (int)));
	connect ( pbtShowSpeed, SIGNAL (toggled (bool)), this, SLOT (showSpeed (bool)));
	connect ( scrSpeedAmplification, SIGNAL (valueChanged (int)), this, SLOT (setSpeedAmplification (int)));
	connect ( pbtNormalizeSpeed, SIGNAL (clicked()), this, SLOT (normalizeSpeed()));
//	connect ( pbtTreeMap, SIGNAL (clicked()), this, SLOT (showTreeMap()));
//	connect ( pbtResetGraphics, SIGNAL (clicked()), this, SLOT (resetGraphics()));
//	connect ( pbtCenterCell, SIGNAL (clicked()), this, SLOT (centerCell()));
        //connect ( pbtShowGenerationTime, SIGNAL (toggled (bool)), this, SLOT (showGenerationTime (bool)));

	connect ( chkContinuousTreeLines, SIGNAL (toggled (bool)), this, SLOT (setDrawContinuousCellLines (bool)));
	
	//loading + saving
	connect ( spbLoadInterval, SIGNAL (valueChanged (int)), this, SLOT (setLoadInterval (int)));
	//connect ( pbtSaveTTTFile, SIGNAL (clicked()), this, SLOT (saveCurrentFile()));
	//connect ( pbtSaveTTTFileDifferentName, SIGNAL (clicked()), this, SLOT (saveCurrentFile()));
	//connect ( pbtSelectTimePoints, SIGNAL (toggled (bool)), this, SLOT (selectRegion (bool)));
	//connect ( pbtSelectPictures, SIGNAL (clicked()), this, SLOT (selectPictures()));
	//connect ( pbtDeSelectPictures, SIGNAL (clicked()), this, SLOT (selectPictures()));
	//connect ( pbtUnloadTimePoints, SIGNAL (toggled (bool)), this, SLOT (unloadPictures (bool)));
	//connect ( pbtContinueLoading, SIGNAL (toggled (bool)), this, SLOT (loadPictures (bool)));
	
	//BS 2010/03/19 wavelength buttons are now created dynamically due to a change to higher wavelength counts

        connect ( pbtAddDefaultSelection, SIGNAL (toggled (bool)), this, SLOT (setDefaultSelection (bool)));
	connect ( lieLoadTimePointStart, SIGNAL (returnPressed()), this, SLOT (setLoadTimePoint()));
	connect ( lieLoadTimePointEnd, SIGNAL (returnPressed()), this, SLOT (setLoadTimePoint()));

	//cell selection
        connect ( pbtSelectCells, SIGNAL (clicked()), this, SLOT (selectCells()));
        connect ( pbtDeselectCells, SIGNAL (clicked()), this, SLOT (selectCells()));
		//does not work (confuses indices): connect ( bgrCellSelectionType, SIGNAL (clicked (int)), this, SLOT (setCellTypeSelection (int)));
	connect ( optSelectAllCells, SIGNAL (clicked()), this, SLOT (setCellTypeSelection()));
	connect ( optSelectApoptoticCells, SIGNAL (clicked()), this, SLOT (setCellTypeSelection()));
	connect ( optSelectDividingCells, SIGNAL (clicked()), this, SLOT (setCellTypeSelection()));
	connect ( optSelectInterruptedCells, SIGNAL (clicked()), this, SLOT (setCellTypeSelection()));
	connect ( optSelectLostCells, SIGNAL (clicked()), this, SLOT (setCellTypeSelection()));
	
	//export
//	connect ( pbtExportCells, SIGNAL (clicked()), this, SLOT (exportCells()));
//	connect ( pbtExportTree, SIGNAL (clicked()), this, SLOT (exportTree()));
//	connect ( pbtExportCompleteTree, SIGNAL (clicked()), this, SLOT (exportTree()));
//	connect ( pbtExportCompleteTreeUnAltered, SIGNAL (clicked()), this, SLOT (exportTree()));
//	connect ( pbtDivisionTimeIntervals, SIGNAL (clicked()), this, SLOT(exportTreeData()));
//	connect ( pbtFirstDivisionTime, SIGNAL (clicked()), this, SLOT(exportTreeData()));
//	connect ( pbtApoptosisInterval, SIGNAL (clicked()), this, SLOT(exportTreeData()));
//	connect ( pbtExportSelectedCells, SIGNAL (clicked()), this, SLOT(exportTreeData()));
//	connect ( pbtExportCellSpeed, SIGNAL (clicked()), this, SLOT (exportTreeData()));
//	connect ( pbtExportCellCount, SIGNAL (clicked()), this, SLOT (exportTreeData()));
//	connect ( pbtExportAllTreeSnapshots, SIGNAL (clicked()), this, SLOT (exportAllTreeSnapshots()));
	
	//statistics
	connect ( pbtCalculatePositionStatistics, SIGNAL (clicked()), this, SLOT (calculatePositionStatistics()));
	
	//legend
	connect ( chkTreeFeaturesVisible, SIGNAL (toggled (bool)), this, SLOT (setAllTreeElementsVisibility (bool)));
	connect ( lvwCurrentTreeItems, SIGNAL (spacePressed (Q3ListViewItem *)), this, SLOT (setTreeItemVisible (Q3ListViewItem *)));
	connect ( lvwCurrentTreeItems, SIGNAL (clicked (Q3ListViewItem *)), this, SLOT (setTreeItemVisible (Q3ListViewItem *)));
	
	//mining & clustering
	connect ( pbtSelectTreeMinerFile, SIGNAL (clicked()), this, SLOT (selectTreeMinerFile()));
	connect ( pbtLoadTreeMinerFile, SIGNAL (clicked()), this, SLOT (loadTreeMinerFile()));
	connect ( pbtOverlayClusters, SIGNAL (clicked()), this, SLOT (overlayClusters()));
	connect ( pbtShowFrequentTrees, SIGNAL (clicked()), this, SLOT (showFrequentTrees())),
	
	//miscellaneous
	connect ( pbtConvertWrongInvertedCoordinates, SIGNAL (clicked()), this, SLOT (convertWrongInvertedCoordinates()));
	connect ( pbtReadMatlabTracks, SIGNAL (clicked()), this, SLOT (readExternalTracks()));
	connect ( pbtReplaceTracksWithExternal, SIGNAL (clicked()), this, SLOT (replaceTracksWithExternal()));
	connect ( pbtReplaceCurrentTPTracksWithExternal, SIGNAL (clicked()), this, SLOT (replaceTracksWithExternal()));
	//connect ( pbtCorrectFactorProblemTracks, SIGNAL (clicked()), this, SLOT (correctFactorProblemTracks()));
	connect ( pbtDeleteStoredPositionsFromTree, SIGNAL (clicked()), this, SLOT (eraseStoredPositions()));
	connect ( pbtMergeTrees, SIGNAL (clicked()), this, SLOT (mergeTrees()));
	connect ( pbtSetLoadingStartToFirstTP, SIGNAL (clicked()), this, SLOT (setLoadingStartToFirstTp()));
	connect ( pbtSetLoadingEndToLastTP, SIGNAL (clicked()), this, SLOT (setLoadingEndToLastTp()));

	
	//position history
	connect ( pbtUpdatePositionHistory, SIGNAL (clicked()), this, SLOT (updatePositionHistory()));
	
	
	//cell option group
	connect ( pbtCellPropsOpenFurtherOptions, SIGNAL (clicked()), this, SLOT (openFurtherCellOptions()));
	
        //@todo: following five: buttons no longer exist, connect slots to (new) menu items, if desired
        //connect ( pbtShowZoom, SIGNAL (clicked()), this, SLOT (showZoomWindow()));
        //connect ( pbtShowCellsOnZoom, SIGNAL (toggled (bool)), this, SLOT (showCellsOnZoom (bool)));
        //connect ( pbtShowMovie, SIGNAL (toggled (bool)), this, SLOT (showMovieForm (bool)));
        //connect ( pbtLoadAllInterruptedCells, SIGNAL (clicked()), this, SLOT (loadAllInterruptedCells()));
        //connect ( pbtMeasureTime, SIGNAL (toggled (bool)), this, SLOT (measureTime (bool)));



        TTTManager::getInst().setTree (new Tree());
	
	QHash<int, TTTPositionManager*>& allPositions = TTTManager::getInst().getAllPositionManagers();
	for (auto iter = allPositions.begin(); iter != allPositions.end(); ++iter) {
		connect ( (*iter), SIGNAL (TWHFChanged (int)), this, SLOT (updateTWHF (int)));
	}
	
	connect ( &TTTManager::getInst(), SIGNAL (timepointSet (int, int)), this, SLOT (setToTimepoint (int, int)));
	connect ( &TTTManager::getInst(), SIGNAL (currentPositionChanged (int, int)), this, SLOT (currentPositionChanged (int, int)));
	
	
	//apply compilation flags
	//-> hide controls triggering modules that should not be visible
	
	if (! COMPLFAG_CUSTOM_ZOOM_TREE)
		spbZoomFactor->hide();
	
	if (! COMPFLAG_SPEED_DISPLAY) {
		pbtShowSpeed->hide();
		scrSpeedAmplification->hide();
		pbtNormalizeSpeed->hide();
		lblSpeedAmplifier->hide();
		grbSpeed->hide();
	}
	
	if (! COMPFLAG_COLOCATION_DISPLAY) {
		pbtShowColocation_Cell->hide();
		pbtShowColocation_All->hide();
		pbtSelectColocationRadius->hide();
		scrColocationRadius->hide();
		lblCoLocationRadius->hide();
		grbColocation->hide();
	}
	
	if (! COMPFLAG_TREE_MAP)
//		pbtTreeMap->hide();
                menuBar->actView_TREE_OVERVIEW->setVisible (false);
	
		
	//toggle double -> speed display is up to date
	showSpeed (pbtShowSpeed->isOn());
	showColocation (pbtShowColocation_Cell->isOn());
	
	lblCoLocationRadius->setText (QString ("current: %1 (pixel)").arg (scrColocationRadius->value()));
	lblSpeedAmplifier->setText (QString ("current: %1").arg (scrSpeedAmplification->value()));
	
	chkTreeFeaturesVisible->setChecked (true);
	
	DataChanged = false;
	TimeScaleChanged = TRUE;
	
	StartLoading = EndLoading = -1;
	LoadingFlag = false;
	LoadInterval = 1;
	
	TTTFileLoaded = false;

	windowLayoutLoaded = false;
	
	SpeedAmplifier = 1;
	SelectionCounter = 0;
	CurrentColonyName = "";
	
        chkRead4MoreBytesInTrees->setChecked (false);

	currentCellType = CT_NOCELLTYPE;
	
	for (int i = 0; i <= MAX_WAVE_LENGTH; i++)
		WaveLengthSelected [i] = 0;
	
	//setting the first bit is equal to selecting the wavelength completely
/*	if (pbtSelectWaveLength0->isOn())
		WaveLengthSelected [0] = 128;
	if (pbtSelectWaveLength1->isOn())
		WaveLengthSelected [1] = 128;
	if (pbtSelectWaveLength2->isOn())
		WaveLengthSelected [2] = 128;
	if (pbtSelectWaveLength3->isOn())
		WaveLengthSelected [3] = 128;
	if (pbtSelectWaveLength4->isOn())
		WaveLengthSelected [4] = 128;
	if (pbtSelectWaveLength5->isOn())
		WaveLengthSelected [5] = 128;*/
	
	
	connect ( &tmrBackup, SIGNAL (timeout()), this, SLOT (createBackup()));
	tmrBackup.start (BACKUP_INTERVAL);
	
	call_slider_setTimePoint = true;

	// Init caption
	setFormCaption();

	// Inherited wavelengths
	inheritedWLs = new QVector<bool>(menuBar->actTracking_INHERIT_WLS.size(), false);

	// Init changelog
	tbwChangelog->setColumnCount(2);
	QStringList headerList;
	headerList.append("Date");
	headerList.append("User");
	tbwChangelog->setHorizontalHeaderLabels(headerList);
}

TTTTracking::~TTTTracking()
{
	//if (timeScale)
	//	delete timeScale;	<-- OH: is our child, deleted automatically
	
	//if (TreeView)
	//	delete TreeView;	<-- OH: is our child, deleted automatically
	
	//if (menuBar)
	//	delete menuBar;		<-- OH: is our child, deleted automatically

	//if (statusBar)
	//	delete statusBar;	<-- OH: is our child, deleted automatically

	if(inheritedWLs)
		delete inheritedWLs;

	if(movieExplorer) 
		delete movieExplorer;

	if(convertOldTreesWindow)
		delete convertOldTreesWindow;
}

void TTTTracking::createWavelengthButtons()
{

	//create wavelength buttons
	//note: reuse the existing buttons, if this function is called more than once
	
	TTTPositionManager *tttpm = TTTManager::getInst().getBasePositionManager();
	
	if (! tttpm)
		return;
	
	//loading and saving section
	//==========================
	
	//apply MAX_WAVE_LENGTH, if changed, to rows and cols
	int grid_rows = 3;
	int grid_cols = 3;
	Q3GridLayout *grl = 0;
	
	//use current layout, if a grid layout is already existing
	QLayout *layout = bgrWavelengthButtons->layout();
	if (layout) {
		if (layout->isA ("QGridLayout"))
			grl = (Q3GridLayout*)layout;
	}
	
	if (! grl)
		grl = new Q3GridLayout (bgrWavelengthButtons, grid_rows, grid_cols);
	
	int button_count = 0;
	int max_height = bgrWavelengthButtons->geometry().height() / grid_rows - 2;
	
	const QSet<int>& wls = tttpm->getAvailableWavelengthsByRef();
	for (QSet<int>::const_iterator iter = wls.begin(); iter != wls.end(); ++iter) {
		
		int wl = *iter;
		
		//find existing button for this wavelength...
		QString button_name = QString ("wlbutton_%1").arg (wl);
		bool buttonExists = (bgrWavelengthButtons->child (button_name) != 0);
		
		//...if it exists, ok
		
		//...if not, create it
		if (! buttonExists) {
			QPushButton *wlbutton = new QPushButton (QString ("%1").arg (wl), bgrWavelengthButtons, button_name);
			wlbutton->setToggleButton (true);
			wlbutton->setMaximumSize (45, max_height);
			wlbutton->show();
			wlbutton->setOn (false);
			bgrWavelengthButtons->insert (wlbutton, wl);
			
			int row = button_count / grid_cols;
			int col = button_count % grid_cols;
			grl->addWidget (wlbutton, row, col);
		}
		
		button_count++;
	}
	
	connect ( bgrWavelengthButtons, SIGNAL (clicked (int)), this, SLOT (selectWaveLength (int)));
	
	
	
	//cell properties section
	//=======================
	
	//apply MAX_WAVE_LENGTH, if changed, to rows and cols
	Q3HBoxLayout *hbl = 0;
	
	//use current layout, if a grid layout is already existing
	layout = bgrCellProperties->layout();
	if (layout) {
		if (layout->isA ("QHBoxLayout"))
			hbl = (Q3HBoxLayout*)layout;
	}
	
	if (! hbl) {
		if(bgrCellProperties->layout())
			delete bgrCellProperties->layout();

		hbl = new Q3HBoxLayout (bgrCellProperties);
	}
	
	button_count = 0;
	//int max_height = bgrWavelengthButtons->geometry().height() / grid_rows - 2;
	
	for (QSet<int>::const_iterator iter = wls.begin(); iter != wls.end(); ++iter) {
		
		int wl = *iter;
		
		//find existing button for this wavelength...
		QString button_name = QString ("wlbutton_cell_prop%1").arg (wl);
		bool buttonExists = (bgrCellProperties->child (button_name) != 0);
		
		//...if it exists, ok
		
		//...if not, create it
		if (! buttonExists) {
			QPushButton *wlbutton = new QPushButton (QString ("%1").arg (wl), bgrCellProperties, button_name);
			wlbutton->setToggleButton (true);
			wlbutton->setMaximumSize (28, 100);	//the width should adapt automatically
			wlbutton->show();
			wlbutton->setOn (false);
			bgrCellProperties->insert (wlbutton, wl);
			
			hbl->addWidget (wlbutton);
		}
		
		button_count++;
	}
	
	hbl->insertStretch (-1);
	connect ( bgrCellProperties, SIGNAL (clicked (int)), this, SLOT (setCellPropertyWavelength (int)));
	
	///@todo write more code...
	bgrCellProperties->hide();
	pbtCellPropsOpenFurtherOptions->hide();
}

//EVENT:
void TTTTracking::resizeEvent (QResizeEvent *)
{
	treeView->fit();
	
	if (TTTManager::getInst().frmTreeMap)
		TTTManager::getInst().frmTreeMap->setDisplayedRegion (treeView->getDisplayRegion());
	
	timeScale->fit();
}

void TTTTracking::keyReleaseEvent (QKeyEvent *ev)
{
	QPoint shift (0, 0);
	
	switch (ev->key()) {
//		case Qt::Key_F2:
//			pbtTrackCell->animateClick();
//			break;
//		case Qt::Key_F3:
//			pbtResetGraphics->animateClick();
//			break;
/*		case Qt::Key_F5:
			pbtSelectTimePoints->animateClick();
			break;
		case Qt::Key_F6:
			pbtSelectPictures->animateClick();
			break;
		case Qt::Key_F7:
			pbtDeSelectPictures->animateClick();
			break;
		case Qt::Key_F8:
			pbtContinueLoading->animateClick();
			break;
		case Qt::Key_F9:
			pbtUnloadTimePoints->animateClick();
			break;
		case Qt::Key_F10:
			pbtSaveTTTFile->animateClick();
			break;*/
//		case Qt::Key_F11:
//			pbtTreeMap->animateClick();
//			break;
/*		case Qt::Key_F12:
			pbtCenterCell->animateClick();
			break;*/
		case Qt::Key_Plus:
			spbZoomFactor->setValue (spbZoomFactor->value() + 10);
			break;
		case Qt::Key_Minus:
			spbZoomFactor->setValue (spbZoomFactor->value() - 10);
			break;
		
		//next 4 keys: navigate tree (num block arrangement!)
/*		case Qt::Key_8:
			if (shift.isNull())
				shift.setY (-10);
		case Qt::Key_2:
			if (shift.isNull())
				shift.setY (+10);
		case Qt::Key_6:
			if (shift.isNull())
				shift.setX (+10);
		case Qt::Key_4:
			if (shift.isNull())
				shift.setX (-10);
			
			if (! shift.isNull()) {
				TreeView->shiftDisplay (shift);
				if (tree) {
					shift.setY (shift.y() * tree->getTWHF());
					frmTreeMap->shiftDisplayedRegion (shift, tree->getTWHF());
				}
			}
			break;*/
		
		case Qt::Key_C:
                        if ((ev->modifiers() & Qt::ControlModifier) == Qt::ControlModifier) {
				//toggle colocation for current cell
				pbtShowColocation_Cell->toggle();
			}
			break;
		case Qt::Key_S:
                        if ((ev->modifiers() & Qt::ControlModifier) == Qt::ControlModifier) {
				//toggle speed for current cell
				pbtShowSpeed->toggle();
			}
			break;
		
/*		case Qt::Key_:
			break;*/
		
		default:
			ev->ignore();
			return;
	}
	
	ev->accept();
}

//void TTTTracking::initForms()
//{
////	Q3DictIterator<TTTPositionManager> iter (TTTManager::getInst().getAllPositionManagers());
////	for ( ; iter.current(); ++iter) {
////		if (iter.current()->frmMovie) {
////			connect ( iter.current()->frmMovie, SIGNAL (RadiusSelected (int)), this, SLOT (displayRadius (int)));
////		}
////		
////		//@todo full RAM
/////*		if (TTTManager::getInst().getCurrentPositionManager().getPictures()) {
////			connect (TTTManager::getInst().getCurrentPositionManager().getPictures(), SIGNAL (LoadedPicture (unsigned int, int, bool)), this, SLOT (updateSelectionCounter (unsigned int, int, bool)));
////			connect (TTTManager::getInst().getCurrentPositionManager().getPictures(), SIGNAL (RAMFull()), this, SLOT (watchRAM()));
////		}*/
////		
////		if (iter.current()->getPictures()) {
////			connect (iter.current()->getPictures(), SIGNAL (RAMFull()), this, SLOT (watchRAM()));
////			connect (iter.current()->getPictures(), SIGNAL (LoadedPicture (unsigned int, int, bool)), this, SLOT (updateSelectionCounter (unsigned int, int, bool)));
////		}
////	}
//	
//	//if (TTTManager::getInst().frmTreeStyleEditor) {
//	//	connect ( TTTManager::getInst().frmTreeStyleEditor, SIGNAL (styleChosen (StyleSheet &)), this, SLOT (applyStyleSheet (StyleSheet &)));
//	//}
//
//}

void TTTTracking::setTimePoints (int _firstTP, int _lastTP, bool _addColony)
{
	timeScale->init();
	treeView->init (TTTManager::getInst().getTree(), TTTManager::getInst().getBasePositionManager());
	
	
	sldTimePoint->setRange (_firstTP, _lastTP);
	sldTimePoint->setLineStep (1);
	sldTimePoint->setPageStep ((_lastTP - _firstTP) / 10);
	
	StartLoading = _firstTP;
	EndLoading = _lastTP;
	
	// Make sure we are not in region selection mode
	timeScale->selectRegion(0);

	// Make sure selection is synchronous in timescale
	timeScale->resetSelection();
	
	if (TTTManager::getInst().frmTreeMap)
		TTTManager::getInst().frmTreeMap->setRectBox (treeView->getDisplayRegion(), treeView->getTreeWidth(), treeView->getTreeHeight());
	
	if (TTTManager::getInst().getBasePositionManager()->getPictures())
		SelectionCounter = TTTManager::getInst().getBasePositionManager()->getPictures()->countSelectedPictures();
	
	if ((! TTTFileLoaded) && _addColony)
		//start a new colony (create a base track)
		addColony();
	
	//create a new track for the base cell
	if (! TTTManager::getInst().getTree()->getBaseTrack()) {
		Track *tmpTrack = new Track (0, 0, TTTManager::getInst().getBasePositionManager()->getLastTimePoint());
		tmpTrack->setNumber (1);
		TTTManager::getInst().getTree()->insert (tmpTrack);
		TTTManager::getInst().setCurrentTrack (tmpTrack);
	}
	
}

//SLOT:
void TTTTracking::setToTimepoint (int _newTimepoint, int)
{
	if (! TTTManager::getInst().getBasePositionManager())
		return;
	if (! TTTManager::getInst().getCurrentPositionManager())
		return;
	
	treeView->displayTimePoint(); // (_newTimepoint, _deleteOld);
	
	if (TTTManager::getInst().getBasePositionManager()->getFiles())
		lblCurrentTimePoint->setText (QString("%1 <font size=""-2"">(%2)</font>").
			arg (_newTimepoint).
			arg (TTTManager::getInst().getBasePositionManager()->getFiles()->getRealTime (_newTimepoint, 1).toString (COMPLETE_DATE_FORMAT)));
	
	showCommentEmbold (_newTimepoint);
	
	if (TTTManager::getInst().getBasePositionManager()->frmRegionSelection->getShowCells()) {
		TTTManager::getInst().getBasePositionManager()->frmRegionSelection->repaint();
	}
	
	if (sldTimePoint->value() != _newTimepoint) {
		//note: no further calls are triggered, as the signalling cascade is prohibited
		call_slider_setTimePoint = false;
		sldTimePoint->setValue (_newTimepoint);
		call_slider_setTimePoint = true;
	}
}

//SLOT: inits the tracking of a cell
void TTTTracking::startCellTracking (bool _start)
{
	if (! treeView)
		return;
	
	if (! _start)
		//this method is only used for starting; if _start is false, it was called by code
		return;
	
//	pbtDeleteAllTrackPoints->show();
//	pbtDeleteStopReason->show();
        menuBar->actTracking_DELETE_TRACKPOINTS->setEnabled (true);
        menuBar->actTracking_DELETESTOPREASON->setEnabled (true);
	
	
	TTTManager::getInst().startTracking();
	
	//when tracking is finished, the method stopCellTracking() is called
}

void TTTTracking::stopCellTracking()
{
	if (! treeView)
		return;
        if (! TTTManager::getInst().isTracking())
                return;

	DataChanged = TRUE;
//	pbtDeleteAllTrackPoints->hide();
//	pbtDeleteStopReason->hide();
        menuBar->actTracking_DELETE_TRACKPOINTS->setEnabled (false);
        menuBar->actTracking_DELETESTOPREASON->setEnabled (false);
	
//	if (pbtTrackCell->isOn())
//		pbtTrackCell->toggle();
        //if (menuBar->actTracking_START->isChecked())
        //        menuBar->actTracking_START->setChecked (false);
	
	displayTrackInformation();
	
	treeView->draw();
	refreshStatistics();
	updatePositionHistory();
	
	TrackCount = TTTManager::getInst().getTree()->getTrackCount();	
}

//SLOT:
void TTTTracking::drawTree (Track *_track)
{
	
	if (TTTManager::getInst().frmTreeMap)
		TTTManager::getInst().frmTreeMap->setDisplayedRegion (treeView->getDisplayRegion());
	
	if ((sender() == spbZoomFactor))
		treeView->setZoomFactor (spbZoomFactor->value());
	else {
		treeView->draw (_track);
	}
	
}

//SLOT:
void TTTTracking::displayTrackInformation()
{
	if (! TTTManager::getInst().getCurrentTrack()) {
		//clear view
		lblCellGeneration->setText ("Generation: ");
		lblCellStatus->setText ("");
		lblCellLifeCycle->setText ("");
		lblCellNumber->setText ("");
		lblCellRealTime->setText ("");
		lblCellTimeAbs->setText ("");
		lblCellActionRadius->setText ("");
		if (TTTManager::getInst().getTree()->getBaseTrack())
			if (TTTManager::getInst().getTree()->getBaseTrack()->tracked())
//				pbtTrackCell->hide();
                                menuBar->actTracking_START->setEnabled (false);

		QFont f( "Sans Serif", 10, QFont::Normal );
//		pbtCellComment->setFont (f);
                menuBar->actEdit_CELL_COMMENT->setFont (f);
		lieCellPropsFromTP->setText ("");
		lieCellPropsToTP->setText ("");
		lblCurrentPosition->setText ("");
		
		updatePositionHistory();	//delete the history of the old cell
	}
	else {
		Track *track = TTTManager::getInst().getCurrentTrack();
		
		lblCellGeneration->setText (QString ("Generation: %1").arg (track->getGeneration()));
		lblCellStatus->setText (track->getStopReasonString());
		lblCellLifeCycle->setText (QString ("%1 - %2").arg (track->getFirstTrace()).arg (track->getLastTrace()));
		lblCellNumber->setNum (track->getNumber());
		if (track->getFirstTrace() > 0) {
			QDateTime first = TTTManager::getInst().getBasePositionManager()->getFiles()->getRealTime (track->getFirstTrace(), 1);
			QDateTime last;
			if (track->getLastTrace() > 0)
				last = TTTManager::getInst().getBasePositionManager()->getFiles()->getRealTime (track->getLastTrace(), 1);
			
			//lblCellRealTime->setText (	"<font size=""-1"">" + first.toString (Qt::LocalDate) + " - " + last.toString (Qt::LocalDate) + "</font>");
			lblCellRealTime->setText ("<font size=""-1"">" + first.toString (COMPLETE_DATE_FORMAT) + " - " + last.toString (COMPLETE_DATE_FORMAT) + "</font>");
			
			int secs = first.secsTo (last);
			if (secs > 0) {
				int min = secs / 60;
				int hours = min / 60;
				int days = hours / 24;
				lblCellTimeAbs->setText (QString ("d: %1; h: %2; m: %3; s: %4").arg (days).arg (hours).arg (min).arg (secs));
			}
			else
				lblCellTimeAbs->setText ("");
			
			
			if (track->tracked()) {
				//calculate the radius in which the cell is moving and the complete covered distance over its lifetime
				QString cellActionRadius = "";
				
				int car = 0, ctd = 0;
				track->calcMovements (car, ctd);
				
				cellActionRadius += QString ("Radius = %1").arg (car);
				cellActionRadius += "\n";
				cellActionRadius += QString ("Total dist = %1").arg (ctd);
				lblCellActionRadius->setText (cellActionRadius);
			}
			else
				lblCellActionRadius->setText ("");
			
			
			lieCellPropsFromTP->setText (QString ("%1").arg (track->getFirstTrace()));
			lieCellPropsToTP->setText (QString ("%1").arg (track->getLastTrace()));
			
			//display the current position where the track is roaming (use last trackpoint as indicator)
			//new system where positions are stored needs to be regarded
			TrackPoint trpo = track->getTrackPoint (track->getLastTrace());
			if (trpo.TimePoint != -1) {
				if (trpo.Position.toInt() > 0) {
					lblCurrentPosition->setText (QString ("Current position:\n%1").arg (trpo.Position));
				}
				else {
					//old system
					
                                        QPointF lastPoint = trpo.point();
					//TTTPositionManager *tttpm = TTTManager::getInst().getPositionAt (lastPoint.x(), lastPoint.y());
					Q3IntDict<TTTPositionManager> tttpms = TTTManager::getInst().getAllPositionsAt (lastPoint.x(), lastPoint.y());
					QString text = "Current position:\n";
					for (uint cc = 0; cc < tttpms.count(); cc++)
						text += (tttpms [cc])->positionInformation.getIndex() + "?";
					
					lblCurrentPosition->setText (text);
				}
			}
		}
//		if (TTTManager::getInst().isTracking())
//			//there still is a track being edited -> no other track can be edited!
//			pbtTrackCell->hide();
//                        menuBar->actTracking_START->setEnabled (false);
		
		//current politics:
		//track data can be overwritten -> button is always visible
		//the overwriting is limited to use- and reasonable bounds (timepoint shifts are only available in range)
		/*
		if (TTTManager::getInst().getCurrentTrack()->tracked()) {
			if (TTTManager::getInst().getCurrentTrack()->getStopReason() == TS_NONE)
				//tracking was interrupted before and is now continued
				pbtTrackCell->show();
			else
				pbtTrackCell->hide();
		}
		else
		*/
//		pbtTrackCell->show();
                menuBar->actTracking_START->setEnabled (true);
		
		
		//if a comment is available at its first trace, the button's caption is embolded
		showCommentEmbold (TTTManager::getInst().getTimepoint());
	}
}

//SLOT:
void TTTTracking::updateTimeScale (unsigned int, int ) //_waveLength)
{
	TimeScaleChanged = TRUE;

	//// Update timepoints
	//timeScale->selectRegion (StartLoading, EndLoading);

	displayTimeScale(); // (_timePoint, _timePoint);
}

//SLOT:
void TTTTracking::displayTimeScale() // (long int _timePointStart, int _timePointEnd)
{
	if (! timeScale)
		return;
	if (! treeView)
		return;
	
	timeScale->setZoomFactor (spbZoomFactor->value());
	
	if (TimeScaleChanged) {			//the timescale has to be repainted as different pictures were loaded
		TimeScaleChanged = false;
		//timeScale->draw (_timePointStart, _timePointEnd, TreeView->getLWF());
		timeScale->draw();
	}
	//timeScale->display();
}

//SLOT:
void TTTTracking::selectTrack (Track *_track)
{
	if (TTTManager::getInst().getCurrentTrack()) {
		//delete speed and colocation display of last current cell
		treeView->clear (TTTManager::getInst().getCurrentTrack(), false, true, false);
		treeView->clear (TTTManager::getInst().getCurrentTrack(), false, false, true);
		treeView->update();
		updatePositionHistory();
	}
	
	TTTManager::getInst().setCurrentTrack (_track);		//might be 0!
	
	displayTrackInformation();
	
	if (sender() != treeView) {
                //if this slot was not called by TreeView but by frmMovie, the tree display has to be updated
		treeView->setCurrent (_track);
	}
	
	if (_track) {
		if (ShowSpeed | ShowColocation) {
                        treeView->draw (_track, true, ShowSpeed, ShowColocation, false);
		}
	}
}

//SLOT:
void TTTTracking::addColony()
{
	//stop tracking if still in progress
	if (TTTManager::getInst().isTracking())
		if (TTTManager::getInst().getBasePositionManager())
			TTTManager::getInst().getBasePositionManager()->frmMovie->stopTracking();
	
	if (! SaveDialog()) 
		return;
	
	currentFilenameFormCaption = "(new colony)";
	this->setFormCaption ();
	
	TTTManager::getInst().getTree()->reset();
	TTTManager::getInst().clearFields();
	
	//create a new base track
	//int start = TTTManager::getInst().getBasePositionManager()->getFirstTimePoint();
	Track *x = new Track (0, 0, TTTManager::getInst().getBasePositionManager()->getLastTimePoint(), -1);
	x->setNumber (1);
	TTTManager::getInst().getTree()->insert (x);
	treeView->draw();
	updateLegend();
	
	currentFilename = "";
	DataChanged = false;
	TrackCount = 0;		//base track is not tracked!
	TTTManager::getInst().setCurrentTrack (0);
//	pbtTrackCell->show();
        menuBar->actTracking_START->setEnabled (true);
//	lboColony->setCurrentItem (0);		//set empty file
	CurrentColonyName = "";
	refreshStatistics();
	//updatePositionHistory();

	// Disable open next/prev actions
	menuBar->actFile_OPEN_NEXT_TREE->setEnabled(false);
	menuBar->actFile_OPEN_PREV_TREE->setEnabled(false);
}

//SLOT:
bool TTTTracking::saveTTTFile (bool _withNewName)
{
/*	if (! fileHandler)
		return false;*/
	
	TTTPositionManager *baseTTTPM = TTTManager::getInst().getBasePositionManager();
	
	if (! baseTTTPM)
		return false;
	
	QString filename = currentFilename;
	
	//the suffix is the colony number - it is set to the highest available number + 1 and is the suggestion for the new index of the new colony
	QString suffix = TTTManager::getInst().getNextFreeFile (baseTTTPM, true);
	
	//get highest available colony number (both NAS and the external disk need to be scanned!)
	
	bool success = false;
	
	//NOTE: if the NAS drive exists (is mounted), the files are always
	//		 written to the network - normally, the experiment folders are on an (write-protected) NTFS disk!
	
	//if ((currentFilename.left (1) == "@")) {
	//QDir nas (TTTManager::getInst().getNASDrive() + "TTTfiles/");
	//QDir nas (SystemInfo::getTTTFileFolderFromBaseName (TTTManager::getInst().getBasePositionManager()->getBasename(), true));
	QDir nas (baseTTTPM->getTTTFileDirectory (true));
		
	QString suggestion = nas.absPath() + "/" + baseTTTPM->getBasename() + suffix;
	
	//if the usersign should be appended automatically...
	if (UserInfo::getInst().getStyleSheet().getAddUserSignToTTTFiles()) {
		//...then do it!
		//.ttt is at the end of the file in any case
		//QString tmp = filename.left (filename.length() - 4);
		
		//check if last four letters are the colony number
		//if (tmp.find (COLONY_MARKER) == tmp.length() - 4)
			//only then add user sign, cos otherwise the structure of the filename is not clear and could be destroyed
		//	filename = tmp + UserInfo::getInst().getUsersign() + ".ttt";
		
		suggestion += UserInfo::getInst().getUsersign();
	}
	
	//ask for filename
	if (filename.isEmpty() || _withNewName)
		filename = Q3FileDialog::getSaveFileName (suggestion, "tTt files (*" + TTT_FILE_SUFFIX + ")", this, 0, "Enter a ttt-filename");
	
	if (filename.isEmpty())
		return false;			//user cancelled
	
	if (filename.right (4) != TTT_FILE_SUFFIX)
		filename.append (TTT_FILE_SUFFIX);
	
	//check if file already exists (if chosen manually), warn if true
	while ((currentFilename.isEmpty() || _withNewName) & QFile::exists (filename)) {
		int ret = QMessageBox::question (this, "tTt", "File already exists. Do you want to overwrite it?", "Yes", "No", "Cancel");
		if (ret == 0)
			//user wants to overwrite file - leave loop
			break;
		else if (ret == 1)
			//user does not want to overwrite file - ask for new name
			filename = Q3FileDialog::getSaveFileName (nas.absPath() + "/" + baseTTTPM->getBasename() + suffix, "tTt files (*" + TTT_FILE_SUFFIX + ")", this, 0, "Enter a ttt-filename");
		else if (ret == 2)
				//user cancelled dialog - exit saving procedure
			return false;
	}
	
	if (filename == QString::null)
		return false;			//user cancelled

	// Change cursor to wait cursor
	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
	
	if (filename.right (4) != TTT_FILE_SUFFIX)
		filename.append (TTT_FILE_SUFFIX);

	// If data has been changed..
	if(DataChanged) {
		// Append change log entry if it is not identical with the last change log entry
		QPair<QDate, QByteArray> changeEntry(QDate::currentDate(), UserInfo::getInst().getUsersign().toLatin1());
	
		// Check if it is identical and add it if not
		const QPair<QDate, QByteArray>* lastEntry = TTTManager::getInst().getTree()->getLastChangelogEntry();
		if(lastEntry == 0 || *lastEntry != changeEntry) {
			TTTManager::getInst().getTree()->appendToChangelog(changeEntry);
			
			// Update display
			updateChangelog();
		}
	}
	
	//save files in local backup directory
	//**************************************
	QString backupPath = SystemInfo::homeDirectory() + TTTFILES_FOLDER + "/" + baseTTTPM->getBasename();
	SystemInfo::checkNcreateDirectory (backupPath, true, true);
/*	QDir backup (backupPath);
	//create subdirectory for current experiment (if it not yet exists)
	bool folderexists = backup.cd (baseTTTPM->getBasename());
	if (! folderexists) {
		backup.mkdir (baseTTTPM->getBasename());
		backup.cd (baseTTTPM->getBasename());
	}*/
	backupPath += "/" + filename.mid (filename.findRev ('/') + 1);			//extract pure filename
	
	success = TTTFileHandler::saveFile (backupPath, TTTManager::getInst().getTree(), baseTTTPM->getFirstTimePoint(), baseTTTPM->getLastTimePoint());
	if (baseTTTPM->frmMovie)
                success &= baseTTTPM->getSymbolHandler().saveSymbolFile (backupPath + "symbols");
	//**************************************
	
	
	// ------ Oliver ------
	// What is this good for? Causes tree to be saved in wrong position, if tree is not from this position
	// and can cause trees to be saved in a different location than selected by the user..
	////save files on net
	////**************************************
	////receive only the pure filename (example: 05_p001-001.ttt)
	//filename = filename.mid (filename.findRev ('/') + 1);
	//filename = nas.absPath() + "/" + filename;
	
	//if the files already exist on the NAS, they are deleted first, 
	// cos otherwise no writing is possible
	//QString output = "";
	//QString command = "";
	//command = "rm -f '" + filename + "'";
	//output = "";
	//success = SystemInfo::execute (command, output);
	QFile::remove(filename);
	//command = "rm -f '" + filename + "comments'";
	//output = "";
	//success = SystemInfo::execute (command, output);
	QFile::remove(filename + "comments");
	//command = "rm -f '" + filename + "symbols'";
	//output = "";
	//success = SystemInfo::execute (command, output);
	QFile::remove(filename + "symbols");
	
	//save files
	if (baseTTTPM->frmMovie)
                success = baseTTTPM->getSymbolHandler().saveSymbolFile (filename + "symbols");

	success = TTTFileHandler::saveFile (filename, TTTManager::getInst().getTree(), baseTTTPM->getFirstTimePoint(), baseTTTPM->getLastTimePoint());
	//**************************************

	if(!success) {
		// Get system error
		QString sysError;
		char* lastSystemError = strerror(errno);
		if(lastSystemError)
			sysError = QString(lastSystemError);
		
		// Display error message
		QString msg = QString("Could not save file \"%1\".\nSystem reports: %2").arg(filename).arg(sysError);

		QMessageBox::critical(this, "Saving failed.", msg);
	}
			
		
	currentFilename = filename;
	
	updateColonyList (filename, true);

	// Restore cursor
	QApplication::restoreOverrideCursor();
	
	return success;
}
	
void TTTTracking::updateColonyList (QString _filename, bool _setCurrent)
{
	//update colony listbox and select current colony
	
/*	int index = -1;
	//QDir nas (TTTManager::getInst().getBasePositionManager()->getTTTFileDirectory (true));
	
	QString tmpInsert = _filename.mid (_filename.findRev ('/') + 1);
	QString checkString = tmpInsert;
	int insert_index = 1;
	for (int i = 0; i < (int)lboColony->count(); i++) {
		if (lboColony->text (i) == checkString) {			
			//filename already in the list
			index = i;
			break;
		}
		
		if ((lboColony->text (i).left (3) == "---") & (i > 2)) {
			//below the dashes are the files in the old folder system
			//thus we have to check for the marker+filename
			checkString = OLD_FOLDER_TTTFILE_MARKER + tmpInsert;
			
			insert_index = i;
		}
	}
	if (index == -1) {						
		//if filename is not yet present
		lboColony->insertItem (tmpInsert, insert_index - 1);
	}*/
	
	if (_setCurrent) {
		currentFilenameFormCaption = _filename;
		this->setFormCaption (_filename);
	}
	
/*	if (_setCurrent)
		lboColony->setCurrentItem (index);*/
}	

bool TTTTracking::loadTTTFile (QString _filename)
{
	
	//note: tree is reset!
	TTTPositionManager *tttpm = TTTManager::getInst().getBasePositionManager();
	if (! tttpm)
		return false;
	
	
	int ftp = 0, ltp = 0;

        QString fn = _filename;
	if (_filename.left (1) == OLD_FOLDER_TTTFILE_MARKER)
                //remove the "@" symbol from the front
                fn = _filename.mid (1);

        bool read4MoreBytes = chkRead4MoreBytesInTrees->isChecked();

        int loadingMessage = TTTFileHandler::readFile (fn, TTTManager::getInst().getTree(), TrackCount, ftp, ltp, &tttpm->positionInformation, read4MoreBytes);
        switch (loadingMessage) {
                case TTT_READING_SUCCESS:
                        TTTFileLoaded = true;
                        break;
                case TTT_READING_FILE_NOT_FOUND:
                        TTTFileLoaded = false;
                        break;
                case TTT_READING_BYTE_COUNT_ERROR: {
                        TTTFileLoaded = false;
                        //inform the user about this problem, retry with "4 bytes more" set
                        QString message = "Due to an unknown reason, the byte frame of this tree file is corrupted.\nPlease do the following: check or uncheck the box 'Read with +4 byte frame' on the left,\ntry opening the file again and, if successful, immediately save it (press F10).\nSorry for the inconvenience.";
                        QMessageBox::warning (this, "Loading problem", message, QMessageBox::Ok);

                        return TTTFileLoaded;
                }
                default:
                        TTTFileLoaded = false;
        }

	//note: as ftp is not stored, it is not set in readFile() - thus use the former first timepoint
	TTTManager::getInst().getCurrentPositionManager()->setExperimentTimepoints (TTTManager::getInst().getCurrentPositionManager()->getFirstTimePoint(), ltp);
	
	if (TTTFileLoaded) {
		currentFilename = _filename;
		CurrentColonyName = _filename.section ('/', -1, -1);		//get the real filename, without path
	}
	
	//filename for symbol file is dependent on the current colony
	//=> xxx.tttsymbols
	if (TTTManager::getInst().getCurrentPositionManager()->frmMovie) {
		if (currentFilename.left (1) == OLD_FOLDER_TTTFILE_MARKER)
			TTTManager::getInst().getCurrentPositionManager()->getSymbolHandler().readSymbolFile (currentFilename.mid (1) + "symbols", TTTManager::getInst().getTree());
		else
			TTTManager::getInst().getCurrentPositionManager()->getSymbolHandler().readSymbolFile (currentFilename + "symbols", TTTManager::getInst().getTree());
	}
	
	TTTManager::getInst().frmTreeMap->setCanvas (treeView->getCanvas());
	
	TTTManager::getInst().setCurrentTrack (0);

	// Draw changelog
	updateChangelog();
	
	return TTTFileLoaded;
}

//SLOT:
void TTTTracking::editCellComment()
{
	if (TTTManager::getInst().getCurrentPositionManager()->frmMovie) {
		TTTManager::getInst().getCurrentPositionManager()->frmMovie->editCellComment (TTTManager::getInst().getCurrentTrack(), TTTManager::getInst().getTimepoint());
	}
}

//SLOT:
void TTTTracking::saveCellComment (QString _comment)
{
	if (! TTTManager::getInst().getCurrentTrack()) 
		return;
	
	TTTManager::getInst().getCurrentTrack()->setComment (_comment, TTTManager::getInst().getTimepoint());
	DataChanged = true;
	
	treeView->draw (TTTManager::getInst().getCurrentTrack(), true, false, false, false);
}

//SLOT:
void TTTTracking::selectRegion (bool _checked)
{
	
	if (! timeScale)
		return;

        //bool start = ! menuBar->isItemChecked (FILE_LOADING_PICTURES_TOGGLE_SELECTION);
        bool start = _checked;

        //activeAction() is null within an already called slot...!
        //QAction *actAct = menuBar->activeAction();
        //if (actAct)
        //        start = ! actAct->isChecked();

//        start = ! menuBar->actFile_LOADING_PICTURES_TOGGLE_SELECTION->isChecked();
//        MenuManager::actFile_LOADING_PICTURES_TOGGLE_SELECTION->setChecked (start);
	
	//lblSelectedTimepoints->setText (QString ("Selected TPs \n %1 - %2 \n").arg (StartLoading).arg (EndLoading));
	
	lieLoadTimePointStart->setText (QString().setNum (StartLoading));
	lieLoadTimePointEnd->setText (QString().setNum (EndLoading));
	
	connect ( timeScale, SIGNAL (regionSelected (int, int)), this, SLOT (setLoadPictures (int, int)));
	
	//if changed, also the button states have to be changed
	//for (int i = 0; i <= MAX_WAVE_LENGTH; i++)
	//	WaveLengthSelected [i] = false;
	
	if (start)
		timeScale->selectRegion (1); //(_index == 0) ? 1 : 2);
	else
		timeScale->selectRegion (0);
}

//SLOT:
void TTTTracking::setLoadPictures (int _start, int _stop)
{
	if (_start > -1) {
		StartLoading = _start;
		EndLoading = _stop;
	}
	
	lieLoadTimePointStart->setText (QString().setNum (StartLoading));
	lieLoadTimePointEnd->setText (QString().setNum (EndLoading));
	
}

//SLOT:
void TTTTracking::selectPictures (int _deselect) 
{
	if (! TTTManager::getInst().getCurrentPositionManager()->getPictures())
		return;
	
	int counter = 0;
	bool select = true;
	
	//if (sender() == pbtDeSelectPictures)
	if (_deselect == 1)
		select = false;
	
	LoadInterval = (spbLoadInterval->value());
	
	bool oldSelectionStatus = false;
	
	
	//get associations for all wavelengths (cf. annotations to array wavelength[] in ttttracking.h)
	//name usage:
	//slaves are the wavelengths that are only loaded with others
	//masters are the wavelengths that are loaded independently and have others pending on them to be loaded
	QString slaveWLs = lieAddWLSelection->text();
	QString masterWLs = lieToWLSelection->text();
	if ((! slaveWLs.isEmpty()) & (! masterWLs.isEmpty())) {
		QStringList slaves = QStringList::split (",", slaveWLs);
		QStringList masters = QStringList::split (",", masterWLs);
		for (QStringList::Iterator it = slaves.begin(); it != slaves.end(); ++it ) {
			int slaveNumber = (*it).toInt();
			if (slaveNumber >= 0 && slaveNumber <= MAX_WAVE_LENGTH) {
				WaveLengthSelected [slaveNumber] &= 128;		//delete bits 1-7, leave bit 0 untouched
				for (QStringList::Iterator it2 = masters.begin(); it2 != masters.end(); ++it2 ) {
					int masterNumber = (*it2).toInt();
					if (masterNumber >= 0 && masterNumber <= MAX_WAVE_LENGTH && masterNumber != slaveNumber) {
						//calculate bit (in powers of 2, of course)
						masterNumber = (int)pow(2.0, 6 - masterNumber);
						//set corresponding bit
						WaveLengthSelected [slaveNumber] |= masterNumber;
					}
				}
			}
		}
	}
	else {
		for (int i = 1; i <= MAX_WAVE_LENGTH; i++)
			WaveLengthSelected [i] &= 128;		//delete all bits except the first
	}
	
	
	for (int i = StartLoading; i <= EndLoading; i++) {
		if (TTTManager::getInst().getCurrentPositionManager()->getFiles()->exists (i, 1, -1)) {
			if (counter % LoadInterval == 0) {			//regard desired interval
				for (int j = 0; j <= MAX_WAVE_LENGTH; j++)
					//check for associations: 128 == 10000000
					//if this is true, the first bit is set, and this wavelength j should be loaded regardless of others
					if ((WaveLengthSelected [j] & 128) > 0) {
						oldSelectionStatus = TTTManager::getInst().getCurrentPositionManager()->getPictures()->getSelectionFlag (i, 1, j);
						if (select != oldSelectionStatus) {
							TTTManager::getInst().getCurrentPositionManager()->getPictures()->setSelection (i, 1, j, select);
							if (select)
								SelectionCounter++;
							else
								SelectionCounter--;
						}
						
						//if ((j > 0) & SelectDefaultPictures) {
						//if ((j > 0) & (WaveLengthSelected [0] & 127 == 127)) {
						
						for (int k = 0; k <= MAX_WAVE_LENGTH; k++) {
							//is any wavelength k associated to wavelength j?
							int xwl = (int)pow (2.0, 6 - j);
							if ((k != j) & (WaveLengthSelected [k] && xwl == xwl)) {
								//if the picture of wavelength k should be loaded for all existing
								// and selected pictures of wavelength j, the existence has to be
								// checked for wavelength k and, if positive, these pictures need to be
								// selected for loading
								if (TTTManager::getInst().getCurrentPositionManager()->getFiles()->exists (i, 1, j)) {
									oldSelectionStatus = TTTManager::getInst().getCurrentPositionManager()->getPictures()->getSelectionFlag (i, 1, k);
									TTTManager::getInst().getCurrentPositionManager()->getPictures()->setSelection (i, 1, k, select);
									if (select != oldSelectionStatus) {
										if (select)
											SelectionCounter++;
										else
											SelectionCounter--;
									}
								}
							}
						}
					}
					else
						TTTManager::getInst().getCurrentPositionManager()->getPictures()->setSelection (i, 1, j, false);
			}
			else
				TTTManager::getInst().getCurrentPositionManager()->getPictures()->setSelection (i, 1, -1, false);
			
			counter++;
		}
	}
		
	//remove old selection denotion in timescale
	//timeScale->draw (OldStartLoading, OldEndLoading);
	//timeScale->display();
	
	//lblSelectedTimepoints->setText (QString ("Selected TPs \n %1 - %2 \n").arg (StartLoading).arg (EndLoading));
	lblSelectedTimepoints->setText (QString ("%1 pictures selected").arg (TTTManager::getInst().getCurrentPositionManager()->getPictures()->countSelectedPictures()));
	lieLoadTimePointStart->setText (QString().setNum (StartLoading));
	lieLoadTimePointEnd->setText (QString().setNum (EndLoading));
	
	//update timescale (draw selected pictures with other color)
	//timeScale->draw (StartLoading, EndLoading, TreeView->getLWF());
	timeScale->draw();
	//timeScale->display();
}

//SLOT:
void TTTTracking::loadPictures (int _unload)
{
	bool start = true;
	bool loading = true;
	

    if (_unload == 0) {
		loading = true;
		
//		start = ! menuBar->isItemChecked (FILE_LOADING_PICTURES_LOAD);
		start = ! menuBar->actFile_LOADING_PICTURES_LOAD->isChecked();
		menuBar->actFile_LOADING_PICTURES_LOAD->setChecked (start);
//		menuBar->setItemChecked (FILE_LOADING_PICTURES_LOAD, start);
	}
	else {
		loading = false;
		
//		start = ! menuBar->isItemChecked (FILE_LOADING_PICTURES_UNLOAD);
                start = ! menuBar->actFile_LOADING_PICTURES_UNLOAD->isChecked();
                menuBar->actFile_LOADING_PICTURES_UNLOAD->setChecked (start);
//		menuBar->setItemChecked (FILE_LOADING_PICTURES_UNLOAD, start);
	}
	
	if (! TTTManager::getInst().getCurrentPositionManager())
		return;
	if (! TTTManager::getInst().getCurrentPositionManager()->getPictures())
		return;
	if (EndLoading == -1)
		return;
	
	if (start) {
		if (loading) {
			
			StartLoading = (lieLoadTimePointStart->text()).toInt();
			EndLoading = (lieLoadTimePointEnd->text()).toInt();
			
/*			if (pbtSelectTimePoints->isOn())
				pbtSelectTimePoints->toggle();*/
			//menuBar->setItemChecked (FILE_LOADING_PICTURES_TOGGLE_SELECTION, false);
			//menuBar->actFile_LOADING_PICTURES_TOGGLE_SELECTION->setChecked(false);
			
			
			int pictureCount = 0;
			//for (int i = StartLoading; i <= EndLoading; i++) {
			for (int i = TTTManager::getInst().getCurrentPositionManager()->getFirstTimePoint(); i <= TTTManager::getInst().getCurrentPositionManager()->getLastTimePoint(); i++) {
				for (int j = 0; j <= MAX_WAVE_LENGTH; j++)
					if (TTTManager::getInst().getCurrentPositionManager()->getPictures()->getSelectionFlag (i, 1, j)) {
						TTTManager::getInst().getCurrentPositionManager()->getPictures()->setLoading (i, 1, j, true);
						TTTManager::getInst().getCurrentPositionManager()->getPictures()->setSelection (i, 1, j, false);
						pictureCount++;
					}
			}
			
			//the timescale changing signal is already connected in frmMainWindow!
			connect (TTTManager::getInst().getCurrentPositionManager()->getPictures(), SIGNAL (LoadingComplete()), this, SLOT (finished_un_Loading()));
			//connect (TTTManager::getInst().getCurrentPositionManager()->getPictures(), SIGNAL (RAMFull()), this, SLOT (watchRAM()));
			
			//BS 2010/02/17: test for speed 
			//501 pics, combined (20 attempts): 42 secs
			//501 pics, async: 28 secs
			//501 pics, sync: 45 secs
			//1502 pics, combined (10 atts): 126 secs
			//1502 pics, sync: 135 secs
			//=> use the combined method
			
			//the status bar is displayed only if the pictures are loaded asynchronously
			//StatusBar *sb = new StatusBar (0, "Loading pictures...", 1, pictureCount, 1);
			
			//TTTManager::getInst().getCurrentPositionManager()->getPictures()->loadPictures (TTTManager::getInst().getCurrentPositionManager()->getFirstTimePoint(), TTTManager::getInst().getCurrentPositionManager()->getLastTimePoint(), -1, true, sb);
			TTTManager::getInst().getCurrentPositionManager()->getPictures()->loadPictures (true, false);
			
			//if (TTTManager::getInst().frmPositionLayout->getCurrentThumbnail())
			//	TTTManager::getInst().frmPositionLayout->getCurrentThumbnail()->markPicturesLoaded (true);

			//TTTManager::getInst().frmPositionLayout->markPicturesLoadedForCurrentPosition(true);
		}
		else {	//unloading

			//disconnect (TTTManager::getInst().getCurrentPositionManager()->getPictures(), SIGNAL (RAMFull()), this, SLOT (watchRAM()));
			
			int ret = 0;
			if (start)
				//ret = QMessageBox::question (this, "Security first", "Do you really want to unload the selected pictures?", QMessageBox::Yes, QMessageBox::No | QMessageBox::Default);
				ret = QMessageBox::question (this, "tTt", "Do you really want to unload the selected pictures?", "Yes", "No", QString::null, 0, 1);
			
			// !Qt constants do not really work!
			if (ret == 0) {
			
/*				if (pbtSelectTimePoints->isOn())
					pbtSelectTimePoints->toggle();*/
				//menuBar->setItemChecked (FILE_LOADING_PICTURES_TOGGLE_SELECTION, false);
				//menuBar->actFile_LOADING_PICTURES_TOGGLE_SELECTION->setChecked(false);
				
				//for (int i = StartLoading; i <= EndLoading; i++) {
				for (int i = TTTManager::getInst().getCurrentPositionManager()->getFirstTimePoint(); i <= TTTManager::getInst().getCurrentPositionManager()->getLastTimePoint(); i++) {
					for (int j = 0; j <= MAX_WAVE_LENGTH; j++)
						if (TTTManager::getInst().getCurrentPositionManager()->getPictures()->getSelectionFlag (i, 1, j)) {
							TTTManager::getInst().getCurrentPositionManager()->getPictures()->setLoading (i, 1, j, false);
							TTTManager::getInst().getCurrentPositionManager()->getPictures()->setSelection (i, 1, j, false);
						}
				}
			
				//the timescale changing signal is already connected in frmMainWindow!
				//connect (TTTManager::getInst().getCurrentPositionManager()->getPictures(), SIGNAL (LoadedPicture (unsigned int, int, bool)), this, SLOT (updateSelectionCounter (unsigned int, int, bool)));
				connect (TTTManager::getInst().getCurrentPositionManager()->getPictures(), SIGNAL (LoadingComplete()), this, SLOT (finished_un_Loading()));
				
				//BS 2010/02/17: unloading is now asynchronous, thus much faster
				TTTManager::getInst().getCurrentPositionManager()->getPictures()->loadPictures (false, true);
				
			}
		}
	}
	else 	//stop loading
		TTTManager::getInst().getCurrentPositionManager()->getPictures()->stopLoading();
	
	
}

//SLOT:
void TTTTracking::finished_un_Loading ()
{
	//pbtSelectTimePoints->setOn (false);
/*	if (pbtSelectTimePoints->isOn())
		pbtSelectTimePoints->toggle();
	pbtUnloadTimePoints->setOn (false);
	
	if (pbtContinueLoading->isOn())
		pbtContinueLoading->toggle();*/
	
	//menuBar->setItemChecked (FILE_LOADING_PICTURES_LOAD, false);
	//menuBar->setItemChecked (FILE_LOADING_PICTURES_UNLOAD, false);
	//menuBar->setItemChecked (FILE_LOADING_PICTURES_TOGGLE_SELECTION, false);

	//menuBar->actFile_LOADING_PICTURES_LOAD->setChecked(false);
	//menuBar->actFile_LOADING_PICTURES_UNLOAD->setChecked(false);
	//menuBar->actFile_LOADING_PICTURES_TOGGLE_SELECTION->setChecked(false);
	
	//if (TTTManager::getInst().frmPositionLayout->getCurrentThumbnail()) {
		//check if there are really no pictures left in memory
		if (TTTManager::getInst().getCurrentPositionManager()) {
			if (TTTManager::getInst().getCurrentPositionManager()->getPictures()->getLoadedPictures() == 0)	
				TTTManager::getInst().frmPositionLayout->markPicturesLoadedForCurrentPosition(false);
				//TTTManager::getInst().frmPositionLayout->getCurrentThumbnail()->markPicturesLoaded (false);
			else
				TTTManager::getInst().frmPositionLayout->markPicturesLoadedForCurrentPosition(true);
		}
	//}
	
	
	//disconnect (TTTManager::getInst().getCurrentPositionManager()->getPictures(), SIGNAL (RAMFull()), this, SLOT (watchRAM()));
	
	//StartLoading = EndLoading = -1;
	//lblSelectedTimepoints->setText ("Nothing selected");
	updateTimeScale();
}

// //SLOT:
// void TTTTracking::setTimePoint (int _timePoint)
// {
// 	//************************************************************
// 	//WARNING: do not emit SIGNAL (TrackingTimePointSet (int)) !!!
// 	//		   otherwise infinite recursion occurs!
// 	//************************************************************
// 	
// 	if (_timePoint == TTTManager::getInst().getCurrentPositionManager()->getTimepoint())
// 		return;
// 	
// 	_timePoint = QMIN (_timePoint, LastTimePoint);
// 	_timePoint = QMAX (_timePoint, FirstTimePoint);
// 	
// 	//this automatically calls displayTimePoint()
// 	sldTimePoint->setValue (_timePoint);
// }

//SLOT:
void TTTTracking::deleteTrack()
{
	if (! TTTManager::getInst().getCurrentTrack())
		return;
	if (TTTManager::getInst().isTracking())
		return;
	if (! treeView)
		return;
	
	DataChanged = treeView->deleteTrack (TTTManager::getInst().getCurrentTrack());
	TTTManager::getInst().setCurrentTrack (0);
	TrackCount = TTTManager::getInst().getTree()->getTrackCount();	
}

//SLOT: called by the slider
void TTTTracking::setTimePoint (int _timePoint)
{
	if (! call_slider_setTimePoint)
		return;
	
	//set timepoint globally (only if it has changed)
	TTTManager::getInst().setTimepoint (_timePoint);
}

//SLOT:
void TTTTracking::showZoomWindow()
{
	TTTManager::getInst().getCurrentPositionManager()->frmRegionSelection->show();
}

//SLOT:
void TTTTracking::showCellsOnZoom (bool _on)
{
	TTTManager::getInst().getCurrentPositionManager()->frmRegionSelection->setShowCells (_on);
	
	TTTManager::getInst().getCurrentPositionManager()->frmRegionSelection->repaint();
}

//SLOT:
void TTTTracking::showMovieForm (bool _show)
{
	if (! TTTManager::getInst().getCurrentPositionManager()->frmMovie)
		return;
	
	if (_show) {
		if (! TTTManager::getInst().getCurrentPositionManager()->frmMovie->isVisible())
			TTTManager::getInst().getCurrentPositionManager()->frmMovie->show();
	
		TTTManager::getInst().getCurrentPositionManager()->frmMovie->raise();
	}
	else
		TTTManager::getInst().getCurrentPositionManager()->frmMovie->hide();
}

//SLOT:
void TTTTracking::showColocation (bool _show)
{
	if (! TTTManager::getInst().getTree())
		return;
	
	ShowColocation = _show;
	
	if (sender() == pbtShowColocation_All) {
		treeView->setShowColocation (_show, true);
	}
	else if (sender() == pbtShowColocation_Cell) {
		treeView->setShowColocation (_show, false);
	}
}

//SLOT:
void TTTTracking::showSpeed (bool _show)
{
	if (! TTTManager::getInst().getTree())
		return;
	
	ShowSpeed = _show;
	
	treeView->setShowSpeed (_show);
}

//SLOT:
void TTTTracking::handleCellDoubleClick (Track *_track)
{
	//selectTrack (_track);
	TTTManager::getInst().setCurrentTrack (_track);		//might be 0!
	
	displayTrackInformation();
	startCellTracking (true);
}

//SLOT:
void TTTTracking::saveCurrentFile (int _saveas)
{
	//stop tracking if still in progress
	if (TTTManager::getInst().getBasePositionManager())
		TTTManager::getInst().getBasePositionManager()->frmMovie->stopTracking();
	
	//note: saving can be aborted, if this was a new file => DataChanged depends on it
	
	//if (sender() == pbtSaveTTTFileDifferentName)

	bool successFull = false;
	if (_saveas == 1)
		//save with different name
		successFull = saveTTTFile (true);
	else
		successFull = saveTTTFile();

	if(successFull) {
		DataChanged = false;
		//statusBar->showMessage("File saved successfully.", 2000);
		displayStatusBarMessage("Tree saved successfully.", 3000);
	}
	else {
		DataChanged = true;
	}
}

//SLOT:
void TTTTracking::deleteAllTrackPoints()
{
	if (! TTTManager::getInst().getCurrentTrack())
		return;
	
	//if this track has children, the track points cannot be deleted
	if (TTTManager::getInst().getCurrentTrack()->getChildTrack (1) || TTTManager::getInst().getCurrentTrack()->getChildTrack (2))
		return;
	
	TTTManager::getInst().getCurrentTrack()->deleteMarks();
	
	DataChanged = true;
	
	if (DataChanged) {
		treeView->draw (TTTManager::getInst().getCurrentTrack());
		displayTrackInformation();
		refreshStatistics();
		updatePositionHistory();
	}
}

//SLOT:
void TTTTracking::deleteStopReason()
{
	if (! TTTManager::getInst().getCurrentTrack())
		return;
	
	bool removeChildren = false;
	
	if (TTTManager::getInst().getCurrentTrack()->getStopReason() == TS_DIVISION)
		//the (unedited) children have to be removed, if removing the stop reason was succesful
		removeChildren = true;
	
	DataChanged = TTTManager::getInst().getCurrentTrack()->deleteStopReason();
	
	if (DataChanged && removeChildren) {
		treeView->deleteTrack (TTTManager::getInst().getCurrentTrack()->getChildTrack (1));
		treeView->deleteTrack (TTTManager::getInst().getCurrentTrack()->getChildTrack (2), true);
	}
	
	if (DataChanged) {
		//problem: 	if only the current track is redrawn, the old stop reason symbol is still visible
		//solution:	all tracks are redrawn
		
		treeView->draw (TTTManager::getInst().getCurrentTrack());
		displayTrackInformation();
		refreshStatistics();
	}
}

//SLOT:
void TTTTracking::setColocationRadius (int _radius)
{
	lblCoLocationRadius->setText (QString ("current: %1 (pixel)").arg (_radius));
	
	if (treeView) {
		treeView->setColocationRadius (_radius);
		
		if (ShowColocation) {
			if (TTTManager::getInst().getCurrentTrack()) {
				treeView->draw (TTTManager::getInst().getCurrentTrack(), true, ShowSpeed, true, false);
			}
			else
				treeView->draw();
		}
	}

//@todo
//	if (TTTManager::getInst().getCurrentPositionManager()->frmMovie)
//		//display a circle around active cell in frmMovie to indicate the radius
//		TTTManager::getInst().getCurrentPositionManager()->frmMovie->PictureFrames.find (TTTManager::getInst().getCurrentPositionManager()->getWavelength())->showColocationRadius (_radius);
	
	
}

////SLOT:
//bool TTTTracking::addCellProperties (Track *_track, int _timePoint, const CellProperties& _props)
//{
//	// OH 25/05/11: Properties now set in TTTMovie
//	
//	//receive the current properties
//	CellProperties props = TTTManager::getInst().frmCellProperties->getProperties();
//	props.EndoMitosis = _props.EndoMitosis;
//
//	if (_track)
//		return _track->setProperties (_timePoint, props, true);
//	else
//		return false;
//}

//SLOT:
void TTTTracking::selectWaveLength (int _waveLength)
{
	if (! sender())
		return;
	
	//get button for testing its status (toggled down or not)
	QObject *tmp = (QObject *)sender();
	QObject *child = tmp->child (QString ("wlbutton_%1").arg (_waveLength));
	if (! child)
		return;
	
	QPushButton *but = (QPushButton*)(child);
	
	if (! but)
		return;
	
	bool on = but->isOn();
	
/*	int wavelength = -1;
	if (sender() == pbtSelectWaveLength0)
		wavelength = 0;
	else if (sender() == pbtSelectWaveLength1)
		wavelength = 1;
	else if (sender() == pbtSelectWaveLength2)
		wavelength = 2;
	else if (sender() == pbtSelectWaveLength3)
		wavelength = 3;
	else if (sender() == pbtSelectWaveLength4)
		wavelength = 4;
	else if (sender() == pbtSelectWaveLength5)
		wavelength = 5;*/
	
        if ((_waveLength > -1) & (_waveLength <= MAX_WAVE_LENGTH)) {
		if (on)
			WaveLengthSelected [_waveLength] |= 128;		//set first bit
		else
			WaveLengthSelected [_waveLength] &= 127;		//delete first bit
	}
	
	setLoadPictures (-1, -1);
}

//SLOT:
void TTTTracking::showComment (QString _comment, QPoint _pos, int _timePoint)
{
	//add the timepoint to the comment text
	pbtComment->setText (QString ("Timepoint %1\n").arg (_timePoint) + _comment);
	
	fraComment->setGeometry (	fraCellTree->geometry().left() + _pos.x(), 
								fraCellTree->geometry().top() + _pos.y(), 
								fraComment->width(), 
								fraComment->height());
	
	//the frame must be inside the window bounds
	// (can only overlap to the right or at the bottom)
	if (! this->geometry().contains (fraComment->geometry())) {
		int left = fraCellTree->geometry().left() + _pos.x();
		int top = fraCellTree->geometry().top() + _pos.y();
		if (_pos.x() + fraComment->width() > fraCellTree->geometry().left() + fraCellTree->width())
			left = fraCellTree->geometry().left() + fraCellTree->width() - fraComment->width();
		if (_pos.y() + fraComment->height() > fraCellTree->geometry().top() + fraCellTree->height())
			top = fraCellTree->geometry().top() + fraCellTree->height() - fraComment->height();
		fraComment->setGeometry (	left,
									top,
									fraComment->width(), 
									fraComment->height());									
	}
	
	fraComment->show();
	fraComment->raise();
}

//SLOT:
void TTTTracking::selectCoLocationRadius()
{
	if (! TTTManager::getInst().getCurrentPositionManager()->frmMovie)
		return;
	
	if (TTTManager::getInst().getCurrentPositionManager()->frmMovie->isHidden()) 
		TTTManager::getInst().getCurrentPositionManager()->frmMovie->show();
	TTTManager::getInst().getCurrentPositionManager()->frmMovie->raise();
	
	TTTManager::getInst().getCurrentPositionManager()->frmMovie->startRadiusSelection();
}

//SLOT:
void TTTTracking::displayRadius (int _radius)
{
	scrColocationRadius->setValue (_radius);
}

//SLOT:
void TTTTracking::exportFluorescenceCells()
{
	if (DataChanged)
		saveCurrentFile (0);
	
	QString filename = "";
	
	QString matrixFilename = "";
	int wl = UserInfo::getInst().getStyleSheet().getExportWavelength();
	QString wavelength = QString ("_w%1").arg (wl);
	QString basename = TTTManager::getInst().getBasePositionManager()->getBasename();
	
	QString cellExportFolder = TTTManager::getInst().getNASDrive() + CELL_EXPORT_FOLDER + "/";
	QString us = UserInfo::getInst().getUsersign();
	QString basename_without_pos = basename.left (basename.findRev (POSITION_MARKER));
	cellExportFolder += us + "/" + basename_without_pos + "/" + basename;
	SystemInfo::checkNcreateDirectory (cellExportFolder, true, true);
	
	filename = currentFilename.mid (currentFilename.findRev ('/') + 1);			//extract pure filename
	filename += "export";
		
	//read antishading matrix file (is in the same folder as the ttt files)
	//matrixFilename = TTTManager::getInst().getNASDrive() + "TTTfiles/" + basename + "/" + basename + wavelength + "." + MATRIX_SUFFIX;
	//matrixFilename = SystemInfo::getTTTFileFolderFromBaseName (TTTManager::getInst().getBasePositionManager()->getBasename(), true);
	//NOTE does not look for the alternative ttt file folder
	matrixFilename = TTTManager::getInst().getBasePositionManager()->getTTTFileDirectory();
	matrixFilename += basename + wavelength + "." + MATRIX_SUFFIX;
	
	//create subdirectory for current experiment
	
	filename = cellExportFolder + "/" + filename + ".csv";
	
	if (! filename.isEmpty()) {
		//if an intensity correction matrix exists, it is used for the calculation
		//if not, it makes no harm
		exporter.exportFluorescence (filename, /*TTTManager::getInst().getBasePositionManager()->getPictures(),*/ TTTManager::getInst().getTree(), 1, wl, matrixFilename);
	}
}

//SLOT:
void TTTTracking::setDefaultSelection (bool _on)
{
	if (_on)
		//set all bits 1-7, bit 0 is untouched
		WaveLengthSelected [0] |= 127;
	else
		//leave only first bit, if set before, and delete bits 1-7
		WaveLengthSelected [0] &= 128;
}

//SLOT:
void TTTTracking::exportTree()
{
	//exports the current tree, three variants possible (current viewport, complete tree unstretched, complete tree stretched)
	
	if (! treeView)
		return;
	
	bool completeTree = false;
//	if (sender() == pbtExportCompleteTree || sender() == pbtExportCompleteTreeUnAltered)
        if (sender() == menuBar->actExport_TREE_COMPLETE_TREE || sender() == menuBar->actExport_TREE_COMPLETE_TREE_UNSTRETCHED)
		completeTree = true;
	
	bool unstretched = false;
//	if (sender() == pbtExportCompleteTreeUnAltered)
        if (sender() == menuBar->actExport_TREE_COMPLETE_TREE_UNSTRETCHED)
		unstretched = true;
	
	QString filename = "";
	
	//create subdirectory for current experiment
	QString folder = TTTManager::getInst().getNASDrive() + TREE_EXPORT_FOLDER + "/";
	QString us = UserInfo::getInst().getUsersign();
	QString basename = TTTManager::getInst().getBasePositionManager()->getBasename();
	QString basename_without_pos = basename.left (basename.findRev (POSITION_MARKER));
	folder += us + "/" + basename_without_pos + "/" + basename;
	SystemInfo::checkNcreateDirectory (folder, true, true);
	
	//filename = nas.absPath() + "/" + TTTManager::getInst().getBasePositionManager()->getBasename() + "_" + QDateTime::currentDateTime().toString ("yyyyMMdd_hhmmss");
	filename = folder + "/" + CurrentColonyName.left (CurrentColonyName.length() - 4) + "_" + QDateTime::currentDateTime().toString ("yyyyMMdd_hhmmss");
	
	
	QString index = CurrentColonyName.left (CurrentColonyName.length() - 4);
	if (index.isEmpty())
		index = TTTManager::getInst().getBasePositionManager()->getBasename() + "(new colony)";
	
	filename += "-tree.bmp";
	treeView->exportTree (filename, completeTree, unstretched, true, index);
	
	//QMessageBox::information (this, "Saving storage space", "Please make sure to delete your exported trees after (ab)using them\nbecause they consume quite a vast amount of storage on the net.", "Surely I will...");
	Tools::displayMessageBoxWithOpenFolder("Tree export completed.. Please make sure to delete your exported trees after (ab)using them.", "tTt", filename, true);
}

//SLOT:
void TTTTracking::setLoadTimePoint()
{
	if (! sender())
		return;
	
	if (sender() == lieLoadTimePointStart)
		StartLoading = (lieLoadTimePointStart->text()).toInt();
	else if (sender() == lieLoadTimePointEnd)
		EndLoading = (lieLoadTimePointEnd->text()).toInt();
	
	//check if the values contain a valid range
	if ((StartLoading < 0) | (StartLoading > TTTManager::getInst().getCurrentPositionManager()->getLastTimePoint()))
		StartLoading = TTTManager::getInst().getCurrentPositionManager()->getFirstTimePoint();
	if ((EndLoading < 0) | (EndLoading > TTTManager::getInst().getCurrentPositionManager()->getLastTimePoint()))
		EndLoading = TTTManager::getInst().getCurrentPositionManager()->getLastTimePoint();
	if ((StartLoading > EndLoading) & (EndLoading > 0)) {	//swap values
		int tmp = StartLoading;
		StartLoading = EndLoading;
		EndLoading = tmp;
	}
	
	lieLoadTimePointStart->setText (QString ("").setNum (StartLoading));
	lieLoadTimePointEnd->setText (QString("").setNum (EndLoading));
	
    if ((StartLoading > -1) && (EndLoading > -1)) {
		timeScale->selectRegion (StartLoading, EndLoading);
		setLoadPictures (StartLoading, EndLoading);
	}
}

//SLOT:
void TTTTracking::setSpeedAmplification (int _amp)
{
	SpeedAmplifier = _amp;
	lblSpeedAmplifier->setText (QString ("current: %1").arg (_amp));
	treeView->setSpeedAmplification (_amp);

	if (TTTManager::getInst().getCurrentTrack() && ShowSpeed) {
		treeView->draw (TTTManager::getInst().getCurrentTrack(), true, true, false, false);
	}
}

//SLOT:
void TTTTracking::normalizeSpeed()
{
	return;
	
	//@todo
/*	if (tree->getMaxSpeed() > 0)
		scrSpeedAmplification->setValue (255 / tree->getMaxSpeed());*/
	
	drawTree();
}

//SLOT:
void TTTTracking::selectColony()
{
	//stop tracking if still in progress
	if (TTTManager::getInst().isTracking())
		if (TTTManager::getInst().getBasePositionManager())
			TTTManager::getInst().getBasePositionManager()->frmMovie->stopTracking();
	
/*	lboColony->show();
	lboColony->raise();*/
	
	
	
	QString sugdir = "";
	//if (TTTManager::getInst().getBasePositionManager())
	//	sugdir = TTTManager::getInst().getBasePositionManager()->getTTTFileDirectory();

	if (TTTPositionManager *ttpm = TTTManager::getInst().getCurrentPositionManager())
		sugdir = ttpm->getTTTFileDirectory(true);
	
	//QString filename = Q3FileDialog::getOpenFileName (sugdir, "Tree files (*.ttt)", this, 0, "Choose a tree file");
	QString filename = QFileDialog::getOpenFileName(sugdir, "Tree files (*.ttt)", this, 0, "Choose a tree file");
	
	if (filename.isEmpty())
		//user cancelled the action - nothing changed
		return;
	else
		setColony (filename);
}

//SLOT:
void TTTTracking::setColony (const QString& _filename)
{
	//note: it is assumed that the ttt-files provided in the combobox really exist
	//(the box is filled in frmMainWindow (when the directory is changed) and the combobox there is also filled)
	
//	lboColony->hide();
	
	if (_filename.isEmpty())
		return;
	
	if (SaveDialog()) {
		TTTManager::getInst().getTree()->reset();
		TTTManager::getInst().clearFields();

		// Clear changelog
		clearChangelog();
		
		//filename now already comes with an absolute path
		QString tmp = _filename;
		
/*		bool old_folder = false;
		if (_filename.left (1) == OLD_FOLDER_TTTFILE_MARKER) {
			//file is in old folder system
			old_folder = true;
			tmp = _filename.mid (1);
			tmp = TTTManager::getInst().getBasePositionManager()->getTTTFileDirectory (false, true) + tmp;
		}
		else {
			//file is in new folder system
			tmp = _filename;
			tmp = TTTManager::getInst().getBasePositionManager()->getTTTFileDirectory() + tmp;
		}*/
		
		
		
		CurrentColonyName = _filename;
		
		treeView->clear();
		treeView->update();
		
		if (! loadTTTFile (tmp)) {
			QMessageBox::information (this, "Bad news...", "Loading " + _filename + " failed.", QMessageBox::Ok);
			addColony();		//open empty colony
			CurrentColonyName = "";
		}
		else {
			// Enable load next / load prev buttons
			menuBar->actFile_OPEN_NEXT_TREE->setEnabled(true);
			menuBar->actFile_OPEN_PREV_TREE->setEnabled(true);

			// Enable go to first position button
			pbtStatusBarGoToFirstPos->setEnabled(true);
		}
		
		this->setFormCaption (tmp);
		
		DataChanged = false;
		
		treeView->draw();
		updateLegend();
		
		//cboColony->clearFocus();
//		pbtTrackCell->setFocus();
		
		TrackCount = TTTManager::getInst().getTree()->getTrackCount();
		
		TTTManager::getInst().setCurrentTrack (0);
		
		refreshStatistics();
		//updatePositionHistory();

	}
}

//SLOT:
void TTTTracking::handleColonyClick (int /*_button*/, Q3ListBoxItem *_item, const QPoint &)
{
	//load (left click) or delete (right click + security question) the selected colony
	
	if (! _item)
		return;
	
/*	switch (_button) {
		case 1:		//left button
			
			lboColony->hide();
			
			if (! _item->text().isEmpty())
				setColony (_item->text());
			break;
		
		case 2:		//right button
			
			if (! _item->text().isEmpty()) {
				//delete colony
				
				QString filename = _item->text();
				
				if (filename.left (1) == OLD_FOLDER_TTTFILE_MARKER) {
					filename = filename.mid (1);
					filename = TTTManager::getInst().getBasePositionManager()->getTTTFileDirectory (false, true) + filename;
				}
				else
					filename = TTTManager::getInst().getBasePositionManager()->getTTTFileDirectory() + filename;
				
				if (QFile::exists (filename)) {
				
					if (QMessageBox::question (this, "tTt question", "Do you really want to delete the file\n" + filename + "?", "Yes", "No",  QString::null, 1) == 0) {
						lboColony->takeItem (_item);
						QFile::remove (filename);
						
						lboColony->hide();
						addColony();
					}
					else
						lboColony->hide();
				}
			}
			
			break;
		default:
			;
	}*/
}

//SLOT:
void TTTTracking::showTreeMap()
{
	if (TTTManager::getInst().frmTreeMap) {
		TTTManager::getInst().frmTreeMap->show();
	}
}

//SLOT::
void TTTTracking::setLoadInterval (int _interval)
{
	LoadInterval = _interval;
}

//SLOT:
void TTTTracking::updateSelectionCounter (unsigned int, int, bool)
{
	if (! TTTManager::getInst().getCurrentPositionManager())
		return;
	
	if (! TTTManager::getInst().getCurrentPositionManager()->getPictures())
		return;
	
	SelectionCounter--;
	
	lblSelectedTimepoints->setText (QString ("%1 pictures selected").arg (TTTManager::getInst().getCurrentPositionManager()->getPictures()->countSelectedPictures()));
}

//SLOT:
void TTTTracking::updateTWHF (int)
{
	if (timeScale) {
		timeScale->update();
	}
}

//SLOT:
void TTTTracking::resetGraphics()
{
	//set zoom factor to 100% in timescale and tree
	spbZoomFactor->setValue (100);
	
	//set timescale to display first timepoint
	timeScale->setTopSecond (0);
	
	//set tree to display upper left corner
	treeView->setDisplayRegion (QRect (0, 0, fraCellTree->width(), fraCellTree->height()));
	
	if (TTTManager::getInst().frmTreeMap)
		TTTManager::getInst().frmTreeMap->setDisplayedRegion (treeView->getDisplayRegion());
	
	treeView->clear();
	treeView->draw();
	updateLegend();
	refreshStatistics();
	updatePositionHistory();
}

//SLOT:
void TTTTracking::adjustControlWidth (bool)
{
/*	if (sender() == cboColony) {
		//note: it is just the other way round as it should be with _gotFocus,
		//	 yet, it works!
		if (! _gotFocus) {
			fraCboColony->setGeometry (	fraCboColony->geometry().left(), 
										fraCboColony->geometry().top(),
										330, 
										fraCboColony->height());
			cboColony->setGeometry (	0, 0, 
										330, //fraCboColony->geometry().width(),
										fraCboColony->geometry().height());
		}
		else {
			fraCboColony->setGeometry (	fraCboColony->geometry().left(), 
										fraCboColony->geometry().top(),
										130, 
										fraCboColony->height());
			cboColony->setGeometry (	0, 0, 
										130, //fraCboColony->geometry().width(),
										fraCboColony->geometry().height());
		}
	}*/
}

//SLOT:
void TTTTracking::createBackup()
{
	
	if (! TTTManager::getInst().getBasePositionManager())
		return;
	
	if (! TTTManager::getInst().getTree())
		return;
	
	QString filename = SystemInfo::homeDirectory() + BACKUP_FILENAME;
	
	TTTFileHandler::saveFile (filename, TTTManager::getInst().getTree(), TTTManager::getInst().getBasePositionManager()->getFirstTimePoint(), TTTManager::getInst().getBasePositionManager()->getLastTimePoint());
}

//SLOT:
void TTTTracking::centerCell()
{
	
	if (! TTTManager::getInst().getCurrentTrack())
		return;
	
	//calculate offset
	QPoint cellPos = treeView->cellPosition (TTTManager::getInst().getCurrentTrack(), false).center();
	QPoint currentCenter = treeView->getDisplayRegion().center();
	
	QPoint shift = cellPos - currentCenter;
	
	//shift display to desired center
	treeView->shiftDisplay (shift);
}

//SLOT:
void TTTTracking::watchRAM()
{
	if (TTTManager::getInst().getCurrentPositionManager()->frmMovie) {
/*		if (pbtContinueLoading->isOn())
			pbtContinueLoading->toggle();*/
		//menuBar->setItemChecked (FILE_LOADING_PICTURES_LOAD, false);
		//menuBar->setItemChecked (FILE_LOADING_PICTURES_UNLOAD, false);

		//menuBar->actFile_LOADING_PICTURES_LOAD->setChecked(false);
		//menuBar->actFile_LOADING_PICTURES_UNLOAD->setChecked(false);
		
		//TTTManager::getInst().getCurrentPositionManager()->getPictures()->stopLoading();
		
		QMessageBox::information (this, "RAM full", 
						"Loading stopped because of full RAM.\nPlease unload some pictures, as the swap should be free.", 
						QMessageBox::Ok);
	}
}

//SLOT:
void TTTTracking::setCellProperties()
{
	if (! TTTManager::getInst().frmCellProperties)
		return;
	if (! TTTManager::getInst().getCurrentTrack())
		return;
	
	int firstTP = lieCellPropsFromTP->text().toInt();
	int lastTP = lieCellPropsToTP->text().toInt();
	
	CellProperties props = TTTManager::getInst().frmCellProperties->getProperties();
	
	setProperties (TTTManager::getInst().getCurrentTrack(), firstTP, lastTP, props, chkSetChildren->isChecked());
	
	DataChanged = true;
	
	treeView->draw (TTTManager::getInst().getCurrentTrack(), true, false, false, chkSetChildren->isChecked());
	
	refreshStatistics();
}

//SLOT:
void TTTTracking::showCellProperties()
{
	if (! TTTManager::getInst().frmCellProperties)
		return;
	if (! TTTManager::getInst().getCurrentTrack())
		return;
	
	int firstTP = lieCellPropsFromTP->text().toInt();
	
	TTTManager::getInst().frmCellProperties->showFurtherOptions (true);		//display adherence buttons
	
	if (TTTManager::getInst().getCurrentTrack()->aliveAtTimePoint (TTTManager::getInst().getTimepoint()))
		TTTManager::getInst().frmCellProperties->setProperties (TTTManager::getInst().getCurrentTrack()->getTrackPoint (TTTManager::getInst().getTimepoint(), false).getPropObject());
	else
		TTTManager::getInst().frmCellProperties->setProperties (TTTManager::getInst().getCurrentTrack()->getTrackPoint (firstTP, false).getPropObject());
	
	TTTManager::getInst().frmCellProperties->show(); // (this, 0, true);
	TTTManager::getInst().frmCellProperties->raise();
	// Make sure window is on top and visible
	TTTManager::getInst().frmCellProperties->activateWindow();
	if(TTTManager::getInst().frmCellProperties->isMinimized())
		TTTManager::getInst().frmCellProperties->showNormal();
}

//SLOT:
void TTTTracking::showGenerationTime (bool _show)
{
	if (! treeView)
		return;
	
	treeView->setShowGenerationTime (_show);
	treeView->draw();
}

//SLOT:
void TTTTracking::exportTreeData()
{
	if (! sender())
		return;
	
	if (! TTTManager::getInst().getTree())
		return;
	
        QVector<QString> data(0);
	QString filename;
	filename = TTTManager::getInst().getBasePositionManager()->getTTTFileDirectory();
	
        //default filename: for colony specific data, use colony name and addition to distinguish data type
	//		    otherwise, use only the base name
        if (sender() == menuBar->actExport_DATA_DIVISION_INTERVALS) {
		filename += CurrentColonyName.left (CurrentColonyName.length() - 4) + "_div_intervals";
	}
        else if (sender() == menuBar->actExport_DATA_FIRST_DIVISION_TIME) {
		//for complete position - thus colony name is obsolete
		filename += TTTManager::getInst().getBasePositionManager()->getBasename() + "_first_div_times";
	}
        else if (sender() == menuBar->actExport_DATA_APOPTOSIS_TIME) {
		filename += CurrentColonyName.left (CurrentColonyName.length() - 4) + "_apo_intervals";
	}
        else if (sender() == menuBar->actExport_DATA_CELL_SPEED) {
		filename += CurrentColonyName.left (CurrentColonyName.length() - 4) + "_cell_speeds";
	}
        else if (sender() == menuBar->actExport_DATA_ALL_SELECTED_CELLS) {
		filename += CurrentColonyName.left (CurrentColonyName.length() - 4) + "_selected_intervalls";
	}
        else if (sender() == menuBar->actExport_DATA_CELL_COUNT) {
		filename += CurrentColonyName.left (CurrentColonyName.length() - 4) + "_cell_count";
	}
	
	filename = Q3FileDialog::getSaveFileName (filename, "CSV files (*.csv)", this, 0, "Enter a filename");		
	
	if (filename.isEmpty())
		return;
	
	//export the desired data in a csv file, so that Excel/Spreadsheet can read it
	
	//each line that should be written into the file needs to be single string, eventually
	// all are combined in a QValueVector and handed over to ExportHandler::exportDataset()
	
	QString line;
	QString sep = ";";
	
        if (sender() == menuBar->actExport_DATA_DIVISION_INTERVALS) {
		//export the division time (seconds between each cell division)
		//format: (cell number);(starting time);(end time);(difference seconds);(difference minutes);(difference hours)
		
		line = "Cell number;Starting time;End time;Lifetime (seconds);Lifetime (minutes);Lifetime (hours)";
		data.push_back(line);
		
		Q3IntDict<Track> tmpTracks = TTTManager::getInst().getTree()->getAllTracks(-1);
		for (uint i = 0; i < tmpTracks.size(); i++) {
			Track *tmpTrack = tmpTracks.find (i);
			if (tmpTrack) {
				if (tmpTrack->getStopReason() == TS_DIVISION) {
					line = QString().setNum (tmpTrack->getNumber());
					line += sep;
					int startTime = tmpTrack->getFirstTrace();
					int endTime = tmpTrack->getLastTrace();
					QDateTime time = TTTManager::getInst().getBasePositionManager()->getFiles()->getRealTime (startTime, 1);
					line += time.toString("yyyy/MM/dd hh:mm:ss");
					line += sep;
					time = TTTManager::getInst().getBasePositionManager()->getFiles()->getRealTime (endTime, 1); 
					line += time.toString("yyyy/MM/dd hh:mm:ss");
					line += sep;
					int secs = TTTManager::getInst().getBasePositionManager()->getFiles()->calcSeconds (startTime, endTime);
					line += QString("%1").arg (secs);
					line += sep;
					//line += QString("%1m:%2s").arg (secs / 60, 2).arg (secs % 60, 2);
					line += QString ("%1").arg ((float)secs / 60.0);
					line += sep;
					//line += QString("%1h:%2m:%3s").arg (secs / 3600, 2).arg ((secs % 3600) / 60, 2).arg ((secs % 3600) % 60, 2);
					line += QString ("%1").arg ((float)secs / 3600.0);
					
					data.push_back (line);
				}
			}
		}
	}
        else if (sender() == menuBar->actExport_DATA_FIRST_DIVISION_TIME) {
		//export the time until the first division of the base cell
		//note: exploits all colonies within the current position, not only the current one
		//format: (colony number);(starting time);(cell starting time);(difference1 seconds);(difference1 minutes);(difference1 hours);(cell division time);(difference2 seconds);(difference2 minutes);(difference2 hours)
		
		line = "Colony number;Experiment starting time;Cell starting time;Difference (seconds);Difference (minutes);Difference (hours);Cell division time;Difference (seconds);Difference (minutes);Difference (hours)";
		data.push_back(line);
		
                //NOTE: does not regard the user setting for the tttfile folder (old or new)
                QVector<QString> fileArray = SystemInfo::listFiles (TTTManager::getInst().getBasePositionManager()->getTTTFileDirectory(), QDir::Files | QDir::Dirs, "*" + TTT_FILE_SUFFIX);
		
		Tree *tmpTree = new Tree();
		int numTracks = 0;
		int firstTP = 0;
		int lastTP = 0;
		QString index = "";
		TTTPositionManager *tttpm = 0;
		
		//for all tree files in the directory: read them and save values into data
		for (Q3ValueVector<QString>::Iterator iter = fileArray.begin(); iter != fileArray.end(); ++iter) {
			if (! iter->isEmpty()) {
				QString filename = (*iter);
				index = Tools::getPositionIndex (*iter);
				tttpm = TTTManager::getInst().getPositionManager (index);
				if (! tttpm)
					continue;	//no use trying
				
                                if (TTTFileHandler::readFile (filename, tmpTree, numTracks, firstTP, lastTP, &tttpm->positionInformation) == TTT_READING_SUCCESS) {
					Track *tmpTrack = tmpTree->getBaseTrack();
					if (tmpTrack) {
						firstTP = TTTManager::getInst().getBasePositionManager()->getFirstTimePoint();
						line = filename.right (7).left (3);		//get colony number
						line += sep;
						int startTime = tmpTrack->getFirstTrace();
						int endTime = tmpTrack->getLastTrace();
						QDateTime time = TTTManager::getInst().getBasePositionManager()->getFiles()->getRealTime (firstTP, 1);
						line += time.toString("yyyy/MM/dd hh:mm:ss");
						line += sep;
						
						time = TTTManager::getInst().getBasePositionManager()->getFiles()->getRealTime (startTime, 1);
						line += time.toString("yyyy/MM/dd hh:mm:ss");
						line += sep;
						int secs = TTTManager::getInst().getBasePositionManager()->getFiles()->calcSeconds (firstTP, startTime);
						line += QString("%1").arg (secs);
						line += sep;
						//line += QString("%1m:%2s").arg (secs / 60, 2).arg (secs % 60, 2);
						line += QString ("%1").arg ((float)secs / 60.0);
						line += sep;
						//line += QString("%1:%2:%3").arg (secs / 3600, 2).arg ((secs % 3600) / 60, 2).arg ((secs % 3600) % 60, 2);
						line += QString ("%1").arg ((float)secs / 3600.0);
						line += sep;
						
						time = TTTManager::getInst().getBasePositionManager()->getFiles()->getRealTime (endTime, 1);
						line += time.toString("yyyy/MM/dd hh:mm:ss");
						line += sep;
						secs = TTTManager::getInst().getBasePositionManager()->getFiles()->calcSeconds (startTime, endTime);
						line += QString("%1").arg (secs);
						line += sep;
						//line += QString("%1m:%2s").arg (secs / 60, 2).arg (secs % 60, 2);
						line += QString ("%1").arg ((float)secs / 60.0);
						line += sep;
						//line += QString("%1h:%2m:%3s").arg (secs / 3600, 2).arg ((secs % 3600) / 60, 2).arg ((secs % 3600) % 60, 2);
						line += QString ("%1").arg ((float)secs / 3600.0);
						
						data.push_back (line);
					}
				}
			}
		}
	}
        else if (sender() == menuBar->actExport_DATA_APOPTOSIS_TIME) {
		//export the time between genesis and apoptosis for each apoptotic cell
		//format: (cell number);(starting time);(apoptosis time);(difference seconds);(difference minutes);(difference hours)
		
		line = "Cell number;Starting time;Apoptosis time;Lifetime (seconds);Lifetime (minutes);Lifetime (hours)";
		data.push_back(line);
		
		Q3IntDict<Track> tmpTracks = TTTManager::getInst().getTree()->getAllTracks(0);
		for (uint i = 0; i < tmpTracks.size(); i++) {
			Track *tmpTrack = tmpTracks.find (i);
			if (tmpTrack) {
				if (tmpTrack->getStopReason() == TS_APOPTOSIS) {
					line = QString().setNum (tmpTrack->getNumber());
					line += sep;
					int startTime = tmpTrack->getFirstTrace();
					int endTime = tmpTrack->getLastTrace();
					QDateTime time = TTTManager::getInst().getBasePositionManager()->getFiles()->getRealTime (startTime);
					line += time.toString("yyyy/MM/dd hh:mm:ss");
					line += sep;
					time = TTTManager::getInst().getBasePositionManager()->getFiles()->getRealTime (endTime, 1); 
					line += time.toString("yyyy/MM/dd hh:mm:ss");
					line += sep;
					int secs = TTTManager::getInst().getBasePositionManager()->getFiles()->calcSeconds (startTime, endTime);
					line += QString("%1").arg (secs);
					line += sep;
					//line += QString("%1m:%2s").arg (secs / 60, 2).arg (secs % 60, 2);
					line += QString ("%1").arg ((float)secs / 60.0);
					line += sep;
					//line += QString("%1h:%2m:%3s").arg (secs / 3600, 2).arg ((secs % 3600) / 60, 2).arg ((secs % 3600) % 60, 2);
					line += QString ("%1").arg ((float)secs / 3600.0);
					
					data.push_back (line);
				}
			}
		}		
	}
        else if (sender() == menuBar->actExport_DATA_CELL_SPEED) {
		//note the column headers where there needs to be distinguished between micrometers and pixels
		if (TTTManager::getInst().USE_NEW_POSITIONS())
			line = "Cell number;Starting time;End time;Timepoint interval (seconds);Absolute time (seconds);Covered Distance (um);Average speed (um/sec)";
		else
			line = "Cell number;Starting time;End time;Timepoint interval (seconds);Absolute time (seconds);Covered Distance (pixel);Average speed (pix/sec)";
		
		data.push_back (line);
		
		Q3IntDict<Track> tmpTracks = TTTManager::getInst().getTree()->getAllTracks(-1);
		TrackPoint t1, t2;
		int elapsedTime = 0;
		int pixelDistance = 0;
		float speed = 0.0f;
		for (uint i = 0; i < tmpTracks.size(); i++) {
			Track *tmpTrack = tmpTracks.find (i);
			if (tmpTrack) {
				if (tmpTrack->tracked()) {
					int completeDistance = 0;
					t1 = tmpTrack->getTrackPointByNumber (1); 
					int startTimepoint = tmpTrack->getFirstTrace();
					int endTimepoint = tmpTrack->getLastTrace();
					int lifeTime = TTTManager::getInst().getBasePositionManager()->getFiles()->calcSeconds (startTimepoint, endTimepoint);
					QDateTime startTime = TTTManager::getInst().getBasePositionManager()->getFiles()->getRealTime (startTimepoint, 1);
					QDateTime endTime = TTTManager::getInst().getBasePositionManager()->getFiles()->getRealTime (endTimepoint, 1); 
					int absoluteTime = 0;
					
					for (int tpc = 1; tpc < tmpTrack->getMarkCount(); tpc++) {
						t2 = tmpTrack->getTrackPointByNumber (tpc + 1); 
						
						elapsedTime = TTTManager::getInst().getBasePositionManager()->getFiles()->calcSeconds (t1.TimePoint, t2.TimePoint);
						absoluteTime += elapsedTime;
						
						pixelDistance = (int)t1.distanceTo (t2);
						completeDistance += pixelDistance;
						
						if (elapsedTime > 0)
							speed = (float)pixelDistance / (float)elapsedTime;
						
						line = QString().setNum (tmpTrack->getNumber());
						line += sep;
						line += ""; //startTime.toString("yyyy/MM/dd hh:mm:ss");
						line += sep;
						line += ""; //endTime.toString("yyyy/MM/dd hh:mm:ss");
						line += sep;
						line += QString("%1").arg (elapsedTime);
						line += sep;
						line += QString("%1").arg (absoluteTime);
						line += sep;
						line += QString("%1").arg (pixelDistance);
						line += sep;
						line += QString("%1").arg (speed, 0, 'f', 3);
						
						data.push_back (line);
						
						t1 = t2;
					}
					
					//summary line
					line = QString("%1 (sum)").arg (tmpTrack->getNumber());
					line += sep;
					line += startTime.toString("yyyy/MM/dd hh:mm:ss");
					line += sep;
					line += endTime.toString("yyyy/MM/dd hh:mm:ss");
					line += sep;
					line += QString("%1").arg (lifeTime);
					line += sep;
					line += ""; //absolute time == life time
					line += sep;
					line += QString("%1").arg (completeDistance);
					line += sep;
					if (lifeTime > 0)
						speed = (float)completeDistance / (float)lifeTime;
					line += QString("%1").arg (speed, 0, 'f', 3);
					
					data.push_back (line);
				}
			}
		}
		
	}
        else if (sender() == menuBar->actExport_DATA_ALL_SELECTED_CELLS) {
		//export the time between genesis and fate for each selected cell
		//format: (cell number);(starting time);(stopping time);(difference seconds);(difference minutes);(difference hours)
		
		line = "Cell number;Starting time;Stopping time;Lifetime (seconds);Lifetime (minutes);Lifetime (hours)";
		data.push_back (line);
		
		Q3IntDict<Track> tmpTracks = treeView->getSelectedTracks (true);
		for (uint i = 0; i < tmpTracks.size(); i++) {
			Track *tmpTrack = tmpTracks.find (i);
			if (tmpTrack) {
				line = QString().setNum (tmpTrack->getNumber());
				line += sep;
				int startTime = tmpTrack->getFirstTrace();
				int endTime = tmpTrack->getLastTrace();
				QDateTime time = TTTManager::getInst().getBasePositionManager()->getFiles()->getRealTime (startTime, 1);
				line += time.toString("yyyy/MM/dd hh:mm:ss");
				line += sep;
				time = TTTManager::getInst().getBasePositionManager()->getFiles()->getRealTime (endTime, 1); 
				line += time.toString("yyyy/MM/dd hh:mm:ss");
				line += sep;
				int secs = TTTManager::getInst().getBasePositionManager()->getFiles()->calcSeconds (startTime, endTime);
				line += QString("%1").arg (secs);
				line += sep;
				//line += QString("%1m:%2s").arg (secs / 60, 2).arg (secs % 60, 2);
				line += QString ("%1").arg ((float)secs / 60.0);
				line += sep;
				//line += QString("%1h:%2m:%3s").arg (secs / 3600, 2).arg ((secs % 3600) / 60, 2).arg ((secs % 3600) % 60, 2);
				line += QString ("%1").arg ((float)secs / 3600.0);
				
				data.push_back (line);
			}
		}		
	}
        else if (sender() == menuBar->actExport_DATA_CELL_COUNT) {
		//export the cell count over the experiment time, but regard only selected cells
		//format: (timepoint);(real time);(time in seconds);(tp difference seconds);(cell count)
		
		line = "Timepoint;Real time;Time in seconds;Timepoint difference (seconds);Cell count";
		data.push_back (line);
		
		Q3IntDict<Track> selectedTracks = treeView->getSelectedTracks (false);
		
		long oldseconds = 0;
		for (int tp = TTTManager::getInst().getBasePositionManager()->getFirstTimePoint(); tp <= TTTManager::getInst().getBasePositionManager()->getLastTimePoint(); tp++) {
			int cellCount = 0;
			
			if (! TTTManager::getInst().getBasePositionManager()->getFiles()->exists (tp, 1, -1))
				continue;
			
			Q3IntDict<Track> *timepointTracks = TTTManager::getInst().getTree()->timePointTracks (tp);
			
			if (timepointTracks) {
				if (timepointTracks->size() > 0) {
					//count those tracks at the timepoint tp that are also selected
					for (uint i = 0; i < timepointTracks->size(); i++) {
						if (timepointTracks->find (i)) {
							//check if track is selected - if not, do not count it
							
							int number = timepointTracks->find (i)->getNumber();
							
							for (uint j = 0; j < selectedTracks.size(); j++) {
								if (selectedTracks.find (j))
									if (selectedTracks.find (j)->getNumber() == number) {
										cellCount++;
										break;
									}
							}
						}
					}
					
					//write data
					
					line = QString ("%1").arg (tp);
					line += sep;
					
					QDateTime time = TTTManager::getInst().getBasePositionManager()->getFiles()->getRealTime (tp, 1);
					line += time.toString("yyyy/MM/dd hh:mm:ss");
					line += sep;
					
					long realseconds = TTTManager::getInst().getBasePositionManager()->getFiles()->calcSeconds (TTTManager::getInst().getBasePositionManager()->getFirstTimePoint(), tp);
					line += QString ("%1").arg (realseconds);
					line += sep;
					
					long diff = realseconds - oldseconds;
					oldseconds = realseconds;
					line += QString ("%1").arg (diff);
					line += sep;
					
					line += QString ("%1").arg (cellCount);
					line += sep;
					
					data.push_back (line);
				}
			}
		}
	}
	
	
	//write data to file
	ExportHandler::exportData (filename, data);
}

////SLOT:
//void TTTTracking::customizeTTT()
//{
//        TTTManager::getInst().showCustomizeDialog();
//}

//SLOT:
void TTTTracking::markTreeFinished (bool _finished)
{
        if (TTTManager::getInst().getTree()) {
                TTTManager::getInst().getTree()->setFinished (_finished);
                treeView->draw();
        }
}

//SLOT:
void TTTTracking::applyStyleSheet (StyleSheet &)
{
	if (! treeView)
		return;
	
	//apply the style sheet to the current tree
	//note: the style sheet is already made global in the editor
	treeView->draw();
	treeView->displayTimePoint();
	
	timeScale->draw();
	updateLegend();
}

//SLOT:
void TTTTracking::selectCells()
{
	bool select = true;
	if (sender() == pbtDeselectCells)
		select = false;
	
	//iterate over all cells and select those (additionally to the currently selected) who fulfill the current selection criteria
	
	Track *tmpTrack = 0;
	Q3IntDict<Track> tracks = TTTManager::getInst().getTree()->getAllTracks (-1);
	for (uint i = 0; i < tracks.size(); i++) {
		tmpTrack = tracks.find (i);
		if (tmpTrack) {
			bool fulfillsCriteria = false;
			switch (currentCellType) {
				case CT_ALLTYPES:
					fulfillsCriteria = true;
					break;
				case CT_DIVIDINGTYPE:
					fulfillsCriteria = tmpTrack->getStopReason() == TS_DIVISION;
					break;
				case CT_APOPTOTICTYPE:
					fulfillsCriteria = tmpTrack->getStopReason() == TS_APOPTOSIS;
					break;
				case CT_LOSTTYPE:
					fulfillsCriteria = tmpTrack->getStopReason() == TS_LOST;
					break;
				case CT_INTERRUPTEDTYPE:
					fulfillsCriteria = tmpTrack->getStopReason() == TS_NONE;
					break;
				default:
					;
			}
			
			if (fulfillsCriteria) {
				treeView->selectTrack (tmpTrack, select, true, false);
			}
		}
	}
	
	treeView->update();
	
	QString text = "";
	text += QString ("%1 selected\n").arg (treeView->countSelectedTracks());
/*	tracks = TreeView->getSelectedTracks();
	int cc = 0;
	for (uint i = 0; i < tracks.size(); i++) {
		tmpTrack = tracks.find (i);
		if (tmpTrack) {
			cc++;
			text += QString ("%1").arg (tmpTrack->getNumber());
			if (cc % 3 == 0)
				text += "\n";
			else if (cc % 2 == 0)
				text += "\t";
			else
				text += "\t";
		}
	}*/
	
	lblCellSelectionCount->setText (text);
	
	//check
/*	tracks = TreeView->getSelectedTracks();
	for (uint i = 0; i < tracks.size(); i++) {
		tmpTrack = tracks.find (i);
		if (tmpTrack) {
			std::cout << tmpTrack->getNumber() << std::endl;
		}
	}
	std::cout << std::endl;*/
}

//SLOT:
void TTTTracking::setCellTypeSelection()
{
	if (! sender())
		return;
	
	if ((QRadioButton*)sender() == optSelectAllCells)
		currentCellType = CT_ALLTYPES;
	else if ((QRadioButton*)sender() == optSelectApoptoticCells)
		currentCellType = CT_APOPTOTICTYPE;
	else if ((QRadioButton*)sender() == optSelectDividingCells)
		currentCellType = CT_DIVIDINGTYPE;
	else if ((QRadioButton*)sender() == optSelectInterruptedCells)
		currentCellType = CT_INTERRUPTEDTYPE;
	else if ((QRadioButton*)sender() == optSelectLostCells)
		currentCellType = CT_LOSTTYPE;
	else
		currentCellType = CT_NOCELLTYPE;
}

//SLOT:
void TTTTracking::calculatePositionStatistics()
{
	//read all ttt files in current position
	
        //NOTE: does not regard the user setting for the tttfile folder (old or new)
        QVector<QString> fileArray = SystemInfo::listFiles (TTTManager::getInst().getCurrentPositionManager()->getTTTFileDirectory(), QDir::Files | QDir::Dirs, "*" + TTT_FILE_SUFFIX);
	
	Tree *tmpTree = new Tree();
	int numTracks = 0;
	int firstTP = 0;
	int lastTP = 0;
	QString index = "";
	TTTPositionManager *tttpm = 0;
	
	//for all tree files in the directory: read them and save values into data
	TreeStatistics completeStat;
	
	for (Q3ValueVector<QString>::Iterator iter = fileArray.begin(); iter != fileArray.end(); ++iter) {
		if (! iter->isEmpty()) {
			QString filename = (*iter);
			index = Tools::getPositionIndex (*iter);
			tttpm = TTTManager::getInst().getPositionManager (index);
			if (! tttpm)
				continue;	//no use trying
			
                        if (TTTFileHandler::readFile (filename, tmpTree, numTracks, firstTP, lastTP, &tttpm->positionInformation) == TTT_READING_SUCCESS) {
				
				TreeStatistics treestat = TreeStatistics::calculate (tmpTree);
				
				completeStat += treestat;
			}
		}
	}
	
	QString stats = "";
	
	//total cell count
	stats = QString ("Total cells = %1\n").arg (completeStat.getTotalCount());
	
	//dividing cells
	stats += QString ("Div. cells = %1\n").arg (completeStat.getDividingCount());
	
	//apoptotic cells
	stats += QString ("Apo. cells = %1\n").arg (completeStat.getApoptoticCount());
	
	//lost cells
	stats += QString ("Lost cells = %1\n").arg (completeStat.getLostCount());
	
	//interrupted cells
	stats += QString ("Int. cells = %1\n").arg (completeStat.getInterruptedCount());
	
	//generation stats
	for (int i = 0; i < completeStat.getGenerations(); i++) {
		stats += QString ("%1 (%2)").arg (i). arg (completeStat.getGenerationCounts (i));
		if (i % 2 == 0)
			stats += "\t";
		else
			stats += "\n";
	}
	
	lblPositionStatistics->setText (stats);
}

//SLOT:
void TTTTracking::setAllTreeElementsVisibility (bool _visible)
{
	//toggle all elements; but redraw tree only once
	Q3ListViewItemIterator iter (lvwCurrentTreeItems);
	
	if (lvwCurrentTreeItems->childCount() > 0) {
		for ( ; iter.current(); ++iter) {
			( ( Q3CheckListItem* )iter.current() )->setOn (_visible);
			
			char key = iter.current()->text (1).ascii() [0];
			
			TreeElementStyle tes = UserInfo::getInst().getStyleSheet().getAssociatedElement (key);
			
			tes.setVisible (_visible);
			
			UserInfo::getInst().getRefStyleSheet().setAssociatedElement (key, tes);
		}
	}
	
	treeView->draw();
	
}

//SLOT:
void TTTTracking::setTreeItemVisible (Q3ListViewItem *_item)
{
	if (! _item)
		return;
	
	//toggle visibility of current item
	char key = _item->text (1).ascii() [0];
	
	TreeElementStyle tes = UserInfo::getInst().getStyleSheet().getAssociatedElement (key);
	
	tes.setVisible (( ( Q3CheckListItem* )_item )->isOn());
	
	UserInfo::getInst().getRefStyleSheet().setAssociatedElement (key, tes);
	
	treeView->draw();
}

//SLOT:
void TTTTracking::loadLastAutosavedColony()
{
	QString filename = SystemInfo::homeDirectory() + BACKUP_FILENAME;
	
	if (SaveDialog()) {
		TTTManager::getInst().getTree()->reset();
		TTTManager::getInst().clearFields();
		
		CurrentColonyName = filename;
		
		treeView->clear();
		treeView->update();
		
		if (! loadTTTFile (filename)) {
			QMessageBox::information (this, "Bad news...", "Loading " + filename + " failed.", QMessageBox::Ok);
			addColony();		//open empty colony
			CurrentColonyName = "";
		}
		
		//the colony must be renamed by the user before saving again, otherwise it would be called "BACKUP.ttt" - which is not desired, I suppose...
		currentFilename = "";
		
		this->setFormCaption (filename);
		
		DataChanged = false;
		
		treeView->draw();
		updateLegend();
		
		//cboColony->clearFocus();
//		pbtTrackCell->setFocus();
		
		TrackCount = TTTManager::getInst().getTree()->getTrackCount();
		
		TTTManager::getInst().setCurrentTrack (0);
		
		refreshStatistics();
		//updatePositionHistory();
	}
	
}

//SLOT;
void TTTTracking::currentPositionChanged (int _oldPos, int _newPos)
{
	// Disable picture selection mode
	if(menuBar->actFile_LOADING_PICTURES_TOGGLE_SELECTION->isChecked()) {
		menuBar->actFile_LOADING_PICTURES_TOGGLE_SELECTION->setChecked(false);
		selectRegion(false);
	}

	// Update time scale
	updateTimeScale();

	// Update form caption
	setFormCaption();

	// Display in status bar
	if(_newPos > 0) 
		defaultMessage = QString("Position ") + Tools::convertIntPositionToString(_newPos);
	else
		defaultMessage = "";

	resetPermanentStatusbarMessage();
}

//SLOT:
void TTTTracking::convertWrongInvertedCoordinates()
{
	//agenda see also BDisplay::calcTransformedCoords()
	//  - determine the position in which the trackpoint was set
	//  - calculate transcoords for each trackpoint in old mode
	//  - calculate abscoords for each trackpoint in new mode (no change in program)
	
	if (! TTTManager::getInst().getTree())
		return;
	
	if (! TTTManager::getInst().getBasePositionManager())
		return;
	
	if (! TTTManager::getInst().getBasePositionManager()->getPictures())
		return;
	
	if (! TTTManager::getInst().coordinateSystemIsInverted())
		return;
		
	if (! TTTManager::getInst().USE_NEW_POSITIONS())
		return;
	
	if (QMessageBox::question (this, "tTt question", "Do you really want to correct the coordinates?\nIf they are already correct, or the conversion was done before, this could possibly mess up your tracks!", "Yes", "No") == 1)
		return;
	
	//save file...
	saveCurrentFile (0);
	
	//automatically backup old file
	
	//...and copy it to a backup location
	QString backupPath = SystemInfo::homeDirectory() + TTTFILES_FOLDER + "/" + TTTManager::getInst().getBasePositionManager()->getBasename();
	QString filename = currentFilename;
/*	QDir backup (backupPath);
	//create subdirectory for current experiment (if it not yet exists)
	bool folderexists = backup.cd (TTTManager::getInst().getBasePositionManager()->getBasename());
	if (! folderexists) {
		backup.mkdir (TTTManager::getInst().getBasePositionManager()->getBasename());
		backup.cd (TTTManager::getInst().getBasePositionManager()->getBasename());
	}*/
	backupPath += "/" + filename.mid (filename.findRev ('/') + 1) + "_invSysBackup";
	
	if (QFile::exists (backupPath)) {
		//if backup file already exists, assign a random number to avoid overwriting the correct files
		backupPath += QString ("%1").arg (rand());
	}
	
	TTTFileHandler::saveFile (backupPath, TTTManager::getInst().getTree(), TTTManager::getInst().getBasePositionManager()->getFirstTimePoint(), TTTManager::getInst().getBasePositionManager()->getLastTimePoint());
	
	
	
	//convert file
	//============
	
	float mmpp = TATInformation::getInst()->getWavelengthInfo (0).getMicrometerPerPixel();
	int width = (int)((float)TATInformation::getInst()->getWavelengthInfo (0).getWidth() * mmpp);
	int height = (int)((float)TATInformation::getInst()->getWavelengthInfo (0).getHeight() * mmpp);
	
	Q3IntDict<Track> tracks = TTTManager::getInst().getTree()->getAllTracks (-1);
	TTTPositionManager *tttpm = TTTManager::getInst().getBasePositionManager();
	TTTPositionManager *pmLeft = TTTManager::getInst().getPositionLeftOf (tttpm->positionInformation.getIndex());
	TTTPositionManager *pmTop = TTTManager::getInst().getPositionTopOf (tttpm->positionInformation.getIndex());
	
	bool stopped = false;
	bool first = true;
	
	for (uint i = 0; i < tracks.size(); i++) {
		if (! tracks [i])
			continue;
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		// 2014/02/24 OH: changed code due to change of getPositions() interface
		// (returns QHash<int, QPointF> instead of Q3ValueVector<QPair<int, QPointF> >)
		// NEW CODE NOT TESTED
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //Q3ValueVector<QPair<int, QPointF> > tps = tracks [i]->getPositions();
		QHash<int, QPointF> trackpoints = tracks [i]->getPositions();
		QList<int> timePoints = trackpoints.keys();

		for (int j = 0; j < timePoints.size(); j++) {
			//read absolute position from tree (still wrong)
			int tp = timePoints[j];
            QPointF pos = trackpoints[tp];
			
			if (pos.x() == -1)
				continue;
			
			//check (for the first trackpoint only) if it is already in the "allowed" range
			//if yes, then it can be assumed that the colony is already converted
			if (first & (pos.x() <= tttpm->positionInformation.getLeft()) &
			    (pos.y() <= tttpm->positionInformation.getTop())) {
			    	//colony is already correct => a conversion would be malicious...
			    	
				QMessageBox::warning (this, "Conversion stopped...", "The current colony is already correct. Conversion will stop.", "Ok");
				stopped = true;
				break;
			}
			
			//find out the position in which this trackpoint was set
			//======================================================
			//this is very important as the coordinate origin is needed
			
			bool left = false;
			bool top = false;
			bool right = false;
			bool bottom = false;
			
			if (pmLeft) {
				if ((pos.x() >= pmLeft->positionInformation.getLeft()) & 
				    (pos.x() - tttpm->positionInformation.getLeft() > width))
					left = true;
			}
			else {
				if (pos.x() < tttpm->positionInformation.getLeft())
					right = true;
			}
			
			if (pmTop) {
				if ((pos.y() >= pmTop->positionInformation.getTop()) & 
				    (pos.y() - tttpm->positionInformation.getTop() > height))
					top = true;
				
			}
			else {
				if (pos.y() < tttpm->positionInformation.getTop())
					bottom = true;
			}
			
			//_direction: 0 = topleft, 1 = top, 2 = topright, 3 = left, 4 = right, 5 = bottomleft, 6 = bottom, 7 = bottomright
			int dir = -1;
			if (left) {
				if (top)
					dir = 0;
				else if (bottom)
					dir = 5;
				else
					dir = 3;
			}
			else if (right) {
				if (top)
					dir = 2;
				else if (bottom)
					dir = 7;
				else
					dir = 4;
			}
			else {
				if (top)
					dir = 1;
				else if (bottom)
					dir = 6;
			}
			
			if (dir >= 0) {
				tttpm = TTTManager::getInst().getPositionSomewhereOf (tttpm->positionInformation.getIndex(), dir);
				
				if (! tttpm->getPictures()) {
					//at least one picture needs to be loaded of this position
					QMessageBox::warning (this, "Picture missing, conversion stopped...", "Please RELOAD THIS COLONY, load at least the first picture of wl 0 for position " + tttpm->positionInformation.getIndex() + " and redo the conversion.", "Ok");
					stopped = true;
					break;
				}
					
				pmLeft = TTTManager::getInst().getPositionLeftOf (tttpm->positionInformation.getIndex());
				pmTop = TTTManager::getInst().getPositionTopOf (tttpm->positionInformation.getIndex());
				
			}
			
			
			//translate coordinates from wrong to right
			//=========================================
			
                        pos = tttpm->getDisplays().at (0).calcTransformedCoords (pos, &tttpm->positionInformation, false, true);
			
			pos = tttpm->getDisplays().at (0).calcAbsCoords (pos, &tttpm->positionInformation);
			
			//int tp = tps [j].first;
			
			tracks [i]->setTPXY (tp, pos.x(), pos.y());
			
			first = false;
		}
		
		if (stopped)
			break;
	}
	
	if (! stopped)
		saveCurrentFile (0);
}

//SLOT:
void TTTTracking::selectTreeMinerFile()
{
	QString filename = Q3FileDialog::getOpenFileName (QString::null, "TreeMiner input file (*.tm)", this, "open file dialog", "Choose a treeminer file");
	
	if (! filename.isEmpty()) {
		lieTreeMinerFile->setText (filename);
	}
}

//SLOT:
void TTTTracking::loadTreeMinerFile()
{
	QString treeMinerFilename = lieTreeMinerFile->text();
	
	if (treeMinerFilename.isEmpty())
		return;
	
	
	MiningResults *m_res = TTTManager::getInst().getMiningResults();
	if (! m_res) {
		m_res = new MiningResults();
		TTTManager::getInst().setMiningResults (m_res);
	}
	m_res->setTMInputFile (treeMinerFilename);
	m_res->parseFiles();
	
	
	//paint general tree hierarchy
	//@ todo
	//fraGeneralTreeHierarchy
}

//SLOT:
void TTTTracking::overlayClusters()
{
	if (! TTTManager::getInst().getMiningResults())
		return;
	
	Tree *tree = TTTManager::getInst().getTree();
	if (! tree)
		return;
	
	//get input tree from the treeminer file which belongs to the current tree
	LittleTree *lt = TTTManager::getInst().getMiningResults()->findLittleTree (currentFilename);
	
	if (lt) {
		//assume the tree to be binary, which should be perfectly OK, as it is an original tTt tree
		//walk through little tree and determine cell number of each LittleCell by its root path
		
		Q3IntDict<LittleCell> cells = lt->getCellsSerial(); //getBaseCell();
		
		for (uint i = 0; i < cells.size(); i++) {
			LittleCell *l_cell = cells.find (i);
			if (l_cell) {
				Track *track = tree->getTrack (l_cell->number);
				if (track) {
					track->setClusterID (l_cell->cluster_index);
				}
			}
		}
		
		drawTree();
		
	}
}

//SLOT:
void TTTTracking::showFrequentTrees()
{
	// @todo
}

//SLOT:
void TTTTracking::readExternalTracks()
{
	
	//read tracks that were calculated by an external program (MATLAB)
	
	//format of Jan's output file:
	//cell nr;timepoint;distance;ix;iy;tries;intvalue;meanvalue
	//
	//explanations:
	//- distance = euclidic distance of the tTt tracking point and the MATLAB point
	//- ix,iy = coordinates of the MATLAB point
	//- tries = which next neighbor was assigned to this point
	//- intvalue = intensity added up
	//- meanvalue = average intensity
	//
	//Note that there can be more timepoints in that file than are actually tracked, as MATLAB interpolates missing trackpoints,
	// if the picture is present
	
	
	QString filename = Q3FileDialog::getOpenFileName (QString::null, "MATLAB output file (*.out)", this, "open file dialog", "Choose a file...");
	
	if (filename.isEmpty())
		return;
	
	QFile file (filename);
	if (! file.open (QIODevice::ReadOnly))
		return;
	
        QTextStream ts (&file);
	
	QString line;
	
	TYPE_externalTrackMap *ext_tracks = new TYPE_externalTrackMap();
	
	while (! ts.atEnd()) {
		line = ts.readLine();
		
		if (! (line.left (4) == "cell")) {		//skip header
			
			QStringList entries = QStringList::split (";", line);
			
			//read line; one for each (real or interpolated) trackpoint
			
			int cellNr = entries [0].toInt();
			int timepoint = entries [1].toInt();
			
			//TYPE_tp_to_mlr = (*ext_tracks) [timepoint];
			
			//float distance = entries [2].toDouble();
			float x_mat = entries [3].toDouble();
			float y_mat = entries [4].toDouble();
                        QPointF mat_coords (x_mat, y_mat);
			int tries = entries [5].toInt();
			float integral = entries [6].toDouble();
			float mean_value = entries [7].toDouble();
			
			//transform to micrometers
			TTTPositionManager *tttpm = TTTManager::getInst().getBasePositionManager();
                        mat_coords = tttpm->getDisplays().at (0).calcAbsCoords (mat_coords, &tttpm->positionInformation);
			
			MatlabRecord mlr;
			mlr.coords = mat_coords;
			mlr.tries = tries;
			mlr.integral = integral;
			mlr.mean_value = mean_value;
			
			(*ext_tracks) [timepoint] [cellNr] = mlr;
			
		}
	}
	
	TTTManager::getInst().setExternalTracks (ext_tracks);
	TTTManager::getInst().setShowExternalTracks (true);
	
}

//SLOT:
void TTTTracking::replaceTracksWithExternal()
{
	///@todo: also use interpolated trackpoints (a new Trackpoint must be created); currently neglected
	
	
	//replace all original coordinates with the ones currently written in the external tracks map
	//method:
	//walk through all tracks, then through all their trackpoints and replace the coordinates with the ones from the external file
	
	Tree *tree = TTTManager::getInst().getTree();
	if (! tree)
		return;
	TYPE_externalTrackMap *extTracks = TTTManager::getInst().getExternalTracks();
	if (! extTracks)
		return;
	
	//whether all tracks should be assumed (true) or just for the current timepoint, excluding the specified ones (false)
	bool allTracks = (sender() == pbtReplaceTracksWithExternal);
	
	if (allTracks) {
		
		if (QMessageBox::question (this, "tTt question", "Do you really want to assume all external tracks instead of your own?", "No", "Yes") == 1) {
			for (int nr = 1; nr <= tree->getMaxPossibleTracks(); nr++) {
				Track *track = tree->getTrack (nr);
				
				if (track) {
					for (int tp = track->getFirstTrace(); tp <= track->getLastTrace(); tp++) {
						TrackPoint *trackpoint = track->getRefTrackPoint (tp, false);
						
						if (trackpoint) {
							MatlabRecord mlr = (*extTracks) [tp] [nr];
							
							if (mlr.tries > -1) {
								//this is no track that failed to be assigned
								
								trackpoint->X = mlr.coords.x();
								trackpoint->Y = mlr.coords.y();
								
								//calculate radius from integral and mean - a neat trick :-)
								if (mlr.mean_value != 0.0) {
									float area = mlr.integral / mlr.mean_value;
									//area = pi * r * r => r = sqr(area / pi)
									float radius = sqrt (area / acos (-1.0));
									
                                                                        trackpoint->CellDiameter = radius * 2.0f;;
								}
							}
						}
					}
				}
			}
		}
	}
	else {
		int timepoint = TTTManager::getInst().getTimepoint();
		
		Q3IntDict<Track> *tracks = tree->timePointTracks (timepoint);
		
		if (tracks) {
			
			//the tracks to be excluded are in the following format:
			//cellnr;cellnr;...    example: 1;2;
			//usually filled automatically via mouseclick
			QString toExclude = lieExcludedMatlabTracks->text();
			
			QStringList excluded = QStringList::split (";", toExclude);
			
			for (uint i = 0; i < tracks->count(); i++) {
				
				Track *track = tracks->find (i);
				
				if (track) {
					int nr = track->getNumber();
					
					QString nrS = QString ("%1").arg (nr);
					
					//check if the track is not excluded
					if (excluded.find (nrS) != excluded.end())
						//track is specified to be excluded from the assumption
						continue;
					
					
					TrackPoint *trackpoint = track->getRefTrackPoint (timepoint, false);
					
					if (trackpoint) {
						MatlabRecord mlr = (*extTracks) [timepoint] [nr];
						
						if (mlr.tries > -1) {
							//this is no track that failed to be assigned
							
							trackpoint->X = mlr.coords.x();
							trackpoint->Y = mlr.coords.y();
							
							//calculate radius from integral and mean - a neat trick :-)
							if (mlr.mean_value != 0.0) {
								float area = mlr.integral / mlr.mean_value;
								//area = pi * r * r => r = sqr(area / pi)
								float radius = sqrt (area / acos (-1.0));
								
                                                                trackpoint->CellDiameter = radius * 2.0f;
							}
						}
					}
				}
			}
		}
		
		if (TTTManager::getInst().getBasePositionManager())
			if (TTTManager::getInst().getBasePositionManager()->frmMovie)
				TTTManager::getInst().getBasePositionManager()->frmMovie->ProceedPicture();

	}
	
	lieExcludedMatlabTracks->setText ("");
	
	
	///NOTE !!! DO   N O T   AUTOMATICALLY SAVE THE FILE !!!
	DataChanged = true;
}

//SLOT:
void TTTTracking::updatePositionHistory()
{
	//load history of the positions where the cells roamed, for the whole tree

	//note: distinguish between new files (17+), where this fact is already stored, and old ones, where it has to be calculated (indicate with a "?")

	txtPositionHistory->clear();

	//parse the current tree upwards, starting from the current cell up to the root cell
	//for each cell and each position change a new line is added with the following format:
	//<cell>:	(tp_a-tp_b)	position
	//note: as the cell numbers are descending, the list would be ordered descending as well, which is somehow confusing
	//      thus, for each track a temporary text with the described lines is created (as these lines are ascending) and 
	//       added to the list in reverse order

	if (TTTManager::getInst().getCurrentTrack()) {
		Track *track = TTTManager::getInst().getCurrentTrack();
		//TTTPositionManager *tttpm = 0;
		QString position = "", old_position = "";
		int timepoint = 0, old_timepoint = 1;
		QString tmpText = "";

		while (track) {

			// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			// 2014/02/24 OH: changed code due to change of getPositions() interface
			// (returns QHash<int, QPointF> instead of Q3ValueVector<QPair<int, QPointF> >)
			// NEW CODE NOT TESTED
			// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

			//for the current track:
			//for each timepoint: see if the position is already stored (file version 17+) or
			//			calculate it
			//Q3ValueVector<QPair<int, QPointF> > tps = track->getPositions();
			QHash<int, QPointF> trackPoints = track->getPositions();
			QList<int> timePoints = trackPoints.keys();
			std::sort(timePoints.begin(), timePoints.end());

			position = "";
			old_timepoint = track->getFirstTrace();
			tmpText = "";

			for (int j = 0; j < timePoints.size(); j++) {
				//read absolute position
				timepoint = timePoints [j];
				QPointF pos = trackPoints[timepoint];

				if (timepoint == -1)
					continue;

				//check if position is already set in trackpoint
				const TrackPoint *trackpoint = &(track->getTrackPoint (timepoint));
				//if ((trackpoint->Position > "") && (trackpoint->Position != "000"))
				if (trackpoint->getPositionNumber() > 0)
					position = trackpoint->Position;
				else {
					//find out the position
					/*					tttpm = TTTManager::getInst().getPositionAt (pos.x(), pos.y());
					if (tttpm) {
					position = tttpm->positionInformation.getIndex();
					}*/

					Q3IntDict<TTTPositionManager> tttpms = TTTManager::getInst().getAllPositionsAt (pos.x(), pos.y());
					position = "";
					for (uint cc = 0; cc < tttpms.count(); cc++)
						position += tttpms [cc]->positionInformation.getIndex() + "?";
				}

				if (old_position == "")
					old_position = position;

				QString line = "";
				bool add_line = false;

				if (position != old_position) {
					line = QString ("%1: (%2-%3) %4").arg (track->getNumber()).arg (old_timepoint).arg (timepoint).arg (old_position.ascii());
					add_line = true;
				}

				if (timepoint >= track->getLastTrace()) {
					line = QString ("%1: (%2-%3) %4").arg (track->getNumber()).arg (old_timepoint).arg (timepoint).arg (position.ascii());
					add_line = true;
				}

				if (add_line) {
					tmpText = tmpText + line + "\n";
					old_position = position;
					old_timepoint = timepoint + 1;
				}
			}

			//add text lines for the current cell to the history (in reverse order)
			txtPositionHistory->setText (tmpText + "\n" + txtPositionHistory->text());

			//move one level upward
			track = track->getMotherTrack();
		}
	}

	txtPositionHistory->setText ("Cell: (Timepoints) Position\n\n" + txtPositionHistory->text());
}

//SLOT:
void TTTTracking::measureTime (bool _start)
{
	if (_start)
		Tools::startTimeMeasure();
	else {
		int secs = Tools::stopTimeMeasure();
		
		lblMeasuredTime->setText (QString ("%1 secs").arg (secs));
	}
}

//SLOT:
void TTTTracking::loadAllInterruptedCells()
{
	///@todo 
	//BS 2010/02/17: Josef's wish for easier tracking
}

//SLOT:
void TTTTracking::correctFactorProblemTracks()
{
	//nothing to be done... this is not the cause of the problem
}

//SLOT:
void TTTTracking::eraseStoredPositions()
{
	if (! TTTManager::getInst().getTree())
		return;
	
	QString text = "Do you really want to delete the stored positions from this tree?\nNote: this step can only be recovered by re-tracking the tree.\nNote 2: Your coordinates are not affected by this step.";
	if (QMessageBox::question (this, "tTt question", text, "Yes", "No") == 1)
		//user aborted
		return;
	
	if (TTTManager::getInst().getTree()->eraseStoredPositions()) {
		QMessageBox::information (this, "tTt information", "All positions of this tree erased. They are calculated as before", "Ok");
		DataChanged = true;
	}
}

//SLOT:
void TTTTracking::mergeTrees()
{
	if (! TTTManager::getInst().frmTreeMerging)
		return;
	
	TTTManager::getInst().frmTreeMerging->lieTreeA->setText (TTTManager::getInst().getTree()->getFilename());
	
	if (! TTTManager::getInst().frmTreeMerging->isVisible()) {
		
		TTTManager::getInst().frmTreeMerging->show();
	}
}

//SLOT:
void TTTTracking::openFurtherCellOptions()
{
	///@todo
	//show further options (cell type, tissue, ...)
	//how to do this?
}

//SLOT:
void TTTTracking::setCellPropertyWavelength (int /*_wl*/)
{
	///@todo
	//apply this setting to the cell properties
}

//SLOT:
void TTTTracking::exportAllTreeSnapshots()
{
	Tools::exportAllTreesOfPositionToImages (TTTManager::getInst().getBasePositionManager());
}


void TTTTracking::setFormCaption (const QString &_filename)
{
	QString cap = "tTt " + internalVersion + " - Cell Editor";
	
	// Add current position
	int pos = TTTManager::getInst().getCurrentPosition();
	if(pos > 0)
		cap += QString(" - Pos %1").arg(Tools::convertIntPositionToString(pos));

	// Overwrite caption filename if new one is provided
	if (! _filename.isEmpty())
		currentFilenameFormCaption = _filename;

	// Add filename
	if(!currentFilename.isEmpty())
		cap += QString(" - ") + currentFilenameFormCaption;
	
	setCaption (cap);
}

void TTTTracking::refreshStatistics()
{
	TreeStatistics treestat = TreeStatistics::calculate (TTTManager::getInst().getTree());
	
	
	QString stats = "";
	
	//total cell count
	stats = QString ("Total cells = %1\n").arg (treestat.getTotalCount());
	
	//dividing cells
	stats += QString ("Div. cells = %1\n").arg (treestat.getDividingCount());
	
	//apoptotic cells
	stats += QString ("Apo. cells = %1\n").arg (treestat.getApoptoticCount());
	
	//lost cells
	stats += QString ("Lost cells = %1\n").arg (treestat.getLostCount());
	
	//interrupted cells
	stats += QString ("Int. cells = %1\n").arg (treestat.getInterruptedCount());
	
	//generation stats
	for (int i = 0; i < treestat.getGenerations(); i++) {
		stats += QString ("%1 (%2)").arg (i). arg (treestat.getGenerationCounts (i));
		if (i % 2 == 0)
			stats += "\t";
		else
			stats += "\n";
	}
	
	lblTreeStatistics->setText (stats);
}

bool TTTTracking::SaveDialog()
{
	if (DataChanged) {
		
		int answer = QMessageBox::question (this, "Data was changed", "Save changes to current file?", "Yes", "No", "Cancel", 0, 2);
		if (answer == 1)
			//answer = no
			return true;
		else if (answer == 0) {
			//answer = yes
			bool success = saveTTTFile();
			if (! success)
				QMessageBox::information (this, "Try again", "Saving failed.");
			return success;
		}
		else if (answer == 2)
			//answer = cancel
			return false;
		else
			return false;
	}
	return true;
}

bool TTTTracking::showCommentEmbold (int _timePoint)
{
	if (! TTTManager::getInst().getCurrentTrack())
		return false;
	
	if (! TTTManager::getInst().getCurrentTrack()->getComment (_timePoint).isEmpty()) {
		QFont f( "Sans Serif", 10, QFont::Bold );
//		pbtCellComment->setFont (f);
                menuBar->actEdit_CELL_COMMENT->setFont (f);
		return true;	
	}
	else {
		QFont f( "Sans Serif", 10, QFont::Normal );
//		pbtCellComment->setFont (f);
                menuBar->actEdit_CELL_COMMENT->setFont (f);
		return false;
	}
}

void TTTTracking::showCurrentProperties()
{
	if (! TTTManager::getInst().frmCellProperties)
		return;
	
	if (TTTManager::getInst().isTracking())
		if (TTTManager::getInst().getCurrentTrack()) {
			TTTManager::getInst().frmCellProperties->setProperties (TTTManager::getInst().getCurrentTrack()->getTrackPoint (TTTManager::getInst().getTimepoint()).getPropObject());
			if (TTTManager::getInst().frmCellProperties->isHidden())
				TTTManager::getInst().frmCellProperties->show();
		}
}

//recursive
void TTTTracking::setProperties (Track *_track, int _firstTP, int _lastTP, CellProperties _props, bool _setChildren)
{		
	if (! _track)
		return;
	
	bool child1set = false, child2set = false;
	
	for (int i = _firstTP; i <= _lastTP; i++) {
		if (_track->aliveAtTimePoint (i)) {
			_track->setProperties (i, _props, true, true, TTTManager::getInst().frmCellProperties->chkAddWavelengthsOnly->isChecked());
		}
		else if (i > _track->getLastTrace()) {
			
			if (_setChildren) {
				if (_track->getChildTrack (1) && ! child1set) {
					setProperties (_track->getChildTrack (1), i, _lastTP, _props, _setChildren);
					child1set = true;
				}
				if (_track->getChildTrack (2) && ! child2set) {
					setProperties (_track->getChildTrack (2), i, _lastTP, _props, _setChildren);
					child2set = true;
				}
				
				break;
			}
			else
				break;
		}
		
	}
}

//EVENT:
void TTTTracking::showEvent (QShowEvent *_ev)
{
	if(!_ev->spontaneous() && !windowLayoutLoaded) {
		// Load window layout
		UserInfo::getInst().loadWindowPosition(this, "TTTTracking");

		windowLayoutLoaded = true;
	}

	_ev->accept();
}

//EVENT:
void TTTTracking::closeEvent (QCloseEvent *ev)
{
	if (! SaveDialog()) {
		ev->ignore(); 
		return;
	}

	//// Disable load next / load prev buttons
	//menuBar->actFile_OPEN_NEXT_TREE->setEnabled(false);
	//menuBar->actFile_OPEN_NEXT_TREE->setEnabled(false);

	// Save window layout
	UserInfo::getInst().saveWindowPosition(this, "TTTTracking");

	//do not call this->close() ==>> endless LOOP!
	
	TTTManager::getInst().stopTracking();
	
	//TTTManager::getInst().resetBasePosition();
	
	QHash<int, TTTPositionManager*>& allPositions = TTTManager::getInst().getAllPositionManagers();
	for (auto iter = allPositions.begin(); iter != allPositions.end(); ++iter) {
		if ((*iter)->getPictures())
			(*iter)->getPictures()->stopLoading();
		if ((*iter)->frmMovie)
			(*iter)->frmMovie->close();
	}

	if(movieExplorer) 
		movieExplorer->close();

	if(convertOldTreesWindow)
		convertOldTreesWindow->close();

	if (TTTManager::getInst().frmTracksOverview)
		TTTManager::getInst().frmTracksOverview->close();

	if (TTTManager::getInst().frmTreeMap)
		TTTManager::getInst().frmTreeMap->close();

	if (TTTManager::getInst().frmTreeStyleEditor)
		TTTManager::getInst().frmTreeStyleEditor->close();

	if (TTTManager::getInst().frmCellProperties)
		TTTManager::getInst().frmCellProperties->close();

	if (TTTManager::getInst().frmTextbox)
		TTTManager::getInst().frmTextbox->close();

	if (TTTManager::getInst().frmSymbolSelection)
		TTTManager::getInst().frmSymbolSelection->close();

	if (TTTManager::getInst().frmAutoTracking)
		TTTManager::getInst().frmAutoTracking->close();


	//surely there is no data left to be saved
	DataChanged = false;
	
	ev->accept();
}

//EVENT:
void TTTTracking::paintEvent (QPaintEvent *)
{
	displayTimeScale();
}

void TTTTracking::updateLegend()
{
	if (! lvwCurrentTreeItems)
		return;
	
	//display all colors that are used in the tree (only if they are visible)
	
	while (lvwCurrentTreeItems->columns())
		lvwCurrentTreeItems->removeColumn (lvwCurrentTreeItems->columns() - 1);
	
	lvwCurrentTreeItems->addColumn("Items");
	lvwCurrentTreeItems->setColumnWidthMode (0, Q3ListView::Maximum);
	lvwCurrentTreeItems->addColumn("Keys");
	lvwCurrentTreeItems->setColumnWidthMode (1, Q3ListView::Maximum);
	
	//usual tree style elements
/*	QStringList elements = UserInfo::getInst().getStyleSheet().elements();
	
	for (QStringList::iterator iter = elements.begin(); iter != elements.end(); ++iter) {
		TreeElementStyle tes = UserInfo::getInst().getStyleSheet().getElement (*iter);
		
		bool add = true;
		if (tes.isVisible()) {
			//exclude some elements that are self-explaining
				//add = false;
			
			if (add) {
				lboCurrentTreeItems->insertItem (new MyQListBoxText (lboCurrentTreeItems, tes.getText(), tes.getColor (0)));
			}
		}
		
	}*/
	
	
	//tracking key elements
	QStringList keys = TrackingKeys::getKeys();
	MyQCheckListItem *elem = 0;
	
	for (QStringList::const_iterator iter = keys.constBegin(); iter != keys.constEnd(); ++iter) {
		TreeElementStyle tes = UserInfo::getInst().getStyleSheet().getAssociatedElement ((*iter).ascii() [0]);
		
		bool add = true;
		if (tes.isVisible()) {
			//exclude some elements that are self-explaining
				//add = false;
			
			if (tes.getSymbol().isEmpty())
				add = false;
			
			if (add) {
				//lboCurrentTreeItems->insertItem (new QListBoxText (lboCurrentTreeItems, tes.getSymbol())); //, tes.getColor (0)));
				//lvwCurrentTreeItems->
				elem = new MyQCheckListItem (lvwCurrentTreeItems, tes.getSymbol(), Q3CheckListItem::CheckBox, tes.getColor (0));
				elem->setOn (true);
				elem->setText (1, QString (QChar (tes.getKey())));
			}
		}
	}
	
}


void TTTTracking::setLoadingStartToFirstTp()
{
	// Get first timepoint
	StartLoading = TTTManager::getInst().getCurrentPositionManager()->getFirstTimePoint();
	lieLoadTimePointStart->setText (QString ("").setNum (StartLoading));

	// Display selection
	timeScale->selectRegion (StartLoading, EndLoading);
}


void TTTTracking::setLoadingEndToLastTp()
{
	// Get first timepoint
	EndLoading = TTTManager::getInst().getCurrentPositionManager()->getLastTimePoint();
	lieLoadTimePointEnd->setText (QString("").setNum (EndLoading));

	// Display selection
	timeScale->selectRegion (StartLoading, EndLoading);
}

void TTTTracking::setPermanentStatusbarMessage( QString _message )
{
	permanentStatusMessage->setText(_message);
}

void TTTTracking::resetPermanentStatusbarMessage()
{
	if(defaultMessage.isEmpty())
		permanentStatusMessage->setText("Ready");
	else
		permanentStatusMessage->setText(defaultMessage);
}

void TTTTracking::displayStatusBarMessage( QString _message, int _time /*= 1000*/ )
{
	statusBar->showMessage(_message, _time);
}

void TTTTracking::toggleWavelengthInhertiance( bool _on )
{
	// Get wavelength index (in this case 0 refers to wavelength 1, 1 to wl 2 and so on)
	int wlIndex = -1;
	for(unsigned int i = 0; i < menuBar->actTracking_INHERIT_WLS.size(); ++i) {
		if(sender() == menuBar->actTracking_INHERIT_WLS[i]) {
			wlIndex = i;
			break;
		}
	}

	// Check if found
	if(wlIndex == -1) {
		qWarning() << "TTTTracking::toggleWavelengthInhertiance(): Internal error 1";
		return;
	}

	// Enable/disable wavelength inheritance
	if(wlIndex < inheritedWLs->size())
		(*inheritedWLs)[wlIndex] = _on;
	else
		qWarning() << "TTTTracking::toggleWavelengthInhertiance(): Internal error 2";
}

const QVector<bool> * TTTTracking::getInheritedWLs() const
{
	return inheritedWLs;
}

void TTTTracking::openNextTree()
{
	// Get ttt files folder
	QString treesFolderString = currentFilename.left(currentFilename.lastIndexOf('/'));

	if(treesFolderString.isEmpty())
		return;

	// Create QDir object
	QDir treesFolder(treesFolderString);
	if(!treesFolder.exists())
		return;

	// Get ttt files
	QStringList tttFiles = FastDirectoryListing::listFiles(treesFolder, QStringList(".ttt"));
	tttFiles.sort();

	// Find next tree
	QString nextTree;
	for(QStringList::const_iterator it=tttFiles.constBegin(); it!=tttFiles.constEnd(); ++it) {
		if(*it == CurrentColonyName) {
			// Go to next tree
			++it;

			// If this is the last tree, go to first tree
			if(it == tttFiles.constEnd())
				it = tttFiles.constBegin();

			nextTree = *it;
			break;
		}
	}

	// Check if we have a next tree
	if(nextTree.isEmpty()) {
		QMessageBox::information(this, "Note", "No next tree found.");
		return;
	}

	// Open next tree
	nextTree = treesFolder.absoluteFilePath(nextTree);
	setColony(nextTree);
}

void TTTTracking::openPreviousTree()
{
	// Get ttt files folder
	//QString treesFolderString;
	//if (TTTPositionManager* bpm = TTTManager::getInst().getBasePositionManager())
	//	treesFolderString = bpm->getTTTFileDirectory();

	QString treesFolderString = currentFilename.left(currentFilename.lastIndexOf('/'));

	if(treesFolderString.isEmpty())
		return;

	// Create QDir object
	QDir treesFolder(treesFolderString);
	if(!treesFolder.exists())
		return;

	// Get ttt files
	QStringList tttFiles = FastDirectoryListing::listFiles(treesFolder, QStringList(".ttt"));
	tttFiles.sort();

	// Find prev tree
	QString prevTree;
	for(QStringList::const_iterator it=tttFiles.constBegin(); it!=tttFiles.constEnd(); ++it) {
		if(*it == CurrentColonyName) {
			if(it != tttFiles.constBegin()) {
				// If we are not at the first tree, go back one tree
				--it;
			}
			else {
				// If we are at the first tree, return the last tree
				it=tttFiles.constEnd();
				--it;	// If the list was empty (first==last), we would not reach this point, so no check neccessary here
			}

			prevTree = *it;
			break;
		}
	}

	// Check if we have a next tree
	if(prevTree.isEmpty()) {
		QMessageBox::information(this, "Note", "No previous tree found.");
		return;
	}

	// Open next tree
	prevTree = treesFolder.absoluteFilePath(prevTree);
	setColony(prevTree);
}

void TTTTracking::updateChangelog()
{
	const Tree* curTree = TTTManager::getInst().getTree();

	// No tree -> no changelog items
	if(!curTree) {
		clearChangelog();
		return;
	}

	// Get changelog and change rowcount accordingly
	const QList<QPair<QDate,QByteArray> >& changeLog = curTree->getChangelog();
	tbwChangelog->setRowCount(changeLog.size());	

	// Iterate over changelog entries
	int curRow = 0;
	for(QList<QPair<QDate,QByteArray> >::const_iterator it = changeLog.constBegin(); it != changeLog.constEnd(); ++it, ++curRow) {
		const QPair<QDate,QByteArray>& curPair = *it;

		// Extract date as string
		QString dateString = curPair.first.toString("dd.MM.yyyy");

		// Add date
		QTableWidgetItem *tmp = new QTableWidgetItem(dateString);
		tmp->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
		tbwChangelog->setItem(curRow, 0, tmp);

		// Add user
		tmp = new QTableWidgetItem(QString(curPair.second));
		tmp->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
		tbwChangelog->setItem(curRow, 1, tmp);
	}

	// Adjust size
	tbwChangelog->resizeColumnsToContents ();
}

void TTTTracking::clearChangelog()
{
	tbwChangelog->setRowCount(0);
}

void TTTTracking::displayTTTChangelog()
{
	// Display modal window
	TTTVersionInfo versionWindow(this, false);
	versionWindow.exec();
}

void TTTTracking::startAutoTrackingAssistant()
{
	// Show autotracking form
	TTTManager::getInst().frmAutoTracking->show();
	TTTManager::getInst().frmAutoTracking->activateWindow();
}

void TTTTracking::test()
{
//#ifdef DEBUGMODE 	

	//TTTTest *window = new TTTTest();
	TTTTest *window = nullptr;
	window->setAttribute(Qt::WA_DeleteOnClose);
	window->show();

//#endif
}

void TTTTracking::updateDisplay()
{
	// Get base and current position
	TTTPositionManager* basePos = TTTManager::getInst().getBasePositionManager();
	if (!basePos) 
		return;
	TTTPositionManager* curPos = TTTManager::getInst().getCurrentPositionManager();
	if (!curPos)
		return;

	// Update lblCurrentTimePoint
	if (basePos->getFiles())
		lblCurrentTimePoint->setText (QString("%1 <font size=""-2"">(%2)</font>").
		arg (curPos->getTimepoint()).
		arg (basePos->getFiles()->getRealTime (curPos->getTimepoint(), 1).toString (COMPLETE_DATE_FORMAT)));
}

void TTTTracking::openMovieWindow()
{
	// Get current position manager
	TTTPositionManager* curPos = TTTManager::getInst().getCurrentPositionManager();
	if (!curPos)
		return;

	// Get frmMovie
	TTTMovie* tmp = curPos->frmMovie;
	if (!tmp)
		return;

	// Show frmMovie
	tmp->show();
	tmp->raise();
	if(tmp->isMinimized())
		tmp->showMaximized();
}

void TTTTracking::goToFirstPosOfTree()
{
	// Get current tree
	Tree *curTree = TTTManager::getInst().getTree();
	if(!curTree) {
		displayStatusBarMessage("Error: No tree opened!", 2000);
		return;
	}

	// Get first trackpoint
	TrackPoint* firstTrackPt = 0;
	Track* motherTrack = curTree->getBaseTrack();
	if(motherTrack) {
		int firstTimePt = motherTrack->getFirstTimePoint();
		if(firstTimePt > 0)
			firstTrackPt = motherTrack->getTrackPointByTimePoint(firstTimePt);
	}

	// Check if trackpoint was found
	if(!firstTrackPt) {
		displayStatusBarMessage("Error: Tree contains no trackpoints!", 2000);
		return;
	}

	// Get position
	const QString pos = firstTrackPt->Position;
	if(pos.isEmpty()) {
		displayStatusBarMessage("Error: Could not determine position of first trackpoint!", 2000);
		return;
	}

	// Finally, change current position
	if(!TTTManager::getInst().frmPositionLayout->setPosition(pos)) {
		displayStatusBarMessage(QString("Error: Could not change position (%1)!").arg(pos), 2000);
		return;
	}
}

void TTTTracking::updateTreeSize()
{
	treeView->updateSize();
}

void TTTTracking::launchTTTStats()
{
	// Try to get path
	QSettings settings("tTt", "tTt");
	QString path = settings.value("stats_path", QString()).toString();

	// Check path
	if(path.isEmpty() || !QFile::exists(path)) {
		path = "";

		// Look in ./
		if(QFile::exists("./tttstats.exe")) {
			path = "./tttstats.exe";
		}
		// Look in ../TTT32bit/
		else if(QFile::exists("../TTT32bit/tttstats.exe")) {
			path = "../TTT32bit/tttstats.exe";
		}
		// Ask user
		else {
			path = QFileDialog::getOpenFileName(this, "Select staTTs executable", QString(), "staTTTs (tttstats.exe)");
		}
	}

	// Run
	if(!path.isEmpty()) {
		QProcess::execute(path);
		settings.setValue("stats_path", path);
	}
}

void TTTTracking::launchAMT()
{
	// Try to get path
	QSettings settings("tTt", "tTt");
	QString path = settings.value("amt_path", QString()).toString();

	// Check path
	if(path.isEmpty() || !QFile::exists(path)) {
		path = QFileDialog::getOpenFileName(this, "Select AMT executable", QString(), "AMT (*.exe)");
	}

	// Run
	if(!path.isEmpty()) {
		QProcess::execute(path);
		settings.setValue("amt_path", path);
	}
}

bool TTTTracking::getInheritAdherence() const
{
	return menuBar->actTracking_INHERIT_ADHERENCE->isChecked();
}

bool TTTTracking::getInheritDifferentiation() const
{
	return menuBar->actTracking_INHERIT_DIFFERENTIATION->isChecked();
}

void TTTTracking::loadAllImagesOfCurrentTree()
{
	// Get current tree
	ITree* curTree = TTTManager::getInst().getTree();
	if(!curTree)
		return;

	// Get selected wavelengths
	QList<int> wls;
	for(int i = 0; i <= MAX_WAVE_LENGTH; ++i) {
		if(WaveLengthSelected[i])
			wls.append(i);
	}

	// Inform user if no wls selected
	if(wls.isEmpty()) {
		QMessageBox::information(this, "Note", "You first need to select the wavelengths to load on the left ('Loading + Saving' tab).");
		return;
	}

	// Display progress bar
	QProgressDialog progessBar("Loading images..", "Cancel", 0, 2, this);
	progessBar.setWindowModality(Qt::ApplicationModal);
	progessBar.setMinimumDuration(0);
	progessBar.setValue(0);

	// PictureArray objects with images to be loaded and time point range with images
	QList<PictureArray*> picsArrays;
	int firstTimePoint = -1,
		lastTimePoint = -1;

	// Encountered Positions (need to be initialized)
	QList<QString> initializedPositions;

	// Tracks to process
	QList<ITrack*> tracksToProcess;
	tracksToProcess.append(curTree->getRootNode());

	// Process tracks
	unsigned int counter = 0;
	while(!tracksToProcess.isEmpty()) {
		if(progessBar.wasCanceled())
			break;

		// Get cur track
		ITrack* curTrack = tracksToProcess.takeLast();
		if(!curTrack)
			continue;

		// Process current track
		for(int tp = curTrack->getFirstTimePoint(); tp <= curTrack->getLastTimePoint(); ++tp) {
			// Process events to prevent progress dialog from freezing
			QApplication::processEvents();

			// Update timepoints
			if(firstTimePoint == -1 || tp < firstTimePoint)
				firstTimePoint = tp;
			if(lastTimePoint == -1 || tp > lastTimePoint)
				lastTimePoint = tp;

			// Get current trackpoint
			ITrackPoint* curTP = curTrack->getTrackPointByTimePoint(tp);
			if(!curTP)
				continue;

			// Mark picture of trackpoint to be loaded
			int posNum = curTP->getPositionNumber();
			if(posNum <= 0)
				continue;

			// Convert posNum to string
			QString posString = Tools::convertIntPositionToString(posNum);
			if(posString.isEmpty())
				continue;

			// Add to positions array
			if(!initializedPositions.contains(posString)) {
				initializedPositions.append(posString);

				// Init position
				TTTManager::getInst().frmPositionLayout->setPosition(posString);
			}

			// Get position manager
			TTTPositionManager* posMan = TTTManager::getInst().getPositionManager(posString);
			if(!posMan)
				continue;

			// Select pictures
			PictureArray* pics = posMan->getPictures();
			if(pics) {
				// Add to array
				if(!picsArrays.contains(pics))
					picsArrays.append(pics);

				// Mark for loading
				for(int i = 0; i < wls.size(); ++i) {
					pics->setLoading(tp, 1, wls[i], true);
				}
			}
		}

		// Add children to list of tracks to process (can be 0)
		tracksToProcess.append(curTrack->getChild1());
		tracksToProcess.append(curTrack->getChild2());
	}

	progessBar.setValue(1);

	// Load images
	for(int i = 0; i < picsArrays.size(); ++i) {
		QApplication::processEvents();
		if(progessBar.wasCanceled())
			break;

		// Load images
		picsArrays[i]->loadPictures(false, false);

		// Loading may have failed because of low free RAM
		if(SystemInfo::shortOfMemory()) 
			break;
	}

	progessBar.setValue(2);
}

void TTTTracking::setInheritAllProperties( bool _on )
{
	// Enable/disable selective inheritance menu buttons
	menuBar->trackingSubMenuInheritWLs->setEnabled(!_on);
	menuBar->actTracking_INHERIT_ADHERENCE->setEnabled(!_on);
	menuBar->actTracking_INHERIT_DIFFERENTIATION->setEnabled(!_on);
}

bool TTTTracking::getInheritAllProperties() const
{
	return menuBar->actTracking_INHERIT_ALL->isChecked();
}

void TTTTracking::updateBackwardTrackingModeDisplay()
{
	if(TTTManager::getInst().inBackwardTrackingMode())
		lblBackwardTrackingMode->setText("Backward Tracking Mode: On");
	else
		lblBackwardTrackingMode->setText("Backward Tracking Mode: Off");
}

void TTTTracking::setDrawContinuousCellLines( bool _on )
{
	if(!treeView)
		return;

	// Change and update treeview
	treeView->setDrawContinuousCellLines(_on);
	drawTree();
}

void TTTTracking::openMovieExplorer()
{
	// Create movieExplorer
	if(!movieExplorer)
		movieExplorer = new TTTMovieExplorer();

	// Show movieExplorer
	movieExplorer->showMaximized();
	movieExplorer->raise();
}

void TTTTracking::convertOldTrees()
{
	// Create window
	if(!convertOldTreesWindow)
		convertOldTreesWindow = new TTTConvertOldTrees();

	// Show window
	convertOldTreesWindow->show();
	convertOldTreesWindow->raise();
}
