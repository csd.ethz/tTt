/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tttuserqueryform.h"
//Added by qt3to4:
#include <QCloseEvent>

TTTUserQueryForm::TTTUserQueryForm (UserInfo *_userInfo, const QString &_path, QWidget *parent)
    :QDialog(parent), path(_path)
{
        setupUi (this);

	// Disable context-help button
	setWindowFlags( (windowFlags() | Qt::CustomizeWindowHint) & (~Qt::WindowContextHelpButtonHint));

	userInfo = _userInfo;
	
	connect ( pbtCancel, SIGNAL (clicked()), this, SLOT (close()));
	connect ( pbtContinue, SIGNAL (clicked()), this, SLOT (close()));
	connect ( pbtAddUser, SIGNAL (clicked()), this, SLOT (addUser()));
	connect ( pbtRemoveUser, SIGNAL (clicked()), this, SLOT (removeUser()));

	if (path.right (1) != "/")
		path += "/";
	
	loadUsersigns ();
}

//EVENT:
void TTTUserQueryForm::closeEvent (QCloseEvent *_ev)
{
	if (sender() == pbtCancel) {
		//cancelled
		if (userInfo)
			userInfo->setUsersign ("");
	}
	else if (sender() == pbtContinue) {
		//user sign entered, try to continue
		if (userInfo)
			userInfo->setUsersign (cboUsersign->currentText());
	}
	
	_ev->accept();
}

void TTTUserQueryForm::loadUsersigns ()
{
	//read user signs from the file "ttt_users" in the directory of the executable
	//std::cout << SystemInfo::getExecutablePath() << std::endl;
	
	//QString userstring = SystemInfo::readTextFile (SystemInfo::getExecutablePath() + "/ttt_users");
	
	QString userstring = Tools::readTextFile (path + "ttt_users");
	
	if (! userstring.isEmpty()) {
		QStringList users = QStringList::split ("\n", userstring);
		
		for (QStringList::const_iterator iter = users.constBegin(); iter != users.constEnd(); ++iter) {
			cboUsersign->insertItem (*iter);
		}
	}
	else {
		pbtContinue->setEnabled(false);
		pbtRemoveUser->setEnabled(false);
	}
}

void TTTUserQueryForm::addUser()
{
	// Ask for new usser sign
	QString newUser = QInputDialog::getText(this, "Add user", "Enter user name to add (we recommend to use initials):");
	if(newUser.isEmpty())
		return;

	// Read current users
	QString usersFile = Tools::readTextFile (path + "ttt_users");
	if(!usersFile.isEmpty()) {
		if(usersFile.right(1) != "\n")
			usersFile.append('\n');
	}

	// Add new user to file
	usersFile.append(newUser);
	usersFile.append('\n');
	Tools::writeTextFile(path + "ttt_users", usersFile);

	// Add new user to combobox
	cboUsersign->insertItem(newUser);

	//// Save users
	//if(Tools::writeTextFile(path + "ttt_users", usersFile))
	//	cboUsersign->insertItem(newUser);
	//else
	//	QMessageBox::critical(this, "Error", "Adding user failed (Could not save users file).");

	// Enable controls
	pbtContinue->setEnabled(true);
	pbtRemoveUser->setEnabled(true);
}

void TTTUserQueryForm::removeUser()
{
	// Get user sign
	QString selectedUser = cboUsersign->currentText();
	if(selectedUser.isEmpty()) {
		QMessageBox::information(this, "No user selected", "You have to select the user sign you want to remove in the list.");
		return;
	}

	// Confirm
	QString msg = QString("Remove user '%1'?").arg(selectedUser);
	if(QMessageBox::question(this, "Confirm", msg, QMessageBox::Yes | QMessageBox::No) != QMessageBox::Yes)
		return;

	// Make new users string
	QString newUsersString = "";
	for(int i = 0; i < cboUsersign->count(); ++i) {
		QString itemText = cboUsersign->itemText(i);
		if(itemText != selectedUser)
			newUsersString += itemText + "\n";
	}

	// Save users
	if(Tools::writeTextFile(path + "ttt_users", newUsersString)) {
		// If saving worked, reload users
		cboUsersign->clear();
		loadUsersigns();
	}
	else
		QMessageBox::critical(this, "Error", "Removing user failed (Could not save users file).");
}

//#include "tttuserqueryform.moc"
