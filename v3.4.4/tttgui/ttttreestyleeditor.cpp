/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ttttreestyleeditor.h"

#include "tttdata/userinfo.h"

#include <QCloseEvent>
#include <QSettings>

TTTTreeStyleEditor::TTTTreeStyleEditor(QWidget *parent)
    :QWidget(parent)
{
        setupUi (this);

        connect ( pbtApplyClose, SIGNAL (clicked()), this, SLOT (close()));
	connect ( pbtApply, SIGNAL (clicked()), this, SLOT (apply()));
	//SystemInfo::report ("style apply connect called");
	connect ( pbtCancel, SIGNAL (clicked()), this, SLOT (close()));
	connect ( pbtLoad, SIGNAL (clicked()), this, SLOT (load()));
	connect ( pbtSave, SIGNAL (clicked()), this, SLOT (save()));
	
	//tree style
	connect ( pbtTreeGrayscale, SIGNAL (clicked()), this, SLOT (setTreeGrayscale()));
	connect ( pbtDefaultTreeStyle, SIGNAL (clicked()), this, SLOT (setTreeDefault()));
	connect ( chkTreeVisible, SIGNAL (toggled (bool)), this, SLOT (showTreePropertyFrame (bool)));
	connect ( cboTreeElement, SIGNAL (activated (const QString&)), this, SLOT (changeTreeElement (const QString&)));
	connect ( pbtTreeColor, SIGNAL (clicked()), this, SLOT (chooseColor()));
	
	//movie layout
	connect ( pbtMovieReset, SIGNAL (clicked()), this, SLOT (resetMovie()));
	connect ( spbMoviePicsPerRow, SIGNAL (valueChanged (int)), this, SLOT (updateMovieRows (int)));
	connect ( pbtMovieCellCircleColor, SIGNAL (clicked()), this, SLOT (setMovieCCC()));
	connect ( pbtMovieCellNumberColor, SIGNAL (clicked()), this, SLOT (setMovieCNC()));
	connect ( spbMovieCellCircleThickness, SIGNAL (valueChanged (int)), this, SLOT (setMovieCCT (int)));
	connect ( pbtMovieCellPathColor, SIGNAL (clicked()), this, SLOT (setMovieCPC()));
	connect ( spbMovieCellNumberFontSize, SIGNAL (valueChanged (int)), this, SLOT (setMovieCNFS (int)));
	connect ( pbtMovieWavelengthBoxColor, SIGNAL (clicked()), this, SLOT (setMovieWBC()));
	connect ( spbMovieWavelengthBoxFontsize, SIGNAL (valueChanged (int)), this, SLOT (setMovieWBFS (int)));

	connect ( spbCellPathThickness, SIGNAL (valueChanged (int)), this, SLOT (setMovieCPT (int)));
	connect ( spbCellPathTimeInterval, SIGNAL (valueChanged (int)), this, SLOT (setMovieCP_TI (int)));
	connect ( chkOutOfSyncBox, SIGNAL (toggled (bool)), this, SLOT (setShowOutOfSynxBox (bool)));
	
	//key associations
	connect ( cboKey, SIGNAL (activated (const QString&)), this, SLOT (changeKeyElement (const QString&)));
	connect ( chkKeyVisible, SIGNAL (toggled (bool)), this, SLOT (showKeyPropertyFrame (bool)));
	connect ( pbtKeyColor, SIGNAL (clicked()), this, SLOT (chooseColor()));
	
	//various global settings
	connect ( chkAddUserSignToTTTFiles, SIGNAL (toggled (bool)), this, SLOT (setAddUserSignToTTTFiles (bool)));
	connect ( chkUseNewTTTFileFolderStructure, SIGNAL (toggled (bool)), this, SLOT (setUseNewTTTFileFolderStructure (bool))),
	connect ( chkUseExperimentGlobalTrees, SIGNAL (toggled (bool)), this, SLOT (setUseExperimentGlobalTrees (bool)));
	connect ( spbExportWavelength, SIGNAL (valueChanged (int)), this, SLOT (setExportWavelength (int)));
	connect ( chkLoadPicturesAsynchronous, SIGNAL (toggled (bool)), this, SLOT (setLoadPicturesAsynchronous (bool)));
	connect ( chkShowTreeCountInPositionLayout, SIGNAL (toggled (bool)), this, SLOT (setShowTreeCountInPositionLayout (bool)));
	connect ( chkUseColorsForAllTracks, SIGNAL (toggled (bool)), this, SLOT (setUseColorsForAllTracks  (bool)));
	connect ( spbNumPicsNeighborLoad, SIGNAL (valueChanged (int)), this, SLOT (setNumPicsToLoadByNextPosItem (int)));
	connect ( chkLoadNeighborTakeOverGamma, SIGNAL (toggled (bool)), this, SLOT (setTakeOverDisplaySettingsNextPosItem  (bool)));
	
	//startup settings
	connect ( chkStartUpMoveToFirstFluor, SIGNAL (toggled (bool)), this, SLOT (setStartUpMoveToFirstFluor (bool)));
	connect ( chkStartUpBgCircleVisible, SIGNAL (toggled (bool)), this, SLOT (setStartUpBgCircleVisible (bool)));
	connect ( chkStartUpFullPicture, SIGNAL (toggled (bool)), this, SLOT (setStartUpFullPicture (bool)));
	connect ( chkStartUpAlternateNormalBgTracking, SIGNAL (toggled (bool)), this, SLOT (setStartUpAlternateNormalBgTracking (bool)));
	
	
	buttons.resize (MAX_WAVE_LENGTH + 1);
	QPushButton *button = 0;
	for (int i = 0; i < (int)buttons.size(); i++) {
		button = new QPushButton (QString().setNum (i), (QWidget*)grbMovieArrangement);
		buttons.insert (i, button);
		connect (button, SIGNAL (clicked()), this, SLOT (setMovieOrder()));
	}
	
	resetMovie();
	
	setStyleSheet (styleSheet);
	
	
	//NOTE: when deleting tabs, the indices shift!
	int tabshift = 0;
	if (! COMPFLAG_STYLESETTING_TREE_LAYOUT) {
		tabLayouts->setCurrentPage (0 - tabshift);
		tabLayouts->removePage (tabLayouts->currentPage());
		tabshift++;
	}
	if (! COMPFLAG_STYLESETTING_MOVIE_LAYOUT) {
		tabLayouts->setCurrentPage (1 - tabshift);
		tabLayouts->removePage (tabLayouts->currentPage());
		tabshift++;
	}
	if (! COMPFLAG_STYLESETTING_TRACKING_KEYS) {
		tabLayouts->setCurrentPage (2 - tabshift);
		tabLayouts->removePage (tabLayouts->currentPage());
		tabshift++;
	}
	if (! COMPFLAG_STYLESETTING_VARIOUS) {
		tabLayouts->setCurrentPage (3 - tabshift);
		tabLayouts->removePage (tabLayouts->currentPage());
		tabshift++;
	}
}


//common methods
//==============

void TTTTreeStyleEditor::closeEvent (QCloseEvent *ev)
{
	if (sender() == pbtApplyClose)
		apply();
	
	ev->accept();
}

//SLOT:
void TTTTreeStyleEditor::apply()
{
	saveTreeElement();
	saveKeyElement();
	
	UserInfo::getInst().setStyleSheet (styleSheet);
	
	// QSettings dependent stuff (saved locally!)
	QSettings settings("tTt", "tTt");
	settings.setValue("amt_path", lieAmtExe->text());
	settings.setValue("stats_path", lieStatsExe->text());

	// New settings saved in ini file on NAS
	QSettings* userSettings = UserInfo::getInst().getUserSettings();
	if(userSettings) {
		// KEY_TTTMOVIE_USECOMMENTASWL
		userSettings->setValue(UserInfo::KEY_TTTMOVIE_USECOMMENTASWL, chkUseCommentAsWl->isChecked());
	}

	emit styleChosen (styleSheet);
	//emit movieLayoutSet();
}

//SLOT:
void TTTTreeStyleEditor::load()
{
	QString filename = Q3FileDialog::getOpenFileName (QString::null, "Tree styles (*" + STYLEFILE_SUFFIX + ")", this, "open file dialog", "Choose a style file");
	
	if (filename.isEmpty())
		return;
	
	if (! styleSheet.load (filename))
		QMessageBox::warning (this, "File error", "Could not open style file. Please try another one.", "OK");
	
	setStyleSheet (styleSheet);
}

//SLOT:
void TTTTreeStyleEditor::save()
{
	QString filename = Q3FileDialog::getSaveFileName (QString::null, "Tree styles (*" + STYLEFILE_SUFFIX + ")", this, "save file dialog", "Choose a style file");
	
	if (filename.isEmpty())
		return;
	
	if (! styleSheet.save (filename))
		QMessageBox::warning (this, "File error", "Could not save to style file. Check your writing permissions.", "OK");
}

void TTTTreeStyleEditor::setStyleSheet (StyleSheet &_ss)
{
	styleSheet = _ss;
	
	//fill tree element combobox
	cboTreeElement->clear();
	cboTreeElement->insertStringList (styleSheet.elements());
	cboTreeElement->setCurrentItem (0);
	showTreeElement (cboTreeElement->currentText());
	
	//fill key combobox
	cboKey->clear();
	cboKey->insertStringList (TrackingKeys::getKeys());
	cboKey->setCurrentItem (0);
	showKeyElement (cboKey->currentText());
	
	showVariousSettings();
	showMovieProperties();
	showStartUpSettings();
}


//SLOT:
void TTTTreeStyleEditor::chooseColor()
{
	if (! sender())
		return;
	
	QColor col = QColorDialog::getColor (((QPushButton*)sender())->paletteBackgroundColor(), this);
	((QPushButton*)sender())->setPaletteBackgroundColor (col);
}


//tree layout methods
//===================

//SLOT:
void TTTTreeStyleEditor::setTreeGrayscale()
{
	saveTreeElement();
	styleSheet.setGrayScale();
	showTreeElement (cboTreeElement->currentText());
}

//SLOT:
void TTTTreeStyleEditor::setTreeDefault()
{
	styleSheet.reset (1);
	showTreeElement (cboTreeElement->currentText());
}

//SLOT:
void TTTTreeStyleEditor::showTreePropertyFrame (bool _show)
{
	if (_show)
		fraTreeProperties->show();
	else
		fraTreeProperties->hide();
}

//SLOT:
void TTTTreeStyleEditor::changeTreeElement (const QString &_name)
{
	//store current element
	saveTreeElement();
	
	//display new element
	showTreeElement (_name);
}

void TTTTreeStyleEditor::saveTreeElement()
{
	if (tesTree.rtti()) {
		tesTree.setVisible (chkTreeVisible->isChecked());
		tesTree.setColor (0, pbtTreeColor->paletteBackgroundColor());
		tesTree.setPositionX (lieTreePosition->text().toInt());
		tesTree.setSize (spbTreeSize->value());
		tesTree.setSymbol (lieTreeSymbol->text());
		styleSheet.setElement (tesTree);
	}
}

void TTTTreeStyleEditor::showTreeElement (const QString &_name)
{
	tesTree = styleSheet.getElement (_name);
	
	if (tesTree.rtti()) {
		//display properties for currently chosen tree element
		chkTreeVisible->setChecked (tesTree.isVisible());
		fraTreeProperties->show();
		if (! tesTree.isVisible())
			fraTreeProperties->hide();
		pbtTreeColor->setPaletteBackgroundColor (tesTree.getColor (0));
		lieTreePosition->setText (QString().setNum (tesTree.getPositionX()));
		spbTreeSize->setValue (tesTree.getSize());
		lieTreeSymbol->setText (tesTree.getSymbol());
	}
}




//movie layout methods
//====================

void TTTTreeStyleEditor::setMovieLayout (QVector<int> _layout, int _picsPerRow, QColor _ccc, QColor _cnc, int _cct, QColor _cpc, int _cnfs, int _cpt, int _cp_ti, QColor _wbc, int _wbfs)
{
	styleSheet.cellColor = _ccc;
	styleSheet.cellNumberColor = _cnc;
	styleSheet.cellPathColor = _cpc;
	styleSheet.cellCircleThickness = _cct;
	styleSheet.cellNumberFontSize = _cnfs;
	styleSheet.setWLMapping (_layout, _picsPerRow);
	styleSheet.cellPathThickness = _cpt;
	styleSheet.cellPathTimeInterval = _cp_ti;
        styleSheet.wavelengthBoxColor = _wbc;
        styleSheet.wavelengthBoxFontSize = _wbfs;

	showMovieProperties();
}

void TTTTreeStyleEditor::showMovieProperties()
{
	pbtMovieCellCircleColor->setPaletteBackgroundColor (styleSheet.cellColor);
	pbtMovieCellNumberColor->setPaletteBackgroundColor (styleSheet.cellNumberColor);
	spbMovieCellCircleThickness->setValue (styleSheet.cellCircleThickness);
	pbtMovieCellPathColor->setPaletteBackgroundColor (styleSheet.cellPathColor);
	spbMovieCellNumberFontSize->setValue (styleSheet.cellNumberFontSize);
	spbCellPathThickness->setValue (styleSheet.cellPathThickness);
	spbCellPathTimeInterval->setValue (styleSheet.cellPathTimeInterval);
	pbtMovieWavelengthBoxColor->setPaletteBackgroundColor (styleSheet.wavelengthBoxColor);
	spbMovieWavelengthBoxFontsize->setValue (styleSheet.wavelengthBoxFontSize);
	chkOutOfSyncBox->setChecked(styleSheet.showOutOfSyncBox);

	QSettings* userSettings = UserInfo::getInst().getUserSettings();
	if(userSettings) {
		chkUseCommentAsWl->setChecked(userSettings->value(UserInfo::KEY_TTTMOVIE_USECOMMENTASWL).toBool());
	}
}

//SLOT:
void TTTTreeStyleEditor::setMovieOrder()
{
	if (! sender())
		return;
	if (! sender()-isA ("QPushButton"))
		return;
	
	//note: button index == wavelength index!
	
	if (order < MAX_WAVE_LENGTH) {
		int new_ = order;
		int index = -1;
		QPushButton *button = 0;
		for (int i = 0; i < (int)buttons.size(); i++) {
			button = buttons.find (i);
			if (button == sender()) {
				index = i;
				button->setText (QString().setNum (order));
				break;
			}
		}
		
		if (index == -1)
			return;
		
		//shift numbers
		for (int i = new_; i <= styleSheet.wavelengthLayout [index] - 1; i++) {
			styleSheet.wavelengthLayout [ordenador [i]]++;
			button = buttons.find (ordenador [i]);
			if (button) {
				button->setText (QString().setNum (styleSheet.wavelengthLayout [ordenador [i]])); 
			}
		}
		styleSheet.wavelengthLayout [index] = new_;
		
		//calculate inversion of currentLayout (== ordenador)
		for (int i = 0; i < (int)styleSheet.wavelengthLayout.size(); i++) {
			ordenador [styleSheet.wavelengthLayout [i]] = i;
		}
		
		order++;
		
	}
}

//SLOT:
void TTTTreeStyleEditor::resetMovie()
{
	styleSheet.picsPerRow = (MAX_WAVE_LENGTH + 1) / 2;
	spbMoviePicsPerRow->setValue (styleSheet.picsPerRow);
	
	updateMovieRows (styleSheet.picsPerRow);
	
	order = 0;
	
	styleSheet.wavelengthLayout.resize (MAX_WAVE_LENGTH + 1);
	ordenador.resize (MAX_WAVE_LENGTH + 1);
	for (int i = 0; i < (int)styleSheet.wavelengthLayout.size(); i++) {
		styleSheet.wavelengthLayout [i] = i;
		ordenador [i] = i;
		if (buttons.find (i))
			buttons.find (i)->setText (QString().setNum (i));
	}


	styleSheet.cellColor = Qt::white;
	pbtMovieCellCircleColor->setPaletteBackgroundColor (styleSheet.cellColor);
	styleSheet.cellNumberColor = Qt::white;
	pbtMovieCellNumberColor->setPaletteBackgroundColor (styleSheet.cellNumberColor);
	styleSheet.cellCircleThickness = 1;
	spbMovieCellCircleThickness->setValue (styleSheet.cellCircleThickness);
	styleSheet.cellPathColor = Qt::red;
	pbtMovieCellPathColor->setPaletteBackgroundColor (styleSheet.cellPathColor);
	styleSheet.cellNumberFontSize = 10;
	spbMovieCellNumberFontSize->setValue (styleSheet.cellNumberFontSize);
	styleSheet.cellPathThickness = 1;
	spbCellPathThickness->setValue (styleSheet.cellPathThickness);
	styleSheet.cellPathTimeInterval = 0;
	spbCellPathTimeInterval->setValue (styleSheet.cellPathTimeInterval);
        styleSheet.wavelengthBoxColor = Qt::red;
        pbtMovieWavelengthBoxColor->setPaletteBackgroundColor (styleSheet.wavelengthBoxColor);
        styleSheet.wavelengthBoxFontSize = 20;
        spbMovieWavelengthBoxFontsize->setValue (styleSheet.wavelengthBoxFontSize);
}

//SLOT:
void TTTTreeStyleEditor::updateMovieRows (int _picsPerRow)
{
	styleSheet.picsPerRow = _picsPerRow;
	
	int row = -1;
	QPushButton *button = 0;
	for (int i = 0; i < (int)buttons.size(); i++) {
		button = buttons.find (i);
		if (button) {
			int x = offsetX + (i % styleSheet.picsPerRow) * 40;
			if (i % styleSheet.picsPerRow == 0) 
				row++;
			int y = offsetY + row * 40;
			button->setGeometry (x, y, 40, 40);
		}
	}
}

//SLOT:
void TTTTreeStyleEditor::setMovieCCC()
{
	styleSheet.cellColor = QColorDialog::getColor (Qt::white, this);
	pbtMovieCellCircleColor->setPaletteBackgroundColor (styleSheet.cellColor);
}

//SLOT:
void TTTTreeStyleEditor::setMovieCNC()
{
	styleSheet.cellNumberColor = QColorDialog::getColor (Qt::white, this);
	pbtMovieCellNumberColor->setPaletteBackgroundColor (styleSheet.cellNumberColor);
}

//SLOT
void TTTTreeStyleEditor::setMovieCCT (int _thickness)
{
	styleSheet.cellCircleThickness = _thickness;
}

//SLOT:
void TTTTreeStyleEditor::setMovieCPC()
{
	styleSheet.cellPathColor = QColorDialog::getColor (Qt::red, this);
	pbtMovieCellPathColor->setPaletteBackgroundColor (styleSheet.cellPathColor);
}

//SLOT
void TTTTreeStyleEditor::setMovieCNFS (int _size)
{
	styleSheet.cellNumberFontSize = _size;
}

//SLOT
void TTTTreeStyleEditor::setMovieWBC()
{
        styleSheet.wavelengthBoxColor = QColorDialog::getColor (Qt::red, this);
        pbtMovieWavelengthBoxColor->setPaletteBackgroundColor (styleSheet.wavelengthBoxColor);
}

//SLOT
void TTTTreeStyleEditor::setMovieWBFS (int _size)
{
        styleSheet.wavelengthBoxFontSize = _size;
}

//SLOT:
void TTTTreeStyleEditor::setMovieCPT (int _thickness)
{
	styleSheet.cellPathThickness = _thickness;
}

//SLOT
void TTTTreeStyleEditor::setMovieCP_TI (int _time_interval)
{
	styleSheet.cellPathTimeInterval = _time_interval;
}


//key association methods
//=======================

//SLOT:
void TTTTreeStyleEditor::showKeyPropertyFrame (bool _show)
{
	if (_show)
		fraKeyProperties->show();
	else
		fraKeyProperties->hide();
}

//SLOT:
void TTTTreeStyleEditor::changeKeyElement (const QString &_newKey)
{
	//store current element
	saveKeyElement();
	
	//display new element
	showKeyElement (_newKey);
}

void TTTTreeStyleEditor::saveKeyElement()
{
	if (tesKey.getKey()) {
		tesKey.setVisible (chkKeyVisible->isChecked());
		tesKey.setColor (0, pbtKeyColor->paletteBackgroundColor());
		tesKey.setPositionX (lieKeyPosition->text().toInt());
		tesKey.setSize (spbKeySize->value());
		tesKey.setSymbol (lieKeySymbol->text());
		styleSheet.setAssociatedElement (tesKey.getKey(), tesKey);
	}
}

void TTTTreeStyleEditor::showKeyElement (const QString &_key)
{
	char key = _key.ascii()[0];
	tesKey = styleSheet.getAssociatedElement (key);
	
	if (tesKey.getKey()) {
		//display properties for currently chosen tree element
		chkKeyVisible->setChecked (tesKey.isVisible());
		showKeyPropertyFrame (tesKey.isVisible());
		pbtKeyColor->setPaletteBackgroundColor (tesKey.getColor (0));
		lieKeyPosition->setText (QString().setNum (tesKey.getPositionX()));
		spbKeySize->setValue (tesKey.getSize());
		lieKeySymbol->setText (tesKey.getSymbol());
	}
}



//various settings methods
//========================

void TTTTreeStyleEditor::showVariousSettings()
{
	chkAddUserSignToTTTFiles->setChecked (styleSheet.addUserSignToTTTFiles);
	chkUseNewTTTFileFolderStructure->setChecked (styleSheet.useNewTTTFileFolderStructure);
	spbExportWavelength->setValue (styleSheet.exportWavelength);
	chkLoadPicturesAsynchronous->setChecked (styleSheet.loadPicturesAsynchronous);
	chkShowTreeCountInPositionLayout->setChecked (styleSheet.showTreeCountInPositionLayout);
	chkUseColorsForAllTracks->setChecked (styleSheet.useColorsForAllTracks);
	spbNumPicsNeighborLoad->setValue(styleSheet.numPicsToLoadByNextPosItem);
	chkLoadNeighborTakeOverGamma->setChecked (styleSheet.takeOverDisplaySettingsNextPosItem);

	// QSettings dependent stuff (saved locally!)
	QSettings settings("tTt", "tTt");
	lieStatsExe->setText(settings.value("stats_path", QString()).toString());
	lieAmtExe->setText(settings.value("amt_path", QString()).toString());
}

//SLOT:
void TTTTreeStyleEditor::setAddUserSignToTTTFiles (bool _add)
{
	styleSheet.addUserSignToTTTFiles = _add;
}

void TTTTreeStyleEditor::setUseNewTTTFileFolderStructure (bool _use)
{
	styleSheet.useNewTTTFileFolderStructure = _use;
}

void TTTTreeStyleEditor::setUseExperimentGlobalTrees (bool /*_use*/)
{
	//styleSheet.
}

void TTTTreeStyleEditor::setExportWavelength (int _ewl)
{
	styleSheet.exportWavelength = _ewl;
}

void TTTTreeStyleEditor::setLoadPicturesAsynchronous (bool _use)
{
	styleSheet.loadPicturesAsynchronous = _use;
}

void TTTTreeStyleEditor::setShowTreeCountInPositionLayout (bool _use)
{
	styleSheet.showTreeCountInPositionLayout = _use;
}

void TTTTreeStyleEditor::setUseColorsForAllTracks  (bool _use)
{
	styleSheet.useColorsForAllTracks = _use;
}

//startup settings methods
//========================

void TTTTreeStyleEditor::showStartUpSettings()
{
	chkStartUpMoveToFirstFluor->setChecked (styleSheet.startUpMoveToFirstFluor);
	chkStartUpBgCircleVisible->setChecked (styleSheet.startUpBgCircleVisible);
	chkStartUpFullPicture->setChecked (styleSheet.startUpFullPicture);
	chkStartUpAlternateNormalBgTracking->setChecked (styleSheet.startUpAlternateNormalBgTracking);
}

void TTTTreeStyleEditor::setStartUpMoveToFirstFluor (bool _use)
{
	styleSheet.startUpMoveToFirstFluor = _use;
}

void TTTTreeStyleEditor::setStartUpBgCircleVisible (bool _use)
{
	styleSheet.startUpBgCircleVisible = _use;
}

void TTTTreeStyleEditor::setStartUpFullPicture (bool _use)
{
	styleSheet.startUpFullPicture = _use;
}

void TTTTreeStyleEditor::setStartUpAlternateNormalBgTracking (bool _use)
{
	styleSheet.startUpAlternateNormalBgTracking = _use;
}

void TTTTreeStyleEditor::setNumPicsToLoadByNextPosItem( int _val )
{
	if(_val > 0)
		styleSheet.setNumPicsToLoadByNextPosItem(_val);
}

void TTTTreeStyleEditor::setTakeOverDisplaySettingsNextPosItem( bool _val )
{
	styleSheet.setTakeOverDisplaySettingsNextPosItem(_val);
}

void TTTTreeStyleEditor::setShowOutOfSynxBox( bool _show )
{
	styleSheet.showOutOfSyncBox = _show;
}




//#include "ttttreestyleeditor.moc"
