/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef STATUSBAR_H
#define STATUSBAR_H

#include <qframe.h>
#include <qtimer.h>
#include <qstring.h>
#include <qevent.h>
#include <qpainter.h>
#include <qapplication.h>
#include <qbrush.h>
#include <qimage.h>
#include <qpixmap.h>

#include <qpushbutton.h>

#include <QDesktopWidget>

/**
@author Bernhard

	This class defines a flexible that can be inserted into any widget or displayed
	as an own window.
	The range can be specified, as with the step size, and optionally a timer can be
	triggered that increments the current status at a certain rate, or otherwise the
	update() method is called by the user, which increments the current status by one
	step.
*/

class StatusBar : public QFrame
{
Q_OBJECT
public:
	
	/**
	 * creates an empty status bar
	 * @param _parent: within this widget (0 => own window) the bar is displayed and fitted by size
	 */
	StatusBar (QWidget *_parent);
	
	///creates a new status bar 
	///@param _parent: within this widget (0 => own window) the bar is displayed and fitted by size
	///@param _caption: the caption (optional), displayed within the widget
	///@param _min: the minimum value (default == 0)
	///@param _max: the maximum value (default == 100)
	///@param _step: the step size (increase value) (default == 1)
	StatusBar (QWidget *_parent, QString _caption, uint _min = 0, uint _max = 100, uint _step = 1);
	
	~StatusBar();
	
        void resizeEvent (QResizeEvent *);

	///inits the status bar
	///@param _caption: the caption (optional), displayed within the widget
	///@param _min: the minimum value (default == 0)
	///@param _max: the maximum value (default == 100)
	///@param _step: the step size (increase value) (default == 1)
	void init (QString _caption, uint _min = 0, uint _max = 100, uint _step = 1);
	
	///sets the position and size of the widget (within its parent or on the screen, if toplevel)
	///@param _x: the x position
	///@param _y: the y position
	///@param _w: the width
	///@param _h: the height
	void setPosition (int _x, int _y, int _w, int _h);
	
	///set the bounds for the value
	///@param _min: the minimal value
	///@param _max: the maximal value
	void setBounds (uint _min, uint _max);
	
	void paintEvent (QPaintEvent *);
	
	///displays or hides the status bar
	///@param _show: true => bar is displayed, false => bar is hidden (but not deleted)
	void display (bool _show = true);
	
	///starts a timer that updates the status
	///@param _interval: the update interval in milliseconds
	///@param _steps: the number of steps that should be added
	void startTimer (uint _interval, uint _steps = 1);
	
	///updates the current status with _steps steps
	///@param _steps: the number of steps (defined in the constructor) that should be added
	///		      if == 0, then the last provided value is used
	///@param _caption: the new caption that should be displayed (if == "", nothing is updated)
        void advance (uint _steps = 0, QString _caption = "");
	
	/**
	 * resets the status bar; if a new step/caption is given, the old one is overwritten
	 * @param _caption: the caption (optional), displayed within the widget
	 * @param _step: the step size (increase value) (default == 1)
	 */
	void reset (QString _caption = "xxx", int _step = -1);
	
	/**
	 * sets the geometry of the status bar
	 * NOTE a call to this function is normally not necessary, as it is embedded into a parent and takes its geometry or is a top-level widget with predefined fixed geometry
	 * @param _left left edge
	 * @param _top top edge
	 * @param _width the width
	 * @param _height the height
	 */
	void setSBGeometry (int _left, int _top, int _width, int _height);
	
	
private slots:

	///increase the status
	void inc();
	
	///paints the bar
	void paintBar (bool _noCaption = false);

private:
	
	///holds the current status
	uint value;
	
	///the step size
	uint stepSize;
	
	///the value range
	uint minValue;
	uint maxValue;
	
	///the number of steps that should be added (both by the timer and the user function)
	uint updateSteps;
	
	///the caption displayed in the widget
	QString captionLine;
	
	///the update timer
	QTimer tmrUpdate;

        QImage bufferImage;
};

#endif
