/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "tttmainwindow.h"

// Project includes
#include "tttbackend/tttmanager.h"
#include "tttdata/userinfo.h"
#include "tttdata/systeminfo.h"
#include "tttio/xmlhandler.h"
#include "tttbackend/tools.h"
#include "tttpositionlayout.h"
//#include "tttbackend/positionmanagervector.h"
//#include "tttpositionstatistics.h"
#include "tttio/fastdirectorylisting.h"
#include "tttbackend/changecursorobject.h"

// Qt includes
#include <QCloseEvent>
#include <QMessageBox>
#include <QDebug>
#include <QFileDialog>

// Qt3 includes
// #include <Q3FileDialog>



TTTMainWindow::TTTMainWindow(QWidget *parent, const char *name)
    : QWidget(parent, name)
{
	setupUi (this);

	// Fast load is currently not supported
	pbtFastLoad->setVisible(false);

	checkNasInLoadExperiment = true;

	connect ( pbtCancelLoading, SIGNAL (clicked()), this, SLOT (close()));
	connect ( pbtLoadExperiment, SIGNAL (clicked()), this, SLOT (loadExperiment()));
	connect ( cboDrives, SIGNAL (activated (const QString&)), this, SLOT (selectDrive (const QString&)));
	connect ( lvwDirectories , SIGNAL (itemDoubleClicked (QListWidgetItem*)), this, SLOT (handleDirectory(QListWidgetItem*)));
	connect ( lvwDirectories , SIGNAL (itemPressed (QListWidgetItem*)), this, SLOT (handleDirectory(QListWidgetItem*)));
	connect ( pbtSetNAS, SIGNAL (clicked()), this, SLOT (selectNASDrive()));
	connect ( pbtFastLoad, SIGNAL (clicked()), this, SLOT (loadExperiment()));
	connect ( lieCurrentNas, SIGNAL(textEdited(const QString&)), this, SLOT(lieNasDriveTextEdited(const QString&)));
	connect ( pbtHelp, SIGNAL(clicked()), this, SLOT(showHelp()));
	
	chkNotUseNewPositions->setChecked (false);
	
	//connect ( chkLoadOnlyTTTFiles, SIGNAL (clicked()
        TTTManager::getInst().setNASDrive("");

	//displayDirectories (true);

	windowLayoutLoaded = false;

	// Hide unused controls (functionality may be broken - make sure to test when re-enabling)
	chkNotUseNewPositions->hide();
	chkLoadOnlyTTTFiles->hide();
}

TTTMainWindow::~TTTMainWindow()
{	
	// Cleanup
	UserInfo::cleanup();
}

void TTTMainWindow::loadNASDrive()
{
    //set default NAS drive - order:
    //1) read it from the configuration file, where the last NAS is stored
    //2) if this does not work, set the default NAS
    QString nasPath = Tools::extractValueFromDOMDocument (TTTManager::getInst().getTTTConfigDocument(), "NASPath");
    if (! nasPath.isEmpty())
            TTTManager::getInst().setNASDrive (nasPath);
    else 
            TTTManager::getInst().setNASDrive (SystemInfo::defaultNAS());

    displayNASDirectory();

    //also load the last set directory
    loadLastDirectory();

	checkForExampleExperimentInNasTopFolder();
}

//EVENT:
void TTTMainWindow::showEvent (QShowEvent *_ev)
{
	if(!_ev->spontaneous() && !windowLayoutLoaded) {
		// Load window layout
		UserInfo::getInst().loadWindowPosition(this, "TTTMainWindow");

		windowLayoutLoaded = true;
	}

	_ev->accept();
}


//EVENT:
void TTTMainWindow::closeEvent (QCloseEvent *_ev)
{
	// Save window layout
	UserInfo::getInst().saveWindowPosition(this, "TTTMainWindow");

	TTTManager::getInst().frmPositionLayout->close();

    //save tTt config document
    XMLHandler::writeDOMDocument (TTTManager::getInst().getTTTConfigPath(), TTTManager::getInst().getTTTConfigDocument());

	_ev->accept();
}

void TTTMainWindow::selectDrive (const QString& _drive)
{
	//set current directory
	currentQDir = _drive;
	
	//display subfolders
	displayDirectories (false);
}

//SLOT: implements double click action on a directory in lvwDirectories
void TTTMainWindow::handleDirectory(QListWidgetItem *direcLVI)
{
	//if the user clicked  within the empty space, nothing is done
	if (! direcLVI)
		return;
		
	if (direcLVI->text() == "..") 
		currentQDir.cdUp();
	else
		currentQDir.cd (direcLVI->text());
	
	
	//if this path is not yet in the combobox, add it
	int i = 0;
	for (; i < cboDrives->count(); i++)
		if (cboDrives->text (i) == currentQDir.path()) 
			break;
	if (i == cboDrives->count()) 	// not yet in the list
		cboDrives->insertItem (currentQDir.path());
	cboDrives->setCurrentItem (i);
	
	displayDirectories();
	
}

void TTTMainWindow::loadExperiment(const QString& overwritePath)
{
	// Release version check of NAS folder
	if(checkNasInLoadExperiment) {
		QString nasFolder = TTTManager::getInst().getNASDrive();
		QString nasFolderFixed = checkNasFolder(nasFolder);
		if(nasFolderFixed != nasFolder) {
			TTTManager::getInst().setNASDrive(nasFolderFixed);
			Tools::setValueInDOMDocument (TTTManager::getInst().getTTTConfigDocument(), "NASPath", nasFolderFixed);
			displayNASDirectory();
		}
	}

	/*
	// Check if NAS folder looks good (make sure user did not select the TTTFiles subfolder)
	QString nasFolder = TTTManager::getInst().getNASDrive();
	nasFolder = QDir::fromNativeSeparators(nasFolder);
	if(nasFolder.right(1) == "/")
		nasFolder = nasFolder.left(nasFolder.length()-1);
	if(nasFolder.lastIndexOf("/") >= 0) {
		QString folder = nasFolder.mid(nasFolder.lastIndexOf("/")+1);
		if(folder.toUpper() == "TTTFILES") {
			QString msg = QString("Your NAS setting seems to be invalid:\n\n%1\n\nThe TTTfiles directory is normally a subfolder of the NAS folder - usually the NAS folder is 'X:/' or 'S:/group/schroeder/Timelapse'.\n\nAre you sure to continue with this setting?").arg(nasFolder);
			if(QMessageBox::warning(this, "Warning", msg, QMessageBox::Yes | QMessageBox::No) != QMessageBox::Yes)
				return;
		}
	}
	*/
	

	// Set wait cursor
	ChangeCursorObject c;

	// Selected folder, unless overwritePath is set
	QString folder;
	if(overwritePath.isEmpty())
		folder = currentQDir.path();
	else
		folder = overwritePath;

//	bool statistics = (sender() == pbtStatistics); // ---------- Konstantin ---------------	
	TTTManager::getInst().setStatisticsMode (false); // ---------- Konstantin ---------------	
	
	bool fastLoad = (sender() == pbtFastLoad);
	
	if (folder.find (POSITION_MARKER) > 0) {
		//this is already a position folder
		//=> not selectable; message is displayed
		
		QMessageBox::information (this, "Loading not possible", "You selected a position folder instead of an experiment folder.\nPlease select the superfolder of the current one.", QMessageBox::Ok);
		
	}
	else {
		
		//read in subdirectories and create position managers by using them
		
		bool experimentFolder = false;
		
		QString expBasename = folder.mid (folder.findRev ('/') + 1);

		qDebug() << "Opening experiment: " << expBasename;

        QVector<QString> dirs = SystemInfo::listFiles (folder/*currentQDir.path()*/, QDir::Dirs);

		int positionCount = 0;
		
		QString tttFolder = "";
		QDir nas (TTTManager::getInst().getNASDrive());
		
		if (! nas.exists()) {
			TTTManager::getInst().quitProgram ("Network not mounted.\nPlease mount the NAS drive.\nNow tTt will close.");
			return;
		}
		
		//tttFolder = nas.absPath();
		tttFolder = SystemInfo::getTTTFileFolderFromBaseName (expBasename + "_p000", false);
		
		//remove "000" position
		tttFolder = tttFolder.left (tttFolder.findRev ("/", -3));
		
		TTTManager::getInst().setLoadTTTFilesOnly (chkLoadOnlyTTTFiles->isOn());
		
		//stored for usage in picture size gaining (see below); can be any position
		TTTPositionManager *anyTTTPM = 0;

		// If we have already stored the number of digits for positions, wls and z-indexes
		bool setNumOfDigits = false;
		
        for (QVector<QString>::iterator iter = dirs.begin(); iter != dirs.end(); ++iter) {
			
			if ((*iter).find (POSITION_MARKER) > 0) {
				//parse position index from directory name
				QString index = (*iter);

				//index = index.mid (index.find (POSITION_MARKER) + 2, 3);
				index = Tools::getPositionNumberFromString(index);

				// Store number of digits
				if(!setNumOfDigits) {
					setNumOfDigits = true;
					
					// Position indexes
					Tools::setNumOfPositionDigits(index.length());
					
					// List image files to determine other information
					QStringList fileExtensions;
					fileExtensions << ".png" << ".jpg" << ".tif";
					QStringList fileNames = FastDirectoryListing::listFiles(QDir(*iter), fileExtensions);

					// Find a valid image (i.e. with _p tag)
					QString sampleFile;
					for(int i = 0; i < fileNames.size(); ++i) {
						if(fileNames[i].contains("_p")) {
							sampleFile = fileNames[i];
							break;
						}
					}

					// If image was found, set other values
					if(!sampleFile.isEmpty()) {
						// Wl indexes
						int tmp = sampleFile.indexOf("_w");
						if(tmp >= 0) {
							int len = 1;
							tmp += 3;
							while(tmp < sampleFile.length() && sampleFile[tmp++].isDigit())
								++len;
							Tools::setNumOfWavelengthDigits(len);
						}

						// Z indexes
						tmp = sampleFile.indexOf("_z");
						if(tmp >= 0) {
							int len = 1;
							tmp += 3;
							while(tmp < sampleFile.length() && sampleFile[tmp++].isDigit())
								++len;
							Tools::setNumOfZDigits(len);
						}
						else
							Tools::setNumOfZDigits(0);
					}

				}
				
				positionCount++;
				
				TTTPositionManager *tttpm = new TTTPositionManager();
				if (tttpm) {
				
					if (! anyTTTPM)
						anyTTTPM = tttpm;
					
					QString basename = expBasename + POSITION_MARKER + index;

					tttpm->positionInformation.setIndex (index);
					tttpm->setPictureDirectory (folder + "/" + basename);
					tttpm->setBackgroundDirectory(folder + "/" + "background/" + basename);
					
					if (chkLoadOnlyTTTFiles->isOn())
						tttpm->setTTTFileDirectory (folder + "/" + basename);
					else
						tttpm->setTTTFileDirectory (tttFolder + "/" + basename);
					
					tttpm->setBasename (basename);
					
					TTTManager::getInst().addPositionManager (index, tttpm);
				}
				
				experimentFolder = true;
			}
		}
			
		if (experimentFolder) {
			//assume now that this is truly an experiment folder and open position layout window
			
			bool neglect_new_pos = chkNotUseNewPositions->isChecked();
			
			//read information of the TAT info file (XML) in the current directory, if present
			bool tatxml_read = false;
			if (! neglect_new_pos) {
				//BS 2010/02/25 new: the filename is no longer just TATexp.xml, but contains the experiment name as well
				//                   example: 090901PH2_TATexp.xml
				
				//thus, an attempt to read a file with the new name is started, if it fails, the old name is tried; if this fails, too, there is none
				tatxml_read = XMLHandler::readTATXML (folder/*currentQDir.path()*/ + "/" + expBasename + "_" + DEFAULT_TAT_FILENAME);
				
				if (! tatxml_read)
					//try old xml name
					tatxml_read = XMLHandler::readTATXML (folder/*currentQDir.path()*/ + "/" + DEFAULT_TAT_FILENAME);
			}
			
			if ((neglect_new_pos) || (! tatxml_read)) {
				//as there is no file that tells us the (absolute) locations of the positions, we have to create a layout by ourselves
				//thus, a grid is created: two positions per row
				
				std::vector<TTTPositionManager*> pmv = TTTManager::getInst().getAllPositionManagersSorted();
				//if (! pmv) {
				//	QMessageBox::information (this, "Loading not possible", "A severe problem occured.\nPlease contact your favorite programmer.", QMessageBox::Ok);
				//	close();
				//	return;
				//}
				
				int cc = 0;
				int x = 0, y = 0;
				
				//set base size in micrometer
				int base_size = (int)(WavelengthInformation::getMicrometerPerPixel (-1, -1, -1) * (float)BASE_PIXEL_WIDTH);
				
				for (int i = 0; i < pmv.size(); i++) {
					
					if (pmv[i]) {
						x = (cc % 2) * (base_size + 100);
						y = (cc / 2) * (base_size + 100);
						
						pmv[i]->positionInformation.setMeasures (x, y);
						cc++;
					}
				}
				
				//set size of pictures into wavelength information
				//for this we really need to read the first available position folder
				QImage img;
				for (int wl = 0; wl <= MAX_WAVE_LENGTH; wl++) {
					//find first picture file in folder

					// OH 04/27/2011: this code could fail as we can have 1 and 2 digit wavelengths and it is very slow
					/*
					QString pattern = QString ("*_w%1.").arg (wl);
					pattern = pattern + "jpg" + ";" + pattern + "tif";
                    QVector<QString> pics = SystemInfo::listFiles (anyTTTPM->getPictureDirectory(), QDir::Files, pattern, false);
                    QVector<QString>::const_iterator iterFiles = pics.constBegin();
					*/

					QStringList pattern;

					if(wl < 10) {
						// Check 2 digit and 1 digit
						pattern.append(QString("_w0%1.jpg").arg(wl));
						pattern.append(QString("_w%1.jpg").arg(wl));
						pattern.append(QString("_w0%1.tif").arg(wl));
						pattern.append(QString("_w%1.tif").arg(wl));
					}
					else {
						// 2 digits only
						pattern.append(QString("_w%1.jpg").arg(wl));
						pattern.append(QString("_w%1.tif").arg(wl));
					}

					// List files
					QStringList pics = FastDirectoryListing::listFiles(anyTTTPM->getPictureDirectory(), pattern);

					// Select some matching file..
					QStringList::const_iterator iterFiles = pics.constBegin();
					if (!pics.isEmpty() && iterFiles != pics.constEnd()) {
						const QString picfilename = anyTTTPM->getPictureDirectory() + '/' + *iterFiles;
						
						//load picture into memory and determine size
						if (img.load (picfilename))
							TATInformation::getInst()->setWavelengthInfo (wl, WavelengthInformation (wl, img.width(), img.height(), ""));
					}
					else {
						if(!TATInformation::getInst()->wavelengthInformationExistsFor(wl))
							TATInformation::getInst()->setWavelengthInfo (wl, WavelengthInformation (wl, -1, -1, ""));
					}
				}
				
				TTTManager::getInst().setUSE_NEW_POSITIONS (false);
			}
			else {
				
				//BS 2010/02/24: warning if a critical parameter is not set in the xml file
				//these include: ocular & tv factors, wavelength data section, position data section
				if (! TATInformation::getInst()->positionDataAvailable()) {
					QMessageBox::critical (this, "Loading problem", "The TATexp.xml file of this experiment seems to be corrupted (position data missing).\nTTT will close now.", "Ok...");
					close();
					return;
				}
				if (! TATInformation::getInst()->factorsRead() && !TATInformation::getInst()->microMeterPerPixelRead()) {
					QMessageBox::warning (this, "Loading problem", "The TATexp.xml file of this experiment seems to be corrupted (Objective and TV magnification missing, but micrometer per pixel not directly specified).\nPlease set Objective and TV magnification factor manually (here in tTt, not the XML), or otherwise your trees will be corrupt!", "I will do.");
				}
				
				if (! TATInformation::getInst()->wavelengthInfoAvailable()) {
					//the wavelength section is missing in the TAT file (old version) -> manually read picture sizes
					//set size of pictures into wavelength information
					//for this we really need to read the first available position folder
					QImage img;
					for (int wl = 0; wl <= MAX_WAVE_LENGTH; wl++) {
						//find first picture file in folder
						QString pattern = QString ("*_w%1.").arg (wl);
						pattern = pattern + "jpg" + ";" + pattern + "tif";
                        QVector<QString> pics = SystemInfo::listFiles (anyTTTPM->getPictureDirectory(), QDir::Files, pattern, false);
                        QVector<QString>::const_iterator iterFiles = pics.constBegin();
						
						if (iterFiles != pics.constEnd()) {
							const QString picfilename = *iterFiles;
							
							//load picture into memory and determine size
							if (img.load (picfilename))
								TATInformation::getInst()->setWavelengthInfo (wl, WavelengthInformation (wl, img.width(), img.height(), ""));
						}
						else
							TATInformation::getInst()->setWavelengthInfo (wl, WavelengthInformation (wl, -1, -1, ""));
					}
				}
				
				TTTManager::getInst().setUSE_NEW_POSITIONS (true);
				
				
				//delete positions that do not exist in the TAT file but as folders on the disk
				//otherwise some positions can fully overlap each other, and then the lower ones are not accessible
				//NOTE: a real deletion does not take place, rather a branding of the position
				QHash<int, TTTPositionManager*>& allPositions = TTTManager::getInst().getAllPositionManagers();
				for (auto iter = allPositions.begin(); iter != allPositions.end(); ++iter) {
					if (! (*iter)->positionInformation.coordinatesSet()) {
						//position not found in the TAT file
						(*iter)->setAvailability (false);
					}
				}
				
			}

//			if (!statistics){
			
				//add all positions to position statistic window
				//TTTManager::getInst().frmPositionStatistics->showPositions();
				
				//load experiment comment (displayed in statistics window)
				//the comment is in the ttt files folder, named "experiment basename".comment
				QString comment_filename = tttFolder + "/" + expBasename + ".comment";
				TTTManager::getInst().setExperimentCommentFilename (comment_filename);
				//QString comment = Tools::readTextFile (comment_filename);
				//TTTManager::getInst().frmPositionStatistics->txtComment->setText (comment);
				
				TTTManager::getInst().frmPositionLayout->setExperimentQDir (folder/*currentQDir*/, positionCount, fastLoad);
				if (TATInformation::getInst()->isMicroscopeFactorSet()) {
					TTTManager::getInst().frmPositionLayout->setFactors (TATInformation::getInst()->getOcularFactor(), TATInformation::getInst()->getTVAdapterFactor());
				}
				TTTManager::getInst().frmPositionLayout->show();
				hide();
				QApplication::processEvents();

				TTTManager::getInst().frmPositionLayout->checkIfLogFilesExist();
//			}
// 			else {
// 				TTTManager::getInst().frmStatistics->showPositions();
// 				TTTManager::getInst().frmStatistics->show();
// 				hide();
// 			}
		}
		else {
			//the current folder is most probably no experiment folder (no position folders present)
			QMessageBox::information (this, "Loading not possible", "The current folder is possibly no experiment folder, as it contains no position folders.\nPlease select another folder.", QMessageBox::Ok);
		}
	}
}

////SLOT:
//void TTTMainWindow::loadStatistics()
//{
//	TTTManager::getInst().setStatisticsMode (true);
//	
//	QString folder = currentQDir.path();
//	
//	bool experimentFolder;
//	bool oneExperimentFolderFound = false;
//
//	if (folder.find (POSITION_MARKER) > 0) {
//		//this is already a position folder
//		//=> not selectable; message is displayed
//		
//		QMessageBox::information (this, "Loading not possible", "You selected a position folder as experiment folder.\nThis is not possible; please select the superfolder of the current one.", QMessageBox::Ok);
//		
//	}
//	else {
//	
//		QString expBasename = folder.mid (folder.findRev ('/') + 1);
//		
//                QVector<QString> dirs = SystemInfo::listFiles (currentQDir.path(), QDir::Dirs); // dirs[0] is currentQDir; dirs[1] is up directory of currentQDir; from dirs[2] are directories in currentQDir
//		
//		///@ todo ask for default folder here!
//		
//		QDir nas (TTTManager::getInst().getNASDrive());
//		
//		if (! nas.exists()) {
//			QMessageBox::critical (this, "tTt panic", "Network not mounted.\nPlease mount the NAS drive.\nNow tTt will close.", "Ok");
//			QApplication::exit (1);
//			return;
//		}
//	
//		if ( dirs[3].find(POSITION_MARKER) > 0 ) {
//			QMessageBox::information (this, "Loading not possible", "You selected a experiment folder.\nThis is not possible; please select the superfolder of the current one.", QMessageBox::Ok);
//		}
//		else {
//                        for (int expDir = 2; expDir < dirs.size(); expDir ++) {
//				QDir d = dirs[expDir];
//		
//                                QVector<QString> posDirs = SystemInfo::listFiles (d.path(), QDir::Dirs); // posDirs[0] is d; posDirs[1] is up directory of d; from posDirs[2] are directories in d
//			
//				if ( posDirs.size() > 2 ) {
//					experimentFolder = false;
//                                        for (int i = 2; i < posDirs.size(); i++) {
//						if (posDirs[i].find (POSITION_MARKER) > 0) {
//							experimentFolder = true;
//							oneExperimentFolderFound = true;
//							break;
//						}
//					}
//					if (experimentFolder) {
//						TTTManager::getInst().frmStatistics->addExperimentFolder(dirs[expDir]);
//					}
//				}
//			}
//		
//			if ( ! oneExperimentFolderFound ) {
//				//the current folder is most probably no experiment folder (no position folders present)
//				QMessageBox::information (this, "Loading not possible", "The current folder is possibly no experiment folder, as it contains no position folders.\nPlease select another folder.", QMessageBox::Ok);
//			}
//			else {
//				TTTManager::getInst().frmStatistics->showExperiments();
//// 				TTTManager::getInst().frmStatistics->showPositions();
//				TTTManager::getInst().frmStatistics->show();
//				hide();
//			}
//		}
//	}
//}


//SLOT:
void TTTMainWindow::selectNASDrive()
{
	QString tmp = QFileDialog::getExistingDirectory (this, "Select the TTTExport directory", TTTManager::getInst().getNASDrive() );
	if (! tmp.isEmpty()) {
		tmp = QDir::fromNativeSeparators(tmp);
		if(tmp.right(1) != "/")
			tmp += "/";

		tmp = checkNasFolder(tmp);

		TTTManager::getInst().setNASDrive (tmp);
		displayNASDirectory();

		//set chosen NAS drive in config xml
		Tools::setValueInDOMDocument (TTTManager::getInst().getTTTConfigDocument(), "NASPath", tmp);

		checkForExampleExperimentInNasTopFolder();
	}
}

//SLOT:
void TTTMainWindow::lieNasDriveTextEdited(const QString& newFolder)
{
	// Folder is not automatically checked when editing line edit directly -> must be checked when loading experiment
	checkNasInLoadExperiment = true;

	QString dir = QDir::fromNativeSeparators(newFolder);
	if(!dir.isEmpty()) {
		if(dir.right(1) != "/")
			dir += "/";

		TTTManager::getInst().setNASDrive (dir);

		//set chosen NAS drive in config xml
		Tools::setValueInDOMDocument (TTTManager::getInst().getTTTConfigDocument(), "NASPath", dir);
		
		//change text color to read if folder does not exist and disable load experiment button
		if (! QDir (dir).exists()) {
			lieCurrentNas->setStyleSheet("color:#ff0000");
			pbtLoadExperiment->setDisabled(true);
		}
		else {
			lieCurrentNas->setStyleSheet("color:#000000");
			pbtLoadExperiment->setDisabled(false);

			checkForExampleExperimentInNasTopFolder();
		}
	}
}

void TTTMainWindow::displayNASDirectory()
{
    //display the currently selected NAS drive
    //if it exists, write it in black; if not, write it in red

    QString dir = TTTManager::getInst().getNASDrive();

	lieCurrentNas->setText(dir);		
    if (dir.isEmpty() || !QDir(dir).exists()) {
        //lblCurrentNAS->setText ("TTTWorkFolder: <font color=\"#ff0000\">" + dir + "</font>");          //red
		lieCurrentNas->setStyleSheet("color:#ff0000");
		pbtLoadExperiment->setDisabled(true);
	}
	else {
		//lblCurrentNAS->setText ("TTTWorkFolder: <font color=\"#000000\">" + dir + "</font>");          //black
		lieCurrentNas->setStyleSheet("color:#000000");
		pbtLoadExperiment->setDisabled(false);
	}
}

//preselects the last drive and directory that was selected when the program was quit
//if this does not exist, the application path is assumed
bool TTTMainWindow::loadLastDirectory()
{
        //set last directory
        //1) read it from the configuration file, where the last NAS is stored
        //2) if this does not work, set the default NAS
//        QString nasPath = Tools::extractValueFromDOMDocument (TTTManager::getInst().getTTTConfigDocument(), "NASPath");
//        if (! nasPath.isEmpty())
//                TTTManager::getInst().setNASDrive (nasPath);
//        else
//                TTTManager::getInst().setNASDrive (SystemInfo::defaultNAS());
//
//        displayNASDirectory();

	// -------- Oliver --------
	// Check if currentQDir is changed
	QDir tmp = currentQDir;

/*
	For release version: always start in home dir

#ifdef _DEBUG
	QString LastDirectory = "C:/LocalExperiments";
#else
    QString LastDirectory = "S:/group/Timelapse/Timelapsedata";
#endif
	if (! QFile::exists (LastDirectory))
		LastDirectory = SystemInfo::homeDirectory();
*/
	QString LastDirectory = SystemInfo::homeDirectory();

	currentQDir = LastDirectory;
	
	if (! currentQDir.exists()) {
		//currentQDir = kapp->applicationDirPath();
		currentQDir = qApp->applicationDirPath(); // ------------- Konstantin -----------------
	}

	// -------- Oliver --------
	// If currentQDir has changed, reload directories
	if(tmp != currentQDir)
		displayDirectories(true);
	
	return true;
}

//fills the views with the available drives and directories
void TTTMainWindow::displayDirectories (bool _loadDrives)
{
	//QFileInfo *fi;

	if (_loadDrives) {
		//put drives into cboDrives
		//CARE: root system in Linux!

		cboDrives->clear();

		cboDrives->insertItem (currentQDir.absolutePath());
		//qDebug() << currentQDir.path();

		const QFileInfoList roots = QDir::drives();
		QList<QFileInfo>::const_iterator iter = roots.constBegin();

		while (iter != roots.constEnd()) {
			cboDrives->addItem (iter->filePath());
			++iter;
		}
	}

	//put directories in currently selected folder into lvwDirectories
	lvwDirectories->clear();
	const QFileInfoList dirs = currentQDir.entryInfoList(QDir::Dirs);
	QList<QFileInfo>::const_iterator iterDir = dirs.constBegin();
	// QListWidgetItem *direcLVI = 0;
	QString d = "";

	//fi = iterDir;
	while (iterDir != dirs.constEnd()) {

		// direcLVI = new QListWidgetItem (lvwDirectories);
		d = iterDir->filePath();
		//take only the relative path
		d = d.right (d.length() - d.findRev ('/') - 1);
		// direcLVI->setText (0, d);//fi->filePath().right (fi->filePath().length() - 1));
		// lvwDirectories->insertItem ( direcLVI );
		lvwDirectories->addItem(d);
		++iterDir;
	}

	//set index to top
	// if (lvwDirectories->childCount()) 
		// lvwDirectories->setSelected (lvwDirectories->firstChild(), true);
}

void TTTMainWindow::showHelp()
{
	QMessageBox msgBox;
	msgBox.setWindowTitle("tTt Help - Select Folders");
	const char* txt = "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:14pt;\">Select Folders</span></p>"
		"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>"
		"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">TTTWorkFolder:</span> analysis results are loaded from and saved to this folder. Cell lineage tress (file extension .ttt), for example, are stored in the TTTfiles subfolder (folder structure: TTTWorkFolder\\TTTFiles\\[Year]\\[Experiment]\\[Position]). tTt stores analysis results separately from the experiment data (e.g. on a shared network drive) to allow further analysis without access to the image data (this is particularly useful e.g. when experiment data is stored on external HDDs).</p>"
		"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600;\"><br /></p>"
		"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">Experiment folder:</span> the folder of the experiment you want to analyze. This folder contains position sub-folders with the acquired images. </p>"
		"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>"
		"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Example: <br />TTTWorkFolder: <span style=\" font-weight:600;\">C:\\Data\\TTTWorkFolder</span> <br />Experiment folder: <span style=\" font-weight:600;\">C:\\Data\\TTTimageData\\111115AF6</span></p>";
	msgBox.setText(txt);
	msgBox.exec();
}

void TTTMainWindow::checkForExampleExperimentInNasTopFolder()
{
	// Get nas drive and make sure it exists
	QString curNas = TTTManager::getInst().getNASDrive();
	QDir curNasQDir(curNas);
	if(curNas.isEmpty() || !curNasQDir.exists())
		return;

	// If nas is called "TTTWorkFolder" and top folder contains "TTTimageData/111115AF6", then select "TTTimageData/111115AF6" as experiment folder
	if(curNasQDir.dirName() == "TTTWorkFolder") {
		curNasQDir.cdUp();
		if(curNasQDir.exists("TTTimageData")) {
			curNasQDir.cd("TTTimageData");
			if(curNasQDir.exists("111115AF6")) {
				// Change experiment folder and update sub folders
				curNasQDir.cd("111115AF6");
				currentQDir = curNasQDir;
				displayDirectories();

				// Select in or add to combobox
				int idx = cboDrives->findText(curNasQDir.absolutePath());
				if(idx >= 0)
					cboDrives->setCurrentIndex(idx);
				else {
					cboDrives->insertItem(curNasQDir.absolutePath());
					cboDrives->setCurrentIndex(cboDrives->count() - 1);
				}
			}
		}
	}
}

QString TTTMainWindow::checkNasFolder(const QString& nasFolder)
{
	if(nasFolder.isEmpty())
		return nasFolder;

	// Check if nasFolder is obviously wrong and if so, offer fix
	QDir nasFolderQDir(nasFolder);
	if(nasFolderQDir.dirName() == "TTTWorkFolder")
		// Ok
		return nasFolder;

	// Probably not ok
	QString nasFolderDirName = nasFolderQDir.dirName();
	if(nasFolderQDir.exists("TTTWorkFolder")) {
		// Offer to select child
		if(QMessageBox::question(this, "tTt Warning", QString("The selected <b>TTTWorkFolder</b> is probably invalid: tTt has detected that you have selected <i>%1</i>, i.e. the parent directory of <i>TTTWorkFolder</i>.<br><br>Do you want to select <i>TTTWorkFolder</i> instead (strongly recommended)?").arg(nasFolderDirName), QMessageBox::Yes | QMessageBox::No) != QMessageBox::No) {
			nasFolderQDir.cd("TTTWorkFolder");
			return nasFolderQDir.absolutePath();
		}
		else
			checkNasInLoadExperiment = false;
	}
	else {
		nasFolderQDir.cdUp();
		if(nasFolderQDir.dirName() == "TTTWorkFolder") {
			// Offer to select parent
			if(QMessageBox::question(this, "tTt Warning", QString("The selected <b>TTTWorkFolder</b> is probably invalid: tTt has detected that you have selected <i>%1</i>, i.e. a child directory of <i>TTTWorkFolder</i>.<br><br>Do you want to select <i>TTTWorkFolder</i> instead (strongly recommended)?").arg(nasFolderDirName), QMessageBox::Yes | QMessageBox::No) != QMessageBox::No) {
				return nasFolderQDir.absolutePath();
			}
			else
				checkNasInLoadExperiment = false;
		}
		else if(currentQDir.exists()) {
			// Check if experiment path top top folder contains a TTTWorkFolder
			QDir tmp = currentQDir;
			tmp.cdUp();
			tmp.cdUp();
			if(tmp.exists("TTTWorkFolder")) {
				if(QMessageBox::question(this, "tTt Warning", QString("The selected <b>TTTWorkFolder</b> is probably invalid: you have selected <i>%1</i>, but tTt has found a <i>TTTWorkFolder</i> in a top folder of your selected experiment (i.e. in <i>%2</i>).<br><br>Do you want to select <i>TTTWorkFolder</i> found by tTt instead (strongly recommended)?").arg(nasFolderDirName).arg(tmp.dirName()), QMessageBox::Yes | QMessageBox::No) != QMessageBox::No) {
					tmp.cd("TTTWorkFolder");
					return tmp.absolutePath();
				}
				else
					checkNasInLoadExperiment = false;
			}
		}
	}

	// No change wanted
	return nasFolder;
}


//#include "tttmainwindow.moc"
