/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "tttautotrackingtreewindow.h"

// Qt
#include <QCloseEvent>
#include <QFileDialog>

// STL
#include <algorithm>
#include <deque>
#include <cassert>

// Project
#include "tttdata/tree.h"
#include "tttbackend/tttmanager.h"
#include "tttbackend/tttpositionmanager.h"
#include "tttdata/userinfo.h"
#include "tttautotracking.h"
#include "tttbackend/changecursorobject.h"
#include "ttttracking.h"
#include "tttpositionlayout.h"
#include "tttbackend/picturearray.h"
#include "pictureview.h"
#include "tttmovie.h"
#include "multiplepictureviewer.h"
#include "autotrackingcellcircle.h"
#include "treewindowmenu.h"
#include "tttsplashscreen.h"
#include "tttbackend/tools.h"
#include "tttautotrackingconnectfragments.h"
#include "tttio/fastdirectorylisting.h"
#include "tttbackend/tttexception.h"
#include "treeviewtimeline.h"


// Debug information variable defined in main.cpp
extern int g_debugInformation;


TTTAutoTrackingTreeWindow::TTTAutoTrackingTreeWindow( TTTAutoTracking* _frmAutoTracking, QWidget *_parent )
	: QWidget(_parent)
{
	ui.setupUi(this);

	// Init variables
	treeModified = false;
	frmAutoTracking = _frmAutoTracking;
	oldTTTManagerTree = nullptr;
	oldTTTManagerTrack = nullptr;
	selectedTrack = nullptr;
	currentTimePoint = -1;
	m_connectTreesTool = nullptr;
	numExported = 0;
	yesToAllAtivated = false;
	noToAllAtivated = false;
	m_lastManualTrackingInAutoTrackingMode = false;
	m_fragmentsOverviewModel = 0;

	// Init tree view
	ui.treeView->setCellSelectionMode(true);
	int treeViewHeightFactor = std::max(1, UserInfo::get(UserInfo::KEY_TREEVIEW_HEIGHTFACTOR, 2).toInt());
	ui.spbHeightFactor->setValue(treeViewHeightFactor);
	connect(ui.spbHeightFactor, SIGNAL(valueChanged(int)), this, SLOT(treeViewHeightFactorChanged(int)));
	ui.spbTrackLineDistance->setValue(UserInfo::get(UserInfo::KEY_TREEVIEW_TRACKLINEDISTANCE, 1).value<int>());
	connect(ui.spbTrackLineDistance, SIGNAL(valueChanged(int)), this, SLOT(treeViewWidthFactorChanged(int)));
	ui.spbTrackLinesWidth->setValue(UserInfo::get(UserInfo::KEY_TREEVIEW_TRACKLINEWIDTH, 1).value<int>());
	connect(ui.spbTrackLinesWidth, SIGNAL(valueChanged(int)), this, SLOT(treeViewTrackLineWidthChanged(int)));
	ui.spbTimeLineFontSize->setValue(UserInfo::get(UserInfo::KEY_TREEVIEW_TIMELINEFONTSIZE, 1).value<int>());
	connect(ui.spbTimeLineFontSize, SIGNAL(valueChanged(int)), this, SLOT(timeLineFontSizeChanged(int)));
	ui.spbCellCircleRadius->setValue(UserInfo::get(UserInfo::KEY_TREEVIEW_CELLCIRCLERADIUS, 1).value<int>());
	connect(ui.spbCellCircleRadius, SIGNAL(valueChanged(int)), this, SLOT(treeViewCellCircleRadiusChanged(int)));
	if(ui.treeView->getTreeHeightFactor() != treeViewHeightFactor)
		ui.treeView->setTreeHeightFactor(treeViewHeightFactor);
	bool showRealTime = UserInfo::get(UserInfo::KEY_TREEVIEW_SHOWREALTIME, true).toBool();
	ui.chkRealTimeTimescale->setChecked(showRealTime);
	ui.treeView->setShowRealTimeInTimeScale(showRealTime);
	connect(ui.chkRealTimeTimescale, SIGNAL(toggled(bool)), this, SLOT(showRealTimeChanged(bool)));
	connect(ui.chkContinuousCellLines, SIGNAL(toggled(bool)), this, SLOT(continuousTrackLinesChanged(bool)));

	// Init state of tree editing assistant
	resetTreeEditingAssistant();

	// Z not implemented yet
	ui.lblAssistantZIndex->setVisible(false);
	ui.spbAssistantZ->setVisible(false);

	// Get current position and convert to int
	currentPosition = TTTManager::getInst().getCurrentPosition();
	if(currentPosition <= 0)
		currentPosition = -1;

	// Make right half of splitter not collapsible
	ui.splitter->setCollapsible(1, false);

	// Init menu
	menuBar = new TreeWindowMenu(this);
	menuBar->setGeometry(QRect(0, 0, 5000, 21));

	// Init statusbar
	statusBar = new QStatusBar(this);
	statusBar->setSizeGripEnabled(false);
	statusBar->setMaximumHeight(25);
	if(statusBar->layout()) {
		statusBar->layout()->setContentsMargins(8, 0, 8, 0);
		statusBar->layout()->setSpacing(3);
	}
	ui.laoMainLayout->addWidget(statusBar);

	// Backward tracking mode label
	lblBackwardTrackingMode = new QLabel("", statusBar);
	statusBar->addPermanentWidget(lblBackwardTrackingMode);
	updateBackwardTrackingModeDisplay();

	// Create status bar buttons
	pbtStatusBarOpenMovieWindow = new QPushButton("Movie Window", statusBar);
	pbtStatusBarOpenMovieWindow->setToolTip("Open movie window for current position");
	statusBar->addPermanentWidget(pbtStatusBarOpenMovieWindow);
	connect ( pbtStatusBarOpenMovieWindow, SIGNAL (clicked()), this, SLOT (openMovieWindow()));

	pbtStatusBarGoToFirstPos = new QPushButton("First Position of Tree", statusBar);
	pbtStatusBarGoToFirstPos->setToolTip("Go to position in which the current tree starts");
	pbtStatusBarGoToFirstPos->setEnabled(false);
	statusBar->addPermanentWidget(pbtStatusBarGoToFirstPos);
	connect ( pbtStatusBarGoToFirstPos, SIGNAL (clicked()), this, SLOT (goToFirstPosOfTree()));

	// Used fragments model
	usedFragmentsModel = new QStringListModel(this);
	ui.lvwUsedFragments->setModel(usedFragmentsModel);

	// Signals/ slots
	connect(ui.treeView, SIGNAL(closeTreeClicked(ITree*)), this, SLOT(closeTree(ITree*)));
	connect(ui.treeView, SIGNAL(saveTreeClicked(ITree*)), this, SLOT(saveTreeClicked(ITree*)));
	connect(ui.treeView, SIGNAL(treeLeftClicked(ITree*)), this, SLOT(treeLeftClicked(ITree*)));
	connect(ui.treeView, SIGNAL(cellSelected(ITree*, ITrack*)), this, SLOT(cellSelected(ITree*, ITrack*)));
	connect(ui.treeView, SIGNAL(cellDeSelected(ITree*, ITrack*)), this, SLOT(cellDeSelected(ITree*, ITrack*)));
	connect(ui.treeView, SIGNAL(timeLineClicked(int, Qt::MouseButton)), this, SLOT(timeLineClicked(int, Qt::MouseButton)));
	connect(ui.pbtDivision, SIGNAL(clicked()), this, SLOT(cellDivisionSelected()));
	connect(ui.pbtCellDeath, SIGNAL(clicked()), this, SLOT(cellDeathSelected()));
	connect(ui.pbtLost, SIGNAL(clicked()), this, SLOT(lostSelected()));
	connect(ui.pbtRemoveCellFate, SIGNAL(clicked()), this, SLOT(removeCellFate()));
	connect(ui.pbtTrackManually, SIGNAL(clicked()), this, SLOT(startManualTracking()));
	connect(ui.pbtCutTree, SIGNAL(clicked()), this, SLOT(cutTree()));
	//connect(ui.pbtLoadImages, SIGNAL(clicked()), this, SLOT(removeCellFate()));
	//connect(ui.pbtUnloadImages, SIGNAL(clicked()), this, SLOT(removeCellFate()));
	connect(ui.pbtApplySearchRange, SIGNAL(clicked()), this, SLOT(applySearchRange()));
	connect(ui.chkHideTreesStartingBeforeLastTP, SIGNAL(toggled(bool)), &TTTManager::getInst(), SLOT(redrawTracks()));
	connect(ui.sldTimepoint, SIGNAL(valueChanged(int)), this, SLOT(timePointSliderChanged(int)));
	connect(ui.pbtFirstTp, SIGNAL(clicked()), this, SLOT(imageLoadingSetFirstTp()));
	connect(ui.pbtLastTp, SIGNAL(clicked()), this, SLOT(imageLoadingSetLastTp()));
	connect(ui.pbtLoadImages, SIGNAL(clicked()), this, SLOT(loadImages()));
	connect(ui.pbtUnloadImages, SIGNAL(clicked()), this, SLOT(unloadImages()));
	connect(ui.pbtAssistantNext, SIGNAL(clicked()), this, SLOT(assistantSelectNextRegion()));
	connect(ui.pbtAssistantPrev, SIGNAL(clicked()), this, SLOT(assistantSelectPrevRegion()));
	connect(ui.pbtAssistantAccept, SIGNAL(clicked()), this, SLOT(assistantAcceptCurRegion()));
	connect(ui.pbtSwapCells, SIGNAL(clicked()), this, SLOT(swapCells()));
	connect(&TTTManager::getInst(), SIGNAL(timepointSet(int, int)), this, SLOT(setTimePoint(int, int)));
	connect(&TTTManager::getInst(), SIGNAL(currentPositionChanged(int, int)), this, SLOT(currentPositionChanged(int, int)));
	connect(&TTTManager::getInst(), SIGNAL(backwardTrackingModeSet(bool)), this, SLOT(updateBackwardTrackingModeDisplay()));
	connect(_frmAutoTracking, SIGNAL(loadedTreeFragmentsChanged()), this, SLOT(loadedTreeFragmentsChanged()));
	connect(ui.chkHideUsedFragments, SIGNAL(toggled(bool)), this, SLOT(showHideUsedTreeFragmentsInTable()));
	connect(frmAutoTracking, SIGNAL(treeFragmentUsedForTree(int)), this, SLOT(showHideUsedTreeFragmentsInTable()));
	connect(ui.tbvTreeFragmentsOverview, SIGNAL(doubleClicked(const QModelIndex&)), this, SLOT(treeFragmentDoubleClicked(const QModelIndex&)));
	connect(ui.pbtTest, SIGNAL(clicked()), this, SLOT(debugCreateTree()));

	// Init gui
	ui.grpAppendableTrees->setEnabled(false);

	// Init everything related to current position
	currentPositionChanged(-1, currentPosition);

	// Create wl buttons
	updateWlButtons();

	// Set current timepoint (i.e. timepoint in current position)
	TTTPositionManager* tttpm = TTTManager::getInst().getCurrentPositionManager();
	if(tttpm) {
		int curTP = tttpm->getTimepoint();
		if(curTP > 0)
			setTimePoint(curTP);
	}

	// Avoid scrollbars in wavelength box
	ui.tbtWls->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

	// Show fragments
	updateTreeFragmentsTable();
}

void TTTAutoTrackingTreeWindow::closeEvent( QCloseEvent *_event )
{
	// Close current tree first
	if(!closeAllTrees()) {
		// Deny closing of window
		_event->ignore();
		return;
	}

	// Accept event, closing window
	_event->accept();
}

void TTTAutoTrackingTreeWindow::addTree( QSharedPointer<Tree> _newCurTree, const QList<QSharedPointer<TreeFragment> >& _fragments, QString _fileName)
{
	// Convert separators
	if(!_fileName.isEmpty())
		_fileName = QDir::fromNativeSeparators(_fileName);

	// Check if tree is already displayed (note: this is only checked by comparing memory addresses of the corresponding Tree objects)
	if(getIndexOfTree(_newCurTree.data()) != -1) {
		// Just update display
		ui.treeView->redrawTree(_newCurTree.data());

		return;
	}

	// Create descriptor
	TreeDescriptor* tmp = new TreeDescriptor();
	QSharedPointer<TreeDescriptor> descr(tmp);

	// Set descriptor data
	descr->theTree = _newCurTree;
	descr->usedFragments = _fragments;

	// Split _fileName
	if(!_fileName.isEmpty()) {
		int i = _fileName.lastIndexOf('/');
		if(i == -1) {
			QMessageBox::warning(this, "Warning", QString("Invalid filename: %1").arg(_fileName));
		}
		else {
			descr->path = _fileName.left(i + 1);
			descr->fileName = _fileName.mid(i + 1);
		}
	}

	// Add it
	currentTrees.append(descr);

	// Display new tree
	ui.treeView->addTree(_newCurTree.data(), true, true);

	// Check if this is the only tree 
	if(currentTrees.size() == 1) {
		// Select tree and its root cell
		setSelectedTree(0);
		cellSelected(_newCurTree.data(), _newCurTree->getRootNode());
	}

	// Also set displayed Y range to max timepoint
	TTTPositionManager* tttpm = TTTManager::getInst().getCurrentPositionManager();
	if(tttpm) {
		int ltp = tttpm->getLastTimePoint();
		if(ltp > 0)
			ui.treeView->setMaxY(ltp);
	}

	// Update controls depending on number of trees
	updateControlsDependingOnNumTrees();

	//// Get current timepoint
	//int curTp = ui.pictureView->getCurrentTimePoint();
	//Track *root = _newCurTree->getRootNode();
	//if(curTp < 1 && root) {
	//	// Get first timepoint of new tree
	//	int firstTp = _newCurTree->getRootNode()->getFirstTrace();
	//	if(firstTp >= 1) 
	//		ui.pictureView->goToPositionAndTimePoint()
	//}

	
}

void TTTAutoTrackingTreeWindow::addNewTree()
{
	// Check current time point as this will be used for the first trace of the first track
	if(currentTimePoint <= 0) {
		QMessageBox::information(this, "Note", "Please select a valid timepoint first.");
		return;
	}

	// Create tree
	Tree* tmp = new Tree();
	QSharedPointer<Tree> newTree(tmp);

	// Create base track
	Track* root = new Track (0, 0, TTTManager::getInst().getBasePositionManager()->getLastTimePoint(), currentTimePoint);
	root->setNumber (1);

	// Add to tree
	newTree->insert(root);

	// Add to display
	addTree(newTree, QList<QSharedPointer<TreeFragment> >(), "");
}


bool TTTAutoTrackingTreeWindow::closeTree( ITree* _tree, bool closeWithoutAsking )
{
	// Try to find corresponding Tree pointer in list
	int index = getIndexOfTree(_tree);
	if(index == -1)
		return false;

	// Get Tree pointer
	QSharedPointer<Tree> tree = currentTrees[index]->theTree;

	// Check if tree is currently tracked manually
	if(TTTManager::getInst().isTracking()) {
		if(TTTManager::getInst().getTree() == _tree) {
			QMessageBox::information(this, "Note", "Stop manual tracking first.");
			return false;
		}
	}

	// Check if tree has been modified
	if(tree->getModified() && !closeWithoutAsking) {
		// Ask user to save changes
		yesToAllAtivated = 
			noToAllAtivated = false;
		if(!saveChangesToTree(index))
			return false;
	}

	// Check if tree is currently selected
	if(selectedTree == currentTrees[index]) {
		// Deselect
		setSelectedTree(-1);
	}

	// Tree can be closed
	ui.treeView->removeTree(tree.data());
	currentTrees.removeAt(index);

	// Update controls depending on number of trees
	updateControlsDependingOnNumTrees();

	return true;
}

void TTTAutoTrackingTreeWindow::saveTreeClicked( ITree* _tree )
{
	// Redirect
	saveTree(_tree, false);
}

bool TTTAutoTrackingTreeWindow::saveTree( ITree* _tree, bool _saveAs /*= false*/ )
{
	// Check if tree is currently tracked manually
	if(TTTManager::getInst().isTracking()) {
		if(TTTManager::getInst().getTree() == _tree) {
			QMessageBox::information(this, "Note", "Stop manual tracking first.");
			return false;
		}
	}

	// Try to find corresponding Tree pointer in list
	int index = getIndexOfTree(_tree);
	if(index == -1)
		return false;

	// Try to save
	return saveTreeInternal(index);
}

bool TTTAutoTrackingTreeWindow::closeAllTrees()
{
	// Check if currently manually tracking
	if(TTTManager::getInst().isTracking()) {
		QMessageBox::information(this, "Note", "Stop manual tracking first.");
		return false;
	}

	// Iterate over trees
	yesToAllAtivated = false;
	noToAllAtivated = false;
	for(int i = 0; i < currentTrees.size(); ++i) {
		// Check if tree has been modified
		if(currentTrees[i]->theTree->getModified()) {
			// Ask user to save changes
			bool yesNoToAll = i < currentTrees.size() - 1;
			if(!saveChangesToTree(i, yesNoToAll))
				return false;
		}
		
		// Close tree
		ui.treeView->removeTree(currentTrees[i]->theTree.data());
	}
	currentTrees.clear();

	// No tree selected (should be done before 
	setSelectedTree(-1);

	// Update controls depending on number of trees
	updateControlsDependingOnNumTrees();

	return true;
}

int TTTAutoTrackingTreeWindow::getIndexOfTree( ITree* _tree )
{
	// Look for _tree
	for(int i = 0; i < currentTrees.size(); ++i) {
		if((ITree*)currentTrees[i]->theTree.data() == _tree)
			return i;
	}

	// Not found
	return -1;
}

bool TTTAutoTrackingTreeWindow::saveChangesToTree( int _index, bool _yesAndNoToAllButton )
{
	// Get descr
	if(_index < 0 || _index >= currentTrees.size())
		return false;
	TreeDescriptor* descr = currentTrees[_index].data();

	// Check if user already clicked yes to all or no to all
	if(yesToAllAtivated)
		return saveTreeInternal(_index);
	else if(noToAllAtivated) 
		return true;

	// Ask user if changes should be discared/saved/whatever
	QString treeName = descr->fileName;
	if(treeName.isEmpty())
		treeName = "(new tree)";
	QString msg = QString("The tree '%1' has been modified.\n\nSave changes before closing?").arg(treeName);
	QMessageBox messageBox;
	messageBox.setCaption("Save changes?");
	messageBox.setText(msg);

	// Add buttons
	QPushButton* pbtYesToAll = nullptr;
	QPushButton* pbtNoToAll = nullptr;
	QPushButton* pbtYes = messageBox.addButton("Yes", QMessageBox::ActionRole);
	if(_yesAndNoToAllButton) 
		pbtYesToAll = messageBox.addButton("Yes to All", QMessageBox::ActionRole);
	QPushButton* pbtNo = messageBox.addButton("No", QMessageBox::ActionRole);
	if(_yesAndNoToAllButton) 
		pbtNoToAll = messageBox.addButton("No to All", QMessageBox::ActionRole);
	QPushButton* pbtCancel = messageBox.addButton("Cancel", QMessageBox::ActionRole);
	
	// Show message box
	messageBox.exec();

	// Check which button was used
	if(messageBox.clickedButton() == pbtNo) {
		// Discard changes
		return true;
	}
	else if(messageBox.clickedButton() == pbtNoToAll) {
		// Discard changes
		noToAllAtivated = true;
		return true;
	}
	else if(messageBox.clickedButton() == pbtYes) {
		// Save changes
		return saveTreeInternal(_index);
	}
	else if(messageBox.clickedButton() == pbtYesToAll) {
		// Save changes
		yesToAllAtivated = true;
		return saveTreeInternal(_index);
	}
	else {
		// Cancel
		return false;
	}
}

bool TTTAutoTrackingTreeWindow::saveTreeInternal( int _index, bool _askForFilename /*= false*/ )
{
	// Check index
	if(_index < 0 || _index >= currentTrees.size())
		return false;

	// Get descriptor
	TreeDescriptor* descr = currentTrees[_index].data();

	// Get position manager of position where tree starts
	TTTPositionManager* tttpm = 0;

	// Get first trackpoint
	TrackPoint* firstTrackPt = 0;
	Track* motherTrack = descr->theTree->getBaseTrack();
	if(motherTrack) {
		int firstTimePt = motherTrack->getFirstTimePoint();
		if(firstTimePt > 0)
			firstTrackPt = motherTrack->getTrackPointByTimePoint(firstTimePt);
	}

	// Get position
	if(firstTrackPt) {
		// Get position manager
		const QString pos = firstTrackPt->Position;
		if(!pos.isEmpty()) {
			// Get folder
			tttpm = TTTManager::getInst().getPositionManager(pos);
		}
	}

	// If tttpm is NULL, set it to current position
	tttpm = TTTManager::getInst().getCurrentPositionManager();
	if(!tttpm)
		return false;

	// Get filename
	QString fileName;
	if(descr->path.isEmpty() || descr->fileName.isEmpty() || _askForFilename) {
		// Get suggested file / folder
		QString suggestion;
		if(!descr->path.isEmpty()) {
			suggestion = descr->path + descr->fileName;
		}
		else {
			// Get ttt folder
			QDir tttFolder (tttpm->getTTTFileDirectory (true));

			// Get colony number
			QString colNum = TTTManager::getInst().getNextFreeFile (tttpm, true);

			// Complete suggestion string
			suggestion = tttFolder.absPath() + '/' + tttpm->getBasename() + colNum;
			if (UserInfo::getInst().getStyleSheet().getAddUserSignToTTTFiles())
				suggestion += UserInfo::getInst().getUsersign();
		}

		// Get filename
		fileName = QFileDialog::getSaveFileName(this, "Save File", suggestion, "tTt Files (*.ttt)");
		if(fileName.isEmpty())
			return false;

		// Check if file already exists
		if(QFile::exists(fileName)) {
			if(QMessageBox::question(this, "Overwrite?", "File already exists. Overwrite?", QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel) != QMessageBox::Yes)
				return false;
		}
	}
	else
		fileName = descr->path + descr->fileName;

	// If data has been changed..
	if(descr->theTree->getModified()) {
		// Append change log entry if it is not identical with the last change log entry
		QPair<QDate, QByteArray> changeEntry(QDate::currentDate(), UserInfo::getInst().getUsersign().toLatin1());

		// Check if it is identical and add it if not
		const QPair<QDate, QByteArray>* lastEntry = TTTManager::getInst().getTree()->getLastChangelogEntry();
		if(lastEntry == 0 || *lastEntry != changeEntry) {
			TTTManager::getInst().getTree()->appendToChangelog(changeEntry);
		}
	}

	// Try to save file
	if (tttpm->frmMovie)
		tttpm->getSymbolHandler().saveSymbolFile (fileName + "symbols");
	bool success = TTTFileHandler::saveFile (fileName, descr->theTree.data(), tttpm->getFirstTimePoint(), tttpm->getLastTimePoint());

	// Check if saving worked
	if(!success) {
		// Get system error
		QString sysError;
		char* lastSystemError = strerror(errno);
		if(lastSystemError)
			sysError = QString(lastSystemError);
		
		// Display error message
		QString msg = QString("Could not save file \"%1\".\nSystem reports: %2").arg(fileName).arg(sysError);
		QMessageBox::critical(this, "Saving failed.", msg);
	}

	// Get path and pure filename without path
	QString path = fileName.left(fileName.lastIndexOf('/') + 1);
	fileName = fileName.mid(fileName.lastIndexOf('/') + 1);

	// Update tree meta data
	if(success) {
		// Tree
		descr->theTree->setModified(false);
		descr->theTree->setFilename(fileName);

		// Descriptor
		descr->path = path;
		descr->fileName = fileName;

		// Display
		ui.treeView->updateDisplayName(descr->theTree.data());

		// Notify
		QString msg = QString("Tree saved successfully to '%1'").arg(path + fileName);
		statusBar->showMessage(msg, 3000);
	}

	// Save for fragments for which tree they were used
	bool errorSavingFragments = false;
	for(QList<QSharedPointer<TreeFragment> >::const_iterator it = descr->usedFragments.constBegin(); it != descr->usedFragments.constEnd(); ++it) {
		TreeFragment* curFr = it->data();

		// Set tree
		curFr->setAssociatedTree(fileName);
		
		// Save
		if(!curFr->writeAssociatedTreeToFile())
			errorSavingFragments = true;
	}

	// Notify if error
	if(errorSavingFragments) {
		QString msg = "An error occurred while trying to save which fragments have been used for this tree. One or more fragment files were not updated.";
		QMessageBox::warning(this, "Error", msg);
	}

	// Update info about selected tree if this one is selected
	if(selectedTree == currentTrees[_index])
		updateCurrentTreeInformation();

	//// Update TTTAutoTracking window
	//frmAutoTracking->updateDisplayOfUsedFragments();

	return success;
}

bool TTTAutoTrackingTreeWindow::setSelectedTree( int _index )
{
	// Update menu bar buttons
	menuBar->actFile_CLOSE_CURRENT_TREE->setEnabled(_index >= 0);
	menuBar->actFile_SAVE_CURRENT_TREE->setEnabled(_index >= 0);
	menuBar->actFile_SAVE_CURRENT_TREE_AS->setEnabled(_index >= 0);

	if(_index == -1) {
		// Make sure no track is selected ( important: must be done before selectedTree.clear(),
		// as this deletes all tracks, but they may be accessed by setSelectedCell() )
		setSelectedCell(nullptr);

		// Deselect tree
		selectedTree.clear();
	}
	else {
		// Select tree
		if(_index < 0 || _index >= currentTrees.size())
			return false;
		TreeDescriptor* descr = currentTrees[_index].data();
		selectedTree = currentTrees[_index];

		// Select in GUI
		ui.treeView->selectTree(descr->theTree.data());
	}

	// Update display info
	updateCurrentTreeInformation();

	return true;
}

void TTTAutoTrackingTreeWindow::updateCurrentTreeInformation()
{
	// No tree selected
	if(selectedTree.isNull()) {
		ui.lblSelectedTree->setText("-");
		usedFragmentsModel->setStringList(QStringList());
	}
	else {
		// Iterate over fragments to generate list
		QStringList usedFragmentsList;
		for(QList<QSharedPointer<TreeFragment> >::const_iterator it = selectedTree->usedFragments.begin(); it != selectedTree->usedFragments.end(); ++it) {
			// Get name of current fragment
			TreeFragment* curFrag = it->data();
			QString fileName = curFrag->getTreeName();
			if(fileName.isEmpty())
				fileName = "(N/A)";

			// Put in list
			usedFragmentsList.append(fileName);
		}

		// Set list
		usedFragmentsModel->setStringList(usedFragmentsList);

		// Get tree name
		QString treeName = selectedTree->fileName;
		if(treeName.isEmpty())
			treeName = "(new tree)";
		ui.lblSelectedTree->setText(treeName);
	}
}

void TTTAutoTrackingTreeWindow::treeLeftClicked( ITree* _tree )
{
	/*
	120422OH: Trees can now only be selected by selecting tracks.

	// Try to find corresponding Tree pointer in list
	int index = getIndexOfTree(_tree);
	if(index == -1)
		return;

	// Select tree
	setSelectedTree(index);
	*/
}

void TTTAutoTrackingTreeWindow::cellSelected( ITree* _tree, ITrack* _track )
{
	//// Do not allow if manual tracking
	//if(TTTManager::getInst().isTracking()) {
	//	QMessageBox::information(this, "Note", "Stop manual tracking first.");
	//	return;
	//}

	// If we are currently tracking manually, stop tracking
	if(TTTManager::getInst().isTracking()) {
		if(TTTPositionManager* tttpm = TTTManager::getInst().getCurrentPositionManager())
			tttpm->frmMovie->stopTracking(TS_NONE);
		else {
			QMessageBox::information(this, "Note", "Stop manual tracking first.");
			return;
		}
	}

	// Apply selection
	setSelectedCell(_track);

	
	// Go to first timepoint of selected cell and open movie window
	int firstTimePoint = -1;
	if(_track) 
		firstTimePoint = _track->getFirstTimePoint();
	if(firstTimePoint > 0) {
		setTimePoint(firstTimePoint);
		openMovieWindow();
	}
}


void TTTAutoTrackingTreeWindow::cellDeSelected( ITree* _tree, ITrack* _track )
{
	// Do not allow if manual tracking
	if(TTTManager::getInst().isTracking()) {
		QMessageBox::information(this, "Note", "Stop manual tracking first.");
		return;
	}

	// Check if _track is selectedTrack
	if(selectedTrack == _track) {
		// Deselect selected cell
		setSelectedCell(nullptr);
	}
}

bool TTTAutoTrackingTreeWindow::startManualTracking()
{
	if(!selectedTrack || selectedTree.isNull())
		return false;

	//// Make sure current track has no children
	//if(selectedTrack->hasChildren()) {
	//	QMessageBox::critical(this, "Error", "Track already has child tracks.");
	//	return;
	//}

	// Timepoint and position where tracking will start
	int timePoint = currentTimePoint;
	int position = TTTManager::getInst().getCurrentPosition();

	// Find trackpoint where tracking will start
	TrackPoint trackPoint = selectedTrack->getTrackPoint (timePoint);
	if(trackPoint.TimePoint == -1) {
		// Try last trace
		trackPoint = selectedTrack->getTrackPoint (selectedTrack->getLastTrace());
		if(trackPoint.TimePoint == -1) {
			// Try mother track
			Track *mother = selectedTrack->getMotherTrack();
			if(mother) {
				trackPoint = mother->getTrackPoint(mother->getLastTrace());
			}
		}
	}
	if(trackPoint.TimePoint != -1) {
		timePoint = trackPoint.TimePoint;
		position = trackPoint.Position.toInt();
	}

	// Get position manager
	TTTPositionManager* ttpm = TTTManager::getInst().getPositionManager(position);
	if(!ttpm) {
		QMessageBox::critical(this, "Error", "Error: Could not get position manager");
		return false;
	}

	// Save current tree in TTTTracking
	if(!TTTManager::getInst().frmTracking->SaveDialog())
		return false;

	// Change position
	if(!TTTManager::getInst().frmPositionLayout->setPosition(Tools::convertIntPositionToString(position))) {
		QMessageBox::critical(this, "Error", QString("Error: Could not select position of last trackpoint (%1)").arg(position));
		return false;
	}

	// Pre load images
	PictureArray* pics = ttpm->getPictures();
	int wl = std::max(ttpm->getWavelength(), 0);
	for(int curTp = timePoint - 10; curTp < timePoint + 50; ++curTp) {
		// Set loading flag
		pics->setLoading(curTp, 1, wl, true);
	}

	// Load images
	pics->loadPictures(false);

	// Remember tree in TTTManager
	oldTTTManagerTrack = TTTManager::getInst().getCurrentTrack();
	oldTTTManagerTree = TTTManager::getInst().getTree();

	// Set current tree in TTTManager
	Tree* theTree = selectedTree->theTree.data();
	TTTManager::getInst().setTree(theTree);

	// Mark tree as modified
	theTree->setModified(true);

	// Select current track
	TTTManager::getInst().setCurrentTrack(selectedTrack);

	// Update timescale in TTTTracking (updates TreeDisplay::Tree)
	int ftp = ttpm->getFirstTimePoint();
	int ltp = ttpm->getLastTimePoint();
	TTTManager::getInst().frmTracking->setTimePoints (ftp, ltp, false);

	// Update old and new tree display
	TTTManager::getInst().frmTracking->drawTree(0);
	ui.treeView->redrawTree(theTree);

	// Start tracking (prohibiting change of timepoint)
	return TTTManager::getInst().startTracking(true);
}

void TTTAutoTrackingTreeWindow::notifyManualTrackingFinished()
{
	// Update variable below when we know that tracking was in auto tracking mode
	m_lastManualTrackingInAutoTrackingMode = false;

	// This function must be called after tracking has finished
	if(TTTManager::getInst().isTracking())
		return;

	// Get position manager
	TTTPositionManager* ttpm = TTTManager::getInst().getCurrentPositionManager();
	if(!ttpm) {
		QMessageBox::critical(this, "Error", "Error: Could not get position manager");
		return;
	}

	// Get current tree
	Tree* currentTree = TTTManager::getInst().getTree();
	if(!currentTree)
		return;

	// Try to find corresponding Tree pointer in list (ignore event if tracked tree is not displayed in this window)
	int index = getIndexOfTree(currentTree);
	if(index == -1)
		return;

	// Make sure oldTTTManagerTree is set 
	if(!oldTTTManagerTree /*|| !oldTTTManagerTrack*/)
		return;

	// Tracking was in auto tracking mode
	m_lastManualTrackingInAutoTrackingMode = true;

	// Update display of current tree
	ui.treeView->redrawTree(currentTree);
	updateCurrentTreeInformation();

	// Set old tree in TTTManager
	TTTManager::getInst().setTree(oldTTTManagerTree);
	TTTManager::getInst().setCurrentTrack(oldTTTManagerTrack);
	TTTManager::getInst().frmTracking->setDataChanged(false);
	TTTManager::getInst().clearFields();

	// Update timescale in TTTTracking (updates TreeDisplay::Tree)
	int ftp = ttpm->getFirstTimePoint();
	int ltp = ttpm->getLastTimePoint();
	TTTManager::getInst().frmTracking->setTimePoints (ftp, ltp, false);

	// Update tree display
	TTTManager::getInst().frmTracking->drawTree(0);

	// Raise this window
	raise();

	// Update buttons
	updateControlsTreeEdit();

	//// Remember currentlyEditedTree and currentlyEditedTrack
	//Track* tmpEditedTrack = currentlyEditedTrack;
	//QSharedPointer<TreeDescriptor> tmpEditedTree = currentlyEditedTree;

	//// Clear display of current track
	//clearCurrentlyEditedTrack();

	//// Go to next track
	//if(tmpEditedTrack) {
	//	if(!tmpEditedTrack->getChild1()) {
	//		// No children, take same track
	//		setTrackForAppendingFragments(tmpEditedTrack, tmpEditedTree, true);
	//	}
	//	else {
	//		// Look for child that has no further children (Just always take left child)
	//		Track* child = tmpEditedTrack->getChild1();
	//		while(child->getChild1())
	//			child = child->getChild1();

	//		// Select it
	//		setTrackForAppendingFragments(child, tmpEditedTree);
	//		ui.treeView->selectCell(tmpEditedTree->theTree.data(), child, true);
	//		setSelectedCell(child);
	//	}
	//}
}


void TTTAutoTrackingTreeWindow::treeOrTreeFragmentToAppendSelected(QSharedPointer<Tree> _tree, QSharedPointer<TreeFragment> _treeFragment, const ITrackPoint* _trackPoint)
{
	// At least one parameter must be set
	if(!_tree && !_treeFragment)
		return;

	if(!selectedTrack || selectedTree.isNull()) {
		QMessageBox::information(this, "Note", "No cell selected.");
		return;
	}

	// Make sure we are currently not tracking normally (this should not be possible anyways)
	if(TTTManager::getInst().isTracking()) {
		QMessageBox::information(this, "Note", "Stop manual tracking first.");
		return;
	}

	// Track number of _trackpoint
	int trackNumberToAppend = -1;
	if(_trackPoint) {
		ITrack* track = _trackPoint->getITrack();
		if(track) 
			trackNumberToAppend = track->getTrackNumber();
	}

	// 120621: trackNumberToAppend must from now on always be provided
	if(trackNumberToAppend <= 0) {
		QString msg = "Internal error: append/insert tree failed because trackNumberToAppend is invalid.\n\nPlease report this as soon as possible.";
		QMessageBox::critical(this, "Error", msg);
		qWarning() << msg;
		return;
	}

	// Append tree or fragment to current track
	if(_tree) {
		appendTreeToTrack(_tree, trackNumberToAppend, selectedTrack, true);

		// Remove appended tree from display
		closeTree(_tree.data(), true);
	}
	else if(_treeFragment) {
		appendTreeFragmentToTrack(_treeFragment, selectedTrack, trackNumberToAppend, true);
	}

	// Go to next track
	if(ui.pbtAssistantNext->isEnabled())
		ui.pbtAssistantNext->click();

	// Update display of tree fragments
	updateCurrentTreeInformation();
}

void TTTAutoTrackingTreeWindow::treeOrTreeFragmentToAppendSelectedDuringManualTracking(QSharedPointer<Tree> _tree, QSharedPointer<TreeFragment> _fragment, const ITrackPoint* _trackPoint)
{
	// At least one parameter must be set
	if(!_tree && !_fragment)
		return;

	// We must have a currentlyEditedTree
	if(!selectedTree)
		return;

	// Get position manager
	TTTPositionManager* ttpm = TTTManager::getInst().getCurrentPositionManager();
	if(!ttpm) {
		QMessageBox::critical(this, "Error", "Error: Could not get position manager");
		return;
	}

	// Stop tracking
	ttpm->frmMovie->switchTrackingButtons(false);
	ttpm->frmMovie->getMultiplePictureViewer()->stopTracking();
	TTTManager::getInst().setTrackStopReason (TS_NONE);
	TTTManager::getInst().stopTracking(false);

	// Make sure tracking mode is now off
	if(TTTManager::getInst().isTracking())
		return;

	// Get current tree
	Tree* currentTree = TTTManager::getInst().getTree();
	if(!currentTree)
		return;

	// Manually tracked tree must be the currently edited tree in order for this to work!
	if(currentTree != selectedTree->theTree)
		return;

	// Update display of current tree
	ui.treeView->redrawTree(currentTree);

	// Get current track
	Track* currentTrack = TTTManager::getInst().getCurrentTrack();
	if(!currentTrack)
		return;
	
	// Track number of _trackpoint
	int trackNumberToAppend = -1;
	if(_trackPoint) {
		ITrack* track = _trackPoint->getITrack();
		if(track) 
			trackNumberToAppend = track->getTrackNumber();
	}
	
	// 120621: trackNumberToAppend must from now on always be provided
	if(trackNumberToAppend <= 0) {
		QString msg = "Internal error: append/insert tree failed because trackNumberToAppend is invalid.\n\nPlease report this as soon as possible.";
		QMessageBox::critical(this, "Error", msg);
		qWarning() << msg;
		return;
	}

	// Append tree or fragment to current track
	if(_tree) {
		appendTreeToTrack(_tree, trackNumberToAppend, currentTrack, true);

		// Remove appended tree from display
		closeTree(_tree.data(), true);
	}
	else if(_fragment)
		appendTreeFragmentToTrack(_fragment, currentTrack, trackNumberToAppend, true);

	// Set old tree in TTTManager
	TTTManager::getInst().setTree(oldTTTManagerTree);
	TTTManager::getInst().setCurrentTrack(oldTTTManagerTrack);
	TTTManager::getInst().frmTracking->setDataChanged(false);
	TTTManager::getInst().clearFields();

	// Update timescale in TTTTracking (updates TreeDisplay::Tree)
	int ftp = ttpm->getFirstTimePoint();
	int ltp = ttpm->getLastTimePoint();
	TTTManager::getInst().frmTracking->setTimePoints (ftp, ltp, false);

	// Update tree display
	TTTManager::getInst().frmTracking->drawTree(0);

	// Raise this window
	raise();

	// Update display of track circles
	TTTManager::getInst().redrawTracks();
}

bool TTTAutoTrackingTreeWindow::appendTreeFragmentToTrack( QSharedPointer<TreeFragment> _frg, Track* _track, int _trackNumberToAppend, bool _allowOverwriteOfTrackPoints )
{
	// Get pos manager
	TTTPositionManager* tttpm = TTTManager::getInst().getBasePositionManager();
	if(!tttpm)
		return false;

	// Check if fragment is already used
	if(_frg->isUsedForTTTTree()) {
		// Confirm
		if(QMessageBox::question(this, "Fragment already used", "The selected fragment has already been used for a tree.\n\nAppend to this tree anyway?", QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel) != QMessageBox::Yes )
			return false;
	}

	// Convert fragment to Tree
	QSharedPointer<Tree> frgConverted = _frg->convertToTree(tttpm->getLastTimePoint());

	// Append
	int trackNumberToAppend = -1;
	if(!appendTreeToTrack(frgConverted, _trackNumberToAppend, _track, _allowOverwriteOfTrackPoints))
		return false;

	// Remove appended tree from display
	closeTree(frgConverted.data(), true);

	// Add fragment to used fragments list
	selectedTree->usedFragments.append(_frg);

	// Set associated tree name of fragment
	QString treeName = _track->getTree()->getTreeName();
	if(treeName.isEmpty())
		treeName = "(new tree)";
	_frg->setAssociatedTree(treeName);

	// Emit signal
	emit frmAutoTracking->treeFragmentUsedForTree(_frg->getTreeFragmentNumber());

	return true;
}

bool TTTAutoTrackingTreeWindow::appendTreeToTrack( QSharedPointer<Tree> _tree, int _trackNumberToAppend, Track* _track, bool _allowOverwriteOfTrackPoints /*= false*/, int _insertionTimePoint )
{
	/**
	 * Step 1: preparation.
	 */

	// Cannot append tree to track of same tree
	if(_track->getTree() == _tree.data()) {
		QMessageBox::critical(this, "Error", "AppendTreeToTrack() failed: attempt to append tree to track of same tree.");
		return false;
	}

	// Check _tree
	if(!_tree) {
		qWarning() << "tTt Warning: attempted to append null-tree to track";
		return false;
	}


	// Get tree root node
	Track* treeRoot = _tree->getRootNode();

	// Get first time point of _tree
	int treeFirstTrace = treeRoot->getFirstTrace();
	if(treeFirstTrace <= 0) {
		QMessageBox::critical(this, "Error", "AppendTreeToTrack() failed: could not get first timepoint of tree.\n\nPlease report this error as soon as possible.");
		return false;
	}

	// Determine _insertionTimePoint if necessary
	if(_insertionTimePoint == -1) {

		// Get last set track point of _track or its mother before current time point
		TrackPoint* lastTP = _track->getLastTrackPointBefore(currentTimePoint, true);
		if(!treeRoot || !lastTP || lastTP->TimePoint <= 0) {
			QMessageBox::critical(this, "Error", "AppendTreeToTrack() failed: attempt to append tree to empty tree.");
			return false;
		}

		// Get time point at which _tree should be appended to track (i.e. before this time point, nothing from _tree will be inserted into _track)
		_insertionTimePoint = std::min(currentTimePoint, lastTP->TimePoint + 1);
	}

	// Get track of _tree that should be inserted into _track
	Track* treeTrackToAppend = _tree->getTrackByNumber(_trackNumberToAppend);
	if(!treeTrackToAppend) {
		// This is very bad
		QString msg = QString("Internal error: appendTreeToTrack() failed because _trackNumberToAppend is invalid: %1.\n\nPlease report this error as soon as possible!").arg(_trackNumberToAppend);
		QMessageBox::critical(this, "Error", msg);
		qWarning() << msg;
		return false;
	}

	// Status log
	qDebug() << "tTt: appending tree to track - target track trackpoint count: " << _track->getMarkCount() 
		<< " - target track number: " << _track->getNumber() 
		<< " - target track first time point: " << _track->getFirstTimePoint() 
		<< " - target track last time point: " << _track->getLastTimePoint() 
		<< " - insertion time point: " << _insertionTimePoint 
		<< " - current time point: " << currentTimePoint
		<< " - tree first time point: " << _tree->getRootNode()->getFirstTimePoint()
		<< " - tree track count: " << _tree->getTrackCount()
		<< " - tree track number: " << _trackNumberToAppend;

	/**
	 * Step 2: cut _track if necessary.
	 */
	if(_track->getMarkCount() > 0 && _insertionTimePoint <= _track->getLastTrace()) {
		qDebug() << "cutting target track..";

		// We need to cut off everything starting from insertionTimePoint
		QSharedPointer<Tree> newTree = cutTreeInternal(_track, _insertionTimePoint, true);
		if(newTree.isNull()) {
			// This is also very bad
			QString msg = QString("Internal error: appendTreeToTrack() failed because cutTreeInternal (1) failed.\n\nPlease report this error as soon as possible!");
			QMessageBox::critical(this, "Error", msg);
			qWarning() << msg;
			return false;
		}
	}


	/**
	 * Step 3: cut _tree at sourceTrackToAppend if necessary.
	 */
	if(_insertionTimePoint > treeFirstTrace) {
		qDebug() << "cutting track of tree to insert..";

		// We need to cut off everything starting from insertionTimePoint
		QSharedPointer<Tree> tmpTree = cutTreeInternal(treeTrackToAppend, _insertionTimePoint, false);
		if(tmpTree.isNull()) {
			// This is also very bad
			QString msg = QString("Internal error: appendTreeToTrack() failed because cutTreeInternal (1) failed.\n\nPlease report this error as soon as possible!");
			QMessageBox::critical(this, "Error", msg);
			qWarning() << msg;
			return false;
		}

		// Make sure 'remaining' tree (i.e. begin of tree) is not lost by adding it to display
		addTree(_tree);

		// Continue with cut off part
		_tree = tmpTree;
	}

	/**
	 * Step 3: append tree to track.
	 */

	// Try to append
	QString error;
	if(!_track->appendTree(_tree.data(), _allowOverwriteOfTrackPoints, &error)) {
		QString msg = "Error: could not append selected fragment to current tree:\n\n" + error;
		QMessageBox::critical(this, "Error", msg);
		qWarning() << msg;
		return false;
	} 

	// Update display
	ui.treeView->redrawTree(_track->getTree());

	return true;

	/*
	120621OH: old code before possibility to append/insert any track into any track
	// Get last trace of _track
	int trackLastTrace = _track->getLastTrace();
	if(trackLastTrace <= 0) {
		// Try to get last trace of mother track
		Track* mother = _track->getMotherTrack();
		if(mother)
			trackLastTrace = mother->getLastTrace();
	}

	// Get tree root node
	Track* treeRoot = _tree->getRootNode();
	if(!treeRoot)
		return false;

	// Get first trace of _tree
	int treeFirstTrace = treeRoot->getFirstTrace();

	// Make sure values are valid
	if(treeFirstTrace <= 0 || trackLastTrace <= 0) {
		QMessageBox::critical(this, "Error", "AppendTreeToTrack() failed: could not get last timepoint of target track.\n\nPlease report this error as soon as possible.");
		return false;
	}

	// Check if _tree starts before _track ends
	if(treeFirstTrace <= trackLastTrace) {
		// Get track of _tree where _track should be appended
		Track* sourceTrackToAppend = _tree->getTrackByNumber(_trackNumberToAppend);
		if(!sourceTrackToAppend) {
			// This is very bad
			QMessageBox::critical(this, "Error", 
				QString("Internal error: appendTreeToTrack() failed because _trackNumberToAppend is invalid: %1.\n\nPlease report this error as soon as possible!").arg(_trackNumberToAppend));
			return false;
		}

		// Now cut trackToAppendTo
		QSharedPointer<Tree> newTree = cutTreeInternal(sourceTrackToAppend, trackLastTrace+1, false);
		if(newTree.isNull()) {
			// This is also very bad
			QMessageBox::critical(this, "Error", 
				QString("Internal error: appendTreeToTrack() failed because cutTreeInternal() failed.\n\nPlease report this error as soon as possible!"));
			return false;
		}

		// Add _tree to display
		addTree(_tree, QList<QSharedPointer<TreeFragment> >(), "");

		// Append newTree instead of _tree to track
		_tree = newTree;
	}

	// Try to append
	QString error;
	if(!_track->appendTree(_tree.data(), _allowOverwriteOfTrackPoints, &error)) {
		QString msg = "Error: could not append selected fragment to current tree:\n\n" + error;
		QMessageBox::critical(this, "Error", msg);
		return false;
	} 

	//// Remember currentlyEditedTree and currentlyEditedTrack to automatically select child
	//Track* tmpEditedTrack = currentlyEditedTrack;
	//QSharedPointer<TreeDescriptor> tmpEditedTree = currentlyEditedTree;

	//// Clear display of current track
	//clearCurrentlyEditedTrack();

	// Update display
	ui.treeView->redrawTree(_track->getTree());

	// Go to next track
	if(Track* child1 = _track->getChild1()) {
		//// Look for child that has no further children (Just always take left child)
		//Track* child = tmpEditedTrack->getChild1();
		//while(child->getChild1())
		//	child = child->getChild1();

		// Select it
		setSelectedCell(child1);
	}

	// Remove appended tree from display
	closeTree(_tree.data(), true);

	return true;
	*/
}

void TTTAutoTrackingTreeWindow::cellDivisionSelected()
{
	// Division
	setCellFate(TS_DIVISION);
}

void TTTAutoTrackingTreeWindow::cellDeathSelected()
{
	// Apoptosis
	setCellFate(TS_APOPTOSIS);
}

void TTTAutoTrackingTreeWindow::lostSelected()
{
	// Lost
	setCellFate(TS_LOST);
}

void TTTAutoTrackingTreeWindow::removeCellFate() 
{ 
	if(!selectedTrack || selectedTree.isNull())
		return;

	// Make sure we are currently not tracking manually
	if(TTTManager::getInst().isTracking()) {
		QMessageBox::information(this, "Note", "Stop manual tracking first.");
		return;
	}

	// Check if track has children
	if(selectedTrack->hasChildren()) {
		removeCellDivision(selectedTrack);
	}
	else {
		// Just remove cell fate
		setCellFate(TS_NONE);
	}
}

void TTTAutoTrackingTreeWindow::setCellFate( TrackStopReason _cellFate )
{
	if(!selectedTrack || selectedTree.isNull())
		return;

	// Track must contain trackpoints
	if(!selectedTrack->tracked()) {
		QMessageBox::information(this, "Note", "Selected track must contain at least one trackpoint.");
		return;
	}

	// If we are currently tracking manually, stop tracking with specified cell fate
	if(TTTManager::getInst().isTracking()) {
		if(TTTPositionManager* tttpm = TTTManager::getInst().getCurrentPositionManager())
			tttpm->frmMovie->stopTracking(_cellFate);
		else
			QMessageBox::information(this, "Note", "Stop manual tracking first.");
		return;
	}

	// Get position manager
	TTTPositionManager* tttpm = TTTManager::getInst().getCurrentPositionManager();
	if(!tttpm) {
		QMessageBox::critical(this, "Error", "Cannot detect current position.");
		return;
	}

	// Get last timepoint
	int ltp = tttpm->getLastTimePoint();
	if(ltp <= 0) {
		QMessageBox::critical(this, "Error", "Cannot detect last timePoint.");
		return;
	}

	// Get tree
	Tree* tree = selectedTrack->getTree();
	if(!tree) {
		QMessageBox::critical(this, "Error", "Could not get pointer to current tree.");
		return;
	}

	// Get timepoint (timepoint of last trackpoint or currently displayed timepoint, depending on what is smaller)
	int tp = qMin(selectedTrack->getLastTrace(), currentTimePoint);
	if(tp <= 0) {
		QMessageBox::critical(this, "Error", "Could not determine timepoint.");
		return;
	}

	// Check if timepoint is not before first timepoint
	int ftp = selectedTrack->getFirstTimePoint();
	if(ftp <= 0){
		QMessageBox::critical(this, "Error", "Cannot detect first timepoint.");
		return;
	}
	if(tp < ftp) {
		QMessageBox::critical(this, "Error", "Current time point is before first time point of selected track.");
		return;
	}

	// Cut tree at tp + 1
	if(tp < selectedTrack->getLastTrace()) {
		QSharedPointer<Tree> newTree = cutTreeInternal(selectedTrack, tp + 1);
		if(!newTree) {
			QMessageBox::critical(this, "Error", "Cannot change cell fate (cut tree failed).");
			return;
		}
	}
	else {
		// Tree is not going to be cut, check if it already has children
		if(selectedTrack->hasChildren()) {
			QMessageBox::critical(this, "Error", "Division already exists at or before the intended timepoint.");
			return;
		}
	}

	if(_cellFate == TS_DIVISION) {

		// Create child tracks
		Track *tmp = new Track (selectedTrack, 1, ltp, tp + 1);
		tmp->setNumber (selectedTrack->getNumber() * 2);
		tree->insert (tmp);

		tmp = new Track (selectedTrack, 2, ltp, tp + 1);
		tmp->setNumber (selectedTrack->getNumber() * 2 + 1);
		tree->insert (tmp);

		selectedTrack->setStopReason (TS_DIVISION);

		// Update display
		ui.treeView->redrawTree(tree);
		updateCurrentTreeInformation();

		// Go to next track (just always take left child)
		Track* child1 = selectedTrack->getChild1();
		setSelectedCell(child1);

		// Go to first timepoint of selected cell and open movie window
		if(child1->getFirstTimePoint() > 0) {
			setTimePoint(child1->getFirstTimePoint());
		}
		openMovieWindow();
	}
	else {

		// Make sure track has no children
		if(selectedTrack->hasChildren()) {
			QMessageBox::critical(this, "Error", "Track already has child tracks.");
			return;
		}

		// Other stop reason -> just change it
		selectedTrack->setStopReason (_cellFate);

		//// Remember currentlyEditedTree and currentlyEditedTrack
		//Track* tmpEditedTrack = currentlyEditedTrack;
		//QSharedPointer<TreeDescriptor> tmpEditedTree = currentlyEditedTree;

		//// Clear display of current track
		//clearCurrentlyEditedTrack();

		// Update display
		ui.treeView->redrawTree(tree);
		updateCurrentTreeInformation();

		//// Go to next track
		//goToTrack(tmpEditedTrack->getChild1(), tmpEditedTree);
		//ui.treeView->selectCell(tmpEditedTree->theTree.data(), tmpEditedTrack->getChild1(), true);
	}

	// Update tree edit buttons
	updateControlsTreeEdit();
}

bool TTTAutoTrackingTreeWindow::cutTree()
{
	/**
	 * Initial checks.
	 */

	// Check current tree and track
	if(!selectedTrack || !selectedTrack->getTree()) {
		QMessageBox::information(this, "Note", "No cell selected.");
		return false;
	}

	// Check timepoint
	if(currentTimePoint <= 0) {
		QMessageBox::information(this, "Note", "Please go to the timepoint where the tree should be cut, i.e. to the first timepoint of the new tree.");
		return false;
	}

	// Make sure tp is in lifetime of current track
	if(currentTimePoint < selectedTrack->getFirstTrace() || currentTimePoint > selectedTrack->getLastTrace()) {
		QMessageBox::information(this, "Note", "Please go to a timepoint within the lifetime of the selected track.");
		return false;
	}

	/**
	 * Actual cutting.
	 */

	// Delegate
	QSharedPointer<Tree> newTree = cutTreeInternal(selectedTrack, currentTimePoint);
	if(newTree.isNull())
		return false;

	// Re-select selectedCell (it could have had children before, now it could be possible to append successors to it)
	if(selectedTrack)
		cellSelected(selectedTrack->getTree(), selectedTrack);

	return true;

	//// Make sure tree is not being tracked
	//if(TTTManager::getInst().isTracking()) {
	//	if(TTTManager::getInst().getTree() == selectedTrack->getTree()) {
	//		QMessageBox::information(this, "Note", "Please stop manual tracking first.");
	//		return false;
	//	}
	//}

	//// Cut tree
	//QSharedPointer<Tree> newTree = selectedTrack->cutTrack(currentTimePoint);
	//if(newTree.isNull()) {
	//	QMessageBox::critical(this, "Error", "Operation failed. Please report this problem as soon as possible.");
	//	return false;
	//}

	//// Add tree to display
	//QList<QSharedPointer<TreeFragment> > fragments;
	//addTree(newTree, fragments);

	//// Update display of old tree
	//ui.treeView->redrawTree(selectedTrack->getTree());

	//return true;
}

QSharedPointer<Tree> TTTAutoTrackingTreeWindow::cutTreeInternal( Track* _trackToCut, int _timePoint, bool _addNewTreeToDisplay )
{
	// Returned object
	QSharedPointer<Tree> ret;

	// Check tree and track
	assert(_trackToCut);
	assert(_trackToCut->getTree());
	if(!_trackToCut || !_trackToCut->getTree()) {
		return ret;
	}

	// Check timepoint
	assert(_timePoint > 0);
	if(_timePoint <= 0) {
		return ret;
	}

	// Make sure tp is in lifetime of current track
	assert(_timePoint >= _trackToCut->getFirstTrace());
	assert(_timePoint <= _trackToCut->getLastTrace());
	if(_timePoint < _trackToCut->getFirstTrace() || _timePoint > _trackToCut->getLastTrace()) {
		return ret;
	}

	// Make sure tree is not being tracked
	if(TTTManager::getInst().isTracking()) {
		if(TTTManager::getInst().getTree() == _trackToCut->getTree()) {
			QMessageBox::information(this, "Note", "Please stop manual tracking first.");
			return ret;
		}
	}

	// Cut tree
	ret = _trackToCut->cutTrack(_timePoint);
	if(ret.isNull()) {
		QMessageBox::critical(this, "Error", "Operation failed. Please report this problem as soon as possible.");
		return ret;
	}

	// Add tree to display
	if(_addNewTreeToDisplay) {
		QList<QSharedPointer<TreeFragment> > fragments;
		addTree(ret, fragments);
	}

	// Update display of old tree
	ui.treeView->redrawTree(_trackToCut->getTree());

	return ret;
}

bool TTTAutoTrackingTreeWindow::preloadImages( TTTPositionManager* tttpm, unsigned int _timePoint, unsigned int _loadBefore, unsigned int _loadAfter, unsigned int _wl /*= 0*/ )
{
	// Get picture array
	PictureArray* pics = tttpm->getPictures();
	if(!pics)
		return false;

	// Mark pictures
	//for(int curTp = _timePoint - _loadBefore; curTp <= _timePoint + _loadAfter; ++curTp) {
	int curTp = qMax(1, (int)_timePoint - (int)_loadBefore);
	while(curTp <= _timePoint + _loadAfter) {
		// Set loading flag
		pics->setLoading(curTp, 1, _wl, true);
		++curTp;
	}

	// Load images
	pics->loadPictures(false);
	return true;
}

bool TTTAutoTrackingTreeWindow::trackIsSelected() const
{
	return selectedTrack != 0;
}

QList<AutoTrackingCellCircle*> TTTAutoTrackingTreeWindow::getCellCirclesForDisplay( int _timePoint, TTTPositionManager* _tttpm, int _wlIndex ) const
{
	QList<AutoTrackingCellCircle*> ret;

	// Check if track is selected
	if(!selectedTrack || !selectedTree || !selectedTree->theTree)
		return ret;

	/**
	 * Display all track points that belong to selected tree.
	 */
	if(TTTManager::getInst().getTree() != selectedTrack->getTree()) {
		QList<ITrackPoint*> trackPoints = selectedTree->theTree->getTrackPointsAtTimePoint(_timePoint);
		for(auto it = trackPoints.constBegin(); it != trackPoints.constEnd(); ++it) {
			ITrackPoint* curTrackPoint = *it;

			// Calc local pixel coordinates
			QPointF trackPictureCoords = _tttpm->getDisplays().at (_wlIndex).calcTransformedCoords (QPointF(curTrackPoint->getX(), curTrackPoint->getY()), &_tttpm->positionInformation);

			// Color by confidence
			float conf = curTrackPoint->getConfidenceLevel();
			QColor color;
			if(conf >= 0)
				color = TreeView::mapConfidenceOnColor(conf);
			else {
				// Default color (i.e. when no confidence level is set) white or magenta if cell is currently selected
				ITrack* curTrackPointTrack = curTrackPoint->getITrack();
				if(curTrackPointTrack && selectedTrack->getTrackNumber() == curTrackPointTrack->getTrackNumber())
					color = Qt::magenta;	
				else
					color = Qt::white;	
			}

			// Create circle
			QString textString = QString("%1").arg(curTrackPoint->getITrack()->getTrackNumber());
			AutoTrackingCellCircle* newCircle = new AutoTrackingCellCircle(AutoTrackingCellCircle::A_NONE, 
				curTrackPoint,
				textString, 
				trackPictureCoords, 
				CELLCIRCLESIZE / 2.0f, 
				color, 
				QSharedPointer<TreeFragment>(), 
				QSharedPointer<Tree>(), 
				color);	

			ret.append(newCircle);
		}
	}	

	/**
	 * Display cells that could be appended to the currently selected cell or inserted, i.e. current track
	 * gets cut at current time point and selected fragment appended.
	 */

	// Get last trackpoint before _timePoint
	const TrackPoint* lastTp = selectedTrack->getLastTrackPointBefore(_timePoint, true);
	if(!lastTp || lastTp->getTimePoint() <= 0)
		return ret;

	// Get max migration per timepoint and convert to micrometers
	float mmpp = TATInformation::getInst()->getWavelengthInfo (0).getMicrometerPerPixel();
	float maxMigrationPerTP = ui.spbMaxMigrationPerTP->value();
	maxMigrationPerTP *= mmpp;

	// Get last timepoint current track exists to calculate which trackpoints should be displayed
	int lastTimePointOfSelectedTrack = lastTp->getTimePoint();

	// Make sure lastTP is valid and we are not out of the temporal search range
	if(_timePoint - lastTimePointOfSelectedTrack <= ui.spbTemporalSearchRange->value()) {

		// Last position
		const QPointF lastPos = lastTp->point();

		// Max distance to lastPos
		float maxDist = (_timePoint - lastTimePointOfSelectedTrack + 1) * maxMigrationPerTP;

		// Get all TreeFragmentTrackpoints at _timePoint of possible successor fragments
		QList<QSharedPointer<TreeFragmentTrackPoint> > trackPoints = frmAutoTracking->getTrackPointsByTimePoint(_timePoint);
		for(int i = 0; i < trackPoints.size(); ++i) {
			TreeFragmentTrackPoint* curTrackPoint = trackPoints[i].data();

			// Check distance
			float dist = QLineF(QPointF(trackPoints[i]->getX(), trackPoints[i]->getY()), lastPos).length();
			if(dist <= maxDist) {

				// Filter if starts before last trackpoint of selectedTrack if desired
				if(ui.chkHideTreesStartingBeforeLastTP->isChecked()) {
					if(treeStartsBeforeTimePoint(curTrackPoint, lastTimePointOfSelectedTrack+1))
						continue;
				}

				// Skip used fragments
				TreeFragment* curFragment = trackPoints[i]->getTrack()->getTree();
				if(curFragment && curFragment->isUsedForTTTTree())
					continue;

				// Color: cyan (indicates that this is a possible successor track that can be selected)
				QColor color = Qt::cyan;

				// Display cell circle
				AutoTrackingCellCircle* circle = frmAutoTracking->createCellCircleFromTreeFragmentTrackPoint(
					*(trackPoints[i].data()), 
					_tttpm, 
					_wlIndex,  
					AutoTrackingCellCircle::A_SELECT_SUCCESSOR,
					"Click with left mouse button to append this tree to the currently selected track.",
					&color);
				if(circle)
					ret.append(circle);
			}
		}

		// Also check currently opened trees
		for(int i = 0; i < currentTrees.size(); ++i) {
			QSharedPointer<Tree> curTree = currentTrees[i]->theTree;
			
			// Tree cannot be appended to itself
			if(curTree == selectedTree->theTree)
				continue;

			// Check all tracks
			QList<Track*> tracksToProcess;
			tracksToProcess.append(curTree->getBaseTrack());
			while(!tracksToProcess.isEmpty()) {
				Track* curTrack = tracksToProcess.takeFirst();
				if(!curTrack)
					continue;

				// Add children to list
				if(curTrack->hasChildren()) {
					tracksToProcess.append(curTrack->getChild1());
					tracksToProcess.append(curTrack->getChild2());
				}

				// Check if it is alive at _timePoint
				TrackPoint* tp = curTrack->getTrackPointByTimePoint(_timePoint);
				if(!tp) 
					continue;

				// Check distance
				float dist = QLineF(tp->point(), lastPos).length();
				if(dist <= maxDist) {
					// Filter if starts before last trackpoint of selectedTrack if desired
					if(ui.chkHideTreesStartingBeforeLastTP->isChecked()) {
						if(treeStartsBeforeTimePoint(tp, lastTimePointOfSelectedTrack+1))
							continue;
					}

					// Calc local pixel coordinates
					QPointF trackPictureCoords = _tttpm->getDisplays().at (_wlIndex).calcTransformedCoords (tp->point(), &_tttpm->positionInformation);

					// Description (set in displayCurrentTrack())
					//QString textString = QString("%1 - %2").arg(curTree->getTreeName()).arg(selectedTrack->getNumber());
					QString textString = curTree->getTreeName();
					if(textString.isEmpty())
						textString = "(new tree)";

					// Color: blue (indicates that this is a possible successor track that can be selected)
					QColor color = Qt::blue;

					// Create circle
					AutoTrackingCellCircle* newCircle = new AutoTrackingCellCircle(
						AutoTrackingCellCircle::A_SELECT_SUCCESSOR, 
						tp,
						textString, 
						trackPictureCoords, 
						CELLCIRCLESIZE / 2.0f, 
						color, 
						QSharedPointer<TreeFragment>(), 
						curTree, 
						color,
						"Click with left mouse button to append this tree to the currently selected track.");	

					// Connect circle
					connect(newCircle, SIGNAL(cellClicked(AutoTrackingCellCircle*, Qt::MouseButton)), frmAutoTracking, SLOT(cellCircleClicked(AutoTrackingCellCircle*, Qt::MouseButton)));

					ret.append(newCircle);
				}
			}
		}
	}

	return ret;
}

void TTTAutoTrackingTreeWindow::setTimePoint( int _timePoint, int  )
{
	// Basic check if tp is valid
	if(_timePoint < 1) {
		return;
	}

	// Draw timepoint line
	ui.treeView->setTimeLineTimePoint(_timePoint);

	// Update currentTimePoint and display
	currentTimePoint = _timePoint;
	updateDisplayOfCurrentTimepoint();
	ui.sldTimepoint->setValue(_timePoint);
}

void TTTAutoTrackingTreeWindow::applySearchRange()
{
	// Just redraw tracks
	TTTManager::getInst().redrawTracks();
}

void TTTAutoTrackingTreeWindow::help()
{
	QString helpMsg = "Use the left mouse button to select or deselect a cell (track) of a tree.\n\n"
		"If no track is selected, all automatically generated tree fragments are displayed in "
		"the movie window. Create a new tree based on the corresponding tree fragment by clicking on "
		"the cell circle with the left mouse button. Use the right mouse button to mark a tree fragment as invalid so that "
		"it will no longer be displayed (e.g. if debris has been tracked).\n\nIf a track is selected, only the track, its mother track and tracks that could be appended "
		"are displayed in the movie window. Click on the cell circle using the left mouse button to append the corresponding "
		"tree fragment to the currently selected track.\n\nUse the buttons on the top of this window to set the cell fate of the currently selected track or to "
		"cut it at the current time point creating a new tree. "
		"The current time point - 1 will be the last time point of the currently selected track and the current time point will be the first time point "
		"of the first track of the newly created tree. "
		"The new tree can then, for example, be appended to other tracks. The '-> Division' button will create a cell division at the current time point. "
		"If the track contains any trackpoints and/or child tracks after the current timepoint, they will be moved into a new tree."
		/*"Note that all trackpoints after the current time point will be removed. To avoid that, cut the track at the time point of the division before "
		"using the '-> Division' button."*/;

	QMessageBox::information(this, "Help", helpMsg);
}

void TTTAutoTrackingTreeWindow::openExistingTreeFiles()
{
	if(!frmAutoTracking)
		return;

	// Get current pos manager
	TTTPositionManager *ttpm = TTTManager::getInst().getCurrentPositionManager();
	if(!ttpm)
		return;

	// Get ttt files directory
	QString sugdir = ttpm->getTTTFileDirectory(true);

	// Get file(s)
	QStringList fileNames = QFileDialog::getOpenFileNames(this, "Select file(s) to open", sugdir, "Tree files (*.ttt)");
	if(fileNames.isEmpty())
		return;

	// Change cursor
	ChangeCursorObject cc;

	// Create trees
	for(int i = 0; i < fileNames.size(); ++i) {
		// Convert separators
		fileNames[i] = QDir::fromNativeSeparators(fileNames[i]);

		// Create tree
		Tree *tmp = new Tree();
		QSharedPointer<Tree> tree(tmp);
		tree->reset();

		// Open file
		int trackCount, firstTP, lastTP;

		if (TTTFileHandler::readFile (fileNames[i], tree.data(), trackCount, firstTP, lastTP, &ttpm->positionInformation) != TTT_READING_SUCCESS) {
			// Reading failed
			return;
		}

		// Find associated fragments
		QString nameWithoutPath = fileNames[i].mid(fileNames[i].lastIndexOf('/')+1);
		QList<QSharedPointer<TreeFragment> > usedFragments = frmAutoTracking->findAssociatedTreeFragments(nameWithoutPath);

		// Display tree
		addTree(tree, usedFragments, fileNames[i]);
	}
}

void TTTAutoTrackingTreeWindow::closeCurrentTree()
{
	// Make sure a tree is selected
	if(!checkIfTreeIsSelected())
		return;

	// Try to close tree
	closeTree(selectedTree->theTree.data());
}

bool TTTAutoTrackingTreeWindow::checkIfTreeIsSelected()
{
	// Check if we have a tree
	if(!selectedTree) {
		QMessageBox::information(this, "Operation not possible", "Please select a tree first.");
		return false;
	}

	return true;
}

void TTTAutoTrackingTreeWindow::saveCurrentTree()
{
	// Make sure a tree is selected
	if(!checkIfTreeIsSelected())
		return;

	// Save tree
	saveTree(selectedTree->theTree.data());
}

void TTTAutoTrackingTreeWindow::saveCurrentTreeAs()
{
	// Make sure a tree is selected
	if(!checkIfTreeIsSelected())
		return;

	// Save tree as
	saveTree(selectedTree->theTree.data(), true);
}

bool TTTAutoTrackingTreeWindow::treeStartsBeforeTimePoint( const ITrackPoint* _trackPoint, int _timePoint )
{
	// Get track
	ITrack* track = _trackPoint->getITrack();
	if(!track)
		return false;

	// Get tree
	ITree* tree = track->getITree();
	if(!tree)
		return false;

	// Get root node
	ITrack* root = tree->getRootNode();
	if(!root)
		return false;

	// Get first timepoint
	int firstTP = root->getFirstTimePoint();

	// Test
	if(firstTP <= 0 || firstTP >= _timePoint)
		return false;
	else
		return true;
}

void TTTAutoTrackingTreeWindow::currentPositionChanged( int _oldPosition, int _newPosition )
{
	// Set currentPosition
	currentPosition = _newPosition;
	if(currentPosition <= 0) {
		// No position selected
		currentPosition = -1;
		pbtStatusBarOpenMovieWindow->setEnabled(false);
		ui.sldTimepoint->setEnabled(false);
		ui.pbtFirstTp->setEnabled(false);
		ui.pbtLastTp->setEnabled(false);

		// Update TreeView Timeline
		ui.treeView->updateTimeLinePosition(-1);
	}
	else {
		// Position selected
		ui.sldTimepoint->setEnabled(true);
		pbtStatusBarOpenMovieWindow->setEnabled(true);
		ui.pbtFirstTp->setEnabled(true);
		ui.pbtLastTp->setEnabled(true);

		// Update TreeView Timeline
		ui.treeView->updateTimeLinePosition(_newPosition);

		// Get pos manager
		TTTPositionManager* tttpm = TTTManager::getInst().getPositionManager(_newPosition);
		if(tttpm) {
			// Set limits of timepoint slider
			ui.sldTimepoint->setMinimum(std::max(tttpm->getFirstTimePoint(), 1));
			ui.sldTimepoint->setMaximum(std::max(tttpm->getLastTimePoint(), 1));

			// Set first and last loading timepoints to min / max time point of position (also set limits)
			int minTp = std::max(tttpm->getFirstTimePoint(), 1);
			int maxTp = std::max(tttpm->getLastTimePoint(), 1);
			ui.spbFirstTp->setMinimum(minTp);
			ui.spbFirstTp->setValue(minTp);
			ui.spbLastTp->setMaximum(maxTp);
			ui.spbLastTp->setValue(maxTp);

			// Subscribe for loading completed signals
			if(PictureArray* pics = tttpm->getPictures()) {
				connect(pics, SIGNAL(LoadedPicture(unsigned int, int, bool)), this, SLOT(loadingCompleted()), Qt::UniqueConnection);
				connect(pics, SIGNAL(LoadingComplete()), this, SLOT(loadingCompleted()), Qt::UniqueConnection);
			}
		}
	}

	// Update current position label
	QString posLabel;
	if(currentPosition > 0) 
		posLabel = QString("%1").arg(Tools::convertIntPositionToString(currentPosition));
	else
		posLabel = QString("-");
	ui.lblCurrentPosition->setText(posLabel);

	// Update title
	updateWindowTitle();

	// Update wavelength selection buttons
	updateWlButtons();

	// Update display of time as real time depends on current position
	updateDisplayOfCurrentTimepoint();
}

void TTTAutoTrackingTreeWindow::updateWindowTitle()
{
	// Update title of window
	QString caption;
	if(currentPosition <= 0)
		caption = QString("tTt %1 - Tree Window").arg(internalVersion);
	else
		caption = QString("tTt %1 - Tree Window - Pos%2").arg(internalVersion).arg(Tools::convertIntPositionToString(currentPosition));

	setCaption(caption);
}

void TTTAutoTrackingTreeWindow::addWlButton( int _row, int _col, int _wl )
{
	// Check if we have enough rows and columns and if button already exists
	if(_row >= ui.tbtWls->rowCount() || _col >= ui.tbtWls->columnCount() || ui.tbtWls->cellWidget(_row, _col))
		return;

	// Create button
	QPushButton* newButton = new QPushButton(QString("%1").arg(_wl));
	newButton->setCheckable(true);
	newButton->setFixedWidth(20);
	newButton->setFixedHeight(20);
	newButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	// Add button to GUI and to buttons hash
	ui.tbtWls->setCellWidget(_row, _col, newButton);
	wlButtons.insert(_wl, newButton);
}

void TTTAutoTrackingTreeWindow::updateWlButtons()
{
	// Remember selected buttons
	QSet<int> checkedWls;
	for(QHash<int, QPushButton*>::iterator it = wlButtons.begin(); it != wlButtons.end(); ++it) {
		QPushButton* btn = *it;
		if(btn->isChecked())
			checkedWls.insert(it.key());
	}

	// Remove current buttons
	ui.tbtWls->clear();
	wlButtons.clear();

	// Check if we have a position
	if(currentPosition <= 0)
		return;

	// Get available wavelengths
	TTTPositionManager* tttpm = TTTManager::getInst().getPositionManager(Tools::convertIntPositionToString(currentPosition));
	if(!tttpm)
		return;
	QList<int> wls = tttpm->getAvailableWavelengths();
	//std::sort(wls.begin(), wls.end());		<-- 120411OH: gives strange warnings with vs2010..
	qSort(wls.begin(), wls.end());

	// Set number of rows (max 25 buttons per row)
	int numRows = (wls.size()-1)/25 + 1;
	ui.tbtWls->setColumnCount(25);
	ui.tbtWls->setRowCount(numRows);

	// Create buttons
	for(int i = 0; i < wls.size(); ++i) 
		addWlButton(i / 25, i % 25, wls[i]);

	// Update height of table view (20px per row, 2 for border)
	ui.tbtWls->setMaximumHeight(20 * numRows + 2);

	// Re-apply checked states
	for(QSet<int>::iterator it = checkedWls.begin(); it != checkedWls.end(); ++it) {
		QPushButton* btn = wlButtons.value(*it, 0);
		if(btn)
			btn->setChecked(true);
	}
}

void TTTAutoTrackingTreeWindow::updateBackwardTrackingModeDisplay()
{
	if(TTTManager::getInst().inBackwardTrackingMode())
		lblBackwardTrackingMode->setText("Backward Tracking Mode: On");
	else
		lblBackwardTrackingMode->setText("Backward Tracking Mode: Off");
}

void TTTAutoTrackingTreeWindow::openMovieWindow()
{
	// Get current position manager
	TTTPositionManager* curPos = TTTManager::getInst().getCurrentPositionManager();
	if (!curPos)
		return;

	// Get frmMovie
	TTTMovie* tmp = curPos->frmMovie;
	if (!tmp)
		return;

	// Show frmMovie and try to activate it
	if(tmp->isMinimized())
		tmp->showMaximized();
	else
		tmp->show();
	tmp->raise();
	tmp->activateWindow();
}

void TTTAutoTrackingTreeWindow::goToFirstPosOfTree()
{
	// Get current tree
	Tree *curTree = TTTManager::getInst().getTree();
	if(!selectedTree) {
		QMessageBox::information(this, "Note", "Cannot go to first position: No tree is currently selected.");
		return;
	}

	// Get first trackpoint
	TrackPoint* firstTrackPt = 0;
	Track* motherTrack = curTree->getBaseTrack();
	if(motherTrack) {
		int firstTimePt = motherTrack->getFirstTimePoint();
		if(firstTimePt > 0)
			firstTrackPt = motherTrack->getTrackPointByTimePoint(firstTimePt);
	}

	// Check if trackpoint was found
	if(!firstTrackPt) {
		QMessageBox::information(this, "Note", "Cannot go to first position: Tree contains no trackpoints.");
		return;
	}

	// Get position
	const QString pos = firstTrackPt->Position;
	if(pos.isEmpty()) {
		QMessageBox::information(this, "Error", "Cannot go to first position: Could not identify first position.");
		return;
	}

	// Finally, change current position
	if(!TTTManager::getInst().frmPositionLayout->setPosition(pos)) {
		QMessageBox::information(this, "Error", QString("Cannot go to first position: Changing to position '%1' failed.").arg(pos));
		return;
	}
}

bool TTTAutoTrackingTreeWindow::setSelectedCell(ITrack* trackToSelect)
{
	if(!trackToSelect) {
		/**
		 * Deselect currently selected cell.
		 */

		// Update GUI
		ui.lblSelectedTrack->setText("-");
		if(selectedTree && selectedTree->theTree && selectedTrack)
			ui.treeView->selectCell(selectedTrack->getTree(), selectedTrack, false);

		// Deselect track
		selectedTrack = nullptr;
	}
	else {
		/**
		 * Selected provided cell.
		 */

		// Get index of corresponding tree and pointer to Tree
		ITree* treeToSelect = trackToSelect->getITree();
		assert(treeToSelect);
		int treeIndex = getIndexOfTree(treeToSelect);
		if(treeIndex < 0) {
			qWarning() << "tTt Warning: Attempted to select invalid track (TTTAutoTrackingTreeWindow).";
			return false;
		}
		Tree* tree = currentTrees[treeIndex]->theTree.data();
		assert(tree);

		// Make sure tree is selected
		if(selectedTree != currentTrees[treeIndex])
			setSelectedTree(treeIndex);
		assert(selectedTree->theTree.data() == tree);

		// Get pointer to Track
		Track* track = tree->getTrackByNumber(trackToSelect->getTrackNumber());
		assert(track);

		// Change selected track
		selectedTrack = track;

		// Update GUI
		ui.lblSelectedTrack->setText(QString("%1").arg(track->getTrackNumber()));
		ui.treeView->selectCell(treeToSelect, trackToSelect, true);
	}

	// Redraw tracks
	TTTManager::getInst().redrawTracks();

	// Update edit tree controls
	updateControlsTreeEdit();

	return true;

	/*
	// Check if track has children
	if(track->getChild1() || track->getChild2()) {
		// No currently edited track (track with children cannot be edited)
		clearCurrentlyEditedTrack();

		// Enable cut and division buttons (both can be used with children)
		ui.pbtCutTree->setEnabled(true);
		ui.pbtDivision->setEnabled(true);
	}
	else {
		// If not, go to track
		setTrackForAppendingFragments(track, currentTrees[index]);
	}
	*/

}

void TTTAutoTrackingTreeWindow::updateDisplayOfCurrentTimepoint()
{

	// Check if timepoint is not available
	if(currentTimePoint <= 0) {
		ui.lblTimePoint->setText("-");
		return;
	}

	// Update label
	QString timePointText = QString("%1").arg(currentTimePoint);

	// Add real time
	TTTPositionManager* tttpm = TTTManager::getInst().getPositionManager(Tools::convertIntPositionToString(currentPosition));
	if(tttpm) {
		if(const FileInfoArray* picsInfo = tttpm->getFiles()) {
			QDateTime realTime = picsInfo->getRealTime(currentTimePoint, 1);
			if(realTime.isValid()) {
				timePointText += QString(" (%1)").arg(realTime.toString("dd.MM.yyyy - hh:mm:ss"));
			}
		}
	}

	ui.lblTimePoint->setText(timePointText);
}

void TTTAutoTrackingTreeWindow::timePointSliderChanged( int value )
{
	if(value <= 0)
		return;

	// Change timepoint
	TTTManager::getInst().setTimepoint(value);
}

void TTTAutoTrackingTreeWindow::imageLoadingSetFirstTp()
{
	// Get position manager and change first loading time point to first time point of position
	TTTPositionManager* tttpm = TTTManager::getInst().getCurrentPositionManager();
	if(tttpm) {
		ui.spbFirstTp->setValue(std::max(tttpm->getFirstTimePoint(), 1));
	
		// Highlight selected region in time line
		ui.treeView->timeLineSetHighlighting(ui.spbFirstTp->value(), ui.spbLastTp->value());
	}
}

void TTTAutoTrackingTreeWindow::imageLoadingSetLastTp()
{
	// Get position manager and change last loading time point to last time point of position
	TTTPositionManager* tttpm = TTTManager::getInst().getCurrentPositionManager();
	if(tttpm) {
		ui.spbLastTp->setValue(std::max(tttpm->getLastTimePoint(), 1));
	
		// Highlight selected region in time line
		ui.treeView->timeLineSetHighlighting(ui.spbFirstTp->value(), ui.spbLastTp->value());
	}
}

void TTTAutoTrackingTreeWindow::loadImages()
{
	// Load
	loadImagesInternal(true);
}


void TTTAutoTrackingTreeWindow::unloadImages()
{
	// Unload
	loadImagesInternal(false);
}

void TTTAutoTrackingTreeWindow::loadImagesInternal( bool load )
{
	// Get pictures
	TTTPositionManager* tttpm = TTTManager::getInst().getCurrentPositionManager();
	if(!tttpm)
		return;
	PictureArray* pics = tttpm->getPictures();
	if(!pics)
		return;

	// Check if any wl is selected
	bool foundSelectedWl = false;
	for(auto it = wlButtons.begin(); it != wlButtons.end(); ++it) {
		if((*it)->isChecked()) {
			foundSelectedWl = true;
			break;
		}
	}
	if(!foundSelectedWl) {
		QMessageBox::information(this, "Load images", "Please select at least one wavelength you want to load.");
		return;
	}

	// Stop highlighting of selected region in time line
	ui.treeView->timeLineSetHighlighting(-1, -1);

	// Mark selected images
	int start = ui.spbFirstTp->value(),
		stop = ui.spbLastTp->value(),
		interval = ui.spbPictureInterval->value();
	for(auto it = wlButtons.begin(); it != wlButtons.end(); ++it) {
		if((*it)->isChecked()) {
			int wl = (*it)->text().toInt();
			for(int i = start; i <= stop; i += interval) {
				pics->setLoading(i, -1, wl, load);
			}
		}
	}

	// Subscribe for loading completed signals
	connect(pics, SIGNAL(LoadedPicture(unsigned int, int, bool)), this, SLOT(loadingCompleted()), Qt::UniqueConnection);
	connect(pics, SIGNAL(LoadingComplete()), this, SLOT(loadingCompleted()), Qt::UniqueConnection);

	// Load images
	pics->loadPictures(true, !load);
}

void TTTAutoTrackingTreeWindow::connectTreeFragmentsTool()
{
	// Open connect tree fragments tool
	if(!m_connectTreesTool)
		m_connectTreesTool = new TTTAutotrackingConnectFragments(this);
	m_connectTreesTool->show();
	m_connectTreesTool->raise();
}

void TTTAutoTrackingTreeWindow::updateControlsTreeEdit()
{
	//  Check if track is selected
	bool trackSelected = selectedTrack != nullptr;

	// Check if track has children
	bool trackHasChildren = false;
	if(selectedTrack)
		trackHasChildren = selectedTrack->hasChildren();

	// Update button states
	ui.pbtDivision->setEnabled(trackSelected);
	ui.pbtCellDeath->setEnabled(trackSelected);
	ui.pbtLost->setEnabled(trackSelected);
	ui.pbtTrackManually->setEnabled(trackSelected);
	ui.pbtCutTree->setEnabled(trackSelected);
	ui.pbtRemoveCellFate->setEnabled(trackSelected);
	ui.pbtSwapCells->setEnabled(trackSelected);
	ui.grpAppendableTrees->setEnabled(trackSelected && !trackHasChildren);

	// Assistant buttons
	updateControlsAssistant();
}


void TTTAutoTrackingTreeWindow::updateControlsAssistant()
{
	//  Check if track is selected
	bool trackSelected = selectedTrack != nullptr;

	// Update assistant control states
	ui.pbtAssistantNext->setEnabled(selectedTree);
	ui.pbtAssistantPrev->setEnabled(selectedTree);
	ui.pbtAssistantAccept->setEnabled(selectedTree == assistantCurrentTree && assistantStartTp > 0); // Region has to be set in current tree
}


void TTTAutoTrackingTreeWindow::updateControlsDependingOnNumTrees()
{
	if(currentTrees.size() > 0) {
		menuBar->actFile_CLOSE_ALL_TREES->setEnabled(true);
	}
	else {
		menuBar->actFile_CLOSE_ALL_TREES->setEnabled(false);
	}
}


//void TTTAutoTrackingTreeWindow::refreshPossibleSuccessors()
//{
//	// Clear everything currently displayed
//	removeTreeFragmentsFromDisplay(possibleTreeFragmentSuccessors);
//	possibleTreeFragmentSuccessors.clear();
//	possibleTreeSuccessors.clear();
//
//	// Check if track with no children is selected
//	Track* track = selectedTrack;
//	if(track && track->hasChildren())
//		track = nullptr;
//	if(!track)
//		return;
//
//	// If track contains no trackpoints, use mother if available
//	if(!track->tracked()) {
//		track = track->getMotherTrack();
//		if(!track || track->tracked())
//			return;
//	}
//
//	// Get last trackpoint
//	TrackPoint lastTP = track->getTrackPoint (track->getLastTrace());
//	assert(lastTP.TimePoint > 0);
//
//	// Get fragments starting in range
//	possibleTreeFragmentSuccessors = frmAutoTracking->getTreeFragmentsStartingInRange(lastTP.TimePoint + 1, lastTP.TimePoint + ui.spbTemporalSearchRange->value(), lastTP.point(), ui.spbMaxMigrationPerTP->value());
//	if(possibleTreeFragmentSuccessors.isEmpty()) {
//		// No fragments found
//		QString msg = "Note: could not find any fragments beginning after track end";
//		statusBar->showMessage(msg, 3000);
//		//QMessageBox::information(this, "Note", msg);
//	}
//
//	// Get also all currently displayed trees starting in range
//	possibleTreeSuccessors.clear();
//	for(int i = 0; i < currentTrees.size(); ++i) {
//		QSharedPointer<Tree> curTree = currentTrees[i]->theTree;
//
//		// Get root
//		Track* root = curTree->getRootNode();
//		if(!root)
//			continue;
//
//		// Get first trackPoint
//		int firstTrace = root->getFirstTimePoint();
//		const TrackPoint& firstTrackPoint = root->getTrackPoint(firstTrace, false);
//		if(firstTrace < 1 || firstTrackPoint.TimePoint < 1)
//			continue;
//
//		// Check if tree starts in range
//		if(firstTrace > lastTP.TimePoint && firstTrace <= lastTP.TimePoint + FRAGMENTS_TEMPORAL_SEARCH_RANGE) {
//			// Distance of first trackpoint of curTree to last trackpoint of edited track and max distance
//			float distance = QLineF(QPointF(firstTrackPoint.X, firstTrackPoint.Y), lastTP.point()).length();
//			float maxDist = ui.spbMaxMigrationPerTP->value() * (firstTrace - lastTP.TimePoint);
//
//			if(distance <= maxDist) {
//				// Tree starts in range
//				possibleTreeSuccessors.append(curTree);
//			}
//		}
//	}
//}

//void TTTAutoTrackingTreeWindow::displayTreeFragments( const QList<QSharedPointer<TreeFragment>>& fragments )
//{
//	// Check if user wants to see them
//	if(!ui.chkShowSuccessors->isChecked())
//		return;
//
//	// Add fragments to treeView
//	int counter = 1;
//	for(auto it = fragments.constBegin(); it != fragments.constEnd(); ++it) {
//		// Set name to number to avoid file name being displayed
//		TreeFragment* curFr = it->data();
//		QString treeName = QString("%1").arg(curFr->getTreeFragmentNumber());
//		curFr->setTreeName(treeName);
//
//		// Add to tree view (at most 50)
//		if(counter++ <= 50) {
//			ui.treeView->addTree(curFr, false, false);
//		}
//	}
//}

void TTTAutoTrackingTreeWindow::removeTreeFragmentsFromDisplay( const QList<QSharedPointer<TreeFragment>>& fragments )
{
	// Remove fragments from treeView
	for(auto it = fragments.constBegin(); it != fragments.constEnd(); ++it) {
		TreeFragment* curFr = it->data();
		ui.treeView->removeTree(curFr);
	}
}

void TTTAutoTrackingTreeWindow::timeLineClicked( int timepoint, Qt::MouseButton button )
{
	if(timepoint <= 0)
		timepoint = 1;

	// Set start/stop timepoints for loading images
	if(button & Qt::LeftButton) {
		//ui.toolBox->setCurrentIndex(TOOLBOX_INDEX_LOADIMAGES);
		ui.spbFirstTp->setValue(timepoint);
		ui.spbFirstTp->setFocus();

		// Highlight selected region in time line
		ui.treeView->timeLineSetHighlighting(ui.spbFirstTp->value(), ui.spbLastTp->value());
	}
	else if(button & Qt::RightButton) {
		//ui.toolBox->setCurrentIndex(TOOLBOX_INDEX_LOADIMAGES);
		ui.spbLastTp->setValue(timepoint);
		ui.spbLastTp->setFocus();

		// Highlight selected region in time line
		ui.treeView->timeLineSetHighlighting(ui.spbFirstTp->value(), ui.spbLastTp->value());
	}
}

void TTTAutoTrackingTreeWindow::saveTreeFragments()
{
	// Get loaded tree fragments and check if trees are loaded
	const QHash<int, QList<QSharedPointer<TreeFragment>>>& treeFragments = frmAutoTracking->getTreeFragments();
	if(treeFragments.size() <= 0) {
		QMessageBox::information(this, "Note", "No tree fragments are currently loaded.");
		return;
	}

	// Get default location
	QString expPath = TTTManager::getInst().frmPositionLayout->getExperimentDir().absolutePath();
	QString pathEdited;
	QString pathStandard;
	if(!expPath.isEmpty()) {
		expPath = QDir::fromNativeSeparators(expPath);
		if(expPath.right(1) != "/")
			expPath.append('/');
		pathEdited = expPath + "autotracking/treesEdited/";
		pathStandard = expPath + "autotracking/trees/";
	}

	// Let user choose where to save the fragments
	QMessageBox msgBox;
	QString msg = QString("Select target directory where tree fragments should be saved:\n\nClick 'Default Folder' to save trf files in '%1' (recommended).\n\nClick 'Standard' to save trf files in '%2'.\n\nClick 'Select Folder' to select a different folder.").arg(pathEdited).arg(pathStandard);
	msgBox.setText(msg);
	msgBox.setCaption("Select directory");
	QPushButton *pbtDefaultLocation = msgBox.addButton(QString("Default Folder"), QMessageBox::ActionRole);
	if(pathEdited.isEmpty())
		pbtDefaultLocation->setEnabled(false);
	QPushButton *pbtStandardLocation = msgBox.addButton(QString("Standard"), QMessageBox::ActionRole);
	if(pathStandard.isEmpty())
		pbtStandardLocation->setEnabled(false);
	QPushButton *pbtSelectFolder = msgBox.addButton(QString("Select Folder"), QMessageBox::ActionRole);
	QPushButton *pbtAbort = msgBox.addButton(QMessageBox::Cancel);
	msgBox.exec();

	// Get choice
	QString selectedPath;
	if(msgBox.clickedButton() == pbtDefaultLocation) {
		selectedPath = pathEdited;
	}
	else if(msgBox.clickedButton() == pbtSelectFolder) {
		selectedPath = QFileDialog::getExistingDirectory(this, "Select output folder", expPath + "autotracking");
	}
	else if(msgBox.clickedButton() == pbtStandardLocation) {
		selectedPath = pathStandard;
	}

	// Check if canceled
	if(selectedPath.isEmpty())
		return;

	// Check if selectedPath contains .trf files
	if(!handleExisingTrfFiles(selectedPath))
		return;

	// Save tree fragments
	int numFailed = 0;
	try {
		ChangeCursorObject cc;
		numExported = 0;
		for(auto it1 = treeFragments.constBegin(); it1 != treeFragments.constEnd(); ++it1) {
			for(auto it2 = it1->constBegin(); it2 != it1->constEnd(); ++it2) {
				TreeFragment* curFragment = it2->data();

				// Get path
				TTTPositionManager* ttpm = TTTManager::getInst().getPositionManager(curFragment->getPositionNumber());
				if(!ttpm) {
					++numFailed;
					continue;
				}
				QString finalPath = selectedPath + ttpm->getBasename(false);

				// Make sure path exists
				QDir dir(finalPath);
				if(!finalPath.isEmpty() && !dir.exists()) {
					if(!dir.mkpath(finalPath)) {
						++numFailed;
						continue;
					}
				}

				// Generate filename
				QString fileName = getNextTreeFilename(curFragment->getFirstTimePoint());
				curFragment->exportToFile(finalPath + "/" + fileName);
				++numExported;
			}
		}
	}
	catch(const TTTException& err) {
		QMessageBox::critical(this, "Critical error", QString("Saving failed:\n\n") + err.what());
		return;
	}

	// Finished
	if(numFailed == 0) {
		QMessageBox::information(this, "Finished", QString("Successfully saved %1 tree fragments.").arg(numExported));
	}
	else {
		QMessageBox::warning(this, "Errors occurred", QString("Warning: Failed saving %1 of %2 trees.").arg(numFailed).arg(treeFragments.size()));
	}
}

bool TTTAutoTrackingTreeWindow::handleExisingTrfFiles(const QString& _pathName)
{
	// Files to delete
	QStringList foundTrfFiles;
	bool foundLogFiles = false;

	/**
	 * Backward compatibility: check if there are .trf or .log files in _pathName.
	 */
	listTrfFiles(_pathName, foundTrfFiles);
	if(!foundTrfFiles.isEmpty()) 
		foundLogFiles = containsLogFiles(_pathName);

	/**
	 * Now check all sub directories.
	 */
	if(!foundLogFiles) {
		QStringList directories = QDir(_pathName).entryList(QDir::Dirs | QDir::NoDotAndDotDot);
		for(int i = 0; i < directories.size(); ++i) {
			listTrfFiles(_pathName + directories[i], foundTrfFiles, directories[i] + '/');
			if(containsLogFiles(_pathName + directories[i])) {
				foundLogFiles = true;
				break;
			}
		}
	}

	// Do not delete log files automatically
	if(foundLogFiles) {
		// Cancel
		QMessageBox::information(this, "Tracking canceled", "The specified output folder contains .trf files (tree fragments) as well as .log files, so the tree fragments have been used in tTt already.\n\nPlease delete/move the .trf and .log files manually before using that folder again or select a different output directory.");
		return false;
	}

	// Check for trf files
	if(!foundTrfFiles.isEmpty()) {
		// Confirm delete
		QString msg = QString("The specified folder already contains %1 .trf files (tree fragments).\n\nShould they be deleted automatically?").arg(foundTrfFiles.size());
		if(QMessageBox::warning(this, "Confirm delete", msg, QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel) != QMessageBox::Yes) {
			// Cancel
			return false;
		}

		// Delete files
		for(QStringList::const_iterator it = foundTrfFiles.constBegin(); it != foundTrfFiles.constEnd(); ++it) {
			// Full filename
			QString fileName = _pathName + *it;

			// Delete
			QFile tmp(fileName);
			if(!tmp.remove()) {
				// Cancel
				QMessageBox::critical(this, "Error", QString("Removing file '%1' failed: %2").arg(fileName).arg(tmp.errorString()));
				return false;
			}
		}

		// Status
		qDebug() << (QString("Deleted %1 files").arg(foundTrfFiles.size()));
	}

	return true;
}

void TTTAutoTrackingTreeWindow::listTrfFiles( const QString& _pathName, QStringList& _fileList, const QString& _prefix )
{
	// Check dir
	if(_pathName.isEmpty() || !QDir(_pathName).exists())
		return;

	// List trf files
	QStringList extensions(".trf");
	QStringList fileListTmp = FastDirectoryListing::listFiles(_pathName, extensions);

	// Prepend prefix
	if(!_prefix.isEmpty()) {
		for(int i = 0; i < fileListTmp.size(); ++i) {
			fileListTmp[i] = _prefix + fileListTmp[i];
		}
	}

	// Append to _fileList
	_fileList.append(fileListTmp);
}

bool TTTAutoTrackingTreeWindow::containsLogFiles( const QString& _pathName )
{
	// Check dir
	if(_pathName.isEmpty() || !QDir(_pathName).exists())
		return false;

	// List log files
	QStringList extensions(".log");
	QStringList fileList = FastDirectoryListing::listFiles(_pathName, extensions);

	return !fileList.isEmpty();
}

QString TTTAutoTrackingTreeWindow::getNextTreeFilename( int minTp ) const
{
	// Complete filename
	QString expBaseName =  TTTManager::getInst().getExperimentBasename();
	QString fileName = QString("%1_t%2_n%3.trf").arg(expBaseName).arg(minTp, 5, 10, QChar('0')).arg(numExported + 1, 5, 10, QChar('0'));
	return fileName;
}

void TTTAutoTrackingTreeWindow::loadingCompleted()
{
	// Update display of loaded images
	if(currentPosition > 0) {
		ui.treeView->redrawTimeScale();
	}
}

void TTTAutoTrackingTreeWindow::resetTreeEditingAssistant()
{
	// Reset assistant
	setTreeEditingAssistantState(QWeakPointer<TreeDescriptor>(), -1, -1, -1);
}

void TTTAutoTrackingTreeWindow::setTreeEditingAssistantState( QWeakPointer<TreeDescriptor> _tree, int _trackNum, int _startTp, int _stopTp )
{
	// Stop displaying of old region 
	QSharedPointer<TreeDescriptor> oldTree = assistantCurrentTree.toStrongRef();
	if(oldTree) {
		ui.treeView->stopHighlightingTrack(oldTree->theTree.data(), assistantCurrentTrack);
	}

	// Change state
	assistantCurrentTree = _tree;
	assistantCurrentTrack = _trackNum;
	assistantStartTp = _startTp;
	assistantStopTp = _stopTp;

	// Update controls
	updateControlsAssistant();

	// Check if new region is valid
	QSharedPointer<TreeDescriptor> newTree = _tree.toStrongRef();
	if(newTree) {
		// Show new region 
		ui.lblCurrentRegion->setText(QString("Tp %1 - %2 in Track %3").arg(assistantStartTp).arg(assistantStopTp).arg(assistantCurrentTrack));
		ui.treeView->highlightTrack(newTree->theTree.data(), _trackNum, _startTp, _stopTp);

		// Make sure track is selected
		Track* track = newTree->theTree->getTrackByNumber(assistantCurrentTrack);
		assert(track);
		if(!track) 
			return;
		if(track != selectedTrack)
			setSelectedCell(track);

		// Go to position of track point where region starts
		TrackPoint* trackPoint = 0;
		assert(_startTp <= _stopTp);
		for(int tmpTp = assistantStartTp; tmpTp <= assistantStopTp; ++tmpTp) {
			trackPoint = track->getTrackPointByTimePoint(tmpTp);	// Find trackpoint in [_startTp,_stopTp]
			if(trackPoint)
				break;
		}
		assert(trackPoint);
		if(!trackPoint)
			return;	
		int position = trackPoint->Position.toInt();
		assert(position > 0);
		if(position <= 0)
			return;
		bool success = TTTManager::getInst().frmPositionLayout->setPosition(trackPoint->Position);
		if(!success)	
			// Can happen e.g. if position is locked
			return;

		// Load images
		TTTPositionManager* tttpm = TTTManager::getInst().getPositionManager(position);
		assert(tttpm);
		PictureArray* pics = tttpm->getPictures();
		assert(pics);
		int wl = ui.spbAssistantWl->value();
		pics->loadInterval(assistantStartTp - ui.spbAssistantTpsBefore->value(), assistantStopTp + ui.spbAssistantTpsAfter->value(), wl, ui.spbAssistantZ->value(), 1, false);

		// Set time point
		TTTManager::getInst().setTimepoint(assistantStartTp, -1);

		// Open movie window
		tttpm->frmMovie->show();
		tttpm->frmMovie->raise();
		if(tttpm->frmMovie->isMinimized())
			tttpm->frmMovie->showMaximized();
		tttpm->frmMovie->setActiveWindow();

		// Focus on cell
		if(ui.chkAssistantFocusOnCell->isChecked())
			tttpm->frmMovie->getMultiplePictureViewer()->centerTrack(track, assistantStartTp);
	}
	else {
		ui.lblCurrentRegion->setText("(no region selected)");
	}
}

void TTTAutoTrackingTreeWindow::assistantSelectNextRegion(bool directionForward)
{
	if(!selectedTree)
		return;

	// Change cursor
	ChangeCursorObject cc;

	/**
	 * Get track and time point where search for next low confidence region should start
	 */
	QWeakPointer<TreeDescriptor> tree = selectedTree;
	Track* startTrack = nullptr;
	int startTp = -1;
	int stopTp = -1;

	// Check if a region is currently selected within the selected tree and the corresponding track is still selected
	if(assistantCurrentTree == selectedTree && selectedTrack && selectedTrack->getNumber() == assistantCurrentTrack) {
		// begin search after current region
		startTrack = selectedTree->theTree->getTrackByNumber(assistantCurrentTrack);
		if(directionForward)
			startTp = assistantStopTp + 1;
		else
			stopTp = assistantStartTp - 1;
	}
	else {
		// begin search at selected track
		startTrack = selectedTrack;
		if(!startTrack)
			startTrack = selectedTree->theTree->getRootNode();
	}
	if(!startTrack)
		startTrack = selectedTree->theTree->getRootNode();
	if(!startTrack)
		return;
	
	/**
	 * Perform breadth-first search
	 */
	bool foundRegion = false;
	std::deque<Track*> tracksQueue;
	tracksQueue.push_back(startTrack);
	while(tracksQueue.size() > 0) {
		// Take first
		Track* curTrack = tracksQueue[0];
		tracksQueue.pop_front();

		// Determine start time point
		int start;
		if(startTp > 0) {
			start = startTp;
			startTp = -1;
		}
		else
			start = curTrack->getFirstTrace();

		// Determine stop time point
		int stop;
		if(stopTp > 0) {
			stop = stopTp;
			stopTp = -1;
		}
		else
			stop = curTrack->getLastTrace();

		// Find low confidence region
		if(findLowConfidenceRegion(curTrack, directionForward, start, stop, ui.dsbAssistantConfThreshold->value())) {
			// Found region
			foundRegion = true;
			setTreeEditingAssistantState(selectedTree, curTrack->getTrackNumber(), start, stop);

			break;
		}
		else {
			if(directionForward) {
				// Nothing found, add child tracks to tracks queue
				if(Track* child1 = curTrack->getChild1())
					tracksQueue.push_back(child1);
				if(Track* child2 = curTrack->getChild2())
					tracksQueue.push_back(child2);

				// Check if no tracks are left
				if(tracksQueue.size() == 0) {
					// Go to last track point of track (if we didn't start there already) and consider it as low confidence region
					if(assistantStartTp < curTrack->getLastTrace()) {
						// Found region
						foundRegion = true;
						setTreeEditingAssistantState(selectedTree, curTrack->getTrackNumber(), curTrack->getLastTrace(), curTrack->getLastTrace());

						break;
					}

					// Find next root
					startTrack = assistantFindNextRootTrack(startTrack);
					if(!startTrack) {
						// No new root found
						break;
					}
					tracksQueue.push_back(startTrack);
				}
			}
			else {
				// If current track was left child, go to mother
				if(curTrack->getTrackNumber() % 2 == 0 && curTrack->getMother()) {
					tracksQueue.push_back(curTrack->getMother());
				}
				else {
					// Investigate left sibling: follow right child tracks until none is found
					Track* nextTrack = curTrack->getSibling();
					if(nextTrack) {
						while(1) {
							Track* child2 = nextTrack->getChild2();
							if(child2)
								nextTrack = child2;
							else {
								tracksQueue.push_back(nextTrack);
								break;
							}
						}
					}
				}
			}

		}
	}

	if(!foundRegion) {
		// Nothing found, select no region
		resetTreeEditingAssistant();
	}

}

bool TTTAutoTrackingTreeWindow::findLowConfidenceRegion( const Track* _track, bool _direction, int& _lowerTp, int& _higherTp, float _confidenceThreshold )
{
	if(_lowerTp > _higherTp)
		return false;

	// Time point step and start
	int delta = 1;
	int start = _lowerTp;
	if(!_direction) {
		delta = -1;
		start = _higherTp;
	}

	// Start search for a contiguous set of track points with confidence levels (=probabilities) below or equal to _confidenceThreshold
	bool regionFound = false;
	for(int tp = start; _direction ? (tp <= _higherTp) : (tp >= _lowerTp); tp += delta) {
		TrackPoint* trackPoint = _track->getTrackPointByTimePoint(tp);
		if(!trackPoint)
			continue;
		float conf = trackPoint->getConfidenceLevel();

		if(!regionFound) {
			// Look for begin of region
			
			if((conf >= 0.0f && conf <= _confidenceThreshold) || (ui.chkAssistantIncludeAccepted->isChecked() && conf == 2.0f)) {
				regionFound = true;
				if(_direction)
					_lowerTp = tp;
				else
					_higherTp = tp;
			}
		}
		else {
			// Look for end of region
			if(conf > _confidenceThreshold || conf <= 1.0f) {
				if(_direction)
					_higherTp = tp - 1;
				else
					_lowerTp = tp + 1;
				return true;
			}
		}
	}

	// Maybe we are still looking for end of region (which remains as it is then)
	if(regionFound) {
		return true;
	}

	return false;
}

void TTTAutoTrackingTreeWindow::assistantSelectPrevRegion()
{
	// Delegate
	assistantSelectNextRegion(false);
}


Track* TTTAutoTrackingTreeWindow::assistantFindNextRootTrack( Track* start )
{
	Track* curTrack = start;
	while(curTrack) {
		int curTrackNum = curTrack->getTrackNumber();
		//if(direction) {
			// Look for right sibling
			if(curTrackNum % 2 == 0) {
				Track* sib = curTrack->getTree()->getTrackByNumber(curTrackNum + 1);
				if(sib)
					return sib;
			}
		//}
		//else {
		//	// Look for left sibling
		//	if(curTrackNum % 2 == 1) {
		//		Track* sib = curTrack->getTree()->getTrackByNumber(curTrackNum - 1);
		//		if(sib)
		//			return sib;
		//	}
		//}

		// Check mother track
		curTrack = curTrack->getMother();
	}

	// Nothing found
	return nullptr;
}

void TTTAutoTrackingTreeWindow::assistantAcceptCurRegion()
{
	// Get track selected in assistant
	QSharedPointer<TreeDescriptor> tree = assistantCurrentTree.toStrongRef();
	if(!tree)
		return;
	Track* track = tree->theTree->getTrackByNumber(assistantCurrentTrack);
	if(!track)
		return;

	 // Set confidence of track points of current region to 2 (indicates: manually inspected)
	for(int tp = assistantStartTp; tp <= assistantStopTp; ++tp) {
		TrackPoint* trackPoint = track->getTrackPointByTimePoint(tp);
		if(!trackPoint)
			continue;
		trackPoint->confidence = 2.0f;
	}
	tree->theTree->setModified(true);

	// Redraw tree
	ui.treeView->redrawTree(tree->theTree.data());

	// Go to next region
	assistantSelectNextRegion();
}

void TTTAutoTrackingTreeWindow::removeCellDivision( Track* track )
{
	// Get tree
	if(!track) {
		qWarning() << "tTt Warning: removeCellDivision() failed because track is null";
		return;
	}
	Tree* tree = track->getTree();
	if(!tree) {
		qWarning() << "tTt Warning: removeCellDivision() failed because track has no tree";
		return;
	}

	// Ask user if he wants to continue with child track
	QMessageBox msgBox;
	msgBox.setCaption("Remove cell division");
	QString text = "Append child track to this track after removing division?\n\nSelect 'Nothing' to append no child track.";
	msgBox.setText(text);
	QPushButton *child1Button = msgBox.addButton("Left Child", QMessageBox::ActionRole);
	QPushButton *child2Button = msgBox.addButton("Right Child", QMessageBox::ActionRole);
	QPushButton *noChildButton = msgBox.addButton("Nothing", QMessageBox::ActionRole);
	QPushButton *cancelButton = msgBox.addButton("Cancel", QMessageBox::ActionRole);
	msgBox.exec();

	// Get clicked button
	int selectedChild = -1;
	QAbstractButton* clickedButton = msgBox.clickedButton();
	if(clickedButton == cancelButton)
		return;
	if(clickedButton == child1Button)
		selectedChild = 1;
	else if(clickedButton == child2Button)
		selectedChild = 2;

	// Cut off both children
	QSharedPointer<Tree> child1Tree;
	QSharedPointer<Tree> child2Tree;
	Track* child1 = track->getChild1();
	Track* child2 = track->getChild2();

	// Cut off children and add child to display, if it will not be appended
	if(child1->getMarkCount() > 0) {
		child1Tree = cutTreeInternal(child1, child1->getFirstTrace(), selectedChild != 1);
		if(!child1Tree) {
			QMessageBox::critical(this, "Error", "Operation failed: cutTreeInternal(1) failed.");
			return;
		}
	}
	if(child2->getMarkCount() > 0) {
		child2Tree = cutTreeInternal(child2, child2->getFirstTrace(), selectedChild != 2);
		if(!child2Tree) {
			// Add tree 1 to display to avoid losing data (if not happened yet)
			if(child1Tree && selectedChild == 1)
				addTree(child1Tree);

			QMessageBox::critical(this, "Error", "Operation failed: cutTreeInternal(2) failed.");
			return;
		}
	}
	
	// Prune tree
	track->pruneTree(track->getLastTimePoint());
	ui.treeView->redrawTree(tree);

	// Set cell fate to nothing
	track->setStopReason(TS_NONE);

	// Append child track
	bool error = false;
	if(selectedChild == 1 && child1Tree) {
		error = !appendTreeToTrack(child1Tree, 1, track, false, child1Tree->getRootNode()->getFirstTrace());

		// Remove appended tree from display
		if(!error)
			closeTree(child1Tree.data(), true);
	}
	else if(selectedChild == 2 && child2Tree) {
		error = !appendTreeToTrack(child2Tree, 1, track, false, child2Tree->getRootNode()->getFirstTrace());

		// Remove appended tree from display
		if(!error)
			closeTree(child2Tree.data(), true);
	}
	if(error) {
		QMessageBox::critical(this, "Error", QString("Operation failed: appendTreeToTrack(%1) failed.").arg(selectedChild));
	}
}

void TTTAutoTrackingTreeWindow::swapCells()
{
	try {

		/**
		 * Initial checks.
		 */

		// Check current tree and track
		if(!selectedTrack || !selectedTrack->getTree())
			throw TTTException("No cell selected.");

		// Check timepoint
		if(currentTimePoint <= 0) 
			throw TTTException("Please go to the timepoint where the tree should be cut, i.e. to the first timepoint of the new tree.");

		// Make sure tp is in lifetime of current track
		if(currentTimePoint < selectedTrack->getFirstTrace() || currentTimePoint > selectedTrack->getLastTrace()) 
			throw TTTException("Please go to a timepoint within the lifetime of the selected cell.");

		// Get TTTPositionManager
		TTTPositionManager* tttpm = TTTManager::getInst().getPositionManager(currentPosition);
		if(!tttpm) 
			throw TTTException("No position selected.");
 
		/**
		 * Find nearest cell of any loaded tree or tree fragment.
		 */

		// Get trackpoint of selected cell at current time point
		TrackPoint* curTp = nullptr;
		if(selectedTrack)
			curTp = selectedTrack->getTrackPointByTimePoint(currentTimePoint);
		if(!curTp) 
			throw TTTException("No cell selected or cell has no trackpoint at the current timepoint.");

		// Find closest fragment within 200 micrometers
		float minDist = 200.0f;
		TreeFragmentTrackPoint* closestFragmentTrackPoint = nullptr;
		QList<QSharedPointer<TreeFragmentTrackPoint> > trackPoints = frmAutoTracking->getTrackPointsByTimePoint(currentTimePoint);
		for(int i = 0; i < trackPoints.size(); ++i) {
			TreeFragmentTrackPoint* curTrackPoint = trackPoints[i].data();

			// Skip used fragments
			TreeFragment* curFragment = curTrackPoint->getTrack()->getTree();
			if(curFragment && curFragment->isUsedForTTTTree())
				continue;

			float dist = QLineF(QPointF(trackPoints[i]->getX(), trackPoints[i]->getY()), curTp->point()).length();
			if(dist < minDist) {
				closestFragmentTrackPoint = curTrackPoint;
				minDist = dist;
			}
		}

		// Find closest tree that is closer than the closest fragment
		QSharedPointer<Tree> closestTree;
		Track* closestTreeTrack = nullptr;
		for(int i = 0; i < currentTrees.size(); ++i) {
			QSharedPointer<Tree>& curTree = currentTrees[i]->theTree;
			QList<Track*> tracksToProcess;
			tracksToProcess.append(curTree->getBaseTrack());
			while(!tracksToProcess.isEmpty()) {
				Track* curTrack = tracksToProcess.takeFirst();
				if(!curTrack || curTrack == selectedTrack)
					continue;
				if(curTrack->hasChildren()) {
					tracksToProcess.append(curTrack->getChild1());
					tracksToProcess.append(curTrack->getChild2());
				}
				TrackPoint* tp = curTrack->getTrackPointByTimePoint(currentTimePoint);
				if(!tp) 
					continue;
				float dist = QLineF(curTp->point(), tp->point()).length();
				if(dist < minDist) {
					minDist = dist;
					closestTree = curTree;
					closestTreeTrack = curTrack;
				}
			}
		}

		// Report if nothing was found
		if(!closestTree && !closestFragmentTrackPoint) 
			throw TTTException("No cell found close to selected cell at current timepoint (within 200 micrometers).");

		// Ask user for affirmation
		QString text;
		if(closestTree == selectedTree->theTree && selectedTrack->getSibling() == closestTreeTrack) 
			text = QString("Swap selected cell with cell %1 (sibling) of same tree?").arg(closestTreeTrack->getTrackNumber());
		else if(closestTree == selectedTree->theTree)
			text = QString("Swap selected cell with cell %1 (not sibling) of same tree?").arg(closestTreeTrack->getTrackNumber());
		else if(closestTree)
			text = QString("Swap selected cell with cell %1 from different tree?").arg(closestTreeTrack->getTrackNumber());
		else
			text = QString("Swap selected cell with cell %1 from unused tree fragment?").arg(closestFragmentTrackPoint->getITrack()->getTrackNumber());
		if(QMessageBox::question(this, "Confirm Action", text, QMessageBox::Yes | QMessageBox::No) != QMessageBox::Yes)
			return;


		/**
		 * Transform tree fragment to tree if necessary.
		 */

		if(closestFragmentTrackPoint && !closestTree) {
			if(TreeFragmentTrack* tr = closestFragmentTrackPoint->getTrack()) {
				int trackNum = tr->getTrackNumber();
				TreeFragment* tfr = tr->getTree();
				if(tfr) {
					// Convert fragment and add tree to display
					QSharedPointer<Tree> convertedTree = tfr->convertToTree(tttpm->getLastTimePoint());
					if(convertedTree) {
						addTree(convertedTree);
						closestTree = convertedTree;
						closestTreeTrack = closestTree->getTrackByNumber(trackNum);

						// Add fragment to used fragments list of selected tree and set associated tree name of fragment
						QWeakPointer<TreeFragment> tmp = tr->getTreeWeakPointer();
						if(tmp)
							selectedTree->usedFragments.append(tmp.toStrongRef());
						QString treeName = selectedTree->theTree->getTreeName();
						if(treeName.isEmpty())
							treeName = "(new tree)";
						tfr->setAssociatedTree(treeName);
					}
				}
			}
		}
		if(!closestTree || !closestTreeTrack)
			throw TTTException("Internal error: closestTree or closestTreeTrack not set.");

		/**
		 * Swap cells.
		 */

		// At least one tree must start before the current time point
		int ftp1 = selectedTree->theTree->getRootNode()->getFirstTimePoint();
		int ftp2 = closestTree->getRootNode()->getFirstTimePoint();
		if((ftp1 >= currentTimePoint && ftp2 >= currentTimePoint) || ftp1 <= 0 || ftp2 <= 0) 
			throw TTTException("Swapping not possible: Both cells seem to have no track points before the current timepoint.");

		// Cut current track if necessary
		QSharedPointer<Tree> treeOneToAppend;
		int trackOneToAppend;
		if(ftp1 < currentTimePoint) {
			treeOneToAppend = cutTreeInternal(selectedTrack, currentTimePoint, false);
			if(!treeOneToAppend)
				throw TTTException("Internal error: cutTreeInternal() failed (1).");
			trackOneToAppend = 1;
		}
		else {
			treeOneToAppend = selectedTree->theTree;
			trackOneToAppend = selectedTrack->getTrackNumber();
		}

		// Cut closest tree if necessary
		QSharedPointer<Tree> treeTwoToAppend;
		int trackTwoToAppend;
		if(ftp2 < currentTimePoint) {
			treeTwoToAppend = cutTreeInternal(closestTreeTrack, currentTimePoint, false);
			if(!treeTwoToAppend)
				throw TTTException("Internal error: cutTreeInternal() failed (2).");
			trackTwoToAppend = 1;
		}
		else {
			treeTwoToAppend = closestTree;
			trackTwoToAppend = closestTreeTrack->getTrackNumber();
		}

		// Append to each other
		if(!appendTreeToTrack(treeOneToAppend, trackOneToAppend, closestTreeTrack))
			throw TTTException("Internal error: appendTreeToTrack failed (1).");
		if(!appendTreeToTrack(treeTwoToAppend, trackTwoToAppend, selectedTrack))
			throw TTTException("Internal error: appendTreeToTrack failed (2).");

		// Update display
		ui.treeView->redrawTree(selectedTree->theTree.data());
		ui.treeView->redrawTree(closestTree.data());

		// Redraw tracks
		TTTManager::getInst().redrawTracks();
	}
	catch(const TTTException& e) {
		QMessageBox::information(this, "Note", "Swapping cells failed:\n\n" + e.what());
	}
}

void TTTAutoTrackingTreeWindow::treeViewHeightFactorChanged( int newValue )
{
	// Change in display
	if(ui.treeView->getTreeHeightFactor() != newValue)
		ui.treeView->setTreeHeightFactor(newValue);

	// Change in settings
	UserInfo::set(UserInfo::KEY_TREEVIEW_HEIGHTFACTOR, newValue);
}

void TTTAutoTrackingTreeWindow::treeViewWidthFactorChanged(int newValue)
{
	// Change value
	UserInfo::set(UserInfo::KEY_TREEVIEW_TRACKLINEDISTANCE, newValue);

	// Redraw trees
	ChangeCursorObject cc;
	ui.treeView->redrawAllTrees();
}

void TTTAutoTrackingTreeWindow::treeViewCellCircleRadiusChanged(int newValue)
{
	// Change value
	UserInfo::set(UserInfo::KEY_TREEVIEW_CELLCIRCLERADIUS, newValue);

	// Redraw trees
	ChangeCursorObject cc;
	ui.treeView->redrawAllTrees();
}

void TTTAutoTrackingTreeWindow::showRealTimeChanged( bool newValue )
{
	// Change in display
	ui.treeView->setShowRealTimeInTimeScale(newValue);

	// Change in settings
	UserInfo::set(UserInfo::KEY_TREEVIEW_SHOWREALTIME, newValue);
}

void TTTAutoTrackingTreeWindow::continuousTrackLinesChanged( bool newValue )
{
		// Change value
	UserInfo::set(UserInfo::KEY_TREEVIEW_TRACKLINESCONTINUOUS, newValue);

	// Redraw trees
	ChangeCursorObject cc;
	ui.treeView->redrawAllTrees();
}

void TTTAutoTrackingTreeWindow::timeLineFontSizeChanged(int newValue)
{
	// Change value
	UserInfo::set(UserInfo::KEY_TREEVIEW_TIMELINEFONTSIZE, newValue);

	// Redraw time line
	if(TreeViewTimeLine* timeLine = ui.treeView->getTimeLine())
		timeLine->updateDisplay();
}

void TTTAutoTrackingTreeWindow::treeViewTrackLineWidthChanged(int newValue)
{
	// Change value
	UserInfo::set(UserInfo::KEY_TREEVIEW_TRACKLINEWIDTH, newValue);

	// Redraw trees
	ChangeCursorObject cc;
	ui.treeView->redrawAllTrees();
}

void TTTAutoTrackingTreeWindow::updateTreeFragmentsTable()
{
	// Create model
	if(!m_fragmentsOverviewModel) {
		m_fragmentsOverviewModel = new TTTAutoTrackingTreeWindowFragmentsTableModel(this);
		ui.tbvTreeFragmentsOverview->setModel(m_fragmentsOverviewModel);
	}

	// Update model
	m_fragmentsOverviewModel->setFragments(frmAutoTracking->getTreeFragments());

	// Update hidden rows
	showHideUsedTreeFragmentsInTable();
}

void TTTAutoTrackingTreeWindow::showHideUsedTreeFragmentsInTable()
{
	if(!m_fragmentsOverviewModel)
		return;

	// Hide fragments used for trees, if desired
	int curRow = 0;
	for(auto it = m_fragmentsOverviewModel->getFragments().begin(); it != m_fragmentsOverviewModel->getFragments().end(); ++it) {
		TreeFragment* curFr = it->data();
		bool hideFragment = curFr != nullptr && ui.chkHideUsedFragments->isChecked() && curFr->isUsedForTTTTree();
		ui.tbvTreeFragmentsOverview->setRowHidden(curRow++, hideFragment);
	}
}

void TTTAutoTrackingTreeWindow::treeFragmentDoubleClicked(const QModelIndex& index)
{
	// Check row
	int row = index.row();
	auto& fragments = m_fragmentsOverviewModel->getFragments();
	if(row < 0 || row >= fragments.size())
		return;
	
	// Get fragment
	QSharedPointer<TreeFragment> fragment = fragments[row].toStrongRef();
	if(fragment.isNull())
		return;

	// Determine position and time point where fragment starts 
	TreeFragmentTrack* rootTrack = fragment->getRootNode();
	if(!rootTrack)
		return;
	TreeFragmentTrackPoint* firstTrackPoint = rootTrack->getFirstTrackPoint();
	if(!firstTrackPoint)
		return;
	int position = firstTrackPoint->getPositionNumber();
	int timePoint = firstTrackPoint->getTimePoint();

	// Open movie window at that position and time point
	QString posNumString = Tools::convertIntPositionToString(position);
	TTTManager::getInst().frmPositionLayout->setPosition(posNumString);
	TTTPositionManager *tttpm = TTTManager::getInst().getPositionManager(posNumString);
	if(!tttpm)
		return;
	int wl = std::max(0, tttpm->getWavelength());
	int zIndex = std::max(0, tttpm->getZIndex());
	if(tttpm->getPictures())
		tttpm->getPictures()->loadOnePicture(PictureIndex(timePoint, zIndex, wl)); // Make sure image is loaded at time point
	if(tttpm->frmMovie) {
		tttpm->frmMovie->show();
		tttpm->frmMovie->raise();

		tttpm->frmMovie->getMultiplePictureViewer()->centerTrack(rootTrack, timePoint); // Show movie window and center this track
	}
	TTTManager::getInst().setTimepoint(timePoint);	 // Change timepoint
		
	// Create tree from fragment
	Tree* newTree = frmAutoTracking->createTreeFromFragment(fragment);

	// Select tree
	if(newTree) {
		if(Track* rootTrackOfNewTree = newTree->getRootNode())
			setSelectedCell(rootTrackOfNewTree);
	}
}

void TTTAutoTrackingTreeWindow::debugCreateTree()
{
	//Tree* volvo = new Tree();
}

void TTTAutoTrackingTreeWindow::exportView()
{
	// Get filename
	QString fileName = QFileDialog::getSaveFileName(this, "Specify export file", QDir::homePath() + "/Documents/export.png", "Images (*.jpg, *.png)");
	if(fileName.isEmpty())
		return;

	// Adjust tree display, re-create trees to reflect changes, export view, restore tree display
	ChangeCursorObject cc;
	QColor cellCircleDefaultInnerColor = UserInfo::get(UserInfo::KEY_TREEVIEW_CELLCIRCLEDEFAULTINNERCOLOR, Qt::white).value<QColor>();	// Black inner circles (unselected cell)
	UserInfo::set(UserInfo::KEY_TREEVIEW_CELLCIRCLEDEFAULTINNERCOLOR, Qt::black);
	QColor cellCircleSelectedInnerColor = UserInfo::get(UserInfo::KEY_TREEVIEW_CELLCIRCLESELECTEDINNERCOLOR, Qt::blue).value<QColor>();	// Black inner circles (selected cell)
	UserInfo::set(UserInfo::KEY_TREEVIEW_CELLCIRCLESELECTEDINNERCOLOR, Qt::black);
	QColor cellCircleSelectedOuterColor = UserInfo::get(UserInfo::KEY_TREEVIEW_CELLCIRCLESELECTEDOUTERCOLOR, Qt::blue).value<QColor>();	// Black outer circles (selected cell)
	UserInfo::set(UserInfo::KEY_TREEVIEW_CELLCIRCLESELECTEDOUTERCOLOR, Qt::black);
	ui.treeView->redrawAllTrees();
	ui.treeView->setTrackNumbersVisible(false);
	ui.treeView->exportView(fileName);
	UserInfo::set(UserInfo::KEY_TREEVIEW_CELLCIRCLEDEFAULTINNERCOLOR, cellCircleDefaultInnerColor);
	UserInfo::set(UserInfo::KEY_TREEVIEW_CELLCIRCLESELECTEDINNERCOLOR, cellCircleSelectedInnerColor);
	UserInfo::set(UserInfo::KEY_TREEVIEW_CELLCIRCLESELECTEDOUTERCOLOR, cellCircleSelectedOuterColor);
	ui.treeView->redrawAllTrees();
	ui.treeView->setTrackNumbersVisible(true);

	// Done
	cc.restoreCursor();
	Tools::displayMessageBoxWithOpenFolder("Export completed.", "Done", fileName, true, this);
}

void TTTAutoTrackingTreeWindowFragmentsTableModel::setFragments(const QHash<int, QList<QSharedPointer<TreeFragment> > >& fragments)
{
	beginResetModel();

	// Clear old list
	m_fragments.clear();

	// Insert new list
	for(auto itHash = fragments.constBegin(); itHash != fragments.constEnd(); ++itHash) {
		const QList<QSharedPointer<TreeFragment>>& curList = *itHash;
		for(auto itList = curList.constBegin(); itList != curList.constEnd(); ++itList) {
			m_fragments.push_back(QWeakPointer<TreeFragment>(*itList));
		}
	}

	// Sort list
	sortInternal(0, Qt::DescendingOrder);

	endResetModel();
}

QVariant TTTAutoTrackingTreeWindowFragmentsTableModel::headerData(int section, Qt::Orientation orientation, int role /*= Qt::DisplayRole */) const
{
	// Only display role
	if(role += Qt::DisplayRole)
		return QVariant();

	// No vertical headers
	if(orientation == Qt::Vertical)
		return QVariant();

	// Horizontal headers
	switch(section) {
	case HLength:
		return QString("Length");
	case HPos:
		return QString("Pos.");
	case HStartTp:
		return QString("Start");
	//case HStopTp:
	//	return QString("Stop");
	case HNR:
		return QString("Nr");
	default:
		return QVariant();
	}
}

QVariant TTTAutoTrackingTreeWindowFragmentsTableModel::data(const QModelIndex & index, int role /*= Qt::DisplayRole */) const
{
	// Get fragment
	TreeFragment* fragment = 0;
	if(index.row() < m_fragments.size())
		fragment = m_fragments[index.row()].data();
	if(!fragment)
		return QVariant();

	// Get information
	if(role == Qt::DisplayRole) {
		switch(index.column()) {
		case HLength:
			return fragment->getNumberOfTrackPoints();
		case HPos:
			return fragment->getPositionNumber();
		case HStartTp:
			return fragment->getFirstTimePoint();
		//case HStopTp:
		//	return -1;
		case HNR:
			return fragment->getTreeFragmentNumber();
		default:
			return QVariant();
		}
	}

	return QVariant();
}

void TTTAutoTrackingTreeWindowFragmentsTableModel::sort(int column, Qt::SortOrder order /*= Qt::AscendingOrder */)
{
	// Check column index
	if(column >= NUM_COLUMNS)
		return;

	// Sort data
	beginResetModel();
	sortInternal(column, order);
	endResetModel();
}

void TTTAutoTrackingTreeWindowFragmentsTableModel::sortInternal(int column, Qt::SortOrder order /*= Qt::AscendingOrder */)
{
	// Sort m_fragments list
	std::sort(m_fragments.begin(), m_fragments.end(), [column, order](const QWeakPointer<TreeFragment>& a, const QWeakPointer<TreeFragment>& b)
		-> bool { 
			// Get fragments
			TreeFragment* fr1 = a.data();
			TreeFragment* fr2 = b.data();
			if(fr1 == 0 || fr2 == 0)
				return false;

			// Compare data
			if(order == Qt::AscendingOrder) {
				switch(column) {
				case TTTAutoTrackingTreeWindowFragmentsTableModel::HLength:
					return fr1->getNumberOfTrackPoints() < fr2->getNumberOfTrackPoints();
				case TTTAutoTrackingTreeWindowFragmentsTableModel::HPos:
					return fr1->getPositionIndex() < fr2->getPositionIndex();
				case TTTAutoTrackingTreeWindowFragmentsTableModel::HStartTp:
					return fr1->getFirstTimePoint() < fr2->getFirstTimePoint();
					//case TTTAutoTrackingTreeWindowFragmentsTableModel::HStopTp:
					//	return false;
				case TTTAutoTrackingTreeWindowFragmentsTableModel::HNR:
					return fr1->getTreeFragmentNumber() < fr2->getTreeFragmentNumber();
				default:
					return false;
				}
			}
			else {
				switch(column) {
				case TTTAutoTrackingTreeWindowFragmentsTableModel::HLength:
					return fr1->getNumberOfTrackPoints() > fr2->getNumberOfTrackPoints();
				case TTTAutoTrackingTreeWindowFragmentsTableModel::HPos:
					return fr1->getPositionIndex() > fr2->getPositionIndex();
				case TTTAutoTrackingTreeWindowFragmentsTableModel::HStartTp:
					return fr1->getFirstTimePoint() > fr2->getFirstTimePoint();
					//case TTTAutoTrackingTreeWindowFragmentsTableModel::HStopTp:
					//	return false;
				case TTTAutoTrackingTreeWindowFragmentsTableModel::HNR:
					return fr1->getTreeFragmentNumber() > fr2->getTreeFragmentNumber();
				default:
					return false;
				}
			}
	});
}


//
//void TTTAutoTrackingTreeWindow::wlSelectionBoxChecked( bool _on )
//{
//	// Enable/disable wavelengths table widget
//	ui.tbtWls->setEnabled(_on);
//
//	// Toggle groupbox for wl interval
//	ui.grbWlsInterval->setChecked(!_on);
//}
//
//void TTTAutoTrackingTreeWindow::wlIntervalBoxChecked( bool _on )
//{
//	// Enable/disable interval controls
//	ui.lblWlFrom->setEnabled(_on);
//	ui.spbWlFrom->setEnabled(_on);
//	ui.pbtWlSetFromToFirst->setEnabled(_on);
//	ui.lblWlTo->setEnabled(_on);
//	ui.spbWlTo->setEnabled(_on);
//	ui.wlSetToToLast->setEnabled(_on);
//
//	// Toggle groupbox for wl selection
//	ui.grbWlsSelect->setChecked(!_on);
//}

void TTTAutoTrackingTreeWindow::keyReleaseEvent(QKeyEvent* _event)
{
	// Use numpad0 to start manual tracking
	switch(_event->key()) {
	case Qt::Key_0:
		startManualTracking();
		break;
	default:
		QWidget::keyReleaseEvent(_event);
	}
}
