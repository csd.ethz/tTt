/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TTTTRACKING_H
#define TTTTRACKING_H

#include "ui_frmTracking.h"

// Project includes
#include "tttdata/cellproperties.h"
#include "tttio/exporthandler.h"
#include "tttdata/track.h"
#include "tttio/tttfilehandler.h"

// Qt includes
#include <QTimer>
#include <QString>
#include <QPoint>

// Forward declarations
class QStatusBar;
class QLabel;
class QSpinBox;
template <class T> class QVector;
class QPoint;
class Q3ListBoxItem;
class Q3ListViewItem;
class TreeDisplay;
class StyleSheet;
class TimeScale;
class TrackingMenuBar;
class TTTMovieExplorer;
class TTTConvertOldTrees;



///the filename for the backup file of the currently edited ttt file
///additionally, the date is added at the end
const QString BACKUP_FILENAME = "BACKUP" + TTT_FILE_SUFFIX;

///the interval for creating the backup file (in ms)
const int BACKUP_INTERVAL = 600000;		//10 minutes in milliseconds



/**
@author Bernhard
*	
*	The class for the tree display GUI
*	NOTE: the TreeDisplay class only manages displaying the tree, not the track & co. management!
*		this is still left here in class TTTTracking
*/

class TTTTracking: public QWidget, public Ui::frmTracking {

	Q_OBJECT

public:

	
	TTTTracking (QWidget *parent = 0, const char *name = 0);
	
	~TTTTracking ();
	
	/////sets the signal/slot connections between this and its contacting forms
	//void initForms();
	
	///sets the timepoints and initializes the timescale & co.; called once initially
	///@param _firstTP the first timepoint
	///@param _lastTP the last timepont
	///@param _addColony whether a new colony should be loaded if there is none yet loaded
	void setTimePoints (int _firstTP, int _lastTP, bool _addColony);

	void showEvent (QShowEvent *_ev);
	
	void closeEvent (QCloseEvent *);
	
	void paintEvent (QPaintEvent *);
	
	void resizeEvent (QResizeEvent *);
	
	void keyReleaseEvent (QKeyEvent *);
	
	///saves the current file to disk
	///the file format is compatible to VB
	///@param _withNewName whether it should be stored under a different name
	///@return success/cancel
	bool saveTTTFile (bool _withNewName = false);
	
	///loads the specified file into RAM and constructs the tree with a track array
	bool loadTTTFile (QString _filename);
	
	///displays the properties for the currently tracked cell in the property form
	void showCurrentProperties();
	
	///inserts the specified _filename in the colony listbox lboColony
	///@param _setCurrent: whether the current colony's name should be altered
	void updateColonyList (QString _filename, bool _setCurrent);
	
	/**
	 * calculates/refreshes the tree statistics
	 */
	void refreshStatistics();
	
	///stops the tracking mode; quits tracking in TTTMovie
	///the specified track stop reason is the button the user clicked
	void stopCellTracking();
	
	/**
	 * sets the caption of this form (with the filename, if provided, additionally)
	 * @param _filename the currently loaded file(name)
	 */
	void setFormCaption (const QString &_filename = "");
	
	///returns the users choice whether to save (false) or not save (true) the current tree
	bool SaveDialog();
	
	/**
	 * creates the necessary wavelength buttons for the current tree (needs to be called after the base position was set)
	 * note: if called again because a new tree was loaded, the old buttons are not overwritten but reused
	 */
	void createWavelengthButtons();

	/**
	 * sets the statusbar text to message, overwriting default 
	 * text until call to resetPermanentStatusbarMessage
	 * @param _message the message to be displayed
	 */
	void setPermanentStatusbarMessage(QString _message);

	/**
	 * sets the statusbar text to the current default text
	 */
	void resetPermanentStatusbarMessage();

	/**
	 * sets the statusbar text to message for time milliseconds
	 * @param _message the message to be displayed
	 * @param _time time in milliseconds
	 */
	void displayStatusBarMessage(QString _message, int _time = 1000);

	/**
	 * Get const pointer for treeview
	 */
	const TreeDisplay* getTreeView() const
	{ 
		return treeView; 
	}

	/**
	 * Get filename as displayed in caption (can be sth like "new colony", too)
	 */
	QString getFilenameFormCaption() const
	{
		return currentFilenameFormCaption;
	}

	/**
	 * Get which wavelengths should be inherited from mother cells
	 * @return pointer to QVector<bool> where index i refers to wavelength i + 1
	 */
	const QVector<bool> *getInheritedWLs() const;

	/**
	 * Get if adherence status should be inherited from mother cells
	 */
	bool getInheritAdherence() const;

	/**
	 * Get if differentiation status should be inherited from mother cells
	 */
	bool getInheritDifferentiation() const;

	/**
	 * Get if all cell properties shold be inherited from mother cells during tracking
	 */
	bool getInheritAllProperties() const;

	/**
	 * Set data changed (WARNING: call this function only if you really know what you are doing!)
	 * @param _dataChanged new value for data changed
	 */
	void setDataChanged(bool _dataChanged) {
		DataChanged = _dataChanged;
	}

	/**
	 * Update size of displayed tree
	 */
	void updateTreeSize();
	
public slots:

    /**
     * displays the vertical bar containing the time scale (own widget class!)
     */
	void displayTimeScale(); // (long int _timePointStart = -1, int _timePointEnd = -1);

	/**
	 * Updates the display to reflect change of current position or similar
	 */
	void updateDisplay();
	
    /**
     * redraws the tree picture in memory and updates its display on screen
     * (according to current zoom and region settings)
     * if _track is specified, only this track is redrawn
     */
	void drawTree (Track *_track = 0);

	/**
	 * @author: Bernhard
	 * Loads the history of the position roamings for the whole tree
	 * A detailed output description can be seen in the definition
	 */
	void updatePositionHistory();

    ///**
    // * displays a form where the user can select his/her own style wishes for the tree
    // */
    //void customizeTTT();

    /**
     * marks the current tree as (un)finished, depending on the parameter
     */
    void markTreeFinished (bool);

	/**
	 * Load all images of the currently opened tree (i.e. load every image in which a trackpoint was set)
	 */
	void loadAllImagesOfCurrentTree();

    /**
     * deletes the current track in the tree
     */
    void deleteTrack();

    /**
     * removes the stop reason from the current track so that the track can be refined
     */
    void deleteStopReason();

    /**
     * starts the tracking of a cell; sets TTTMovie into tracking mode
     * @param _start only used to distinguish between user and code trigger
     */
    void startCellTracking (bool _start = true);

    /**
     * opens a popup where the user can enter a comment for the currently selected track
     */
    void editCellComment();

    /**
     * show the form with the tree map
     */
    void showTreeMap();

    /**
     * shifts the active cell to the center of the display
     */
    void centerCell();

    /**
     * resets all shift and zoom settings to the default values (upper left corner and 100%)
     */
    void resetGraphics();

    /**
     * exports the fluorescent cells and their background into a file
     */
    void exportFluorescenceCells();

    /**
     * write various tree data to a file
     * currently implemented for Mr. Tonge, should be excluded to external tool (e.g. TreeAnalysis)
     * this method distinguishes the sender of the signal to find out about the desired data
     */
    void exportTreeData();


	/**
	 * Start assistant for loading autotracking results and creating .ttt trees from them
	 */
	void startAutoTrackingAssistant();

	/**
	 * Open movie explorer
	 */
	void openMovieExplorer();

	/**
	 * Open dialog to convert old trees
	 */
	void convertOldTrees();

	/**
	 * Launch TTTStats (or staTTTs) in a separate process
	 */
	void launchTTTStats();

	/**
	 * Launch AMT in a separate process
	 */
	void launchAMT();

	///loads the colony with the provided filename
	///the current tree is saved (user dialog), tree is reset and the new colony loaded
	void setColony (const QString&);

	/**
	 * Update display of backward tracking mode
	 */
	void updateBackwardTrackingModeDisplay();

private slots:
	
	/**
	 * displays the provided timepoint (is called by TTTManager)
	 * @param _newTimepoint the new (current) timepoint
	 * @param _oldTimepoint the timepoint that was valid before
	 */
	void setToTimepoint (int _newTimepoint, int _oldTimepoint);
	
	///displays the information belonging to CurrentTrack
	void displayTrackInformation();
	
	///sets the timescale to be redrawn in memory and displays the latest version
	///if the parameters are specified and the timescale has to be drawn for update,
	/// only the specified timepoint/wavelength is considered
	void updateTimeScale (unsigned int _timePoint = 0, int _waveLength = -1);
	
	///sets _track to be the current track
	void selectTrack (Track *_track);
	
	///adds a colony to the current experiment folder (means a new tTt-file)
	void addColony();	
	
	///connected to the closing of the comment popup; the comment is stored in the track
	void saveCellComment (QString);
	
	///called after the user toggled the selection mode button
    void selectRegion (bool _checked);
	
	///sets the boundaries and displays the latest timescale version
	void setLoadPictures (int _start, int _stop);
	
	///loads the selected timepoints (triggers a TTTMovie method)
	///@param _unload whether unloading was chosen (menu parameter, 1)
	void loadPictures (int _unload);
	
	///called after (un)loading is complete; updates the display
	void finished_un_Loading ();
	
	///called by the timepoint slider; updates the timepoint in TTTManager
	void setTimePoint (int _timePoint);
	
	///shows the window with the picture regions
	void showZoomWindow();
	
	///sets whether cell should be displayed in the region window
	void showCellsOnZoom (bool);
	
	///raises/hides frmMovie
	void showMovieForm (bool);
	
	///sets whether the colocation lines should be displayed (time consuming)
	void showColocation (bool);
	
	///sets whether the speed markers should be displayed (time consuming)
	void showSpeed (bool);
	
	///called by a TreeDisplay instance
	///selects _track and starts tracking it
	void handleCellDoubleClick (Track *_track);
	
	///saves the current tree to disk
	///@param _saveas sender parameter, whether the file should be saved with a different name
	void saveCurrentFile (int _saveas);
	
	///delets all track points for the current cell (leaves an "untracked" cell)
	void deleteAllTrackPoints();
	
	/////adds the currently configured cell properties to the recently added TrackPoint
	/////@param _track the current track
	/////@param _tp the timepoint
	/////@param _cp the properties (contains rude information, updated here)
	/////the properties are received by querying TTTCellProperties
	/////@return success
	//bool addCellProperties (Track *_track, int _tp, const CellProperties& _cp);
	
	///sets the user selected radius for colocation calculation
	void setColocationRadius (int _radius);
	
	///sets the clicked wavelength to be (un)loaded for the selected timepoints
	///@param _wl the wavelength (== button index of the wavelength button group)
	void selectWaveLength (int);
	
	///displays the specified string in the tooltip frame, positioned at the specified position
	///called by TreeView when the user clicks on an asterisk to see the associated comment
	void showComment (QString, QPoint, int);
	
	///displays frmMovie where the user can select a radius for the colocation
	/// detection by directly entering it via mouse
	void selectCoLocationRadius();
	
	///sets the specified int as the colocation radius - slider value
	///called after the user selected a radius via frmMovie
	void displayRadius (int);
	
	///sets whether all default wavelength pictures should be loaded with selected 
	/// fluorescent pictures
	void setDefaultSelection (bool);
	
	///stores the currently visible view into a bmp-file
	void exportTree();
	
	///sets the (un)load start/stop timepoint after manual entry in one of the line edit boxes + return press
	void setLoadTimePoint();
	
	///sets the speed amplification value
	void setSpeedAmplification (int);
	
	///normalizes the speed amplification value
	void normalizeSpeed();
	
	///handles a mouse click on the colony list box
	void handleColonyClick (int, Q3ListBoxItem *, const QPoint &);
	
	///sets the current interval for (un)loading pictures (for the complete selected timepoint region!)
	void setLoadInterval (int);
	
	///actually selects the pictures (sets the flag) for the ones marked by mouse or keyboard
	///@param _deselect whether deselection instead of seletion was chosen (menu parameter, 1)
	void selectPictures (int _deselect);
	
	///updates the picture selection counter and displays the current value
	void updateSelectionCounter (unsigned int, int, bool);
	
	///called when TreeWindowHeightFactor in Tree changes
	void updateTWHF (int);
	
	///adjusts the size of the sending control according to whether is has the focus
	/// or not (provided in the bool parameter)
	void adjustControlWidth (bool);
	
	///saves a backup of the currently edited colony in the current user's home directory
	void createBackup();
	
	///show the listbox with all colonies
	void selectColony();
	
//	///sets the "stamp line" flag for the tree (heritage lines) to the parameter
//	void toggleStampLine (bool);
	
	///actions, if the RAM runs below limit (MINIMUM_RAM in PictureArray.h) while loading
	/// pictures
	///-> stop loading, display message
	void watchRAM();
	
	///sets the properties the user set in the property form for the current cell for a certain timepoint range
	///useful for setting properties at once after tracking, so the cell does not have to be retracked
	void setCellProperties();
		
	///displays a form where the user can enter the properties for a cell between a certain timepoint range
	///the properties displayed at popup are the ones of the first timepoint of the changing range
	void showCellProperties();
	
	///switches the display of the generation time next to each cell heri
	void showGenerationTime (bool _show);
	
	///called when the user clicks "Apply" or "Close" in the tree style editor form
	///applies the provided style sheet to the current tree
	///@param _ss the StyleSheet object
	void applyStyleSheet (StyleSheet &_ss);

	/**
	 * called when the user clicks on the buttons "(De)select cells"
	 * selects those cells which are set to be selected (cf. cellTypeSelected())
	 */
	void selectCells();
	
    /**
    * called on cell selection option click
    * sets the chosen cell type to be selected
    */
    void setCellTypeSelection();
        
	/**
	 * called when the user clicks "calculate" for position statistics
	 * calculates the position statistics
	 */
	void calculatePositionStatistics();
	
	/**
	 * called on visibility check toggle
	 * toggles the visibility currently available tree items in the list view at once
	 * @param _visible whether all items should be set visible or not
	 */
	void setAllTreeElementsVisibility (bool _visible);
	
	/**
	 * sets the visibility of the selected tree item
	 * @param _item the selected listview item
	 */
	void setTreeItemVisible (Q3ListViewItem *_item);
	
	/**
	 * loads the last autosaved ttt file (confer BACKUP_FILENAME)
	 * NOTE: this colony is not necessarily of the current experiment/position -> the user must be able to dispose of it
	 */
	void loadLastAutosavedColony();
	
	/**
	 * called when the current position was changed; thus, the timescale can be updated
	 * @param _ the old position key
	 * @param _ the new position key
	 */
	void currentPositionChanged (int, int);
	
	/**
	 * converts the wrongly saved inverted coordinates for the currently open file
	 */
	void convertWrongInvertedCoordinates();
	
	/**
	 * lets the user select a treeminer file
	 */
	void selectTreeMinerFile();
	
	/**
	 * loads the selected treeminer file and its attached explanation and results file
	 */
	void loadTreeMinerFile();
	
	/**
	 * overlays the trees with the assigned clusters
	 */
	void overlayClusters();
	
	/**
	 * displays a window with the frequent trees that were mined from the treeminer file
	 */
	void showFrequentTrees();
	
	/**
	 * reads track data from an external file
	 * currently supported formats: Matlab
	 */
	void readExternalTracks();
	
	/**
	 * replaces the original track coordinates with the coordinates from the external file
	 * exceptions can be given; see code for details
	 */
	void replaceTracksWithExternal();
	
	/**
	 * measures an arbitrary time interval: on toggle->true the timer starts counting, on toggle->false, the timer stops and displays the measured interval in seconds
	 * @param _start true: timer starts; _stop: timer stops
	 */
	void measureTime (bool _start);
	
	/**
	 * @todo
	 * loads for all 'interrupted' cells the pictures (some before, some after) around their tracking stop timepoint, in all affected positions
	 */
	void loadAllInterruptedCells();
	
	/**
	 * corrects the coordinates of the tracks that were incorrectly stored due to a misset tv/ocular factor
	 */
	void correctFactorProblemTracks();
	
	/**
	 * deletes the positions stored for each trackpoint in the current tree
	 * note: does not change the coordinates, thus the missing positions are calculated (as before)
	 */
	void eraseStoredPositions();
	
	/**
	 * shows a dialog where the user can merge trees
	 */
	void mergeTrees();
	
	/**
	 * opens further cell property options
	 */
	void openFurtherCellOptions();
	
	/**
	 * toggle the selected wavelength in the cell properties
	 * @param _wl the wavelength that was clicked (on or off must be decided in the slot)
	 */
	void setCellPropertyWavelength (int);
	
	/**
	 * exports snapshots of all trees of the current position
	 * for details confer implementation and exportTree()
	 */
	void exportAllTreeSnapshots();

	/**
	 * Set start timepoint for loading to first timepoint
	 */
	void setLoadingStartToFirstTp();

	/**
	 * Set end timepoint for loading to last timepoint
	 */
	void setLoadingEndToLastTp();

	/**
	 * Toggle wavelength inheritance
	 * @param _on turn inheritance on or off
	 */
	void toggleWavelengthInhertiance(bool _on);

	/**
	 * Open next tree in current position
	 */
	void openNextTree();

	/**
	 * Open previous tree in current position
	 */
	void openPreviousTree();

	/**
	 * Display changelog of tTt (Not of the tree!)
	 */
	void displayTTTChangelog();

	/**
	 * Open movie window of current position
	 */
	void openMovieWindow();

	/**
	 * Go to first position of tree (position where first trackpoint was set)
	 */
	void goToFirstPosOfTree();

	/**
	 * Toggle inherit all cell features from mother cells while tracking
	 * @param _on true if enabled
	 */
	void setInheritAllProperties(bool _on);

	/**
	 * Toggle if cell lines should be continous (vertically) and not only for tracked timepoints
	 * @param _on true if enabled
	 */
	void setDrawContinuousCellLines(bool _on);

	/**
	 * Test button (not for release builds)
	 */
	void test();
	
private:

	///wavelengths that should be inherited (i.e. when continuing tracking, wavelength properties should be inherited from last trackpoint/mother cell)
	QVector<bool>* inheritedWLs; 
	
	///special spin boxes without keyboard control
    QSpinBox *spbZoomFactor;
    QSpinBox *spbLoadInterval;
	
	///the control for displaying the tree
	TreeDisplay *treeView;
	
	///the timescale pointer
	TimeScale *timeScale;
	
	///whether for a display of the timescale it has to be redrawn in memory for data change
	bool TimeScaleChanged;
	
	///whether the last saved data was changed (necessary for saving security question)
	bool DataChanged;
	
	///the filename of the currently loaded colony (without path)
	///for a new colony, it is ""
	QString CurrentColonyName;
	
	///the number of tracks in the tree
	int TrackCount;
	
	///the parameters for (un)loading pictures
	int StartLoading;
	int EndLoading;
	bool LoadingFlag;
	int LoadInterval;
	
	///whether any file was loaded (then no new colony has to be added initially)
	bool TTTFileLoaded;
	
	///contains the file name for the current ttt file
	QString currentFilename;

	///contains the file name as it should be displayed in the form's title
	QString currentFilenameFormCaption;
	
	///@todo adjust with MAX_WAVE_LENGTH -> arbitrary
	///uchar array for the wavelengths of the timepoints that are selected
	///meaning of the 8 bits of each item:
	///bit 0    - wavelength selected independently
	///bit 1-7  - wavelength selected, but only those timepoints that are also selected in wavelength (bit-1)
	///example:
	///if wavelength 2 should only be loaded with wavelength 1 and 3, then 
	/// wavelength[2] = "00101000" = 40 = 2^(6-1) + 2^(6-3)
	/// as the bits number 3 and 5 have to be set for 1 and 3 (in big endian)
	///note: it does not make much sense to have the first and other bits set in a wavelength (they should be used exclusively),
	///       yet the user can do this on his own responsibility
	uchar WaveLengthSelected [MAX_WAVE_LENGTH + 1];
	
	///whether the speed frame should be displayed
	bool ShowSpeed;
	
	///whether the colocation display is active
	bool ShowColocation;
	
	// /the TTTFileHandler instance, necessary for storing and reading any file
	//TTTFileHandler *fileHandler;
	
	///the export handler instance for exporting the fluorescence data to disk
	ExportHandler exporter;
	
	///the speed amplifier (the calculated speed is multiplied by this value)
	int SpeedAmplifier;
	
	///the number of currently selected pictures
	int SelectionCounter;
	
	///timer for creating a ttt file with the current basename in the home directory
	///this is done every 10 minutes to ensure no data is lost
	///if the user explicitly saves the file, this backup file is NOT deleted - it
	/// is, however, overwritten the next time a colony is tracked, as the name
	/// of the file does not change
	QTimer tmrBackup;
	
	///holds the cell type that should be (de)selected when the user clicks on "(De)select cells"
	CellType currentCellType;
	
	///decides whether on a timepoint slider value change, the global timepoint setting routine should be called
	bool call_slider_setTimePoint;

	///window layout already loaded
	bool windowLayoutLoaded;

	///menubar
	TrackingMenuBar *menuBar;

	///statusbar
	QStatusBar *statusBar;
	QLabel *permanentStatusMessage;
	QLabel *lblBackwardTrackingMode;
	QPushButton *pbtStatusBarOpenMovieWindow;
	QPushButton *pbtStatusBarGoToFirstPos;

	// Default message; usually the current position
	QString defaultMessage;

	// Movie explorer window (can be 0)
	TTTMovieExplorer* movieExplorer;

	// Convert old trees window (can be 0)
	TTTConvertOldTrees* convertOldTreesWindow;
	
///private functions	
	
	///displays the "Comment" button in bold/not bold, depending on the existence
	/// of a comment at _timePoint
	///@return: whether there is a comment or not
	bool showCommentEmbold (int _timePoint);
	
	///sets the provided _props for _track between _firstTP and _lastTP and 
	/// its children resp., if desired (_setChildren)
	void setProperties (Track *_track, int _firstTP, int _lastTP, CellProperties _props, bool _setChildren);
	
	/**
	 * refreshes the legend for the tree (called whenever the stylesheet has changed or the used colors have changed)
	 */
	void updateLegend();

	/**
	 * Update changelog table contents. Called when tree has been opened, closed or changelog has been changed
	 */
	void updateChangelog();

	/**
	 * Clear changelog
	 */
	void clearChangelog();
	
	///friend declarations
	///********************
	friend class TTTMovie;
	friend class PictureView;		// -------------------Oliver------------------
};


#endif
