/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "propagatingqframe.h"

PropagatingQFrame::PropagatingQFrame (QWidget *_parent)
        : QFrame (_parent)
{
        draw_img = 0;
}

void PropagatingQFrame::keyPressEvent (QKeyEvent *_ev)
{
        emit keyPressed (_ev);
}

void PropagatingQFrame::keyReleaseEvent (QKeyEvent *_ev)
{
        emit keyReleased (_ev);
}

void PropagatingQFrame::mouseDoubleClickEvent (QMouseEvent *_ev)
{
        emit mouseDoubleClicked (_ev);
}

void PropagatingQFrame::mouseMoveEvent (QMouseEvent *_ev)
{
        emit mouseMoved (_ev);
}

void PropagatingQFrame::mousePressEvent (QMouseEvent *_ev)
{
        emit mousePressed (_ev);
}

void PropagatingQFrame::mouseReleaseEvent (QMouseEvent *_ev)
{
        emit mouseReleased (_ev);
}

void PropagatingQFrame::paintEvent (QPaintEvent *)
{
        if (draw_img) {
                QPainter p (this);
                p.drawImage (0, 0, *draw_img);
                p.end();
        }
}
