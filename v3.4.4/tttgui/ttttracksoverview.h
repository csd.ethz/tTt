/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TTTTRACKSOVERVIEW_H
#define TTTTRACKSOVERVIEW_H

#include "ui_frmTracksOverview.h"

#include "tttbackend/tttpositionmanager.h"
#include "tttbackend/tools.h"
#include "myqtableitem.h"

#include <qpushbutton.h>
#include <qcombobox.h>
#include <q3table.h>

#include <qclipboard.h>

//#include "qwt/qwt_plot.h"           // --------------- Konstantin ----------------
//#include "qwt/qwt_interval_data.h"  // --------------- Konstantin ----------------
       // --------------- Konstantin ----------------
//#include "qwt_interval_data.h"  // --------------- Konstantin ----------------

//Added by qt3to4:
#include <Q3PtrList>
//#include "tttstatsgui/histogram_item.h"     // --------------- Konstantin ----------------
#include "tttbackend/mathfunctions.h"      // --------------- Konstantin ----------------

#include <QWidget>

class QwtPlot;

enum TTO_DISPLAY_MODE {TTO_DISPLAY_MODE_FLUORESCENCE, TTO_DISPLAY_MODE_TRACKING_DATA};


class TTTTracksOverview: public QWidget, public Ui::frmTracksOverview {

Q_OBJECT

public:
	TTTTracksOverview(QWidget *parent = 0, const char *name = 0);

	/**
	 * display data relevant for detecting fluorescence tracking errors, and other purposes
	 * Dirk (Feb. 2010): "preview on fluorescence Excel file"
	 * @param _wavelength display the data for the selected wavelength
	 */
	void showFluorescence (int _wavelength);
	
	/**
	 * sets the position manager currently necessary (called by the displaying form)
	 * @param _tttpm the position manager
	 */
	void setPositionManager (TTTPositionManager *_tttpm);
	
public slots:
	
	/**
	 * display data relevant for detecting tracking errors, and other purposes
	 */
	void showTrackingData();
	
	/**
	 * show fluorescence data for default export wavelength
	 */
	void showFluorescence();
	
private slots:
		
	/**
	 * jump to the currently selected event (timepoint, tracks, location, ...)
	 */
	void jumpToEvent();
	
	/**
	 * sets the current wavelength
	 * @param _wl the wavelength as string
	 */
	void setWavelength (const QString &_wl);
	
	/**
	 * copies the current selection of the table to the clipboard; happens after each selection change
	 */
	void copyTableToClipboard();
	
	
	
private:
	
	///the current position manager (necessary for a lot of data)
	TTTPositionManager *tttpm;
	
	///the currently active display
	TTO_DISPLAY_MODE dispmode;
	
	// --------------- Konstantin ----------------
	/// pointer to plot elements for delete
	/* TODO
	Q3PtrList<HistogramItem> barPointers;
	*/
	Q3PtrList<QwtPlot> plotPointers;
	
	
	// --------------- Konstantin ----------------
	/// draws two columns from table
	void drawBarChart(Q3ValueVector<double> _v1, Q3ValueVector<double> _v2);
	

	/* TODO
	// --------------- Konstantin ----------------
	HistogramItem* getHistogramItem(Q3ValueVector<double> _v, double _positionOfBar, QColor _c);
	*/
	
//private methods
	
	/**
	 * loads all available wavelengths for the current position manager
	 */
	void loadWavelengths();
};

#endif
