/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "treeviewcellcircle.h"

// Qt
#include <QPen>

// Project
//#include "tttbackend/treeviewsettings.h"
#include "treeview.h"
#include "treeviewtrackitem.h"
#include "treeviewtreeitem.h"
#include "tttbackend/itrack.h"
#include "tttdata/userinfo.h"


TreeViewCellCircle::TreeViewCellCircle( TreeViewTrackItem* _parent )
	: QGraphicsEllipseItem(_parent)
{
	// Init variables
	//displaySettings = TreeViewSettings::getInstance();
	trackItem = _parent;
	mouseHoveringOver = false;
	movingTree = false;

	// Check if this is the mother cell
	if(trackItem->getTrack()->getTrackNumber() == 1)
		isMotherCell = true;
	else
		isMotherCell = false;

	// Enable hover events
	setAcceptHoverEvents(true);

	// Tooltip
	if(isMotherCell)
		setToolTip("Use left mouse button to select this cell, use right mouse button to start or continue tracking this cell\nHold down the left mouse button and move the mouse to the left or right to move the tree accordingly");
	else
		setToolTip("Use left mouse button to select this cell, use right mouse button to start or continue tracking this cell");

	// Position and Z-Value
	int pos = UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TREEVIEW_TRACKLINEWIDTH).toInt();
	setPos(pos / 2, 0);
	setZValue(TreeView::Z_CELL_CIRCLE);

	// Other stuff
	updateDisplay();
}

void TreeViewCellCircle::updateDisplay()
{
	// Size
	int radius = UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TREEVIEW_CELLCIRCLERADIUS).toInt();
	setRect(-radius, -radius, 2*radius, 2*radius);

	// Outline
	QPen pen(Qt::SolidLine); 
	pen.setWidth(UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TREEVIEW_CELLCIRCLEOUTLINEWIDTH).toInt());
	
	// Check if cell is selected
	if(trackItem->isSelected()) {
		setBrush(UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TREEVIEW_CELLCIRCLESELECTEDINNERCOLOR).value<QColor>());
		pen.setColor(UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TREEVIEW_CELLCIRCLESELECTEDOUTERCOLOR).value<QColor>());
	}
	else {
		// Check if mouse is in front
		if(mouseHoveringOver)
			pen.setColor(UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TREEVIEW_CELLCIRCLEMOUSEHOVEROUTERCOLOR).value<QColor>());
		else
			pen.setColor(UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TREEVIEW_CELLCIRCLEDEFAULTOUTERCOLOR).value<QColor>());

		setBrush(UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TREEVIEW_CELLCIRCLEDEFAULTINNERCOLOR).value<QColor>());
	}

	/* 
	Hard coded coloring used for export for open house images (04/2014)
	
	int trackNum = trackItem->getTrack()->getTrackNumber();
	if(trackNum == 1 || trackNum == 2 || trackNum == 4 || trackNum == 9 || trackNum == 18 || trackNum == 36 || trackNum == 73 || trackNum == 146 || trackNum == 292 || trackNum == 584 || trackNum == 1168) {
		setBrush(QBrush(qRgb(246, 74, 3), Qt::SolidPattern));
		pen.setColor(qRgb(246, 74, 3));
	}
	else if(trackNum == 3 || trackNum == 6 || trackNum == 13 || trackNum == 27 || trackNum == 54 || trackNum == 109 || trackNum == 218 || trackNum == 437 || trackNum == 875 || trackNum == 1751 || trackNum == 1168) {
		setBrush(QBrush(qRgb(81, 207, 172), Qt::SolidPattern));
		pen.setColor(qRgb(81, 207, 172));
	}
	else {
		setBrush(QBrush(qRgb(172, 172, 172), Qt::SolidPattern));
		pen.setColor(qRgb(172, 172, 172));
	}*/

	// Set pen
	setPen(pen);
}

void TreeViewCellCircle::mouseReleaseEvent( QGraphicsSceneMouseEvent* _event )
{
	// Stop tree movement
	if(movingTree && _event->button() == Qt::LeftButton) {
		movingTree = false;

		// Update scene rect in view
		trackItem->getTreeItem()->getTreeView()->updateSceneRect();
	}

	// Delegate
	trackItem->notifyMouseReleaseEvent(_event->button());
}

void TreeViewCellCircle::mousePressEvent( QGraphicsSceneMouseEvent *_event )
{
	// Prevent this event from starting mouse navigation in TreeView
	trackItem->getTreeItem()->getTreeView()->setIgnoreNextMousePressEvent();

	// Start moving tree if mother cell
	if(isMotherCell && _event->button() == Qt::LeftButton) {
		movingTree = true;
		//lastPosition = mapToScene(0, 0);
	}
}

void TreeViewCellCircle::hoverEnterEvent( QGraphicsSceneHoverEvent* _event )
{
	// Indicate that circles can be selected
	mouseHoveringOver = true;
	updateDisplay();
}

void TreeViewCellCircle::hoverLeaveEvent( QGraphicsSceneHoverEvent* _event )
{
	// Indicate that circles can be selected
	mouseHoveringOver = false;
	updateDisplay();
}

void TreeViewCellCircle::mouseMoveEvent( QGraphicsSceneMouseEvent* _event )
{
	// Move tree
	if(movingTree) {
		// Get distance in x direction
		float deltaX = _event->scenePos().x() - _event->lastScenePos().x();

		// Update position of tree
		trackItem->getTreeItem()->moveBy(deltaX, 0);
	}
}
