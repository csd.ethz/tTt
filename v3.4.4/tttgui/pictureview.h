/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PICTUREVIEW_H
#define PICTUREVIEW_H

// Project includes
#include "tttdata/symbol.h"
#include "mycircle.h"
#include "tttgui/clickableqgraphicsellipseitem.h"
#include "tttdata/stylesheet.h"

// Qt includes
#include <QColor>
#include <QString>
#include <QGraphicsView>
#include <QPointF>
#include <QVector>
#include <QRectF>
#include <QRect>
#include <QCursor>
#include <QPainter>

// OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>


// Forward declarations
class MultiplePictureViewer;
class TTTPositionManager;
class PictureContainer;
class StyleSheet;
class Track;
class MovieNextPosItem;
class TreeFragment;
class ClickableQGraphicsEllipseItem;
class AutoTrackingCellCircle;

///Z values for the graphical parts
const qreal ZVALUE_TRACKCIRCLE = 1.0;
const qreal ZVALUE_SYMBOL = 1.0;
const qreal ZVALUE_CELLPATH = 0.9;
const qreal ZVALUE_ORIENTATION_BOX = 0.6;
const qreal ZVALUE_WAVELENGTH_BOX = 0.5;
const qreal ZVALUE_OUTOFSYNC_BOX = 0.4;
const qreal ZVALUE_BACKGROUND_PICTURE = 0;

// Distance of out of sync box to the left
const qreal OUT_OF_SYNC_DISTANCE_LEFT = 20.0;
const qreal OUT_OF_SYNC_DISTANCE_TOP = 3.0;

///the default size for the ellipse indicating a track (in pixels)
const float CELLCIRCLESIZE = 25.0f;

const QColor ACTIVE_CELL_COLOR = Qt::red;
const QColor INACTIVE_CELL_COLOR = UNIFORM_CELL_COLOR;//Qt::yellow;
const QColor MOTHER_CELL_COLOR = Qt::magenta;

const QColor OrientationBoxColor = Qt::magenta;
//const QColor MouseCircleColor = Qt::yellow;
const QColor RadiusLineColor = Qt::green;
const QColor ColocationCircleColor = RadiusLineColor;

///the color sequence when loading exernal colonies into the display and for other purposes
const QColor ColonyColor[] = {	Qt::white, Qt::gray, Qt::red, Qt::darkRed, Qt::green, 
								Qt::darkGreen, Qt::blue, Qt::darkBlue, Qt::cyan, Qt::darkCyan, 
								Qt::magenta, Qt::darkMagenta, Qt::darkYellow, Qt::gray};


///constants for declaring the data type of graphical elements
const int GRAPHICSDATA_USAGE = 0;
const int GRAPHICSDATA_CELLNUMBER = 1;
const int GRAPHICSDATA_SYMBOL_INDEX = 2;
const int GRAPHICSDATA_COLONY_START_INDEX = 3;

///these are sometimes used as "categories", subsuming more than only the named items, making it easy to delete connected items
const QString GRAPHICSDATA_USAGE_TRACKCIRCLE = "TrackCircle";	// Note: these items are "recycled" to avoid time consuming deletions (see clearView() for details)
const QString GRAPHICSDATA_USAGE_PICTURE = "Picture";
const QString GRAPHICSDATA_USAGE_SYMBOL = "Symbol";
const QString GRAPHICSDATA_USAGE_CELLPATH = "CellPath";
const QString GRAPHICSDATA_USAGE_SELECTED_COLONY_START = "Selected colony start";


class MultiplePictureViewer;

/**
@author Bernhard
*	
*	This class is a widget to display a picture.
*	It contains some access functions and a little overhead to deal with position management for tracking
*	There exists one instance for each wavelength for each position.
*	In single wavelength mode they are stacked above each other and just raised on demand
*	In multiple mode they are laid out in a grid 
*	Additionally there are functions to shift the pictures in order to deal with multiple shifts
*	Key and mouse events are processed via signals to TTTMovie, where they are sent to all 
*	 existing instances of PictureView to handle them in all instances
*/


class PictureView : public QGraphicsView
{
Q_OBJECT
public:
    PictureView (MultiplePictureViewer *_multiPic, int _number, QWidget *_parent, TTTPositionManager *_positionManager, bool _overlay = false);
	
	PictureView();
	
	~PictureView();
	
	void keyPressEvent (QKeyEvent *_ev);

	void keyReleaseEvent (QKeyEvent *);
	
	void mousePressEvent (QMouseEvent *);
	
	void mouseMoveEvent (QMouseEvent *);
	
	void mouseReleaseEvent (QMouseEvent *);
	
	void mouseDoubleClickEvent (QMouseEvent *);
	
	//void paintEvent (QPaintEvent *);
	
	void wheelEvent (QWheelEvent *);
	
	void enterEvent (QEvent *);
	
    void resizeEvent (QResizeEvent *);

    void scrollContentsBy (int _dx, int _dy);

    void drawPicture (const PictureContainer *_picContainer, bool _overlay = false);

    /**
        * clear the items which have _dataValue associated with _dataKey
        * if the key is -1 (default), all items are deleted
        * for values, refer to constants declared above
        */
    void clearView (int _dataKey = -1, QString _dataValue = "");

    void setZoom (int _zoomPercent = 100, float _zoomX = -1.0f, float _zoomY = -1.0f);

    //void toggleBWAdjust (bool _on);

    void setOrientationBoxVisible (bool _visible);
    void setOrientationBoxPosition (QPointF _pos);

    void setWavelengthBoxVisible (bool _visible);

    bool isVisible() const
            {return isShown;}

        ///draws the current picture, regarding the display settings
	///@param _pd: the paint device in which the picture should be painted
	///@param _colorChannel: the index of the color channel (red [0], green [1] or blue [2]), in which it
	///						 should be drawn
	///@return 1 -> picture exists, painting succeeded; 2 -> no picture was found at the current timepoint (the current one is redrawn); 0 -> something else went wrong
        //int draw (PictureView *_pv = 0, int _colorChannel = -2);
	
	/////clears the view
 //       void clearX();
	
	///draws a little box to help the user coordinate pictures in multiple wavelength mode
        //void drawOrientationBox();
	
	/**
	 * draws the trackpoints (only called if there exist some!)
         * // @param _trackSize the cell circle radius
         * // @ param _showNumbers whether the numbers of the tracks/cells should be displayed
         * // @param _showCellBackground whether the background circle should also be displayed (if present)
         * // @param _useCellSize whether the cell radius stored in the TrackPoint instances should be used instead of the current size
         * // @param _pd the painting device into which the tracks should be painted (0 = default = THIS)
	 */
        void drawTracks (/*QPaintDevice *_pd = 0*/);
	
	/**
	 * starts tracking mode for this view
	 * @param _isInMainMovieWindow whether this view is in the current position's movie window
	 * @param _setBackground whether the background should be tracked instead of the cells
	 */
	void startTracking (bool _isInMainMovieWindow, bool _setBackground = false);
	
	///stops tracking mode for this view
	void stopTracking ();
	
	///shifts the current picture with _shift
        ///@return: the actual shift (there can be a cutoff, if the shift would exceed the boundaries)
        QPointF shift (QPointF _shift);
	
	///sets the orientation box to _pos, without regard if the widget has the mouse focus
	///_pos is already in usable coordinates from the upper left edge and needs not to be mapped
        //void positionOrientationBox (QPointF _pos);
	
	///sets the orientation box to _position (called by frmMovie)
	///_position is in screen coordinates and needs to be mapped to widget coordinates
	///this procedure does nothing if the widget has not the mouse focus
        //void setOrientationBox (QPointF _position);
	
	///sets whether the orientation box is shown and displays/removes it
        //void setOrientationShow (bool _show);
	
	///whether the box with the picture is visible (wavelength is selected in frmMovie)
	bool getShow() const
                {return isShown;}
	void setShow (bool _show = true) 
                {isShown = _show;}
	
        // /displays the black/white point lines and the gamma function
        // /those are editable for the user if _edit == true
        //void toggleGraphicAdjust (bool _edit = true);
	
        // /hides the black/white point lines and the gamma function
        //void stopGraphicAdjust();
	
	///sets whether all cells should be displayed with the same color
	void setAllCellsUniform (bool);
	
	///sets the current view in radius selection mode
	/// where the user can define a distance on a picture
	/// with a left and right mouse click
	///the stop of the radius selection mode is triggered by a right mouse click
	void startRadiusSelectionMode();
	
	///starts the position selection mode for the current symbol
	///as denotion, the currently selected symbol is drawn along with the mouse cursor
	/// and a click with the left button saves the position
	void receiveSymbolPosition (Symbol);
	
//	///sets the attribute "symbols" to be able to draw the symbols for the current
//	/// timepoint fast
//	void setCurrentSymbols (Q3ValueVector<Symbol> _symbols);
	
	///draws the current symbols (for the current timepoint)
	void drawCurrentSymbols();
	
	///tells this widget which PictureView has the mouse focus currently
	/// (necessary to decide which mouse position is correct during tracking)
	// /@param: the widget, its index and the pressed mouse buttons
	void setMouseWidget (PictureView *);
	
	///displays a circle around the current cell to indicate the provided radius
	void showColocationRadius (int _radius);
	
//	///draws the index of the current wavelength in the top left corner
//	///@param _draw whether the index should be drawn (true) or not (false)
//	void drawIndex (bool _draw);
	
//	///draws the specified circles, set by middle point and radius
//	///@param _rop: specifies the raster operation
//	///@param _connect: whether the circles should be connected with a line (dashed style)
//	///@param _store: whether the drawn circles should be stored in the internal array to be redrawn on paintEvent()
//        void drawCircles (Q3ValueVector<QPointF> _circles, float _radius, QColor _color, QPainter::CompositionMode _rop = QPainter::CompositionMode_Source, bool _connect = false, bool _store = false);
	
	///draws the circles from the MyCircle objects
        void drawCircles (QVector<MyCircle> _circles); //, QPainter::CompositionMode _compMode = QPainter::CompositionMode_Source);
	
	/* *
	 * draws the cell path (stored in the vector)
	 * @param _circles a vector of circles that contain the living (tracked) positions of a cell
	 * @param _persistent whether the path should be attached to the currently stored paths
	 */
        //void drawCellPath (QValueVector<QPointF> _circles, bool _persistent = true);
	
	/**
	 * starts a mode in which the user can select the location of a background track
	 * after a mouse click, the signal singleBackgroundSet() is emitted
	 */
	void startSingleBackgroundSelectionMode();
	
	/**
	 * sets whether the track circles are visible for this wavelength
	 * @param _showTracks true => tracks visible; false => hidden (numbers as well!)
	 */
	void setShowTracks (bool _showTracks);
	
	/**
	 * @return whether the track circles are visible for this wavelength
	 */
	bool tracksShown() const;
	
	/**
	 * sets whether the track numbers are shown for this wavelength
	 * @param _showTrackNumbers true => numbers visible; false => hidden
	 */
	void setShowTrackNumbers (bool _showTrackNumbers);
	
	/**
	 * @return whether the track numbers are shown for this wavelength
	 */
	bool trackNumbersShown() const;
	
	/**
	 * whether a track mark cannot yet be set due to a little delay between picture display and mouse position storing
	 * @param _wstm true => still wait; false => another track mark can be set
	 */
	void setWaitSettingTrackmark (bool _wstm)
		{
			wait_for_trackmark_set = _wstm;
		}

//        /**
//         * sets whether the wavelength index should be drawn
//         */
//        void setDrawWavelengthIndex (bool _drawWI)
//                {drawWavelengthIndex = _drawWI;}

	/**
	* @Deprecated
	* returns the current zoom factor (can have invalid values like 0 or -1..)
	*/
	int getCurrentZoom() const
	{return currentZoom;}

	/**
	 * @return current zoom factor (not in percent!)
	 */
	double getCurrentZoomAccurate() const {
		return currentZoomAccurate;
	}

	/**
	 * @return timepoint of currently displayed picture or -1 if no picture is displayed (white background)
	 */
	int getCurrentTimePoint() const {
		return currentTimepoint;
	}

	/**
	* whether to draw only selected tracks or all tracks. Affects all wavelengths
	* @param _drawOnlySelectedTracks true to draw only selected, false to draw all tracks
	*/
	static void setDrawOnlySelectedTracks(bool _drawOnlySelectedTracks) 
	{
		drawOnlySelectedTracks = _drawOnlySelectedTracks;
	}

	/**
	* get whether drawing of selected tracks only is enabled
	*/
	static bool setDrawOnlySelectedTracks() 
	{
		return drawOnlySelectedTracks;
	}

	/**
	* centers the view on the provided position (regarding the bounds, of course)
	* @param _position is provided in absolute coordinates, so transform them to picture coordinates
	*/
	void centerAbsolutePosition (QPointF _position);

	/**
	* returns the currently visible region (regarding zoom, shift, ...)
	*/
	QRectF getCurrrentlyVisibleRegion() const;

	/**
	* paint all additional elements on the view (orientation box, wavelength box, ...)
	*/
	void drawAdditions();

	/**
	* @return whether a picture was loaded already (once true, always true)
	*/
	bool pictureLoaded() const;

	/**
	* updates the view/... to reflect the provided style sheet
	*/
	void applyStyleSheet (const StyleSheet &_ss);


	/**
	* used as a callback function for clickable graphic items
	* NOTE: needs to be static due to a missing THIS-pointer in the (general) CallbackItem's procedures
	*/
	static void handleItemSelection (const QString &_parameter, void *_item, QMouseEvent *_ev);

	/**
	 * Insert a button for opening a neighbor position
	 */
	void addPositionButton(MovieNextPosItem* _posButton);

	/**
	* Show or hide position buttons
	* @param _show true if show
	*/
	void showPositionButtons(bool _show);

	/**
	 * Enable/Disable drawing of previous trackpoints (special effect)
	 * @param _on true if enable
	 * @param _numPrevTrackPointsToDraw about the maximum number of prev trackpoints to draw (see drawTracks() for details)
	 */
	void setShowPreviousTrackPoints(bool _on, int _numPrevTrackPointsToDraw, int _widthOfTails, bool _drawPrevTrackPointsTransparencyEffect, bool _drawPrevTrackPointsOnlyOfSelectedCellBranch);

	/**
	 * Enable/disable drawing of segmentation (special effect).
	 * @parameter _on true if enable
	 */
	void setDrawSegmentation(bool _on);

	/**
	 * Enable/disable highlighting with colored frame of this PictureView.
	 */
	void setHighlightingOn(bool on);

	// For debugging purposes only
	void debugTest();


signals:
	
	///emitted when the user grabs the picture with the left mouse and shifts the picture
	///necessary to repaint all other wavelengths
	///the parameters are the index of this instance and the shift offset in screen coordinates
	//void shifted (int, QPointF);

	///emitted during tracking mode when the next/previous (bool = false) picture should be displayed
	///the int parameter specifies the step width:
	///0: one picture
	///-1: first/last, according to the bool parameter
	///other positive values: the step width in percent of all loaded pictures
	void PictureProceedDemand (bool, int, bool afterTrackpointSet = false);

	///emitted when the user changes the black/white barrier
	void blackWhitePointChosen (int _black, int _white);

	///emitted when the user changes the gamma correction function
	void gammaChosen ();

	///emitted when the user clicked on a cell circle
	///the first bool is whether the click was with right mouse button -> start tracking!
	///second bool: whether a normal track was selected (true) or an external track (false)
	// /note: no distinction is made if the cells are currently visible - this has to be processed in the receiver
	void trackSelected (Track *, bool, bool);

	///emitted when the user shifts/sets the multiple wavelength orientation box
	///the parameters are the index of this instance and the new position in screen coordinates
	//void orientationBoxSet (int, QPointF);

	///emitted when the user presses escape during tracking process
	void trackingInterrupted (Track *);

	///emitted when the user clicks into a frame in order to set the current wavelength
	///triggered by mouseReleaseEvent()
	void displaySelected (int _index);

	///emitted when the mouse is moved over the widget
	///caught somewhere in order to display the absolute mouse coordinates
	void mouseMovedInPicture (QPointF _pos);

	///emitted when the user pressed key 0 or 2 (picture forward/backward) while
	/// tracking
	///(is handed up to TTTTracking to receive the currently set cell properties
	/// and add them to the just set TrackPoint)
	void TrackPointSet (Track *, int);

	///emitted when the user turns the mouse wheel in circle mouse mode
	void cellSizeChanged (int);

	///emitted when the user clicked with the right mouse button to select a radius
	///only emitted if RadiusSelectionMode == true
	void RadiusSelected (int);

	/**
	* emitted when the user clicked with the left mouse button during the symbol position selection mode
	* @param the position in experiment coordinates
	*/
	void symbolPositionSelected (QPointF);

	///emitted when the user released a key in Tracking mode (where the keys are exclusive to this widget), which is accepted, but not processed
	///@param int,bool,bool: key code, shift, control
	void unhandledKeyReleased (int, bool, bool);

	///emitted on user-click, indicates the symbol with (array) index _index in _waveLength should be deleted
	void symbolDeleteDemand (int _waveLength, int _index);

	/**
	* emitted when the user right-clicks a currently set colony starting cell - a delete demand is sent
	* @param _index the index of this cell in its containing array (for details see MultiplePictureViewer::handlePictureMouseRelease())
	*/
	void colonyStartDeleteDemand (int _index);

	///emitted when a new (fluorescence) picture was displayed
	///used for notifying the user about the change
	void pictureUpdated();

	///emitted when the user releases any mouse button
	///the provided position (second argument) is already in experiment global coordinates
	void mouseReleased (QMouseEvent *, QPointF);

	/**
	* emitted after a left mouse click in single background selection mode
	* @param _ the location of the click in global coordinates
	*/
	void singleBackgroundSet (QPointF);

	///**
	//* triggers a slot which centers the current track (if there is one)
	//*/
	//void centerCurrentTrackDemand();

	/**
	* emitted when the user scrolls the picture, either with the mouse or the scrollbars
	*/
	void scrolled (int _index, int _dx, int _dy);

	/**
	* emitted when the user left-clicks on this viewer
	*/
	void pictureViewChosen (int _index);

public slots:
	
	// /sets the current cell radius
	//void setCellRadius (int _radius);

	///sets whether cell or background is displayed
	void setShowBackground (bool _showBackground)
	{showBackground = _showBackground;}

	// /sets whether the cells are of default or of the stored size
	//void setUseCellSize (bool _useCellSize);

	///sets whether the cell or its background is tracked
	void setBackgroundTracking (bool _on)
	{setBackground = _on;}

	// /does nothing but call keyReleaseEvent() with the parameter
	// /necessary since keyReleaseEvent() is no slot per definition
	//	void callKeyReleaseEvent (int);

	//	///called by TTTManager when a timepoint is set
	//	void setToTimepoint (int _newTimepoint, int _oldTimepoint);

	/**
	* signals that the delay between picture display and trackmark setting is over
	*/
	void activateTrackMarkSetting()
	{setWaitSettingTrackmark (false);}

	
//private slots:

private:

	// KeyReleaseEvent parameters
	struct KeyReleaseEventParams {
		int key;
		QPointF mouseCoordinates; 
		QPointF pictureMouseCoordinates; 
		float diameter; 
		int mouseButtons; 
		Qt::KeyboardModifiers modifiers; 

		// Constructor
		KeyReleaseEventParams(int _key = -1, const QPointF& _mouseCoordinates = QPointF(), const QPointF& _pictureMouseCoordinates = QPointF(), float _diameter = 0.0f, int _mouseButtons = 0, Qt::KeyboardModifiers _modifiers = 0)
			: key(_key),
			mouseCoordinates(_mouseCoordinates), 
			pictureMouseCoordinates(_pictureMouseCoordinates), 
			diameter(_diameter), 
			mouseButtons(_mouseButtons), 
			modifiers(_modifiers)
		{}
	};

	// Draw tails of cells
	void drawTails(const QPointF& trackPictureCoords, float diameter, ITrack* track, const ITrackPoint* trackpoint, const QColor& circleColor);

	// Draw segmentation belonging to track
	void drawSegmentationToTrack(const unsigned char* segData, int segDataSizeX, int segDataSizeY, ITrack* track, const ITrackPoint* trackpoint, const QColor& color);

	// Handle KeyReleaseEvent
	bool handleKeyReleaseEvent(const KeyReleaseEventParams& params);

	///the position manager that is responsible for this picture view
	///is set in the constructor via TTTMovie
	TTTPositionManager *positionManager;


	// /a label for the wavelength index
	//QLabel *lblWaveLength;

	///the index of this instance (de facto the wavelength)
	int wlIndex;

	///whether this instance is visible (wavelength selected to be shown)
	bool isShown;

	// /the central QImage instance which is used as a painting device instead of THIS - in paintEvent() only the current state is copied to the screen
	// /note: this image always needs to have the correct size, exactly that of the display
	//QImage *draw_img;

	///whether the user is currently shifting the picture with the mouse
	bool shifting;

	///whether the user wants to shift the orientation box, if visible
	//bool OrientationBoxShifting;

	///holds the mouse position before the move event
	///only updated if one of the graphic additions is switched on!
	QPointF oldMousePos;

	///holds the mouse position before the move event
	///updated every time the move event is called
	//QPointF utdOldMousePos;

	///whether the mouse keys are pressed (held down)
	bool leftMouseDown;
	bool rightMouseDown;

	//	///true, when the user can change the graphic settings
	//        bool graphicEditable;

	///whether the track circles should be displayed
	bool showTracks;

	///whether the track numbers should be displayed (only available with ShowTracks == true)
	bool showTrackNumbers;

	///whether all cells/tracks have the same color or they are distinguished between active/inactive
	bool allCellsUniform;

	///whether X/Y or X/YBackground should be set with a mouse click
	bool setBackground;

	///whether X/Y or X/YBackground should be displayed
	bool showBackground;

	///whether the instance is in radius selection mode
	bool radiusSelectionMode;

	///the positions for the line indicating the radius
	///note: they contain real mouse coordinates; 
	/// 	 the transformation is before the signal is emitted
	QPointF radiusLineStartPos;
	QPointF radiusLineEndPos;

	///the timepoint of the last correctly displayed picture
	///necessary for updating "silent" pictures to avoid graphical accumulation
	int lastDisplayedTimePoint;

	///whether the instance is in symbol position selection mode
	bool symbolPositionMode;

	//	//true, if the draw method has not been executed yet for the symbol in symbol selection mode
	//        bool firstSymbolDrawCall;

	//	///contains the current symbols (for the current timepoint) for displaying
	//        QVector<Symbol> *symbols;

	//	///necessary for repainting in paintEvent()
	//	/// (direct calls are clipped, switching off clipping does not work)
	//	QTimer tmr;

	///contains the current PictureView instance which has the mouse over it 
	PictureView *mouseWidget;

	///contains the currently displayed radius for the colocation circle
	///if >0,  the circle must be deleted before drawing a new
	///if ==0, the new circle can immediately be drawn
	///NOTE: must be set to 0 whenever the picture is repainted!
	int currentColocationRadius;

	///contains the position of the current track
	///should only be used if CurrentTrack != 0
	///set in drawTracks()
	QPointF currentTrackPosition;

	///whether this picture frame is used as an overlay frame where more pictures are combined into one
	bool overlayFrame;

	//	///stores all drawn circles as MyCircle objects to be redrawn in paintEvent()
	//        QVector<MyCircle> internalCircles;

	// /whether the wavelength index should be drawn
	//	bool drawWavelengthIndex;

	///whether the widget is in single background track selection mode
	bool singleBackgroundSelectionMode;

	///whether the pressing of keys 0/2 is not yet accepted, as there is a little delay between picture display and actual track mark setting
	///this is necessary for exact, fast and easy tracking without having to adjust the keyboard repeat rate
	///the delay itself is managed in TTTMovie, and if it is run out, this variable is set to false
	bool wait_for_trackmark_set;

	// ------------------Oliver----------------
	///whether to display only tracks that have been selected in frmTracking ("cell editor")
	///since this can only be enabled or disabled for all wavelengths, a static variable is sufficient
	static bool drawOnlySelectedTracks;


	//the owning multiple picture viewer
	MultiplePictureViewer *multiPicViewer;

	//the geometry of the background picture
	QRect picGeometry;

	///the currrent zoom factor
	int currentZoom;

	///current zoom factor (accurate)
	double currentZoomAccurate;

	///the shift backup QCursor, necessary for restoring after dragging
	QCursor shiftBackupCursor;

	///another backup cursor, if multiple are necessary
	QCursor otherBackupCursor;

	///the background picture item 
	QGraphicsPixmapItem* picture;

	// The original data of the image currently displayed (note: modification causes deep copy)
	cv::Mat pictureData;

	///the placeholder rectangle, if no picture is available yet
	///necessary to enable concurrent scrolling already without a picture
	QGraphicsRectItem *placeHolderRect;
	///indicates whether the placeholder rectangle was already set
	bool placeHolderSet;

	///the orientation box
	QGraphicsRectItem *orientationBox;

	///the wavelength index label
	QGraphicsSimpleTextItem *wavelengthBox;

	///the out-of-sync symbol
	QGraphicsRectItem *outOfSyncBox;

	/////whether the control key is pressed
	//bool controlKeyPressed;

	///scaling factors. Same as in PictureContainer::getScalingX/Y().
	double scaleX;
	double scaleY;

	///Position buttons
	QVector<MovieNextPosItem*> posButtons;

	///currently displayed timepoint
	int currentTimepoint;

	// Draw previous trackpoints
	bool drawPrevTrackPoitns;

	// If transparency effect should be used when drawing prev trackpoints
	bool drawPrevTrackPointsTransparencyEffect;

	// If only cells belonging to branch of currently selected cell should be drawn
	bool drawPrevTrackPointsOnlyOfSelectedCellBranch;

	// Draw segmentation
	bool drawSegmentation;

	// Number of previous trackpoints to draw (at most) if drawPrevTrackPoints is true and width of tails
	int numPrevTrackPointsToDraw;
	int widthOfTails;

	// Parameters of the last KeyReleaseEvent and the time when it was received
	KeyReleaseEventParams lastKeyReleaseEventParameters;
	/*
	QDateTime lastKeyReleaseEventTime;
	
	// If this PictureView is waiting for a paint event (i.e. the shown image/addons/whatever was changed, but not painted yet)
	bool waitingForPaintEvent;

	// If the last KeyReleaseEvent must be handled after the next PaintEvent
	bool handleKeyReleaseAfterPaint;
	*/
	
	/**to do: 
	 * - test this carefully
		-> on the server: does delayed key handler work?
	   - remove qDebug() messages
	   - put beta on server and send mail to group
	    -> what happened: 16 bit support -> image display slower -> problem in ttt occurred: when keeping tracking key pressed, ttt could not display images fast enough anymore, so image display could "freeze" (time point changed and track marks set, but image not updated before key released) -> to fix this behavior, major changes were required 
		-> code for tracking had to be changed for the first time in years
		-> i expect everything to work fine, but be careful if you notice anything unusual and if so, let me know
	 */


	/**
	 * Re-use GRAPHICSDATA_USAGE_TRACKCIRCLE items, because deleting them takes much time..
	 */
	//ClickableQGraphicsEllipseItem
	QList<ClickableQGraphicsEllipseItem*> m_freeEllipseItemsStack;
	QList<ClickableQGraphicsEllipseItem*> m_usedEllipseItemsStack;
	ClickableQGraphicsEllipseItem* getClickableEllipseItem()
	{
		if(m_freeEllipseItemsStack.size()) {
			ClickableQGraphicsEllipseItem* recycled = m_freeEllipseItemsStack.takeLast();
			m_usedEllipseItemsStack.push_back(recycled);
			return recycled;
		}
		else {
			ClickableQGraphicsEllipseItem* newItem = new ClickableQGraphicsEllipseItem();
			m_usedEllipseItemsStack.push_back(newItem);
			newItem->setData (GRAPHICSDATA_USAGE, GRAPHICSDATA_USAGE_TRACKCIRCLE);	
			return newItem;
		}
	}
	//QGraphicsLineItem
	QList<QGraphicsLineItem*> m_freeLineItems;
	QList<QGraphicsLineItem*> m_usedLineItems;
	QGraphicsLineItem* getQGraphicsLineItem(const QLineF& line)
	{
		if(m_freeLineItems.size()) {
			QGraphicsLineItem* recycled = m_freeLineItems.takeLast();
			m_usedLineItems.push_back(recycled);
			recycled->setLine(line);
			return recycled;
		}
		else {
			QGraphicsLineItem* newItem = new QGraphicsLineItem(line);
			newItem->setData (GRAPHICSDATA_USAGE, GRAPHICSDATA_USAGE_TRACKCIRCLE);	
			m_usedLineItems.push_back(newItem);
			return newItem;
		}
	}
	//QGraphicsTextItem
	QList<QGraphicsSimpleTextItem*> m_freeTextItems;
	QList<QGraphicsSimpleTextItem*> m_usedTextItems;
	QGraphicsSimpleTextItem* getQGraphicsTextItem(const QString& text)
	{
		if(m_freeTextItems.size()) {
			QGraphicsSimpleTextItem* recycled = m_freeTextItems.takeLast();
			m_usedTextItems.push_back(recycled);
			recycled->setText(text);
			return recycled;
		}
		else {
			QGraphicsSimpleTextItem* newItem = new QGraphicsSimpleTextItem(text);
			m_usedTextItems.push_back(newItem);
			newItem->setData (GRAPHICSDATA_USAGE, GRAPHICSDATA_USAGE_TRACKCIRCLE);	
			return newItem;
		}
	}
	//QGraphicsEllipseItem
	QList<QGraphicsEllipseItem*> m_freeQGraphicsEllipseItems;
	QList<QGraphicsEllipseItem*> m_usedQGraphicsEllipseItems;
	QGraphicsEllipseItem* getQGraphicsEllipseItem()
	{
		if(m_freeQGraphicsEllipseItems.size()) {
			QGraphicsEllipseItem* recycled = m_freeQGraphicsEllipseItems.takeLast();
			m_usedQGraphicsEllipseItems.push_back(recycled);
			return recycled;
		}
		else {
			QGraphicsEllipseItem* newItem = new QGraphicsEllipseItem();
			m_usedQGraphicsEllipseItems.push_back(newItem);
			newItem->setData (GRAPHICSDATA_USAGE, GRAPHICSDATA_USAGE_TRACKCIRCLE);	
			return newItem;
		}
	}
	// AutoTrackingCellCircle
	QList<AutoTrackingCellCircle*> m_usedAutoTrackingCellCircles;

	///methods

	///draws a line from RadiusLineStartPos to RadiusLineEndPos
	void drawRadiusLine (QPointF _pos);

	///draws the provided symbol at the specified position with Copy pen
	void drawSymbol (Symbol, QPointF);

	///draws the provided symbol at the specified position with XOr pen
	///only used in symbol position mode
	void drawDragSymbol (Symbol, QPointF);

	///draws a circle in xor style at _pos with _radius in _color
	void drawCircle (QPointF _pos, float _radius, QColor _color, QPainter::CompositionMode _rop = QPainter::RasterOp_SourceXorDestination);

	///creates the basic graphic elements as orientation box, wavelength label, ...
	void createBasicElements();
};


inline bool PictureView::tracksShown() const
{
        return showTracks;
}

inline bool PictureView::trackNumbersShown() const
{
        return showTrackNumbers;
}

#endif
