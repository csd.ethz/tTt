/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef tttautotracking_h__
#define tttautotracking_h__

#include "ui_frmAutoTracking.h"

// Qt
#include <QSharedPointer>
#include <QAbstractTableModel>
#include <QPointF>
#include <QColor>

// Project
#include "tttbackend/treefragmenttrackpoint.h"
#include "tttbackend/treefragment.h"
#include "autotrackingcellcircle.h"

// Forward declarations
class TTTAutoTrackingTreeWindow;

class AutoTrackingCellCircle;


class TTTAutoTracking : public QWidget {

	Q_OBJECT

public:

	// Color for cell circles for used tree fragments
	static const QColor USED_TREES_COLOR;

	// Constructor
	TTTAutoTracking(QWidget *_parent = 0);

	// Destructor
	~TTTAutoTracking();

	/**
	 * Get list of all trackpoints from all loaded tree fragments for a certain timepoint. Function runs in constant time.
	 * @param _tp the timepoint
	 * @return list of shared pointers to the trackpoints
	 */
	QList<QSharedPointer<TreeFragmentTrackPoint> > getTrackPointsByTimePoint(int _tp) const {
		// Extract trackpoints from hashmap
		return trackpointsHashmap.value(_tp, QList<QSharedPointer<TreeFragmentTrackPoint> >());
	}

	/**
	 * Get list of all TreeFragments which start in the timepoint range [_tp1; _tp2] and whose dist to _point is not bigger than _maxDist
	 * @param _tp1 start timepoint
	 * @param _tp2 end timepoint
	 * @param _point point in global coordinates
	 * @param _maxDistPerTP maximum distance in micrometers per timepoint
	 * @param _distances optional pointer to array where distances are saved in same order as in returned array.
	 * @return list of shared pointers to TreeFragments
	 */
	QList<QSharedPointer<TreeFragment> > getTreeFragmentsStartingInRange(int _tp1, int _tp2, const QPointF& _point, float _maxDistPerTP, QList<float>* _distances = nullptr);

	/**
	 * @return if trackpoints from tree fragments should be displayed in movie windows
	 */
	bool displayTrackpoints() const;

	/**
	 * Get number of tree fragments starting in position with index _index within the first MIN_POSITION_DISPLAY_TP timepoints
	 * @param _index index of position
	 * @return number of fragments loaded for position
	 */
	int getTreeFragmentCount(const QString& _index);

	/**
	 * @return pointer to TTTAutoTrackingTreeWindow instance, can be 0!
	 */
	TTTAutoTrackingTreeWindow* getTreeWindow() const {
		return frmTreeWindow;
	}

	/**
	 * Find currently loaded TreeFragments that are associated with the provided tree. Works by simply comparing filenames
	 * which is maybe not the optimal solution
	 * @param _treeName filename of the tree, without path
	 * @return list of found TreeFragments
	 */
	QList<QSharedPointer<TreeFragment> > findAssociatedTreeFragments(QString _treeName);

	/**
	 * Get all cell circles that should be displayed in the movie window at the specified timepoint
	 * @param _timePoint the timepoint
	 * @param _tttpm used to transform global to local coordinates
	 * @param _wlIndex index of wavelength
	 * @return list of AutoTrackingCellCircle instances that should be displayed (caller must make sure to delete them properly!)
	 */
	QList<AutoTrackingCellCircle*> getCellCirclesForDisplay(int _timePoint, TTTPositionManager* _tttpm, int _wlIndex, bool _noText) const;

	/**
	 * @param _trackPoint the trackpoint
	 * @param _tttpm used to transform global to local coordinates
	 * @param _wlIndex index of wavelength
	 * @param _action associated action (see AutoTrackingCellCircle)
	 * @param _tooltipText tooltip text to display
	 * @param _color pointer to QColor instance, will be used instead of confidence color if not 0
	 * @return pointer to newly created AutoTrackingCellCircle instance that can be selected by the user
	 */
	AutoTrackingCellCircle* createCellCircleFromTreeFragmentTrackPoint(const TreeFragmentTrackPoint& _trackPoint, TTTPositionManager* _tttpm, int _wlIndex, AutoTrackingCellCircle::Action _action, QString _tooltipText, QColor* _color = 0, bool _noText = false) const;

	/**
	 * Get all tree fragments.
	 * @return hashmap with tree fragments by position.
	 */
	QHash<int, QList<QSharedPointer<TreeFragment> > >& getTreeFragments() {
		return trees;
	}

	/**
	 * Get all tree fragments (constant).
	 * @return hashmap with tree fragments by position.
	 */
	const QHash<int, QList<QSharedPointer<TreeFragment> > >& getTreeFragments() const {
		return trees;
	}

public slots:

	/**
	 * Create regular tTt tree from provided tree fragment and show it in TTTAutotrackingTreeWindow
	 * @param _treeFragment the fragment to use
	 * @return pointer to created tTt tree or 0 in case of an error.
	 */
	Tree* createTreeFromFragment(QSharedPointer<TreeFragment> _fragment);

	/**
	 * User clicked on a cell circle with the mouse
	 */
	void cellCircleClicked(AutoTrackingCellCircle* _circle, Qt::MouseButton _button);

	/**
	 * Update position layout.
	 */
	void updateDisplay();

	/**
	 * Remove loaded fragments from memory
	 */
	void reset();


signals:

	/**
	 *	Emited when the loaded tree fragments have changed (e.g. after load or unload).
	 */
	void loadedTreeFragmentsChanged();

	/**
	 * Emited when a tree fragment has been used for a tree.
	 */
	void treeFragmentUsedForTree(int fragmentNumber);

protected:

	// Show event: loads window layout
	void showEvent(QShowEvent *_ev);

	// Close event: saves window layout and clears data
	void closeEvent(QCloseEvent *_ev);

private slots:

	/**
	 * Load tree fragments. Called by pbtLoad, when pressing enter in lieInputFolder or after selecting a folder with pbtSelectFolder
	 */
	void loadTreeFragments();

	/**
	 * Select input folder with file selection dialog
	 */
	void pbtSelectFolderClicked();

	/**
	 * User clicked in position in position layout
	 */
	void positionLeftClicked (const QString &_index);

	/**
	 * User double clicked on position in position layout.
	 */
	void positionDoubleClicked(const QString& index);

	/**
	 * Let user select an existing ttt file
	 */
	void openTTTFile();
	
	/**
	 * Automatically select connected positions toggled. Only stores choice in settings.
	 */
	void selectOverlappingToggled(bool on);

	// Opens tree window
	void showTreeWindow();

	/**
	 * Import auto tracking results directly from external tools
	 */
	void importAutoTrackingResults();

	// Selected method changed -> adjust default value for temporal offset
	void selectedMethodChanged(int newIndex);

private:

	// Read tree fragments, called by loadTreeFragments()
	void loadTreeFragmentsInternal();

	/**
	 * Helper function. Lists all .trf files in specified directory and
	 * appends them to _fileList. Optionally prepends a prefix to all found files.
	 */
	void listTrfFiles(const QString& _pathName, QStringList& _fileList, const QString& _prefix = "");

	// Import auto tracking results for specific software
	void importAutoTrackingResultsTrackMate();
	void importAutoTrackingResultsCellProfiler();

	// Hashmap of tree fragments for each position
	QHash<int, QList<QSharedPointer<TreeFragment>>> trees;

	// Hashmap with list of trackpoints for each timepoint
	QHash<int, QList<QSharedPointer<TreeFragmentTrackPoint> > > trackpointsHashmap;

	// Tree window
	TTTAutoTrackingTreeWindow* frmTreeWindow;

	// GUI
	Ui::frmAutoTracking ui;

	// Window layout already loaded
	bool windowLayoutLoaded;

	friend TTTAutoTrackingTreeWindow;
};


#endif // tttautotracking_h__
