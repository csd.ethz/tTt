/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef tttcelldivisionoverviewexport_h__
#define tttcelldivisionoverviewexport_h__


#include "ui_frmCellDivisionOverviewExport.h"
#include "tttio/celldivisionoverviewexporter.h"

#include <QVector>


// Forward declarations
class CellDivisionOverviewExporter;
class TTTException;
class Track;
class TTTMovie;


/**
 * @author Oliver Hilsenbeck
 *
 * Form to configure and run a cell division export
 */
class TTTCellDivisionOverviewExport : public QDialog, public Ui::frmCellDivisionOverviewExport {
	
	Q_OBJECT

public:
	TTTCellDivisionOverviewExport(TTTMovie *_parent/*, int _minTimePoint, int _maxTimePoint*/);

	
private slots:
	// Start export
	void startExport();

	// Select export directory
	void selectDir();

private:
	// Movie window
	TTTMovie * movieWindow;

	// Input
	int interval, tpsBefore, tpsAfter, 
		picSizeX, picSizeY;
	QString directory;
	CellDivisionOverviewExporter::FORMAT format;
	Track* motherTrack;
	QVector<int> wavelengths;
	bool hideTrackpoints;

	// Check input
	void getInput() throw(TTTException);

	// Exporter object
	CellDivisionOverviewExporter* exporter;
};

#endif // tttcelldivisionoverviewexport_h__