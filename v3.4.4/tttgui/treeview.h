/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef treeview_h__
#define treeview_h__

// Qt
#include <QGraphicsView>
#include <QList>

// Forward declarations
class TreeViewSettings;
class TreeViewTreeItem;
class TreeViewTimeLine;
class TreeViewTrackItem;
class ITree;
class ITrack;

/**
 * @author Oliver Hilsenbeck
 *
 * Widget to display an arbitrary number of trees
 * Note: This class assumes that ITree objects are not copied and can therefore be compared simply by their addresses
 * 
 * Principle: TreeViewTreeItem ...
 */

class TreeView : public QGraphicsView {

	Q_OBJECT
		
public:

	/**
	 * Constructor
	 * @param _parent parent window
	 * @param _treeViewHeightFactor the number of pixels per time point.
	 */
	TreeView(QWidget *_parent = 0, int _treeViewHeightFactor = 2);

	/**
	 * Add tree to display. Important: If a track is removed from a tree that is being displayed, you must call redrawTree() immediately
	 * or there will be a heap corruption when the view is drawn next time.
	 * @param _tree pointer to the tree to be added
	 * @param _displayCloseIcon if a close icon should be displayed
	 * @param _displaySaveIcon if a save icon should be displayed if tree has been modified
	 * @return true if successful
	 */
	void addTree(ITree* _tree, bool _displayCloseIcon = false, bool _displaySaveIcon = false);

	/**
	 * Toggle exclusive cell selection mode, i.e. not more than one cell can be selected at a time
	 * Deselect all currently selected cells
	 * _exclusive true to enable
	 */
	void setCellSelectionMode(bool _exclusive);

	/**
	 * Clear cell selection
	 */
	void deselectAllCells();

	/**
	 * Select cell. Will deselect currently selected cell if selection mode is exclusive and _selected is true, in that
	 * case cellDeSelected() signal(s) are emitted
	 * @param _track which cell to select
	 * @param _selected true to select, false to deselect
	 * @return true if successful, i.e. cell has been found
	 */
	bool selectCell(ITree* _tree, ITrack* _track, bool _selected);

	/**
	 * Deselect all cells belonging to the provided tree. Does not emit any signals
	 * @param _tree pointer to the provided tree
	 */
	void deselectCellsByTree(ITree* _tree);

	/**
	 * Remove tree from display
	 * @param _tree pointer to tree to be removed
	 * @return true if tree was found and removed
	 */
	bool removeTree(ITree* _tree);

	/**
	 * Redraw tree (completely reconstructs display object). Must be called after removing a track to avoid crashes.
	 * @param _tree the tree to be redrawn
	 * @return true if successful, false if _tree could not be found
	 */
	bool redrawTree(ITree* _tree);

	/**
	 * Redraw all trees, compare redrawTree().
	 */
	bool redrawAllTrees();

	/**
	 * Update displayed name of tree
	 * @param _tree the tree
	 * @return true if successful, false if _tree could not be found
	 */
	bool updateDisplayName(ITree* _tree);

	/**
	 * Select tree (selected tree will be highlighted). Will deselect any other selected tree
	 * @param _tree the tree to select
	 * @return true if successful
	 */
	bool selectTree(ITree* _tree);

	/**
	 * Deselect tree (so that no tree is selected anymore)
	 */
	void deselectTree();

	/**
	 * Remove all trees from display
	 * @return true if successful
	 */
	bool clearDisplay();

	/**
	 * Draw time line bar. Updates horizontal bar at current time point.
	 * @param _tp the timepoint, determines y-position
	 */
	void setTimeLineTimePoint(int _tp);

	/**
	 * Redraw time scale, e.g. to update display of loaded images.
	 */
	void redrawTimeScale();

	/**
	 * Set position index of timeline. Updates display of loaded images.
	 * @param positionIndex index of position, can be -1 to display nothing.
	 */
	void updateTimeLinePosition(int positionIndex);

	/**
	 * Highlight part of a track of a tree. Currently only one region per track can be highlighted.
	 * @param _tree pointer to displayed tree.
	 * @param _trackNumber nr of track to highlight.
	 * @param _startTp start time point of highlighting.
	 * @param _stopTP stop time point.
	 */
	void highlightTrack(ITree* _tree, int _trackNumber, int _startTp, int _stopTp);

	/**
	 * Stop highlighting track.
	 * @param _tree pointer to displayed tree.
	 * @param _trackNumber nr of track to highlight.
	 */
	void stopHighlightingTrack(ITree* _tree, int _trackNumber);

	/**
	 * Highlight range of time points in time line.
	 * @param startTp begin of range, set to -1 to highlight nothing.
	 * @param stopTp stop of range, set to -1 to highlight nothing.
	 */
	void timeLineSetHighlighting(int startTp = -1, int stopTp = -1);

	/**
	 * Show real time in time scale or time points.
	 */
	void setShowRealTimeInTimeScale(bool setShowRealTime);

	/**
	 * Get a tree item representing the provided tree, e.g. to update specific tracks
	 * @param _tree pointer to the tree
	 * @return pointer to TreeViewTreeItem or NULL if not found
	 */
	TreeViewTreeItem* getTreeViewItemForTree(ITree* _tree);

	/**
	 * Get current zoom
	 * @return current scaling factor (e.g. 0.1f for 10% zoom)
	 */
	float getZoom() const {
		return scalingFactor;
	}

	/**
	 * Set maximum y-value to be displayed
	 * @param _maxY max y-value
	 */
	void setMaxY(int _maxY);

	/**
	 * Update scene rect so that it is not too big / small.
	 */
	void updateSceneRect();

	/**
	 * Get tree height factor, i.e. number of pixels per timepoint.
	 */
	int getTreeHeightFactor() const {
		return treeHeightFactor;
	}

	/**
	 * Export view. Hides vertical bar for current time point and the icons to save/close trees.
	 */
	bool exportView(const QString& fileName);

	/**
	 * Show/hide track numbers.
	 */
	void setTrackNumbersVisible(bool visible);

	/**
	 * Get time line.
	 */
	TreeViewTimeLine* getTimeLine() const
	{
		return timeLine;
	}

	/**
	 * Map quality value (1 = good, 0 = bad, < 0 = n/a) to QColor (green = good, red = bad, black = n/a)
	 * @param _confidence the confidence value
	 * @return QColor code
	 */
	static QColor mapConfidenceOnColor(float _confidence);

	// Minimum and maximum scaling values
	static const float MAX_SCALE;
	static const float MIN_SCALE;

	// Z-Values, sorted ascendingly
	static const float Z_TIME_VERTICAL_LINE;
	static const float Z_TIME_CURRENT_TP_LINE;
	static const float Z_DIVISION_LINE;
	static const float Z_TRACK_LINE;
	static const float Z_CELL_CIRCLE;
	static const float Z_CELL_NUMBER;
	static const float Z_TIME_SCALE;

signals:

	/**
	 * emitted when zoom has been changed
	 * @param _scale the new scaling factor
	 */
	void zoomChanged(float _scale);

	/**
	 * emitted when the user has selected a cell. TreeView will not change the selection, the receiver of this 
	 * signal has to do this by  calling selectCell()
	 * @param _tree pointer to tree the cell belongs to
	 * @param _track pointer to track that was clicked
	 */
	void cellSelected(ITree* _tree, ITrack* _track);

	/**
	 * emitted when the user has deselected a cell
	 * @param _tree pointer to tree the cell belongs to
	 * @param _track pointer to track that was clicked
	 */
	void cellDeSelected(ITree* _tree, ITrack* _track);

	/**
	 * emitted when the user has right clicked on a cell
	 * @param _tree pointer to tree the cell belongs to
	 * @param _track pointer to track that was clicked
	 */
	void cellRightClicked(ITree* _tree, ITrack* _track);

	/**
	 * emitted when save button of a tree was clicked
	 * @param _tree pointer to tree 
	 */
	void saveTreeClicked(ITree* _tree);

	/**
	 * emitted when close button of a tree was clicked
	 * @param _tree pointer to tree 
	 */
	void closeTreeClicked(ITree* _tree);

	/**
	 * emitted when user clicked on a tree with left mouse buttons
	 * @param _tree pointer to tree
	 */
	void treeLeftClicked(ITree* _tree);

	/**
	 * emitted when user clicked into timeline
	 */
	void timeLineClicked(int timepoint, Qt::MouseButton button);

public slots:

	/**
	 * Set tree height factor. Updates display of everything accordingly.
	 */
	void setTreeHeightFactor(int _treeHeightFactor);

protected:

	// Mouse wheel event, used for zoom
	void wheelEvent(QWheelEvent *_event);

	// Mouse button and move events, used for navigation with mouse
	void mousePressEvent( QMouseEvent *_event );
	void mouseReleaseEvent( QMouseEvent *_event );
	void mouseMoveEvent ( QMouseEvent *_event );

	//// Scroll event, moves timeline
	//void scrollContentsBy (int _dx, int _dy);

private:
	
	/**
	 * Zoom function
	 */
	void scaleView(qreal _scaleFactor);

	/**
	 * Get index of tree item for provided tree in trees list
	 * @param _tree the tree to look for
	 * @return a valid index in trees or -1
	 */
	int findTreeItem(ITree* _tree);

	/**
	 * Notifies TreeView that a cell has been selected/unselected, only called by TreeViewTrackItem
	 * @param _tree the tree
	 * @param _track the cell (track)
	 */
	void notifyCellSelection(TreeViewTreeItem* _tree, TreeViewTrackItem* _track);

	/**
	 * Notifies TreeView that tree's save button has been clicked
	 * @param _tree the tree
	 */
	void notifyTreeSaveButtonClicked(ITree* _tree);

	/**
	 * Notifies TreeView that tree's close button has been clicked
	 * @param _tree the tree
	 */
	void notifyTreeCloseButtonClicked(ITree* _tree);

	/**
	 * Notifies TreeView that tree has been clicked
	 * @param _tree the tree
	 */
	void notifyTreeLeftClicked(ITree* _tree);

	/**
	 * Notified TreeView that timeline has been clicked
	 */
	void notifyTimeLineClicked(int timepoint, Qt::MouseButton button) {
		emit timeLineClicked(timepoint, button);
	}
	
	/**
	 * Ignore next mouse press event. Called by scene items when they have already handled mouse press events
	 */
	void setIgnoreNextMousePressEvent() {
		ignoreNextMousePressEvent = true;
	}
	
	/**
	 * Update cell selection states (i.e. call TreeViewTrackItem::setSelectionState(true) for all selected cells of provided tree)
	 * @param _treeItem the tree item
	 */
	void updateCellSelectionStatesForTreeItem(TreeViewTreeItem* _treeItem) const;

	// Currently displayed trees (it is assumed that this will only include a few trees, 
	QList<TreeViewTreeItem*> trees;
	typedef QList<TreeViewTreeItem*>::const_iterator const_trees_iterator;
	typedef QList<TreeViewTreeItem*>::iterator trees_iterator;

	// Currently selected tree
	TreeViewTreeItem* currentlySelectedTree;

	// Currently selected cells (pairs of TreeItems and track numbers)
	QList<QPair<TreeViewTreeItem*, int> > currentlySelectedCells;

	// If cell selection is exclusive (only one cell can be selected at most)
	bool cellSelectionExclusive;

	// Timeline
	TreeViewTimeLine* timeLine;

	// Current scaling factor
	float scalingFactor;

	// Mouse navigation
	bool ignoreNextMousePressEvent;
	int mouseMoveStartX, mouseMoveStartY;
	bool scrolling;

	// Max y-value
	int maxY;

	// Tree view height factor, this many pixels are used per timepoint
	int treeHeightFactor;

	// Margin of the scroll area to bottom and right border
	static const int MARGIN_RIGHT_BOTTOM = 100;

	// Top left margin
	static const int MARGIN_TOP_LEFT = 20;

public:

	// Default zoom
	static const float DEFAULT_SCALING;

	// Items are friends so e.g. notify methods don't need to be public
	friend class TreeViewTreeItem;
	friend class TreeViewTreeNameItem;
	friend class TreeViewCellCircle;
	friend class TreeViewTimeLine;
	friend class TreeViewTrackItem;
};


#endif // treeview_h__
