/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef tttspecialeffects_h__
#define tttspecialeffects_h__

#include "ui_frmSpecialEffects.h"

// Forward declarations
class TTTMovie;


class TTTSpecialEffects : public QDialog {

	Q_OBJECT

public:

	/**
	 * Constructor
	 * @param _parent associated movie window
	 */
	TTTSpecialEffects(TTTMovie* _parent);

private slots:

	/**
	 * Apply settings
	 */
	void apply();

private:

	// Associated TTTMovie instance
	TTTMovie *movieWindow;

	// GUI
	Ui::frmSpecialEffects ui;

};


#endif // tttspecialeffects_h__
