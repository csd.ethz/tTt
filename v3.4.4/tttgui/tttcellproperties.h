/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TTTCELLPROPERTIES_H
#define TTTCELLPROPERTIES_H

#include "ui_frmCellProperties.h"

#include "tttdata/cellproperties.h"

#include <qapplication.h>
#include <q3dict.h>
#include <qwidget.h>

#include <q3buttongroup.h>
#include <qpushbutton.h>
#include <qradiobutton.h>
#include <qcheckbox.h>
//Added by qt3to4:
#include <QCloseEvent>
#include <QDialog>

/**
	@author Bernhard
	
	The class for editing the cell properties in a dialog
	Its "return" value is a CellProperties object containing all user settings
*/

class TTTCellProperties: public QDialog, public Ui::frmCell_Properties {

Q_OBJECT

public:
        TTTCellProperties(QWidget *parent = 0);
	
	void closeEvent (QCloseEvent *);

	void showEvent (QShowEvent *_ev);
	
	///returns the cell object with the current configuration
	///does not close the window or anything else!
	CellProperties getProperties();
	
	///displays the properties of the provided trackpoint 
	///(if there are none yet set, "nothing" is set)
	void setProperties (CellProperties _props);
	
	///show/hide further option buttons
	///@param _showAdherence whether the cell state buttons should also be visible
	void showFurtherOptions (bool _showAdherence);
	
public slots:
    
	///sets the user selection back to nothing (default value)
	void selectNothing();
	
private slots:

	///called after a click on one of the tissue types, displays the corresponding frame
	void showTissueFrame (int);
	
	///called after a click on one of the blood cell types, displays the corresponding frame
	void showBloodFrame (int);
	
	///called after a click on one of the stroma cell types, displays the corresponding frame
	void showStromaFrame (int);
	
	///called after a click on one of the heart cell types, displays the corresponding frame
	void showNerveFrame (int);
	
	///called when an option button was clicked
	///sets the CellLineage property
	void setCellLineage (int);
	
	
private:

	///the attributes are set every time the query getProperties() is run
	CellProperties cellProperties;
	
	///the index that is internally used for defining a tisse type
	int tissueTypeIndex;
	
	///the index that is internally used for defining a cell general type
	int cellGeneralTypeIndex;
	
	///the array of all option buttons, index == "(tissue type)_(cell general type)_(cell lineage)"
	Q3Dict<QRadioButton> options;
	
	
	///checks the radio button belonging to the currently selected lineage
	void displayCellLineage (int);

///data

	///window layout already loaded
	bool windowLayoutLoaded;
};

#endif
