/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Module for testing functionality within tTt that is not accessible for regular users.
 */
#ifndef ttttest_h__
#define ttttest_h__

#include "ui_frmTest.h"

// Qt
#include <QList>
#include <QSharedPointer>

// Project
#include "tttbackend/treefragment.h"
#include "tttdata/tree.h"

// Forward declarations
class CvKalman;
class CvMat;


/**
 * Predictor base class
 */
class PredictorBase {
public:

	// Set first point
	virtual void initialize(const QPointF& _startPos) = 0;

	// Predict next point
	virtual QPointF predictNextPos() = 0;

	// Set next point
	virtual void setCurrentPos(const QPointF& _point) = 0;

	virtual ~PredictorBase() {}
};

/**
 * Predictor using speed based on previous measurement
 */
class SimplePredictor : public PredictorBase {

	float factor;
	QPointF lastPos;
	QPointF beforeLastPos;

public:

	/**
	 * Constructor
	 * @param _factor value between 0 and 1, difference vector to last measurement will
	 * be multiplied by this value
	 */
	SimplePredictor(float _factor = 1.0f) : factor(_factor) {}

	// Set first point
	void initialize(const QPointF& _startPos);

	// Predict next point
	QPointF predictNextPos();

	// Set next point
	void setCurrentPos(const QPointF& _point);

	// Get speed if beforeLastPos is available
	QPointF getSpeed();

	~SimplePredictor() {}
};

/**
 * Predictor using kalman filter
 */
class KalmanPredictor : public PredictorBase {

	// Filter
	CvKalman* kalman;

	// Measurement matrix
	CvMat* measurement;

public:

	KalmanPredictor();

	// Set first point
	void initialize(const QPointF& _startPos);

	// Predict next point
	QPointF predictNextPos();

	// Set next point
	void setCurrentPos(const QPointF& _point);

	~KalmanPredictor();

};

/**
 * @author Oliver Hilsenbeck
 *
 * Form for trying out new functionality. Should not be accessible for users in release builds.
 * Can be access from TTTTracking help menu
 */
class TTTTest : public QWidget {

	Q_OBJECT

public:

	// Constructor
	TTTTest(QWidget* _parent = 0);

private slots:

	void thresholdsTest();

	void thresholdsTest3D();

	void errorTestSingle();

	void tttHistogram();

	void trfHistogram();

	void speedHist();

	void displacement();

	void expCoords();

	void absDispl();

	void trfSpeedPlot();

	// Test prediction (Kalman etc)
	void testPrediction();

	// Generate statistics needed for kalman filter (transition noise)
	void generateKalmanStats();

	// Generate stats to estimate initial kalman error estimate
	void generateKalmanInitErrStats();

	// Export speed values of trf files
	void exportSpeedOfTrfs();

private:

	// Output
	void say(QString _what);

	// General helper funtions
	QList<QSharedPointer<TreeFragment> > readTrfFiles(QString _dir);
	QList<QSharedPointer<Tree> > readTTTFiles(QString _dir);
	QStringList listDirectories(QString _dir);
	bool getFolders();
	QSharedPointer<TreeFragmentTrack> getClosestTrack(QPointF _pos, int _tp, const QList<QSharedPointer<TreeFragment> >& _fragments, float& _outDistance);
	

	// Thresholds analysis functions
	QString thrObservation(const QList<QSharedPointer<TreeFragment> >& _fragments, const QList<QSharedPointer<Tree> >& _trees);

	// Count 'serious errors' by comparing fragments and trees
	int countSeriousErrors(const QList<QSharedPointer<TreeFragment> >& _fragments, const QList<QSharedPointer<Tree> >& _trees);

	// Calc avg speed of tree fragment cell
	float avgSpeed(TreeFragmentTrack* _track);

	// Prediction test function for a tree
	void testPredictorsOnTrack(Track* _track, QList<QSharedPointer<PredictorBase> >& _predictors, QTextStream& _out);

	// Kalman Stats function for a tree
	void kalmanStatsOnTrack(Track* _track, QSharedPointer<SimplePredictor>& _predictor, QTextStream& _out);

	Ui::frmTest ui;

	// Folders
	QString trfFolder;
	QString tttFolder;
	QString outFolder;
	QString trfSingleFolder;
};


#endif // ttttest_h__
