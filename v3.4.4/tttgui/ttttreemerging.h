/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TTTTREEMERGING_H
#define TTTTREEMERGING_H

#include "ui_frmTreeMerging.h"

#include <QWidget>

enum MergingModes {MM_NONE = 0, MM_INTO_NEW_TREE = 1, MM_A_INTO_B = 2, MM_B_INTO_A = 3};

class TTTTreeMerging : public QWidget, public Ui::frmTreeMerging {
	Q_OBJECT

public:
	
	TTTTreeMerging(QWidget *parent = 0, const char *name = 0);
	
private slots:
	
	/**
	 * lets the user choose a tree name (for A,B or the new one depending on the button)
	 */
	void chooseTreeName();
	
	/**
	 * a merging mode was selected, so show its options and hide the others
	 * @param _on option status
	 */
	void selectMergingMode (bool);
	
	/**
	 * merge the trees A and B as set by the selected merging option
	 */
	void mergeTrees();

	/**
	 * Special merging function. See implementation for details.
	 */
	void specialMerging();

	/**
	 * Randomly add cell attributes to tree A and save as new tree e.g. to generate a nice looking tree.
	 */
	void randomlyAddCellAttributes();

	/**
	 * Cut off ends of tree A and save as new tree e.g. to generate a nice looking tree.
	 */
	void cutOffEnds(int maxTimePoint = 150);


private:
	
	///the selected merging mode
	MergingModes mergingMode;
	
};

#endif
