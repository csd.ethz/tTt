/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef graphicsviewimageitem_h__
#define graphicsviewimageitem_h__

// Qt
#include <QImage>
#include <QGraphicsItem>
#include <QPainter>


/**
 * Item for QGraphicsView to efficiently display a QImage (without  
 * conversion to QPixmap as required by QGraphicsPixmapItem).
 */
class GraphicsViewImageItem : public QGraphicsItem {

public:

	/**
	 * Constructor.
	 */
	GraphicsViewImageItem(QGraphicsItem* parent = 0) 
		: QGraphicsItem(parent)
	{
		//// Hover events to move cursor
		//setAcceptHoverEvents(true);

		//// Focusable to receive key events
		//setFlag(QGraphicsItem::ItemIsFocusable);
		m_imagePainted = true;
	}

	/**
	 * Set image.
	 */
	void setImage(const QImage& image) 
	{
		// Change image
		if(m_image.size() != image.size())
			prepareGeometryChange();
		m_image = image;
		m_imagePainted = false;

		// Redraw everything (schedules paint() event)
		update();
	}

	/**
	 * Get image.
	 */
	QImage& getImage()
	{
		return m_image;
	}

	/**
	 * Get if paint() has been executed since the last image change.
	 */
	bool getImagePainted() const
	{
		return m_imagePainted;
	}

	/**
	 * Reimplemented.
	 */
	QRectF boundingRect() const 
	{
		if(m_image.isNull())
			return QRectF();
		else
			return QRectF(QPoint(0, 0), m_image.size());
	}

protected:

	/**
	 * Reimplemented.
	 */
	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
	{
		m_imagePainted = true;
		if(!m_image.isNull()) {
			// Draw original image
			painter->drawImage(0, 0, m_image);
		}
	}


private:

	// Displayed image
	QImage m_image;

	// If paint() has been executed since last image update
	bool m_imagePainted;

};



#endif // graphicsviewimageitem_h__
