/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef treeviewcellcircle_h__
#define treeviewcellcircle_h__

// Qt
#include <QGraphicsEllipseItem>
#include <QGraphicsSceneMouseEvent>

// Project
#include "treeviewtrackitem.h"

// Forward declarations
//class TreeViewSettings;



/**
 * @author Oliver Hilsenbeck
 *
 * Represents a cell circle symbol of a displayed tree
 */
class TreeViewCellCircle : public QGraphicsEllipseItem {

public:

	/**
	 * Constructor
	 * @param _parent the TreeViewTrackItem representing the track this cell circle belongs to
	 */
	TreeViewCellCircle(TreeViewTrackItem* _parent);

	/**
	 * Update display, e.g. after selection of associated track or display settings have changed
	 */
	void updateDisplay();

protected:

	// Mouse pressed (needs to be reimplemented in order to be able to receive mouse release and mouse double click events!)
	void mousePressEvent(QGraphicsSceneMouseEvent *_event);

	// Mouse button released event
	void mouseReleaseEvent(QGraphicsSceneMouseEvent* _event);

	// Mouse hovering in
	void hoverEnterEvent(QGraphicsSceneHoverEvent* _event);

	// Mouse hovering out
	void hoverLeaveEvent(QGraphicsSceneHoverEvent* _event);

	// Mouse moved
	void mouseMoveEvent(QGraphicsSceneMouseEvent* _event);

private:

	// Item representing the associated track
	TreeViewTrackItem *trackItem;

	//// Display settings
	//TreeViewSettings *displaySettings;

	// If mouse is currently over item
	bool mouseHoveringOver;

	// If this represents the mother track
	bool isMotherCell;

	// Tree movement
	bool movingTree;
	//QPointF lastPosition;
};

#endif // treeviewcellcircle_h__
