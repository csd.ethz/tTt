/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "treeviewtreeitem.h"

// Project
#include "treeview.h"
//#include "tttbackend/treeviewsettings.h"
#include "tttbackend/itree.h"
#include "treeviewtrackitem.h"
#include "tttbackend/itrack.h"
#include "tttbackend/tttexception.h"
#include "treeviewtreenameitem.h"
#include "treeviewclickableiconitem.h"
#include "tttdata/userinfo.h"



TreeViewTreeItem::TreeViewTreeItem( TreeView *_treeView, ITree *_tree, int _posX, bool _displayCloseIcon, bool _displaySaveIcon )
{
	// Init variables
	displayCloseIcon = _displayCloseIcon;
	displaySaveIcon = _displaySaveIcon;
	tree = _tree;
	treeView = _treeView;
	//displaySettings = TreeViewSettings::getInstance();
	width = 0;
	generationCount = 0;
	treeNameItem = 0;
	closeIcon = 0;
	saveIcon = 0;
	selected = false;

	// Move to _position
	setPos(_posX, 0);

	// This item has no contents (only its children)
	setFlag(QGraphicsItem::ItemHasNoContents);

	// Update (i.e. initialize) display
	updateDisplay();
}

void TreeViewTreeItem::updateDisplay()
{
	//// Inform QT that item is about to change its shape (bounding rect)
	//prepareGeometryChange();

	// Remove old stuff
	removeAllChildren();

	 // Get max track number
	int maxTrackNumber = tree->getMaxTrackNumber();
	
	// Calc width and height
	generationCount = getHeightFromMaxTrackNumer(maxTrackNumber);
	width = getWidthFromTreeHeight(generationCount);

	// Get root and recursively construct TreeViewTrackItems
	ITrack* root = tree->getRootNode();
	if(root)
		createTreeViewTrackItems(root, width / 2, width);

	// Create treeNameItem and buttons
	updateTreeNameDisplay();

	// Update selection states
	treeView->updateCellSelectionStatesForTreeItem(this);
}

int TreeViewTreeItem::getHeightFromMaxTrackNumer( unsigned int _maxTrackNumber ) const
{
	// We look for the smallest value for height that fulfills the condition 2^height > _maxTreeNumber
	int height = 0;
	unsigned int tmpMaxTrackNumber = 1;
	while(tmpMaxTrackNumber <= _maxTrackNumber) {
		tmpMaxTrackNumber *= 2;
		++height;
	}

	return height;
}

int TreeViewTreeItem::getWidthFromTreeHeight( unsigned int _height ) const
{
	// Get distance between lines
	int lineDist = UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TREEVIEW_MAINTRACKLINESDISTANCE).toInt();
	
	// Get max number of lines in one row
	int maxLinesPerRow = pow(2.0f, (int)_height - 1);

	// This gives trees with only one track the same width like trees with 3 tracks, so they are not too thin
	if(maxLinesPerRow < 2)
		maxLinesPerRow = 2;

	// Calc required space
	return lineDist * (maxLinesPerRow-1);
}

void TreeViewTreeItem::createTreeViewTrackItems( ITrack* _track, int _x, int _width )
{
	// Check for error
	if(tracks.contains(_track->getTrackNumber()))
		throw TTTException(QString("TreeView error: attempt to insert track with number %1 twice.").arg(_track->getTrackNumber()));

	// Create new item
	TreeViewTrackItem *newItem = new TreeViewTrackItem(_track, this, _x);
	tracks.insert(_track->getTrackNumber(), newItem);

	// Child tracks
	ITrack* child1 = _track->getChild1();
	ITrack* child2 = _track->getChild2();
	if(child1)
		createTreeViewTrackItems(child1, _x - _width / 4, _width / 2);
	if(child2)
		createTreeViewTrackItems(child2, _x + _width / 4, _width / 2);

	// Create division line if necessary
	if(child1 || child2) {
		// Calc line geometry
		int lineHeight = UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TREEVIEW_TRACKLINEWIDTH).toInt(),
			lineWidth = _width / 2,
			x = _x - _width / 4,
			y = _track->getLastTimePoint() * treeView->getTreeHeightFactor() - lineHeight / 2;

		if(y > 0) {
			// Create line
			QGraphicsRectItem *newLine = new QGraphicsRectItem(x, y, lineWidth, lineHeight, this);
			cellDivisionLines.append(newLine);
			newLine->setZValue(TreeView::Z_DIVISION_LINE);

			// ToDo: Color and so on
			newLine->setBrush(QBrush(Qt::black, Qt::SolidPattern));
			newLine->setPen(Qt::NoPen);

			/* 
			Hard coded coloring used for export for open house images (04/2014)
			
			newLine->setBrush(QBrush(qRgb(172, 172, 172), Qt::SolidPattern));*/
		}
	}
}

QSize TreeViewTreeItem::sizeIncludingChildren() const
{
	QRectF childrenRect = childrenBoundingRect();
	return QSize(childrenRect.x() + childrenRect.width(), childrenRect.y() + childrenRect.height());		// The tree always has y position 0
}


QRectF TreeViewTreeItem::boundingRect() const
{
	//return boundingRectIncludingChildren();
	return QRectF(0, 0, 0, 0);
}

void TreeViewTreeItem::paint( QPainter* _painter, const QStyleOptionGraphicsItem* _option, QWidget* _widget /*= 0*/ )
{

}

void TreeViewTreeItem::removeAllChildren()
{
	// Get scene
	QGraphicsScene* scene = treeView->scene();

	// Remove all track items
	for(tracks_iterator it = tracks.begin(); it != tracks.end(); ++it) {
		// Remove from scene and delete (note: removeItem calls boundingRect() of the item)
		scene->removeItem(*it);
		delete *it;
	}
	tracks.clear();

	// Remove all division lines
	for(int i = 0; i < cellDivisionLines.size(); ++i) {
		// Remove from scene and delete
		scene->removeItem(cellDivisionLines[i]);
		delete cellDivisionLines[i];
	}
	cellDivisionLines.clear();

	// Remove name
	if(treeNameItem) {
		scene->removeItem(treeNameItem);
		delete treeNameItem;
		treeNameItem = 0;
	}

	// Remove icons
	if(saveIcon) {
		scene->removeItem(saveIcon);
		delete saveIcon;
		saveIcon = 0;
	}
	if(closeIcon) {
		scene->removeItem(closeIcon);
		delete closeIcon;
		closeIcon = 0;
	}
}

bool TreeViewTreeItem::createCloseButton()
{
	// Get image
	QPixmap img(":/ttt/ttticons/Icon_dialog-close-2.png");
	if(img.isNull())
		return false;

	// Calc position, is based on treeNameItem
	if(!treeNameItem)
		return false;

	QRectF neighborRect = treeNameItem->boundingRect();
	QPointF pos = QPointF(neighborRect.right() + 10, neighborRect.top());
	pos = treeNameItem->mapToParent(pos);

	//
	//neighborRect = treeNameItem->mapToParent(neighborRect);

	//float x = neighborRect.right() + 10;
	//float y = neighborRect.top();

	// Create button
	if(!closeIcon)
		closeIcon = new TreeViewClickableIconItem(this, img, TreeViewClickableIconItem::CLOSE_BUTTON);
	closeIcon->setPos(pos);

	return true;
}

bool TreeViewTreeItem::createSaveButton()
{
	// Get image
	QPixmap img(":/ttt/ttticons/Icon_document-save-5.png");
	if(img.isNull())
		return false;

	// Calc position, is based on closeIcon or treeNameItem if we have noce closeIcon
	QGraphicsItem* neighborItem = closeIcon;
	if(!neighborItem)
		neighborItem = treeNameItem;

	if(!neighborItem)
		return false;
	
	QRectF neighborRect = neighborItem->boundingRect();
	QPointF pos = QPointF(neighborRect.right() + 10, neighborRect.top());
	pos = neighborItem->mapToParent(pos);

	// Create button
	if(!saveIcon)
		saveIcon = new TreeViewClickableIconItem(this, img, TreeViewClickableIconItem::SAVE_BUTTON);
	saveIcon->setPos(pos);

	return true;
}

void TreeViewTreeItem::closeClicked()
{
	// Notify treeview
	treeView->notifyTreeCloseButtonClicked(tree);
}

void TreeViewTreeItem::saveClicked()
{
	// Notify treeview
	treeView->notifyTreeSaveButtonClicked(tree);
}

void TreeViewTreeItem::updateTreeNameDisplay()
{
	// Update display string
	QString displayString = getTreeDisplayString();

	if(!treeNameItem)
		treeNameItem = new TreeViewTreeNameItem(this, displayString);
	else {
		treeNameItem->setPlainText(displayString);
		treeNameItem->updateDisplay();
	}

	// Set color according to whether tree is selected
	if(selected)
		// Use same color as for selected cell circles
		treeNameItem->setDefaultTextColor(UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TREEVIEW_CELLCIRCLESELECTEDINNERCOLOR).value<QColor>());
	else
		treeNameItem->setDefaultTextColor(UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TREEVIEW_TREENAMECOLOR).value<QColor>());

	// Update icon positions
	if(displayCloseIcon)
		createCloseButton();
	if(displaySaveIcon)
		createSaveButton();
}

QString TreeViewTreeItem::getTreeDisplayString() const
{
	// Get name
	QString treeName = tree->getTreeName();
	if(treeName.isEmpty())
		treeName = "(new tree)";

	// Check if modified
	if(tree->getModified())
		treeName += "*";

	return treeName;
}

void TreeViewTreeItem::setSelectionState( bool _selected )
{
	// Nothing to do
	if(selected == _selected)
		return;

	// Change state
	selected = _selected;

	// Update display
	updateTreeNameDisplay();
}

TreeViewTrackItem* TreeViewTreeItem::getTrackItem( int _num ) const
{
	return tracks.value(_num, 0);
}

void TreeViewTreeItem::setShowTreeNameAndIcons(bool show)
{
	if(treeNameItem)
		treeNameItem->setVisible(show);
	if(closeIcon)
		closeIcon->setVisible(show);
	if(saveIcon)
		saveIcon->setVisible(show);
}

void TreeViewTreeItem::setTrackNumbersVisible(bool visible)
{
	// Show or hide track numbers of all tracks
	for(tracks_iterator it = tracks.begin(); it != tracks.end(); ++it)
		(*it)->setTrackNumberVisible(visible);
}