/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef tttconvertoldtrees_h__
#define tttconvertoldtrees_h__

#include "ui_frmConvertOldTrees.h"

// Qt
#include <QWidget>


/**
 * Tool to convert old trees into the newest format
 */
class TTTConvertOldTrees : public QWidget {

	Q_OBJECT

public:

	/**
	 * Constructor
	 */
	TTTConvertOldTrees(QWidget* _parent = 0);

private slots:

	/**
	 * Select the input folder
	 */
	void selectInputFolder();

	/**
	 * Select the output folder
	 */
	void selectOutputFolder();

	/**
	 * Start conversion
	 */
	void startConversion(QString inDir = "", QString outDir = "");

private:

	// GUI
	Ui::frmConvertOldTrees ui;

};



#endif // tttconvertoldtrees_h__
