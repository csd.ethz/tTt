/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "tttconvertoldtrees.h"

// tTt
#include "tttdata/userinfo.h"
#include "tttbackend/changecursorobject.h"
#include "tttio/fastdirectorylisting.h"
#include "tttdata/tree.h"
#include "tttio/tttfilehandler.h"
#include "tttbackend/tttmanager.h"
#include "tttbackend/tttpositionmanager.h"

// Qt
#include <QFileDialog>
#include <QMessageBox>
#include <QDir>
#include <QSettings>


TTTConvertOldTrees::TTTConvertOldTrees( QWidget* _parent )
	: QWidget(_parent)
{
	// Setup Gui
	ui.setupUi(this);

	// Signals/slots
	connect(ui.pbtSelectInputFolder, SIGNAL(clicked()), this, SLOT(selectInputFolder()));
	connect(ui.pbtSelectOutputFolder, SIGNAL(clicked()), this, SLOT(selectOutputFolder()));
	connect(ui.pbtStartConversion, SIGNAL(clicked()), this, SLOT(startConversion()));

	// Load paths
	QSettings* localSettings = UserInfo::getInst().getLocalSettings();
	if(localSettings) {
		ui.lieInputFolder->setText(localSettings->value(UserInfo::KEY_TTTCONVERTTREES_INPATH, QString()).toString());
		ui.lieOutputFolder->setText(localSettings->value(UserInfo::KEY_TTTCONVERTTREES_OUTPATH, QString()).toString());
	}
}

void TTTConvertOldTrees::selectInputFolder()
{
	// Open folder selection dialog
	QString newFolder = QFileDialog::getExistingDirectory(this, "Select input folder");
	if(newFolder.isEmpty())
		return;

	ui.lieInputFolder->setText(newFolder);

	// Remember path
	QSettings* localSettings = UserInfo::getInst().getLocalSettings();
	if(localSettings) {
		localSettings->setValue(UserInfo::KEY_TTTCONVERTTREES_INPATH, newFolder);
	}
}

void TTTConvertOldTrees::selectOutputFolder()
{
	// Open folder selection dialog
	QString newFolder = QFileDialog::getExistingDirectory(this, "Select output folder");
	if(newFolder.isEmpty())
		return;

	ui.lieOutputFolder->setText(newFolder);

	// Remember path
	QSettings* localSettings = UserInfo::getInst().getLocalSettings();
	if(localSettings) {
		localSettings->setValue(UserInfo::KEY_TTTCONVERTTREES_OUTPATH, newFolder);
	}
}

void TTTConvertOldTrees::startConversion(QString inDir, QString outDir)
{
	// Check folders
	if(inDir.isEmpty())
		inDir = ui.lieInputFolder->text();
	if(outDir.isEmpty())
		outDir = ui.lieOutputFolder->text();
	if(inDir.isEmpty() || outDir.isEmpty() || !QDir(inDir).exists() || !QDir(outDir).exists()) {
		QMessageBox::information(this, "Note", "The specified paths are invalid.");
		return;
	}

	// Remember paths
	QSettings* localSettings = UserInfo::getInst().getLocalSettings();
	if(localSettings) {
		localSettings->setValue(UserInfo::KEY_TTTCONVERTTREES_INPATH, inDir);
		localSettings->setValue(UserInfo::KEY_TTTCONVERTTREES_OUTPATH, outDir);
	}

	// Get any pos manager
	TTTPositionManager* tttpm = TTTManager::getInst().getCurrentPositionManager();
	if(!tttpm) {
		QMessageBox::information(this, "Note", "Please select any position first in the position layout window.");
		return;
	}

	// Make sure paths end with '/'
	inDir = QDir::fromNativeSeparators(inDir);
	outDir = QDir::fromNativeSeparators(outDir);
	if(inDir.right(1) != "/")
		inDir.append('/');
	if(outDir.right(1) != "/")
		outDir.append('/');

	// Wait cursor
	ChangeCursorObject cc;

	// Trees that needed +4 bytes and failed trees
	QStringList plusFourBytesTrees;
	QStringList failedTrees;

	// List sub directories and main directory (.)
	QStringList subDirs = QDir(inDir).entryList(QDir::Dirs | QDir::NoDotDot | QDir::NoSymLinks);

	// If input folder contains several experiments, we need to list the subfolders in there and work on them
	if(ui.chkConvertSeveralExperiments->isChecked()) {
		QStringList finalList;
		for(int i = 0; i < subDirs.size(); ++i) {
			if(subDirs[i] == ".")
				continue;

			// List directories in current experiment folder and add to final list
			QStringList tmp = QDir(inDir + subDirs[i]).entryList(QDir::Dirs | QDir::NoDotDot | QDir::NoSymLinks);
			for(auto it = tmp.begin(); it != tmp.end(); ++it)
				*it = subDirs[i] + "/" + *it;

			finalList.append(tmp);
		}

		subDirs = finalList;
	}

	// Convert files
	QDir outQDir(outDir);
	for(int i = 0; i < subDirs.size(); ++i) {
		QString curDir = subDirs[i];

		// Create directory in target directory
		if(curDir != ".") {
			if(!outQDir.exists(curDir)) {
				if(!outQDir.mkdir(curDir)) {
					QMessageBox::critical(this, "Error", QString("Cannot create output directory '%1'.").arg(outDir + curDir));
					return;
				}
			}
		}
		QString curInputPath = inDir + curDir + "/";
		QString curOutputPath = outDir + curDir + "/";

		// List ttt files in curInputPath
		QStringList extensions;
		extensions << ".ttt";
		QStringList inputFiles = FastDirectoryListing::listFiles(QDir(curInputPath), extensions);

		// Convert ttt files
		for(int j = 0; j < inputFiles.size(); ++j) {
			// Read file
			QString curFile = curInputPath + inputFiles[j];
			int trackCount, firstTP, lastTP;
			Tree *curTree = new Tree();
			curTree->reset();
			if (TTTFileHandler::readFile (curFile, curTree, trackCount, firstTP, lastTP, &tttpm->positionInformation, false, true) != TTT_READING_SUCCESS) {
				// Attempt 4 more bytes
				if (TTTFileHandler::readFile (curFile, curTree, trackCount, firstTP, lastTP, &tttpm->positionInformation, true, true) != TTT_READING_SUCCESS) {
					//QMessageBox::critical(this, "Error", QString("Cannot read tTt file '%1'.").arg(curFile));
					failedTrees.append(curFile);
					
					delete curTree;
					continue;
				}
				else {
					plusFourBytesTrees.append(curFile);
				}
			}

			// Old positions
			bool forceOldPositions = false;
			if(curTree->getFileVersion() <= 14)
				forceOldPositions = true;

			// Save file
			QString outFile = curOutputPath + inputFiles[j];
			if (!TTTFileHandler::saveFile(outFile, curTree, firstTP, lastTP, -1, forceOldPositions)) {
				QMessageBox::critical(this, "Error", QString("Cannot save tTt file '%1'.").arg(outFile));
				delete curTree;
				return;
			}

			delete curTree;
		}
	}

	QMessageBox::information(this, "Finished", QString("Tree conversion completed.\n'read 4 more bytes' needed for %1 Trees.\nCould not convert %2 trees.").arg(plusFourBytesTrees.size()).arg(failedTrees.size()));

	// Create final log file message
	QString msg = "Tree conversion completed.";
	if(!plusFourBytesTrees.isEmpty()) {
		msg += "\nThe following trees could only be converted using 'read 4 more bytes':\r\n";
		for(int i = 0; i < plusFourBytesTrees.length(); ++i) {
			msg += plusFourBytesTrees[i];
			msg += "\r\n";
		}
	}
	if(!failedTrees.isEmpty()) {
		msg += "\nThe following trees could NOT be converted:\r\n";
		for(int i = 0; i < failedTrees.length(); ++i) {
			msg += failedTrees[i];
			msg += "\r\n";
		}
	}
	
	// Create log file
	QFile outFile(outDir + "conversionlog.txt");
	if(!outFile.open(QIODevice::WriteOnly)) {
		QMessageBox::critical(this, "Error", "Could not create log file.");
		return;
	}

	// Write to log file
	QTextStream out(&outFile);
	out << msg;
}
