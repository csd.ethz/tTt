/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "clickableqgraphicspolygonitem.h"

#include <QGraphicsSceneMouseEvent>
#include <QPen>
#include <QBrush>
#include <QMouseEvent>

ClickableQGraphicsPolygonItem::ClickableQGraphicsPolygonItem (const QString &_parameter)
        : QGraphicsPolygonItem(), CallbackItem (_parameter)
{
}

ClickableQGraphicsPolygonItem::ClickableQGraphicsPolygonItem (QGraphicsPolygonItem &_polyItem, const QString &_parameter)
        : QGraphicsPolygonItem(), CallbackItem (_parameter)
{
        setPolygon (_polyItem.polygon());
        setPen (_polyItem.pen());
        setBrush (_polyItem.brush());
        setMatrix (_polyItem.matrix());
}

void ClickableQGraphicsPolygonItem::mousePressEvent (QGraphicsSceneMouseEvent *_ev)
{
        this->QGraphicsItem::mousePressEvent (_ev);

        QMouseEvent *ev = new QMouseEvent (_ev->type(), _ev->pos().toPoint(), _ev->button(), _ev->buttons(), _ev->modifiers());
        callCBMP (this, ev);

        _ev->accept();
}

void ClickableQGraphicsPolygonItem::mouseReleaseEvent (QGraphicsSceneMouseEvent *_ev)
{
        this->QGraphicsItem::mouseReleaseEvent (_ev);

        QMouseEvent *ev = new QMouseEvent (_ev->type(), _ev->pos().toPoint(), _ev->button(), _ev->buttons(), _ev->modifiers());
        callCBMR (this, ev);

        _ev->accept();
}

