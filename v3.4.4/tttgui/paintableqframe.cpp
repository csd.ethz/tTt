/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "paintableqframe.h"

#include <QPainter>
#include <QImage>
#include <QString>

PaintableQFrame::PaintableQFrame (QWidget *_parent)
        : QFrame (_parent), pix (0)
{
}

void PaintableQFrame::paintEvent (QPaintEvent *)
{
        //NOTE: this method has become a little testing field for graphics routines; actually necessary are only the lowest three lines
        //      so please, while playing, keep these lines the lowest ones!

        if (pix) {
                //bitBlt (this, 0, 0, pix, 0, 0, pix->width(), pix->height(), QPainter::CompositionMode_Source);
//                QImage img_pix = pix->toImage();
//                QImage img = img_pix.convertToFormat (QImage::Format_Indexed8);
//                //drawing img works, why not img2?
//                QImage img2 (pix->width(), pix->height(), QImage::Format_Indexed8);
//                //copy() also works
//                //img2 = img.copy (0, 0, img.width(), img.height());

//drawImage() does not work, for whatever f...ing reason!
//                QPainter p2 (&img2);
//                p2.drawImage (0, 0, img, 0, 0, -1, -1, Qt::ColorOnly);
//                p2.end();

//                //qDebug (QString ("%1").arg (img2.depth()).toAscii());

//                QPainter p (this);
//                p.drawImage (0, 0, img2);
//                p.end();


                QPainter p (this);
                p.drawPixmap (0, 0, *pix, 0, 0, pix->width(), pix->height());
                p.end();
        }
}

void PaintableQFrame::setPixmap (QPixmap *_pixmap)
{
        pix = _pixmap;
}
