/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TIMESCALE_H
#define TIMESCALE_H


#include "tttgui/tttmovie.h"
#include "tttbackend/fileinfoarray.h"
#include "tttdata/tree.h"
#include "myqcanvasline.h"
#include "myqcanvastext.h"
#include "tttdata/stylesheet.h"
#include "tttbackend/treeelementstyle.h"

#include <qevent.h>
#include <qpoint.h>
#include <qdatetime.h>
#include <q3canvas.h>
#include <qmatrix.h>
#include <q3intdict.h>
//Added by qt3to4:
#include <QMouseEvent>

///the usage constants for MyQCanvas... objects
const int LineUsageTimeScaleLine = 101;
const int TextUsageTimeScaleText = 201;


const QColor ExistingColor[] = {Qt::lightGray, Qt::gray, Qt::darkGray, Qt::black, Qt::darkYellow, Qt::yellow, Qt::darkGray, Qt::gray, Qt::lightGray, Qt::black};
const QColor SelectedColor = Qt::blue;
const QColor LoadedColor = Qt::green;

const QColor SelectionColor = Qt::magenta;		// !!!turns into green when displayed???

/**
@author Bernhard
*
*	A widget for displaying the time scale, based on a QCanvasView
*	All necessary drawing and region selecting methods are implemented here
*/

class TimeScale : public Q3CanvasView
{

Q_OBJECT

public:
	TimeScale(QWidget *_container, QWidget *parent = 0, const char *name = 0);
	
	~TimeScale();
	
	/**
	 * initializes the timescale
	 */
	void init();
	
	/**
	 * resizes the size of the widget to fit exactly into the container
	 */
	void fit();
	
	void contentsMousePressEvent (QMouseEvent *);
	void contentsMouseReleaseEvent (QMouseEvent *);
	void contentsMouseMoveEvent (QMouseEvent *);
	
	/**
	 * sets the current zoom factor
	 * @param _zoomFactor the zoom factor (in percent)
	 */
	void setZoomFactor (int _zoomFactor);
	
	/**
	 * @return whether the timescale is in selection mode (true, iff the selection box is displayed)
	 */
	bool isSelecting() const
		{return Selecting == 1;}
	
	/**
	 * sets the TopSecond to the provided value and updates the display
	 * used for shifting the timescale
	 * @param _topSecond the top visible second
	 */
	void setTopSecond (int _topSecond);
	
signals:

	/**
	 * emitted when the user clicked during selection mode into the timescale
	 * @param _start the first selected timepoint
	 * @param _stop the last selected timepoint
	 */
	void regionSelected (int _start, int _stop);
	
public slots:
	
	///draws the time scale into the pixmap scale
	///optionally a timepoint range can be specified, which should be updated
	/// - nothing else is drawn => much faster
	// /@param _ss the style sheet used for drawing
	void draw(); // (StyleSheet *_ss); // (long int _timePointStart = -1, long int _timePointEnd = -1, int _lineWidthFactor = 100000);
	
	/**
	 * shifts the display
	 * @param _shift the shift offset (in pixel coordinates)
	 */
	void shiftTimeScale (QPoint _shift);
	
	/**
	 * toggles (displays/hides) the selection box
	 * @param _select if == 0, the selection mode is closed; if == 1, the selection mode is started
	 */
	void selectRegion (int _select);
	
	/**
	 * selects a timepoint region from _start to _stop (the selection box is updated)
	 * (as if the user had shifted/changed the selection box)
	 * @param _start the first timepoint to select
	 * @param _stop the last timepoint to select
	 */
	void selectRegion (int _start, int _stop);

	/**
	 * Resets the selection
	 */
	void resetSelection();
	
private:
	
	///the canvas that contains the timescale
	Q3Canvas canvas;

	///the "parent" widget (necessary to keep for size adjustment)
	QWidget *Container;
	
	///the current zoom factor (in percent)
	int zoomFactor;
	
	///the number of displayed seconds 
	///(all seconds between FirstTimePoint and LastTimePoint)
	long int DisplayedSeconds;
	
	///contains the topmost second currently displayed
	long int TopSecond;
	
	///the top in pixel
	long int top;
	
	///Selection mode
	///0: not selecting
	///1: selecting
	char Selecting;
	
	///contain the first and last timepoints of the current selection
	int TopSelected;
	int BottomSelected;
	
	///the selection box
	Q3CanvasRectangle *selection;
	
	///the mouse postion before mouseMoveEvent()
	QPoint oldMousePos;
	
	///whether the middle mouse button is pressed
	bool MiddleButtonPressed;
	
	
///private methods
	
	
	/**
	 * draws the rectbox in the left part of the timescale
	 * @param _top the top value (in seconds / TWHF)
	 * @param _height the height of the box (in seconds / TWHF)
	 */
	void drawSelectionBox (int _top, int _height);
	
	/**
	 * returns the value of _y transformed to the logical coordinate system, regarding shift & zoom
	 * @param _y the absolute y value
	 * @param _shift indicates whether _y is a shift offset or a real position
	 * @return the logical value of _y
	 */
	int getLogical (int _y, bool _shift = false);
};

#endif
