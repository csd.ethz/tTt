/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CIRCLE_H
#define CIRCLE_H

#include <qcolor.h>
#include <qpoint.h>

/**
	@author Bernhard
	
	This class holds a simple circle and is intended to be stored in a QValueVector\<MyCircle\>.
*/

class MyCircle {
public:
	/**
	 * constructs a default circle (x/y = -1/-1)
	 */
	MyCircle()
		{reset();}
	
	/**
	 * constructs a circle from the given parameters
	 * @param _x the abscisse of the middle
	 * @param _y the ordinate of the middle
	 * @param _r the radius of the circle
	 * @param _color the color of the outline
	 * @param _connect whether this circle should be connected with its precessor
	 * @param _connex_line_thickness the thickness of the connection line
	 * @param _timeStamp the time stamp
	 */
	MyCircle (int _x, int _y, int _r, QColor _color, bool _connect, int _connex_line_thickness = 1, int _timeStamp = -1)
		{setParams (_x, _y, _r, _color, _connect, _connex_line_thickness, _timeStamp);}
	
	~MyCircle() 
		{}
	
	/**
	 * sets the attributes of this circle
	 * @param _x the abscisse of the middle
	 * @param _y the ordinate of the middle
	 * @param _r the radius of the circle
	 * @param _color the color of the outline
	 * @param _connect whether this circle should be connected with its precessor
	 * @param _connex_line_thickness the thickness of the connection line
	 * @param _timeStamp the time stamp
	 */
	void setParams (int _x = -1, int _y = -1, int _r = -1, QColor _color = Qt::black, bool _connect = false, int _connex_line_thickness = 1, int _timeStamp = -1)
		{
			if (_x != -1)
				x = _x;
			if (_y != -1)
				y = _y;
			if (_r > -1)
				r = _r;
			color = _color;
			connex = _connect;
			timeStamp = _timeStamp;
			connex_line_thickness = _connex_line_thickness;
                        usage = "";
		}
	
        /**
         * sets the usage parameter (see pictureview.h for some possible values)
         */
        void setUsageParameter (const QString &_usage)
                {usage = _usage;}

        /**
         * @return the usage parameter
         */
        const QString& getUsageParameter() const
                {return usage;}

	/**
	 * sets the default values for this circle (x,y,r = -1, color = black, not connected)
	 */
	void reset()
		{
			x = -1;
			y = -1;
			r = -1;
			color = Qt::black;
			connex = false;
			timeStamp = -1;
			connex_line_thickness = 1;
                        usage = "";
		}
	
	/**
	 * @return whether the circle was not yet initialized
	 */
	bool isNull() const
		{return x == -1;}
	
	
	/**
	 * @return the middle point of the circle 
	 */
	QPoint getMiddle() const
		{return QPoint (x, y);}
	/**
	 * @return the abscisse of the middle point
	 */
	int getX() const
		{return x;}
	/**
	 * @return the ordinate of the middle point
	 */
	int getY() const
		{return y;}
	/**
	 * @return the radius of the circle
	 */
	int getRadius() const
		{return r;}
	/**
	 * @return the color of the circle
	 */
	QColor getColor() const
		{return color;}
	/**
	 * @return the connection flag for this circle
	 */
	bool getConnect() const
		{return connex;}
	
	/**
	 * sets the time stamp of this circle
	 * @param _timeStamp the time (seconds, timepoints, whatever)
	 */
	void setTimeStamp (int _timeStamp)
		{timeStamp = _timeStamp;}
	
	/**
	 * @return the time stamp of this circle
	 */
	int getTimeStamp() const
		{return timeStamp;}
	
	
	void setConnectLineThickness (int _connex_line_thickness)
		{connex_line_thickness = _connex_line_thickness;}
	
	int getConnectLineThickness() const
		{return connex_line_thickness;}
	
	
	
	
private:

	///the middle point
	int x;
	int y;
	
	///the radius
	int r;
	
	///the color
	QColor color;
	
	///whether this circle should be connected with the previous drawn circle
	bool connex;
	
	///the thickness of the connection line
	int connex_line_thickness;
	
	///the time stamp (used for cell paths)
	int timeStamp;

        ///the usage parameter
        QString usage;
};

#endif
