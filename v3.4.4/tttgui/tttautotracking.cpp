/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "tttautotracking.h"

// Qt
#include <QFileDialog>
#include <QMessageBox>
#include <QSettings>
#include <QProgressBar>
#include <QProgressDialog>
#include <QShowEvent>

// Project
#include "tttbackend/tttexception.h"
#include "tttio/fastdirectorylisting.h"
#include "tttdata/userinfo.h"
#include "tttautotrackingtreewindow.h"
#include "tttbackend/tttmanager.h"
#include "tttbackend/changecursorobject.h"
#include "pictureview.h"
#include "autotrackingcellcircle.h"
#include "tttpositionlayout.h"
#include "tttmovie.h"
#include "tttbackend/tools.h"

// Debug information variable defined in main.cpp
extern int g_debugInformation;

// Static member definitions
const QColor TTTAutoTracking::USED_TREES_COLOR = Qt::darkBlue;


TTTAutoTracking::TTTAutoTracking( QWidget *_parent /*= 0*/ )
	: QWidget(_parent)
{
	ui.setupUi(this);

	// Init variables
	frmTreeWindow = 0;
	windowLayoutLoaded = false;

	// Setup connections
	connect(ui.pbtLoad, SIGNAL(clicked()), this, SLOT(loadTreeFragments()));
	connect(ui.lieInputFolder, SIGNAL(returnPressed()), this, SLOT(loadTreeFragments()));
	connect(ui.pbtSelectFolder, SIGNAL(clicked()), this, SLOT(pbtSelectFolderClicked()));
	connect(ui.positionView, SIGNAL(positionLeftClicked(const QString&)), this, SLOT(positionLeftClicked(const QString&)));
	//connect(ui.pbtOpenTTTFile, SIGNAL(clicked()), this, SLOT(openTTTFile()));
	connect(ui.chkSelectOverlapping, SIGNAL(toggled(bool)), this, SLOT(selectOverlappingToggled(bool)));
	connect(ui.pbtOpenTreeWindow, SIGNAL(clicked()), this, SLOT(showTreeWindow()));
	connect(ui.positionView, SIGNAL(positionDoubleClicked(const QString&)), this, SLOT(positionDoubleClicked(const QString&)));
	connect(ui.pbtImport, SIGNAL(clicked()), this, SLOT(importAutoTrackingResults()));
	connect(ui.pbtUnload, SIGNAL(clicked()), this, SLOT(reset()));
	connect(ui.cmbImportMethod, SIGNAL(currentIndexChanged(int)), this, SLOT(selectedMethodChanged(int)));
}

TTTAutoTracking::~TTTAutoTracking()
{

}

void TTTAutoTracking::pbtSelectFolderClicked()
{
	// Get filename
	QString newFolder = QFileDialog::getExistingDirectory(this, "Select input folder", ui.lieInputFolder->text());
	if(newFolder.isEmpty())
		return;

	// Change lieInputFolder
	ui.lieInputFolder->setText(newFolder);

	// Load data
	loadTreeFragments();
}

void TTTAutoTracking::loadTreeFragments()
{
	QString folder = ui.lieInputFolder->text();

	// Check if folder exists
	QDir dir(folder);
	if(folder.isEmpty() || !dir.exists()) {
		QMessageBox::information(this, "Error", "The path you specified does not exist.");
		return;
	}

	// Folder is valid
	folder = QDir::fromNativeSeparators(folder);
	if(folder.right(1) != "/")
		folder.append('/');
	ui.lieInputFolder->setText(folder);

	// Actually read data
	try {
		loadTreeFragmentsInternal();
	}
	catch(const TTTException& _e) {
		// Error
		QMessageBox::critical(this, "Error", "An error occurred:\n\n" + _e.what());
	}
}

bool TTTAutoTracking::displayTrackpoints() const
{
	return ui.chkDisplayTrackpoints->isChecked();
}

void TTTAutoTracking::loadTreeFragmentsInternal()
{
	// Directories to look in for trf files
	QStringList directories;

	// Get selected positions
	QList<TTTPositionManager*> positions = ui.positionView->getActivePositions();
	if(positions.isEmpty()) {
		// Offer to use all folders
		if(QMessageBox::question(this, "Use all positions?", "No positions selected - load all available tree fragments?", QMessageBox::Yes | QMessageBox::No) != QMessageBox::Yes)
			return;

		// Use all folders
		directories = QDir(ui.lieInputFolder->text()).entryList(QDir::Dirs | QDir::NoDotAndDotDot);
	}
	else {
		// Use position folders
		for(int i = 0; i < positions.size(); ++i) {
			QString baseName = positions[i]->getBasename();
			if(!baseName.isEmpty())
				directories.append(baseName);
		}
	}

	// Always look in parent dir for backward compatibility
	directories.append(".");

	// Unload old stuff
	reset();

	// List files
	QStringList fileNames;
	for(int i = 0; i < directories.size(); ++i) {
		listTrfFiles(ui.lieInputFolder->text() + directories[i], fileNames, directories[i] + '/');
	}
	if(fileNames.size() == 0) {
		QMessageBox::information(this, "Note", "Could not find any tree fragments in the specified input folder.");
		return;
	}

	// Display progress bar
	QProgressDialog progessBar("Loading data..", "Cancel", 0, fileNames.size(), this);
	progessBar.setWindowModality(Qt::ApplicationModal);
	progessBar.setMinimumDuration(0);
	progessBar.setValue(0);

	// Get current base name for checking if tree fragments belong to our experiment
	const QString baseName = TTTManager::getInst().getExperimentBasename();
	bool checkBaseName = !baseName.isEmpty();

	// Read tree fragments
	int counter = 0;
	for(QStringList::const_iterator it = fileNames.constBegin(); it != fileNames.constEnd() && !progessBar.wasCanceled(); ++it) {
		// Check basename
		if(checkBaseName) {
			// Get basename of tree fragment
			QString curBaseName = it->left(it->indexOf('_'));
			int tmpIndex = curBaseName.lastIndexOf('/');
			if(tmpIndex >= 0)
				curBaseName = curBaseName.mid(tmpIndex + 1);
			if(!curBaseName.isEmpty()) {
				// Compare
				if(curBaseName != baseName) {
					QString msg = QString("The specified folder seems to contain tree fragments (%1) that do not belong to this experiment (%2).\n\nContinue loading?").arg(*it).arg(baseName);
					if(QMessageBox::warning(this, "Warning", msg, QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel) != QMessageBox::Yes) {
						// Cancel
						reset();
						return;
					}

					// Continue anyways
					checkBaseName = false;
				}
			}
		}
		
		// Read tree
		TreeFragment *tmp = new TreeFragment(ui.lieInputFolder->text() + *it, trackpointsHashmap);
		QSharedPointer<TreeFragment> nextTree(tmp);
		tmp->setWeakPointer(QWeakPointer<TreeFragment>(nextTree));

		// Get position
		int pos = nextTree->getPositionNumber();
		
		// Add to hashmap
		if(!trees.contains(pos))
			trees.insert(pos, QList<QSharedPointer<TreeFragment> >());
		trees[pos].append(nextTree);

		// Update progress bar
		progessBar.setValue(++counter);
	}

	// Display results
	updateDisplay();

	// Display in movie windows
	TTTManager::getInst().redrawTracks();

	// Emit signal
	emit loadedTreeFragmentsChanged();
}

void TTTAutoTracking::reset()
{
	// Just empty containers, QSharedPointer will do the rest
	trees.clear();
	trackpointsHashmap.clear();

	// Emit signal
	emit loadedTreeFragmentsChanged();

	// Update position display
	updateDisplay();
	TTTManager::getInst().redrawTracks();
}

void TTTAutoTracking::updateDisplay()
{
	// Update position display
	ui.positionView->updateDisplay();
}

void TTTAutoTracking::showEvent( QShowEvent *_ev )
{
	if(!_ev->spontaneous() && !windowLayoutLoaded) {
		// Load window layout
		UserInfo::getInst().loadWindowPosition(this, "TTTAutoTracking");
		windowLayoutLoaded = true;

		// Load chkSelectOverlapping state
		ui.chkSelectOverlapping->setChecked(UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TTTAUTOTRACKING_SELECTCONNECTED).toBool());

		// Propose experiment subfolder 
		QString proposal = TTTManager::getInst().frmPositionLayout->getExperimentDir().absolutePath();
		if(!proposal.isEmpty()) {
			proposal = QDir::fromNativeSeparators(proposal);
			if(proposal.right(1) != "/")
				proposal.append('/');

			// Check if new autotracking4 trees folder exists
			QString testFolder = proposal + "TTTrack/Trees";
			if(QDir(testFolder).exists()) 
				proposal = testFolder;
			else {
				// Check if 'treesEdited' folder exists
				testFolder = proposal + "autotracking/treesEdited";
				if(QDir(testFolder).exists())
					proposal = testFolder;
				else
					proposal += "autotracking/trees/";
			}

			ui.lieInputFolder->setText(proposal);
		}
	}

	// Init position display
	if(!ui.positionView->isInitialized()) {
		ui.positionView->initialize(true, false, false);
		ui.positionView->setTreeCountTreeFragments(true);
	}

	// Update display if event is not spontaneous
	if(!_ev->spontaneous()) {
		updateDisplay();

		// Init position selection
		ui.cmbImportPosition->clear();
		std::vector<TTTPositionManager*> positions = TTTManager::getInst().getAllPositionManagersSorted();
		for(int i = 0; i < positions.size(); ++i) {
			ui.cmbImportPosition->insertItem(positions[i]->positionInformation.getIndex());
		}
	}

	_ev->accept();
}

void TTTAutoTracking::closeEvent( QCloseEvent *_ev )
{
	// Close tree window if it is visible
	if(frmTreeWindow) {
		// Close
		if(!frmTreeWindow->close()) {
			// Ignore close event
			_ev->ignore();
			return;
		}

		// Delete
		delete frmTreeWindow;
		frmTreeWindow = 0;
	}

	// Save window layout
	UserInfo::getInst().saveWindowPosition(this, "TTTAutoTracking");

	// Clear data
	reset();

	_ev->accept();
}

int TTTAutoTracking::getTreeFragmentCount( const QString& _index )
{
	// Convert to int
	bool ok;
	int pos = _index.toInt(&ok);

	// If conversion failed or pos is not in trees, return 0
	if(ok && trees.contains(pos))
		return trees[pos].size();
	else
		return 0;
}

void TTTAutoTracking::positionLeftClicked( const QString &_index )
{
	// Confirm selection
	bool select;
	if(ui.positionView->isPositionSelected(_index)) {
		ui.positionView->deSelectPosition(_index);
		select = false;
	}
	else {
		ui.positionView->selectPosition(_index);
		select = true;
	}
	
	// (De-)select all overlapping positions if desired
	if(ui.chkSelectOverlapping->isChecked()) {
		// Wait cursor
		ChangeCursorObject cc;

		// Additional positions that need to be selected
		QStringList additionalPositions;

		// Get list of all positions
		QHash<int, TTTPositionManager*>& allPositions = TTTManager::getInst().getAllPositionManagers();

		// Find connected positions
		std::vector<TTTPositionManager*> posManagersToProcess;
		posManagersToProcess.push_back(TTTManager::getInst().getPositionManager(_index));
		while(posManagersToProcess.size() > 0) {
			// Take last element
			TTTPositionManager* tttpm = posManagersToProcess[posManagersToProcess.size()-1];
			QRectF curRect = tttpm->getGlobalImageRect();
			posManagersToProcess.pop_back();

			// Find overlapping positions
			for(auto it = allPositions.constBegin(); it != allPositions.constEnd(); ++it) {
				if((*it)->getGlobalImageRect().intersects(curRect)) {
					// Overlap detected
					if(!additionalPositions.contains((*it)->positionInformation.getIndex())) {
						additionalPositions.append((*it)->positionInformation.getIndex());
						posManagersToProcess.push_back(*it);
					}
				}
			}
		}

		// (De-)select additional positions
		for(int i = 0; i < additionalPositions.size(); ++i) {
			if(!ui.positionView->isPositionSelected(additionalPositions[i])) {
				if(select)
					ui.positionView->selectPosition(additionalPositions[i]);
			}
			else {
				if(!select)
					ui.positionView->deSelectPosition(additionalPositions[i]);
			}
		}
	}
}


void TTTAutoTracking::openTTTFile()
{
	// Open tree in tree window
	showTreeWindow();

	// Let user open tree
	if(frmTreeWindow)
		frmTreeWindow->openExistingTreeFiles();
}

void TTTAutoTracking::showTreeWindow()
{
	// Create if necessary
	if(!frmTreeWindow)
		frmTreeWindow = new TTTAutoTrackingTreeWindow(this);
	frmTreeWindow->raise();
	frmTreeWindow->show();
}



QList<QSharedPointer<TreeFragment> > TTTAutoTracking::findAssociatedTreeFragments( QString _treeName )
{
	QList<QSharedPointer<TreeFragment> > ret;
	if(_treeName.isEmpty())
		return ret;

	// Iterate over loaded fragments
	for(QHash<int, QList<QSharedPointer<TreeFragment> > >::const_iterator it = trees.constBegin(); it != trees.constEnd(); ++it) {
		const QList<QSharedPointer<TreeFragment> >& curList = *it;
		for(QList<QSharedPointer<TreeFragment> >::const_iterator it2 = curList.constBegin(); it2 != curList.constEnd(); ++it2) {
			TreeFragment* curFr = it2->data();

			// Compare associated tree
			if(curFr->getAssociatedTreeName() == _treeName)
				ret.append(*it2);
		}
	}

	return ret;
}

QList<QSharedPointer<TreeFragment> > TTTAutoTracking::getTreeFragmentsStartingInRange(int _tp1, int _tp2, const QPointF& _point, float _maxDistPerTP, QList<float>* _distances)
{
	QList<QSharedPointer<TreeFragment> > ret;

	// Iterate over loaded fragments
	for(QHash<int, QList<QSharedPointer<TreeFragment> > >::const_iterator it = trees.constBegin(); it != trees.constEnd(); ++it) {
		const QList<QSharedPointer<TreeFragment> >& curList = *it;
		for(QList<QSharedPointer<TreeFragment> >::const_iterator it2 = curList.constBegin(); it2 != curList.constEnd(); ++it2) {
			TreeFragment* curFr = it2->data();
			
			// Skip invalid fragments
			if(curFr->isInvalid())
				continue;

			// Check timepoint
			int firstTimePoint = curFr->getFirstTimePoint();
			if(firstTimePoint >= _tp1 && firstTimePoint <= _tp2) {
				// Check range
				TreeFragmentTrack* root = curFr->getRootNode();
				if(root) {
					TreeFragmentTrackPoint* tp = root->getFirstTrackPoint();
					if(tp) {
						// Calc distance by using QLineF object
						float distance = QLineF(QPointF(tp->posX, tp->posY), _point).length(); 
						if(distance <= _maxDistPerTP * (firstTimePoint - _tp1 + 1)) {
							ret.append(*it2);
							if(_distances)
								_distances->append(distance);
						}
					}
				}

			}
		}
	}

	return ret;
}

Tree* TTTAutoTracking::createTreeFromFragment( QSharedPointer<TreeFragment> _fragment )
{
	// Check selected fragment
	if(_fragment.isNull())
		return 0;

	// Get position manager
	TTTPositionManager* tttpm = TTTManager::getInst().getBasePositionManager();
	if(!tttpm)
		return 0;

	// Change cursor
	ChangeCursorObject cc;

	// Create tree from it
	QSharedPointer<Tree> frTree = _fragment->convertToTree(tttpm->getLastTimePoint());
	if(!frTree) {
		QMessageBox::critical(this, "Error", "Error: Could not convert selected tree fragment to tTt-tree.");
		return 0;
	}

	// Set associated tree name
	_fragment->setAssociatedTree("(new tree)");

	// Open autotracking tree window
	showTreeWindow();

	// Display tree
	QList<QSharedPointer<TreeFragment> > fragments;
	fragments.append(_fragment);
	frmTreeWindow->addTree(frTree, fragments);

	// Emit signal 
	treeFragmentUsedForTree(_fragment->getTreeFragmentNumber());

	// Done
	return frTree.data();
}

QList<AutoTrackingCellCircle*> TTTAutoTracking::getCellCirclesForDisplay( int _timePoint, TTTPositionManager* _tttpm, int _wlIndex, bool _noText) const
{
	QList<AutoTrackingCellCircle*> ret;

	// Check if a track is currently selected
	if(frmTreeWindow && frmTreeWindow->trackIsSelected()) {
		// Only trees from frmTreeWindow should be displayed -> delegate to frmTreeWindow
		return frmTreeWindow->getCellCirclesForDisplay(_timePoint, _tttpm, _wlIndex);
	} 
	else if(ui.chkDisplayTrackpoints->isChecked()) {
		// Display only trackpoints that are actually visible in provided position (including some margin)
		QRectF posRect = _tttpm->getGlobalImageRect();
		const float margin = 10.0f;
		posRect.setLeft(posRect.left() - margin);
		posRect.setTop(posRect.top() - margin);
		posRect.setBottom(posRect.bottom() + margin);
		posRect.setRight(posRect.right() + margin);

		// Display all loaded fragments
		const QList<QSharedPointer<TreeFragmentTrackPoint> > tps = getTrackPointsByTimePoint(_timePoint);
		for(QList<QSharedPointer<TreeFragmentTrackPoint> >::const_iterator it = tps.constBegin(); it != tps.constEnd(); ++it) {
			TreeFragmentTrackPoint* curTrackPoint = it->data();

			// Skip if outside posRect
			if(!posRect.contains(curTrackPoint->posX, curTrackPoint->posY))
				continue;

			// Create circle
			AutoTrackingCellCircle* newCircle = createCellCircleFromTreeFragmentTrackPoint(
				*curTrackPoint, 
				_tttpm,
				_wlIndex, 
				AutoTrackingCellCircle::A_CREATE_TREE_OR_INVALIDATE, 
				"Click with left mouse button to create a new tree based on this tree fragment or with right mouse button to mark this tree fragment as invalid.",
				0,
				_noText);

			// Add item
			if(newCircle)
				ret.append(newCircle);
		}
	}
	return ret;
}

void TTTAutoTracking::cellCircleClicked( AutoTrackingCellCircle* _circle, Qt::MouseButton _button )
{
	//// Check if we are tracking -> user selected fragment to be appended
	//if(TTTManager::getInst().isTracking() && _button & Qt::LeftButton) {
	//	// Get pointer to TTTAutoTrackingTreeWindow
	//	TTTAutoTrackingTreeWindow* autoTrackingTreeWindow = frmAutoTracking->getTreeWindow();
	//	if(!autoTrackingTreeWindow)
	//		return;

	//	// Stop tracking
	//	positionManager->frmMovie->switchTrackingButtons(false);
	//	multiPicViewer->stopTracking();
	//	TTTManager::getInst().setTrackStopReason (TS_NONE);
	//	TTTManager::getInst().stopTracking(false);

	//	// Notify autotracking
	//	autoTrackingTreeWindow->notifyManualTrackingFragmentSelected(_fragment);
	//}
	//else {

	// Get tree or TreeFragment if available
	QSharedPointer<Tree> tree = _circle->getTree();
	QSharedPointer<TreeFragment> treeFragment = _circle->getTreeFragmentSharedPtr();

	// Get action
	AutoTrackingCellCircle::Action action = _circle->getAction();

	// Check which button was used
	if(_button & Qt::LeftButton) {
		// Left mouse button was used
		if(action == AutoTrackingCellCircle::A_CREATE_TREE_OR_INVALIDATE && treeFragment) {
			// circle represents TreeFragment and a tree should be created from it
			createTreeFromFragment(treeFragment);
		}
		else if(action == AutoTrackingCellCircle::A_SELECT_SUCCESSOR) {
			// Make sure we have tree window
			if(!frmTreeWindow)
				return;

			// Check if manual tracking mode is active
			if(TTTManager::getInst().isTracking()) {
				// Notify autotracking
				frmTreeWindow->treeOrTreeFragmentToAppendSelectedDuringManualTracking(tree, treeFragment, _circle->getTrackPoint());
			}
			else {
				// Manual tracking is not active
				frmTreeWindow->treeOrTreeFragmentToAppendSelected(tree, treeFragment, _circle->getTrackPoint());
			}
		}
	}
	else if(_button & Qt::RightButton) {
		// Right mouse button was used
		if(action == AutoTrackingCellCircle::A_CREATE_TREE_OR_INVALIDATE && treeFragment) {
			// User marked fragment as invalid
			treeFragment->markAsInvalid();
			treeFragment->writeAssociatedTreeToFile();

			// Make circle invisible (ToDo: better call PictureView::drawTracks()?)
			_circle->setVisible(false);
		}
	}
}

AutoTrackingCellCircle* TTTAutoTracking::createCellCircleFromTreeFragmentTrackPoint( const TreeFragmentTrackPoint& _trackPoint, TTTPositionManager* _tttpm, int _wlIndex, AutoTrackingCellCircle::Action _action, QString _tooltipText, QColor* _color /*= 0*/, bool _noText ) const
{
	// Calc local pixel coordinates
	QPointF trackGlobalCoords = QPointF(_trackPoint.posX, _trackPoint.posY);
	QPointF trackPictureCoords = _tttpm->getDisplays().at (_wlIndex).calcTransformedCoords (trackGlobalCoords, &_tttpm->positionInformation);

	// Get track object
	TreeFragmentTrack* track = _trackPoint.getTrack();
	if(!track || !track->getTree() || track->getTree()->isInvalid())
		return 0;

	// Description
	QString textString; 
	if(track && !_noText)
		textString = QString("%1 - %2").arg(track->getTree()->getTreeFragmentNumber()).arg(track->getTrackNumber());

	// Color
	QColor color;
	if(_color)
		color = *_color;
	else {
		if(track->getTree()->isUsedForTTTTree())
			color = USED_TREES_COLOR;
		else
			color = TreeView::mapConfidenceOnColor(_trackPoint.getConfidenceLevel());
	}

	// If in manual tracking mode, do not change cursor (user is probably using 'circle mouse pointer')
	bool noOwnCursor = TTTManager::getInst().isTracking();

	// Create circle
	AutoTrackingCellCircle* newCircle = new AutoTrackingCellCircle(_action, 
		&_trackPoint,
		textString, 
		trackPictureCoords, 
		CELLCIRCLESIZE / 2.0f, 
		color, 
		track->getTreeWeakPointer(), 
		QSharedPointer<Tree>(), 
		color,
		_tooltipText,
		noOwnCursor);	

	// Connect
	connect(newCircle, SIGNAL(cellClicked(AutoTrackingCellCircle*, Qt::MouseButton)), this, SLOT(cellCircleClicked(AutoTrackingCellCircle*, Qt::MouseButton)));

	return newCircle;
}

void TTTAutoTracking::selectOverlappingToggled( bool on )
{
	// Store setting
	QSettings* userSettings = UserInfo::getInst().getUserSettings();
	if(userSettings)
		userSettings->setValue(UserInfo::KEY_TTTAUTOTRACKING_SELECTCONNECTED, on);
}

void TTTAutoTracking::listTrfFiles( const QString& _pathName, QStringList& _fileList, const QString& _prefix /*= ""*/ )
{
	// Check dir
	if(_pathName.isEmpty() || !QDir(_pathName).exists())
		return;

	// List trf, csv files
	QStringList fileListTmp = FastDirectoryListing::listFiles(_pathName, QStringList() << ".trf" << ".csv");

	// Prepend prefix
	if(!_prefix.isEmpty()) {
		for(int i = 0; i < fileListTmp.size(); ++i) {
			fileListTmp[i] = _prefix + fileListTmp[i];
		}
	}

	// Append to _fileList
	_fileList.append(fileListTmp);
}

void TTTAutoTracking::positionDoubleClicked( const QString& index )
{
	// First select the position
	TTTPositionLayout* tttpl = TTTManager::getInst().frmPositionLayout;
	if(!tttpl || !tttpl->setPosition(index)) {
		qWarning() << "TTTAutoTracking::positionDoubleClicked() Error 1 - " << index;
		return;
	}

	// Get movie window
	TTTPositionManager* tttpm = TTTManager::getInst().getPositionManager (index);
	if (!tttpm)
		return;

	// Show movie window
	tttpm->frmMovie->show();
	tttpm->frmMovie->raise();
	if(tttpm->frmMovie->isMinimized())
		tttpm->frmMovie->showMaximized();
}

void TTTAutoTracking::importAutoTrackingResults()
{
	try {
		// Delegate to function for specific auto tracking method
		switch(ui.cmbImportMethod->currentIndex()) {
		case 0:
			importAutoTrackingResultsTrackMate();
			break;
		case  1:
			importAutoTrackingResultsCellProfiler();
			break;
		default:
			;
		}
	}
	catch(const TTTException& _e) {
		// Error
		QMessageBox::critical(this, "Error", "An error occurred during import:\n\n" + _e.what());
	}

	// Display results
	updateDisplay();

	// Display in movie windows
	TTTManager::getInst().redrawTracks();

	// Emit signal
	emit loadedTreeFragmentsChanged();

}

void TTTAutoTracking::importAutoTrackingResultsTrackMate()
{
	// Get position
	int pos = ui.cmbImportPosition->currentText().toInt();
	TTTPositionManager* tttpm = TTTManager::getInst().getPositionManager(pos);
	if(!tttpm) {
		QMessageBox::information(this, "Error", QString("Please select a valid position first."));
		return;
	}

	// Get xml file name
	QString defaultLocation = TTTManager::getInst().frmPositionLayout->getExperimentDir().absolutePath();
	QString fileName = QFileDialog::getOpenFileName(this, "Select TrackMate XML File", defaultLocation, "XML Files (*.xml)");
	if(fileName.isEmpty())
		return;

	ChangeCursorObject cc;

	// Parse xml file
	QFile file(fileName);
	if (!file.open(QIODevice::ReadOnly))
		throw TTTException(QString("Cannot open file: ") + file.errorString());
	QDomDocument doc("importTracks");
	if (!doc.setContent(&file))
		throw TTTException(QString("Cannot parse xml file: doc.setContent() failed."));

	// Convert data to TreeFragments
	QDomElement docElem = doc.documentElement();
	if(docElem.tagName() != "Tracks")
		throw TTTException(QString("Cannot parse xml file: unexpected DOM element '%1'.").arg(docElem.tagName()));
	
	QDomNode n = docElem.firstChild();
	int treeCount = 0;
	while(!n.isNull()) {
		QDomElement currentTrack = n.toElement(); // try to convert the node to an element.
		if(!currentTrack.isNull()) {
			if(currentTrack.tagName() == "particle") {
				// Add new tree
				int treeNumber = pos * 10000 + ++treeCount;
				QString pseudoFileName;
				if(fileName.lastIndexOf('.') >= 0)
					pseudoFileName = fileName.left(fileName.lastIndexOf('.'));
				else
					pseudoFileName = fileName;
				pseudoFileName += QString("_%1.tree").arg(treeNumber);
				QSharedPointer<TreeFragment> nextTree(new TreeFragment(pseudoFileName, treeNumber, pos));
				
				// Get track points
				int trackNumber = 1; // TrackMate XML file currently only supports one track per tree
				QHash<int, QSharedPointer<TreeFragmentTrackPoint>> trackPoints;
				QDomNode childNode = currentTrack.firstChild();
				int startTp = -1, 
					stopTp = -1;
				while(!childNode.isNull()) {
					QDomElement curTrackPoint = childNode.toElement();
					if(!curTrackPoint.isNull() && curTrackPoint.hasAttribute("t") && curTrackPoint.hasAttribute("x") && curTrackPoint.hasAttribute("y")) {
						// Basic sanity check: time point is ok?
						bool ok;
						int timePoint = curTrackPoint.attribute("t").toInt(&ok) + ui.spbImportTimeOffset->value();
						if(ok && timePoint > 0) {
							// Found valid track point
							if(startTp == -1 || startTp > timePoint)
								startTp = timePoint;
							if(stopTp == -1 || timePoint > stopTp)
								stopTp = timePoint;

							// Map local to global coordinates
							QPointF localCoordinates(curTrackPoint.attribute("x").toFloat(), curTrackPoint.attribute("y").toFloat());
							QPointF globalCoordinates = tttpm->getDisplays().at (0).calcAbsCoords (localCoordinates, &tttpm->positionInformation);

							// Create track point
							QSharedPointer<TreeFragmentTrackPoint>& trackPoint = trackPoints[timePoint];
							if(!trackPoint.isNull())
								throw TTTException(QString("File %1 has trackpoint %2 in track %3 more than once.").arg(fileName).arg(timePoint).arg(trackNumber));
							trackPoint = QSharedPointer<TreeFragmentTrackPoint>(new TreeFragmentTrackPoint(timePoint, pos, globalCoordinates.x(), globalCoordinates.y(), -1.0f));

							// Add track point to trackpointsHashmap
							if(!trackpointsHashmap.contains(timePoint)) 
								trackpointsHashmap.insert(timePoint, QList<QSharedPointer<TreeFragmentTrackPoint> >());
							trackpointsHashmap[timePoint].append(trackPoint);
						}
					}

					childNode = curTrackPoint.nextSibling();
				}

				// Create track with track points and, if successful, add it to the trees
				if(nextTree->insertTrack(trackNumber, startTp, stopTp, std::move(trackPoints))) {

					// Finish tree creation
					nextTree->setWeakPointer(QWeakPointer<TreeFragment>(nextTree));

					// Add tree to hashmap
					if(!trees.contains(pos))
						trees.insert(pos, QList<QSharedPointer<TreeFragment> >());
					trees[pos].append(nextTree);
				}
			}
		}
		n = currentTrack.nextSibling();
	}

	// Done
	cc.restoreCursor();
	QMessageBox::information(this, "Import completed", QString("Successfully imported %1 trees.").arg(treeCount));
}

void TTTAutoTracking::importAutoTrackingResultsCellProfiler()
{
	// Get position
	int pos = ui.cmbImportPosition->currentText().toInt();
	TTTPositionManager* tttpm = TTTManager::getInst().getPositionManager(pos);
	if(!tttpm) {
		QMessageBox::information(this, "Error", QString("Please select a valid position first."));
		return;
	}

	// Get csv file name
	QString defaultLocation = TTTManager::getInst().frmPositionLayout->getExperimentDir().absolutePath();
	QString fileName = QFileDialog::getOpenFileName(this, "Select CellProfiler CSV File", defaultLocation, "CSV Files (*.csv)");
	if(fileName.isEmpty())
		return;

	ChangeCursorObject cc;

	// Parse csv file
	QFile file(fileName);
	if (!file.open(QIODevice::ReadOnly))
		throw TTTException(QString("Cannot open file: ") + file.errorString());
	QString csvData = file.readAll();
	if(csvData.isEmpty())
		throw TTTException(QString("The selected file appears to be empty."));
	QVector<QVector<double>> rows;
	QVector<QString> columnNames;
	QVector<QString> columnsToFind;
	QVector<int> columnsToFindIndexes;
	columnsToFind.push_back("ImageNumber");
	columnsToFind.push_back("ObjectNumber");
	columnsToFind.push_back("TrackObjects_ParentImageNumber"); 
	columnsToFind.push_back("TrackObjects_ParentObjectNumber");
	columnsToFind.push_back("Location_Center_X");
	columnsToFind.push_back("Location_Center_Y");
	int err = Tools::parseCsvDataDouble(csvData, ",", rows, &columnNames, &columnsToFind, &columnsToFindIndexes, true);
	if(err)
		throw TTTException(QString("Error while parsing file (error code: %1)").arg(err));

	// Make sure columns were found
	for(int i = 0; i < columnsToFindIndexes.size(); ++i) {
		if(columnsToFindIndexes[i] < 0)		
			throw TTTException(QString("Error while reading file: missing mandatory column '%1'").arg(columnsToFind[i]));
	}
	int colTimePoint = columnsToFindIndexes[0];
	int colObjectId = columnsToFindIndexes[1];
	int colPreviousTimePoint = columnsToFindIndexes[2];
	int colPreviousObjectId = columnsToFindIndexes[3];
	int colX = columnsToFindIndexes[4];
	int colY = columnsToFindIndexes[5];

	// Statistics
	int treeCount = 0;
	int numberOfSplitsInMoreThanTwo = 0;
	int numberOfExcessiveSplitEvents = 0;
	int numberInsertTrackPointErrors = 0;

	// Convert list of track points by image to trees (ignoring merge events, interpreting splitting events as divisions,
	// ignoring splits in more than two fragments)
	QHash<QPair<int, int>, TreeFragmentTrackPoint*> trackPointsByTimePointAndObjectId;	// Helper to find parent track points
	for(auto itRow = rows.constBegin(); itRow != rows.constEnd(); ++itRow) {
		const QVector<double>& curRow = *itRow;
		int tp = curRow[colTimePoint] + ui.spbImportTimeOffset->value();
		int objectId = curRow[colObjectId];
		if(tp <= 0 || trackPointsByTimePointAndObjectId.contains(QPair<int,int>(tp, objectId)))
			continue;

		// Convert coordinates and 
		QPointF localCoordinates(curRow[colX], curRow[colY]);
		QPointF globalCoordinates = tttpm->getDisplays().at (0).calcAbsCoords (localCoordinates, &tttpm->positionInformation);

		// Create track point
		QSharedPointer<TreeFragmentTrackPoint> trackPoint = QSharedPointer<TreeFragmentTrackPoint>(new TreeFragmentTrackPoint(tp, pos, globalCoordinates.x(), globalCoordinates.y(), -1.0f));
		
		// See if a track point exists to which current track point should be appended (directly or as daughter cell) - prevTrackPoint
		// will be 0 if there is no parent track point or the parent track point has already 2 successor track points
		TreeFragmentTrackPoint* prevTrackPoint = 0;
		TreeFragmentTrack* prevTrack = 0;
		TreeFragmentTrack* trackToInsertTrackPoint = 0;
		int prevTimePoint = curRow[colPreviousTimePoint] + ui.spbImportTimeOffset->value();
		int prevObjectId = curRow[colPreviousObjectId];
		prevTrackPoint = trackPointsByTimePointAndObjectId.value(QPair<int,int>(prevTimePoint, prevObjectId));
		if(prevTimePoint < tp && prevTrackPoint && prevTrackPoint->getTrack()) {
			// There is a previous track point -> see if it has a division already (then a new tree has to be started), if 
			// it has track points already at current time point (then insert a division) or none of the above (then just append
			// new track point)

			prevTrack = prevTrackPoint->getTrack();
			if(prevTrack->getChild1() || prevTrack->getChild2() ) {	
				// Track already has two daughter cells (i.e. split in > 2 cells by CellProfiler), so start a new tree
				prevTrackPoint = 0;
				prevTrack = 0;
				++numberOfSplitsInMoreThanTwo;
			}
			else if(prevTrack->getLastTimePoint() > prevTrackPoint->timePoint) {
				// prevTrack already has track points after prevTrackPoint -> insert cell division, moving existing track points to 
				// new sibling cell (unless there are too many divisions already)
				if(prevTrack->getTrackNumber() >= 1024) {
				//if(prevTrack->getTrackNumber() >= 1) {
					// Allow at most 10 generations, as excessive "split events" can cause usability problems
					prevTrackPoint = 0;
					prevTrack = 0;
					++numberOfExcessiveSplitEvents;
				}
				else {		
					TreeFragment* prevTrackPointTree = prevTrack->getTree();
					assert(prevTrackPointTree);

					// Remove track points that will go to sibling track
					QHash<int, QSharedPointer<TreeFragmentTrackPoint>> trackPointContainer;
					int siblingStartTp = -1;
					int siblingStopTp = -1;
					for(int tpOfTrackPointToMove = prevTrackPoint->timePoint + 1; tpOfTrackPointToMove <= prevTrack->getLastTimePoint(); ++tpOfTrackPointToMove) {
						QSharedPointer<TreeFragmentTrackPoint> trackPointToMove = prevTrack->getTrackPointByTimePointSharedPtr(tpOfTrackPointToMove);
						if(trackPointToMove) {
							trackPointContainer[tpOfTrackPointToMove] = trackPointToMove;
							prevTrack->deleteTrackPointByTimePoint(tpOfTrackPointToMove);
							if(siblingStartTp == -1 || siblingStartTp > tpOfTrackPointToMove)
								siblingStartTp = tpOfTrackPointToMove;
							if(siblingStopTp == -1 || siblingStopTp < tpOfTrackPointToMove)
								siblingStopTp = tpOfTrackPointToMove;
						}
					}

					// Create sibling track 
					TreeFragmentTrack* siblingTrack = prevTrackPointTree->insertTrack(prevTrack->getTrackNumber()*2, siblingStartTp, siblingStopTp, std::move(trackPointContainer));
					if(!siblingTrack)
						throw TTTException(QString("Could not create sibling track for object %d at time point %d").arg(objectId).arg(trackPoint->timePoint));					

					// Create track for this track point
					trackPointContainer.clear();
					//trackPointContainer[trackPoint->timePoint] = trackPoint;	// Track point will be inserted later (a
					trackToInsertTrackPoint = prevTrackPointTree->insertTrack(prevTrack->getTrackNumber()*2 + 1, trackPoint->timePoint, trackPoint->timePoint, std::move(trackPointContainer));
					if(!trackToInsertTrackPoint)
						throw TTTException(QString("Could not create track for object %d at time point %d").arg(objectId).arg(trackPoint->timePoint));
				}
			}
			else {
				// prevTrack does not split at all (so far) -> can simply insert new track point
				trackToInsertTrackPoint = prevTrack;
			}
		}

		// If we have no trackToInsertTrackPoint, we have to create a new tree
		if(!trackToInsertTrackPoint) {
			int treeNumber = pos * 10000 + ++treeCount;
			QString pseudoFileName;
			if(fileName.lastIndexOf('.') >= 0)
				pseudoFileName = fileName.left(fileName.lastIndexOf('.'));
			else
				pseudoFileName = fileName;
			pseudoFileName += QString("_%1.tree").arg(treeNumber);
			QSharedPointer<TreeFragment> nextTree(new TreeFragment(pseudoFileName, treeNumber, pos));

			// Create root track 
			QHash<int, QSharedPointer<TreeFragmentTrackPoint>> empty;
			trackToInsertTrackPoint = nextTree->insertTrack(1, tp, tp, std::move(empty));
			if(!trackToInsertTrackPoint)
				throw TTTException(QString("Could not create tree for object %d at time point %d").arg(objectId).arg(tp));

			// Add tree to hashmap
			if(!trees.contains(pos))
				trees.insert(pos, QList<QSharedPointer<TreeFragment> >());
			trees[pos].append(nextTree);
		}

		// Insert track point into track
		assert(trackToInsertTrackPoint);
		if(trackToInsertTrackPoint->insertTrackPoint(trackPoint)) {
			// Trackpoint was inserted successfully, so add it to local index hash 
			trackPointsByTimePointAndObjectId.insert(QPair<int,int>(tp, objectId), trackPoint.data());

			// And to trackpointsHashmap
			if(!trackpointsHashmap.contains(trackPoint->timePoint)) 
				trackpointsHashmap.insert(trackPoint->timePoint, QList<QSharedPointer<TreeFragmentTrackPoint> >());
			trackpointsHashmap[trackPoint->timePoint].append(trackPoint);
		}
		else
			++numberInsertTrackPointErrors;
	}

	// Finish tree creation
	const QList<QSharedPointer<TreeFragment>>& createdTrees = trees[pos];
	for(int i = 0; i < createdTrees.size(); ++i)
		createdTrees[i]->setWeakPointer(QWeakPointer<TreeFragment>(createdTrees[i]));

	// Done
	cc.restoreCursor();
	QMessageBox::information(this, "Import completed", QString("Successfully imported %1 trees.").arg(treeCount));
	qDebug() << "Imported " << treeCount << " CellProfiler trees - excessive splits: " << numberOfExcessiveSplitEvents << ", splits in more than two cells: " << numberOfSplitsInMoreThanTwo << ", failedTrackPointInserts: " << numberInsertTrackPointErrors;
}

void TTTAutoTracking::selectedMethodChanged(int newIndex)
{
	// Update default value of temporal offset (but only if value was not changed by user before to something else than the default value)
	switch(ui.cmbImportMethod->currentIndex()) {
	case 0:
		// TrackMate
		if(ui.spbImportTimeOffset->value() == 0)
			ui.spbImportTimeOffset->setValue(1);
		break;
	case  1:
		// CellProfiler
		if(ui.spbImportTimeOffset->value() == 1)
			ui.spbImportTimeOffset->setValue(0);
		break;
	default:
		;
	}
}

//void AutoTrackingFragmentListModel::setCurrentPosition( const QString& _index )
//{
//	// Check if different
//	if(_index == curPosition)
//		return;
//
//	// Update
//	curPosition = _index;
