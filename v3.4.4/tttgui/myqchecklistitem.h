/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MYQCHECKLISTITEM_H
#define MYQCHECKLISTITEM_H

#include <q3listview.h>
#include <qpalette.h>
#include <qpainter.h>
#include <qcolor.h>

/**
@author Bernhard
	
	This class enables colored check list items, derived from QCheckListItem.
*/


class MyQCheckListItem : public Q3CheckListItem
{

public:
	/**
	 * constructs a colored QCheckListItem
	 * @param _parent the parent QListView of the item (cf. QCheckListItem)
	 * @param _text the text of the item (cf. QCheckListItem)
	 * @param _tt the type of the item (cf. QCheckListItem)
	 * @param _color the text color of the item
	 */
	MyQCheckListItem (Q3ListView *_parent, const QString &_text, Type _tt = RadioButtonController, QColor _color = Qt::black)
		: Q3CheckListItem (_parent, _text, _tt)
	{
		color = _color;
	}
	
	~MyQCheckListItem();
	
	/**
	 * draws this item into a QListView, overloaded from QCheckListItem
	 * @param _p the current painter
	 * @param _cg the current color group
	 * @param _column the current column
	 * @param _width the current width
	 * @param _align the current alignment
	 */
	void paintCell (QPainter *_p, const QColorGroup &_cg, int _column, int _width, int _align);

private:
	
	///holds the desired text color
	QColor color;
};

#endif
