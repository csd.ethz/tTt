/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "callbackitem.h"

CallbackItem::CallbackItem (const QString &_parameter)
{
        callBackMP = 0;
        callBackMR = 0;

        parameter = _parameter;
}

void CallbackItem::callCBMP (void *_x, QMouseEvent *_ev)
{
        if (callBackMP)
                (*callBackMP) (parameter, _x, _ev);
}

void CallbackItem::callCBMR (void *_x, QMouseEvent *_ev)
{
        if (callBackMR)
                (*callBackMR) (parameter, _x, _ev);
}

void CallbackItem::setMousePressCallBackMethod (void (*_callBackMP) (const QString &, void *, QMouseEvent *))
{
        callBackMP = _callBackMP;
}

void CallbackItem::setMouseReleaseCallBackMethod (void (*_callBackMR) (const QString &, void *, QMouseEvent *))
{
        callBackMR = _callBackMR;
}

