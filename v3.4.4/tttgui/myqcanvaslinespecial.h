/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MYQCANVASLINESPECIAL_H
#define MYQCANVASLINESPECIAL_H

#include "myqcanvasline.h"

/**
	@author Bernhard
	
	This class is a MyQCanvasLine with the additional feature to have stamp
	 points of different width.
	Used for the additional tracking attributes.
*/

class MyQCanvasLineSpecial : public MyQCanvasLine
{
public:
	
	/**
	 * constructs a line on a canvas and no coordinates
	 * @param _canvas the canvas this line is associated to
	 * @param _stamped whether the line should be stamped (rather single points on/off than a complete line)
	 * @param _usePC use x() and y() functions in drawShape() (cannot be combined with _stamped)
	 */
	MyQCanvasLineSpecial (Q3Canvas* _canvas, bool _stamped = false, bool _usePC = false)
	: MyQCanvasLine (_canvas, _stamped, _usePC)
		{setZ (1);}
	
	~MyQCanvasLineSpecial()
		{}
	
	/**
	 * adds a middle point with a width (if in stamped mode, otherwise useless)
	 * @param _x the x coordinate of the middle point (somewhere in the direction of the line)
	 * @param _y the y coordinate of the middle point (somewhere in the direction of the line)
	 * @param _width the width of the point (which is in fact a line)
	 */
	void addMiddlePoint (int _x, int _y, int _width);
	
	/**
	 * adds a colored middle point with a width (if in stamped mode, otherwise useless)
	 * @param _x the x coordinate of the middle point (somewhere in the direction of the line)
	 * @param _y the y coordinate of the middle point (somewhere in the direction of the line)
	 * @param _width the width of the point (which is in fact a line)
	 * @param _color the color of the middle point (each point can have a different color)
	 */
	void addMiddlePoint (int _x, int _y, int _width, QColor _color);
	
	
protected:
	
	void drawShape (QPainter &);

private:
	
	///the widths of the stamped points (filled in addMiddlePoint())
	Q3ValueVector<int> widths;
};

#endif
