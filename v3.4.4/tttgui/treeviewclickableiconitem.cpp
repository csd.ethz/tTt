/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "treeviewclickableiconitem.h"

// Project
#include "treeview.h"
#include "treeviewtreeitem.h"


TreeViewClickableIconItem::TreeViewClickableIconItem( TreeViewTreeItem* _treeViewTreeItem, QPixmap _image, BUTTON_TYPE _type )
	: QGraphicsPixmapItem(_image, _treeViewTreeItem)
{
	// Init variables
	treeViewTreeItem = _treeViewTreeItem;
	type = _type;

	// Make icon appear in half of normal size when zoom is set to default
	setScale(0.5f / TreeView::DEFAULT_SCALING);

	// Set cursor to hand
	setCursor(QCursor(Qt::PointingHandCursor));
}

void TreeViewClickableIconItem::mousePressEvent( QGraphicsSceneMouseEvent *_event )
{

}

void TreeViewClickableIconItem::mouseReleaseEvent( QGraphicsSceneMouseEvent* _event )
{
	// Just notify whoever
	if(type == CLOSE_BUTTON)
		treeViewTreeItem->closeClicked();
	else if(type == SAVE_BUTTON)
		treeViewTreeItem->saveClicked();
}
