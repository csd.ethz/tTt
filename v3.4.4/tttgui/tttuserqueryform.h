/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TTTUSERQUERYFORM_H
#define TTTUSERQUERYFORM_H

#ifndef TREEANALYSIS
#include "ui_frmUsersign.h"
#else
#include "ui_frmStaTTTsUsersign.h"
#endif
#include "tttdata/userinfo.h"
#include "tttbackend/tools.h"

#include <QDialog>
#include <qapplication.h>
#include <qcombobox.h>
#include <qpushbutton.h>
//Added by qt3to4:
#include <QCloseEvent>

class TTTUserQueryForm: public QDialog, public Ui::frmUsersign {
	Q_OBJECT

public:

        TTTUserQueryForm (UserInfo *_userInfo, const QString &_path, QWidget *parent = 0);
	
	void closeEvent (QCloseEvent *_ev);
	
private:
	
	///a reference to the static user info class, necessary for setting the user sign
	UserInfo *userInfo;

	///path to users file
	QString path;
	
	
//private methods
	
	/**
	 * loads all available user signs from a text file (see details in implementation)
	 * @param _path the path with the user names file
	 */
	void loadUsersigns ();

private slots:

	/**
	 * Get user sign to add and save in users file
	 */
	void addUser();

	/**
	 * Remove selected user and save users file
	 */
	void removeUser();
};

#endif
