/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "tttpositionlayout.h"

// Project includes
#include "positiondisplay.h"
#include "tttbackend/tttmanager.h"
#include "tttbackend/positionlayoutpicturelistmodel.h"
#include "tttgui/tttpositiondetails.h"
#include "tttbackend/compileflags.h"
#include "ttttracking.h"
#include "statusbar.h"
#include "tttdata/systeminfo.h"
#include "tttdata/userinfo.h"
#include "tttversionInfo.h"
#include "tttsplashscreen.h"
#include "tttio/fastdirectorylisting.h"
#include "tttregionselection.h"
#include "tttmovie.h"
#include "tttbackend/picturearray.h"
#include "tttbackend/tools.h"
//#include "tttpositionstatistics.h"
#include "tttpositionxmldisplay.h"
#include "tttloadpicsfrommorepositions.h"
#include "ttttreemerging.h"
#include "tttbackend/changecursorobject.h"
#include "tttgui/ttttreestyleeditor.h"
#include "tttgui/tttlogfileconverter.h"
#include "tttpositionlayoutmovieexport.h"
//#include "tttbackgroundcorrection.h"
#include "tttgui/tttexportfluorescencepatches.h"

// Qt includes
#include <QSignalMapper>
#include <QProgressDialog>


//// For Debugging only
//#ifdef DEBUGMODE
//#include <Windows.h>
//#endif




TTTPositionLayout::TTTPositionLayout(QWidget *parent, const char *name)
    :QWidget(parent, name)
{
    setupUi (this);

	// Init variables
	movieExporter = 0;
	windowLayoutLoaded = false;
	currentBasename = "";
	maxMemImages = 0;
	//frmBackgroundCorrection = 0;
	frmExportImagePatches = 0;

	//// Hide stats button
	//pbtStatistics->hide();

	//cuurentPositionInitialized = false;

	//positionView = new PositionDisplay (fraLayout);
	
	pbtLoadImages->setEnabled (false);

	// Listview handler
	pictureListModel = new PositionLayoutPictureListModel(lvwImages);

	// Disable editing of list
	lvwImages->setEditTriggers(QAbstractItemView::NoEditTriggers);
	
	//global widgets
	connect ( pbtClose, SIGNAL (clicked()), this, SLOT (close())),
	connect ( pbtLoadImages, SIGNAL (clicked()), this, SLOT (loadImages()));
	connect ( pbtLoadTTTFile, SIGNAL (clicked()), this, SLOT (loadTTTFile()));
	//connect( pbtBackgroundCorrection, SIGNAL(clicked()), this, SLOT(showBackgroundCorrectionWindow()));
	//connect ( pbtNewColony, SIGNAL (clicked()), this, SLOT (addNewColony()));
	//connect ( pbtConvertIncorrectCoordinates, SIGNAL (clicked()), this, SLOT (convertIncorrectCoordinates()));
	connect ( pbtPosLayoutMovieExport, SIGNAL (clicked()), this, SLOT (posLayoutMovieExport()));
	connect ( pbtExportAllTTTfiles, SIGNAL (clicked()), this, SLOT (exportAllTTTFiles()));
	connect ( pbtCreateSnapshotsOfAllTrees, SIGNAL (clicked()), this, SLOT (createSnapshotsOfAllTrees()));
	connect ( pbtUpdateThumbNails, SIGNAL (clicked()), this, SLOT (updateThumbNails()));
	connect ( pbtExport, SIGNAL(clicked()), this, SLOT(exportPosLayout()));
	
	//--------------oliver-------------
        //Convert trees button (currently only for Dirk)
        //connect ( pbtConvertTrees, SIGNAL (clicked()), this, SLOT (convertDirksTrees()));
        
	connect ( pbtLoadPicsFromMultiplePositions, SIGNAL (clicked()), this, SLOT (loadPicsFromMultiplePositions()));
	connect ( pbtUnloadAllPictures, SIGNAL (clicked()), this, SLOT (unloadAllPictures()));
	//connect ( pbtStatistics, SIGNAL (clicked()), this, SLOT (showStatisticsWindow()));
	connect ( pbtPositionXML, SIGNAL (clicked()), this, SLOT (showPositionXMLDisplay()));
	connect ( pbtCustomizeTTT, SIGNAL (clicked()), this, SLOT (showStyleEditor()));
	connect ( pbtRefreshLayout, SIGNAL (clicked()), this, SLOT (refreshLayout()));
	connect ( pbtMergeTrees, SIGNAL (clicked()), this, SLOT (mergeTrees()));
	connect ( pbtLogFileConverter, SIGNAL (clicked()), this, SLOT (showLogFileConverterWindow()));
	
	//display options
	connect ( cboOcularFactor, SIGNAL (activated (const QString&)), this, SLOT (setOcularFactor (const QString&)));

    QSignalMapper *tvSignalMapper = new QSignalMapper (this);

	connect ( optTVFactor040, SIGNAL (clicked()), tvSignalMapper, SLOT (map()));
	tvSignalMapper->setMapping (optTVFactor040, "040");
	connect ( optTVFactor050, SIGNAL (clicked()), tvSignalMapper, SLOT (map()));
	tvSignalMapper->setMapping (optTVFactor050, "050");
    connect ( optTVFactor063, SIGNAL (clicked()), tvSignalMapper, SLOT (map()));
	tvSignalMapper->setMapping (optTVFactor063, "063");
	connect ( optTVFactor070, SIGNAL (clicked()), tvSignalMapper, SLOT (map()));
	tvSignalMapper->setMapping (optTVFactor070, "070");
    connect ( optTVFactor1, SIGNAL (clicked()), tvSignalMapper, SLOT (map()));
    tvSignalMapper->setMapping (optTVFactor1, "1");
    connect ( tvSignalMapper, SIGNAL (mapped (const QString &)), this, SLOT (setTVAdapterFactor (const QString &)));


    connect ( chkInvertedCoordinateSystem, SIGNAL (toggled (bool)), this, SLOT (setInvertedCoordinateSystem (bool)));
	connect ( chkDrawFrame, SIGNAL (toggled (bool)), this, SLOT (toggleDrawThumbnailFrame (bool)));
	connect ( chkDrawText, SIGNAL (toggled (bool)), this, SLOT (toggleDrawThumbnailText (bool)));
	
	connect ( positionView, SIGNAL (positionLeftClicked (const QString&)), this, SLOT (setPosition (const QString&)));
	connect ( positionView, SIGNAL (positionDoubleClicked (const QString&)), this, SLOT (showMovieWindow (const QString&)));
	connect ( positionView, SIGNAL (positionRightClicked (const QString&)), this, SLOT (showPositionInformation (const QString&)));
	connect ( positionView, SIGNAL (zoomChanged (float)), this, SLOT (zoomChanged (float)));
	
	//picture selection
	connect ( pbtSelectAll, SIGNAL (clicked()), this, SLOT (selectionHandler()));
	connect ( pbtSelectFirst, SIGNAL (clicked()), this, SLOT (selectionHandler()));
	connect ( pbtSelectLast, SIGNAL (clicked()), this, SLOT (selectionHandler()));
	connect ( pbtSelectNone, SIGNAL (clicked()), this, SLOT (selectionHandler()));
	connect ( pbtSelectInterval, SIGNAL (clicked()), this, SLOT (selectionHandler()));
	connect ( pbtSelectMemLast, SIGNAL (clicked()), this, SLOT (selectionHandler()));

	//connect ( lvwImages, SIGNAL (spacePressed (Q3ListViewItem *)), this, SLOT (displayPictureInformation (Q3ListViewItem *))); //countSelected(QListViewItem*)));
	//connect ( lvwImages, SIGNAL (clicked (Q3ListViewItem *)), this, SLOT (displayPictureInformation (Q3ListViewItem *))); //countSelected(QListViewItem*)));
	connect ( pictureListModel, SIGNAL (itemSelectionChanged()), this, SLOT(displayInfoAboutSelectedPics()));

	connect ( pbtSelectRegion, SIGNAL (clicked()), this, SLOT (openRegionSelection()));
	
	
	//wavelength selection
	connect ( pbtSelect0ForEachX, SIGNAL (clicked()), this, SLOT (select0ForEachX()));

	// Settings
	if (TTTManager::getInst().frmTreeStyleEditor) {
		connect ( TTTManager::getInst().frmTreeStyleEditor, SIGNAL (styleChosen (StyleSheet &)), this, SLOT (styleChosen (StyleSheet &)));
	}
	
	
	//other
	//connect ( TTTManager::getInst().frmPositionDetails, SIGNAL (positionStatisticClosed (PositionThumbnail *)), this, SLOT (updateDisplayedPicture (PositionThumbnail *)));
	
	

	
	cboOcularFactor->insertItem ("! NOT SET !");
	cboOcularFactor->insertItem ("4x");
	cboOcularFactor->insertItem ("5x");
	cboOcularFactor->insertItem ("10x");
	cboOcularFactor->insertItem ("20x");
	cboOcularFactor->insertItem ("40x");
	cboOcularFactor->insertItem ("63x");
	cboOcularFactor->insertItem ("100x");
	
	//note: if a tat file was read, the tv factor and ocular factor are usually set (in experiments after 2008/04/18)
	cboOcularFactor->setCurrentItem (0);
	optTVFactor1->setChecked (true);
	
	chkInvertedCoordinateSystem->setChecked (true);
	TTTManager::getInst().setInvertedCoordinateSystem (chkInvertedCoordinateSystem->isChecked());
	
	//currentThumbnail = 0;
	
	wavelengthsHidden = 0;
	
	versionWindow = 0;

	wavelengthButtonGroups.clear();

	
	//apply compile flags
	
	//if (! COMPFLAG_SIMPLE_STATISTICS)
	//	pbtStatistics->hide();
	
	// Hide "Select positions" button
	pbtSelectRegion->hide();
}

TTTPositionLayout::~TTTPositionLayout()
{
	if(movieExporter) {
		movieExporter->close();
		delete movieExporter;
		movieExporter = 0;
	}
	//if(frmBackgroundCorrection) {
	//	frmBackgroundCorrection->close();
	//	delete frmBackgroundCorrection;
	//}
}


////SLOT:
//void TTTPositionLayout::setPosition (const QString &_index, const QString &_lastIndex)
//{
//	
//	if (_index == currentPosition)
//		return;
//	
//	// Get new pos manager
//	TTTPositionManager *tttpm = TTTManager::getInst().getPositionManager (_index);
//	if (tttpm) {
//		// Check if position is locked
//		if (tttpm->isLocked()) {
//            QMessageBox::information (this, "Position not available", "This position is locked (due to problems in the logfile). You cannot access it now, please redo the log file conversion", "Yes, I agree...");
//			
//			// Undo selection
//			positionView->selectPosition(_lastIndex, true);
//			
//			return;
//		}
//	}
//
//	//cuurentPositionInitialized = false;
//	
//	// Set in TTTManager (i.e. inform the rest of the application)
//	TTTManager::getInst().setCurrentPosition (_index);
//
//	// Update variables
//	currentPosition = _index;
//	currentQDir.setPath (experimentQDir.path() + "/" + experimentBasename + POSITION_MARKER + _index);
//	currentBasename = experimentBasename + POSITION_MARKER + _index;
//	
//	// Get position manager again (why?)
//	tttpm = TTTManager::getInst().getPositionManager (currentPosition);
//	if (tttpm) {
//		tttpm->setBasename (currentBasename);
//	}
//	//if (_oldTN)
//	//	_oldTN->markActive (false);
//	//if (_currentTN)
//	//	_currentTN->markActive();
//	
//	//currentThumbnail = _currentTN;
//	
//	
//	//note: 60 is used to stretch the text for a nice display
//        //StatusBar sb (fraLayout, "Reading hard disk...", 0, 60, 1);
//        QApplication::setOverrideCursor( QCursor(Qt::WaitCursor) );
//        //sb.display (true);
//
//        displayFiles();
//
//        //sb.display (false);
//	QApplication::restoreOverrideCursor();
//	
//}


bool TTTPositionLayout::setPosition( QString _posIndex/*, bool _initPosition */)
{
	// Nothing to do if already selected
	if(_posIndex == currentPosition)
		return true;

	// Change cursor
	ChangeCursorObject c;

	// Get position manager
	TTTPositionManager* newPosManager = TTTManager::getInst().getPositionManager(_posIndex);
	if(!newPosManager) {
		qWarning() << "TTTPositionLayout::setPosition() Error 1 - " << _posIndex;
		return false;
	}

	// Check if it is locked
	if (newPosManager->isLocked()) {
		QMessageBox::information (this, "Position not available", "This position is locked (due to problems in the logfile). You cannot access it now, please redo the log file conversion", "Ok");
		return false;
	}

	// Confirm selection in position view
	if(!positionView->selectPosition(_posIndex)) {
		qWarning() << "TTTPositionLayout::setPosition() Error 2 - " << _posIndex;
		return false;
	}

	// Update variables
	currentPosition = _posIndex;
	currentQDir.setPath (experimentQDir.path() + "/" + experimentBasename + POSITION_MARKER + _posIndex);
	currentBasename = experimentBasename + POSITION_MARKER + _posIndex;

	// Init position (does nothing if position is already initialized)
	/*if(_initPosition) {*/
	if(!initCurrentPosition(false)) {
		qWarning() << "TTTPositionLayout::setPosition() Error 3 - " << _posIndex;
		return false;
	}
	/*}*/

	// Set basename in TTTPositionManager
	newPosManager->setBasename (currentBasename);

	// Set CurrentPosition in TTTManager
	TTTManager::getInst().setCurrentPosition (_posIndex);

	// Set base position
	TTTManager::getInst().setBasePosition (currentPosition.toInt());

	// Update timescale in TTTTracking
	int ftp = newPosManager->getFirstTimePoint();
	int ltp = newPosManager->getLastTimePoint();
	TTTManager::getInst().frmTracking->setTimePoints (ftp, ltp, false);
	TTTManager::getInst().frmTracking->createWavelengthButtons();

	// Update display
	TTTManager::getInst().frmTracking->updateDisplay();

	// List image files
	displayFiles();

	// List tTt files
	showTTTFiles ();

	//display initial timescale
	TTTManager::getInst().frmTracking->displayTimeScale();

	return true;
}


void TTTPositionLayout::setExperimentQDir (const QDir &_expDir, int _positionCount, bool _fastLoad)
{
	
	experimentQDir = _expDir;
	experimentBasename = _expDir.path().mid (_expDir.path().findRev ('/') + 1);
	TTTManager::getInst().setExperimentBasename (experimentBasename);
	
	setWindowTitle (QString ("tTt %1 - Position Layout (%2)").arg(tttVersionLong).arg (experimentQDir.path()));
	//create position thumbnails and add them to the position view element
	
//	StatusBar sb (0, "Loading first picture of each position", 1, _positionCount);
//	sb.display (true);
//	
//	PositionThumbnail *pt = 0;
//	
//	Q3DictIterator<TTTPositionManager> iter (TTTManager::getInst().getAllPositionManagers());
//	for ( ; iter.current(); ++iter) {
//		
//		sb.update();
//		
//		if (! iter.current()->isAvailable())
//			continue;
//		
//		
//		QString picfilename = "";
//		//find first picture file in folder
////takes too long for normal hard disks as the positions contain too many pictures
///*		QValueVector<QString> pics = SystemInfo::listFiles (iter.current()->getPictureDirectory(), QDir::Files, "*.jpg;*.tif", false);
//		QValueVector<QString>::Iterator iterFiles = pics.begin();
//		
//		if (iterFiles)
//			picfilename = *iterFiles;*/
//		
//		///@todo if the picture sizes differ from the standard, give a warning!
//		
//		//now, faster politics are of need: just assume there is a picture with pattern "_t00001_w0.jpg" in the current position folder
//		if (_fastLoad)
//			picfilename = "";
//		else
//			picfilename = iter.current()->getPictureDirectory() + "/" + iter.current()->getBasename (true) + "00001_w0.jpg";
//		
//		pt = new PositionThumbnail (positionView->viewport(), iter.current()->positionInformation.getIndex(), picfilename, iter.current()->positionInformation.getComment(), chkDrawFrame->isChecked());
//		positionView->addChildLocal (pt, iter.current()->positionInformation.getLeft(), iter.current()->positionInformation.getTop());
//		iter.current()->posThumbnailInTTTPosLayout = pt;
//	}
//	
//	//positionView->setConnections();
//	
//	positionView->setFactors (5, 1.0f);

	positionView->initialize(chkDrawFrame->isChecked(), true, true);

	updatePositionDisplay();
	
	//sb.display (false);
}

void TTTPositionLayout::resizeEvent (QResizeEvent *)
{
	//positionView->fit();
}

void TTTPositionLayout::closeEvent (QCloseEvent *_ev)
{
	if (! TTTManager::getInst().frmTracking->SaveDialog()) {
		//check whether the current tree is already saved
		_ev->ignore();
		return;
	}

	// Request close of frmExportImagePatches, cancel closeEvent if form could not be closed
	if(frmExportImagePatches) {
		if(frmExportImagePatches->isVisible()) {
			if(!frmExportImagePatches->close()) {
				_ev->ignore();
				return;
			}
		}
		delete frmExportImagePatches;
	}

	// Save window layout
	UserInfo::getInst().saveWindowPosition(this, "TTTPositionLayout");
	
    TTTManager::getInst().shutDownTTT (false);

	_ev->accept();

}

//EVENT:
void TTTPositionLayout::showEvent (QShowEvent *_ev)
{
	if(!_ev->spontaneous()) {
		// Show version window, if necessary
		if(!versionWindow) {
			QString lastVersionWindowWasShown = UserInfo::getInst().getStyleSheet().getLastTTTChangelogVersion();

			// Only show if this is a new version
			if(lastVersionWindowWasShown.isEmpty() || internalVersion > lastVersionWindowWasShown) {
				versionWindow = new TTTVersionInfo(this, true);
				versionWindow->setModal(true);
				versionWindow->show();
			}
		}

		// Load window layout if that hasn't happened yet..
		if(!windowLayoutLoaded) {
			UserInfo::getInst().loadWindowPosition(this, "TTTPositionLayout");
			windowLayoutLoaded = true;
		}
	}

	_ev->accept();
}


//SLOT:
void TTTPositionLayout::displayInfoAboutSelectedPics ()
{
	
	TTTPositionManager *tttpm = TTTManager::getInst().getPositionManager (currentPosition);

	if(!tttpm)
		return;
	
	//if (lvwImages->childCount() == 0)
	//	//if there is no image in this folder and the user clicked into the file display
	//	return;
	
	//number of selected images
	int selcounter = countSelected();
	lblImageCount->setText (QString ("%1 images selected").arg (selcounter));
	
	pbtLoadImages->setEnabled (selcounter > 0);
	pbtLoadImages->setText (QString ("Load %1 images").arg (selcounter));
	
	int memoryConsumption = 0;
	if (tttpm->positionInformation.LoadingRegion.initialized())
		memoryConsumption = tttpm->positionInformation.LoadingRegion.width() * tttpm->positionInformation.LoadingRegion.height();
	else
		memoryConsumption = tttpm->positionInformation.ImageRect[0].width() * tttpm->positionInformation.ImageRect[0].height();
	
	// Byte -> KByte
	memoryConsumption /= 1024;

	if (memoryConsumption < 1024)
		lblImageSize->setText (QString ("%1 KB per image").arg (memoryConsumption));
	else
		lblImageSize->setText (QString ("%1 MB per image").arg ((float)memoryConsumption / 1024.0, 0, 'f', 2));
	
	//total memory amount
	lblSizeTotal->setText (QString ("%1 MB completely").arg (memoryConsumption * selcounter / 1024));
	
	
	//number of images that fit into free memory
	int freeRAM = SystemInfo::getFreeRAM ('k');
	if (memoryConsumption != 0)
		maxMemImages = freeRAM / memoryConsumption;
	
	lblMaxImageCount->setText (QString ("Max images: %1").arg (maxMemImages));
	
}

//SLOT:
int TTTPositionLayout::countSelected()
{
	
	////index is the index for the QBitArray that holds the checked status for each item
	////problem: 	when some wavelengths are not visible, a simple increment would overwrite
	////			the previously stored values
	////solution:	the index is calculated timepoint & wavelength dependent -> unique
	//
	//int index;
	//int pos = 0;
	//QString tmpText;
	//int selcounter = 0; //SelectionCounter = 0;
	//
	//
	//for (Q3ListViewItemIterator iter (lvwImages); iter.current(); ++iter) {
	//	
	//	tmpText = ( ( Q3CheckListItem* )iter.current() )->text();
	//	pos = tmpText.find (WAVELENGTH_MARKER) - 5;
	//	
	//	//calculate selection array index from timepoint and wavelength
	//	//note: assumes that the filenames in the list include the pattern "00000_w0.jpg" 
	//	//index = (tmpText.mid (pos, 5)).toInt() * (MAX_WAVE_LENGTH + 1) + (tmpText.right (1)).toInt();
	//	index = FileInfoArray::CalcTimePointFromFilename (tmpText) * (MAX_WAVE_LENGTH + 1) + FileInfoArray::CalcWaveLengthFromFilename (tmpText);
	//	
	//	if (( ( Q3CheckListItem* )iter.current() )->isOn()) {
	//		selcounter++;
	//		
	//		//store the current check status for the current item
	//		itemsChecked.setBit (index);
	//	}
	//	else
	//		itemsChecked.clearBit (index);
	//}
	//
	//return selcounter;

	return pictureListModel->numCheckedPictures();
}

Q3ButtonGroup* TTTPositionLayout::createNewWavelengthGroup (int _wavelength, int _wavelengthsTillNow)
{

	//layout:
	//button group containing: - wl button
	//                         - select all button
	//                         - select none button
	//                         - size info label
	
	//problem: name conflicts when controls with the same name are created more than once (a delete does not cure this problem)
	//solution: an already created object needs not to be created again -> thus, we test whether this object/wavelength group is already there and reuse it
	
	
	Q3ButtonGroup *wlgroup = wavelengthButtonGroups [_wavelength];
	
	if (wlgroup) {
		//a group for this wavelength already exists => re-use it
		
		//make sure that the "show wavelength" buttons are all set on
		QObject *wlButton = wlgroup->child (QString ("show_%1").arg (_wavelength));
		if (wlButton)
			((QPushButton *)wlButton)->setOn (true);
	}
	else {
		//a group for this wavelength does not yet exist => create it
		
		
		wlgroup = new Q3ButtonGroup (grbWavelengths, QString ("wlgroup_%1").arg (_wavelength));
		wlgroup->setMinimumSize (106, 56);
		wlgroup->setMaximumSize (106, 56);
                wlgroup->setFrameShape (Q3GroupBox::NoFrame);
		
		QPushButton *wlButton = new QPushButton (wlgroup, QString ("show_%1").arg (_wavelength));
		wlButton->setText (QString ("%1").arg (_wavelength));
		wlButton->setGeometry (1, 1, 30, 30);
		wlButton->setMinimumSize (30, 30);
		wlButton->setMaximumSize (30, 30);
		wlButton->setToggleButton (true);
		wlButton->setOn (true);
		wlButton->show();
		
		QPushButton *selectButton = new QPushButton (wlgroup, QString ("select_%1").arg (_wavelength));
		selectButton->setText ("(x)");
		selectButton->setGeometry (37, 1, 30, 30);
		selectButton->setMinimumSize (30, 30);
		selectButton->setMaximumSize (30, 30);
		selectButton->show();
		
		QPushButton *deselectButton = new QPushButton (wlgroup, QString ("deselect_%1").arg (_wavelength));
		deselectButton->setText ("(  )");
		deselectButton->setGeometry (73, 1, 30, 30);
		deselectButton->setMinimumSize (30, 30);
		deselectButton->setMaximumSize (30, 30);
		deselectButton->show();
		
        QLabel *sizeLabel = new QLabel (wlgroup); //, QString ("sizeinfo_%1").arg (_wavelength));
		sizeLabel->setGeometry (1, 37, 102, 16);
		sizeLabel->setMinimumSize (30, 10);
		sizeLabel->setText ("(size info)");
		sizeLabel->show();

		// Put in hash
		imageSizeLabels.insert(_wavelength, sizeLabel);
		
		//note: deciphering the sender button is done in the slots
		connect ( wlButton, SIGNAL (clicked()), this, SLOT (showWaveLength()));
		connect ( selectButton, SIGNAL (clicked()), this, SLOT (selectWaveLength()));
		connect ( deselectButton, SIGNAL (clicked()), this, SLOT (selectWaveLength()));
		
		wavelengthButtonGroups.insert (_wavelength, wlgroup);
		
	}
	
	//the geometry has to be adapted nonetheless, as maybe now there are other wavelengths than before
	int y = 20 + _wavelengthsTillNow * (56 + 5);
	wlgroup->setGeometry (2, y, 106, 56);
	wlgroup->show();
	
	
	return wlgroup;
}

void TTTPositionLayout::hideWavelengthButtons()
{
	Q3IntDictIterator<Q3ButtonGroup> iter (wavelengthButtonGroups);
	
	for ( ; iter.current(); ++iter) {
		iter.current()->hide();
	}
}

void TTTPositionLayout::displayImageSizes()
{
	if (image_sizes_set)
		return;
	
	
	TTTPositionManager *tttpm = TTTManager::getInst().getPositionManager (currentPosition);
	
	if (! tttpm)
		return;
	
	int wl = 0;
	//Q3ValueList<int> wls (tttpm->getAvailableWavelengths());
	//Q3ValueList<int>::iterator iter;

	const QSet<int>& wls = tttpm->getAvailableWavelengthsByRef();
    for (QSet<int>::const_iterator iter = wls.begin(); iter != wls.end(); ++iter) {
		
		wl = *iter;
		

		////find correct wavelength size label
		//QString label_name = QString ("sizeinfo_%1").arg (wl);
		//QObject *label = grbWavelengths->child (label_name);
		
		// Check if label exists for wl
		QLabel* label = imageSizeLabels.value(wl, 0);
		if (label) {
			//correct label found
			QString tmp = QString ("%1*%2 pixel").arg (tttpm->positionInformation.ImageRect [wl].width())
				.arg (tttpm->positionInformation.ImageRect [wl].height());

			label->setText (tmp);
		}
		
	}
	
	image_sizes_set = true;
}


void TTTPositionLayout::displayFiles (bool _setWaveLengthBoxes)
{
//#ifdef DEBUGMODE
//	DWORD start = GetTickCount();
//#endif

	// Get pos manager
	TTTPositionManager *tttpm = TTTManager::getInst().getPositionManager (currentPosition);
	if (! tttpm)
		return;

	//// Create list of file extensions
	//QStringList extensions;
	//extensions << ".jpg" << ".jpeg" << ".tiff" << ".tif";

	//// Get unsorted file list
	//QStringList fileList = FastDirectoryListing::listFiles(currentQDir, extensions);

	//// Sort it
	//fileList.sort();

	//if(fileList.size() == 0) {
	//	QApplication::restoreOverrideCursor();	
	//	return;
	//}

	// Get filelist
	const QStringList& fileList = tttpm->getPictures()->getTruncatedFileList();

	//// Show statusbar while iterating over files
	//StatusBar sb (fraStatusBar, "Loading files...", 1, fileList.size());
	//sb.display (true);

	// Iterator
	int i = 0;

	// (Re-)create wl-boxes -> we have a new position folder
	if (_setWaveLengthBoxes) {
		image_sizes_set = false;

		// Hide old ones
		for (int j = 0; j <= MAX_WAVE_LENGTH; j++) 
			ShowWaveLength [j] = false;

		//tttpm->resetAvailableWavelengths();

		//BS 2010/03/12 controls are now created dynamically
		hideWavelengthButtons();

		// Get name of first file
		const QString& firstFile = fileList[i];

		if(!firstFile.isEmpty()) {
			lblBaseName->setText ("Base name: " + currentBasename);
			suffix = firstFile.right (firstFile.length() - firstFile.findRev('.') - 1);	//get the file ending (assumed to be constant for all files in this folder)
		}
	}

	//// Min/max timepoints of all pictures
	//int maxTimePoint = 0;
	//int minTimePoint = 9999999;

	// Number of created wl-buttons
	int wavelengthButtonsCreatedTillNow = 0;

	//// Iterate over files
	//for(; i < fileList.size(); ++i) {
	//	//// Update statusbar
	//	//sb.update();

	//	//// Truncate basename
	//	//fileList[i] = fileList[i].right (fileList[i].length() - currentBasename.length() - 2);

	//	// Get current filename
	//	const QString& curFile = fileList[i];

	//	// Extract wl and tp from filename
	//	int waveLength = FileInfoArray::CalcWaveLengthFromFilename (curFile);
	//	int timePoint = FileInfoArray::CalcTimePointFromFilename (curFile);

	//	//// Get min and max timepoint
	//	//maxTimePoint = qMax(maxTimePoint, timePoint);
	//	//minTimePoint = qMin(minTimePoint, timePoint);

	//	// Create wl-buttons if necessary
	//	if (_setWaveLengthBoxes) {
	//		ShowWaveLength [waveLength] = true;

	//		//show necessary wavelength selection buttons
	//		//BS 2010/03/12 controls are now created dynamically
	//		if (! tttpm->availableWavelengthsSet()) {
	//			if (! tttpm->isWavelengthAvailable (waveLength)) {
	//				//not yet encountererd, thus create the buttons
	//				createNewWavelengthGroup (waveLength, wavelengthButtonsCreatedTillNow);
	//				wavelengthButtonsCreatedTillNow++;
	//			}
	//			//tttpm->setWavelengthAvailable (waveLength);
	//		}
	//	}
	//}

	// Create wl buttons
	if(_setWaveLengthBoxes) {
		for(int wl = 0; wl <= tttpm->getMaxAvailableWavelength(); ++wl) {
			// Check if wl is available
			if(tttpm->isWavelengthAvailable(wl)) {
				ShowWaveLength [wl] = true;

				createNewWavelengthGroup (wl, wavelengthButtonsCreatedTillNow++);
			}
		}
	}

	// Put them into list
	pictureListModel->setStringList(fileList);

	//if (_setWaveLengthBoxes) {
	//	//// Reset image size information
	//	//for (int i = 0; i <= MAX_WAVE_LENGTH; ++i)
	//	//	TTTManager::getInst().getPositionManager (currentPosition)->positionInformation.ImageRect [i].setCoords (0, 0, -1, -1);	

	//	// New pos -> show ttt files
	//	showTTTFiles();
	//}

	// Get new image size information for all wavelengths and display them
	//getImageSize();
	displayImageSizes();

	// Init region selection form
	initLoadingRegion (currentPosition);

	// Init selected pics info
	displayInfoAboutSelectedPics();

	//// Available WLs have been set
	//tttpm->setAvailableWavelengthsSet (true);

	//// Hide statusbar
	//sb.display (false);

	//// DEBUG
	//DWORD endTime = GetTickCount();
	//QString dbg = QString("Time: %1").arg((endTime-startTime));
	//qDebug() << dbg;

//#ifdef DEBUGMODE
//	DWORD end = GetTickCount();
//
//	qDebug() << "listFiles() Time: " << end - start;
//#endif
	
	return;
}

////display all picture-files in currently selected directory
//void TTTPositionLayout::displayFiles (bool _setWaveLengthBoxes, int _waveLength, bool _show)
//{

//
//	TTTPositionManager *tttpm = TTTManager::getInst().getPositionManager (currentPosition);
//	
//	if (! tttpm)
//		return;
//	
//	
//	QString d = "";
//	
/////@todo change to log file parsing...	note: the log file is not yet read at this place!
/////      so try at HZM what costs less time: to read the log file and display the pics or to leave it "as is"
//
//    QVector<QString> pics = SystemInfo::listFiles (currentQDir.absolutePath(), QDir::Files, "*.jpg;*.jpeg;*.tiff;*.tif", false);
//    //const QFileInfoList pics = currentQDir.entryInfoList ("*.jpg;*.jpeg;*.tiff;*.tif");
//    if (pics.empty())
//		return;
//
//    QVector<QString>::Iterator iterFiles = pics.begin();
//
//    //QFileInfoListIterator iterPics (pics.begin());
//    //QFileInfo *info = 0;
//
//
//    StatusBar sb (fraStatusBar, "Loading files...", 1, pics.count());
//	sb.display (true);
//	
//	MyQCheckListItem *imageCLI = 0;
//	
//	//if this procedure is called because a different folder was selected, the old files are completely removed and the current ones are added to the empty list
//	//otherwise, only the current list is updated (files that should be invisible are removed and files that should be visible are inserted alphabetically, resp.)
//	if (_setWaveLengthBoxes) {
//		image_sizes_set = false;
//					
//		while (lvwImages->columns())
//			lvwImages->removeColumn (lvwImages->columns() - 1);
//		
//		lvwImages->addColumn("Pictures");
//		//lvwImages->setColumnWidthMode (0, Q3ListView::Maximum);		// Takes too much time
//		//lvwImages->setColumnWidthMode (0, Q3ListView::Manual);
//		
//		
//		//remove all wavelength option buttons; those are added later
//		for (int i = 0; i <= MAX_WAVE_LENGTH; i++) 
//			ShowWaveLength [i] = false;
//		
//		
//		///@todo use available wavelengths in tttpositionmanager for faster display!
//		tttpm->resetAvailableWavelengths();
//		
//		//BS 2010/03/12 controls are now created dynamically
//		hideWavelengthButtons();
//	}
//	
//	
//	
//	int waveLength = 0;
//	bool addFile = true;
//	
//	// Number of wavelengths that have been created yet
//	int wavelengthButtonsCreatedTillNow = 0;
//	
//	if (_setWaveLengthBoxes) {
//		//get experiment base name and file extension (suffix)
//                d = *iterFiles;
//		if (! d.isEmpty()) {
//			d = d.right (d.length() - d.findRev ('/') - 1);   	//only the pure filename without path is needed
//			lblBaseName->setText ("Base name: " + currentBasename);
//			suffix = d.right (d.length() - d.findRev('.') - 1);	//get the file ending (assumed to be constant for all files in this folder)
//		}
//	}
//	
//	int timePoint = 0;
//	int minTimePoint = 9999999;
//	int maxTimePoint = 0;
//	
//	//does not work correctly with multiple wavelengths
//	//lvwImages->setUpdatesEnabled (false);
//	
//    //info = iterPics;
//    //while (info) {
//
//	// Iterate over all pictures
//    while (iterFiles != pics.end()) {
//            //++iterPics;
//		
//		sb.update();
//		
//        //d = info->filePath();
//        // Get pure filename
//		d = *iterFiles;
//		d = d.right (d.length() - d.findRev ('/') - 1);   //only the pure filename without path is needed
//		
//		// Extract wl and tp from filename
//		waveLength = FileInfoArray::CalcWaveLengthFromFilename (d);
//		timePoint = FileInfoArray::CalcTimePointFromFilename (d);
//		if (timePoint > maxTimePoint)
//			maxTimePoint = timePoint;
//		if (timePoint < minTimePoint)
//			minTimePoint = timePoint;
//		
//		// Check if file has to be added to listview
//		addFile = false;
//		if (_setWaveLengthBoxes)
//			addFile = true;
//		else
//			if ((waveLength == _waveLength) || (_waveLength == -1))	// File has required wavelength
//				addFile = true;
//		
//		// Wavelength buttons have to be created
//		if (_setWaveLengthBoxes) {
//			ShowWaveLength [waveLength] = true;
//			
//			//show necessary wavelength selection buttons
//			//BS 2010/03/12 controls are now created dynamically
//			if (! tttpm->availableWavelengthsSet()) {
//				if (! tttpm->isWavelengthAvailable (waveLength)) {
//					//not yet encountererd, thus create the buttons
//					createNewWavelengthGroup (waveLength, wavelengthButtonsCreatedTillNow);
//					wavelengthButtonsCreatedTillNow++;
//				}
//				
//				tttpm->setWavelengthAvailable (waveLength);
//			}
//			
//			
//		}
//		else {
//			// Hide file if wl is hidden
//			if (waveLength >= 0 && waveLength <= MAX_WAVE_LENGTH)
//				if (! ShowWaveLength [waveLength])
//					addFile = false;	
//		}
//		
//		//the (usual) pattern for a filename:
//		//example: 051104-2_p025_t01299_w0.jpg/tif
//		//parts:   (date)??_p025_t(tpt)_w(wl).jpg/tif
//		//the only thing of interest is the timepoint and the wavelength!
//		
//		//the base name is truncated
//		d = d.right (d.length() - currentBasename.length() - 2);
//		
//		// Get color for item in listview
//		QColor color = Qt::black;
//        if ((waveLength > 0) && (waveLength <= MAX_WAVE_LENGTH))
//			color = WAVELENGTHCOLORS [waveLength];
//		
//		if (_setWaveLengthBoxes) {
//			if (addFile)
//				imageCLI = new MyQCheckListItem (lvwImages, d, Q3CheckListItem::CheckBox, color);
//		}
//		else {
//			//only the provided wavelength should be updated (shown or hidden)
//			if (_show && addFile) {		//add file entry, if not yet visible [assumed at the moment]
//				//note: the list is sorted after complete update, so there is no need to add it alphabetically
//				imageCLI = new MyQCheckListItem (lvwImages, d, Q3CheckListItem::CheckBox, color);
//				
//				//the check status is set for this item
//				imageCLI->setOn (itemsChecked.testBit (timePoint * (MAX_WAVE_LENGTH + 1) + waveLength));
//			}
//			else {			//remove file entry, if existing and wavelength is correct
//				if ((waveLength == _waveLength) | (_waveLength == -1)) {
//					Q3ListViewItem *rm = lvwImages->findItem (d, 0);
//					if (rm)
//						lvwImages->takeItem (rm);
//				}
//			}
//		}
//		
//                //info = *iterPics;
//                ++iterFiles;
//	}
//	
//	//lvwImages->setUpdatesEnabled (true);
//	
//	if (! _setWaveLengthBoxes && _show)
//		lvwImages->sort();
//	
//	////span first column over the complete width of the widget		// Takes too much time
//	//lvwImages->adjustColumn (0);
//	lvwImages->setResizeMode(Q3ListView::AllColumns);
//	
//	if (_setWaveLengthBoxes)
//		for (int i = 0; i <= MAX_WAVE_LENGTH; ++i)
//			TTTManager::getInst().getPositionManager (currentPosition)->positionInformation.ImageRect [i].setCoords (0, 0, -1, -1);			//sets a null rect -> information needs to be received again
//	
//	//check if there are existing ttt files
//	if (_setWaveLengthBoxes) {
//		showTTTFiles();
//	}
//	
//	if (_setWaveLengthBoxes) {
//		//resize the "checked pictures" array
//		itemsChecked.resize ((maxTimePoint + 1) * (MAX_WAVE_LENGTH + 1));
//		itemsChecked.fill (0);
//	}
//	
//	//memory demand per image
//	getImageSize();
//	
//	displayImageSizes();
//	
//	initLoadingRegion (currentPosition);
//	displayPictureInformation();
//	
//	tttpm->setAvailableWavelengthsSet (true);
//	
//	sb.display (false);
//	
///*	//testing...
//	QValueList<int> wls (tttpm->getAvailableWavelengths());
//	QValueList<int>::iterator iter;
//    	for (iter = wls.begin(); iter != wls.end(); ++iter)
//        	std::cout << (*iter) << std::endl;*/
//

//}

bool TTTPositionLayout::searchAndDisplayTTTFiles (const QString &_folder, bool _onlyFrmTracking, const QString &_mark)
{
	QDir dir (_folder);

	bool filesfound = false;
	if (dir.exists()) {

		QVector<QString> pics = SystemInfo::listFiles (dir.absolutePath(), QDir::Files, "*" + TTT_FILE_SUFFIX, false);
		if (pics.empty())
			//nothing found -> no ttt files in the folder
			return filesfound;

		QVector<QString>::const_iterator iterFiles = pics.constBegin();

		if (_onlyFrmTracking) {
			/*			if (TTTManager::getInst().frmTracking) {
			TTTManager::getInst().frmTracking->lboColony->insertItem ("---  files on " + dir.absPath() + "  ---");
			TTTManager::getInst().frmTracking->lboColony->insertItem ("");
			}*/
		}
		else {
			cboTTTFiles->insertItem ("---  files on " + dir.absPath() + "  ---");
			cboTTTFiles->insertItem ("");
		}


		//while (info) {
		while (iterFiles != pics.constEnd()) {
			//++iterTTT;

			//ttt file exists - get the filename
			//d = info->filePath();
			QString d = *iterFiles;
			d = d.right (d.length() - d.findRev ('/') - 1);

			if (_onlyFrmTracking) {
				/*				if (TTTManager::getInst().frmTracking)
				TTTManager::getInst().frmTracking->lboColony->insertItem (_mark + d);*/
			}
			else {
				cboTTTFiles->insertItem (_mark + d);
			}

			filesfound = true;

			//info = *iterTTT;
			++iterFiles;
		}
	}

	return filesfound;
}

void TTTPositionLayout::showTTTFiles (bool _onlyFrmTracking)
{
	//if _onlyFrmTracking is true, then the current position is the base position
	//which means that we can safely display the ttt files in the cell editor's colony list
	//otherwise (for other positions), the colonies must be displayed only here
	
	
	TTTPositionManager *tttpm = TTTManager::getInst().getPositionManager (currentPosition);
	
	if (! tttpm)
		return;
	
	if (_onlyFrmTracking) {
/*		if (TTTManager::getInst().frmTracking) {
			TTTManager::getInst().frmTracking->lboColony->clear();
			TTTManager::getInst().frmTracking->lboColony->insertItem ("");	//empty colony
		}*/
	}
	else
		cboTTTFiles->clear();
	
	//look for ttt files on network drive (in both new and old folder)
	bool filesfound = searchAndDisplayTTTFiles (tttpm->getTTTFileDirectory(), _onlyFrmTracking, "");
	
	//BS 2010/03/19 check if there are still files on the old folder system
	//add @ to indicate that the file is in the old folder
	if (filesfound)
		cboTTTFiles->insertItem ("");
	
	filesfound |= searchAndDisplayTTTFiles (tttpm->getTTTFileDirectory (false, true), _onlyFrmTracking, OLD_FOLDER_TTTFILE_MARKER);
	
	
	
//BS 2010/03/17: discontinued - if really old files need to be loaded, they can be copied to the nas folders

/*	//look for ttt files in current folder
	const QFileInfoList *ttt = currentQDir.entryInfoList ("*" + TTT_FILE_SUFFIX);
	QFileInfoListIterator iterTTT (*ttt);
	
	if (_onlyFrmTracking) {
		if (TTTManager::getInst().frmTracking) {
			TTTManager::getInst().frmTracking->lboColony->insertItem ("");
			TTTManager::getInst().frmTracking->lboColony->insertItem ("---  files on " + currentQDir.absPath() + "  ---");
			TTTManager::getInst().frmTracking->lboColony->insertItem ("");
		}
	}
	else {
		cboTTTFiles->insertItem ("");
		cboTTTFiles->insertItem ("---  files on " + currentQDir.absPath() + "  ---");
		cboTTTFiles->insertItem ("");
	}
	
	info = *iterTTT;
	while (info) {
		++iterTTT;
		
		//ttt file exists - get the filename
		d = info->filePath();
		d = d.right (d.length() - d.findRev ('/') - 1);
		
		if (_onlyFrmTracking) {
			if (TTTManager::getInst().frmTracking)
				TTTManager::getInst().frmTracking->lboColony->insertItem (d);
		}
		else
			cboTTTFiles->insertItem (d);
		
		
		filesfound = true;
		
		info = *iterTTT;
	}*/
	
	
        //if (! TTTManager::getInst().getBasePositionManager()) {
		//a ttt file can only be loaded if there is none yet loaded
		if (filesfound)
			pbtLoadTTTFile->setEnabled (true);
		else
			pbtLoadTTTFile->setEnabled (false);
	//}
	
}

QString TTTPositionLayout::getImageSize (bool _fromListView, QString _posIndex)
{
	if (_posIndex == "")
		_posIndex = currentPosition;
	
	TTTPositionManager *tttpm = TTTManager::getInst().getPositionManager (_posIndex);
	
	if (! tttpm)
		return "";
	
	QString tmpSuffix = "";
	
	if (tttpm->positionInformation.ImageRect [0].isNull()) {
		//no information yet received for the first wavelength
		
		if (_fromListView) {
			if (pictureListModel->numPictures() == 0) {
				//set all sizes to (10,10)
				for (int i = 0; i <= MAX_WAVE_LENGTH; i++)
					tttpm->positionInformation.ImageRect [i].setRect (0, 0, 10, 10);
				
				return "xxx";		//nonsense, but reading was OK
			}
		}
		
		bool WaveLengthReceived [MAX_WAVE_LENGTH + 1];
		bool AllSet = false;
		
		for (int i = 0; i <= MAX_WAVE_LENGTH; i++)
			WaveLengthReceived [i] = false;
		
		
		QImage img;
		int wl = 0;
		
		if (_fromListView) {
			//read picture filenames from the list view object
			
			QString tmp = "";
			//Q3ListViewItemIterator iter (lvwImages);

			// Get pictures in list
			const QStringList& pics = pictureListModel->stringList();
			QStringListIterator iter(pics);
			
			//if (! iter.hasNext())
			//	return "";			//no files in view
			
			while (iter.hasNext() && ! AllSet) {
				
				//simply the first picture of each available wavelength is loaded
				tmp = iter.next();
				wl = FileInfoArray::CalcWaveLengthFromFilename (tmp);
				
				if (! WaveLengthReceived [wl]) {
					if (wl == 0) {
						//set suffix; find "." in filename:
						int pos = tmp.findRev (".");
						tmpSuffix = tmp.mid (pos + 1);
					}
					
					WaveLengthReceived [wl] = true;
					const QString fileName = currentQDir.absPath() + "/" + currentBasename + TIMEPOINT_MARKER + tmp;
					if (img.load (fileName)) {
						tttpm->positionInformation.ImageRect [wl].setRect (0, 0, img.width(), img.height());
					}
					else
						return "";		//nothing was set -> general failure!
				}
				
				AllSet = true;
				//if all available wavelengths' picture sizes are set, the loop can be exited
				for (int i = 0; i <= MAX_WAVE_LENGTH; i++) {
					//if there is a visible wavelength whose size is not yet read, the loop has to proceed
					if (ShowWaveLength [i] && (! WaveLengthReceived [i])) {
						AllSet = false;
						break;
					}
				}
				
				//++iter;
			}
		}
		else {
			//read picture filenames from the hard disk
			
			while (! AllSet) {
				
				QString pattern = QString ("*_w%1.jpg;*_w%1.tif").arg (wl);
				
				//find first picture file in folder for the current wavelength
				//NOTE: takes very long for normal hard disks as the positions contain too many pictures
                QVector<QString> pics = SystemInfo::listFiles (tttpm->getPictureDirectory(), QDir::Files, pattern, false);
                QVector<QString>::const_iterator iterFiles = pics.constBegin();
				
				if (iterFiles != pics.constEnd()) {
					QString picfilename = *iterFiles;
					
					if (wl == 0) {
						//set suffix; find "." in filename:
						int pos = picfilename.findRev (".");
						tmpSuffix = picfilename.mid (pos + 1);
					}
					
					if (! WaveLengthReceived [wl]) {
						WaveLengthReceived [wl] = true;
						if (img.load (picfilename)) {
							tttpm->positionInformation.ImageRect [wl].setRect (0, 0, img.width(), img.height());
						}
						else
							return "";		//nothing was set, due to loading problem of this picture -> general failure!
					}
				}
				else {
					//no file for the current wavelength found
					//to note this, set WaveLengthReceived [wl] = true
					WaveLengthReceived [wl] = true;
				}
				
				AllSet = true;
				//if for all available wavelengths the picture sizes are set, the loop can be left
				for (int i = 0; i <= MAX_WAVE_LENGTH; i++) {
					//if there is a wavelength whose size is not yet read, the loop has to proceed
					if ((! WaveLengthReceived [i])) {
						AllSet = false;
						break;
					}
				}
				
				wl++;
			}
			
		}
	}
	
	return tmpSuffix;
}

void TTTPositionLayout::initLoadingRegion (const QString &_posIndex)
{

	TTTPositionManager *tttpm = TTTManager::getInst().getPositionManager (_posIndex);
	
	if (! tttpm)
		return;
	
	//the loading region is initialised with the size of the pictures of wavelength 0
	if (! tttpm->frmRegionSelection)
		tttpm->createForms (false);
	
	tttpm->frmRegionSelection->init (tttpm->positionInformation.ImageRect [0], 0.08);
	tttpm->frmRegionSelection->setBoundingRect (tttpm->positionInformation.ImageRect [0], QColor (10, 100, 255), 0.08);
	tttpm->frmRegionSelection->setCaption (QString("Pos. %1, %2*%3 pixel")	.arg (_posIndex)
										.arg (tttpm->positionInformation.ImageRect [0].width())
										.arg (tttpm->positionInformation.ImageRect [0].height()));
	tttpm->frmRegionSelection->setScale (tttpm->positionInformation.ImageRect [0]);
	
	if (! tttpm->positionInformation.LoadingRegion.initialized()) {
		tttpm->positionInformation.LoadingRegion.initRectBox (tttpm->frmRegionSelection, tttpm->positionInformation.ImageRect [0], QColor (120, 215, 25), 0.08);
			//cut off the edges outside the original image size
		tttpm->positionInformation.LoadingRegion.truncate();
	}
}

void TTTPositionLayout::loadImages (bool _initPosition)
{
	TTTPositionManager *tttpm = TTTManager::getInst().getPositionManager (currentPosition);

	// Change cursor
	ChangeCursorObject c;
	
	if (_initPosition) {
		if (! initCurrentPosition (false))
			return;
	}
	
	if (pictureListModel->numCheckedPictures() > 0) {
	
		int picsMarkedForLoading = markSelectedPicturesForLoading();

		if (picsMarkedForLoading) {
			// Load selected pics asynchronous
			tttpm->getPictures()->loadPictures (true);

			// Do not use markPicturesLoadedForCurrentPosition() here as asynchronous loading will cause getLoadedPictures() to return 0 now
			//if(currentThumbnail)
			//	currentThumbnail->markPicturesLoaded (true);
			positionView->markPicturesLoaded(currentPosition, true);
		}
	}

	tttpm->frmRegionSelection->pbtApply->hide();		//not necessary anymore and just disturbing

	if (! TTTManager::getInst().frmTracking->isVisible())
		TTTManager::getInst().frmTracking->show();
}

//SLOT:
void TTTPositionLayout::loadTTTFile()
{
	if (! TTTManager::getInst().frmTracking)
		return;
	
	QString filename = cboTTTFiles->currentText();
	if (filename.isEmpty())
		return;
	
	if (! initCurrentPosition (false)) {
		////base position must be reset to nothing as otherwise trying another position would not be possible
		//TTTManager::getInst().resetBasePosition();
		return;
	}
	
	bool old_folder = false;
	if (filename.left (1) == OLD_FOLDER_TTTFILE_MARKER) {
		//file is in old folder system
		filename = filename.mid (1);
		old_folder = true;
	}
	
	//add absolute path to filename
	QString filename_with_path = "";
	if (! old_folder)
		filename_with_path = TTTManager::getInst().getBasePositionManager()->getTTTFileDirectory() + filename;
	else
		filename_with_path = TTTManager::getInst().getBasePositionManager()->getTTTFileDirectory (false, true) + filename;
	
	//load ttt file
	/*
	if (! TTTManager::getInst().frmTracking->loadTTTFile (filename_with_path)) {
		QMessageBox::information (this, "Error", "Loading " + filename_with_path + " failed.\n" + 
								QString ("Maybe the file version is not up to date (current = #%1)").arg (TTTFileVersion));
		
		pbtLoadTTTFile->setEnabled (true);
		pbtNewColony->setEnabled (true);
		
		return;
	}
	

	
	
	loadImages (false);
	
	//set current file in frmTracking->cboColony
	QString tmpFile = "";
//	int index = -1;
	
	if (old_folder)
		tmpFile = OLD_FOLDER_TTTFILE_MARKER + filename;
	*/
/*	for (int i = 0; i < (int)TTTManager::getInst().frmTracking->lboColony->count(); i++)
		if (TTTManager::getInst().frmTracking->lboColony->text (i) == tmpFile) {
			index = i;
			break;
		}
	
	TTTManager::getInst().frmTracking->lboColony->setCurrentItem (index);*/
	
	/*
	TTTManager::getInst().frmTracking->drawTree();
	TTTManager::getInst().frmTracking->refreshStatistics();
	//TTTManager::getInst().frmTracking->updatePositionHistory();
	TTTManager::getInst().frmTracking->setFormCaption (filename_with_path);
	
	TTTManager::getInst().frmTracking->show();
	*/

	TTTManager::getInst().frmTracking->show();
	TTTManager::getInst().frmTracking->setColony(filename_with_path);
	TTTManager::getInst().frmTracking->raise();
	if(TTTManager::getInst().frmTracking->isMinimized())
		TTTManager::getInst().frmTracking->showNormal();
}

//SLOT:
/* (Not used right now)
void TTTPositionLayout::addNewColony()
{
	if (! TTTManager::getInst().frmTracking)
		return;
	
	
// 	TTTManager::getInst().setBasePosition (TTTManager::getInst().getCurrentPosition());
	
	
	if (! initCurrentPosition (true))
		return;
	
	loadImages (false);
	
	//TTTManager::getInst().frmTracking->lboColony->setCurrentItem (-1);
	
	TTTManager::getInst().frmTracking->drawTree();
	TTTManager::getInst().frmTracking->refreshStatistics();
	//TTTManager::getInst().frmTracking->updatePositionHistory();
	
	TTTManager::getInst().frmTracking->show();
	
}
*/

//SLOT: opens the form for selecting a region
void TTTPositionLayout::openRegionSelection()
{
        //BS
        //does obviously not work, and nobody uses it anyway (currently)...
        return;

	//one picture has to be loaded to get the filesize information
	
        if (getImageSize().isEmpty())
		return;
	
	TTTManager::getInst().getPositionManager (currentPosition)->createForms (false);
	
        //TTTManager::getInst().getPositionManager (currentPosition)->frmRegionSelection->setBox (& TTTManager::getInst().getPositionManager (currentPosition)->positionInformation.LoadingRegion);
	
	TTTManager::getInst().getPositionManager (currentPosition)->frmRegionSelection->exec();
	
	displayInfoAboutSelectedPics();
}

//SLOT:
void TTTPositionLayout::toggleDrawThumbnailFrame (bool _draw)
{
	//Q3IntDict<PositionThumbnail> thumbnails = positionView->getThumbnails();
	//
	//for (uint i = 0; i < thumbnails.size(); i++)
	//	if (thumbnails.find (i)) {
	//		thumbnails.find (i)->setDrawFrame (_draw);
	//		thumbnails.find (i)->resizeEvent (0);
	//	}

	// Much easier now:
	positionView->setDrawThumbNailFrames(_draw);	
}


void TTTPositionLayout::toggleDrawThumbnailText( bool _draw )
{
	// Draw text
	positionView->setDrawText(_draw);
}


//bool TTTPositionLayout::initPositionManager (TTTPositionManager *_tttpm, const QString &_pictureSuffix)
//{
//	if (! _tttpm)
//		return false;
//	
//        bool success = _tttpm->initialize (_pictureSuffix);
//	
//        //if (! success) {
//        //    ////initialization failed, thus the position is locked -> redraw the position layout
//        //    //positionView->refresh();
//        //}
//
//        return success;
///*	if (_tttpm->isInitialized())
//		//method already run before
//		return true;
//	
//	QString posIndex = _tttpm->positionInformation.index;
//	
//	//the maximum of possible wavelengths
//	int WaveLengthCount = MAX_WAVE_LENGTH + 1;
//	
//	int ftp = -1, ltp = -1;
//	ftp = _tttpm->getFirstTimePoint();
//	ltp = _tttpm->getLastTimePoint();
//	
//	if (! _tttpm->getFiles()) {
//		Tools::readLogFile (_tttpm, ltp, ftp, _pictureSuffix);
//	}
//	else {
//		_tttpm->setFileInfoArray (0);	//just update attributes, do not delete file associations
//	}
//	
//	//create a new PictureArray with the size of ALL possible pictures 
//	//(if a later loading is necessary, no resizing needs to be done)
//	//it only needs to be created when it not yet exists
//	//NOTE: assumes that the directory for pictures and with it the number of pics stays the same during a run of tTt!
//	
//	_tttpm->createForms();
//	
//	if (! _tttpm->getPictures()) 
//		//Pictures = new PictureArray (lvwImages->childCount());
//		_tttpm->setPictures (new PictureArray (_tttpm, (ltp - ftp + 1) * WaveLengthCount));
//	
//	if (! _tttpm->getPictures()) {
//		QMessageBox::information (this, "Fatal error", "tTt ran out of memory and will close now.", QMessageBox::Ok);
//		close();		//close tTt - no sense continuing for allocation failed most probably because of too less memory!
//	}
//	
//	_tttpm->getPictures()->init();
//	
//	_tttpm->frmMovie->setTimePoints (ftp, ltp);
//	
//	_tttpm->frmRegionSelection->setBoundingBox (& TTTManager::getInst().getPositionManager (currentPosition)->positionInformation.LoadingRegion, true);
//	
//	_tttpm->frmMovie->initDisplays();
//	_tttpm->frmMovie->initForms();
//	
//	_tttpm->setInitialized (true);	
//	
//	return true;*/
//}

bool TTTPositionLayout::initCurrentPosition (bool _addColony)
{
//#ifdef DEBUGMODE
//	DWORD start = GetTickCount();
//#endif


	// Get position manager of selected position
	TTTPositionManager *ctttpm = TTTManager::getInst().getPositionManager (currentPosition);
	if (! ctttpm)
		return false;

	// Return if position already initialized
	if(ctttpm->isInitialized())
		return true;

	//if(cuurentPositionInitialized)
	//	return true;

	//cuurentPositionInitialized = true;
	
	//TTTManager::getInst().setCurrentPosition (currentPosition);
	
	if (TTTManager::getInst().getPositionManager (currentPosition)->isLocked())
		return false;
	
	//if (suffix.isEmpty()) {
	//	//no pictures in current folder
	//	if (TTTManager::getInst().loadTTTFilesOnly())
	//		//it's ok, use default
	//		suffix = "jpg";
	//	else {
	//		QMessageBox::information (this, "Loading error", "No picture files in directory - nothing is loaded.", "Ok");
	//		return false;
	//	}
	//}

	// Init pos manager (read log file, create picture array)
	if(!ctttpm->initialize(/*suffix*/))
		return false;

	// Check if we have no base positon yet
	if (! TTTManager::getInst().getBasePositionManager()) {
		/*
		Not anymore:
		//this is the only place in the complete program where the base position is set to a value
		//it is reset when the cell editor (tree window) is closed
		*/

		// Set base position manager as early as possible
		TTTManager::getInst().setBasePosition (currentPosition.toInt());
		showTTTFiles (true);
	}
		
	if (! TTTManager::getInst().getBasePositionManager())
		return false;

	/*
	int ftp = ctttpm->getFirstTimePoint();
	int ltp = ctttpm->getLastTimePoint();
	TTTManager::getInst().frmTracking->setTimePoints (ftp, ltp, _addColony);
	TTTManager::getInst().frmTracking->createWavelengthButtons();
	*/
	//TTTManager::getInst().frmTracking->initForms();
	
	
	pbtLoadTTTFile->setEnabled (false);
	//pbtNewColony->setEnabled (false);
	
	TTTManager::getInst().setExternalTracks (0);
	TTTManager::getInst().setShowExternalTracks (false);
	
	////display initial timescale
	//TTTManager::getInst().frmTracking->displayTimeScale();

	// Why is window shown now?
	//if (! TTTManager::getInst().frmTracking->isVisible())
	//	TTTManager::getInst().frmTracking->show();

//#ifdef DEBUGMODE
//	DWORD end = GetTickCount();
//
//	qDebug() << "Time: " << end - start;
//#endif

	return true;
}

//SLOT:
void TTTPositionLayout::showWaveLength()
{
	if (! sender())
		return;
	
	QString name = sender()->name();
	
	bool _on = ( ( QPushButton* )sender())->isOn();
	
	//BS 2010/03/12 controls are now created dynamically
	
	int wl = -1;
	
	if (name.left (4) == "show")
		wl = name.mid (5).toInt();
	
	if (wl == -1)
		return;
	
	if (! _on) {
		if (ShowWaveLength [wl])
			wavelengthsHidden++;
	}
	else
		if (! ShowWaveLength [wl])
			wavelengthsHidden--;
	
	ShowWaveLength [wl] = _on;
	
	//update the list (insert new items or remove unwanted ones)	
	//displayFiles (false, wl, _on);
	pictureListModel->setWaveLengthHidden(wl, !_on);	// _on = true -> hide = false

}

//SLOT:
void TTTPositionLayout::selectWaveLength()
{
	if (! sender())
		return;
	
	QString name = sender()->name();
	
	int wl = -1;
	
	bool show;
	
	if (name.left (6) == "select") {
		show = true;
		wl = name.mid (7).toInt();
	}
	else if (name.left (8) == "deselect") {
		show = false;
		wl = name.mid (9).toInt();
	}
	
	if (wl >= 0)
		selectWaveLength (wl, show);
	
}

//SLOT: 
void TTTPositionLayout::select0ForEachX()
{
	//select all pictures with wavelength 0 for which a picture of another wavelength is selected
	
	bool selectionAtTimePoint = false;
	
	//Q3ListViewItemIterator iter (lvwImages);
	//iter = lvwImages->lastItem();
	// Get pictures
	const QStringList& pics = pictureListModel->stringList();

	
	// No items available?
	if (pics.size() == 0)
		return;


	
	//int timepoint = FileInfoArray::CalcTimePointFromFilename (*iter);
	int timepoint = -1;
	
	QString d = "";
	
	//loop is running backwards for alphabetical order of wavelengths
	for(int i = pics.size() - 1; i >= 0; --i) {
		if(!lvwImages->isRowHidden(i)) {
			d = pics[i];

			if (FileInfoArray::CalcTimePointFromFilename (d) != timepoint && timepoint != -1)
				selectionAtTimePoint = false;

			timepoint = FileInfoArray::CalcTimePointFromFilename (d);

			selectionAtTimePoint = selectionAtTimePoint || pictureListModel->isItemChecked(i);

			if (selectionAtTimePoint && (FileInfoArray::CalcWaveLengthFromFilename (d) == 0)) {
				pictureListModel->setItemChecked(i);

				selectionAtTimePoint = false;
			}
		}
	}
	
	displayInfoAboutSelectedPics();
}


//NOTE: for this function, SelectionHandler() can not be used 
//		since it cannot take parameters (it is a SLOT)
void TTTPositionLayout::selectWaveLength (int _waveLength, bool _select)
{
	//loop through all items, selecting the desired ones
	
	//Q3ListViewItemIterator iter (lvwImages);

	const QStringList& pics = pictureListModel->stringList();
	
	
	for(int i = 0; i < pics.size(); ++i) {
		const QString& d = pics[i];
		
		//note: contains only the short name!
		if (!lvwImages->isRowHidden(i) && FileInfoArray::CalcWaveLengthFromFilename (d) == _waveLength) {
		
			//( ( Q3CheckListItem* )iter.current() )->setOn ( (_select) );
			pictureListModel->setItemChecked(i, _select);
		}
	} 
	
	displayInfoAboutSelectedPics();
}

//SLOT: selects the desired images 
void TTTPositionLayout::selectionHandler()
{

	char Select = 0;
	short interval = 1;
	int IntervalCounter = 0;
	int FirstSelected = 0;
	int LastSelected = pictureListModel->getIndexOfLastVisibleItem() + 1;
	
	//if (LastSelected == 0)
	//	return;
	
	if (sender() == pbtSelectNone)  Select = 0;
	else if (sender() == pbtSelectAll)  Select = 1;
	else if (sender() == pbtSelectFirst)  Select = 2;
	else if (sender() == pbtSelectLast)  Select = 3;
	else if (sender() == pbtSelectInterval)  Select = 4;
	else if (sender() == pbtSelectMemLast)  Select = 5;
	else {}
		//does not uncheck, but deselect! -> not interesting
		//lvwImages->clearSelection();

	//QStringList pics = pictureListModel->stringList();
	
	
	if (Select == 2) {
		// Toggle check state of first pic
		int index = pictureListModel->getIndexOfFirstVisibleItem();
		if(index != -1)
			pictureListModel->setItemChecked(index, !pictureListModel->isItemChecked(index));

		//if (! ( ( Q3CheckListItem* )lvwImages->firstChild() )->isOn () ) {
		//	( ( Q3CheckListItem* )lvwImages->firstChild() )->setOn (true);
		//}
		
	}
	if (Select == 3) {
		// Toggle check state of last pic
		int index = pictureListModel->getIndexOfLastVisibleItem();
		if(index != -1)
			pictureListModel->setItemChecked(index, !pictureListModel->isItemChecked(index));

		//if (! ( ( Q3CheckListItem* )lvwImages->lastItem() )->isOn () ) {
		//	( ( Q3CheckListItem* )lvwImages->lastItem() )->setOn (true);
		//}
	}
	
	//for interval selection find first and last selected item
	if (Select >= 4)  {
		interval = spbSelectionInterval->value();
		
		//Q3ListViewItemIterator iter (lvwImages);
		//counter = 0;
		//while (iter.current()) {
		//	counter++;
		//	if ( ( (Q3CheckListItem*) iter.current() )->isOn() ) {
		//		FirstSelected = counter;
		//		break;
		//	}
		//	++iter;
		//}
		//iter = lvwImages->lastItem();
		//counter = lvwImages->childCount();
		//while (iter.current()) {
		//	counter--;
		//	if (counter == FirstSelected)  break;
		//	if ( ( (Q3CheckListItem*) iter.current() )->isOn() ) {
		//		LastSelected = counter;
		//		break;
		//	}
		//	--iter;
		//}

		// this is awkward: Give first/last selected variabls only if anything is selected
		int reallyLastSelected = pictureListModel->getIndexOfLastCheckedItem();
		if(reallyLastSelected != -1)
			LastSelected = reallyLastSelected;

		int reallyFirstSelected = pictureListModel->getIndexOfFirstCheckedItem();
		if(reallyFirstSelected != -1)
			FirstSelected = reallyFirstSelected;
	}
	
	int selected = 0;
	
	//loop through all items, selecting the desired ones (all, none or interval)
	if (Select <= 1 || Select >= 4) {
		//Q3ListViewItemIterator iter (lvwImages);
		const QStringList& pics = pictureListModel->stringList();


		IntervalCounter = FirstSelected;
		//while (iter.current()) {
		for(int counter = 1; counter <= pics.size(); ++counter) {

			if (counter >= FirstSelected && counter <= LastSelected && !lvwImages->isRowHidden(counter - 1)) {
				if ((Select == 5 && selected < maxMemImages) || Select != 5) {
					if (((IntervalCounter - FirstSelected) % interval == 0)) {
					
						//( ( Q3CheckListItem* )iter.current() )->setOn ( (Select > 0) );
						if(Select > 0)
							pictureListModel->setItemChecked(counter - 1, true);
						else
							pictureListModel->setItemChecked(counter - 1, false);
						
						if (Select > 0)
							selected++;
					}
					IntervalCounter++;
				}
			}
			//++iter;
		} 
	}
	
	displayInfoAboutSelectedPics();
}

//SLOT:
void TTTPositionLayout::showMovieWindow (const QString &_index)
{
	// First select the position
	if(!setPosition(_index)) {
		qWarning() << "TTTPositionLayout::showMovieWindow() Error 1 - " << _index;
		return;
	}

	// Get movie window
	TTTMovie* tmp = TTTManager::getInst().getPositionManager (_index)->frmMovie;
	if (!tmp)
		return;

	// Show movie window
	tmp->show();
	tmp->raise();
	if(tmp->isMinimized())
		tmp->showMaximized();
        //TTTManager::getInst().getPositionManager (_index)->frmMovie->initialDrawing();
	
}

//SLOT
void TTTPositionLayout::showPositionInformation (const QString &_index)
{
	//e.g. tree statistics
	if (! TTTManager::getInst().frmPositionDetails)
		return;

	// First select the position
	if(!setPosition(_index)) {
		qWarning() << "TTTPositionLayout::showMovieWindow() Error 1 - " << _index;
		return;
	}
	
	// Change cursor
	ChangeCursorObject c;

	// Show form
	TTTManager::getInst().frmPositionDetails->setCaption ("Additional information for position " + _index);
	TTTManager::getInst().frmPositionDetails->setPositionThumbnail (positionView->getThumbnail(_index));
	TTTManager::getInst().frmPositionDetails->show();
}

// //SLOT:
// void TTTPositionLayout::showPositionInformation()
// {
// 	if (! TTTManager::getInst().frmPositionDetails)
// 		return;
// 	
// 	if (! TTTManager::getInst().getCurrentPosition())
// 		return;
// 	
// 	
// }

////SLOT:
//void TTTPositionLayout::updateDisplayedPicture (PositionThumbnail *_thumbnail)
//{
//	if (! _thumbnail)
//		return;
//    if (! TTTManager::getInst().frmPositionDetails)
//            return;
//
//	//in the statistics window, the first picture of the folder is opened
//	//if this was not opened here before, it is copied from the statistics window to the thumbnail
//	
//	
//	//_thumbnail->setPicture (TTTManager::getInst().frmPositionDetails->getDisplayedImage());
//	//
//	////repaint the picture
//	//_thumbnail->resizeEvent (0);
//	
//	updatePositionDisplay();
//}

void TTTPositionLayout::setFactors (int _ocularFactor, float _tvFactor)
{
	if (! positionView)
		return;
	
	
	//activate corresponding option buttons
	
	int ocular_factor = _ocularFactor;
	
	cboOcularFactor->setCurrentText (QString ("%1x").arg (ocular_factor));
/*	switch (_ocularFactor) {
		case 5:
			optOcularFactor5->setChecked (true);
			break;
		case 10:
			optOcularFactor10->setChecked (true);
			break;
		case 20:
			optOcularFactor20->setChecked (true);
			break;
		default:
			optOcularFactor5->setChecked (true);
	}*/
	
	if (Tools::floatEquals (_tvFactor, 1.0f, 2) == 0)
		optTVFactor1->setChecked (true);
	else if (Tools::floatEquals (_tvFactor, 0.70f, 2) == 0)
		optTVFactor070->setChecked (true);
	else if (Tools::floatEquals (_tvFactor, 0.63f, 2) == 0)
		optTVFactor063->setChecked (true);
	else if (Tools::floatEquals (_tvFactor, 0.50f, 2) == 0)
		optTVFactor050->setChecked (true);
	else if (Tools::floatEquals (_tvFactor, 0.40f, 2) == 0)
		optTVFactor040->setChecked (true);
	else
		optTVFactor1->setChecked (true);
	
	// Set in TTTManager
	TTTManager::getInst().setTVFactor (_tvFactor);
	TTTManager::getInst().setOcularFactor(_ocularFactor);

	// Update position view
	positionView->updateDisplay();

	//positionView->setFactors (_ocularFactor, _tvFactor);
}

//SLOT:
//void TTTPositionLayout::setOcularFactor (int _buttonIndex)
void TTTPositionLayout::setOcularFactor (const QString &_text)
{
	if (! positionView)
		return;
	
	if (! _text.contains ("x"))
		return;
	
	//cut off the "x" before converting to int, otherwise it would be 0 all the time
	int ocular_factor = _text.left (_text.length() - 1).toInt();

	TTTManager::getInst().setOcularFactor (ocular_factor);
	positionView->updateDisplay();

	//positionView->setOcularFactor (ocular_factor);
	
/*	switch (_buttonIndex) {
		case 0:
			positionView->setOcularFactor (5);
			break;
		case 1:
			positionView->setOcularFactor (10);
			break;
		case 2:
			positionView->setOcularFactor (20);
			break;
		default:
			positionView->setOcularFactor (5);
	}*/
}

//SLOT:
void TTTPositionLayout::setTVAdapterFactor (const QString &_factor) //int _buttonIndex)
{
	if (! positionView)
		return;
	
	//if(_factor == "050") 
	//	positionView->setTVFactor (0.50);
	//else if (_factor == "063")
	//	positionView->setTVFactor (0.63);
	//else if (_factor == "1")
	//	positionView->setTVFactor (1.0);
	//else
	//	positionView->setTVFactor (1.0);

	if(_factor == "040")
		TTTManager::getInst().setTVFactor (0.40);
	else if(_factor == "050") 
		TTTManager::getInst().setTVFactor (0.50);
	else if (_factor == "063")
		TTTManager::getInst().setTVFactor (0.63);
	else if (_factor == "070")
		TTTManager::getInst().setTVFactor (0.70);
	else if (_factor == "1")
		TTTManager::getInst().setTVFactor (1.0);
	else
		TTTManager::getInst().setTVFactor (1.0);

	positionView->updateDisplay();

//	switch (_buttonIndex) {
//		case 0:
//			positionView->setTVFactor (0.63);
//			break;
//		case 1:
//			positionView->setTVFactor (1.0);
//			break;
//		default:
//			positionView->setTVFactor (1.0);
//	}
}

////SLOT:
//void TTTPositionLayout::showStatisticsWindow()
//{
//	if (! TTTManager::getInst().frmPositionStatistics)
//		return;
//	
//	TTTManager::getInst().setCurrentPosition (currentPosition);
//	TTTManager::getInst().frmPositionStatistics->show();
//}

//SLOT;
void TTTPositionLayout::setInvertedCoordinateSystem (bool _inverted)
{
	// Change
	TTTManager::getInst().setInvertedCoordinateSystem (_inverted);

	// Update position layout
	positionView->updateDisplay();
}

//SLOT:
void TTTPositionLayout::convertIncorrectCoordinates()
{

	if (QMessageBox::question (this, "tTt question", "Do you really want to correct all incorrectly inverted coordinates?\nNote that for correct coordinates this can prove fatal!", "No", "Yes") != 1)
		return;

	if (! TTTManager::getInst().coordinateSystemIsInverted())
		return;

	if (! TTTManager::getInst().USE_NEW_POSITIONS())
		return;

	StatusBar sb (0, "Reading first picture of all positions...", 1, TTTManager::getInst().getAllPositionManagers().size());
	sb.display (true);


	QHash<int, TTTPositionManager*>& allPositions = TTTManager::getInst().getAllPositionManagers();
	for (auto iter = allPositions.begin(); iter != allPositions.end(); ++iter) {

		sb.update();

		//load first w0 picture of each position


		//check if there is a picture with pattern "_t00001_w0.jpg" in the current position folder
		QString picfilename = (*iter)->getPictureDirectory() + "/" + (*iter)->getBasename (true) + "00001_w0.jpg";
		if (! QFile::exists (picfilename)) {

			//file does not exist - then really search the first one
			QVector<QString> pics = SystemInfo::listFiles ((*iter)->getPictureDirectory(), QDir::Files, "*_w0.jpg;*_w0.tif", false);
			QVector<QString>::const_iterator iterFiles = pics.constBegin();

			if (iterFiles != pics.constEnd()) {
				picfilename = *iterFiles;
			}
		}
		suffix = picfilename.right (3);


		if (! picfilename.isEmpty()) {

			//load picture

			QString index = (*iter)->positionInformation.getIndex();
			currentPosition = index;
			initCurrentPosition (false);

			/*			currentQDir.setPath (experimentQDir.path() + "/" + experimentBasename + POSITION_MARKER + index);
			currentBasename = experimentBasename + POSITION_MARKER + index;
			getImageSize();*/

			QImage img;
			img.load (picfilename);
			QRect imageRect (0, 0, img.width(), img.height());

			int waveLength = FileInfoArray::CalcWaveLengthFromFilename (picfilename);
			int tp = FileInfoArray::CalcTimePointFromFilename (picfilename);

			//iter.current()->getPictures()->initPicture (tp,
			//						waveLength,
			//						picfilename,
			//						true,
			//						imageRect);

			(*iter)->getPictures()->loadPictures (false);
		}
	}



	sb.reset ("Converting all colonies in all positions...");

	float mmpp = TATInformation::getInst()->getWavelengthInfo (0).getMicrometerPerPixel();
	int width = (int)((float)TATInformation::getInst()->getWavelengthInfo (0).getWidth() * mmpp);
	int height = (int)((float)TATInformation::getInst()->getWavelengthInfo (0).getHeight() * mmpp);

	Tree tree;
	//TTTFileHandler fileHandler;
	int firstTP = -1, lastTP = -1;
	int trackCount = 0;
	QString notConvertedColonies = "";

	for (auto iter = allPositions.begin(); iter != allPositions.end(); ++iter) {
		sb.update();

		//read all colonies in this position

		//NOTE: does not regard the user setting for the tttfile folder (old or new)
		QVector<QString> files = SystemInfo::listFiles ((*iter)->getTTTFileDirectory(), QDir::Files, "*" + TTT_FILE_SUFFIX, false);
		for (QVector<QString>::const_iterator fileIter = files.constBegin(); fileIter != files.constEnd(); ++fileIter) {

			const QString filename = (*fileIter);

			//if position in filename does not correspond to the current position, then no conversion is done, as its correctness cannot be guaranteed
			if (Tools::getPositionIndex (filename) != (*iter)->positionInformation.getIndex())
				//proceed with the next file
				continue;


			if (TTTFileHandler::readFile (filename, &tree, trackCount, firstTP, lastTP, &(*iter)->positionInformation) != TTT_READING_SUCCESS)
				continue;

			Q3IntDict<Track> tracks = tree.getAllTracks (-1);
			TTTPositionManager *tttpm = (*iter);
			TTTPositionManager *pmLeft = TTTManager::getInst().getPositionLeftOf ((*iter)->positionInformation.getIndex());
			TTTPositionManager *pmTop = TTTManager::getInst().getPositionTopOf ((*iter)->positionInformation.getIndex());

			bool stopped = false;


			//test if colony is not already converted (otherwise, a second conversion would be very bad...)
			//=============================================================================================


			for (uint i = 0; i < tracks.size(); i++) {

				if (! tracks [i])
					continue;

				// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				// 2014/02/24 OH: changed code due to change of getPositions() interface
				// (returns QHash<int, QPointF> instead of Q3ValueVector<QPair<int, QPointF> >)
				// NEW CODE NOT TESTED
				// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				//Q3ValueVector<QPair<int, QPointF> > tps = tracks [i]->getPositions();
				QHash<int, QPointF> tps = tracks [i]->getPositions();
				QList<int> trackTimePoints = tps.keys();

				//for (int j = 0; j < tps.size(); j++) {
				for (int j = 0; j < trackTimePoints.size(); j++) {
					//read absolute position from tree (still wrong)
					QPointF pos = tps[trackTimePoints [j]];

					if (pos.x() == -1)
						continue;

					//check (for the first trackpoint only) if it is already in the "allowed" range
					//if yes, then it can be assumed that the colony is already converted
					if ((pos.x() <= tttpm->positionInformation.getLeft()) &
						(pos.y() <= tttpm->positionInformation.getTop())) {
							//colony is already correct => a conversion would be malicious...

							stopped = true;
							notConvertedColonies.append (QString ("Already converted: %1\n").arg (filename));
					}

					break;
				}

				break;
			}

			if (stopped)
				//take next file
				continue;


			stopped = false;
			//std::cout << filename << std::endl;


			//automatically backup old file
			//=============================
			QString backupPath = SystemInfo::homeDirectory() + TTTFILES_FOLDER + "/" + (*iter)->getBasename();
			SystemInfo::checkNcreateDirectory (backupPath, true, true);
			/*			QDir backup (backupPath);
			//create subdirectory for current experiment (if it not yet exists)
			bool folderexists = backup.cd ((*iter)->getBasename());
			if (! folderexists) {
			backup.mkdir ((*iter)->getBasename());
			backup.cd ((*iter)->getBasename());
			}*/
			backupPath += "/" + filename.mid (filename.findRev ('/') + 1) + "_invSysBackup";


			if (QFile::exists (backupPath)) {
				//if backup file already exists, assign a random number to avoid overwriting the correct files
				backupPath += QString ("%1").arg (rand());
			}

			TTTFileHandler::saveFile (backupPath, &tree, (*iter)->getFirstTimePoint(), (*iter)->getLastTimePoint());



			//convert file
			//============

			for (uint i = 0; i < tracks.size(); i++) {
				if (! tracks [i])
					continue;

				// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				// 2014/02/24 OH: changed code due to change of getPositions() interface
				// (returns QHash<int, QPointF> instead of Q3ValueVector<QPair<int, QPointF> >)
				// NEW CODE NOT TESTED
				// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				//Q3ValueVector<QPair<int, QPointF> > tps = tracks [i]->getPositions();
				QHash<int, QPointF> tps = tracks [i]->getPositions();
				QList<int> trackTimePoints = tps.keys();

				for (int j = 0; j < trackTimePoints.size(); j++) {
					//            for (int j = 0; j < tps.size(); j++) {
					//read absolute position from tree (still wrong)
					int curTimePoint = trackTimePoints[j];
					QPointF pos = tps [curTimePoint];

					if (pos.x() == -1)
						continue;

					//find out the position in which this trackpoint was set
					//======================================================
					//this is very important as the coordinate origin is needed

					bool left = false;
					bool top = false;
					bool right = false;
					bool bottom = false;

					if (pmLeft) {
						if ((pos.x() >= pmLeft->positionInformation.getLeft()) & 
							(pos.x() - tttpm->positionInformation.getLeft() > width))
							left = true;
					}
					else {
						if (pos.x() < tttpm->positionInformation.getLeft())
							right = true;
					}

					if (pmTop) {
						if ((pos.y() >= pmTop->positionInformation.getTop()) & 
							(pos.y() - tttpm->positionInformation.getTop() > height))
							top = true;

					}
					else {
						if (pos.y() < tttpm->positionInformation.getTop())
							bottom = true;
					}

					//_direction: 0 = topleft, 1 = top, 2 = topright, 3 = left, 4 = right, 5 = bottomleft, 6 = bottom, 7 = bottomright
					int dir = -1;
					if (left) {
						if (top)
							dir = 0;
						else if (bottom)
							dir = 5;
						else
							dir = 3;
					}
					else if (right) {
						if (top)
							dir = 2;
						else if (bottom)
							dir = 7;
						else
							dir = 4;
					}
					else {
						if (top)
							dir = 1;
						else if (bottom)
							dir = 6;
					}

					if (dir >= 0) {
						tttpm = TTTManager::getInst().getPositionSomewhereOf (tttpm->positionInformation.getIndex(), dir);

						if (! tttpm) {
							//for what reason ever, this colony cannot be converted
							stopped = true;
							notConvertedColonies.append (QString ("NOT converted (tttpm = 0, dir = %1): %2\n").arg (dir).arg (filename));
							break;
						}

						pmLeft = TTTManager::getInst().getPositionLeftOf (tttpm->positionInformation.getIndex());
						pmTop = TTTManager::getInst().getPositionTopOf (tttpm->positionInformation.getIndex());

					}


					//translate coordinates from wrong to right
					//=========================================

					pos = tttpm->getDisplays().at (0).calcTransformedCoords (pos, &tttpm->positionInformation, false, true);

					pos = tttpm->getDisplays().at (0).calcAbsCoords (pos, &tttpm->positionInformation);


					tracks [i]->setTPXY (curTimePoint, pos.x(), pos.y());
				}

				if (stopped)
					break;
			}

			if (! stopped) {
				//save converted file at original place
				TTTFileHandler::saveFile (filename, &tree, (*iter)->getFirstTimePoint(), (*iter)->getLastTimePoint());
			}

		}

	}

	sb.display (false);

	SystemInfo::report (notConvertedColonies);
	//std::cout << notConvertedColonies << std::endl;

	TTTManager::getInst().resetBasePosition();
}

//SLOT:
void TTTPositionLayout::showPositionXMLDisplay()
{
	if (! TTTManager::getInst().frmPositionXMLDisplay)
		return;
	
	TTTManager::getInst().frmPositionXMLDisplay->init();
	
	if (TTTManager::getInst().getCurrentPositionManager()) {
		int curIndex = TTTManager::getInst().getCurrentPosition();
		TTTManager::getInst().frmPositionXMLDisplay->setPositionIndex (curIndex);
	}
	
	TTTManager::getInst().frmPositionXMLDisplay->loadTATXML();
	TTTManager::getInst().frmPositionXMLDisplay->show();
}

//SLOT:
void TTTPositionLayout::showStyleEditor()
{
	TTTManager::getInst().showCustomizeDialog();
}

//SLOT:
void TTTPositionLayout::updatePositionDisplay()
{
	positionView->updateDisplay();
}

//SLOT:
void TTTPositionLayout::exportAllTTTFiles()
{
	//convert all ttt files of the current experiment to the following format (each ttt file separately)
	//cell nr;timepoint;position index;X;Y;bg X;bg Y;wl 1-5;stop reason
	//the filename is equal to the ttt file, except the ending .ttt -> .ttt_simple
	//new format (Bernhard, 2009/02/12): added "stop reason" at the end  (note: will be the same for all lines of one cell)
	
	
	QString headline = QString ("tTt Version %1").arg (internalVersion);		//Bernhard, 2009/12/16: to identify the ttt version
	
	QString format = "cell nr;timepoint;absolute time;position index;X;Y;background X;background Y;";
	for (int i = 1; i <= MAX_WAVE_LENGTH; i++){ // ----------------- Konstantin ----------------
		format += QString("wavelength_%1;").arg(i);
	}
	format += "stop reason";
	
	StatusBar sb (0, "Exporting ttt files of all positions...", 1, TTTManager::getInst().getAllPositionManagers().size());
	sb.display (true);
	
	Tree tree;
	//TTTFileHandler fileHandler;
	int firstTP = -1, lastTP = -1;
	int trackCount = 0;
	QString absoluteTime = "";
	
	//iterate over all positions
	QHash<int, TTTPositionManager*>& allPositions = TTTManager::getInst().getAllPositionManagers();
	for (auto iter = allPositions.begin(); iter != allPositions.end(); ++iter) {
		
		sb.update();
		
		//iterate over all ttt files in the current position
		
/*		QValueVector<QString> summed_files (17);
		
		//read from new directory
		QValueVector<QString> files = SystemInfo::listFiles ((*iter)->getTTTFileDirectory(), QDir::Files, "*" + TTT_FILE_SUFFIX, false);
		for (QValueVector<QString>::Iterator fileIter = files.begin(); fileIter  != files.end(); ++fileIter) {
			QString filename = (*fileIter);
			if (summed_files.count() - 1 >= summed_files.capacity())
				summed_files.resize (summed_files.size() * 2);
			summed_files.append (filename);
		}
		
		//read from old directory
		files = SystemInfo::listFiles ((*iter)->getTTTFileDirectory (false, true), QDir::Files, "*" + TTT_FILE_SUFFIX, false);
		for (QValueVector<QString>::Iterator fileIter = files.begin(); fileIter  != files.end(); ++fileIter) {
			QString filename = (*fileIter);
			if (summed_files.count() - 1 >= summed_files.capacity())
				summed_files.resize (summed_files.size() * 2);
			summed_files.append (filename);
		}
		*/
		
		QStringList summed_files = (*iter)->getAllTTTFiles();
		for (QStringList::Iterator fileIter = summed_files.begin(); fileIter != summed_files.end(); ++fileIter) {
			
			QString filename = (*fileIter);
			QString filename_simple = filename.left (filename.length() - 4) + TTT_FILE_SUFFIX + "_simple";
			QString convertedFile = headline + "\n";
			convertedFile += format + "\n";
			
            if (TTTFileHandler::readFile (filename, &tree, trackCount, firstTP, lastTP, &(*iter)->positionInformation) != TTT_READING_SUCCESS)
                    continue;
			
			Q3IntDict<Track> tracks = tree.getAllTracks (-1);
			
			TTTPositionManager *tttpm = (*iter);
			
			//iterate over all tracks in the current ttt file
			for (uint i = 0; i < tracks.size(); i++) {
				if (! tracks [i])
					continue;
				
			// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			// 2014/02/24 OH: changed code due to change of getPositions() interface
			// (returns QHash<int, QPointF> instead of Q3ValueVector<QPair<int, QPointF> >)
			// NEW CODE NOT TESTED
			// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


				//receive tuples with (timepoint, coordinates) for each trackpoint; once for foreground, once for background
				//Q3ValueVector<QPair<int, QPointF> > tps = tracks [i]->getPositions();
				//Q3ValueVector<QPair<int, QPointF> > tpsBg = tracks [i]->getPositions (-1, -1, true);

				//for (int j = 0; j < tps.size(); j++) {
				for(int tp = tracks[i]->getFirstTimePoint(); tp <= tracks[i]->getLastTimePoint(); ++tp) {	
					TrackPoint* trackPoint = tracks[i]->getTrackPointByTimePoint(tp);
					if(!trackPoint)
						continue;

					//QPointF pos = tps [j].second;
					//QPointF posBg = tpsBg [j].second;
					QPointF pos = trackPoint->point();
					QPointF posBg = trackPoint->backgroundPoint();
					
					if (pos.x() == -1)
						continue;
					
					//new version (Bernhard, 2009/11/23):
					//the position where the trackpoint was set is stored, so we can use it directly
					//yet, for older ones, this is not stored, so we have to calculate the position
					// NOTE: set as comment for old movies without real positions
					
					//position_key = tracks [i]->getTrackPoint (tps [j].first).Position;
					//position_key = trackPoint->Position;
					int posKey = trackPoint->getPositionNumber();
					//if (position_key == "" || position_key == "000") {
					if(posKey < 1) {
						if (! TTTManager::getInst().pointIsInPosition (tttpm, pos)) {
							tttpm = TTTManager::getInst().getPositionAt (pos.x(), pos.y());
						}
					}
					else {
						tttpm = TTTManager::getInst().getPositionManager (posKey);
					}
					
					
					if (tttpm) {
						
						//get absolute time
						
						if (! tttpm->getFiles()) {
							//the log file is not yet loaded for this position - do it now
							
							int lastExpTP = -1;
							int firstExpTP = -1;
							//note: the suffix "jpg" is assumed as we currently do not know the correct one without reading the files - which would take too much time
							if (! Tools::readLogFile (tttpm, lastExpTP, firstExpTP, /*"jpg",*/ false))
								//log file could not be read - continue with next position
								continue;
							
						}
						
						//absoluteTime = tttpm->getFiles()->getRealTime (tps [j].first, 1, -1, true).toString ("yyyy/MM/dd hh:mm:ss");
						absoluteTime = tttpm->getFiles()->getRealTime (tp, 1, -1, true).toString ("yyyy/MM/dd hh:mm:ss");
						
						
						
                        QPointF transCoords = tttpm->getDisplays().at (0).calcTransformedCoords (pos, &tttpm->positionInformation);
                        QPointF transBgCoords = tttpm->getDisplays().at (0).calcTransformedCoords (posBg, &tttpm->positionInformation);
						
						convertedFile += QString ("%1;").arg (tracks [i]->getNumber());
						//convertedFile += QString ("%1;").arg (tps [j].first);
						convertedFile += QString ("%1;").arg (tp);
						convertedFile += QString ("%1;").arg (absoluteTime);
						convertedFile += QString ("%1;").arg (tttpm->positionInformation.getIndex());
//std::cout<<filename<<"   "<<tttpm->positionInformation.getIndex()<<std::endl;
						convertedFile += QString ("%1;").arg (transCoords.x());
						convertedFile += QString ("%1;").arg (transCoords.y());
						convertedFile += QString ("%1;").arg (transBgCoords.x());
						convertedFile += QString ("%1;").arg (transBgCoords.y());
						for (int wl = 1; wl <= MAX_WAVE_LENGTH; wl++){  // ----------------- Konstantin ----------------
							//if (tracks [i]->getRefTrackPoint(tps [j].first)->getValue(OPT_WAVELENGTH_X, wl) > 0){
							if (tracks [i]->getRefTrackPoint(tp)->getValue(OPT_WAVELENGTH_X, wl) > 0){
								convertedFile += QString ("1;");
							}
							else {
								convertedFile += QString ("0;");
							}
						}
						convertedFile += QString ("%1;").arg (tracks [i]->getStopReason());
						convertedFile += "\n";
						
					}
					else {
						SystemInfo::report (QString ("tttpm %1 not found").arg (posKey), false, true);
					}
				}
			}
			
			//save simple ttt file
			Tools::writeTextFile (filename_simple, convertedFile);
			
		}
		
	}
	
	sb.display (false);
}

//SLOT:
void TTTPositionLayout::refreshLayout()
{
	//refresh tree count
	QHash<int, TTTPositionManager*>& allPositions = TTTManager::getInst().getAllPositionManagers();
	for (auto iter = allPositions.begin(); iter != allPositions.end(); ++iter) {
		(*iter)->getTreeCount (true);
	}
	
	updatePositionDisplay();
}

//SLOT:
void TTTPositionLayout::mergeTrees()
{
	if (! TTTManager::getInst().frmTreeMerging)
		return;
	
	if (! TTTManager::getInst().frmTreeMerging->isVisible()) {
		
		TTTManager::getInst().frmTreeMerging->show();
	}
}

//SLOT:
void TTTPositionLayout::createSnapshotsOfAllTrees()
{
	//iterate over all positions and export an image of the complete view of all trees for each position
	
	// Display progress bar
	QProgressDialog progessBar("Exporting snapshots..", "Cancel", 0, TTTManager::getInst().getAllPositionManagers().count(), this);
	progessBar.setWindowModality(Qt::ApplicationModal);
	progessBar.setMinimumDuration(0);
	progessBar.setValue(0);

	//StatusBar sb (0, "Exporting snapshots...", 1, TTTManager::getInst().getAllPositionManagers().count());
	//sb.display (true);
	
	QString destination;
	int counter = 0;
	QHash<int, TTTPositionManager*>& allPositions = TTTManager::getInst().getAllPositionManagers();
	for (auto iter = allPositions.begin(); iter != allPositions.end(); ++iter) {
		
		//sb.update();
		destination = Tools::exportAllTreesOfPositionToImages ((*iter), false);
		progessBar.setValue(++counter);
	}
	
	Tools::displayMessageBoxWithOpenFolder("Export completed.", "Note", destination, false, this);

	//QMessageBox::information (0, "Saving storage space", "Please make sure to delete your exported trees after (ab)using them\nbecause they consume quite a vast amount of storage on the net.", "Surely I will...");
	
	//sb.display (false);
}
//
////SLOT:
//void TTTPositionLayout::convertDirksTrees()
//{
//
//	//int s = pictureListModel->numPictures();
//
//	//for(int i = 0, j = 0; i < 400 && j < s; j++) {
//	//	if(!lvwImages->isRowHidden(j)) {
//	//		pictureListModel->setItemChecked(j);
//	//		i++;
//	//	}
//	//}
//
//	//return;
//
//	// Get position manager
//	TTTPositionManager *tttpm = TTTManager::getInst().getPositionManager (currentPosition);
//	if (!tttpm) {
//		QMessageBox::information(this, "No position selected.", "Please select a position to convert its trees.");
//		return;
//	}
//
//	// Verify user knows what he is doing
//	if(QMessageBox::warning(this, "Are you sure", "This will convert all tree files of the selected position. Continue?", QMessageBox::Yes, QMessageBox::Cancel) != QMessageBox::Yes)
//		return;
//
//	// Statistics
//	int treeCountSuccess = 0,
//		treeReadErrorCount = 0,
//		treeWriteErrorCount = 0,
//		trackCount = 0, 
//		trackPointCount = 0,
//		trackPointsConverted = 0; 
//
//	// Get ttt files that belong to current position
//        QVector<QString> fileNames = SystemInfo::listFiles(tttpm->getTTTFileDirectory(), QDir::Files, "*" + TTT_FILE_SUFFIX, false);
//	
//	// Convert trees in fileNames vector
//        for(QVector<QString>::iterator itFileName = fileNames.begin(); itFileName != fileNames.end(); ++itFileName) {
//		QString& currentFileName = *itFileName;
//		
//		// Read file
//		Tree currentTree;
//		int numTracks, firstTimePoint, lastTimePoint;
//		PositionInformation positionInformation;
//		
//                if (TTTFileHandler::readFile(currentFileName, &currentTree, numTracks, firstTimePoint, lastTimePoint, &positionInformation) != TTT_READING_SUCCESS) {
//			// Ask user if he wants to continue conversion despite errors
//			QString errorMessage = QString("An error occurred: Could not read ttt file \"%1\".\n\nAbort conversion?").arg(currentFileName);
//			if(QMessageBox::warning(this, "Error", errorMessage, QMessageBox::Yes, QMessageBox::No) == QMessageBox::Yes)
//				return;
//			
//			// Count invalid tree and continue with next tree
//			++treeReadErrorCount;
// 			continue;
//		}
//
//		// We have a valid tree
//		++treeCountSuccess;
//
//		// Get all tracks of current tree
//		Q3IntDict<Track> tracks = currentTree.getAllTracks();
//
//		// Iterate over tracks
//		Q3IntDictIterator<Track> itTrack(tracks);
//		for(; itTrack.current(); ++itTrack, ++trackCount) {
//			// Iterate over Trackpoints of current track
//			const Q3PtrVector<TrackPoint> trackPoints = (itTrack.current())->getAllTrackpoints();
//
//			for(unsigned int i = 0; i < trackPoints.size(); ++i) {
//				// Make sure we have a valid trackpoint
//				if(TrackPoint* currentTrackPoint = trackPoints[i]) {
//					++trackPointCount;
//
//					// Change trackpoint
//					if(currentTrackPoint->NonAdherent && currentTrackPoint->FreeFloating) {
//						currentTrackPoint->NonAdherent = false;
//						currentTrackPoint->FreeFloating = false;
//
//						++trackPointsConverted;
//					}
//					else if(!currentTrackPoint->NonAdherent && !currentTrackPoint->FreeFloating) {
//						currentTrackPoint->NonAdherent = true;
//						currentTrackPoint->FreeFloating = true;
//
//						++trackPointsConverted;
//					}
//				}
//			}
//		}
//
//		// Save current tree
//		if(!TTTFileHandler::saveFile(currentFileName + "_CONV", &currentTree, firstTimePoint, lastTimePoint)) {
//			// Get last error
//			char* lastSystemError = strerror(errno);	
//			
//			if(!lastSystemError)
//				lastSystemError = const_cast<char *>(QString ("(nothing)").ascii());
//
//			// Ask user if he wants to continue conversion despite errors
//			QString errorMessage = QString("An error occurred: Could not write ttt file \"%1\".\nSystem reports: %2\n\nAbort conversion?").arg(currentFileName).arg(lastSystemError);
//			if(QMessageBox::warning(this, "Error", errorMessage, QMessageBox::Yes, QMessageBox::No) == QMessageBox::Yes)
//				return;
//			
//			// Count write error
//			++treeWriteErrorCount;
//		}
//	}
//	
//	// Display status message
//	QString msg = QString("Conversion completed successfully.\n\n(%1 trees, %2 trees could not be read, %3 trees could not be saved, %4 tracks,"
//				" %5 track points of %6 track points converted)")
//			.arg(treeCountSuccess).arg(treeReadErrorCount).arg(treeWriteErrorCount).arg(trackCount).arg(trackPointCount).arg(trackPointsConverted);
//
//	QMessageBox::information(this, "Conversion completed", msg);
//}

int TTTPositionLayout::markSelectedPicturesForLoading()
{
	TTTPositionManager *tttpm = TTTManager::getInst().getPositionManager (currentPosition);

	//for (int i = 0; i <= MAX_WAVE_LENGTH; i++) {
	//	if (! tttpm->positionInformation.ImageRect [i].isNull())
	//		tttpm->getDisplays().setPictureSize (i, tttpm->positionInformation.ImageRect [i].width(), tttpm->positionInformation.ImageRect [i].height());
	//	else
	//		tttpm->getDisplays().setPictureSize (i, tttpm->positionInformation.ImageRect [0].width(), tttpm->positionInformation.ImageRect [0].height());
	//}

	//////Init loading region (Does nothing currently, not used)
	////RectBox loadingRegion = tttpm->positionInformation.LoadingRegion;
	////tttpm->getDisplays().setLoadingRegion (-1, loadingRegion.left(), loadingRegion.top(), loadingRegion.width(), loadingRegion.height());

	////// Get default picture size
	////QRect imageRect = tttpm->positionInformation.ImageRect [0];

	//// Return value
	int numMarkedPics = 0;

	// Iterate over all pictures
	const QStringList& picNames = pictureListModel->stringList();
	//int currentTp = 0;
	//int currentWL = 0;
	for(int curRow = 0; curRow < picNames.size(); ++curRow) {
	//	// Get image name
		QString d = picNames[curRow];
		d = tttpm->getBasename (true)  + d;



		// If image should be marked for loading
		if(pictureListModel->isItemChecked(curRow)) {

			// Get wl and tp from image name
			int currentWL = FileInfoArray::CalcWaveLengthFromFilename (d);
			int currentTp = FileInfoArray::CalcTimePointFromFilename (d);
			int currentZIndex = FileInfoArray::CalcZIndexFromFileName(d);
			if(currentZIndex < 0)
				currentZIndex = 1;

			tttpm->getPictures()->setLoading(currentTp, currentZIndex, currentWL, true);

			++numMarkedPics;
		}

	//	//// Create picture container for every picture
	//	//tttpm->getPictures()->initPicture (currentTp, currentWL, tttpm->getPictureDirectory() + "/" + d, markForLoading, imageRect);
	}

	return numMarkedPics;
}

bool TTTPositionLayout::markPicturesLoadedForCurrentPosition(bool _loaded)
{
	TTTPositionManager *tttpm = TTTManager::getInst().getPositionManager (currentPosition);
	if (tttpm/* && currentThumbnail*/) {
		if (!_loaded || tttpm->getPictures()->getLoadedPictures() > 0) {
			//currentThumbnail->markPicturesLoaded (_loaded);
			//return true;

			return positionView->markPicturesLoaded(currentPosition, _loaded);
		}
	}

	return false;
}

void TTTPositionLayout::unloadAllPictures()
{
	//unload all pictures
	QHash<int, TTTPositionManager*>& allPositions = TTTManager::getInst().getAllPositionManagers();
	for (auto iter = allPositions.begin(); iter != allPositions.end(); ++iter) {
		// Close movie window
		if ((*iter)->frmMovie)
			(*iter)->frmMovie->close();

		//unload all pictures
		if ((*iter)->getPictures())
			(*iter)->getPictures()->unloadAllPictures(/*false*/);

		//delete all "P" signs in the position layout as there are no loaded picture anymore
		//NOTE: creates a SIGSEV signal
		if ((*iter)->posThumbnailInTTTPosLayout)
			(*iter)->posThumbnailInTTTPosLayout->markPicturesLoaded (false);
	}
}

void TTTPositionLayout::loadPicsFromMultiplePositions()
{
	TTTLoadPicsFromMorePositions dialog(this);

	dialog.exec();
}

PositionThumbnail* TTTPositionLayout::getThumbnail( const QString& _index ) const
{
	if(positionView)
		return positionView->getThumbnail(_index);

	return 0;
}

void TTTPositionLayout::zoomChanged( float _scale )
{
	QString zoom = QString("Zoom: %1%").arg(_scale * 100, 0, 'f', 2);
	lblZoom->setText(zoom);
}

void TTTPositionLayout::styleChosen( StyleSheet &_ss )
{
	// Just refresh layout
	refreshLayout();
}

void TTTPositionLayout::showLogFileConverterWindow()
{
	// Warn if currently a position is selected
	if(!currentPosition.isEmpty())
		QMessageBox::information(this, "Note", "It is not advisable to convert log files after having selected a position in tTt.\n\nPlease restart tTt after the conversion to avoid problems, if you still want to convert logfiles now.");

	// Show frmLogFileConverter
	TTTManager::getInst().frmLogFileConverter->show();
}

void TTTPositionLayout::checkIfLogFilesExist() 
{
	// Get any available positionmanager whose folder exists
	TTTPositionManager* tttpm = 0;
	QString dir;
	QHash<int, TTTPositionManager*>& allPositions = TTTManager::getInst().getAllPositionManagers();
	for (auto iter = allPositions.begin(); iter != allPositions.end(); ++iter) {
		TTTPositionManager* tmp = (*iter);
		if(tmp && tmp->isAvailable()) {
			dir = tmp->getPictureDirectory();
			if(!dir.isEmpty() && QDir(dir).exists()) {
				tttpm = tmp;
				break;
			}
		}
	}

	// None found
	if(!tttpm)
		return;

	// Check if log file exists
	QString baseName = tttpm->getBasename();
	if(baseName.isEmpty())
		return;
	if(dir.right(1) != "/")
		dir.append('/');

	// Log file name
	QString logFileName = dir + baseName + ".log";
	if(!QFile(logFileName).exists()) {
		QString msg = QString("Position '%1' appears to contain no log file.\n\nOpen LogFileConverter?").arg(tttpm->positionInformation.getIndex());
		if(QMessageBox::question(this, "Log file conversion", msg, QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes)
			showLogFileConverterWindow();
	}

}

void TTTPositionLayout::reInitializePositionDisplay()
{
	positionView->initialize(chkDrawFrame->isChecked(), true, true);
}

void TTTPositionLayout::posLayoutMovieExport()
{
	// Create exporter
	if(!movieExporter)
		movieExporter = new TTTPositionLayoutMovieExport();

	// Show exporter
	movieExporter->show();
}

void TTTPositionLayout::updateThumbNails()
{
	// Wait cursor
	ChangeCursorObject cc;

	// Set timepoint and wl
	positionView->setThumbnailsTimePoint(spbTimePoint->value());
	positionView->setThumbnailsWavelength(spbWaveLength->value());

	// Remember zoom and position (center)
	float oldScaling = positionView->getCurrentScaling();
	QPointF curCenter = positionView->mapToScene(positionView->width() / 2, positionView->height() / 2);

	// Apply changes
	positionView->initialize(chkDrawFrame->isChecked(), true, true);

	// Re-apply zoom and position
	float newScaling = positionView->getCurrentScaling();
	if(newScaling > 0 && oldScaling > 0)
		positionView->scaleView(oldScaling / newScaling);
	if(!curCenter.isNull())
		positionView->centerOn(curCenter);
}

//void TTTPositionLayout::showBackgroundCorrectionWindow()
//{
//	// Show background correction window 
//	if(!frmBackgroundCorrection)
//		frmBackgroundCorrection = new TTTBackgroundCorrection();
//	frmBackgroundCorrection->show();
//}

void TTTPositionLayout::exportCellImagePatches()
{
	// Show background correction window 
	if(!frmExportImagePatches)
		frmExportImagePatches = new TTTExportFluorescencePatches();
	frmExportImagePatches->show();
}

void TTTPositionLayout::exportPosLayout()
{
	// Determine file name
	QString fileName = QDir::homePath() + "/positionLayout.png";

	// Create pixmap
	QPixmap pix;
	float width = positionView->scene()->width();
	float height = positionView->scene()->height();
	if(width > 13000.0f) {
		float f = 13000.0f / width;
		width *= f;
		height *= f;
	}
	pix.resize(width, height);

	// Render scene
	QPainter p(&pix);
	positionView->scene()->render(&p);

	// Save
	QImage img = pix.toImage();
	if(!img.isNull()) { 
		if(img.save(fileName))
			Tools::displayMessageBoxWithOpenFolder("Done", "Done", fileName, true, this);
		else
			QMessageBox::critical(this, "Error", "Export failed.");
	}
}






//bool TTTPositionLayout::initCurrentPositionAndImages()
//{
//	// First init position
//	if(!initCurrentPosition(false))
//		return false;
//
//	// Then init images
//	if(!initImagesOfCurrentPosition(false))
//		return false;
//
//	return true;
//}
