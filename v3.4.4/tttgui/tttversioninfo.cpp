/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "tttversioninfo.h"

// Project includes
#include "tttsplashscreen.h"
#include "tttdata/userinfo.h"

// Qt includes
#include <QFile>
#include <QTextStream>



TTTVersionInfo::TTTVersionInfo(QWidget* _parent, bool _autoOpenMode) : QDialog(_parent)
{
	ui.setupUi(this);

	// Read changelog
	QFile changelog(":/ttt/ttt_changelog.txt");
	if(!changelog.open(QIODevice::ReadOnly))
		return;

	// Read changelog from resource
	QTextStream in(&changelog);
	const QString changelogString = changelog.readAll();

	// Add to text field
	ui.txeChangeLog->setText(changelogString);

	// Hide option and cancel if this window was not opened automatically
	if(!_autoOpenMode) {
		ui.chkHideForThisVersion->setVisible(false);
		ui.pbtCancel->setVisible(false);
	}

	// Set fancy title
	QString title = "What's new in tTt " + Version;
	setCaption(title);

	// Remove context help  button
	setWindowFlags( (windowFlags() | Qt::CustomizeWindowHint) & ~Qt::WindowContextHelpButtonHint );
}

void TTTVersionInfo::accept()
{
	// Make this dialog not show again if desired (until next version)
	if(ui.chkHideForThisVersion->isVisible() && ui.chkHideForThisVersion->isChecked()) {
		UserInfo::getInst().getRefStyleSheet().setLastTTTChangelogVersion(internalVersion);
	}

	QDialog::accept();
}
