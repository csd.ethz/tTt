/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef treewindowmenu_h__
#define treewindowmenu_h__

// Qt includes
#include <QMenuBar>

// Forward declarations
class TTTAutoTrackingTreeWindow;


/**
 * Menubar for TTTAutoTrackingTreeWindow
 */
class TreeWindowMenu : public QMenuBar {
	
	Q_OBJECT

public:

	/**
	 * Constructor
	 */
	TreeWindowMenu(TTTAutoTrackingTreeWindow* _parent);

private:

	/**
	 * Create menus
	 */
	void init();

	// Associated tree window
	TTTAutoTrackingTreeWindow* treeWindow;

	// File menu
	QMenu *fileMenu;
	QMenu *fileSubMenuLoadTrees;
	QMenu *fileSubMenuLoadPictures;

	// Autotracking menu
	QMenu* autotrackingMenu;
	QMenu* autotrackingSubMenuAssistant;

	// Tools menu
	QMenu *toolsMenu;

	// Help menu
	QMenu *helpMenu;

	// File actions
	QAction* actFile_OPEN_TREE;
	QAction* actFile_NEW_TREE;
	QAction* actFile_SAVE_CURRENT_TREE;
	QAction* actFile_SAVE_CURRENT_TREE_AS;
	QAction* actFile_CLOSE_CURRENT_TREE;
	QAction* actFile_CLOSE_ALL_TREES;
	//QAction *actFile_LOADING_PICTURES_TOGGLE_SELECTION;
	//QAction *actFile_LOADING_PICTURES_SELECT;
	//QAction *actFile_LOADING_PICTURES_DESELECT;
	QAction *actFile_LOADING_PICTURES_LOAD;
	QAction *actFile_LOADING_PICTURES_CURTREE;
	QAction *actFile_LOADING_PICTURES_UNLOAD;

	// Autotracking actions
	QAction *actAT_CONNECT_FRAGMENTS;
	QAction *actAT_ASSISTANT_PREV;
	QAction *actAT_ASSISTANT_NEXT;
	QAction *actAT_ASSISTANT_ACCEPT;

	// Tools actions
	QAction *actTools_EXPORT_VIEW;

	// Help actions
	QAction* actHelp_HELP;


	// Friend classes
	friend TTTAutoTrackingTreeWindow;
};

#endif // treewindowmenu_h__
