/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ttttracksoverview.h"

#include "qwt_plot.h"    
#include "tttbackend/tttmanager.h"
#include "tttdata/userinfo.h"
//Added by qt3to4:
#include <Q3ValueList>

TTTTracksOverview::TTTTracksOverview (QWidget *parent, const char *name)
    :QWidget(parent, name)
{
        setupUi (this);

	connect ( pbtClose, SIGNAL (clicked()), this, SLOT (close()));
	connect ( pbtShowTrackingData, SIGNAL (clicked()), this, SLOT (showTrackingData()));
	connect ( pbtShowFluorescence, SIGNAL (clicked()), this, SLOT (showFluorescence()));
	connect ( pbtJumpToEvent, SIGNAL (clicked()), this, SLOT (jumpToEvent()));
	
	connect ( cboWavelength, SIGNAL (activated (const QString &)), this, SLOT (setWavelength (const QString &)));
	
	connect ( tabData, SIGNAL (selectionChanged()), this, SLOT (copyTableToClipboard()));
	
	//pbtJumpToEvent->setEnabled (false);
	tttpm = 0;
	/* TODO
	barPointers.setAutoDelete(true);   // --------------- Konstantin ----------------
	plotPointers.setAutoDelete(true);  // --------------- Konstantin ----------------
	*/

	// Add minimize/maximize buttons, make always on top
	setWindowFlags( windowFlags() | Qt::CustomizeWindowHint | Qt::WindowMinMaxButtonsHint | Qt::WindowStaysOnTopHint);
}

//SLOT:
void TTTTracksOverview::copyTableToClipboard()
{
	QString selected_text = "";
	
	Q3TableSelection ts = tabData->selection (tabData->currentSelection());
	
	for (int row = ts.topRow(); row <= ts.bottomRow(); row++) {
		for (int col = ts.leftCol(); col < ts.rightCol(); col++) {
			selected_text += tabData->text (row, col) + ",";
		}
		selected_text += tabData->text (row, ts.rightCol());
		
		selected_text += "\n";
	}
	
	QApplication::clipboard()->setText(selected_text);
	
}

//SLOT:
void TTTTracksOverview::showTrackingData()
{
	dispmode = TTO_DISPLAY_MODE_TRACKING_DATA;
	
	//clear old data
	tabData->setNumRows (0);
	
	//fill with random data
	tabData->setNumCols (5);
	
	//note: line number column is added automatically
	int rows = tabData->numRows();
	
	tabData->setReadOnly (false);
	
	
	
	int col = 0;
	tabData->horizontalHeader()->setLabel (col, "Numbers");
	tabData->horizontalHeader()->setLabel (++col, "Timepoint");
	tabData->horizontalHeader()->setLabel (++col, "Cell 1");
	tabData->horizontalHeader()->setLabel (++col, "Cell 2");
	tabData->horizontalHeader()->setLabel (++col, "Bkgr 1");
	
	for (uint i = 1; i < 10; i++) {
		for (uint j = 0; j < 5; j++) {
			
			tabData->setNumRows (rows + 1);
			
			tabData->setText (rows, 0, QString ("%1").arg (i));
			
			tabData->setText (rows, 1, QString ("%1").arg (j));
			
			
			float integral1 = 5;
			float integral2 = 76 * i;
			tabData->setText (rows, 2, QString ("%1").arg (integral1));
			tabData->setText (rows, 3, QString ("%1").arg (integral2));
			
			float background1 = 10 * i;
			tabData->setText (rows, 4, QString ("%1").arg (background1));
			
			rows++;
		}
	}
	
}

//SLOT:
void TTTTracksOverview::showFluorescence()
{
	dispmode = TTO_DISPLAY_MODE_FLUORESCENCE;
	
	loadWavelengths();
	int exportwl = UserInfo::getInst().getStyleSheet().getExportWavelength();
	showFluorescence (exportwl);
}

void TTTTracksOverview::showFluorescence (int _wavelength)
{
	//display current tracks (or selection)
	//idea: rapidly find tracking errors
	//format:
	//cell1, cell2, bg1, bg2, abs.intens.1, abs.intens.2, rel (:= abs1/abs2)
	
	if (! tttpm)
		return;
	if (! TTTManager::getInst().getTree())
		return;
	
	//debug: tabData->setText (1, 1, QString ("%1").arg (_wavelength));
	
	Q3ValueVector<double> absIntVector1, absIntVector2; // --------------- Konstantin ----------------
	
	//clear old data
	tabData->setNumRows (0);
	
	tabData->setNumCols (9);
	
	//note: line number column is added automatically
	int col = 0;
	tabData->horizontalHeader()->setLabel (col, "Numbers");
	tabData->horizontalHeader()->setLabel (++col, "Timepoint");
	tabData->horizontalHeader()->setLabel (++col, "Integral Cell 1");
	tabData->horizontalHeader()->setLabel (++col, "Integral Cell 2");
	tabData->horizontalHeader()->setLabel (++col, "BckGrIntegral 1");
	tabData->horizontalHeader()->setLabel (++col, "BckGrIntegral 2");
	tabData->horizontalHeader()->setLabel (++col, "Integral normalized Cell 1");
	tabData->horizontalHeader()->setLabel (++col, "Integral normalized Cell 2");
	tabData->horizontalHeader()->setLabel (++col, "Rel (abs1/abs2)");
	
	//for all tracks with a background set: add them to the table
	
	int rows = tabData->numRows();
	
	tabData->setReadOnly (true);
	
	Q3IntDict<Track> tracks = TTTManager::getInst().getTree()->getAllTracks();
	
	for (uint i = 1; i < tracks.count(); i++) {
		Track *track = tracks.find (i);
		
		if (track) {
			//if the track has a background set and has an even number
			//(odd numbers are covered as siblings of even numbers)
			if ((track->backgroundTracked (_wavelength))
			    && (track->getNumber() % 2 == 0)) {
				
				Track *track2 = TTTManager::getInst().getTree()->getTrack (track->getNumber() + 1);
				
				if (! track2)
					continue;
				if (! track2->backgroundTracked (_wavelength))
					continue;

				// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				// 2014/02/24 OH: changed code due to change of getPositions() interface
				// (returns QHash<int, QPointF> instead of Q3ValueVector<QPair<int, QPointF> >)
				// NEW CODE NOT TESTED
				// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++				
				//Q3PtrVector<TrackPoint> trackPoints = track->getAllTrackpoints();
				const QVector<TrackPoint*>& trackPoints = track->getAllTrackpoints();
				
				for (uint tj = 0; tj < trackPoints.size(); tj++) {
					TrackPoint *tp1 = trackPoints [tj];
					if (tp1) {
						if (Tools::floatEquals (tp1->XBackground, -1.0f) != 0) {
							//background point set for first track -> display data
							
							TrackPoint tp2 = track2->getTrackPoint (tp1->TimePoint);
							
							
							tabData->setNumRows (rows + 1);
							
							tabData->setText (rows, 0, QString ("%1/%2").arg (track->getNumber()).arg (track2->getNumber()));
							
							tabData->setText (rows, 1, QString ("%1").arg (tp1->TimePoint));
							
							IntensityData id1 = Tools::measureCellIntensity (*tp1, _wavelength, true);
							IntensityData id2 = Tools::measureCellIntensity (tp2, _wavelength, true);
							
							//float integral1 = id1.cellIntegral;
							//float integral2 = id2.cellIntegral;
							int integral1 = id1.absIntegral;
							int integral2 = id2.absIntegral;
							tabData->setText (rows, 2, QString ("%1").arg (integral1));
							tabData->setText (rows, 3, QString ("%1").arg (integral2));
							
							//float background1 = id1.backgroundIntegral;
							//float background2 = id2.backgroundIntegral;
							int background1 = id1.absIntegralBackground;
							int background2 = id2.absIntegralBackground;
							tabData->setText (rows, 4, QString ("%1").arg (background1));
							tabData->setText (rows, 5, QString ("%1").arg (background2));
							
							int abs_intensity1 = id1.getTrueCellIntensity();
							int abs_intensity2 = id2.getTrueCellIntensity();
							//float abs_intensity1 = id1.cellIntegral;
							//float abs_intensity2 = id2.cellIntegral;
							
							absIntVector1.push_back(abs_intensity1); // --------------- Konstantin ----------------
							absIntVector2.push_back(abs_intensity2); // --------------- Konstantin ----------------
							
							///@todo red color is not yet stable
							///@todo data display is refreshed at strange times and too often
							if (abs_intensity1 < 0.0f) {
								tabData->setItem (rows, 6, new MyQTableItem (tabData, QString ("%1").arg (abs_intensity1), Q3TableItem::Always, Qt::red));
							}
							else
								tabData->setText (rows, 6, QString ("%1").arg (abs_intensity1));
							
							if (abs_intensity2 < 0.0f) {
								tabData->setItem (rows, 7, new MyQTableItem (tabData, QString ("%1").arg (abs_intensity2), Q3TableItem::Always, Qt::red));
							}
							else
								tabData->setText (rows, 7, QString ("%1").arg (abs_intensity2));
							
							if (Tools::floatEquals (abs_intensity2, 0.0f, 3) != 0) {
								float rel = abs_intensity1 / abs_intensity2;
								tabData->setText (rows, 8, QString ("%1").arg (rel));
							}
							
							rows++;
						}
					}
				}
			}
		}
	}
	
/*	for (int row = 1; row < tabData->numRows(); row++)
		for (int col = 1; col < tabData->numCols(); col++)
			tabData->updateCell (row, col);*/
			
	drawBarChart(absIntVector1, absIntVector2);
}

//SLOT:
void TTTTracksOverview::jumpToEvent()
{
	if (dispmode == TTO_DISPLAY_MODE_FLUORESCENCE) {
		TTTManager::getInst().setTimepoint (tabData->text (tabData->currentRow(), 1).toInt());
	}
}

void TTTTracksOverview::setPositionManager (TTTPositionManager *_tttpm)
{
	tttpm = _tttpm;
}

void TTTTracksOverview::loadWavelengths()
{
	if (! tttpm)
		return;
	
	cboWavelength->clear();
	
	//Q3ValueList<int> wls (tttpm->getAvailableWavelengths());
	//Q3ValueList<int>::iterator iter;
	const QSet<int>& wls = tttpm->getAvailableWavelengthsByRef();
    for (QSet<int>::const_iterator iter = wls.begin(); iter != wls.end(); ++iter) {
		
		int wl = *iter;
		cboWavelength->insertItem (QString ("%1").arg (wl));
	}
}

//SLOT:
void TTTTracksOverview::setWavelength (const QString &_wl)
{
	int wl = _wl.toInt();
	
	if (dispmode == TTO_DISPLAY_MODE_FLUORESCENCE)
		showFluorescence (wl);
	else if (dispmode == TTO_DISPLAY_MODE_TRACKING_DATA)
		showTrackingData();
}

// --------------- Konstantin ----------------
void TTTTracksOverview::drawBarChart(Q3ValueVector<double> _v1, Q3ValueVector<double> _v2)
{
	/* TODO
	barPointers.clear();
	plotPointers.clear();

	QwtPlot* plot = new QwtPlot(fraBarChart);
	*/
	
// 	QwtArray<double> histogramCurve;
// 	QwtArray<double> stdDevVector;
	
	
	///////////////////////////////
	// Bars for the first column
	///////////////////////////////
//	HistogramItem *histogram1 = new HistogramItem(); // !!!!!!! muss geloescht werden!
// 	histogram1->setColor(Qt::darkCyan);
// 	histogram1->setAdditionalData(0);	
// 
// 	QwtArray<QwtDoubleInterval> intervals(_v1.size());
// 	histogramCurve.resize(_v1.size());
// 	stdDevVector.resize(_v1.size());
// 	
// 	double positionOfBar = 1.0;
// 	int widthOfBar = 1;
// 	for (int i = 0; i < (int)histogramCurve.size(); i++) {
// 		histogramCurve[i] = _v1[i];
// 		intervals [i] = QwtDoubleInterval(positionOfBar, positionOfBar + double(widthOfBar));
// 		positionOfBar += 3; //2*width;
// 		
// 		stdDevVector[i] = 0;
// 	}
// 	double maxY = MathFunctions::getMaxValue(histogramCurve);
// 	double minY = MathFunctions::getMinValue(histogramCurve);
// 
// 	
// 	histogram1->setData(QwtIntervalData(intervals, histogramCurve));
// 	histogram1->setStdDev(stdDevVector);

	/* TODO
	HistogramItem *histogram1 = getHistogramItem(_v1, 1.0, Qt::darkCyan);
	histogram1->attach(plot);
	
	barPointers.append(histogram1);
	*/
	
	
	///////////////////////////////
	// Bars for the second column
	///////////////////////////////	
// 	HistogramItem *histogram2 = new HistogramItem(); // !!!!!!! muss geloescht werden!
// 	histogram2->setColor(Qt::blue);
// 	histogram2->setAdditionalData(0);	
// 
// 	intervals.resize(_v2.size());
// 	histogramCurve.resize(_v2.size());
// 	stdDevVector.resize(_v2.size());
// 	
// 	positionOfBar = 2.0;
// 	widthOfBar = 1;
// 	for (int i = 0; i < (int)histogramCurve.size(); i++) {
// 		histogramCurve[i] = _v2[i];
// 		intervals [i] = QwtDoubleInterval(positionOfBar, positionOfBar + double(widthOfBar));
// 		positionOfBar += 3; //2*width;
// 		
// 		stdDevVector[i] = 0;
// 	}
// 	
// 	if ( maxY < MathFunctions::getMaxValue(histogramCurve) )
// 		maxY = MathFunctions::getMaxValue(histogramCurve);
// 		
// 	if ( minY > MathFunctions::getMinValue(histogramCurve) )
// 		minY = MathFunctions::getMinValue(histogramCurve);
// 
// 	
// 	histogram2->setData(QwtIntervalData(intervals, histogramCurve));
// 	histogram2->setStdDev(stdDevVector);
	/* TODO
	HistogramItem *histogram2 = getHistogramItem(_v2, 2.0, Qt::blue);
 	histogram2->attach(plot);
	
 	barPointers.append(histogram2);
	
	double minY, maxY;
	if ( MathFunctions::getMaxValue(_v1) > MathFunctions::getMaxValue(_v2) )
		maxY = MathFunctions::getMaxValue(_v1);
	else
		maxY = MathFunctions::getMaxValue(_v2);
	
	if ( MathFunctions::getMinValue(_v1) < MathFunctions::getMinValue(_v2) )
		minY = MathFunctions::getMinValue(_v1);
	else
		minY = MathFunctions::getMinValue(_v2);
		
	double maxX = 3*_v1.size() + 1;
	
	plot->setAxisScale(QwtPlot::yLeft, minY, maxY);
	plot->setAxisScale(QwtPlot::xBottom, 0.0, maxX);

	plot->replot();
	
	plot->resize(fraBarChart->width (), fraBarChart->height());
	plot->show();
	
	
	plotPointers.append(plot);
	*/

}
/* TODO
// --------------- Konstantin ----------------
HistogramItem* TTTTracksOverview::getHistogramItem(Q3ValueVector<double> _v, double _positionOfBar, QColor _c)
{
	HistogramItem *histogram = new HistogramItem(); // !!!!!!! muss geloescht werden!
	histogram->setColor(_c);
        QVector<QVector<double> > nix;
        histogram->setAdditionalData(nix);
	
	QwtArray<double> histogramCurve(_v.size());
	QwtArray<double> stdDevVector(_v.size());

	QwtArray<QwtDoubleInterval> intervals(_v.size());
// 	histogramCurve.resize(_v.size());
// 	stdDevVector.resize(_v.size());
	
	int widthOfBar = 1;
	for (int i = 0; i < (int)histogramCurve.size(); i++) {
		histogramCurve[i] = _v[i];
		intervals [i] = QwtDoubleInterval(_positionOfBar, _positionOfBar + double(widthOfBar));
		_positionOfBar += 3; //2*width;
		
		stdDevVector[i] = 0;
	}
		
	histogram->setData(QwtIntervalData(intervals, histogramCurve));
	histogram->setStdDev(stdDevVector);
	
	return histogram;
}
*/


//#include "ttttracksoverview.moc"
