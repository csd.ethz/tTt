/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MYQCANVASLINE_H
#define MYQCANVASLINE_H

#include "tttdata/track.h"

#include <q3canvas.h>

#include <qpainter.h>
#include <q3pointarray.h>
#include <qpoint.h>
#include <q3valuevector.h>
#include <qcolor.h>

/**
 * Draws a line on a QCanvas object.
 * Additional function: stamped line (with arbitrary points displayed or not)
 * 			(with the normal QCanvasLine, you would create as many objects as points desired)
 * NOTE: stamping only works currently when the line is completely vertical (otherwise strange results)
 * 
 * @author Bernhard
*/

class MyQCanvasLine : public Q3CanvasPolygonalItem
{
public:
	/**
	 * constructs a line on a canvas and no coordinates
	 * @param _canvas the canvas this line is associated to
	 * @param _stamped whether the line should be stamped (rather single points on/off than a complete line)
	 * @param _usePC use x() and y() functions in drawShape() (cannot be combined with _stamped)
	 * @param _continuousLine special parameter for continuous cell lines
	 */
	MyQCanvasLine (Q3Canvas* _canvas, bool _stamped = false, bool _usePC = false, bool _continuousLine = false)
        : Q3CanvasPolygonalItem (_canvas), colors (0), colorsUsed (false), stamped (_stamped), usePC (_usePC), width_2 (5), rop (QPainter::CompositionMode_Source), track (0), usage (0), continuousLine(_continuousLine)
		{setZ (1);}
	
	~MyQCanvasLine();
	
	/**
	 * sets the endpoints of the line (regardless if stamped or not)
	 * @param _x1 the x coordinate of the beginning point
	 * @param _y1 the y coordinate of the beginning point
	 * @param _x2 the x coordinate of the end point
	 * @param _y2 the y coordinate of the end point
	 */
	void setPoints (int _x1, int _y1, int _x2, int _y2);
	
	/**
	 * sets the color of the starting and end point
	 * if not used, they are of the line color (deriving from QPen)
	 * @param _startColor the color of the starting point
	 * @param _endColor the color of the end point
	 */
	void setPointColors (QColor _startColor, QColor _endColor);
	
	/**
	 * @return the starting point of the line as QPoint object
	 */
	QPoint startPoint() const
		{return points.point (0);}
	/**
	 * @return the end point of the line as QPoint object
	 */
	QPoint endPoint() const
		{return points.point (1);}
	
	/**
	 * adds a middle point (if in stamped mode, otherwise useless)
	 * @param _x the x coordinate of the middle point (somewhere in the direction of the line)
	 * @param _y the y coordinate of the middle point (somewhere in the direction of the line)
	 */
	void addMiddlePoint (int _x, int _y);

	/**
	 * check if we already have a middle point with the specified coordinates
	 * @param _x the x coordinate of the middle point (somewhere in the direction of the line)
	 * @param _y the y coordinate of the middle point (somewhere in the direction of the line)
	 */
	bool containsMiddlePointAt(int _x, int _y);

	
	/**
	 * adds a colored middle point (if in stamped mode, otherwise useless)
	 * @param _x the x coordinate of the middle point (somewhere in the direction of the line)
	 * @param _y the y coordinate of the middle point (somewhere in the direction of the line)
	 * @param _color the color of the middle point (each point can have a different color)
	 */
	void addMiddlePoint (int _x, int _y, QColor _color);
	
	/**
	 * set the width of the stamp points
	 * @param _width the width (line length) of a single middle "point"
	 */
	void setWidth (int _width)
		{width_2 = (int)((float)_width / 2.0f);}
	
	/**
	 * sets the complete points (for both modes)
	 * note: the first two points are the endpoints
	 * @param _points the QPointArray that contains all points (starting, end, and, if used, the middle points) of the line
	 */
	void setPoints (Q3PointArray _points);
	
	/**
	 * sets whether the line is in stamped style or not
	 * @param _stamped whether the line should consist of a set of single points
	 */
	void setStamped (bool _stamped)
		{stamped = _stamped;}
	
	/**
	 * sets the painter raster operation
	 * @param _rop the raster operation for the QPainter that is used in drawShape()
	 */
        void setRasterOp (QPainter::CompositionMode _rop);
	
	/**
	 * resets (deletes) the complete line
	 * @param _onlyMiddlepoints whether only the middle points are deleted, the edge points are kept
	 */
	virtual void reset (bool _onlyMiddlepoints);
	
	/**
	 * @return the QPointArray surrounding this line (obligatory for any subclass of QCanvasPolygonalItem) 
	 */
	Q3PointArray areaPoints() const;
	
	/**
	 * sets the usage for this object
	 * @param _usage the usage (cf. int constants defined in treedisplay.h)
	 */
	void setUsage (int _usage)
		{usage = _usage;}
	
	/**
	 * @return the usage for this object
	 */
	int getUsage() const
		 {return usage;}
	
	/**
	 * sets the associated Track object
	 * @param _track a pointer to an (existing) Track object
	 */
	void setTrack (Track *_track)
		{track = _track;}
	
	/**
	 * @return the Track object associated with this text object
	 */
	Track *getTrack() const
		{return track;}
	
	
	/**
	 * @return the RTTI value for this class
	 */
	int rtti() const
		{return RTTI;}
	
	///the RTTI value for this class (1005)
	static int RTTI;
	
protected:

//public:
	void drawShape (QPainter &);

//private:
	
	///the points
	Q3PointArray points;
	
	///the colors of the stamped points (filled in addMiddlePoint())
	Q3ValueVector<QColor> *colors;
	
	///whether at any time (at least once) the color setting addMiddlePoint() version was called
	bool colorsUsed;
	
	///whether the line is drawn normally or with each dot single
	bool stamped;
	
	///whether the x() and y() functions should be used in drawing process
	bool usePC;
	
	///the half width of the stamp points (so no more calculation necessary)
	int width_2;
    	
    	///for compatibility to QCanvasLine's areaPoints()
	int x1,y1,x2,y2;
	
	///the painter's raster operation (default: Qt::CopyROP)
        //CompositionMode rop;
        QPainter::CompositionMode rop;

	///the associated track (often very useful for detecting correspondencies)
	Track *track;
	
	///can be used for any association in the creating module
	///best way of using this attribute: define some constants that distinguish different line types and query them
	int usage;

	// Continuous lines
	bool continuousLine;
};

#endif
