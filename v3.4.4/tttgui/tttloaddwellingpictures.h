/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * DEPRECATED!
 */

#ifndef TTTLOADPICTUREPOSITIONS_H
#define TTTLOADPICTUREPOSITIONS_H

#include "ui_frmLoadDwellingPictures.h"
#include "tttdata/userinfo.h"
#include "positiondisplay.h"
#include "tttbackend/tttmanager.h"
#include "tttgui/statusbar.h"
#include "tttdata/systeminfo.h"

#include <qcheckbox.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qlabel.h>


#include <q3intdict.h>
//Added by qt3to4:
#include <QResizeEvent>
#include <QCloseEvent>
#include <QDialog>


const bool FASTLOAD_WITHOUT_PICTURES = false;			//whether no pictures should be loaded for the positions to have a faster display


/**
 * Window to load pictures from several positions for a specific cell, i.e.
 * for n timepoints before and m timepoints after the last time point of the specified cell
 * where n and m can be set by the user ("Load dwelling pictures", activated by right-clicking
 * on a cell in the tree window while pressing ctrl).
 *
 * This class is old and currently not developed further as long as there are no user requests.
 */
class TTTLoadDwellingPictures: public QDialog, public Ui::frmLoadDwellingPictures {

Q_OBJECT

public:
	TTTLoadDwellingPictures(QWidget *parent = 0, const char *name = 0);
	
	void resizeEvent (QResizeEvent *);
	
	void closeEvent (QCloseEvent *);

	void showEvent (QShowEvent *_ev);
	
	/**
	 * Sets the provided position active
	 * @param _pos the index of the position
	 * @param _active whether it should be active or not
	 */
	void markPosition (const QString& _pos, bool _active = true);
	
	/**
	 * Writes the currently selected cells into the label ("current track" in brackets)
	 * @param _cells the cells, already formatted for display
	 */
	void writeSelectedCells (const QString& _cells);
	
	
private slots:
	
	/**
	 * starts the loading process for the selected pictures/positions
	 * called by a click on the "Load" button
	 */
	void startLoading();
	
	/**
	 * called by positionView, when a left-click is performed
	 * activates the selected positions (pictures are loaded for this one)
	 * @param _index the index of the position to be shown
	 */
	void activatePosition (const QString &);
	
	/**
	 * called by positionView, when a right-click is performed
	 * deactivates the selected positions (pictures are not loaded for this one)
	 * @param _index the index of the position to be shown
	 */
	void deactivatePosition (const QString &);
	
	/**
	 * called when the text in one of the timepoint boxes changes
	 * updates the timepoints actually loaded
	 */
	void calcTimepoints();
	
	/**
	 * resets some settings and closes the dialog via accept()
	 */
	void cleanup_n_close();
	
private:
	
	/////the widget containing the thumbnails of the positions
	//PositionDisplay *positionView;
	
	///the number of currently active positions
	int active_count;

	///window layout already loaded
	bool windowLayoutLoaded;
	
	/**
	 * Initializes the provided position manager - makes it possible to load pictures
	 * @param _tttpm the position manager to be initialized
	 * @return whether the initialization was successful (true) or not (false)
	 */
	bool initializePositionManager (TTTPositionManager *_tttpm);
};

#endif
