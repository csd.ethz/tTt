/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "tttsymbolselection.h"

#include "tttbackend/tttmanager.h"
//Added by qt3to4:
#include <QCloseEvent>

TTTSymbolSelection::TTTSymbolSelection(QWidget *parent, const char *name)
    :QWidget(parent, name)
{
        setupUi (this);

        connect ( pbtApply, SIGNAL (clicked()), this, SLOT (apply()));
	connect ( pbtCancel, SIGNAL (clicked()), this, SLOT (cancel()));
	connect ( pbtClose, SIGNAL (clicked()), this, SLOT (close()));
	
	connect ( pbtPosition, SIGNAL (clicked()), this, SLOT (selectPosition()));
	connect ( pbtSetTrack, SIGNAL (clicked()), this, SLOT (selectTrack()));
	
	connect ( pbtDeleteSymbols, SIGNAL (clicked()), this, SLOT (deleteSymbols()));
	
	connect ( bgrSymbol, SIGNAL (clicked (int)), this, SLOT (setSymbol (int)));
	connect ( pbtColor, SIGNAL (clicked()), this, SLOT (setColor()));
	
	connect ( spbSize, SIGNAL (valueChanged (int)), this, SLOT (setSize (int)));
	connect ( spbSymbolAngle, SIGNAL (valueChanged (int)), this, SLOT (setAngle (int)));
	
	symbol.setSize (spbSize->value());
}

void TTTSymbolSelection::closeEvent (QCloseEvent *ev)
{
	apply();
	ev->accept();
}

//SLOT:
void TTTSymbolSelection::apply()
{
	symbol.setTimePoints (spbTimePointStart->value(), spbTimePointEnd->value());
	emit symbolSet (symbol);
	
	//clear view and attrbutes
	lblCurrentTrack->setText ("(none)");
	symbol.setTrack (0);
}

//SLOT:
void TTTSymbolSelection::cancel()
{
	///indication for cancelling the dialog
	symbol.setSize (-1);
	close();
}

//SLOT:
void TTTSymbolSelection::setSymbol (int _index)
{
	//note: the enum order must be the same as the button order!
	for (int k = 0; k < 11 /*Symbols.count*/; k++)
                if (_index == SymbolTypes(k))
                        symbol.setSymbol (SymbolTypes (k));
}

//SLOT:
void TTTSymbolSelection::setColor()
{
	//show a color dialog
	
	QColor col = QColorDialog::getColor();
	
	symbol.setColor (col);
	pbtColor->setPaletteBackgroundColor (col);
}

//SLOT:
void TTTSymbolSelection::setSize (int _size)
{
	symbol.setSize (_size);
}

//SLOT:
void TTTSymbolSelection::setAngle (int _angle)
{
	symbol.setAngle (_angle);
}

//SLOT:
void TTTSymbolSelection::selectPosition()
{
        //triggers a signal/slot cascade which lets the user select a position for the currently selected symbol
        //this position is stored in the PictureView instance that caught the signal

	//note: there is no signal handed back to this form after the position was selected!
	
        emit symbolPositionDesired (symbol);
}

void TTTSymbolSelection::setTimePoints (int _first, int _last)
{
	spbTimePointStart->setValue (_first);
	spbTimePointEnd->setValue (_last);
}

void TTTSymbolSelection::selectTrack()
{
	symbol.setTrack (TTTManager::getInst().getCurrentTrack());
	if (TTTManager::getInst().getCurrentTrack())
		lblCurrentTrack->setText (QString ("currently associated with %1").arg (TTTManager::getInst().getCurrentTrack()->getNumber()));
	else
		lblCurrentTrack->setText ("(none)");
}

void TTTSymbolSelection::deleteSymbols()
{
	emit symbolDelete();
}

//#include "tttsymbolselection.moc"
