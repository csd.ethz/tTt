/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "moviemenubar.h"

// Project includes
#include "tttgui/tttmovie.h"
#include "tttbackend/tttmanager.h"
#include "tttgui/ttttracking.h"
#include "tttdata/trackpoint.h"

// Qt includes
#include <QSignalMapper>
#include <QActionGroup>



MovieMenuBar::MovieMenuBar(TTTMovie* _parent) : QMenuBar(_parent), actTracking_PROPERTY_WLS(15), differentiationGroup(this), adherenceGroup(this)
{
	movieWindow = _parent;

	// Create menus
	createMenus();

	// Connect actions
	connectActions();

	//// Necessary to prevent the application from breaking the menu apart
	//setMinimumSize (0xFFFFFF, 25);
}

void MovieMenuBar::createMenus()
{
	// File
	fileMenu = addMenu("&File");
	fileSubMenuColonyCreation = fileMenu->addMenu("Colony &Creation");

    actFile_COLONYCREATION_SELECT = fileSubMenuColonyCreation->addAction ("Select Colonies");
    actFile_COLONYCREATION_SELECT->setCheckable (true);

    actFile_COLONYCREATION_CREATE = fileSubMenuColonyCreation->addAction ("Create Selected Colonies");

    actFile_DELETECOLONY = fileMenu->addAction ("Delete Colony");

    actFile_CLOSE_WINDOW = fileMenu->addAction ("&Close");
    actFile_CLOSE_WINDOW->setShortcut (Qt::ALT + Qt::Key_F4);

	//// Edit
	//editMenu = addMenu("&Edit");

	// View
	viewMenu = addMenu("&View");

	viewSubMenuTracks = viewMenu->addMenu("&Tracks");

	actView_TRACKS_TRACKS = viewSubMenuTracks->addAction("Show Tracks");
	actView_TRACKS_TRACKS->setCheckable (true);
	actView_TRACKS_TRACKS->setChecked(true);
	actView_TRACKS_TRACKNUMBERS = viewSubMenuTracks->addAction("Show Tracknumbers");
	actView_TRACKS_TRACKNUMBERS->setCheckable (true);
	actView_TRACKS_TRACKNUMBERS->setChecked (true);
	actView_TRACKS_SETFORALLWLS = viewSubMenuTracks->addAction("Set for all Wavelengths");
	actView_TRACKS_SETFORALLWLS->setCheckable (true);
	viewSubMenuTracks->addSeparator();
	actView_TRACKS_POSITION_TRACKS = viewSubMenuTracks->addAction("Show all Tracks of this Position");
	actView_TRACKS_POSITION_TRACKS->setCheckable (true);
	actView_TRACKS_POSITION_TRACKS->setShortcut (Qt::Key_F5);
	actView_TRACKS_ALL_POSITIONS_TRACKS = viewSubMenuTracks->addAction("Show all Tracks of all Positions");
	actView_TRACKS_ALL_POSITIONS_TRACKS->setCheckable (true);
	actView_TRACKS_ALL_POSITIONS_TRACKS->setShortcut (Qt::Key_F6);
	actView_TRACKS_FIRST_TRACKS = viewSubMenuTracks->addAction("First Trackpoints only");
	actView_TRACKS_FIRST_TRACKS->setCheckable (true);
	actView_TRACKS_FIRST_TRACKS->setShortcut (Qt::Key_F7);
	viewSubMenuTracks->addSeparator();
	actView_TRACKS_BACKGROUND_TRACKS = viewSubMenuTracks->addAction("Show Background Tracks");
	actView_TRACKS_BACKGROUND_TRACKS->setCheckable (true);
	actView_TRACKS_BACKGROUND_TRACKS->setChecked (true);


	viewSubMenuCellPath = viewMenu->addMenu("Cell &Paths");

    actView_CIRCLEMOUSE = viewMenu->addAction("Circle &Mouse Pointer");
    actView_CIRCLEMOUSE->setCheckable (true);
	actView_CIRCLEMOUSE->setChecked(true);

	actView_HIDECONTROLS = viewMenu->addAction("&Hide Controls");
	actView_HIDECONTROLS->setShortcut (Qt::CTRL + Qt::Key_H);

	actView_SYMBOLS = viewMenu->addAction("S&ymbols");

	actView_CELL_PATH_ADDCURRENT = viewSubMenuCellPath->addAction("&Add Current Cell");
	actView_CELL_PATH_ADDCURRENT->setShortcut (Qt::CTRL + Qt::SHIFT + Qt::Key_A);
   
	actView_CELL_PATH_CLEARPATHS = viewSubMenuCellPath->addAction("&Clear Paths"); 
    actView_CELL_PATH_CLEARPATHS->setShortcut (Qt::CTRL + Qt::SHIFT + Qt::Key_C);   

	actView_CENTERCELL = viewMenu->addAction("Ce&nter Cell");
	actView_CENTERCELL->setShortcut(Qt::Key_5);

	actView_GAMMA = viewMenu->addAction("&Graphic Adjustment...");
	actView_GAMMA->setShortcut (Qt::CTRL + Qt::Key_G);

	actView_CUSTOMIZE = viewMenu->addAction("Custo&mize...");

	// Tracking
	trackingMenu = addMenu("&Tracking");

	actTracking_START = trackingMenu->addAction("&Start Tracking");
	actTracking_START->setShortcut (Qt::Key_F2);

	trackingSubMenuStopReason = trackingMenu->addMenu("Stop &Reason");

	actTracking_STOPREASON_DIVISION = trackingSubMenuStopReason->addAction("&Division");
	actTracking_STOPREASON_DIVISION->setShortcut (Qt::CTRL + Qt::Key_D);

	actTracking_STOPREASON_APOPTOSIS = trackingSubMenuStopReason->addAction("&Apoptosis");
	actTracking_STOPREASON_APOPTOSIS->setShortcut (Qt::CTRL + Qt::Key_A);

	actTracking_STOPREASON_LOST = trackingSubMenuStopReason->addAction("&Lost");
	actTracking_STOPREASON_LOST->setShortcut (Qt::CTRL + Qt::Key_L);

	actTracking_STOPREASON_INTERRUPT = trackingSubMenuStopReason->addAction("&Interrupt");
	actTracking_STOPREASON_INTERRUPT->setShortcut (Qt::Key_Escape);

	trackingSubMenuStopReason->addSeparator();

	actTracking_STOPREASON_DIVISION_CONTINUETRACKING = trackingSubMenuStopReason->addAction("Division + &Continue Tracking Daughter Cell");
	actTracking_STOPREASON_DIVISION_CONTINUETRACKING->setToolTip("Stop tracking of current cell, annotate cell division and immediately continue tracking with daughter cell.");
	actTracking_STOPREASON_DIVISION_CONTINUETRACKING->setShortcut(Qt::SHIFT + Qt::Key_D);

	trackingSubMenuCellProperties = trackingMenu->addMenu("Cell &Properties");

	trackingSubMenuSelectOtherCell =  trackingMenu->addMenu("Select other &Cell");
	
	// Cell properties -> fluorescence
	trackingSubMenuFluorescence = trackingSubMenuCellProperties->addMenu("&Fluorescence");
	for(unsigned int i = 0; i < actTracking_PROPERTY_WLS.size(); ++i) {
		actTracking_PROPERTY_WLS[i] = trackingSubMenuFluorescence->addAction(QString("Wavelength &%1").arg(i+1));
		actTracking_PROPERTY_WLS[i]->setCheckable(true);
	}

	// Cell properties -> differentiation
	trackingSubMenuDifferentiation = trackingSubMenuCellProperties->addMenu("&Differentiation");

	// Cell properties -> differentiation -> Blood
	trackingSubMenuDifferentiationBlood = trackingSubMenuDifferentiation->addMenu("&Blood");
	// Cell properties -> differentiation -> Blood -> Myeloid
	trackingSubMenuDifferentiationBloodMyeloid = trackingSubMenuDifferentiationBlood->addMenu("&Myeloid");
	actTracking_PROPERTY_DIFF_BLOOD_MYLOID_MEGA = trackingSubMenuDifferentiationBloodMyeloid->addAction("&Mega");
	actTracking_PROPERTY_DIFF_BLOOD_MYLOID_ERY = trackingSubMenuDifferentiationBloodMyeloid->addAction("&Ery");
	actTracking_PROPERTY_DIFF_BLOOD_MYLOID_MAKRO = trackingSubMenuDifferentiationBloodMyeloid->addAction("Ma&kro");
	actTracking_PROPERTY_DIFF_BLOOD_MYLOID_NEUTRO = trackingSubMenuDifferentiationBloodMyeloid->addAction("&Neutro");
	actTracking_PROPERTY_DIFF_BLOOD_MYLOID_EOSINO = trackingSubMenuDifferentiationBloodMyeloid->addAction("Eo&sino");
	actTracking_PROPERTY_DIFF_BLOOD_MYLOID_BASO = trackingSubMenuDifferentiationBloodMyeloid->addAction("&Baso");
	actTracking_PROPERTY_DIFF_BLOOD_MYLOID_MAST = trackingSubMenuDifferentiationBloodMyeloid->addAction("M&ast");
	actTracking_PROPERTY_DIFF_BLOOD_MYLOID_DENDRITIC = trackingSubMenuDifferentiationBloodMyeloid->addAction("Dendritic");
	trackingSubMenuDifferentiationBloodMyeloid->addSeparator();
	actTracking_PROPERTY_DIFF_BLOOD_MYLOID_NONE = trackingSubMenuDifferentiationBloodMyeloid->addAction("N&one");
	// Cell properties -> differentiation -> Blood -> Lymphoid
	trackingSubMenuDifferentiationBloodLymphoid = trackingSubMenuDifferentiationBlood->addMenu("&Lymphoid");
	actTracking_PROPERTY_DIFF_BLOOD_LYMPHOID_B = trackingSubMenuDifferentiationBloodLymphoid->addAction("&B");
	actTracking_PROPERTY_DIFF_BLOOD_LYMPHOID_T = trackingSubMenuDifferentiationBloodLymphoid->addAction("&T");
	actTracking_PROPERTY_DIFF_BLOOD_LYMPHOID_NK = trackingSubMenuDifferentiationBloodLymphoid->addAction("&NK");
	trackingSubMenuDifferentiationBloodLymphoid->addSeparator();
	actTracking_PROPERTY_DIFF_BLOOD_LYMPHOID_NONE = trackingSubMenuDifferentiationBloodLymphoid->addAction("N&one");
	// Cell properties -> differentiation -> Blood -> HSC
	trackingSubMenuDifferentiationBloodHSC = trackingSubMenuDifferentiationBlood->addMenu("&HSC");
	actTracking_PROPERTY_DIFF_BLOOD_HSC_LTR = trackingSubMenuDifferentiationBloodHSC->addAction("&LTR");
	actTracking_PROPERTY_DIFF_BLOOD_HSC_STR = trackingSubMenuDifferentiationBloodHSC->addAction("&STR");
	actTracking_PROPERTY_DIFF_BLOOD_HSC_MPP = trackingSubMenuDifferentiationBloodHSC->addAction("&MPP");
	actTracking_PROPERTY_DIFF_BLOOD_HSC_CMP = trackingSubMenuDifferentiationBloodHSC->addAction("&CMP");
	actTracking_PROPERTY_DIFF_BLOOD_HSC_GMP = trackingSubMenuDifferentiationBloodHSC->addAction("&GMP");
	actTracking_PROPERTY_DIFF_BLOOD_HSC_MEP = trackingSubMenuDifferentiationBloodHSC->addAction("M&EP");
	trackingSubMenuDifferentiationBloodHSC->addSeparator();
	actTracking_PROPERTY_DIFF_BLOOD_HSC_NONE = trackingSubMenuDifferentiationBloodHSC->addAction("&None");
	// Cell properties -> differentiation -> Blood -> Primitive
	actTracking_PROPERTY_DIFF_BLOOD_PRIMITIVE = trackingSubMenuDifferentiationBlood->addAction("&Primitive");
	// Cell properties -> differentiation -> Blood -> None
	trackingSubMenuDifferentiationBlood->addSeparator();
	actTracking_PROPERTY_DIFF_BLOOD_NONE = trackingSubMenuDifferentiationBlood->addAction("&None");
	// Cell properties -> differentiation -> Stroma
	trackingSubMenuDifferentiationStroma = trackingSubMenuDifferentiation->addMenu("&Stroma");
	// Cell properties -> differentiation -> Stroma -> Primary
	trackingSubMenuDifferentiationStromaPrimary = trackingSubMenuDifferentiationStroma->addMenu("&Primary");
	actTracking_PROPERTY_DIFF_STROMA_PRIM_WBM = trackingSubMenuDifferentiationStromaPrimary->addAction("&WBM");
	actTracking_PROPERTY_DIFF_STROMA_PRIM_OB = trackingSubMenuDifferentiationStromaPrimary->addAction("&OB");
	actTracking_PROPERTY_DIFF_STROMA_PRIM_EC = trackingSubMenuDifferentiationStromaPrimary->addAction("&EC");
	trackingSubMenuDifferentiationStromaPrimary->addSeparator();
	actTracking_PROPERTY_DIFF_STROMA_PRIM_NONE = trackingSubMenuDifferentiationStromaPrimary->addAction("&None");
	// Cell properties -> differentiation -> Stroma -> CellLine
	trackingSubMenuDifferentiationStromaCellLine = trackingSubMenuDifferentiationStroma->addMenu("&Cell line");
	actTracking_PROPERTY_DIFF_STROMA_CELLLINE_AFT = trackingSubMenuDifferentiationStromaCellLine->addAction("&AFT024");
	actTracking_PROPERTY_DIFF_STROMA_CELLLINE_2018 = trackingSubMenuDifferentiationStromaCellLine->addAction("&2018");
	actTracking_PROPERTY_DIFF_STROMA_CELLLINE_BFC = trackingSubMenuDifferentiationStromaCellLine->addAction("&BFC");
	actTracking_PROPERTY_DIFF_STROMA_CELLLINE_OP9 = trackingSubMenuDifferentiationStromaCellLine->addAction("&OP9");
	actTracking_PROPERTY_DIFF_STROMA_CELLLINE_PA6 = trackingSubMenuDifferentiationStromaCellLine->addAction("&PA6");
	trackingSubMenuDifferentiationStromaCellLine->addSeparator();
	actTracking_PROPERTY_DIFF_STROMA_CELLLINE_NONE = trackingSubMenuDifferentiationStromaCellLine->addAction("&None");
	// Cell properties -> differentiation -> Stroma -> None
	trackingSubMenuDifferentiationStroma->addSeparator();
	actTracking_PROPERTY_DIFF_STROMA_NONE = trackingSubMenuDifferentiationStroma->addAction("&None");
	// Cell properties -> differentiation -> Endothelium
	actTracking_PROPERTY_DIFF_ENDOTHELIUM = trackingSubMenuDifferentiation->addAction("Endo&thelium");
	// Cell properties -> differentiation -> Heart
	actTracking_PROPERTY_DIFF_HEART = trackingSubMenuDifferentiation->addAction("&Heart");
	// Cell properties -> differentiation -> Nerve
	trackingSubMenuDifferentiationNerve = trackingSubMenuDifferentiation->addMenu("&Nerve");
	actTracking_PROPERTY_DIFF_NERVE_NEURO = trackingSubMenuDifferentiationNerve->addAction("&Neuro");
	actTracking_PROPERTY_DIFF_NERVE_GLIA = trackingSubMenuDifferentiationNerve->addAction("&Glia");
	trackingSubMenuDifferentiationNerve->addSeparator();
	actTracking_PROPERTY_DIFF_NERVE_NONE = trackingSubMenuDifferentiationNerve->addAction("N&one");
	// Cell properties -> differentiation -> Endoderm
	actTracking_PROPERTY_DIFF_ENDODERM = trackingSubMenuDifferentiation->addAction("&Endoderm");
	// Cell properties -> differentiation -> Mesoderm
	actTracking_PROPERTY_DIFF_MESODERM = trackingSubMenuDifferentiation->addAction("&Mesoderm");
	// Cell properties -> differentiation -> Not set
	trackingSubMenuDifferentiation->addSeparator();
	actTracking_PROPERTY_DIFF_TISSUE_NONE = trackingSubMenuDifferentiation->addAction("&None");

	actTracking_PROPERTY_DIFF_BLOOD_MYLOID_MEGA->setCheckable(true);
	actTracking_PROPERTY_DIFF_BLOOD_MYLOID_ERY->setCheckable(true);
	actTracking_PROPERTY_DIFF_BLOOD_MYLOID_MAKRO->setCheckable(true);
	actTracking_PROPERTY_DIFF_BLOOD_MYLOID_NEUTRO->setCheckable(true);
	actTracking_PROPERTY_DIFF_BLOOD_MYLOID_EOSINO->setCheckable(true);
	actTracking_PROPERTY_DIFF_BLOOD_MYLOID_BASO->setCheckable(true);
	actTracking_PROPERTY_DIFF_BLOOD_MYLOID_MAST->setCheckable(true);
	actTracking_PROPERTY_DIFF_BLOOD_MYLOID_DENDRITIC->setCheckable(true);
	actTracking_PROPERTY_DIFF_BLOOD_LYMPHOID_B->setCheckable(true);
	actTracking_PROPERTY_DIFF_BLOOD_LYMPHOID_T->setCheckable(true);
	actTracking_PROPERTY_DIFF_BLOOD_LYMPHOID_NK->setCheckable(true);
	actTracking_PROPERTY_DIFF_BLOOD_HSC_LTR->setCheckable(true);
	actTracking_PROPERTY_DIFF_BLOOD_HSC_STR->setCheckable(true);
	actTracking_PROPERTY_DIFF_BLOOD_HSC_MPP->setCheckable(true);
	actTracking_PROPERTY_DIFF_BLOOD_HSC_CMP->setCheckable(true);
	actTracking_PROPERTY_DIFF_BLOOD_HSC_GMP->setCheckable(true);
	actTracking_PROPERTY_DIFF_BLOOD_HSC_MEP->setCheckable(true);
	actTracking_PROPERTY_DIFF_BLOOD_PRIMITIVE->setCheckable(true);
	actTracking_PROPERTY_DIFF_STROMA_PRIM_WBM->setCheckable(true);
	actTracking_PROPERTY_DIFF_STROMA_PRIM_OB->setCheckable(true);
	actTracking_PROPERTY_DIFF_STROMA_PRIM_EC->setCheckable(true);
	actTracking_PROPERTY_DIFF_STROMA_CELLLINE_AFT->setCheckable(true);
	actTracking_PROPERTY_DIFF_STROMA_CELLLINE_2018->setCheckable(true);
	actTracking_PROPERTY_DIFF_STROMA_CELLLINE_BFC->setCheckable(true);
	actTracking_PROPERTY_DIFF_STROMA_CELLLINE_OP9->setCheckable(true);
	actTracking_PROPERTY_DIFF_STROMA_CELLLINE_PA6->setCheckable(true);
	actTracking_PROPERTY_DIFF_ENDOTHELIUM->setCheckable(true);
	actTracking_PROPERTY_DIFF_HEART->setCheckable(true);
	actTracking_PROPERTY_DIFF_NERVE_NEURO->setCheckable(true);
	actTracking_PROPERTY_DIFF_NERVE_GLIA->setCheckable(true);
	actTracking_PROPERTY_DIFF_ENDODERM->setCheckable(true);
	actTracking_PROPERTY_DIFF_MESODERM->setCheckable(true);
	actTracking_PROPERTY_DIFF_TISSUE_NONE->setCheckable(true);
	actTracking_PROPERTY_DIFF_STROMA_NONE->setCheckable(true);
	actTracking_PROPERTY_DIFF_STROMA_PRIM_NONE->setCheckable(true);
	actTracking_PROPERTY_DIFF_BLOOD_NONE->setCheckable(true);
	actTracking_PROPERTY_DIFF_BLOOD_LYMPHOID_NONE->setCheckable(true);
	actTracking_PROPERTY_DIFF_BLOOD_MYLOID_NONE->setCheckable(true);
	actTracking_PROPERTY_DIFF_NERVE_NONE->setCheckable(true);
	differentiationGroup.setExclusive(true);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_BLOOD_MYLOID_MEGA);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_BLOOD_MYLOID_ERY);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_BLOOD_MYLOID_MAKRO);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_BLOOD_MYLOID_NEUTRO);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_BLOOD_MYLOID_EOSINO);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_BLOOD_MYLOID_BASO);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_BLOOD_MYLOID_MAST);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_BLOOD_MYLOID_DENDRITIC);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_BLOOD_LYMPHOID_B);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_BLOOD_LYMPHOID_T);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_BLOOD_LYMPHOID_NK);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_BLOOD_HSC_LTR);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_BLOOD_HSC_STR);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_BLOOD_HSC_MPP);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_BLOOD_HSC_CMP);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_BLOOD_HSC_GMP);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_BLOOD_HSC_MEP);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_BLOOD_PRIMITIVE);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_STROMA_PRIM_WBM);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_STROMA_PRIM_OB);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_STROMA_PRIM_EC);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_STROMA_CELLLINE_AFT);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_STROMA_CELLLINE_2018);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_STROMA_CELLLINE_BFC);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_STROMA_CELLLINE_OP9);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_STROMA_CELLLINE_PA6);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_ENDOTHELIUM);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_HEART);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_NERVE_NEURO);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_NERVE_GLIA);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_ENDODERM);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_MESODERM);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_TISSUE_NONE);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_STROMA_NONE);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_STROMA_PRIM_NONE);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_BLOOD_NONE);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_BLOOD_LYMPHOID_NONE);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_BLOOD_MYLOID_NONE);
	differentiationGroup.addAction(actTracking_PROPERTY_DIFF_NERVE_NONE);
	actTracking_PROPERTY_DIFF_TISSUE_NONE->setChecked(true);


	// Cell properties -> adherence
	trackingSubMenuAdherence = trackingSubMenuCellProperties->addMenu("&Adherence");
	actTracking_PROPERTY_ADHERENCE_ADHERENT = trackingSubMenuAdherence->addAction("&Adherent");
	actTracking_PROPERTY_ADHERENCE_FREEFLOATING = trackingSubMenuAdherence->addAction("&FreeFloating");
	actTracking_PROPERTY_ADHERENCE_SEMIADHERENT = trackingSubMenuAdherence->addAction("&Semiadherent");
	trackingSubMenuAdherence->addSeparator();
	actTracking_PROPERTY_ADHERENCE_NONE = trackingSubMenuAdherence->addAction("&Not Set");
	actTracking_PROPERTY_ADHERENCE_ADHERENT->setCheckable(true);
	actTracking_PROPERTY_ADHERENCE_FREEFLOATING->setCheckable(true);
	actTracking_PROPERTY_ADHERENCE_SEMIADHERENT->setCheckable(true);
	actTracking_PROPERTY_ADHERENCE_NONE->setCheckable(true);
	adherenceGroup.setExclusive(true);
	adherenceGroup.addAction(actTracking_PROPERTY_ADHERENCE_ADHERENT);
	adherenceGroup.addAction(actTracking_PROPERTY_ADHERENCE_FREEFLOATING);
	adherenceGroup.addAction(actTracking_PROPERTY_ADHERENCE_SEMIADHERENT);
	adherenceGroup.addAction(actTracking_PROPERTY_ADHERENCE_NONE);
	actTracking_PROPERTY_ADHERENCE_NONE->setChecked(true);

	// Cell properties -> endomitosis
	actTracking_PROPERTY_ENDOMITOSIS = trackingSubMenuCellProperties->addAction("&Endomitosis");
	actTracking_PROPERTY_ENDOMITOSIS->setCheckable(true);

	// Switch cell
	actTracking_SELECTCELL_SISTER = trackingSubMenuSelectOtherCell->addAction("&Sister Cell");
	actTracking_SELECTCELL_SISTER->setShortcut(Qt::CTRL + Qt::Key_Down);
	actTracking_SELECTCELL_MOTHER = trackingSubMenuSelectOtherCell->addAction("&Mother Cell");
	actTracking_SELECTCELL_MOTHER->setShortcut(Qt::CTRL + Qt::Key_Up);
	actTracking_SELECTCELL_DAUGHTER1 = trackingSubMenuSelectOtherCell->addAction("&Left Daughter Cell");
	actTracking_SELECTCELL_DAUGHTER1->setShortcut(Qt::CTRL + Qt::Key_Left);
	actTracking_SELECTCELL_DAUGHTER2 = trackingSubMenuSelectOtherCell->addAction("&Right Daughter Cell");
	actTracking_SELECTCELL_DAUGHTER2->setShortcut(Qt::CTRL + Qt::Key_Right);

	//// OTODO:
	//trackingSubMenuCellProperties->setEnabled(false);

	// Export
	exportMenu = addMenu("Export");

	//exportSubMenuMovie = exportMenu->addMenu("Export &movie");

	actExport_MOVIE = exportMenu->addAction("Export Movie");
	actExport_MOVIE->setShortcut (Qt::CTRL + Qt::Key_E);
	actExport_MOVIESPECIAL = exportMenu->addAction("Special Movie Export");
	exportMenu->addSeparator();

	/*
	ToDo:
	exportSubMenuTree = exportMenu->addMenu("Export &Tree"); 
	exportSubMenuData = exportMenu->addMenu("Export &Data");
	*/

	actExport_DIVISION  = exportMenu->addAction("Export Division Overview");

	// Tools
	toolsMenu = addMenu("T&ools");
	actTools_PLAYER = toolsMenu->addAction("&Player");
	actTools_SPECIALEFFECTS = toolsMenu->addAction("Special &Effects");
	toolsMenu->addSeparator();
	actTools_STATTTS = toolsMenu->addAction("&StaTTTs");
	actTools_AMT = toolsMenu->addAction("&AMT");
	
	// Help
	helpMenu = addMenu("&Help");

	actHelp_SHOWLOGFILEPATH = helpMenu->addAction("Show Path to Log File");
}

void MovieMenuBar::connectActions()
{
	// File
    connect ( actFile_COLONYCREATION_SELECT, SIGNAL (triggered (bool)), movieWindow->getMultiplePictureViewer(), SLOT (selectStartCells (bool)));
    connect ( actFile_COLONYCREATION_CREATE, SIGNAL (triggered()), movieWindow->getMultiplePictureViewer(), SLOT (createColonies()));
	connect ( actFile_CLOSE_WINDOW, SIGNAL (triggered()), movieWindow, SLOT (close()));

	// Tracking
	connect ( actTracking_START, SIGNAL (triggered()), TTTManager::getInst().frmTracking, SLOT (startCellTracking ()));
	
	//use signal mapper to connect all stop reason menu buttons with one slot (bool TTTMovie::stopTracking (TrackStopReason _reason))
	//and pass correct stop reason in each case
	QSignalMapper *trackingStopReason_SM = new QSignalMapper (this);
	trackingStopReason_SM->setMapping(actTracking_STOPREASON_DIVISION, (int)TS_DIVISION);
	trackingStopReason_SM->setMapping(actTracking_STOPREASON_APOPTOSIS, (int)TS_APOPTOSIS);
	trackingStopReason_SM->setMapping(actTracking_STOPREASON_LOST, (int)TS_LOST);
	trackingStopReason_SM->setMapping(actTracking_STOPREASON_INTERRUPT, (int)TS_NONE);

	connect ( actTracking_STOPREASON_DIVISION, SIGNAL (triggered()), trackingStopReason_SM, SLOT (map()) );
	connect ( actTracking_STOPREASON_APOPTOSIS, SIGNAL (triggered()), trackingStopReason_SM, SLOT (map()) );
	connect ( actTracking_STOPREASON_LOST, SIGNAL (triggered()), trackingStopReason_SM, SLOT (map()) );
	connect ( actTracking_STOPREASON_INTERRUPT, SIGNAL (triggered()), trackingStopReason_SM, SLOT (map()) );

	connect(trackingStopReason_SM, SIGNAL(mapped(int)), movieWindow, SLOT(stopTracking(int)));

	connect(actTracking_STOPREASON_DIVISION_CONTINUETRACKING, SIGNAL(triggered()), movieWindow, SLOT(stopTrackingAndContinueWithDaughter()));


	// Create differentiation signal mapper
	differentiationMapper = new QSignalMapper(this);
	connect(differentiationMapper, SIGNAL(mapped(int)), movieWindow, SLOT(setTrackingCellPropertiesDifferentiation(int)));
	
	// Set mappings
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_BLOOD_MYLOID_MEGA, 1101);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_BLOOD_MYLOID_ERY, 1102);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_BLOOD_MYLOID_MAKRO, 1103);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_BLOOD_MYLOID_NEUTRO, 1104);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_BLOOD_MYLOID_EOSINO, 1105);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_BLOOD_MYLOID_BASO, 1106);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_BLOOD_MYLOID_MAST, 1107);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_BLOOD_MYLOID_DENDRITIC, 1108);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_BLOOD_MYLOID_NONE, 1100);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_BLOOD_LYMPHOID_B, 1201);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_BLOOD_LYMPHOID_T, 1202);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_BLOOD_LYMPHOID_NK, 1203);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_BLOOD_LYMPHOID_NONE, 1200);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_BLOOD_HSC_LTR, 1301);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_BLOOD_HSC_STR, 1302);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_BLOOD_HSC_MPP, 1303);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_BLOOD_HSC_CMP, 1304);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_BLOOD_HSC_GMP, 1305);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_BLOOD_HSC_MEP, 1306);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_BLOOD_HSC_NONE, 1300);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_BLOOD_PRIMITIVE, 1400);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_BLOOD_NONE, 1000);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_STROMA_PRIM_WBM, 2501);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_STROMA_PRIM_OB, 2502);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_STROMA_PRIM_EC, 2503);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_STROMA_PRIM_NONE, 2500);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_STROMA_CELLLINE_AFT, 2601);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_STROMA_CELLLINE_2018, 2602);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_STROMA_CELLLINE_BFC, 2603);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_STROMA_CELLLINE_OP9, 2604);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_STROMA_CELLLINE_PA6, 2605);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_STROMA_CELLLINE_NONE, 2600);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_STROMA_NONE, 2000);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_ENDOTHELIUM, 3000);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_HEART, 4000);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_NERVE_NEURO, 5700);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_NERVE_GLIA, 5800);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_NERVE_NONE, 5000);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_ENDODERM, 6000);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_MESODERM, 7000);
	differentiationMapper->setMapping(actTracking_PROPERTY_DIFF_TISSUE_NONE, 0000);


	// Connect actions to mapper
	connect(actTracking_PROPERTY_DIFF_BLOOD_MYLOID_MEGA, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_BLOOD_MYLOID_ERY, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_BLOOD_MYLOID_MAKRO, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_BLOOD_MYLOID_NEUTRO, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_BLOOD_MYLOID_EOSINO, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_BLOOD_MYLOID_BASO, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_BLOOD_MYLOID_MAST, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_BLOOD_MYLOID_DENDRITIC, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_BLOOD_MYLOID_NONE, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_BLOOD_LYMPHOID_B, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_BLOOD_LYMPHOID_T, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_BLOOD_LYMPHOID_NK, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_BLOOD_LYMPHOID_NONE, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_BLOOD_HSC_LTR, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_BLOOD_HSC_STR, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_BLOOD_HSC_MPP, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_BLOOD_HSC_CMP, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_BLOOD_HSC_GMP, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_BLOOD_HSC_MEP, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_BLOOD_HSC_NONE, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_BLOOD_PRIMITIVE, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_BLOOD_NONE, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_STROMA_PRIM_WBM, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_STROMA_PRIM_OB, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_STROMA_PRIM_EC, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_STROMA_PRIM_NONE, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_STROMA_CELLLINE_AFT, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_STROMA_CELLLINE_2018, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_STROMA_CELLLINE_BFC, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_STROMA_CELLLINE_OP9, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_STROMA_CELLLINE_PA6, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_STROMA_CELLLINE_NONE, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_STROMA_NONE, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_ENDOTHELIUM, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_HEART, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_NERVE_NEURO, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_NERVE_GLIA, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_NERVE_NONE, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_ENDODERM, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_MESODERM, SIGNAL(triggered()), differentiationMapper, SLOT(map()));
	connect(actTracking_PROPERTY_DIFF_TISSUE_NONE, SIGNAL(triggered()), differentiationMapper, SLOT(map()));

	// Adherence
	connect(actTracking_PROPERTY_ADHERENCE_ADHERENT, SIGNAL(triggered(bool)), movieWindow, SLOT (setTrackingCellPropertiesAdherence (bool)));
	connect(actTracking_PROPERTY_ADHERENCE_FREEFLOATING, SIGNAL(triggered(bool)), movieWindow, SLOT (setTrackingCellPropertiesAdherence (bool)));
	connect(actTracking_PROPERTY_ADHERENCE_SEMIADHERENT, SIGNAL(triggered(bool)), movieWindow, SLOT (setTrackingCellPropertiesAdherence (bool)));
	connect(actTracking_PROPERTY_ADHERENCE_NONE, SIGNAL(triggered(bool)), movieWindow, SLOT (setTrackingCellPropertiesAdherence (bool)));

	// Cell properties -> fluorescence
	for(unsigned int i = 0; i < actTracking_PROPERTY_WLS.size(); ++i) {
		connect(actTracking_PROPERTY_WLS[i], SIGNAL(triggered(bool)), movieWindow, SLOT (setTrackingCellPropertiesWavelength (bool)));
	}

	// Select other cell
	selectCellMapper = new QSignalMapper(this);
	connect(selectCellMapper, SIGNAL(mapped(int)), movieWindow, SLOT(selectOtherCell(int)));
	connect(actTracking_SELECTCELL_SISTER, SIGNAL(triggered()), selectCellMapper, SLOT(map()));
	connect(actTracking_SELECTCELL_MOTHER, SIGNAL(triggered()), selectCellMapper, SLOT(map()));
	connect(actTracking_SELECTCELL_DAUGHTER1, SIGNAL(triggered()), selectCellMapper, SLOT(map()));
	connect(actTracking_SELECTCELL_DAUGHTER2, SIGNAL(triggered()), selectCellMapper, SLOT(map()));
	selectCellMapper->setMapping(actTracking_SELECTCELL_SISTER, 0);
	selectCellMapper->setMapping(actTracking_SELECTCELL_MOTHER, 1);
	selectCellMapper->setMapping(actTracking_SELECTCELL_DAUGHTER1, 2);
	selectCellMapper->setMapping(actTracking_SELECTCELL_DAUGHTER2, 3);

	// View
    connect ( actView_HIDECONTROLS, SIGNAL (triggered()), movieWindow, SLOT (hideShowControls()));
    connect ( actView_SYMBOLS, SIGNAL (triggered()), movieWindow, SLOT (showSymbolForm()));
    connect ( actView_CIRCLEMOUSE, SIGNAL (triggered (bool)), movieWindow, SLOT (setCircleMousePointer (bool)));
    connect ( actView_CENTERCELL, SIGNAL (triggered()), movieWindow->getMultiplePictureViewer(), SLOT (centerCurrentTrack()));
    connect ( actView_GAMMA, SIGNAL (triggered()), movieWindow, SLOT (adjustGraphics()));
    connect ( actView_CELL_PATH_ADDCURRENT, SIGNAL (triggered()), movieWindow->getMultiplePictureViewer(), SLOT (addCellPath()));
    connect ( actView_CELL_PATH_CLEARPATHS, SIGNAL (triggered()), movieWindow->getMultiplePictureViewer(), SLOT (clearCellPaths()));
	connect ( actView_CUSTOMIZE, SIGNAL (triggered()), &TTTManager::getInst(), SLOT (showCustomizeDialog()));
	connect ( actView_TRACKS_TRACKS, SIGNAL (triggered(bool)), movieWindow, SLOT ( setShowTracks(bool)));
	connect ( actView_TRACKS_TRACKNUMBERS, SIGNAL (triggered (bool)), movieWindow, SLOT ( setShowTrackNumbers(bool)));
	connect ( actView_TRACKS_SETFORALLWLS, SIGNAL (triggered (bool)), movieWindow->chkTrackVisibilityAllWavelengths, SLOT ( setChecked(bool)));

	connect ( actView_TRACKS_POSITION_TRACKS, SIGNAL (triggered (bool)), movieWindow, SLOT (setShowExternalTracks (bool)));
	connect ( actView_TRACKS_ALL_POSITIONS_TRACKS, SIGNAL (triggered (bool)), movieWindow, SLOT (setShowExternalTracks (bool)));
	connect ( actView_TRACKS_FIRST_TRACKS, SIGNAL (triggered (bool)), movieWindow, SLOT (setShowExternalTracks (bool)));
	connect ( actView_TRACKS_BACKGROUND_TRACKS, SIGNAL (triggered (bool)), movieWindow->getMultiplePictureViewer(), SLOT (setCellBackgroundDisplay (bool)));
	connect ( actView_TRACKS_BACKGROUND_TRACKS, SIGNAL (triggered (bool)), movieWindow->pbtShowBackground, SLOT (setChecked (bool)));
	
	//connect ( actView_TRACKS_TRACKS, SIGNAL (triggered(bool)), movieWindow, SLOT (setShowTracksForAllWls(bool)));
	//connect ( actView_TRACKS_BACKGROUND_TRACKS, SIGNAL (triggered(bool)), movieWindow, SLOT ());
	//connect ( actView_TRACKS_FIRST_TRACKS, SIGNAL (triggered(bool)), movieWindow->getMultiplePictureViewer(), SLOT (setDisplayFirstTracks (bool)));
	//connect ( actView_TRACKS_POSITION_TRACKS, SIGNAL (triggered(bool)), movieWindow->getMultiplePictureViewer(), SLOT (setDisplayAllTracksOfCurPos (bool)));
	//connect ( actView_TRACKS_ALL_POSITIONS_TRACKS, SIGNAL (triggered(bool)), movieWindow->getMultiplePictureViewer(), SLOT (setDisplayAllTracksOfAllPos (bool)));

	// Export
	connect(actExport_MOVIE, SIGNAL(triggered()), movieWindow, SLOT(launchMovieExportDialog()));
	connect(actExport_MOVIESPECIAL, SIGNAL(triggered()), movieWindow, SLOT(specialExport ()));
	connect(actExport_DIVISION, SIGNAL(triggered()), movieWindow, SLOT(launchDivisionExportDialog()));
	//connect ( actExport_MOVIE_FORMAT_JPG, SIGNAL (triggered()), movieWindow, SLOT (exportMovie()));
	//connect ( actExport_MOVIE_FORMAT_BMP, SIGNAL (triggered()), movieWindow, SLOT (exportMovie()));
	//connect ( actExport_MOVIE_FORMAT_PNG, SIGNAL (triggered()), movieWindow, SLOT (exportMovie()));

	// Tools
	connect ( actTools_PLAYER, SIGNAL (triggered()), movieWindow, SLOT (showMoviePlayer()));
	connect ( actTools_SPECIALEFFECTS, SIGNAL (triggered()), movieWindow, SLOT (showSpecialEffectsDialog()));
	connect ( actTools_STATTTS, SIGNAL (triggered()), TTTManager::getInst().frmTracking, SLOT (launchTTTStats()));
	connect ( actTools_AMT, SIGNAL (triggered()), TTTManager::getInst().frmTracking, SLOT (launchAMT()));
	
	// Help
	connect(actHelp_SHOWLOGFILEPATH, SIGNAL(triggered()), movieWindow, SLOT(showLogFilePath()));

}

MovieMenuBar::~MovieMenuBar()
{

}