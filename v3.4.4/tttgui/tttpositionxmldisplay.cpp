/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "tttpositionxmldisplay.h"

// Project includes
#include "tttbackend/tttmanager.h"
#include "tttdata/userinfo.h"
#include "tttio/xmlhandler.h"
//#include "tttbackend/positionmanagervector.h"
#include "tttbackend/tools.h"

// Qt includes
#include <QCloseEvent>
#include <QShowEvent>



TTTPositionXMLDisplay::TTTPositionXMLDisplay(QWidget *parent, const char *name)
    :QWidget(parent, name)
{
        setupUi (this);

	connect ( pbtClose, SIGNAL (clicked()), this, SLOT (close()));
	
	connect ( cboPosition, SIGNAL (activated (const QString &)), this, SLOT (setPositionIndexString (const QString &)));
	connect ( lieTimepoint, SIGNAL (returnPressed()), this, SLOT (setTimepoint()));
	connect ( lieWavelength, SIGNAL (returnPressed()), this, SLOT (setWavelength()));
	
	connect ( pbtTATExpandAll, SIGNAL (clicked()), this, SLOT (expandItem()));
	connect ( pbtTATCollapseAll, SIGNAL (clicked()), this, SLOT (collapseItem()));
	connect ( pbtImageExpandAll, SIGNAL (clicked()), this, SLOT (expandItem()));
	connect ( pbtImageCollapseAll, SIGNAL (clicked()), this, SLOT (collapseItem()));
	
	tatXML_loaded = false;
	timepoint = 1;
	wavelength = 0;
	windowLayoutLoaded = false;
	positionIndex = -1;
}

//EVENT: closeEvent()
void TTTPositionXMLDisplay::closeEvent (QCloseEvent *_ev)
{
	// Save window layout
	UserInfo::getInst().saveWindowPosition(this, "TTTPositionXMLDisplay");

	//closing the movie form does not quit the current analysis (the tree display form is the main form)
	_ev->accept();
}

//EVENT:
void TTTPositionXMLDisplay::showEvent (QShowEvent *_ev)
{
	if(!_ev->spontaneous() && !windowLayoutLoaded) {
		// Load window layout
		UserInfo::getInst().loadWindowPosition(this, "TTTPositionXMLDisplay");

		windowLayoutLoaded = true;
	}

	_ev->accept();
}

//SLOT:
void TTTPositionXMLDisplay::setPositionIndex (int _posIndex)
{
	if (! sender()) {
		//this slot was called by code, so we have to display the position index
		cboPosition->setCurrentText (Tools::convertIntPositionToString(_posIndex));
	}
	
	TTTPositionManager *tttpm = TTTManager::getInst().getPositionManager (_posIndex);
	
	if (! tttpm) {
		//this position does not exist
		return;
	}
	
	positionIndex = _posIndex;
	
	//preselect first picture for avoiding to read all files in the position folder
	lieTimepoint->setText ("1");
	lieWavelength->setText ("0");
	
	cboPosition->setCurrentText (Tools::convertIntPositionToString(_posIndex));
	
	timepoint = lieTimepoint->text().toInt();
	wavelength = lieWavelength->text().toInt();
	
	loadImageXML (_posIndex, timepoint, wavelength);
}

void TTTPositionXMLDisplay::loadImageXML()
{
	//use currently set values
	loadImageXML (positionIndex, timepoint, wavelength);
}

void TTTPositionXMLDisplay::loadImageXML (int _posIndex, const int _timepoint, const int _wavelength)
{
	TTTPositionManager *tttpm = TTTManager::getInst().getPositionManager (_posIndex);
	
	lvwImageXML->clear();
	
	if (tttpm) {
		
		//read information from XML file for the first picture and display it
		
		//NOTE this assumes that the timepoint section consumes exactly five digits
		QString tp_wl = TIMEPOINT_MARKER + QString().sprintf ("%05d", _timepoint) + WAVELENGTH_MARKER + QString ("%1").arg (_wavelength);
		
                QString filter = "*" + tp_wl + ".jpg" + PICTURE_META_APPENDIX + ";" + "*" + tp_wl + ".tif" + PICTURE_META_APPENDIX + ";" + "*" + tp_wl + ".tiff" + PICTURE_META_APPENDIX;
                QVector<QString> xmls = SystemInfo::listFiles (tttpm->getPictureDirectory(), QDir::Files, filter, false);
                QVector<QString>::Iterator iterFiles = xmls.begin();
		
		if (iterFiles != xmls.end()) {
			//note: there should be exactly one file with the provided filter name, if there is one at all
			QString xmlFilename = *iterFiles;
			
			lblImageXMLFilename->setText ("Read from: " + xmlFilename);
			
			QDomDocument metaXML_DOM ("imgMetaXML");
			XMLHandler::readXML2DOM (xmlFilename, metaXML_DOM);
			Tools::parseDOMDocumentIntoListview (metaXML_DOM, lvwImageXML);
			
/*			//read only the v87 tag (fluor filter info)
			QDomElement docElem = metaXML_DOM.documentElement();
			lblV87->setText ("");
			QDomNodeList dnl = metaXML_DOM.elementsByTagName ("V87");
			QDomNode v87 = dnl.item (0);
			if (! v87.isNull()) {
				QDomElement e = v87.toElement(); // try to convert the node to an element.
				if (! e.isNull()) {
					lblV87->setText (e.text());
				}
			}*/
			
		}
	}
}

void TTTPositionXMLDisplay::loadTATXML()
{
	if (tatXML_loaded)
		//tat XML already loaded
		return;
	
	lblTATXMLFilename->setText ("Read from: " + TATInformation::getInst()->getXMLFilename());
	
	QDomDocument tatXML_DOM ("TATXML");
	XMLHandler::readXML2DOM (TATInformation::getInst()->getXMLFilename(), tatXML_DOM);
	
	Tools::parseDOMDocumentIntoListview (tatXML_DOM, lvwTATXML);
	
	tatXML_loaded = true;
}

//SLOT:
void TTTPositionXMLDisplay::expandItem()
{
	Q3ListView *lv = 0;
	
	if (sender() == pbtTATExpandAll)
		lv = lvwTATXML;
	else if (sender() == pbtImageExpandAll)
		lv = lvwImageXML;
	
	if (! lv)
		return;
	
	Q3ListViewItem *lvi = lv->selectedItem();
	
	if (lvi) {
		//parse item tree
		Tools::exp_collListViewItems (lvi, true, true);
	}
}

//SLOT:
void TTTPositionXMLDisplay::collapseItem()
{
	Q3ListView *lv = 0;
	
	if (sender() == pbtTATCollapseAll)
		lv = lvwTATXML;
	else if (sender() == pbtImageCollapseAll)
		lv = lvwImageXML;
	
	if (! lv)
		return;
	
	Q3ListViewItem *lvi = lv->selectedItem();
	
	if (lvi) {
		//parse item tree
		Tools::exp_collListViewItems (lvi, false, true);	
	}
}

void TTTPositionXMLDisplay::setTimepoint()
{
	timepoint = lieTimepoint->text().toInt();
	
	loadImageXML();
}
	
void TTTPositionXMLDisplay::setWavelength()
{
	wavelength = lieWavelength->text().toInt();
	
	loadImageXML();
}

void TTTPositionXMLDisplay::init()
{
	std::vector<TTTPositionManager*> pmv = TTTManager::getInst().getAllPositionManagersSorted();
	
	//if (! pmv) {
	//	//something strange happened...
	//	return;
	//}
	
	cboPosition->clear();
	
	for (uint i = 0; i < pmv.size(); i++) {
		
		if (pmv[i]) {
			cboPosition->insertItem (pmv[i]->positionInformation.getIndex());
		}
	}
}
	
//#include "tttpositionxmldisplay.moc"
