/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef autotrackingcellcircle_h__
#define autotrackingcellcircle_h__

// Qt
#include <QGraphicsEllipseItem>
#include <QGraphicsTextItem>
#include <QSharedPointer>

// Project
#include "tttbackend/treefragment.h"


/**
 * @author Oliver Hilsenbeck
 *
 * Cell circle as used by TTTAutoTrackingTreeWindow, represents one TreeFragment instance
 */
class AutoTrackingCellCircle : public QObject, public QGraphicsEllipseItem {

	Q_OBJECT

public:

	/**
	 * Action associated with a cell circle, activated if the user clicks on it
	 */
	enum Action {
		A_NONE,
		A_CREATE_TREE_OR_INVALIDATE,
		A_SELECT_SUCCESSOR
	};

	/**
	 * Constructor
	 * @param _action associated action, determines if cell circle reacts to click events (will also change mouse cursor when moving over circle)
	 * @param _trackPoint associated trackpoint
	 * @param _text text string to draw next to the circle
	 * @param _position local position of the circle
	 * @param _color color of the circle
	 * @param _treeFragment tree fragment associated with this circle if any (can be 0)
	 * @param _tree tree associated with this circle if any (can be 0)
	 * @param _textColor color for the text
	 * @param _toolTip tooltip text
	 * @param _noOwnCursor prevent circle from changing mouse cursor
	 */
	AutoTrackingCellCircle(Action _action, const ITrackPoint* _trackPoint, QString _text, QPointF _position, int _radius, QColor _color, QWeakPointer<TreeFragment> _treeFragment, QSharedPointer<Tree> _tree, QColor _textColor, const QString _toolTip = "", bool _noOwnCursor = false);

	/**
	 * @return associated tree fragment (can be 0)
	 */
	QWeakPointer<TreeFragment> getTreeFragment() const {
		return treeFragment;
	}

	/**
	 * @return associated tree fragment as shared pointer (can be 0)
	 */
	QSharedPointer<TreeFragment> getTreeFragmentSharedPtr() const {
		return treeFragment.toStrongRef();
	}

	/**
	 * @return associated tree (can be 0)
	 */
	QSharedPointer<Tree> getTree() const {
		return tree;
	}

	/**
	 * @return associated trackpoint (can be 0)
	 */
	const ITrackPoint* getTrackPoint() const {
		return trackPoint;
	}

	/**
	 * @return action associated with this circle
	 */
	Action getAction() const {
		return action;
	}

	/**
	 * Adjust color.
	 * @param color new color.
	 */
	void setColor(const QColor& color, const QColor& textColor) {
		QPen pen (color, thickness);
		setPen(pen);
		if(textItem)
			textItem->setDefaultTextColor(textColor);
	}

	/**
	 * Get radius.
	 */
	int getRadius() const {
		return radius;
	}

	/**
	 * Set radius.
	 */
	void setRadius(int _radius);

	/**
	 * Get circle thickness.
	 */
	int getThickness() const {
		return thickness;
	}

signals:

	/**
	 * Reports that user has clicked on a displayed cell circle
	 * @param _cellCircle pointer to cell circle instance which raised the signal
	 * @param _button the mouse button (if any) that caused the event
	 */
	void cellClicked(AutoTrackingCellCircle* _cellCircle, Qt::MouseButton _button);

protected:

	// Mouse press event, to implement button functionality
	virtual void mousePressEvent( QGraphicsSceneMouseEvent* _event );

private:

	//// AutoTrackingImageView instance this circle belongs to
	//AutoTrackingImageView* autoTrackingImageView;

	// Description text
	QGraphicsTextItem* textItem;

	// Associated tree or tree fragments
	QSharedPointer<Tree> tree;
	QWeakPointer<TreeFragment> treeFragment;

	// Associated trackpoint
	const ITrackPoint* trackPoint;

	// Circle radius
	int radius;

	// Circle thickness
	int thickness;

	// Action
	Action action;
};

#endif // autotrackingcellcircle_h__
