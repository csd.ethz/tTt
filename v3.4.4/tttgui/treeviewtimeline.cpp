/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "treeviewtimeline.h"

// Qt
#include <QPen>
#include <QFont>
#include <QGraphicsSceneMouseEvent>

// Project
#include "treeview.h"
#include "../tttdata/userinfo.h"
#include "../tttbackend/tttpositionmanager.h"
#include "../tttbackend/tttmanager.h"
#include "../tttbackend/picturearray.h"


TreeViewTimeLine::TreeViewTimeLine( TreeView *_treeView )
	: QGraphicsRectItem(0, -TOP_MARGIN, TIME_LINE_DEFAULT_WIDTH, TIME_LINE_HEIGHT)
{
	// Init variables
	//displaySettings = TreeViewSettings::getInstance();
	m_treeView = _treeView;
	m_initialized = false;
	m_currentTimePointLine = 0;
	m_currentPositionIndex = -1;
	m_highlightedStart = -1;
	m_highlightedStop = -1;
	m_showRealTime = true;

	// Init position
	setPos(-TIME_LINE_DEFAULT_WIDTH, 0);
	setZValue(TreeView::Z_TIME_SCALE);

	// Disable outline, set light grey filling
	setPen(Qt::NoPen);

	// Set cursor
	setCursor(Qt::CrossCursor);

	// Set tool tip text
	setToolTip("Click to select images for loading (F8) or unloading (F9).");
}


void TreeViewTimeLine::initialize()
{
	// Make sure to do this only once
	if(m_initialized)
		return;

	// Set up
	setUpTimeLine();

	// Add to TreeView
	m_treeView->scene()->addItem(this);

	m_initialized = true;
}

void TreeViewTimeLine::setUpTimeLine()
{
	// Remove existing items
	QGraphicsScene* scene = m_treeView->scene();
	for(int i = 0; i < m_timeLabels.size(); ++i) {
		scene->removeItem(m_timeLabels[i]);
		delete m_timeLabels[i];
	} 
	m_timeLabels.clear();
	for(int i = 0; i < m_timeLines.size(); ++i) {
		scene->removeItem(m_timeLines[i]);
		delete m_timeLines[i];
	} 
	m_timeLines.clear();

	// Calculate needed width
	int neededWidth = TIME_LINE_DEFAULT_WIDTH;

	// Create labels
	bool labelsCreatedSuccessfully = false;
	if(m_showRealTime) {
		// Create real time labels
		TTTPositionManager* tttpm = TTTManager::getInst().getPositionManager(m_currentPositionIndex);
		if(tttpm) {
			int ftp = tttpm->getFirstTimePoint();
			int ltp = tttpm->getLastTimePoint();
			const FileInfoArray* fia = tttpm->getFiles();
			if(fia && ftp > 0 && ftp < ltp) {
				QDateTime prevDateTime = fia->getRealTime(ftp, 1, -1, false);
				int prevTimePoint = ftp;
				if(prevDateTime.isValid()) {
					int labelCount = 0;
					for(int curTp = ftp; curTp <= ltp; ++curTp) {
						QDateTime curDateTime = fia->getRealTime(curTp, 1, -1, false);
						if(curDateTime.isValid()) {
							int deltaTimeSeconds = prevDateTime.secsTo(curDateTime);
							if(deltaTimeSeconds > 3600 * 12) {
								++labelCount;
								
								// Create label item and line, if enough time points have passed since last label was added
								if((curTp - prevTimePoint)*m_treeView->getTreeHeightFactor() > 100) {
									QString label;
									if(labelCount % 2 == 0) 
										label = QString("d%1").arg(labelCount/2);
									else 
										label = QString("%1h").arg(labelCount*12);
									
									addTimeLabelAndLine(curTp, label, neededWidth);
								}

								prevDateTime = prevDateTime.addSecs(3600 * 12);
								prevTimePoint = curTp;
							}
						}
					}
				}
				labelsCreatedSuccessfully = true;
			}
		}
	}
	
	// If label creation with real time failed or if no real time desired, create simple "Tp X" lables
	if(!labelsCreatedSuccessfully) {
		for(int curTp = 0; curTp < TIME_LINE_HEIGHT; curTp += LABELS_DELTA) {	// ToDo: Change delta depending on tree window height factor
			addTimeLabelAndLine(curTp, QString("Tp %1").arg(curTp), neededWidth);
		}
	}

	// Set width
	updateWidth(neededWidth);
}

void TreeViewTimeLine::addTimeLabelAndLine( int timePoint, QString labelText, int& neededWidth )
{
	// Create time point label
	QGraphicsTextItem *newLabel = new QGraphicsTextItem(this);
	m_timeLabels.push_back(newLabel);
	newLabel->setPlainText(labelText);
	newLabel->setData(0, timePoint);

	// Set appearance
	int fontSize = UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TREEVIEW_TIMELINEFONTSIZE).toInt();
	newLabel->setFont(QFont("Arial", fontSize));
	QColor fontColor = UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TREEVIEW_TIMELINEFONTCOL).value<QColor>();
	newLabel->setDefaultTextColor(fontColor);

	// Update neededWidth
	QRectF sceneRect = newLabel->sceneBoundingRect();
	neededWidth = qMax(neededWidth, (int)sceneRect.width());

	// Set position
	newLabel->setPos(0, timePoint * m_treeView->getTreeHeightFactor() - sceneRect.height() / 2);

	// Init new line
	int lineWidth = UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TREEVIEW_TIMELINELINEWIDTH).toInt();
	QGraphicsRectItem *newLine = new QGraphicsRectItem(0, 0, TIME_LINES_LENGTH, lineWidth);
	m_timeLines.push_back(newLine);
	newLine->setPos(0, timePoint * m_treeView->getTreeHeightFactor());
	newLine->setData(0, timePoint);

	// Set appearance
	newLine->setPen(Qt::NoPen);
	QColor col = UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TREEVIEW_TIMELINELINECOLOR).value<QColor>();
	newLine->setBrush(QBrush(col));
	newLine->setZValue(TreeView::Z_TIME_VERTICAL_LINE);

	// Add to scene (as lines have no parent we need to do this)
	m_treeView->scene()->addItem(newLine);
}

int TreeViewTimeLine::getWidth() const
{
	return rect().width();
}

void TreeViewTimeLine::updateWidth( int _width )
{
	// Get current rect
	QRect curRect = rect().toRect();

	// Change width
	QRect newRect(curRect.x(), curRect.y(), _width, curRect.height());
	setRect(newRect);

	// Update position
	setPos(-_width, 0);

	// Update treeView
	m_treeView->updateSceneRect();
}

void TreeViewTimeLine::updateDisplay()
{
	// Make sure we have something to update
	if(m_timeLabels.isEmpty() && m_timeLines.isEmpty())
		return;

	// Set background color
	QColor bcol = UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TREEVIEW_TIMELINEBACKGROUNDCOLOR).value<QColor>(); 
	setBrush(QBrush(bcol));

	// Calculate needed width
	int neededWidth = TIME_LINE_DEFAULT_WIDTH;

	// Update labels
	int fsize = UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TREEVIEW_TIMELINEFONTSIZE).toInt();
	QColor fcol = UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TREEVIEW_TIMELINEFONTCOL).value<QColor>(); 
	for(int i = 0; i < m_timeLabels.size(); ++i) {
		// Update appearance
		m_timeLabels[i]->setFont(QFont("Arial", fsize));
		m_timeLabels[i]->setDefaultTextColor(fcol);

		// Update neededWidth
		QRectF sceneRect = m_timeLabels[i]->sceneBoundingRect();
		neededWidth = qMax(neededWidth, (int)sceneRect.width());

		// Update position
		QVariant tp = m_timeLabels[i]->data(0);
		if(!tp.isNull()) {
			m_timeLabels[i]->setPos(0, tp.toInt() * m_treeView->getTreeHeightFactor() - sceneRect.height() / 2);
		}
	}

	// Update lines
	QColor lcol = UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TREEVIEW_TIMELINELINECOLOR).value<QColor>(); 
	for(int i = 0; i < m_timeLines.size(); ++i) {
		// Update appearance
		m_timeLines[i]->setBrush(QBrush(lcol));

		// Update position
		QVariant tp = m_timeLabels[i]->data(0);
		if(!tp.isNull())
			m_timeLines[i]->setPos(0, tp.toInt() * m_treeView->getTreeHeightFactor());
	}

	// Update width
	updateWidth(neededWidth);

	// Redraw
	update();
}

void TreeViewTimeLine::setCurrentTimePoint( int _tp )
{
	// Check if _tp is valid
	if(_tp <= 0)
		return;

	// Create timeline
	if(!m_currentTimePointLine) {
		m_currentTimePointLine = new QGraphicsRectItem(0, 0, TIME_LINES_LENGTH, 15);

		// Set appearance
		m_currentTimePointLine->setPen(Qt::NoPen);
		m_currentTimePointLine->setBrush(QBrush(QColor (20, 255, 30)));
		m_currentTimePointLine->setZValue(TreeView::Z_TIME_CURRENT_TP_LINE);

		// Add to scene (as lines have no parent we need to do this)
		m_treeView->scene()->addItem(m_currentTimePointLine);		
	}

	// Update position
	m_currentTimePointLine->setPos(0, _tp * m_treeView->getTreeHeightFactor());
}

void TreeViewTimeLine::mouseReleaseEvent( QGraphicsSceneMouseEvent* event )
{
	// Get position
	QPointF pos = event->scenePos();

	// Get timepoint
	int tp = std::max(0, static_cast<int>(pos.y()+0.5) / m_treeView->getTreeHeightFactor());

	// Notify treeview
	m_treeView->notifyTimeLineClicked(tp, event->button());
}

void TreeViewTimeLine::mousePressEvent( QGraphicsSceneMouseEvent* event )
{
	// Needs to be implemented to receive mouseReleaseEvent
}

void TreeViewTimeLine::setCurrentPosition( int positionIndex )
{
	// Set index and update display
	m_currentPositionIndex = positionIndex;
	setUpTimeLine();
}

void TreeViewTimeLine::paint( QPainter* _painter, const QStyleOptionGraphicsItem* _option, QWidget* _widget /*= 0*/ )
{
	// Draw rectangle
	QGraphicsRectItem::paint(_painter, _option, _widget);

	// Get loaded images
	TTTPositionManager* tttpm = TTTManager::getInst().getPositionManager(m_currentPositionIndex);
	if(!tttpm)
		return;
	PictureArray* pics = tttpm->getPictures();
	if(!pics)
		return;

	// ToDo: Color..
	_painter->setPen(Qt::NoPen);
	
	// Draw loaded images
	_painter->setBrush(QBrush(Qt::green, Qt::SolidPattern));
	int maxNumOfTps = TIME_LINE_HEIGHT / m_treeView->getTreeHeightFactor();
	for(int i = 0; i <= std::min(tttpm->getLastTimePoint(), maxNumOfTps); ++i) {
		int tp = i + 1;

		if(pics->isLoaded(tp, -1, -1)) {
			int curPosY = i * m_treeView->getTreeHeightFactor();
			_painter->drawRect(0, curPosY, 15, m_treeView->getTreeHeightFactor());
		}
	}

	// Draw highlighted region
	if(m_highlightedStart > 0 && m_highlightedStop > 0 && m_highlightedStop >= m_highlightedStart) {
		// Map time points to y-positions (do not allow time points higher than last time point of position)
		int yStart = (std::min(tttpm->getLastTimePoint(), m_highlightedStart)-1) * m_treeView->getTreeHeightFactor();
		int yStop = std::min(tttpm->getLastTimePoint(), m_highlightedStop) * m_treeView->getTreeHeightFactor();

		_painter->setBrush(QBrush(Qt::darkGreen, Qt::SolidPattern));
		_painter->drawRect(15, yStart, 15, yStop - yStart);
	}
}

void TreeViewTimeLine::setHighlighting( int startTp /*= -1*/, int stopTp /*= -1*/ )
{
	// Change
	m_highlightedStart = startTp;
	m_highlightedStop = stopTp;

	// Redraw
	update();
}

void TreeViewTimeLine::setShowRealTime(bool showRealTime)
{
	if(m_showRealTime == showRealTime)
		return;

	// Change and recreate 
	m_showRealTime = showRealTime;
	setUpTimeLine();
}


void TreeViewTimeLine::setVisible(bool visible)
{
	// Hide/show children
	for(int i = 0; i < m_timeLabels.size(); ++i)
		m_timeLabels[i]->setVisible(visible);
	for(int i = 0; i < m_timeLines.size(); ++i)
		m_timeLines[i]->setVisible(visible);
	if(m_currentTimePointLine)
		m_currentTimePointLine->setVisible(visible);

	// Hide/show self
	QGraphicsRectItem::setVisible(visible);
}

void TreeViewTimeLine::setCurrentTimePointBarVisible(bool visible)
{
	if(m_currentTimePointLine)
		m_currentTimePointLine->setVisible(visible);
}


//QString TreeViewTimeLine::getTimeScaleItemLabel( int tp, TTTPositionManager* tttpm )
//{
//	// If desired, return only tp as string
//	if(!m_showRealTime || !tttpm || !tttpm->getFiles() || tttpm->getFirstTimePoint() < 1)
//		return QString("Tp %1").arg(tp);
//
//	// Calculate delta time to first time point in seconds
//	int fistTp = tttpm->getFirstTimePoint();
//	int delta = tttpm->getFiles()->calcSeconds(fistTp, tp, false);
//	if(delta < 1)
//		return QString("Tp %1").arg(tp);
//
//	if(delta < 60)
//		return QString("%1sec").arg(delta);
//	else if(delta < 60*60)
//		return QString("%1min").arg(delta / 60);
//	else if(delta < 60*60*24)
//		return QString("%1h").arg(delta / (60*60));
//	else
//		return QString("d%1").arg(delta / (60*60*24));
//}


//void TreeViewTimeLine::updateDisplay()
//{
//	// Move timeline
//	QPointF viewOriginScene = treeView->mapToScene(0, 0);
//	setPos(viewOriginScene.x(), 0);
//
//	// Scale timeline
//	float scale = 1.0f / treeView->getZoom();
//	setScale(scale);
//}
