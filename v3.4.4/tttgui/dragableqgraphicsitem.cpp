/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "dragableqgraphicsitem.h"

#include <QtGui>

DragableQGraphicsItem* DragableQGraphicsItem::draggedItem (0);

DragableQGraphicsItem::DragableQGraphicsItem (ItemShape _shape)
        : QGraphicsItem ()
{
        setToolTip ("Drag this item with the mouse - see what happens...");
        setCursor (Qt::OpenHandCursor);

        shape = _shape;
        setLineLength (25);
        setEllipseRadius (20);
}

QRectF DragableQGraphicsItem::boundingRect() const
{
        switch (shape) {
                case DGIS_Ellipse:
                        return QRectF(-11, -11, ellipseRadius + 4, ellipseRadius + 4);
                case DGIS_Line:
                        return QRectF (0, 0, 2, lineLength);
        }

}

void DragableQGraphicsItem::paint (QPainter *_painter, const QStyleOptionGraphicsItem *_option, QWidget *_widget)
{
        Q_UNUSED (_option);
        Q_UNUSED (_widget);

        switch (shape) {
                case DGIS_Ellipse:
                        _painter->setPen (QPen (LINECOLOR, 1));
                        _painter->setBrush (QBrush (LINECOLOR));
                        _painter->drawEllipse (-11, -11, ellipseRadius, ellipseRadius);
                        break;
                case DGIS_Line:
                        _painter->setPen (QPen (LINECOLOR, 1));
                        _painter->drawLine (0, 0, 0, lineLength);
                        break;
        }

}

void DragableQGraphicsItem::mousePressEvent (QGraphicsSceneMouseEvent *_ev)
{
        if (_ev->button() != Qt::LeftButton) {
                _ev->ignore();
                return;
        }

        //start dragging
        setCursor (Qt::ClosedHandCursor);
        draggedItem = this;
        positionBeforeDrag = mapToScene (_ev->pos());
}

void DragableQGraphicsItem::mouseMoveEvent (QGraphicsSceneMouseEvent *_ev)
{
//        if (QLineF(event->screenPos(), event->buttonDownScreenPos(Qt::LeftButton)).length() < QApplication::startDragDistance()) {
//                return;
//        }

        QDrag *drag = new QDrag (_ev->widget());
        QMimeData *mime = new QMimeData;
        mime->setText (GRAPHICS_ADJUST_MIME_DATA_TEXT);
        drag->setMimeData (mime);

        switch (shape) {
                case DGIS_Ellipse: {
                        mime->setColorData (LINECOLOR);

                        QPixmap pixmap (24, 24);
                        pixmap.fill (Qt::white);

                        QPainter painter (&pixmap);
                        painter.translate (11, 11);
                        painter.setRenderHint (QPainter::Antialiasing);
                        paint (&painter, 0, 0);
                        painter.end();

                        pixmap.setMask (pixmap.createHeuristicMask());

                        drag->setPixmap (pixmap);
                        drag->setHotSpot (QPoint (15, 20));
                        break;
                }
                case DGIS_Line:
                        break;
        }

        drag->exec();
        setCursor(Qt::OpenHandCursor);
}

void DragableQGraphicsItem::mouseReleaseEvent (QGraphicsSceneMouseEvent *)
{
        setCursor (Qt::OpenHandCursor);
        draggedItem = 0;
}

void DragableQGraphicsItem::setLineLength (float _length)
{
        lineLength = _length;
}

void DragableQGraphicsItem::setEllipseRadius (float _radius)
{
        ellipseRadius = _radius;
}

void DragableQGraphicsItem::wasDropped (QPointF _scenePos)
{
        //@ todo make actions parameter controlled

        if (shape == DGIS_Ellipse)
                //remove x shift for gamma circles
                _scenePos.setX (positionBeforeDrag.x());

        setPos (_scenePos);

        //instead of emitting a signal, invoke the callback method
        callCBMR (this, 0);
}
