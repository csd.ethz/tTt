/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef treeviewtreeitem_h__
#define treeviewtreeitem_h__

// Qt
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QHash>

// Forward declarations
class TreeView;
class TreeViewTrackItem;
class ITree;
class ITrack;
//class TreeViewSettings;
class TreeViewTreeNameItem;
class TreeViewClickableIconItem;


/**
 * @author Oliver Hilsenbeck
 *
 * Represents a tree that is displayed in an instance of the TreeView class.
 * The y-position of a TreeViewTreeItem must always be 0 or the tree will not be displayed correctly with respect to the timeline
 */
class TreeViewTreeItem : public QGraphicsItem {

public:

	/**
	 * Constructor, inits variables and call updateDisplay
	 * @param _treeView the associated TreeView instance
	 * @param _tree the tree
	 * @param _posX the x position in scene units where tree should be added
	 * @param _displayCloseIcon if a close icon should be displayed
	 * @param _displaySaveIcon if a save icon should be displayed if tree has been modified
	 */
	TreeViewTreeItem(TreeView *_treeView, ITree *_tree, int _posX, bool _displayCloseIcon = false, bool _displaySaveIcon = false);

	/**
	 * Updates the display, reconstructs the tree display objects to reflect changes in the tree
	 */
	void updateDisplay();

	/**
	 * Update the tree string
	 */
	void updateTreeNameDisplay();

	/**
	 * @return pointer ITree this item represents
	 */
	ITree* getTree() const {
		return tree;
	}

	/**
	 * Return actual size of the tree item including all children (In scene coordinates)
	 */
	QSize sizeIncludingChildren() const;

	/**
	 * @return bounding rectangle around area of this tree item. Overwritten from QGraphicsItem
	 */
	QRectF boundingRect() const;

	/**
	 * Paint this tree (does nothing). Overwritten from QGraphicsItem
	 */
	void paint(QPainter* _painter, const QStyleOptionGraphicsItem* _option, QWidget* _widget = 0);

	/**
	 * @return the TreeView instance this belongs to
	 */
	TreeView* getTreeView() const {
		return treeView;
	}

	/**
	 * Get TreeViewTrackItem instance by track number
	 * @param _trackNum track number
	 * @return Pointer to TreeViewTrackItem by track number or 0 if _trackNum was not found
	 */
	TreeViewTrackItem* getTrackItem(int _num) const;

	/**
	 * @return width of tree
	 */
	int getWidth() const {
		return width;
	}

	/**
	 * Set selection state
	 * @param _selected true if tree should be selected, false if unselected
	 */
	void setSelectionState(bool _selected);

	/**
	 * Close button clicked
	 */
	void closeClicked();

	/**
	 * Save button clicked
	 */
	void saveClicked();

	/**
	 * Specify if tree name and icons should be shown.
	 */
	void setShowTreeNameAndIcons(bool show);

	/**
	 * Show/hide track numbers.
	 */
	void setTrackNumbersVisible(bool visible);

private:

	/**
	 * @param _trackNumber the maximum tracknumber
	 * @return the tree height of a tree with the provided maximum track number
	 */
	int getHeightFromMaxTrackNumer(unsigned int _maxTrackNumber) const;

	/**
	 * Calculate width of displayed tree from tree height (assuming that tree is complete, 
	 * so this is a max width actually; it does, however, not include wavelength lines and so on)
	 * @param _height the tree height
	 * @return width in scene units
	 */
	int getWidthFromTreeHeight(unsigned int _height) const;

	/**
	 * Recursively create TreeViewTrackItem instances for _track and all its children
	 * @param _track the track
	 * @param _x the x coordinate for _track in parent coordinates
	 * @param _width the available width for this track (and all its children)
	 */
	void createTreeViewTrackItems(ITrack* _track, int _x, int _width);

	/**
	 * Removes all children from the scene and deletes them, clears tracks. Note: this function
	 * assumes that elements of the tree may have been deleted, so e.g. pointers to tracks must
	 * not be used.
	 */
	void removeAllChildren();

	/**
	 * Create close button (if we dont have one yet) and set position
	 * @return true if successful
	 */
	bool createCloseButton();

	/**
	 * Create save button (if we dont have one yet) and set position
	 * @return true if successful
	 */
	bool createSaveButton();

	/**
	 * Get display string for current tree (usually file name or '(new tree)' and with '*' at end for modified trees)
	 * @return display string
	 */
	QString getTreeDisplayString() const;

private:

	//// Display settings
	//TreeViewSettings *displaySettings;

	// The tree to be drawn by this class
	ITree* tree;

	// The tree view we belong to
	TreeView *treeView;

	// Tree item specific settings
	bool displayCloseIcon;
	bool displaySaveIcon;

	// If tree is selected
	bool selected;

	// Track items, by track numbers
	QHash<int, TreeViewTrackItem*> tracks;
	typedef QHash<int, TreeViewTrackItem*>::iterator tracks_iterator;
	typedef QHash<int, TreeViewTrackItem*>::const_iterator const_tracks_iterator;

	// Vertical cell division lines
	QList<QGraphicsRectItem*> cellDivisionLines;

	// Treename
	TreeViewTreeNameItem *treeNameItem;

	// Icons
	TreeViewClickableIconItem *closeIcon;
	TreeViewClickableIconItem *saveIcon;

	// Width in scene units and generation count 
	int width, generationCount;
};


#endif // treeviewtreeitem_h__
