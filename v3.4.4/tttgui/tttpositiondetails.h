/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TTTPOSITIONDETAILS_H
#define TTTPOSITIONDETAILS_H

#include "ui_frmPositionDetails.h"

#include "positionthumbnail.h"
#include "pictureview.h"
#include "paintableqframe.h"

#include <qpainter.h>
#include <qimage.h>
#include <qpixmap.h>
#include <qevent.h>
#include <qtimer.h>
//#include <qbuttongroup.h>
#include <qcombobox.h>
//Added by qt3to4:
#include <QPaintEvent>
#include <QResizeEvent>
#include <QShowEvent>
#include <QCloseEvent>
#include <QWidget>

class TTTPositionDetails: public QWidget, public Ui::frmPositionDetails {

Q_OBJECT

public:

	TTTPositionDetails(QWidget *parent = 0, const char *name = 0);
	
	~TTTPositionDetails();
	
	/**
	 * sets the current PositionThumbnail object (necessary for picture access)
	 * @param _thumbnail the selected position thumbnail
	 */
	void setPositionThumbnail (PositionThumbnail* _thumbnail);
	
	void paintEvent (QPaintEvent *);
	void resizeEvent (QResizeEvent *);
	void showEvent (QShowEvent *);
	void closeEvent (QCloseEvent *);
	
	/**
	 * @return the displayed image (ususally the first in the position folder)
	 */
	QImage* getDisplayedImage();
	
//signals:
//	
//	///emitted when the form is closed
//	///@param : the current thumbnail
//	void positionStatisticClosed (PositionThumbnail *);
	
private slots:
	
	/**
	 *  draws the current picture from memory
	 */
	void draw();
	
        /* *
	 * just calls draw(); necessary for the tab widget
	 * @param : not of interest
	 */
        //void draw (const QString &);
	
	/**
	 * sets the provided wavelength to be displayed
	 * @param _wavelength the wavelength (between 0 and MAX_WAVE_LENGTH, included)
	 */
	void setWavelength (const QString &_wavelength);
	
private:
	
	///the current picture in original size
	///is set in setPositionThumbnail
	QImage *img;
	
	///the current picture with tracks and other stuff
	///is scaled from img to fit the current display widget size
	QPixmap *pix;
	
	///the timer for drawing, as paintEvent() and draw() do not want to cooperate as they should...
	QTimer drawTimer;
	
	///the current PositionThumbnail (which was set in setPositionThumbnail)
	PositionThumbnail *thumbnail;
	
	///store the size of the first picture in wavelength 0 (phase contrast)
	///necessary for calculating the zoom factor for the tracks, if the fluorescent pictures are of different size
	int wl0width;
	int wl0height;

	///window layout already loaded
	bool windowLayoutLoaded;
	
        ///the QFrame object that heeds the graphical drawing pipeline, is initialized in the constructor
        PaintableQFrame *fraPicture;
};


inline QImage* TTTPositionDetails::getDisplayedImage()
{
	return img;
}


#endif
