/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "tttcellcomment.h"
//Added by qt3to4:
#include <QCloseEvent>

#include "tttdata/userinfo.h"

TTTCellComment::TTTCellComment(QWidget *parent)
    :QDialog(parent)
{
        setupUi (this);

	Comment = "";
	windowLayoutLoaded = false;
	
	//txeCellComment->setTextFormat (PlainText);
	
	connect ( pbtSave, SIGNAL (clicked()), this, SLOT (save()));
	connect ( pbtCancel, SIGNAL (clicked()), this, SLOT (cancel()));
}

void TTTCellComment::closeEvent (QCloseEvent *ev)
{
	// Save window layout
	UserInfo::getInst().saveWindowPosition(this, "TTTCellComment");

	ev->accept();
}

//EVENT:
void TTTCellComment::showEvent (QShowEvent *_ev)
{
	if(!_ev->spontaneous() && !windowLayoutLoaded) {
		// Load window layout
		UserInfo::getInst().loadWindowPosition(this, "TTTCellComment");

		windowLayoutLoaded = true;
	}

	_ev->accept();
}

void TTTCellComment::save()
{
	Comment = txeCellComment->text();
	emit commentSaved (Comment);
	close();
}

void TTTCellComment::cancel()
{
	Comment = "";
	emit cancelled();
	close();
}

void TTTCellComment::setComment (QString _comment)
{
	Comment = _comment;
	txeCellComment->setText (_comment);
}

//#include "tttcellcomment.moc"
