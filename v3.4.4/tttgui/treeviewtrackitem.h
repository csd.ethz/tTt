/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef treeviewtrackitem_h__
#define treeviewtrackitem_h__

// Qt
#include <QGraphicsItem>
#include <QGraphicsRectItem>

// Forward declarations
class ITrack;
class TreeViewTreeItem;
//class TreeViewSettings;
class TreeViewCellCircle;



/**
 * @author Oliver Hilsenbeck
 *
 * Represents one track of a TreeViewTreeItem and corresponds to an ITrack instance. Responsible for drawing vertical track 
 * lines and wavelength lines. It also manages cell circles and symbols (as child items).
 * Vertically every timepoint corresponds to one unit in scene coordinates.
 * Every track starts at first timepoint - 1 and ends at last timepoint
 */
class TreeViewTrackItem : public QGraphicsItem {

public:

	/**
	 * Constructor
	 * @param _track the ITrack instance this object represents, must not be NULL
	 * @param _treeItem the tree item this belongs to
	 * @param _x the x coordinate for this item relative to its parent (which is _treeItem)
	 */
	TreeViewTrackItem(ITrack* _track, TreeViewTreeItem* _treeItem, int _x);

	/**
	 * Update display e.g. after settings have changed or trackpoints were added or removed
	 */
	void updateDisplay();

	/**
	 * Set highlighted region of track.
	 */
	void setHighlighting(int startTp, int stopTp);

	/**
	 * @return pointer asscociated to associated ITrack object
	 */
	ITrack* getTrack() const {
		return track;
	}

	/**
	 * @return associated TreeViewTreeItem instance
	 */
	TreeViewTreeItem* getTreeItem() const {
		return treeItem;
	}

	/**
	 * Set selection state. updates display automatically
	 * @param _selected new selection state
	 */
	void setSelectionState(bool _selected);

	/**
	 * @return if this track is currently selected
	 */
	bool isSelected() const {
		return selected;
	}

	/**
	 * Called by TreeViewCellCircle instance that belongs to this object when user has clicked on it with left or right button
	 * @parameter _button the mouse button that was used
	 */
	void notifyMouseReleaseEvent(Qt::MouseButton _button);

	/**
	 * @return bounding rectangle around area of this track item. Overwritten from QGraphicsItem
	 */
	QRectF boundingRect() const;

	/**
	 * Show/hide track number.
	 */
	void setTrackNumberVisible(bool visible);

	/**
	 * Paint this track. Overwritten from QGraphicsItem
	 */
	void paint(QPainter* _painter, const QStyleOptionGraphicsItem* _option, QWidget* _widget = 0);

	/**
	 * The maximum number of wavelength lines that can be displayed in a tree (excluding wavelength 0)
	 */
	static const int MAX_DISPLAYED_WAVELENGTHS = 9;

protected:

	// Mouse press event: opens movie window
	void mousePressEvent( QGraphicsSceneMouseEvent *_event );

private:

	/**
	 * @return width of item
	 */
	int calcWidth() const;

	/**
	 * @return height of item
	 */
	int calcHeight() const;

	/**
	 * Update associated symbols (e.g. cell death)
	 */
	void updateSymbols();

	// The track
	ITrack *track;

	// Tree item
	TreeViewTreeItem *treeItem;

	// Circle
	TreeViewCellCircle* cellCircle;

	// Tracknumber
	QGraphicsTextItem* trackNumberItem;

	// Symbol items (e.g. apoptosis symbol)
	QList<QGraphicsTextItem*> symbols;

	//// Display settings
	//TreeViewSettings *displaySettings;

	// The x coordinate
	int x;

	// If this track is currently selected (by clicking on cellCircle)
	bool selected;

	// Tree width and height during last paint event
	int m_treeWidthAtLastPaint;
	int m_treeHeightAtLastPaint;

	// Highlighted region of track, set to -1 if nothing is highlighted
	int m_hightlightStartTp;
	int m_hightlightStopTp;

	// Default WL colors
	static const QColor DEFAULT_WL_COLORS[];
};


#endif // treeviewtrackitem_h__
