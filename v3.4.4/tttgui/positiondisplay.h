/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef POSITIONDISPLAY_H
#define POSITIONDISPLAY_H

// Project includes
#include "positionthumbnail.h"

// Qt3 includes
#include <QGraphicsView>



/**
	@author Bernhard
	
	This class can display many small position frames on an arbitrary huge canvas.
	
	Some basics:\n
	
	All positions have the same size.
	The visible range depends on the microscope scaling factors (TV adapter and ocular).
	The factor micrometer -> pixel is calculated from the size in micrometer and the size in pixel.
	
*/

class PositionDisplay : public QGraphicsView
{

	Q_OBJECT
	
public:
	
	/**
	* Constructor, just created the widget without contents
	*/
	PositionDisplay (QWidget *_parent);

	/**
	* Load position thumbnails (does not check if this has already been done!)
	* @param _drawThumbNailFrame if frame around thumbnails should be drawn
	* @param _exclusiveSelection if at most one position can be selected at the same time
	* @param _updatePosManagers if pointers to thumbnails in position managers should be set to the created thumbnails
	*/
	void initialize(bool _drawThumbNailFrame = true, bool _exclusiveSelection = true, bool _updatePosManagers = false);

	/**
	 * @return if initialization has already been done successfully
	 */
	bool isInitialized() const {
		return initialized;
	}

	/**
	* Returns the thumbnail corresponding to the provided position index, 0 if it does not exist
	* @param _index the position index
	* @return the thumbnail for the provided index, or 0 if it does not exist
	*/
	PositionThumbnail* getThumbnail (const QString& _index) const;

	/**
	 * Update display according to current tv-factor, ocular factor and other display relevant settings
	 */
	void updateDisplay();

	/**
	 * Disable or enable frames of thumbnails, automatically updates display
	 */
	void setDrawThumbNailFrames(bool _on);
	
	/**
	 * @return drawThumbNailFrames
	 */
	bool getDrawThumbNailFrames() const {
		return drawThumbNailFrames;
	}

	/**
	 * Disable or enable text on thumbnails, automatically updates display
	 */
	void setDrawText(bool _on);

	/**
	 * @return drawText
	 */
	bool getDrawText() const {
		return drawText;
	}

	/**
	 * Disable or enable if treecount refers to tree fragments instead of trees
	 */
	void setTreeCountTreeFragments(bool _on);

	/**
	 * @return treeCountTreeFragments
	 */
	bool getTreeCountTreeFragments() const {
		return treeCountTreeFragments;
	}

	/**
	 * Get list of active (i.e. selected) positions
	 * @return list of position managers of active (selected) positions. Can be empty
	 */
	QList<TTTPositionManager*> getActivePositions() const;

	/**
	 * check if position is selected
	 * @param _index index of position to check
	 * @return true position exists and is selected
	 */
	bool isPositionSelected(const QString& _index);

	/**
	 * Mark pictures loaded for specified position
	 * @param _index index of position
	 * @param _loaded if pictures are loaded or not
	 * @return true if no error occurred
	 */
	bool markPicturesLoaded(const QString& _index, bool _loaded = true);

	/**
	 * Select position thumbnail
	 * @param _index index of position to select
	 * @return true if successful (i.e. if no error occurred)
	 */
	bool selectPosition(const QString& _index);

	/**
	 * Deselect position thumbnail
	 * @param _index index of position to select. if this string is empty, all position will be unselected (call updateDisplay() afterwards).
	 * @return true if successful (i.e. if no error occurred)
	 */
	bool deSelectPosition(const QString& _index);

	/**
	 * Clear display. Deletes all thumbnails
	 */
	void clearView();

	/**
	 * Change the timepoint used for the thumbnail preview images.
	 * For the changes to take effect, initialize() must be called after this function!
	 * @param _timePoint new timepoint
	 */
	void setThumbnailsTimePoint(int _timePoint) {
		thumbnailsTimePoint = _timePoint;
	}

	/**
	 * Change the wavelength used for the thumbnail preview images.
	 * For the changes to take effect, initialize() must be called after this function!
	 * @param _timePoint new timepoint
	 */
	void setThumbnailsWavelength(int _wl) {
		thumbnailsWaveLength = _wl;
	}

	/**
	 * Convert global micrometer coordinates to coordinates used in position display.
	 * @param point point in experiment wide micrometers.
	 * @return point in local coordinate system used by this QGraphicsView's scene.
	 */
	QPointF convertGlobalToLocal(const QPointF& point) const;

	/**
	 * Zoom function.
	 * @param _scaleFactor scaling factor, 1.0f for 100% zoom.
	 */
	void scaleView(qreal _scaleFactor);

	/**
	 * Get current scaling factor.
	 */
	float getCurrentScaling() const {
		return scalingFactor;
	}

	/////a conversion factor for displaying the thumbnails in the correct sizes
	/////is completely independent of the real microscope values which are defined in WavelengthInformation and is just used for this display - nothing else
	//static const float PIXEL_PER_MICROMETER_FOR_DISPLAY;

	// Minimum and maximum scaling values
	static const float MAX_SCALE;
	static const float MIN_SCALE;


signals:

	/**
	* emitted when a position was left clicked
	* @param _lastIndex index of position that was selected before
	*/
	void positionLeftClicked (const QString &_index);

	/**
	 * emitted when a position was double clicked
	 * @param _index the index of the selected position
	 */
	void positionDoubleClicked (const QString &_index);
	
	/**
	 * emitted when the user clicked on a position with the right mouse button
	 * @param _index the index of the selected position
	 */
	void positionRightClicked (const QString &_index);

	/**
	 * emitted when zoom has been changed
	 * @param _zoom the new scaling factor
	 */
	void zoomChanged(float _zoom);

protected:

	// Mouse wheel event, used for zoom
	void wheelEvent(QWheelEvent *_event);

	// Mouse button and move events, used for navigation with mouse
	void mousePressEvent( QMouseEvent *_event );
	void mouseReleaseEvent( QMouseEvent *_event );
	void mouseMoveEvent ( QMouseEvent *_event );

private:

	/**
	 * Emits double click signal, called by thumbnails
	 * @param _index index of left clicked position
	 */
	void reportPositionLeftClicked(const QString& _index);

	/**
	 * Emits double click signal, called by thumbnails
	 * @param _index index of double clicked position
	 */
	void reportPositionDoubleClicked(const QString& _index);

	/**
	 * Emits right click signal called by thumbnails
	 * @param _index index of right clicked position
	 */
	void reportPositionRightClicked(const QString& _index);

	/**
	* called when the sizes of the position thumbnails have to change due to a change in the microscope factor settings
	*/
	void updateSizes();

	// Thumbnails by position index
	QHash<QString, PositionThumbnail*> thumbnails;

	// If at most one position can be selected at the same time
	bool exclusiveSelection;

	// Current scaling factor
	float scalingFactor;

	// Position that was selected at last (if exclusive this is the only selected position)
	QString indexOfLastSelectedPosition;

	// If thumbnails should have frames
	bool drawThumbNailFrames;

	// If text on thmbunails should be dranw
	bool drawText;

	// If tree count should refer to tree fragments
	bool treeCountTreeFragments;

	// Already initialized
	bool initialized;

	// Mouse navigation
	int mouseMoveStartX, mouseMoveStartY;
	bool scrolling;

	// Timepoint and wavelength of pictures used for thumbnails
	int thumbnailsTimePoint;
	int thumbnailsWaveLength;

	//	/**
	//* sets the microscope ocular factor
	//* @param _of the ocular factor
	//*/
	//void setOcularFactor (int _of);
	//	
	///**
	//* sets the TV adapter factor
	//* @param _tvf the tv adapter factor
	//*/
	//void setTVFactor (float _tvf);

	///**
	// * sets whether the coordinate system is inverted
	// * @param _inverted true, if the coordinate system is inverted (positive axes are up and left); false, if it is normal (positive axes are down and right)
	// */
	//void setInverted (bool _inverted);
//	// 28/03/11-OH: Deprecated
//	///**
//	// * creates the signal/slot connections
//	// */
//	//void setConnections();
//	

//	

//	

//	
//	/**
//	 * sets both microscope factors to avoid updating the size twice
//	 * @param _ocularFactor the ocular factor
//	 * @param _tvFactor the tv adapter factor
//	 */
//	void setFactors (int _ocularFactor, float _tvFactor);
//	

//	
//	/**
//	 * adds a child widget (saves it in the local array and adds it with the QScrollView method)
//	 * @param _child 
//	 * @param _x 
//	 * @param _y 
//	 */
//	void addChildLocal (PositionThumbnail *_child, int _x = 0, int _y = 0);
//	
//	/**
//	 * @return all thumbnails in this view
//	 */
//	const Q3IntDict<PositionThumbnail>& getThumbnails() const
//		{return children;}
//	

//
//	/**
//	 * Sets the current thumbnail
//	 */
//	void setThumbnail(PositionThumbnail* _tn);
//	
//	
//signals:
//	
//	/**
//	 * emitted when a position was selected (the user clicked on it with the left mouse button)
//	 * @param _index the index of the selected position
//	 * @param _thumbnail the selected thumbnail widget
//	 * @param _oldThumbnail the thumbnail that was selected before the click (can be 0)
//	 */
//	void positionSelected (const QString &, PositionThumbnail*, PositionThumbnail*);
//	
//	/**
//	 * emitted when a position was double clicked
//	 * @param _index the index of the selected position
//	 * @param _thumbnail the selected thumbnail widget
//	 * @param _oldThumbnail the thumbnail that was selected before the click (can be 0)
//	 */
//	void positionDoubleClicked (const QString &, PositionThumbnail*, PositionThumbnail*);
//	
//	/**
//	 * emitted when the user clicked on a position with the right mouse button
//	 * @param _index the index of the selected position
//	 * @param _thumbnail the selected thumbnail widget
//	 * @param _oldThumbnail the thumbnail that was selected before the click (can be 0)
//	 */
//	void positionRightClicked (const QString &, PositionThumbnail*, PositionThumbnail*);
//	
//private slots:
//	
//	//wrap the signals of PositionThumbnail to signals of this class
//	//for parameter description cf. TTTPositionLayout
//	void wrapLeftClick (const QString &, PositionThumbnail *);
//	void wrapRightClick (const QString &, PositionThumbnail *);
//	void wrapDoubleClick (const QString &, PositionThumbnail *);
//	
//private:
//	
//	
//	///holds the currently selected thumbnail
//	///necessary for some signals as old current thumbnail
//	PositionThumbnail *currentPT;
//	
//	///keeps the current children
//	Q3IntDict<PositionThumbnail> children;
//	
////private methods
//	

	
	friend class PositionThumbnail;
};

#endif
