/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TTTCELLCOMMENT_H
#define TTTCELLCOMMENT_H

#include "ui_frmCell_Comment.h"

#include <qstring.h>
#include <qevent.h>
#include <qpushbutton.h>
#include <q3textedit.h>
//Added by qt3to4:
#include <QCloseEvent>
//#include <qlineedit.h>
#include <QDialog>

/**
@author Bernhard

	This class is the GUI for editing comments
*/

class TTTCellComment: public QDialog, public Ui::frmCellComment {
Q_OBJECT
public:
        TTTCellComment(QWidget *parent = 0);
	
	void closeEvent (QCloseEvent *);

	void showEvent (QShowEvent *_ev);
	
	/**
	 * sets the comment in the textedit control
	 * @param _comment the current comment which should be displayed
	 */
	void setComment (QString _comment);
	
signals:
	
	/**
	 * emitted when the user clicks "Save"
	 * @param _comment the comment that was entered by the user
	 */
	void commentSaved (QString _comment);
	
	/**
	 * emitted when the user clicks "Cancel"
	 */
	void cancelled();
	
public slots:
	
	/**
	 * called by button "Save"
	 */
	void save();
	
	/**
	 * called by button "Cancel"
	 */
	void cancel();
	
private:

	///the current comment
	QString Comment;

	///window layout already loaded
	bool windowLayoutLoaded;
};

#endif
