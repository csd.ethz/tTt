/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef movienextpositem_h__
#define movienextpositem_h__

// Qt
#include <QGraphicsRectItem>

// Forward declarations
class QGraphicsSceneHoverEvent;
class QGraphicsSimpleTextItem;


/**
 * @author Oliver
 *
 * This class represents a rectangular button that is used to open a neighbor position from
 * within the movie window.
 */
class MovieNextPosItem : public QGraphicsRectItem {

public:
	// Constructor
	MovieNextPosItem(const QString& _posKey);

	/**
	 * static helper function. Converts a experiment wide position rectangle in micrometers
	 * to a rectangle relative to the point _myLeft and _myTop in pixels
	 * @param _rect position rectangle in micrometers
	 * @param _myLeft see above
	 * @param _myTop see above
	 * @param _mmpp micrometers per pixel
	 * @return transformed rectangle
	 */
	static QRect transformRectToPixel(QRect _rect, int _myLeft, int _myTop, float _mmpp);
		

	//////////////////////////////////////////////////////////////////////////
	// Static constants
	//////////////////////////////////////////////////////////////////////////

	// Width of buttons
	static const int WIDTH;

	// Distance from movie picture
	static const int BAR_DISTANCE_PIC;

	// Number of timepoints to load before and after the current, when neighbor pos is opened
	static const int TPS_TO_LOAD_BEFORE;
	//static const int TPS_TO_LOAD_AFTER;

	// Color scheme
	static const QColor ACTIVE_COLOR;
	static const QColor INACTIVE_COLOR;

protected:

	//////////////////////////////////////////////////////////////////////////
	// Events
	//////////////////////////////////////////////////////////////////////////

	// Hover events, to change color
	virtual void hoverEnterEvent( QGraphicsSceneHoverEvent* _event );
	virtual void hoverLeaveEvent( QGraphicsSceneHoverEvent* _event );

	// Mouse press event, to implement button functionality
	virtual void mousePressEvent( QGraphicsSceneMouseEvent* _event );

private:
	// Key of position of this button
	QString posKey;
};


#endif // movienextpositem_h__