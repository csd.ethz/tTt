/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "statusbar.h"

//#include <QPixmap>
//#include <QPaintEvent>

#include <iostream>

StatusBar::StatusBar (QWidget *_parent)
 : QFrame (_parent)
{
}

StatusBar::StatusBar (QWidget *_parent, QString _caption, uint _min, uint _max, uint _step)
 : QFrame (_parent)
{
	init (_caption, _min, _max, _step);
}

void StatusBar::resizeEvent (QResizeEvent *)
{
//        buffer = QPixmap (width(), height());
//        buffer.fill (Qt::white);
}

void StatusBar::init (QString _caption, uint _min, uint _max, uint _step)
{
	minValue = _min;
	maxValue = _max;
	stepSize = _step;
	captionLine = _caption;
	updateSteps = 1;
	value = _min;
	
	//QPushButton *test = new QPushButton (this, "mine");
	//test->show();
	
	//setPaletteBackgroundColor (Qt::black);
        setFrameShape (QFrame::Box);
	
	connect ( &tmrUpdate, SIGNAL (timeout()), this, SLOT (inc()));
	
	//fit size into parent widget
        int w = 400;
        int h = 70;
	if (parentWidget()) {
                w = parentWidget()->geometry().width();
                h = parentWidget()->geometry().height();
                setGeometry (10, 10, w, h);
	}
	else {
                setGeometry (QApplication::desktop()->screenGeometry().left(), QApplication::desktop()->screenGeometry().top(), w, h);
	}

        bufferImage = QImage (w, h, QImage::Format_ARGB32);
        bufferImage.fill (Qt::white);
}


StatusBar::~StatusBar()
{
}

void StatusBar::setPosition (int _x, int _y, int _w, int _h)
{
	setGeometry (_x, _y, _w, _h);
}

void StatusBar::setBounds (uint _min, uint _max)
{
	minValue = _min;
	maxValue = _max;
	
	if (value > maxValue)
		value = maxValue;
	if (value < minValue)
		value = minValue;
}

void StatusBar::display (bool _show)
{
	if (_show) {
		show();
		//raise();
                paintBar();
	}
	else {
		hide();
                //problem: erase() does not exist anymore
                //if (parentWidget())
                //	parentWidget()->erase();
	}
}
	
void StatusBar::startTimer (uint _interval, uint _steps)
{
	updateSteps = _steps;
	
	tmrUpdate.start (_interval);
}
	
void StatusBar::advance (uint _steps, QString _caption)
{
	if (_steps > 0)
		updateSteps = _steps;
	if (! _caption.isEmpty())
		captionLine = _caption;
	
	inc();
}

void StatusBar::inc()
{
	if (value >= maxValue)
		return;
	
	value += updateSteps * stepSize;
	
	if (value > maxValue)
		value = maxValue;
	
        paintBar();
        repaint();
}

void StatusBar::paintEvent (QPaintEvent *)
{
        QPainter p (this);
        //std::cout << "paint..." << std::endl;
        p.drawImage (0, 0, bufferImage);
        p.end();
}

void StatusBar::paintBar (bool _noCaption)
{
        QPainter p (&bufferImage);
	
        //std::cout << value << std::endl;
        //std::cout << bufferImage.width() << " " << bufferImage.height() << std::endl;

	//establish coordinate system [ (minValue, 0) - (maxValue, 50) ]
	p.setWindow (minValue, 0, maxValue * 10, 50);
	
	p.setPen (QPen (Qt::black, 1));
	p.setFont (QFont ("Arial", 10));
	
	int top = 0;
	
	if (! captionLine.isEmpty()) {
		top = 25;
		if (! _noCaption) {
			p.drawText (minValue, 0, maxValue * 10, top, 0, captionLine);
		}
	}
	
	p.fillRect (0, top, value * 10, 50, Qt::black);
	
	p.end();		
}

void StatusBar::reset (QString _caption, int _step)
{
	if (_step > 0)
		stepSize = (uint)_step;
	if (_caption != "xxx")
		captionLine = _caption;
	
	value = minValue;
        //paintBar();
        repaint();
}

void StatusBar::setSBGeometry (int _left, int _top, int _width, int _height)
{
	setGeometry (_left, _top, _width, _height);
}

//#include "statusbar.moc"
