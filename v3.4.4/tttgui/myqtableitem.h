/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MYQTABLEITEM_H
#define MYQTABLEITEM_H

#include <q3table.h>

/**
	@author Bernhard <bernhard.schauberger@campus.lmu.de>
	
	Subclass for allowing to draw a table item in different colors
*/
class MyQTableItem : public Q3TableItem
{
	
public:
	MyQTableItem (Q3Table *_table, const QString &_text, EditType _et = Q3TableItem::Always, const QColor _color = Qt::black);
	
	~MyQTableItem();
	
	///reimplmented virtual function
	void paint (QPainter *_p, const QColorGroup &_cg, const QRect &_cr, bool _selected);

private:
	
	///the font color
	QColor color;
};

#endif
