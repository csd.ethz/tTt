/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "tttplayerassistant.h"

// Project
#include "tttmovie.h"

TTTPlayerAssistant::TTTPlayerAssistant( TTTMovie* _parentMovieWindow )
	: QDialog(_parentMovieWindow)
{
	// Init gui
	ui.setupUi(this);

	// Init variables
	parentMovieWindow = _parentMovieWindow;
	currentlyPlaying = false;

	// Signals/slots
	connect(ui.pbtStartStopPlayer, SIGNAL(clicked()), this, SLOT(startStopPlayer()));
}

void TTTPlayerAssistant::startStopPlayer()
{
	// Toggle GUI state
	currentlyPlaying = !currentlyPlaying;
	if(currentlyPlaying) {
		ui.pbtStartStopPlayer->setText("Stop Player");
		ui.grbOptions->setEnabled(false);
	}
	else {
		ui.pbtStartStopPlayer->setText("Start Player");
		ui.grbOptions->setEnabled(true);
	}

	// Calc interval
	unsigned int interval = 1000 / ui.spbFPS->value();

	// Toggle player state in TTTMovie
	if(currentlyPlaying)
		parentMovieWindow->startPlayer(ui.chkPlayerLoop->isChecked(), ui.chkPlayerReverse->isChecked(), interval);
	else
		parentMovieWindow->stopPlayer();
}
