/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef tttplayerassistant_h__
#define tttplayerassistant_h__

#include "ui_frmPlayerAssistant.h"

// Qt
#include <QDialog>

// Forward declarations
class TTTMovie;

/**
 * Small assistant to automatically play a movie window
 */
class TTTPlayerAssistant : public QDialog {

	Q_OBJECT

public:

	/**
	 * Constructor
	 * @param _parentMovieWindow pointer to the movie window this instance belongs to
	 */
	TTTPlayerAssistant(TTTMovie* _parentMovieWindow);

private slots:

	/**
	 * Start / stop player
	 */
	void startStopPlayer();

private:

	// Movie window this dialog belongs to
	TTTMovie* parentMovieWindow;

	// If player is currently on
	bool currentlyPlaying;

	// GUI
	Ui::frmPlayerAssistant ui;
};


#endif // tttplayerassistant_h__
