/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TTTTREEMAP_H
#define TTTTREEMAP_H

#include "ui_frmTreeMap.h"

#include "rectbox.h"


#include <qimage.h>
#include <qpixmap.h>
#include <qevent.h>
#include <qpoint.h>
#include <qtimer.h>

#include <q3canvas.h>
#include <qmatrix.h>
//Added by qt3to4:
#include <QResizeEvent>
#include <QMouseEvent>
#include <QWidget>

const QColor CurrentRegionColor = Qt::gray;


/**
@author Bernhard
	
	The class for the tree map GUI
	
	A little map of the current tree is displayed so that the user knows where he/she (resp. Hanna...)
	 currently works
	The display is very simple, just the already drawn pixmap of the tree is bitblted directly into
	 the form, yet the user can zoom the window, so the tree is also zoomed
*/

class TTTTreeMap: public QWidget, public Ui::frmTreeMap {
Q_OBJECT
public:
	TTTTreeMap(QWidget *parent = 0, const char *name = 0);
	
	~TTTTreeMap();
	
	///set the QCanvas object for this widget
	///@param _canvas the canvas object (should be equal to TreeDisplay's canvas)
	void setCanvas (Q3Canvas *_canvas);
	
	void resizeEvent (QResizeEvent *);
	
/*	void paintEvent (QPaintEvent *); */
	void mousePressEvent (QMouseEvent *);
/*	void mouseMoveEvent (QMouseEvent *); */
	void mouseReleaseEvent (QMouseEvent *);
	
/*	///draws the tree provided in _pixmap (contains the complete tree!) into THIS
	void drawTree (QImage *_image);*/
	
	///sets the displayed region, the indicating box is redrawn
	void setRectBox (QRect _displayedRegion, int _treeWidth, int _treeHeight);
	
signals:
		
	/**
	 * emitted when the user clicks a point with the mouse
	 * @param _position the logical coordinates of the mouse click (in canvas coordinates)
	 */
	void displayedRegionSet (QPoint _position);
	
public slots:
	
	///sets the rectangle for the displayed region to the provided rectangle
	void setDisplayedRegion (QRect _displayedRegion);
	
	///shits the rectangle for the displayed region with _shift
	///additionally, the height factor is necessary to calculate the pixel from seconds
	void shiftDisplayedRegion (QPoint _shift, int _twhf);
	
/*private slots:
	
	///drawing slot, called indirectly by paintEvent()
	void drawTimerEvent();*/
	
private:
	
	///the canvas viewing object (fills the complete form)
	Q3CanvasView *cv;
	
	///copy of the last provided pixmap pointer; necessary for the events
/*	QImage *completeImage;
	
	///the pointer to the last created image, necessary for the events
	QImage *image;*/
	
	///whether the middle mouse button is pressed
	bool MiddleMouseDown;
	
	///a rectangle that indicates the current viewport in TTTTracking
	///can be shifted via mouse, the viewport changes with it
/*	RectBox CurrentRegion;
	
	
	///the old mouse position
	QPoint oldMousePos;
	
	///the ratios between displayed size and real size of the complete tree
	float factorX;
	float factorY;
	
	///timer for drawing stuff in paintEvent()
	QTimer tmr;*/
};

#endif
