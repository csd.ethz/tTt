/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PROPAGATINGQFRAME_H
#define PROPAGATINGQFRAME_H

#include <QFrame>
#include <QWidget>
#include <QPainter>
#include <QImage>

/**
 * @author Bernhard
 *
 * An extended QFrame which converts its events to signals - no more subclassing necessary for easy and conventional usage.
 * Additionally, a QImage instance is stored which is always painted in paintEvent(). Note that the user is responsible that it always has the same size as the frame, otherwise it is clipped.
 *
 */

class PropagatingQFrame : public QFrame
{
        Q_OBJECT

public:

        PropagatingQFrame (QWidget *_parent = 0);

        //note: they are kept virtual to enable further subclassing

        virtual void keyPressEvent (QKeyEvent *);
        virtual void keyReleaseEvent (QKeyEvent *);
        virtual void mouseDoubleClickEvent (QMouseEvent *);
        virtual void mouseMoveEvent (QMouseEvent *);
        virtual void mousePressEvent (QMouseEvent *);
        virtual void mouseReleaseEvent (QMouseEvent *);
        virtual void paintEvent (QPaintEvent *);

        QImage* getImage()
                {return draw_img;}
        void setImage (QImage *_img)
                {draw_img = _img;}

signals:

        void keyPressed (QKeyEvent *);
        void keyReleased (QKeyEvent *);
        void mouseDoubleClicked (QMouseEvent *);
        void mousePressed (QMouseEvent *);
        void mouseMoved (QMouseEvent *);
        void mouseReleased (QMouseEvent *);
        void wheelEvent (QWheelEvent *);

private:

        QImage* draw_img;
};

#endif // PROPAGATINGQFRAME_H
