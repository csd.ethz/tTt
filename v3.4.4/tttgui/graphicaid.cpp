/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "graphicaid.h"

#include <math.h>

GraphicAid::GraphicAid()
{
}

GraphicAid::~GraphicAid()
{
}

QColor GraphicAid::next (QColor _color)
{
	QString name = _color.name();
	return _color;
}

QColor GraphicAid::mainColor (uint _index)
{
	return mainColors [_index % 16];
}

void GraphicAid::saveColor (QDataStream &_str, const QColor &_color)
{
        _str << _color.red();
        _str << _color.green();
        _str << _color.blue();
	
}

void GraphicAid::readColor (QDataStream& _str, QColor &_color)
{
	int red = 0, green = 0, blue = 0;
	_str >> red;
	_str >> green;
	_str >> blue;
	_color = QColor (red, green, blue);
	
}

unsigned int GraphicAid::colorToLong (const QColor &_color)
{
	return _color.rgb();
}

QColor GraphicAid::longToColor (unsigned int _color)
{
	return QColor (_color);
}

const QString GraphicAid::colorToString (const QColor &_color)
{
	return _color.name();
}

QColor GraphicAid::stringToColor (const QString &_name)
{
	QColor resi;
	resi.setNamedColor (_name);
	
	return resi;
}

void GraphicAid::overlay (QImage *_img1, const QImage *_img2, bool _useAlpha)
{
        if (! _img1 || _img1->isNull())
                return;
        if (! _img2 || _img2->isNull())
                return;

        //new: use included QPainter drawing modes

        //@todo try different ones; do not override current bw&contrast settings for the wavelengths
        //QPainter::CompositionMode overlayMode = QPainter::CompositionMode_Overlay;
		QPainter::CompositionMode overlayMode = QPainter::CompositionMode_Plus;

        if (_useAlpha) {
                //draw _img1 as background, paint _img2 over it

                QImage *resultImg = new QImage (_img1->size(), QImage::Format_ARGB32);

                QPainter p (resultImg);
                p.setCompositionMode (QPainter::CompositionMode_Source);
                p.drawImage (0, 0, *_img1);
                p.setCompositionMode (overlayMode);
                p.drawImage (0, 0, *_img2);
                p.end();

                *_img1 = *resultImg;

                delete resultImg;

        }
        else {
                //simple paint _img2 over _img1
                QPainter p (_img1);
                p.setCompositionMode (overlayMode);
                p.drawImage (0, 0, *_img2);
                p.end();
        }

//        return _img1;



//BS graphics

//	//note: assumes both pictures to be in 32bpp mode!
//
//	QRgb *oline, *sline;
//	QRgb c1, c2;
//
//	int r1, g1, b1, a1;
//	int r2, g2, b2, a2;
//
//	for (int i = 0; i < _img1->height(); i++) {
//
//	    oline = (QRgb *) _img2->scanLine(i);
//	    sline = (QRgb *) _img1->scanLine(i);
//
//	    for (int j = 0; j < _img1->width(); j++) {
//			if (_useAlpha) {
//				r1 = qRed (oline[j]);
//				g1 = qGreen (oline[j]);
//				b1 = qBlue (oline[j]);
//				a1 = qAlpha (oline[j]);
//
//				r2 = qRed (sline[j]);
//				g2 = qGreen (sline[j]);
//				b2 = qBlue (sline[j]);
//				a2 = qAlpha (sline[j]);
//
//				r2 = (a1 * r1 + (255 - a1) * r2) >> 8;
//				g2 = (a1 * g1 + (255 - a1) * g2) >> 8;
//				b2 = (a1 * b1 + (255 - a1) * b2) >> 8;
//				a2 = QMAX (a1, a2);
//
//				sline[j] = qRgba (r2, g2, b2, a2);
//			}
//			else {
//				c1 = oline[j];
//				c2 = sline[j];
//				c2 = c1 | c2;		//OR-connect both colors
//				sline[j] = c2; //qRgb (r2, g2, b2);
//			}
//	    }
//	}
//
//	return _img1;
}

QImage GraphicAid::to32bpp (const QImage *_img) //, int _width, int _height)
{
        if (! _img)
                return QImage();
        else
                return _img->convertDepth (32);
}

QColor GraphicAid::convertToGrayScale (QColor _color)
{
        const int cR = 77, cG = 150, cB = 28;
        int rgb = (cR * _color.red() + cG * _color.green() + cB * _color.blue()) / 255;
        return QColor(rgb, rgb, rgb);
}

QImage GraphicAid::adjustImageGraphicsForDisplay (const cv::Mat& inputImage, const BDisplay &_settings)
{
	// Create output image
	if(!inputImage.data)
		return QImage();
	if(inputImage.type() != CV_8U && inputImage.type() != CV_16U)
		return QImage();

	// Adjust graphics
	if(inputImage.type() == CV_8U)
		return adjust8BitImageGraphicsForDisplayInternal(inputImage, _settings);
	else
		return adjust16BitImageGraphicsForDisplayInternal(inputImage, _settings);

	/* Old code before switching to cv::Mat:

        //NOTE:
        //this function consumes, no matter how much settings have to be regarded, only up to maximal 5 milliseconds
        //=> not time-critical at all!


//@todo transcolor and alpha stuff - only if not yet done with QGraphicsView's methods
//      (idea: provide an alpha value spinbox in TTTGammaAdjust)

        //int alpha = 0;
        int alpha = _settings.Alpha;

//	int oldTCI = PicDisplaySettings->TransColorIndex;
//	if (_colorChannel == -1) {
//		//the alpha channel has to be set to 50% (= 125) for all entries
//		alpha = 125;
//	}
//	if (_colorChannel > -1) {
//		//set transformation color (only red/green/blue part of the color should be set)
//		//only if there is not yet a custom color selected
//		if (PicDisplaySettings->TransColorIndex == -1)
//			PicDisplaySettings->TransColorIndex = _colorChannel;
//	}

        //_settings contains all information

        //with color (int) and setColor (int, c) the palette of the image is manipulable

        if ((_img != 0)) {
                for (int i = 0; i < _img->numColors(); ++i) {

                        QRgb c = _img->color (i);

                        //apply black/white point
                        if (qRed (c) < _settings.BlackPoint) {
                                c = qRgb (0, 0, 0);
                                _img->setColor (i, c);
                        }
                        else if (qRed (c) > _settings.WhitePoint) {
                                c = qRgb (255, 255, 255);
                                _img->setColor (i, c);

//                                //NOT CURRENT POLITICS (see below in this procedure)
//                                //set the transformation color
//                                //note: the picture is handled 8-bit, but not necessarily grayscale!
//                                //		important is only the entry in the color table, and this can be
//                                //		any rgb-value
//                                if (_settings.TransColorThreshold > 0) {
//                                        _img->setColor (i, _settings.TransColor.rgb());
//                                }
                        }
                        else {
                                //apply [gammma] values
                                //WARNING: do not use  i  as index !!! because the initial palette of the picture might differ!
                                int x = qRed (c);
                                c = qRgb (_settings.GammaValues [x], _settings.GammaValues [x], _settings.GammaValues [x]);
                                _img->setColor (i, c);
                        }

                        //apply contrast

                        //@ todo: contrast function

                        //formula
                        //α=threshold, pivot => == 50%
                        //β=contrast factor  => 0,5 - 10
                        //(1/(1+exp(β(α-u))) - 1/(1+exp(β))) / (1/(1+exp(β(α-u))/(1+exp(β))))
                        //guarantees that the curve goes through (0,0) and (1,1)

                        //the points (0,0) and (1,1) have to "applied" to fit the current black/white
                        // point settings

                        if (_settings.Contrast != 100) {
                                int cv = qRed (c);

                                //steps:
                                //map color value to interval [0, 1]
                                //transform color
                                //map result value to interval [blackpoint, whitepoint]
                                float cvf = (float)cv;
                                float b = (float)_settings.Contrast / 10.0f;
                                float a = 0.5f;
                                //cvf must lie in the range between 0 (=blackpoint) and 1 (=whitepoint)
                                //normalization function: y = m * x + t, where m := 1/(wp-bp) and t := 1-m*wp
                                float m = 1.0f / ((float)_settings.BWDiff());
                                float t = 1.0f - m * (float)_settings.WhitePoint;
                                cvf = m * cvf + t;

                                cvf = (1/(1+exp(b*(a-cvf))) - 1/(1+exp(b))) / (1/(1+exp(b*(a-cvf))/(1+exp(b))));

                                //now the cvf value has to be remapped to the real region
                                //the function is now: y = m * x + t, where m := wp-bp and t := bp
                                m = (float)_settings.BWDiff();
                                t = (float)_settings.BlackPoint;
                                cvf = m * cvf + t;

                                cv = (int)cvf;

                                c = qRgb (cv, cv, cv);
                                _img->setColor (i, c);
                        }

                        //apply brightness
                        if (_settings.Brightness != 1.0f) {
                                float xb = 1.0f / _settings.Brightness;
                                int cv = (int) ( (pow ( float (qRed (c)) / 255.0f, xb)) * 255.0f);
                                c = qRgb (cv, cv, cv);
                                _img->setColor (i, c);
                        }

                        //if any color translation is applied, a color run is introduced
                        //current politics:
                        //only the channel can be selected (red, green or blue)
                        //this channel is set to (gray scale value), the other channels are set to 0
                        if ((_settings.TransColorIndex > -1) & (_settings.TransColorIndex < 3)) {
                                QRgb c = _img->color (i);
                                if (_settings.TransColorIndex == 0)
                                        c = qRgb (qRed (c), 0, 0);
                                if (_settings.TransColorIndex == 1)
                                        c = qRgb (0, qGreen (c), 0);
                                if (_settings.TransColorIndex == 2)
                                        c = qRgb (0, 0, qBlue (c));

                                _img->setColor (i, c);
                        }

                        //apply alpha blending
                        if (alpha > 0) {
                                QRgb c = _img->color (i);

                                c = qRgba (qRed (c), qGreen (c), qBlue (c), alpha);

                                _img->setColor (i, c);
                        }
                }

        }

        //return _img;
		*/
}
