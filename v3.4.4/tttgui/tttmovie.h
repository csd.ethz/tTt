/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TTTMOVIE_H
#define TTTMOVIE_H

#include "ui_frmPicture_Movie.h"

// Project includes
#include "multiplepictureviewer.h"
#include "tttdata/symbol.h"

// Qt includes
#include <QWidget>
#include <QPointF>
#include <QSize>
#include <QPixmap>
#include <QString>
#include <QColor>

// Forward declarations
class TTTPositionManager;
class PictureArray;
class Track;
class TTTSpecialExport;
class StyleSheet;
class QTimer;
class MovieMenuBar;
class TTTMovieExport;
class TTTCellDivisionOverviewExport;
class QStatusBar;
class TTTGammaAdjust;
class TTTPlayerAssistant;
class TTTSpecialEffects;


const float Pi = 3.141592f;

/**
@author Bernhard
*	
*	The class for the picture displaying and tracking GUI
*/

class TTTMovie: public QWidget, public Ui::frmPicture_Movie {

Q_OBJECT

public:
	TTTMovie (TTTPositionManager *_positionManager, QWidget *parent = 0, const char *name = 0);
	
	~TTTMovie();
	
	/**
	 * initializes the displaying bounds and stuff
	 */
	void initDisplays();
	
	///connects the slots of the different forms that have contact with TTTMovie
	void initForms();
	
	void showEvent (QShowEvent *_ev);

	void closeEvent (QCloseEvent *);
	
	//void focusInEvent (QFocusEvent *);
	
	///sets the borders for the possible timepoints
	///necessary every time pictures are (un)loaded
	void setTimePoints (int _firstTP, int _lastTP);
	
	void paintEvent (QPaintEvent *);
	
	void resizeEvent (QResizeEvent *);
	
	void mousePressEvent (QMouseEvent *);
	void mouseMoveEvent (QMouseEvent *);
	//void mouseReleaseEvent (QMouseEvent *);
	
	void keyReleaseEvent (QKeyEvent *);
	
	///should not be used!
	///at the moment necessary for non-friend classes like TimeScale
	PictureArray* getMoviePictures() const;
	
	///lets the user select a distance on a picture
	///= sets the form in radius selection mode
	///-> interpreted as colocation radius
	void startRadiusSelection();
	
	///displays a form where the user can enter a comment for the current cell/track
	///@param _track the track for which the the comment should be entered
	///@param _timepoint the timepoint for which the comment should be entered
	void editCellComment (Track *_track, int _timepoint);

	///starts the tracking process
	///@param _isMainMovieWindow whether this is the main movie window (i.e., of the current position)
	bool startTracking (bool _isMainMovieWindow);
	
	/**
	 * @return whether the form is in the mode to select more colony starts at once
	 */
	bool inStartingCellSelectionMode() const
                {return multiPicViewer->inStartingCellSelectionMode();}
	
	/**
	 * sets the tracking buttons enabled or disabled
	 * @param _enable true => buttons are enabled; false => buttons are disabled
	 */
	void switchTrackingButtons (bool _enable);
	
    /**
     * set the provided zoom to be displayed in the zoom spinbox
     * @param _triggerCascade whether the zoom setting cascade should be triggered (default = true)
     */
    void setZoomSpinboxValue (int _zoom, bool _triggerCascade = true);

    /**
     * @return the multiple picture viewer instance
     */
    MultiplePictureViewer *getMultiplePictureViewer() 
            {return multiPicViewer;}

	/**
	* a special picture export function which allows various x/y and zoom shifts and builds up the tree along the time
	* contains some hard-coded parts and should only be used under supervision
	* programmed only for Timm and Joost currently
	*
	* ToDo:	- Timebox position option
	*		- Relative TP option
	*		- Font width option
	*		- Font bold option
	*
	* @param exportWindow the corresponding export window
	* @param sizeX desired width of exported pictures, -1 to use current size
	* @param sizeY desired height of exported pictures, -1 to use current size
	*				(if one of the size arguments is -1, both will be ignored)
	* @param timeBoxIntoCsvFile if time box strings should be exported into separate csv file
	*/
	void runSpecialExport (TTTSpecialExport *exportWindow, int sizeX = -1, int sizeY = -1, bool timeBoxIntoCsvFile = false);


	/**
	 * Adjust size of movie window, so that picture display will have specified size
	 * @param x width in pixels 
	 * @param y heigth in pixels
	 */
	void setPictureSize(int x, int y);

	/**
	 * If controls are hidden
	 */
	bool getControlsHidden() const { return controlsHidden; }

	/**
	 * sets the display to the desired timepoint, zooms the picture to the desired zoom factor and centers it to the desired location
	 * @param _timepoint the timepoint
	 * @param _pos the x/y position
	 * @param _zoomFactor the zoom factor (in percent)
	 * @param _wavelengths the visible wavelengths, as a comma separated list
	 * @param _overlay whether these wavelengths should be overlayed or just split
	 * @param _firstAllTracks whether the first/all tracks should be displayed (0 = neither of them, 1 = first, 2 = all at current tp, 3 = no tracks at all, -1 = do not change this)
	 */
    void setPictureDisplay (int _timepoint, QPointF _pos, int _zoom, /*QString _wavelengths = "", bool _overlay = false,*/ int _firstAllTracks = 0);
	

	/**
	 * Return size of picture display region (without frame and controls)
	 */
	QSize getPictureDisplaySize() const;

	/**
	 * Get currently displayed picture as QPixmap
	 */
	QPixmap grabCurrentPicture() const {
		return QPixmap::grabWidget(fraPictures);
	}

	/**
	 * Get position manager for this movie window
	 */
	TTTPositionManager* getPositionManager() {
		return positionManager;
	}

	/**
	 * Get if wavelength-box is displayed
	 */
	bool getWavelengthBoxVisible() const {
		return chkShowWavelengthBox->isChecked();
	}

	/**
	 * Display mode for "display external tracks"
	 */
	enum DisplayExternalTracksMode {
		OFF,
		ALL_CUR_POS,		// All tracks from current position
		ALL_ALL_POS,		// All tracks from all positions
		FIRST_CUR_POS,		// First track from all tracks from current position
		FIRST_ALL_POS		// First track from all tracks from all position
	};

	/**
	 * @return current mode for displaying external tracks
	 */
	DisplayExternalTracksMode getExternalTracksMode() const {
		return showExternalTracksMode;
	}

	/**
	 * Set external tracks mode. This does not update the movie window's menu, so make sure to change this back when finished
	 */
	void setExternalTracksMode(DisplayExternalTracksMode _mode);

	/**
	 * Synchronize selected cell properties for with _props
	 */
	void setSelectedTrackingProperties(const CellProperties& _props);

	/**
	 * Toggle player
	 * @param _loop endless loop 
	 * @param _reverse if movie should contimue in reverse direction if end has been reached
	 * @param _interval number of milliseconds between each frame
	 */
	void startPlayer(bool _loop, bool _reverse, unsigned int _interval);

	/**
	 * Stop player if running
	 */
	void stopPlayer();

	///**
	// * Propose a tree fragment when creating a tree from autotracking results. The fragment will be appended to the current track
	// */
	//void activateProposeTreeFragmentBtn(const QString& _buttonText);

	// Gamma window
	TTTGammaAdjust *frmGammaAdjust;

	// Player assistant window
	TTTPlayerAssistant *frmPlayer;

	// Special effects window (can be 0)
	TTTSpecialEffects *specialEffectsWindow;

	bool event(QEvent* _e);

	/**
	 * Test if the provided wavelength should be used for overlay.
	 * @param wl wl number.
	 * @return true if wl should be used for overlay.
	 */
	bool useWlForOverlay(int wl);

public slots:

	///stops the tracking process (will call function below)
    bool stopTracking();

	/**
	 * stops the tracking mode globally
	 * @param _reason the cell stop reason (must be value from TrackStopReason enum)
	 * @return success (dependent on TTTManager::stopTracking())
	 */
	bool stopTracking (int _reason);

	/**
	 * Stop tracking with stop reason cell division and immediately continue tracking of daughter cell.
	 */
	bool stopTrackingAndContinueWithDaughter();

	/**
	 * Select a different cell in the tree (and continue tracking).
	 */
	bool selectOtherCell(int code);

    ///proceeds one picture in the current direction
    void ProceedPicture(bool afterTrackpointSet = false);

    ///hides/shows most controls (widgets) in the window (only with hidden controls the window can be shrinked to very small sizes)
    void hideShowControls();

    ///shows the form for selecting symbols to add them to the picture
    void showSymbolForm();

    ///set/unset the circle mouse pointer
    void setCircleMousePointer (bool);

	///open the dialog for movie export
	void launchMovieExportDialog();

	///open the dialog for division export
	void launchDivisionExportDialog();



	///see above, called by an instance of PictureView
	///_step specifies the number of loaded pictures that are skipped
	///_step == -1 	means end/begin
	///_step == 0 	means 1 picture forward/backward
	///_step >  0 	means percentage of all loaded pictures in _direction
	///this method uses calls to ProceedPicture() from above
	void ProceedPicture (bool _direction, int _step, bool afterTrackpointSet = false);

	///sets whether the tracks are shown
	void setShowTracks (bool _on, bool _forceForAllTracks = false);

	///same as above for all WLs, bescause the function above cannot be used as a slot for
	///a toggle button, when the second parameter should be true.
	void setShowTracksForAllWls (bool _on) {
		setShowTracks(_on, true);
	}

	/// Show path to log file
	void showLogFilePath();

	/**
	 * sets the position for the timebox in a movie export
	 * @param _pos the position, coded as integer; needs to be converted to StyleSheet's possible values
	 */
	void setMovieTimeboxPosition (int _pos);

signals:
	
	///emitted when the user selected a radius
	///only wraps the same signal from PictureView
	void RadiusSelected (int);
	
	
private slots:

	///**
	// * Event handler for movie export buttons in the tab on the left
	// *
	// */
	//void exportMovieClicked();

	/**
	 * displays the provided timepoint (is called by TTTManager)
	 * @param _newTimepoint the new (current) timepoint
	 * @param _oldTimepoint the timepoint that was valid before
	 */
	void setToTimepoint (int _newTimepoint, int _oldTimepoint);
	
	//// @Deprecated:
	/////starts/stops the movie player in the current direction
	//void play (bool _play = true);
	
	//// @Deprecated:
	/////set player attributes
	//void setPlayerReverse (bool);
	//void setPlayerLoop (bool);
	//void setTimerInterval (int);
	
	///the main routine for displaying pictures for the current timepoints
	///_timepoint: 	current timepoint
	///_wavelength:	the desired wavelength, whose display should be updated
	///_reloadAllInMultipleMode: specifies whether all displayed pictures should be updated (then _wavelength can be set to -1)
        //void showPicture (int _timepoint = -2, int _wavelength = -1, bool _reloadAllInMultipleMode = true);
	
	///updates the display to the latest settings 
	///this concerns: 
	/// - indices of loaded pictures
	/// - 
        void updateView();
	
	///adapts the form's size to fit the selected size in frmRegionSelection
	void changeDisplaySize();
	
	///shifts the currently displayed pictures to the desired region selected in frmRegionSelection
        void changeDisplayPosition (QPointF);
	
	///displays a form where the user can enter a comment for the current cell/track
	void editCellComment();
	
	///saves the comment edited in the widget in the current track/cell
	void saveCellComment (QString);

 //   ///display all/first tracks (what happens is decided on the sending widget)
	////@DEPRECATED
 //   void displayAllTracks (bool);

    ///lets the user adjust grahpic settings (black/white point, gamma, ...)
    void adjustGraphics();

	///sets the current picture to the value the slider is at
	void slidePicture (int);
	
	///called by the various zoom selection buttons
        void setZoom();
	
	///called if in one PictureView instance the user shifts the picture
	///the coordinates are provided with screen measures
        void shiftPictureSlot (int, QPointF);
	
	///called by the wavelength selecting buttons to raise/hide a wavelength frame
	///as consequence, the layout of the visible frames is adjusted
	void setWaveLength (int);
	
	///called by an instance of PictureView to set the active wavelength
	///(no wavelength's visibility is changed!)
	void setCurrentWaveLength (int);
	
	///called by the multiple wavelength switching button
        void setMultipleMode (bool);
	
	///triggers/stops the timer in MoviePictures, which loads additional pictures
	void continueLoading (bool);
	
	///sets the track circle size
	void setTrackSize (int);
	
	///sets whether the numbers of the tracks should be displayed when the track circles
	/// are shown
	void setShowTrackNumbers (bool);
	
	///selects the provided track in frmTracking
	///bool: whether tracking should be triggered for this cell
	void selectTrack (Track *, bool, bool);
	
	///displays the current mouse position in absolute coordinates of the current
	///wavelength/picture
	///_pos is already transformed according to the current wavelength display settings
        void displayMousePosition (QPointF _pos);
	
        // /sets the threshold for the "white" point for color translation
        //void setTransColorThreshold (int);
	
	///called by an instance of PictureView
	///wraps the SIGNAL (TrackPointSet()) and adds some information to the current properties
	void wrapTrackPointSet (Track *, int);
	
	///toggles whether the normal cell position or the background region should be set
	/// in the current trackpoint (on Key_0 press)
	void setEditCellBackground (bool);
	
	///called by an instance of PictureView
	///changes the size of the track circle when in tracking und size changing mode (UseCellRadius == true)
	void changeCellSize (int);
	
	///wrapper for the same signal from PictureView
	///only emits radiusSelected (int)
	void radiusSelected (int);
	
	///called when the user clicked either on apply or close in the symbol selection form
	///adds the provided Symbol to the symbol list for the current wavelength
	void addSymbol (Symbol);
	
	///deletes all symbols currently added to this colony
	void deleteSymbols();
	
	///sets the endomitosis flag
	///called on button toggle
	void setEndoMitosis (bool);
	
	///displays the cell property form
	void showPropertyForm();
	
	///all keys that are received by a PictureView instance and are accepted, but not processed, are handled to this slot to be
	/// processed in here
	///@param int,bool,bool: key code, shift, control
	void processPictureKey (int, bool, bool);
	
	///sets whether the user sees a cross pointer or a circle when in tracking mode
        //void setCircleTrack (bool);
	
	///whether the selected wavelengths should be arranged in an overlay instead of an array
	void setOverlay (bool, bool _callShowPicture = true);
	
	///called by TTTTreeStyleEditor
	///applies the provided style/layout
	///@param _ss the style sheet (needs not be the same as the global one in UserInfo!)
	void applyStyleSheetMovie (StyleSheet &);
	
	/////displays a form where the user can select some options for exporting movies (jpgs)
	//void exportOptions();

    ///shows/hides the wavelength index box in the top left corner of the picture
    void setShowWavelengthBox (bool);

	///shows a form to assist users using special export
	void specialExport();
	
        /* *
	 * lets the user set a (temporary) background track for the currently marked cell and stores the integrals of the current cell and its sibling into
	 *  an open textbox form in csv format
	 * implemented for Andrea H., 2008/09/18
	 */
        //void addFluorescenceLine();
	
	/**
	 * adds for the daughter cells of the currently selected cells their mutual distances during their lifetime
	 */
	void addChildrenDistanceLine();
	
        /**
	 * displays a form with a short overview/statistics of the current tracks
	 * (e.g. fluorescence, error detection, ...)
	 */
	void showTracksOverview();

	/**
	* Set if track size for display should be overwritten by the value of spbDisplayCircleSize
	* Connected with toggle event of pbtSetDisplayCircleSize
	* @param _on toggle on or off
	*/
	void toggleOverwriteTrackRadiusForDisplay(bool _on);

	/**
	* Set overwrite radius for display
	* @param _radius radius in pixels
	*/
	void setOverwriteTrackRadiusForDisplay(int _radius);

	/**
	 * Update show external tracks mode. Called by menu QActions
	 */
	void setShowExternalTracks(bool _on);

	/**
	 * Update selected cell differentiation properties for tracking, called by MovieMenubar actions
	 * @param _properties defines selected property, see implementation of function for details
	 */
	void setTrackingCellPropertiesDifferentiation(int _properites);

	/**
	 * Update selected cell adherence properties for tracking, called by MovieMenubar actions
	 * @param _on on or off
	 */
	void setTrackingCellPropertiesAdherence(bool _on);

	/**
	* Update selected cell wavelength properties for tracking, called by MovieMenubar actions
	* @param _on on or off
	 */
	void setTrackingCellPropertiesWavelength(bool _on);

	/**
	 * 
	 */
	//void setTrackingCellPropertiesWavelength();

	///**
	// * Autotracking button
	// */
	//void autoTrackingButtonClicked();

	/**
	 * Checkbox to hide bars for loading adjacent positions was toggled
	 * @param _on on or off
	 */
	void chkHideLoadPicsBarsToggled(bool _on);

	/**
	 * Show movie player dialog
	 */
	void showMoviePlayer();

	/**
	 * Show special effects dialog
	 */
	void showSpecialEffectsDialog();

	/**
	 * Wavelength for overlay has been enabled or disabled.
	 * @param id id of corresponding button (= wl).
	 */
	void overlayWlButtonClicked(int id);

	/**
	 * Change z-index.
	 */
	void setZIndex(int newZIndex);

	// Testing function for debugging only
	void debugTest();
	
private:

	///the position manager that is responsible for this movie window
	///is set in the constructor via TTTManager
	TTTPositionManager *positionManager;
	
	///the stack of widgets for displaying a picture (one for each wavelength)
    MultiplePictureViewer *multiPicViewer;

	///parameters for the player
	bool MoviePlaying;
	bool DirectionForward;
	bool MovieLoop;
	bool MovieReverse;
	
	///the timer for the player
	QTimer *MovieTimer;
	
	//// @Deprecated:
	/////the timer interval (can be set via spinbox)
	//int TimerInterval;
	
	///specifiees whether the display region box in frmRegionSelection should be adjusted
	///this is important for the resize event, cos otherwise a change of the box' size would
	///  trigger a double resizing with recursive size changing
	bool adjustDisplayBox;
	
	///necessary for changing the movie direction with a click on the direction label
	bool DirectionLabelMousePressed;
	
	///takes the background color of a QPushButton (necessary for restoring it)
	QColor ButtonColor;
	
	///true, if the timepoint should be set when the slot setToTimePoint_NoSignal (int) is called
	bool Process_TimePoint_Set;
	
	///contain the first and the last timepoint that should be exported
	///if they are reverse, the export runs backward!
	int LastExportTimePoint;
	int FirstExportTimePoint;
	
	///contain the first and last trace of the track BEFORE the tracking was started - maybe -1!
	///necessary for deciding whether a track was just updated or newly created
	int Track_OldFirstTrace;
	int Track_OldLastTrace;
	
	///the timepoint of the first trackpoint - often different to Track_OldFirstTrace
	int Track_OldFirstTrackPoint;
	
	/////whether endomitosis is selected for the current trackpoint
	//bool EndoMitosis_Selected;
	
	///whether the code in slidePicture() should be executed
	///set to false in showPicture() before the slider value is altered to avoid cascadeous calling
	bool call_slidePicture;
	
	///the timepoint for which the comment is currently edited
	int commentTimepoint;
	
	///whether the slot setZoom() should be handled on an event of spbZoomFactor
	bool call_setZoomOnspZoomFactor;
	
	///whether controls including tabs on left are currently hidden; can be set by user by pressing ctrl+H
	bool controlsHidden;

	///window layout already loaded
	bool windowLayoutLoaded;

	///display all tracks mode
	DisplayExternalTracksMode showExternalTracksMode;

	///statusbar
	QStatusBar *statusBar;

	///statusbar widgets
	QLabel* lblZIndex;
	QSpinBox* spbZIndex;
	QLabel *lblTracking;
	QPushButton *pbtZoom50;
	QPushButton *pbtZoom100;
	QSpinBox *spbZoomFactor;
	QPushButton *pbtFullPicture;

	///statusbar button for autotracking for proposing tree fragment to append to current track
	QPushButton *pbtProposeTreeFragment;

	///status bar message informations
	int currentIndex;
	int lastIndex;
	float mousePosX, mousePosY;
	bool showMousePos;
		

///controls

	QSpinBox *spbTrackSize;
	//QSpinBox *spbTimerInterval;	
	QSpinBox *spbPathTPStart;
	QSpinBox *spbPathTPEnd;
	//QSpinBox *spbTrackmarkDelay;
	//MyQSpinBox *spbStackRepeat;

	// Button groups for wavelength buttons and overlay wavelength buttons
	QButtonGroup* bgrWaveLengths;
	QButtonGroup* bgrOverlayWaveLengths;

	
///methods


	
	/**
	 * displays the time stamp between the two timepoints into the first available picture frame
	 * @param _relativeTimepoint the relative timepoint
	 * @param _currentTimepoint the current timepoint
	 * @param _pix the pixmap to be used for drawing
	 *
	 * @DEPRECATED only used by special export anymore
	 */
	void printMovieExportTime (int _relativeTimepoint, int _currentTimepoint, QPixmap *_pix);
	QString getMovieExportTimeString(int _relativeTimepoint, int _currentTimepoint);
	
	/**
	 * creates the wavelength buttons for this positions (depending on the actually available wavelengths)
	 * @param _tttpm 
	 */
	void createWavelengthButtons (TTTPositionManager *_tttpm);
	
	///creates the layout for all PictureViews that have the display flag set
        //void layoutPictureFrames();
	
	///sets the displayed region of the current wavelength to the current settings
	///if _waveLength is specified, the displayed region for this one is updated
	void adjustDisplayedRegion (int _waveLength = -1);
	
	///this function handles all not yet treated keys that were pressed (e.g. F keys)
	void processKey (int _keyCode, bool _shift, bool _ctrl);
	
	///displays the absolute cell integral value for the cell and its background
	///@param _track the track for which the display should be calculated
	///@param _zIndex the z-index
	///@param _timepoint the timepoint which should be used for calculation
	///@param _wavelength for which wavelength the background is tracked
	void showCellIntegrals (Track *_track, int _timepoint, int _zIndex, int _wavelength);
	
	/**
	 * sets the zoom to _zoomPercent (in percent)
	 * @param _zoomPercent the zoom factor (100% => 1 pixel in the picture corresponds to 1 pixel on the screen)
	 * @param _zoomX the abscisse of the point to which should be zoomed (only important when zooming into the picture); if -1, the picture center is used
	 * @param _zoomY see _zoomX, here the ordinate
	 * @param _forceSetting normally, if _zoomPercent == current zoom, nothing is done; with _forceSetting == true, this check is skipped
	 */
	void setZoom (int _zoomPercent, float _zoomX = -1, float _zoomY = -1, bool _forceSetting = false);
	
	
	/**
	 * sets the wavelength provided to be displayed
	 * @param _wavelength the wavelength
	 * @param _show show it (true) or hide it (false)?
	 * @param _callShowPicture whether showPicture() should be called
	 */
	void setWaveLength (int _wavelength, bool _show, bool _callShowPicture = true);
	
	/**
	 * shifts all pictures by the provided offset
	 * @param _index the trigger (-2: no trigger, -1: region selection box, 0+: wavelength pictureView)
	 * @param _shift the shift in screen coordinates
	 * @return the actually performed shift (with regarding the picture and frame bounds)
	 */
    QPointF shiftPictures (int _index, QPointF _shift);

	/**
	 * Update lblLastPicture to display current timepoint and last loaded timepoint
	 * @param _timepoint the new timepoint
	 */
	void updateLblLastPicture(int _timepoint);

	/**
	 * Update status bar message
	 */
	void updateStatusBarMessage();
	
	///menubar
	MovieMenuBar *menuBar;

	///movie exporter
	TTTMovieExport *frmMovieExportDialog;

	///advanced movie exporter
	TTTSpecialExport *frmSpecialMovieExportDialog;

	///division overview exporter
	TTTCellDivisionOverviewExport *divisionExportDialog;

	friend class MovieNextPosItem;
};


#endif
