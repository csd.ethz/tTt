/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "autotrackingcellcircle.h"

// Qt
#include <QGraphicsSceneMouseEvent>

// Project
#include "pictureview.h"
#include "tttdata/userinfo.h"


AutoTrackingCellCircle::AutoTrackingCellCircle(Action _action, const ITrackPoint* _trackPoint, QString _text, QPointF _position, int _radius, QColor _color, QWeakPointer<TreeFragment> _treeFragment, QSharedPointer<Tree> _tree, QColor _textColor, const QString _toolTip, bool _noOwnCursor)
	: QGraphicsEllipseItem(_position.x() - _radius, _position.y() - _radius, 2*_radius, 2*_radius)
{
	// Init variables
	//clickable = _clickable;
	action = _action;
	radius = _radius;
	//autoTrackingImageView = _autoTrackingImageView;
	tree = _tree;
	treeFragment = _treeFragment;
	textItem = 0;
	trackPoint = _trackPoint;

	// Set item's cursor
	if(action != A_NONE && !_noOwnCursor)
		setCursor(Qt::PointingHandCursor);

	// Set color and thickness
	thickness = UserInfo::getInst().getStyleSheet().getCellCircleThickness();
	QPen pen (_color, thickness);
	setPen(pen);

	// Z-Value
	setZValue(ZVALUE_TRACKCIRCLE);

	// Set text
	if(!_text.isEmpty()) {
		// Create text item
		textItem = new QGraphicsTextItem(_text, this);
		textItem->setDefaultTextColor(_textColor);
		textItem->setPos(_position.x() + _radius, _position.y() - _radius);

		// Set font size
		QFont font ("Arial", UserInfo::getInst().getStyleSheet().getCellNumberFontSize());
		textItem->setFont(font);

		// Same Z as circle
		textItem->setZValue(ZVALUE_TRACKCIRCLE);

		// Set tooltip text 
		if(!_toolTip.isEmpty()) {
			//QString toolTip = "Click to append fragment " + _text;
			setToolTip(_toolTip);
		}
	}
}

void AutoTrackingCellCircle::setRadius( int _radius )
{
	// Update ellipse position
	QRectF rectf = rect();
	rectf.setX(rectf.x() + radius - _radius);
	rectf.setY(rectf.y() + radius - _radius);
	setRect(rectf.x(), rectf.y(), 2*_radius, 2*_radius);

	// Update text item position
	if(textItem) {	
		QPointF position = textItem->scenePos();
		position.setX(position.x() - radius + _radius);
		position.setY(position.y() + radius - _radius);
		textItem->setPos(position);
	}

	radius = _radius;
}

void AutoTrackingCellCircle::mousePressEvent( QGraphicsSceneMouseEvent* _event )
{
	if(action != A_NONE) {
		//autoTrackingImageView->notifyCircleClick(tree);
		_event->accept();
		emit cellClicked(this, _event->button());
	}
}
