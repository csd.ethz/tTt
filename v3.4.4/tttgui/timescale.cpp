/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "timescale.h"

// Project includes
#include "tttbackend/picturearray.h"
#include "tttdata/userinfo.h"


#include "treedisplay.h"
#include "tttbackend/tttmanager.h"
//Added by qt3to4:
#include <QMouseEvent>


TimeScale::TimeScale(QWidget *_container, QWidget *parent, const char *name)
 : Q3CanvasView (parent, name), 
   zoomFactor (0), DisplayedSeconds (0)
{
	if (_container)
		Container = _container;
		
	Selecting = 0;
	TopSelected = -1;
	BottomSelected = -1;
	MiddleButtonPressed = false;
	
	TopSecond = 0;
	top = 0;
	zoomFactor = 100;
	
	canvas.resize (10, 10);
	canvas.setDoubleBuffering (false);
	this->setCanvas (&canvas);
	
	//@todo switch off scrolling with mouse wheel
	
	this->setVScrollBarMode (Q3ScrollView::AlwaysOff);
	this->setHScrollBarMode (Q3ScrollView::AlwaysOff);
	
	selection = 0;
	
}

TimeScale::~TimeScale()
{
	//DO NOT DELETE Pics, Files, Container, frmMovie
}

void TimeScale::init()
{
	// Get position manager
	TTTPositionManager* posManager = TTTManager::getInst().getCurrentPositionManager();
	if(!posManager)
		return;
	
	if (posManager->getLastTimePoint() > 0)
		if (posManager->getFiles())
			if (posManager->getFirstTimePoint() > 0)
				DisplayedSeconds = posManager->getExperimentSeconds();
	
	if (DisplayedSeconds > 0) {
		canvas.resize (Container->width(), DisplayedSeconds / posManager->getTWHF());
		canvas.update();
	}
}

void TimeScale::setZoomFactor (int _zoomFactor)
{
	QMatrix wm = this->worldMatrix();
	wm.scale ((double)_zoomFactor / (double)zoomFactor, (double)_zoomFactor / (double)zoomFactor);
	zoomFactor = _zoomFactor;
	this->setWorldMatrix (wm);
}

//SLOT:
void TimeScale::draw() // (const StyleSheet &_ss) // (long int _timePointStart, long int _timePointEnd, int _lineWidthFactor)
{
	///@todo redraw only the changed items!

	// Get position manager
	TTTPositionManager* posManager = TTTManager::getInst().getCurrentPositionManager();
	if(!posManager)
		return;

	// Get file info array and picture array
	const FileInfoArray* fileInfos = posManager->getFiles();
	const PictureArray* pics = posManager->getPictures();
	if(!fileInfos || !pics)
		return;
	
	//clear previous lines
	Q3CanvasItemList allItems = canvas.allItems();
	for (Q3CanvasItemList::Iterator iter = allItems.begin(); iter != allItems.end(); ++iter) {
		int rtti = (*iter)->rtti();
		if (rtti == Q3CanvasRectangle::RTTI)
			//do not delete selection box
			;
		else
			delete (*iter);
	}
	
	int y = 0;
	
	int firstTimepoint = posManager->getFirstTimePoint();
	int lastTimepoint = posManager->getLastTimePoint();
	
	long int start = firstTimepoint;
	long int stop = lastTimepoint;
	
	int bound = MAX_WAVE_LENGTH + 1;
	int width = (Container->width() - 4) / bound;
	int width_2 = width / 2;
	
	Q3IntDict<MyQCanvasLine> lines (MAX_WAVE_LENGTH + 1);
	lines.setAutoDelete (false);
	MyQCanvasLine *l = 0;
	for (int i = 0; i < (int)lines.size(); i++) {
		l = new MyQCanvasLine (&canvas, true);
		l->setPoints (2 + width_2 + width * i, 0, 2 + width_2 + width * i, canvas.height());
		l->setPen (QPen (ExistingColor [i], 2));
		l->setWidth (width);
		l->setUsage (LineUsageTimeScaleLine);
		l->show();
		lines.insert (i, l);
	}
	
	for (long int i = start; i <= stop; i++) {
		//TTTPositionManager* posManager = TTTManager::getInst().getBasePositionManager();

		if (fileInfos->exists (i, 1, -1)) {		//if any file at timepoint i exists
			//if (TTTManager::getInst().getFirstTimePoint() > 0 && i > 0)
			y = fileInfos->calcSeconds (firstTimepoint, i) / posManager->getTWHF();
			
			//for each wavelength on the disk there is a separate line in different color
			for (int j = 0; j < bound; j++) {
				l = lines.find (j);
				if (l) {
					if (pics->getSelectionFlag (i, 1, j))
						l->addMiddlePoint (2 + width_2 + width * j, y, SelectedColor);
					else if (pics->isLoaded (i, j, 1)) {
						l->addMiddlePoint (2 + width_2 + width * j, y, LoadedColor);
					}
					else if (fileInfos->exists (i, 1, j))
						l->addMiddlePoint (2 + width_2 + width * j, y, ExistingColor [j]);
					
				}
			}
		}
	}
	l = 0;
	
	QString x = "";
	int xstart = 0;
	TreeElementStyle tes = UserInfo::getInst().getStyleSheet().getElement (RTTI_TIMESCALE_FIGURES);
	for (int i = 43200; i < DisplayedSeconds; i += 43200) {
		MyQCanvasText *t = new MyQCanvasText (&canvas);
		t->setFont (QFont ("Arial", tes.getSize()));
		t->setColor (tes.getColor (0));
		t->setX (xstart);
		t->setY (i / posManager->getTWHF());
		if (i % 86400 == 0)	{			//complete day
			x = QString ("d%1").arg (i / 86400);
		}
		else {						//12 hours
			x = QString ("%1h").arg (i / 3600);
		}
		t->setText (x);
		t->setUsage (TextUsageTimeScaleText);
		t->show();
	}
	
	canvas.update();
	
}
	
void TimeScale::fit()
{
	setGeometry (Container->geometry());	
}

//EVENT:
void TimeScale::contentsMousePressEvent (QMouseEvent *ev)
{
	if (! Selecting) {
		ev->ignore();
		return;
	}
	if (! selection)
		return;
	
	int y = getLogical (ev->pos().y());
	int top = (int)selection->y();
	int topdiff = y - (int)selection->y();
	int bottom = (int)(selection->y() + selection->height());
	int bottomdiff = y - bottom;
	switch (ev->button()) {
		case Qt::LeftButton:		//set top point
			drawSelectionBox (y, selection->height() - topdiff);
			break;
		case Qt::MidButton:			//shift box
			oldMousePos = ev->pos();
			MiddleButtonPressed = true;
			break;
		case Qt::RightButton:		//set bottom point
			drawSelectionBox (top, selection->height() + bottomdiff);
			break;
		default:
			;
	}
	ev->accept();
}

//EVENT:
void TimeScale::contentsMouseMoveEvent (QMouseEvent *ev)
{
	// Get position manager
	TTTPositionManager* posManager = TTTManager::getInst().getCurrentPositionManager();
	if(!posManager)
		return;

	if (! posManager->getFiles())
		return;
	if (! Selecting) {
		ev->ignore();
		return;
	}
	if (! selection)
		return;
	
	// User can drag selection box up and down by holding middle mouse button
	if (MiddleButtonPressed) {
		int yshift = getLogical (ev->pos().y() - oldMousePos.y(), true);
		if (selection->y() + selection->height() + yshift > canvas.height())
			yshift = 0;
		if (selection->y() + yshift < 0)
			yshift = 0;
		
		drawSelectionBox ((int)selection->y() + yshift, (int)selection->height());
		oldMousePos = ev->pos();
		ev->accept();
		
		TopSelected = posManager->getFiles()->calcTimePoint (posManager->getFirstTimePoint(), (int)(selection->y()) * posManager->getTWHF(), false);
		BottomSelected = posManager->getFiles()->calcTimePoint (posManager->getFirstTimePoint(), (int)(selection->y() + selection->height()) * posManager->getTWHF(), true);
		
		emit regionSelected (TopSelected, BottomSelected);
		
	}
}

//EVENT:
void TimeScale::contentsMouseReleaseEvent (QMouseEvent *ev)
{
	// Get position manager
	TTTPositionManager* posManager = TTTManager::getInst().getCurrentPositionManager();
	if(!posManager)
		return;
	
	if (! posManager->getFiles())
		return;
	
	if (! Selecting) {
		ev->ignore();
		return;
	}
	if (! selection)
		return;
	
	TopSelected = posManager->getFiles()->calcTimePoint (posManager->getFirstTimePoint(), (int)(selection->y()) * posManager->getTWHF(), false);
	BottomSelected = posManager->getFiles()->calcTimePoint (posManager->getFirstTimePoint(), (int)(selection->y() + selection->height()) * posManager->getTWHF(), true);
	
	emit regionSelected (TopSelected, BottomSelected); //, (Selecting == 1 ? true : false));
	
	
	switch (ev->button()) {
		/*
		case Qt::LeftButton:
			emit regionSelected (TopSelected, BottomSelected, (Selecting == 1 ? true : false));
			break;
		*/
		case Qt::MidButton:
			MiddleButtonPressed = false;
			break;
		/*
		case Qt::RightButton:
			emit regionSelected (TopSelected, BottomSelected, (Selecting == 1 ? true : false));
			break;
		*/
		default:
			;
	}
	
	ev->accept();
}

//SLOT:
void TimeScale::shiftTimeScale (QPoint _shift)
{
	// Get position manager
	TTTPositionManager* posManager = TTTManager::getInst().getCurrentPositionManager();
	if(!posManager)
		return;

	if (_shift.isNull())
		return;
	
	long int yshift = _shift.y() * TTTManager::getInst().getBasePositionManager()->getTWHF();
	
	TopSecond += yshift;
        TopSecond = (TopSecond > 0) ? TopSecond : 0;					//must be 0 minimal
	//cannot be higher than (highest second) - (seconds fitting in current zoom mode)
	TopSecond = QMIN ((long int) DisplayedSeconds - (height() * 100) / zoomFactor * posManager->getTWHF(), TopSecond);
	
	top += _shift.y();
	
	
	//zoom is not yet included in _shift => do it...
	//_shift /= (float)zoomFactor / 100.0f;
	float shift = - (float)_shift.y() / ((float)zoomFactor / 100.0f);
	
	QMatrix wm = this->worldMatrix();
	
	//set world matrix
	wm.translate (0, shift);
	
	//note: it is assumed that there is no shift beyond the bounds 
	//(as a shift is always triggered by a shift of the tree)
	this->setWorldMatrix (wm);
}

//SLOT:
void TimeScale::selectRegion (int _select)
{
	if (_select == 0 && Selecting != 0) {
		Selecting = 0;
		if (selection) {
			selection->hide();
			canvas.update();
		}
	}
	else if (_select == 1 && Selecting != 1) {
		Selecting = 1;
		if (selection) {
			selection->show();
			drawSelectionBox ((int)selection->y(), selection->height());
		}
		else
			drawSelectionBox (-1, -1);
	}
}

void TimeScale::drawSelectionBox (int _top, int _height)
{
	// Get position manager
	TTTPositionManager* posManager = TTTManager::getInst().getCurrentPositionManager();
	if(!posManager)
		return;

	if (_top == -1)
		_top = 0;
	if (_height == -1)
		_height = DisplayedSeconds / posManager->getTWHF();
	
	if (! selection) {
		selection = new Q3CanvasRectangle (0, 0, canvas.width(), DisplayedSeconds / posManager->getTWHF(), &canvas);
		selection->setZ (0.5);
		selection->setPen (QPen (SelectionColor, 2));
		selection->show();
	}
	
	if (selection) {
		selection->setY (_top);
		selection->setSize (selection->width(), _height);
		canvas.update();
	}
}

int TimeScale::getLogical (int _y, bool _shift)
{
	//if _shift == false (default), the absolute second is returned
	//          == true,            the difference in seconds is returned (regardless of displayed region)
	
	QMatrix wm = this->worldMatrix();
	if (wm.isInvertible()) {
		QPoint r =  wm.invert().map (QPoint (0, _y));
		if (_shift)
			r.setY (r.y() - top);
		
		return r.y();
	}
	else
		return -1;
}

void TimeScale::selectRegion (int _start, int _stop)
{
	// Get position manager
	TTTPositionManager* posManager = TTTManager::getInst().getCurrentPositionManager();
	if(!posManager)
		return;

	if (Selecting == 0)
		return;			//only possible in selection mode
	
	//update box display
	int top2 = posManager->getFiles()->calcSeconds (posManager->getFirstTimePoint(), _start) / posManager->getTWHF();
	int bottom = posManager->getFiles()->calcSeconds (posManager->getFirstTimePoint(), _stop) / posManager->getTWHF();
	drawSelectionBox (top2, bottom - top2);
	
	TopSelected = _start;
	BottomSelected = _stop;
	
	emit regionSelected (_start, _stop);
}

void TimeScale::setTopSecond (int _topSecond)
{
	// Get position manager
	TTTPositionManager* posManager = TTTManager::getInst().getCurrentPositionManager();
	if(!posManager)
		return;
	
	TopSecond = _topSecond;
	top = _topSecond / posManager->getTWHF();
	//draw();
}

void TimeScale::resetSelection()
{
	TopSelected = -1;
	BottomSelected = -1;

	if(selection) {
		delete selection;
		selection = 0;
	}
}


//#include "timescale.moc"

