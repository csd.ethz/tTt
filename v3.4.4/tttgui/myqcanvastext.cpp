/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "myqcanvastext.h"

int MyQCanvasText::RTTI = 1007;

void MyQCanvasText::draw (QPainter& _p)
{
	//save the current painter state
	_p.save();
	
	
	//rotate the painter with the angle, but without shifting the x/y-position of the text
	_p.translate (x(), y());		//now the rotation point is the position of the text
	_p.rotate (angle);
	_p.translate (-x(), -y());		//translate to original position so that the text is at the desired position
	
	//call original QCanvasText drawing function
	Q3CanvasText::draw (_p);
	
	//reset the original painter
	_p.restore();
}

