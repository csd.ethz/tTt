/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "rectbox.h"

RectBox::~RectBox()
{
	// D O  N O T  D E L E T E  Parent!!!
}

void RectBox::initRectBox (QPaintDevice *_parent, const QRect &_rect, QColor _color, float _zoom)
{
	Parent = _parent;
	Rect = _rect;
	OriginalRect = _rect;
	Color = _color;
	ZoomFactor = _zoom;
	ZoomFactorY = _zoom;
	Initialized = true;
}

void RectBox::draw ()
{
	if (! Parent)
		return;
	
	QPainter _p (Parent);
	
	_p.setPen (Color);
        //_p.setCompositionMode (QPainter::RasterOp_SourceXorDestination);
	//Rect needs to be stretched by ZoomFactor before painting
	QRect xRect ((int)(Rect.left() * ZoomFactor), (int)(Rect.top() * ZoomFactorY), (int)(Rect.width() * ZoomFactor), (int)(Rect.height() * ZoomFactorY));
	_p.drawRect (xRect);
	
	_p.end();
}

//void RectBox::drawWindow (int _left, int _top, int _width, int _height)
//{
//	QPainter _p (Parent);
//
//	_p.setWindow (_left, _top, _width, _height);
//
//	_p.setPen (Color);
//        _p.setCompositionMode (QPainter::RasterOp_SourceXorDestination);
//        //_p.setRasterOp (Qt::XorROP);
//	//Rect needs to be stretched by ZoomFactor before painting
//	QRect xRect ((int)(Rect.left() * ZoomFactor), (int)(Rect.top() * ZoomFactorY), (int)(Rect.width() * ZoomFactor), (int)(Rect.height() * ZoomFactorY));
//	_p.drawRect (xRect);
//
//	_p.end();
//}
//
//void RectBox::drawEllipse()
//{
//	QPainter p (Parent);
//	p.setPen (Color);
//        p.setCompositionMode (QPainter::RasterOp_SourceXorDestination);
////	p.setRasterOp (Qt::XorROP);
//	//Rect needs to be stretched by ZoomFactor before painting
//	QRect xRect ((int)(Rect.left() * ZoomFactor), (int)(Rect.top() * ZoomFactorY), (int)(Rect.width() * ZoomFactor), (int)(Rect.height() * ZoomFactorY));
//	p.drawEllipse (xRect);
//	p.end();
//}

void RectBox::reset()
{
	Rect = OriginalRect;
}

void RectBox::truncate()
{
	//if Rect is not inside the original, the edges overlapping are cut off
	if (! OriginalRect.contains (Rect)) {
		Rect = Rect.intersect (OriginalRect).normalize();
	}
}

void RectBox::shift (int _x, int _y, int _boundLeft, int _boundTop, int _boundRight, int _boundBottom)
{
	if (_boundLeft != -1)
		if (Rect.left() + _x < _boundLeft)
			_x = 0;
	if (_boundRight != -1)
		if (Rect.right() + _x > _boundRight)
			_x = 0;
	if (_boundTop != -1)
		if (Rect.top() + _y < _boundTop)
			_y = 0;
	if (_boundBottom != -1)
		if (Rect.bottom() + _y > _boundBottom)
			_y = 0;
	
	Rect.moveBy (_x, _y);
}

void RectBox::shiftMax (int _x, int _y, int _boundLeft, int _boundTop, int _boundRight, int _boundBottom)
{
	if (_boundLeft != -1)
		if (Rect.left() + _x < _boundLeft)
			_x = _boundLeft - Rect.left();
	if (_boundRight != -1)
		if (Rect.right() + _x > _boundRight)
			_x = _boundRight - Rect.right();
	if (_boundTop != -1)
		if (Rect.top() + _y < _boundTop)
			_y = _boundTop - Rect.top();
	if (_boundBottom != -1)
		if (Rect.bottom() + _y > _boundBottom)
			_y = _boundBottom - Rect.bottom();
	
	Rect.moveBy (_x, _y);
}

void RectBox::resize (int _width, int _height)
{
	if (_width > 0)		Rect.setWidth (_width);
	if (_height > 0)	Rect.setHeight (_height);
}

void RectBox::shiftTopLeft (int _x, int _y)
{
	Rect.setTop (Rect.top() + _y);
	Rect.setLeft (Rect.left() + _x);
}

void RectBox::shiftBottomRight (int _x, int _y)
{
	Rect.setBottom (Rect.bottom() + _y);
	Rect.setRight (Rect.right() + _x);
}

void RectBox::setTopLeft (int _x, int _y)
{
	Rect.setTop (_y);
	Rect.setLeft (_x);
}

void RectBox::setBottomRight (int _x, int _y)
{
	Rect.setBottom (_y);
	Rect.setRight (_x);
}

void RectBox::setPosition (int _x, int _y)
{
	Rect.setBottom (Rect.height() + _y);
	Rect.setRight (Rect.width() + _x);
	Rect.setTop (_y);
	Rect.setLeft (_x);
}

void RectBox::setBox (QRect _rect)
{
	Rect = _rect;
}

void RectBox::setZoomFactor (float _zoomFactorX, float _zoomFactorY)
{
	if (_zoomFactorX != 0)
		ZoomFactor = _zoomFactorX;
	if (_zoomFactorY != 0)
		ZoomFactorY = _zoomFactorY;
}

