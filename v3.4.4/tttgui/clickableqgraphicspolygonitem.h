/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CLICKABLEQGRAPHICSPOLYGONITEM_H
#define CLICKABLEQGRAPHICSPOLYGONITEM_H

#include "callbackitem.h"

#include <QGraphicsPolygonItem>

/**
 * @author Bernhard
 *
 * This class provides a polygon item within a QGraphicsView object that calls a callback function when it is clicked (mouse pressed or released, distinct).
 * The callback management is handled in CallbackItem
 * WARNING: the order of inheritance is important here! (otherwise you'd get a compile error, complaining about the moc files).
 *          QGraphicsItem (+ subclasses) does not inherit from QObject!
 */
class ClickableQGraphicsPolygonItem : public QGraphicsPolygonItem, public CallbackItem
{

public:
        ClickableQGraphicsPolygonItem (const QString &_parameter = "");

        ClickableQGraphicsPolygonItem (QGraphicsPolygonItem &_polyItem, const QString &_parameter);

        void mousePressEvent (QGraphicsSceneMouseEvent *_ev);
        void mouseReleaseEvent (QGraphicsSceneMouseEvent *_ev);

};

#endif // CLICKABLEQGRAPHICSPOLYGONITEM_H
