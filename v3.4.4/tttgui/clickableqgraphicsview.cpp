/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "clickableqgraphicsview.h"

#include "dragableqgraphicsitem.h"

#include <QMouseEvent>
#include <QGraphicsSceneDragDropEvent>

ClickableQGraphicsView::ClickableQGraphicsView (QWidget *_parent, const QString &_parameter)
        : QGraphicsView (_parent), CallbackItem (_parameter)
{

        setAcceptDrops (true);
        dragOver = false;
}

void ClickableQGraphicsView::mousePressEvent (QMouseEvent *_ev)
{
        this->QGraphicsView::mousePressEvent (_ev);

        callCBMP (this, _ev);

        _ev->accept();
}

void ClickableQGraphicsView::mouseReleaseEvent (QMouseEvent *_ev)
{
        this->QGraphicsView::mouseReleaseEvent (_ev);

        callCBMR (this, _ev);

        _ev->accept();
}

void ClickableQGraphicsView::dragEnterEvent (QDragEnterEvent *_ev)
{
//this method is not called...?
        if (_ev->mimeData()->text() == GRAPHICS_ADJUST_MIME_DATA_TEXT) {
                _ev->setAccepted (true);
                dragOver = true;
                //update();
        } else {
                _ev->setAccepted (false);
        }
}

void ClickableQGraphicsView::dragMoveEvent (QDragMoveEvent *_ev)
{
        //note: needs to be reimplemented to receive the dropEvent()!
        _ev->accept();
}

void ClickableQGraphicsView::dragLeaveEvent (QDragLeaveEvent *_ev)
{
        Q_UNUSED (_ev);
        dragOver = false;
        //update();
}

void ClickableQGraphicsView::dropEvent (QDropEvent *_ev)
{
        dragOver = false;

        if (_ev->mimeData()->text() == GRAPHICS_ADJUST_MIME_DATA_TEXT) {
                //set the dropped item to the new position
                QPointF pos = _ev->pos();
                pos = mapToScene (pos.toPoint());
                if (DragableQGraphicsItem::draggedItem)
                        DragableQGraphicsItem::draggedItem->wasDropped (pos);
        }

        update();
}
