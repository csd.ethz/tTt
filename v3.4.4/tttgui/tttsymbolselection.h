/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TTTSYMBOLSELECTION_H
#define TTTSYMBOLSELECTION_H

#include "ui_frmSymbols.h"
#include "tttdata/symbol.h"
#include "tttdata/track.h"

#include <q3buttongroup.h>
#include <qpushbutton.h>
#include <qspinbox.h>
#include <qevent.h>
#include <qlabel.h>
#include <qcolordialog.h>
//Added by qt3to4:
#include <QCloseEvent>
#include <QWidget>

const int ColorCount = 6;

/**
@author Bernhard
*	
*	The class for the symbol selection or handling GUI	
*/

class TTTSymbolSelection: public QWidget, public Ui::frmSymbols {

Q_OBJECT

public:
	
	TTTSymbolSelection (QWidget *parent = 0, const char *name = 0);
	
	void closeEvent (QCloseEvent *);
	
	///set the value of the spinboxes
	void setTimePoints (int _first, int _last);
	
signals:

        ///emitted when the form is closed, no matter if via close or cancel or if apply was pressed
	///(the difference is in the Symbol instance)
	void symbolSet (Symbol);
	
	///emitted when the user clicks the position button
	///caught by TTTMovie, induces the position selection mode
	///(similar to colocation radius selection mode)
        void symbolPositionDesired (Symbol);
	
	///emitted on user-click, indicates that all symbols should be deleted
	void symbolDelete();
	
private slots:
	
	///called when the user clicks the apply or close button
	///sets the currently selected attributes
	void apply();
	
	///called when the user clicks the cancel button
	///closes the form and sets default values for the symbol to indicate abort
	void cancel();
	
	///called by a click on the symbol button group box
	void setSymbol (int);
	
	///called by a click on the color button group box
	void setColor();
	
	///called by a change of the size spinbutton value
	void setSize (int);
	
	///called by a click on the position button
	void selectPosition();
	
	///sets the currently active track to be followed by the selected symbol
	void selectTrack();
	
	///deletes all symbols associated with this colony
	void deleteSymbols();
	
	///sets the angle of the symbol (0 is default layout, rotation is counter-clockwise with positive angles)
	void setAngle (int);
	
private:

	///saves the current settings
        Symbol symbol;
	
};

#endif
