/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "tttcellproperties.h"
//Added by qt3to4:
#include <QCloseEvent>

#include "tttdata/userinfo.h"
#include "tttdata/trackingkeys.h"

TTTCellProperties::TTTCellProperties(QWidget *parent)
    :QDialog(parent)
{	
   setupUi (this);

	// Add minimize/maximize buttons
   setWindowFlags( windowFlags() | Qt::CustomizeWindowHint | Qt::WindowMinMaxButtonsHint);

        //hide all frames - none is selected at begin!
	bgrBlood->hide();
	bgrStroma->hide();
	bgrNerve->hide();
	bgrBloodMyeloid->hide();
	bgrBloodLymphoid->hide();
	bgrBloodHSC->hide();
	bgrStromaPrimary->hide();
	bgrStromaCellLine->hide();
	bgrNerveNeuro->hide();
	bgrNerveGlia->hide();
	optAdherent->hide();
	optFreeFloating->hide();
	optSpyhop->hide();
	chkSetAdherence->hide();
	chkSetTissue->hide();
	chkSetTissue->setChecked (true);
	chkSetWavelength->hide();
	chkSetWavelength->setChecked (true);
	optFreeFloating->setChecked (true);
	
	connect ( pbtNothing, SIGNAL (clicked()), this, SLOT (selectNothing()));
	connect ( pbtClose, SIGNAL (clicked()), this, SLOT (close()));
	connect ( bgrTissue, SIGNAL (clicked (int)), this, SLOT (showTissueFrame (int)));
	connect ( bgrBlood, SIGNAL (clicked (int)), this, SLOT (showBloodFrame (int)));
	connect ( bgrStroma, SIGNAL (clicked (int)), this, SLOT (showStromaFrame (int)));
	connect ( bgrNerve, SIGNAL (clicked (int)), this, SLOT (showNerveFrame (int)));
	connect ( bgrBloodMyeloid, SIGNAL (clicked (int)), this, SLOT (setCellLineage (int)));
	connect ( bgrBloodLymphoid, SIGNAL (clicked (int)), this, SLOT (setCellLineage (int)));
	connect ( bgrBloodHSC, SIGNAL (clicked (int)), this, SLOT (setCellLineage (int)));
	connect ( bgrStromaPrimary, SIGNAL (clicked (int)), this, SLOT (setCellLineage (int)));
	connect ( bgrStromaCellLine, SIGNAL (clicked (int)), this, SLOT (setCellLineage (int)));
	connect ( bgrNerveNeuro, SIGNAL (clicked (int)), this, SLOT (setCellLineage (int)));
	connect ( bgrNerveGlia, SIGNAL (clicked (int)), this, SLOT (setCellLineage (int)));
	
	
	//create array of all radio buttons for easy access to them
	QString key = "";
	QString wName = "";
	windowLayoutLoaded = false;

        foreach (QWidget *w, QApplication::allWidgets()) {
                if (w) {
                        if ( (w != this) && (w->topLevelWidget() == this)) {
                                if (w->isA ("QRadioButton")) {
                                        wName = w->name();
                                        key = wName.right (wName.length() - 4);		//remove "opt_" from begin
                                        //key = QString("%1_%2_%3").arg (tissueTypeIndex).arg (cellGeneralTypeIndex).arg (_index + 1);
                                        options.insert (key, (QRadioButton *) w);
                                }
                        }
                }
//                ++it;
        }
		

	
	tissueTypeIndex = -1;
	cellGeneralTypeIndex = -1;
}

void TTTCellProperties::closeEvent (QCloseEvent *ev)
{
	optAdherent->hide();
	optFreeFloating->hide();
	optSpyhop->hide();
	chkSetAdherence->hide();
	chkSetTissue->hide();
	chkSetWavelength->hide();

	// Save window layout
	UserInfo::getInst().saveWindowPosition(this, "TTTCellProperties");
	
	ev->accept();
}

//EVENT:
void TTTCellProperties::showEvent (QShowEvent *_ev)
{
	if(!_ev->spontaneous() && !windowLayoutLoaded) {
		// Load window layout
		UserInfo::getInst().loadWindowPosition(this, "TTTCellProperties");

		windowLayoutLoaded = true;
	}

	_ev->accept();
}

void TTTCellProperties::showTissueFrame (int _index)
{
	//hide all frames
	bgrBlood->hide();
	bgrStroma->hide();
	bgrNerve->hide();
	
	//note:
	//the button state (on) is set as showTissueFrame() is also called for displaying stored properties
	// -> for this purpose, the buttons must be switched on
	
	//show the desired frame
	switch (_index) {
		case 0:			//blood
			pbtBlood->setOn (true);
			bgrBlood->show();
			cellProperties.tissueType = Blood;
			break;
		case 1:			//stroma
			pbtStroma->setOn (true);
			bgrStroma->show();
			cellProperties.tissueType = Stroma;
			break;
		case 2:			//endothelium
			pbtEndothelium->setOn (true);
			cellProperties.tissueType = Endothelium;
			break;
		case 3:			//heart
			pbtHeart->setOn (true);
			cellProperties.tissueType = Heart;
			break;
		case 4:			//nerve
			pbtNerve->setOn (true);
			bgrNerve->show();
			cellProperties.tissueType = Nerve;
			break;
		case 5:			//endoderm
			pbtEndoderm->setOn (true);
			cellProperties.tissueType = Endoderm;
			break;
		case 6:			//mesoderm
			pbtMesoderm->setOn (true);
			cellProperties.tissueType = Mesoderm;
			break;
		default:
			cellProperties.tissueType = TT_NONE;
	}
	
	tissueTypeIndex = _index;
}

void TTTCellProperties::showBloodFrame (int _index)
{
	//if (bgrBloodMyeloid
	//hide all frames
	bgrBloodMyeloid->hide();
	bgrBloodLymphoid->hide();
	bgrBloodHSC->hide();
	
	//show the desired frame
	switch (_index) {
		case 0:			//myeloid
			pbtMyeloid->setOn (true);
			bgrBloodMyeloid->show();
			cellProperties.cellGeneralType = Myeloid;
			break;
		case 1:			//lymphoid
			pbtLymphoid->setOn (true);
			bgrBloodLymphoid->show();
			cellProperties.cellGeneralType = Lymphoid;
			break;
		case 2:			//hsc
			pbtHSC->setOn (true);
			bgrBloodHSC->show();
			cellProperties.cellGeneralType = HSC;
			break;
		case 3:
			pbtPrimitive->setOn (true);
			cellProperties.cellGeneralType = Primitive;
			break;
		default:
			cellProperties.cellGeneralType = CGT_NONE;
	}
	
	cellGeneralTypeIndex = _index;
}

void TTTCellProperties::showStromaFrame (int _index)
{
	//hide all frames
	bgrStromaPrimary->hide();
	bgrStromaCellLine->hide();
	
	//show the desired frame
	switch (_index) {
		case 0:			//primary
			pbtPrimary->setOn (true);
			bgrStromaPrimary->show();
			cellProperties.cellGeneralType = Primary;
			break;
		case 1:			//cell line
			pbtCellLine->setOn (true);
			bgrStromaCellLine->show();
			cellProperties.cellGeneralType = CellLine;
			break;
		default:
			cellProperties.cellGeneralType = CGT_NONE;
	}	
	
	cellGeneralTypeIndex = _index;
}

void TTTCellProperties::showNerveFrame (int _index)
{
	//hide all frames
	bgrNerveNeuro->hide();
	bgrNerveGlia->hide();
	
	//show the desired frame
	switch (_index) {
		case 0:			//neuro
			pbtNeuro->setOn (true);
			bgrNerveNeuro->show();
			cellProperties.cellGeneralType = Neuro;
			break;
		case 1:			//glia
			pbtGlia->setOn (true);
			bgrNerveGlia->show();
			cellProperties.cellGeneralType = Glia;
			break;
		default:
			cellProperties.cellGeneralType = CGT_NONE;
	}	
	
	cellGeneralTypeIndex = _index;
}

void TTTCellProperties::displayCellLineage (int _index)
{
	setCellLineage (_index - 1);
	
	if (_index > -1) {
		//note: assume that the tissue and general type were already set
		
		QString key = QString("%1_%2_%3").arg (tissueTypeIndex)
										.arg (cellGeneralTypeIndex)
										.arg (_index);
		QRadioButton *tmpRB = options.find (key);
		if (tmpRB) {
			tmpRB->setChecked (true);
		}
	}
	else {
		//unselect all option buttons
		//the keys have to be constructed in a strange way, as there is no iterator for QDict
		
		int max = 20;
		QString tmp = "";
		for (int i = 0; i < max; i++) {
			for (int j = 0; j < max; j++) {
				for (int k = 0; k < max; k++) {
					tmp = QString("%1_%2_%3").arg (i).arg (j).arg (k);
					if (options.find (tmp))
						options.find (tmp)->setChecked (false);
				}
			}
		}
		
		//for (QDict<QRadioButton>::Iterator iter = options.begin(); iter != options.end(); ++iter)
		//	if (iter)
		//		iter->setChecked (false);
	}
}
	
void TTTCellProperties::setCellLineage (int _index)
{
	//note: if _index == -1, cellLineage = 0 -> nothing
	
	if (_index < 0)
		cellProperties.cellLineage = 0;
	else
		cellProperties.cellLineage = _index + 1;
}

CellProperties TTTCellProperties::getProperties()
{	
	cellProperties.WaveLength[1] = (pbtWaveLength1->isOn() ? 1 : 0);
	cellProperties.WaveLength[2] = (pbtWaveLength2->isOn() ? 1 : 0);
	cellProperties.WaveLength[3] = (pbtWaveLength3->isOn() ? 1 : 0);
	cellProperties.WaveLength[4] = (pbtWaveLength4->isOn() ? 1 : 0);
	cellProperties.WaveLength[5] = (pbtWaveLength5->isOn() ? 1 : 0);
	
	cellProperties.Set_Tissues = true;
	if (chkSetTissue->isVisible())
		cellProperties.Set_Tissues = chkSetTissue->isChecked();
	cellProperties.Set_Wavelengths = true;
	if (chkSetWavelength->isVisible())
		cellProperties.Set_Wavelengths = chkSetWavelength->isChecked();
	
	cellProperties.Set_FreeFloating = false;
	cellProperties.Set_NonAdherent = false;
	if (optAdherent->isVisible()) {
		if (chkSetAdherence->isChecked()) {
			cellProperties.Set_FreeFloating = true;
			cellProperties.Set_NonAdherent = true;
			
			cellProperties.NonAdherent = ! (optAdherent->isChecked());
			cellProperties.FreeFloating = optFreeFloating->isChecked();
			if (optSpyhop->isChecked()) {
				cellProperties.NonAdherent = true;
				cellProperties.FreeFloating = false;
			}
		}
	}

	// Tracking keys
	cellProperties.Set_Addon = false;
	if(grbTrackingKeys->isChecked()) {
		QAbstractButton* checkedButton = bgrTrackingKeys->checkedButton();
		if(checkedButton) {
			QString key = checkedButton->text();
			if(key.length() == 1) {
				cellProperties.AddOn1 = TrackingKeys::getValue(key[0]);
				cellProperties.Set_Addon = true;
			}
			else if(key == "None") {
				cellProperties.AddOn1 = 0;
				cellProperties.Set_Addon = true;
			}
		}
	}

	//all other attributes where already set on button click!
	
	return cellProperties;
}

void TTTCellProperties::selectNothing()
{
	bgrTissue->setButton (-1);
	showTissueFrame (-1);
	showBloodFrame (-1);
	showStromaFrame (-1);
	showNerveFrame (-1);
	displayCellLineage (-1);
	pbtWaveLength1->setOn (false);
	pbtWaveLength2->setOn (false);
	pbtWaveLength3->setOn (false);
	pbtWaveLength4->setOn (false);
	pbtWaveLength5->setOn (false);
	pbtMyeloid->setOn (false);
	pbtLymphoid->setOn (false);
	pbtHSC->setOn (false);
	pbtPrimitive->setOn (false);
	pbtPrimary->setOn (false);
	pbtCellLine->setOn (false);
	pbtNeuro->setOn (false);
	pbtGlia->setOn (false);
	pbtBlood->setOn (false);
	pbtStroma->setOn (false);
	pbtEndothelium->setOn (false);
	pbtHeart->setOn (false);
	pbtNerve->setOn (false);
	pbtEndoderm->setOn (false);
	pbtMesoderm->setOn (false);
	chkSetTissue->setChecked (true);
	chkSetWavelength->setChecked (true);
	
	optAdherent->setChecked (false);
	optFreeFloating->setChecked (false);
	optSpyhop->setChecked (false);
	chkSetAdherence->setChecked (false);
}

void TTTCellProperties::setProperties (CellProperties _props)
{
	
	//bad failure! -> if activated, the user must select the properties anew for each trackpoint!
	//selectNothing();
	
	//@todo
	//if the user changed the properties for an already tracked cell at the first timepoint while tracking it again, the old properties should not be loaded, rather the new ones be taken over
	if (_props.tissueType != TT_NONE) {
	
		showTissueFrame (_props.tissueType - 1);
		
		if (_props.cellGeneralType != CGT_NONE) {
			if (_props.tissueType == Blood)
				showBloodFrame (_props.cellGeneralType - 1);
			else if (_props.tissueType == Stroma)
				showStromaFrame (_props.cellGeneralType - 5);
			else if (_props.tissueType == Nerve)
				showNerveFrame (_props.cellGeneralType - 7);
		}
		
		if (_props.cellLineage != 0)
			displayCellLineage (_props.cellLineage);
		
		pbtWaveLength1->setOn (_props.WaveLength[1]);
		pbtWaveLength2->setOn (_props.WaveLength[2]);
		pbtWaveLength3->setOn (_props.WaveLength[3]);
		pbtWaveLength4->setOn (_props.WaveLength[4]);
		pbtWaveLength5->setOn (_props.WaveLength[5]);
	}

	if (optAdherent->isVisible()) {
		optAdherent->setChecked (! _props.NonAdherent);
		optFreeFloating->setChecked (_props.FreeFloating);
		optSpyhop->setChecked (_props.NonAdherent & ! _props.FreeFloating);
	}

}

void TTTCellProperties::showFurtherOptions (bool _showAdherence)
{
	if (_showAdherence) {
		optAdherent->show();
		optFreeFloating->show();
		optSpyhop->show();
		chkSetAdherence->show();
		chkSetTissue->show();
		chkSetWavelength->show();
	}
	else {
		optAdherent->hide();
		optFreeFloating->hide();
		optSpyhop->hide();
		chkSetAdherence->hide();
		chkSetTissue->hide();
		chkSetWavelength->hide();
	}
}

//#include "tttcellproperties.moc"

