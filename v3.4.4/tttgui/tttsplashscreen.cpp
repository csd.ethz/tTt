/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "tttsplashscreen.h"
//Added by qt3to4:
#include <QTextStream>
#include <QMouseEvent>

//long version string, contains if 32 or 64 bit version (definition)
QString tttVersionLong;

TTTSplashScreen::TTTSplashScreen(QWidget *parent, const char *name)
    :QDialog(parent, name)
{
    setupUi (this);

	// Disable context-help button
	setWindowFlags( (windowFlags() | Qt::CustomizeWindowHint) & (~Qt::WindowContextHelpButtonHint));

	// Build long version string
	if(sizeof(int*) == 4)
		tttVersionLong = Version + " (32 bit)";
	else if (sizeof(int*) == 8)
		tttVersionLong = Version + " (64 bit)";
	else
		tttVersionLong = Version;

	// Splash screen image
	QImage pix(":/ttt/ttticons/TTT_IconBig.png");
	if(!pix.isNull())
		pixmapLabel1->setPixmap(QPixmap::fromImage(pix));

	QTimer tmr;
	tmr.singleShot (ShowSplasherTime * 1000, this, SLOT (close()));
	lblVersion->setText ("");
	lblVersion->setText (QString ("<font size=""+1""><b>Version %1</b></font>").arg (tttVersionLong));
	
	/*
	OH 07.04.2014: removed, because version file is not used anymore
	//the executable's directory is not yet available here (qApp == 0)
	//look for latest version file on the net
	//if this version and the net version file do not correspond, a message is displayed that there is a later version
	QDir dir (SystemInfo::defaultNAS());
	//QDir dir (SystemInfo::getExecutablePath());
	
	QString version = "";
	if (dir.exists()) {
		QFile versionFile (dir.absPath() + "/tttversion");
		if (versionFile.open (QIODevice::ReadOnly)) {
                        QTextStream stream (&versionFile);
			if (! stream.atEnd())
				version = stream.readLine(); 
			
			versionFile.close();
		}
	}
	
	if (! version.isEmpty()) {
		if (version > internalVersion)
			QMessageBox::warning (this, "Version out of date", "The tTt version you are using is out of date. \nPlease try to use the latest one, as support cannot be guaranteed.", "OK");
	}
	*/
}

void TTTSplashScreen::mouseReleaseEvent (QMouseEvent *)
{
	close();
}

//void TTTSplashScreen::mousePressEvent( QMouseEvent * _event )
//{
//	close();
//}


//#include "tttsplashscreen.moc"
