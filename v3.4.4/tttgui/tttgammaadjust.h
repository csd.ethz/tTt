/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TTTGAMMAADJUST_H
#define TTTGAMMAADJUST_H

#include "ui_frmGammaAdjust.h"

// Forward declarations
class TTTPositionManager;
class BDisplay;
class PropagatingQFrame;



const QString GAMMAADJUSTVIEW = "GAMMAVIEW";
const QString GAMMACIRCLE = "GAMMACIRCLE";

const QColor GRAPHICS_COLOR = Qt::red;
const int ANCHOR_DETECTION_RANGE = 10;

class TTTGammaAdjust : public QDialog, public Ui::frmGammaAdjust
{
        Q_OBJECT

public:
        TTTGammaAdjust (QWidget *parent = 0);

        ~TTTGammaAdjust();

        /**
         * display the attributes of the provided display object, hidden in the position manager and wl attribute
         */
        void setDisplay (TTTPositionManager *_tttpm, int _wavelength);

        /* *
         * the mouse-release callback function, for various purposes
         */
        //static void handleActions (const QString &_param, void *, QMouseEvent *);

protected:

	// Events
	void resizeEvent (QResizeEvent *);
	void showEvent (QShowEvent *_ev);
	void closeEvent (QCloseEvent *);

signals:

        /**
         * emitted when the user changes a graphical setting
         */
        void graphicsAdjusted (int _wavelength);

public slots:

        /**
         * set all values to the default values
         */
        void resetDisplay();

private slots:

        /**
         * calculates the best values for black/whitepoint, gamma correction, contrast, ...
         */
        void autoFit();

        /**
         * opens a dialog where the user can choose the transcolor
         */
        void chooseTransColor();

        /**
         * exports the current settings to the RAM
         */
        void exportDisplaySettings();

        /**
         * pastes the currently stored settings from the RAM
         */
        void importDisplaySettings();

        /**
         * set brightness from spinbox value
         */
        void setBrightness (int);

        /**
         * set contrast from spinbox value
         */
        void setContrast (int);

        /**
         * sets the alpha value for the picture from the spinbox value
         */
        void setAlpha (int);

        /**
         * following handleFrame...: connected to the embedded QFrame's signals, which only propagate mouseEvents
         */
        void handleFrameMousePressed (QMouseEvent *);
        void handleFrameMouseReleased (QMouseEvent *);
        void handleFrameMouseMoved (QMouseEvent *);

        /**
         * displays a window with color information for the current picture
         */
        void showPictureInformation();

		/**
		 * Show or hide the controls except the graphics frame, so window size can be reduced dramatically
		 */
		void showHideControls();

private:

//        ///a QGraphicsView object which returns its mouse-release events to handleViewClick()
//        ClickableQGraphicsView *gview;
//
//        DragableQGraphicsItem *whiteLine;
//        DragableQGraphicsItem *blackLine;
//        DragableQGraphicsItem* gammaCircles[3];

        //pointer to the current BDisplay object, to propagate changes
        BDisplay *displaySettings;

        int wavelength;

        ///a QFrame that converts its xxxevent() methods to signals
        PropagatingQFrame *pqFrame;

        ///whether the user can change the function of the gamma correction
        bool gammaShifting;

        ///the index of the currently shifted gamma anchor point (-1 <=> no shifting is in progress)
        int currentAnchorPoint;

		///window layout already loaded
		bool windowLayoutLoaded;

		///controls hidden
		bool controlsHidden;

//private methods

        /**
         * display all settings in the current BDisplay object
         */
        void adaptDisplay();
};

#endif // TTTGAMMAADJUST_H
