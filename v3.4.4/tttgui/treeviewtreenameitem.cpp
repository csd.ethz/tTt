/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "treeviewtreenameitem.h"

// Qt
#include <QGraphicsSceneMouseEvent>
#include <QApplication>

// Project
#include "tttbackend/itrack.h"
#include "tttbackend/itree.h"
//#include "tttbackend/treeviewsettings.h"
#include "tttdata/userinfo.h"
#include "treeviewtreeitem.h"
#include "treeview.h"


TreeViewTreeNameItem::TreeViewTreeNameItem( TreeViewTreeItem* _treeViewTreeItem, const QString& _text )
	: QGraphicsTextItem(_text, _treeViewTreeItem)
{
	// Init variables
	treeViewTreeItem = _treeViewTreeItem;
	//displaySettings = TreeViewSettings::getInstance();
	movingTree = false;

	// Set z to same as cell number
	setZValue(TreeView::Z_CELL_NUMBER);

	// Enable hover events
	setAcceptHoverEvents(true);

	// Init diplay
	updateDisplay();
}

void TreeViewTreeNameItem::updateDisplay()
{
	// Update appearance
	setFont(QFont("Arial", UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TREEVIEW_TREENAMEFONTSIZE).toInt()));
	//setDefaultTextColor(displaySettings->getTreeNameColor());		// Color now set in TreeViewTreeItem::updateTreeNameDisplay()

	// Calc position 
	int x = treeViewTreeItem->getWidth() / 2 + TREE_NAME_DIST_TO_CENTER;
	
	// Move to first timepoint
	int y = 1;
	if(ITrack* root = treeViewTreeItem->getTree()->getRootNode()) 
		y = qMax(root->getFirstTimePoint() * treeViewTreeItem->getTreeView()->getTreeHeightFactor(), 1);
	y -= boundingRect().height() / 2;

	setPos(x, y);
}

void TreeViewTreeNameItem::mousePressEvent( QGraphicsSceneMouseEvent *_event )
{
	// Prevent this event from starting mouse navigation in TreeView
	treeViewTreeItem->getTreeView()->setIgnoreNextMousePressEvent();

	// Start moving tree 
	if(_event->button() == Qt::LeftButton) {
		movingTree = true;
		QApplication::setOverrideCursor(QCursor(Qt::ClosedHandCursor));
		//lastPosition = mapToScene(0, 0);
	}
}

void TreeViewTreeNameItem::mouseReleaseEvent( QGraphicsSceneMouseEvent* _event )
{
	// Stop tree movement
	if(movingTree && _event->button() == Qt::LeftButton) {
		movingTree = false;

		// Update scene rect in view
		treeViewTreeItem->getTreeView()->updateSceneRect();

		// Restore cursor
		QApplication::restoreOverrideCursor();
	}

	// Tree left clicked
	if(_event->button() == Qt::LeftButton) {
		treeViewTreeItem->getTreeView()->notifyTreeLeftClicked(treeViewTreeItem->getTree());
	}
}

void TreeViewTreeNameItem::mouseMoveEvent( QGraphicsSceneMouseEvent* _event )
{
	// Move tree
	if(movingTree) {
		// Get distance in x direction
		float deltaX = _event->scenePos().x() - _event->lastScenePos().x();

		// Update position of tree
		treeViewTreeItem->moveBy(deltaX, 0);
	}
}

void TreeViewTreeNameItem::hoverEnterEvent( QGraphicsSceneHoverEvent* _event )
{
	// Indicate that tree can be moved
	QApplication::setOverrideCursor(QCursor(Qt::OpenHandCursor));
}

void TreeViewTreeNameItem::hoverLeaveEvent( QGraphicsSceneHoverEvent* _event )
{
	// Restore cursor
	QApplication::restoreOverrideCursor();
}