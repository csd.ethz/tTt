/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TRACKINGMENUBAR_H
#define TRACKINGMENUBAR_H

// Qt includes
#include <QMenuBar>
#include <QVector>

// Forward declarations
class TTTTracking;

/**
 * This class represents the menu bar of a TTTTracking object
 */
class TrackingMenuBar : public QMenuBar {
public:
	TrackingMenuBar(TTTTracking* parent);

private:
	///creates all menus
	void createMenus();

	///connects actions
	void connectActions();

///Private data

	///movie window this menu belongs to
	TTTTracking* trackingWindow;

	///Public data
public:

	///Menus
	QMenu *fileMenu;
	QMenu *fileSubMenuLoadTrees;
	QMenu *fileSubMenuLoadPictures;
	QMenu *editMenu;
	QMenu *viewMenu;
	QMenu *viewSubMenuCellPath;
	QMenu *trackingMenu;
	QMenu *trackingSubMenuInheritProperties;
	QMenu *trackingSubMenuInheritWLs;
	QMenu *exportMenu;
	//QMenu *exportSubMenuMovie;
	QMenu *exportSubMenuTree; 
	QMenu *exportSubMenuData;
	QMenu *toolsMenu;
	QMenu *helpMenu;
	// Actions


    ///file menu
    QAction *actFile_OPEN_EXISTINGCOLONY;
    QAction *actFile_OPEN_ADDNEWCOLONY;
    QAction *actFile_OPEN_LAST_AUTOSAVE;
	QAction *actFile_OPEN_NEXT_TREE;
	QAction *actFile_OPEN_PREV_TREE;
    QAction *actFile_LOADING_PICTURES_TOGGLE_SELECTION;
    QAction *actFile_LOADING_PICTURES_SELECT;
    QAction *actFile_LOADING_PICTURES_DESELECT;
    QAction *actFile_LOADING_PICTURES_LOAD;
	QAction *actFile_LOADING_PICTURES_CURTREE;
    QAction *actFile_LOADING_PICTURES_UNLOAD;
    QAction *actFile_SAVING_TREES_SAVE;
    QAction *actFile_SAVING_TREES_SAVEAS;
    QAction *actFile_MARK_TREE_FINISHED;
    QAction *actFile_COLONYCREATION;
    QAction *actFile_COLONYCREATION_SELECT;
    QAction *actFile_COLONYCREATION_CREATE;
    QAction *actFile_DELETECOLONY;
    QAction *actFile_CLOSE_WINDOW;

    ///edit menu
    QAction *actEdit_CELLPROPS;
    QAction *actEdit_CELLPROPS_WAVELENGTH;
    QAction *actEdit_CELLPROPS_ADHERENCE;
    QAction *actEdit_CELLPROPS_TISSUETYPE;
    QAction *actEdit_CELLPROPS_GENERALTYPE;
    QAction *actEdit_CELLPROPS_LINEAGE;
    QAction *actEdit_CELLPROPS_CELLCYCLE;
    QAction *actEdit_CELLPROPS_CELLCYCLE_G0;
    QAction *actEdit_CELLPROPS_CELLCYCLE_G1;
    QAction *actEdit_CELLPROPS_CELLCYCLE_G2;
    QAction *actEdit_CELLPROPS_CELLCYCLE_S;
    QAction *actEdit_CELLPROPS_COMMENT;
    QAction *actEdit_CELLPROPS_WINDOW;
    QAction *actEdit_SELECTCELLS_AUTOMATED_DESELECTION;
    QAction *actEdit_SELECTCELLS;
    QAction* actEdit_CELL_COMMENT;
    QAction *actEdit_UNDO;
    QAction *actEdit_REDO;

    ///view menu
    QAction *actView_TRACKS;
    QAction *actView_TRACKS_FIRST;
    QAction *actView_TRACKS_POSITION;
    QAction *actView_TRACKS_ALL_POSITIONS;
    QAction *actView_TIME;
    QAction *actView_CIRCLEMOUSE;
    QAction *actView_CELLS;
    QAction *actView_CELLS_GATE;
    QAction *actView_CELLS_NUMBERS;
    QAction *actView_CELLS_APPLYTOALLWL;
    QAction *actView_HIDECONTROLS;
    QAction *actView_SYMBOLS;
    QAction *actView_RULER;
    QAction *actView_SCALE;
    QAction *actView_CELL_PATH;
    QAction *actView_CELL_PATH_ADDCURRENT;
    QAction *actView_CELL_PATH_CLEARPATHS;
    QAction *actView_ZOOM;
    QAction *actView_ZOOM_FULL;
    QAction *actView_ZOOM_50;
    QAction *actView_ZOOM_100;
    QAction *actView_ZOOM_200;
    QAction *actView_ZOOM_PLUS25;
    QAction *actView_ZOOM_MINUS25;
    QAction *actView_TREE_OVERVIEW;
    QAction *actView_CUSTOMIZE;
    QAction *actView_WLINFO;
    QAction *actView_CELLDATA;
    QAction *actView_CENTERCELL;
    QAction *actView_GAMMA;
    QAction *actView_COLOCATION;
    QAction *actView_COLOCATION_SELECTRADIUS;
    QAction *actView_COLOCATION_CURRENTCELL;
    QAction *actView_COLOCATION_ALLCELLS;
    QAction *actView_SPEED;
    QAction *actView_RESET_TREE_DISPLAY;

    ///tracking menu
    QAction *actTracking_START;
    QAction *actTracking_STOPREASON;
    QAction *actTracking_STOPREASON_APOPTOSIS;
    QAction *actTracking_STOPREASON_DIVISION;
    QAction *actTracking_STOPREASON_LOST;
    QAction *actTracking_STOPREASON_ENDOMITOSIS;
    QAction *actTracking_STOPREASON_TRIVISION;
    QAction *actTracking_STOPREASON_NECROSIS;
    QAction *actTracking_QUANTIFICATION;
    QAction *actTracking_QUANTIFICATION_BACKGROUNDTRACKING;
    QAction *actTracking_QUANTIFICATION_OVERVIEW;
    //QAction *actTracking_QUANTIFICATION_;
    QAction *actTracking_DELETECELL;
    QAction *actTracking_DELETESTOPREASON;
    QAction *actTracking_DELETECOLONY;
    QAction *actTracking_DELETE_TRACKPOINTS;
	QVector<QAction*> actTracking_INHERIT_WLS;
	QAction *actTracking_INHERIT_ADHERENCE;
	QAction *actTracking_INHERIT_DIFFERENTIATION;
	QAction *actTracking_INHERIT_ALL;

    ///export menu
    //QAction *actExport_MOVIE;
    QAction *actExport_MOVIE_OPTIONS;
    QAction *actExport_MOVIE_FORMAT_JPG;
    QAction *actExport_MOVIE_FORMAT_BMP;
    QAction *actExport_MOVIE_FORMAT_PNG;
    //QAction *actExport_TREE;
    QAction *actExport_TREE_CURRENT_VIEW;
    QAction *actExport_TREE_COMPLETE_TREE;
    QAction *actExport_TREE_COMPLETE_TREE_UNSTRETCHED;
    QAction *actExport_TREE_ALL_TREES_OF_POSITION;
    //QAction *actExport_DATA;
    QAction *actExport_DATA_DIVISION_INTERVALS;
    QAction *actExport_DATA_FIRST_DIVISION_TIME;
    QAction *actExport_DATA_APOPTOSIS_TIME;
    QAction *actExport_DATA_CELL_SPEED;
    QAction *actExport_DATA_ALL_SELECTED_CELLS;
    QAction *actExport_DATA_CELL_COUNT;
    QAction *actExport_DATA_FLUORESCENCE;
    QAction *actExport_CELLIMAGEPATCHES;

    ///tools menu

	QAction *actTools_AUTOTRACKING;
	QAction *actTools_MOVIEEXPLORER;
	QAction *actTools_CONVERTOLDTREES;
	QAction *actTools_STATTTS;
	QAction *actTools_AMT;

    QAction *actTools_MOVIEPLAYER;
    QAction *actTools_POSITIONHISTORY;
    QAction *actTools_MATLABINTEGRATION;
    QAction *actTools_CUSTOMIZETTT;
    QAction *actTools_STOPWATCH;
    QAction *actTools_MININGTOOLS;
    QAction *actTools_TREELEGEND;

    ///help menu
	QAction *actHelp_CHANGELOG;
    QAction *actHelp_SEARCH;
    QAction *actHelp_TUTORIAL;
    QAction *actHelp_TOTD;	//tip of the day
    QAction *actHelp_EDITORIAL;
    QAction *actHelp_COPYLEFT;
    QAction *actHelp_ABOUTQT;
	QAction *actHelp_TEST;
};


#endif