/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "treeviewtrackitem.h"

// Qt
#include <QPainter>
#include <QKeyEvent>

// Project
//#include "tttbackend/treeviewsettings.h"
#include "treeviewtreeitem.h"
#include "tttbackend/itrack.h"
#include "treeviewcellcircle.h"
#include "treeview.h"
#include "tttbackend/itrackpoint.h"
#include "tttdata/userinfo.h"
#include "tttbackend/tttmanager.h"
#include "tttbackend/tttpositionmanager.h"
#include "tttpositionlayout.h"
#include "tttbackend/tools.h"
#include "tttmovie.h"
#include "tttbackend/picturearray.h"


const QColor TreeViewTrackItem::DEFAULT_WL_COLORS[] = {Qt::green, Qt::red, Qt::blue, Qt::magenta, Qt::yellow, Qt::cyan, Qt::gray, Qt::darkGreen, Qt::darkBlue};



TreeViewTrackItem::TreeViewTrackItem( ITrack* _track, TreeViewTreeItem* _treeItem, int _x )
	: QGraphicsItem(_treeItem)
{
	// Init variables
	track = _track;
	treeItem = _treeItem;
	//displaySettings = TreeViewSettings::getInstance();
	x = _x;
	selected = false;
	m_hightlightStartTp = -1;
	m_hightlightStartTp = -1;
	m_treeWidthAtLastPaint = calcWidth();
	m_treeHeightAtLastPaint = calcHeight();

	// Init position
	int y = qMax(track->getFirstTimePoint() - 1, 0);
	y *= treeItem->getTreeView()->getTreeHeightFactor();
	setPos(x, y);
	setZValue(TreeView::Z_TRACK_LINE);

	// Create cell circle
	cellCircle = new TreeViewCellCircle(this);

	// Create track number
	int trackNum = _track->getTrackNumber();
	QString trackNumText = QString("%1").arg(trackNum);
	trackNumberItem = new QGraphicsTextItem(trackNum > 0 ? trackNumText : "", this);

	// Set cross cursor and tool tip
	setCursor(Qt::CrossCursor);
	setToolTip("Click to open movie window");

	updateDisplay();
}

QRectF TreeViewTrackItem::boundingRect() const
{
	//return QRectF(0, 0, calcWidth(), calcHeight()); <- bad, because function is called e.g.
	// from scene->removeItem() which in turn can be called after track has been deleted --> maybe use shared pointers for tracks?
	return QRectF(0, 0, m_treeWidthAtLastPaint, m_treeHeightAtLastPaint);
}

void TreeViewTrackItem::paint( QPainter* _painter, const QStyleOptionGraphicsItem* _option, QWidget* _widget /*= 0*/ )
{
	// Make sure first timepoint is valid
	if(track->getFirstTimePoint() == -1)
		return;

	// Remember tree size
	m_treeHeightAtLastPaint = calcHeight();
	m_treeWidthAtLastPaint = calcWidth();

	// ToDo: Color..
	_painter->setPen(Qt::NoPen);
	_painter->setBrush(QBrush(Qt::black, Qt::SolidPattern));

	/* 
	Hard coded coloring used for export for open house images (04/2014)
	
	int trackNum = track->getTrackNumber();
	if(trackNum == 1 || trackNum == 2 || trackNum == 4 || trackNum == 9 || trackNum == 18 || trackNum == 36 || trackNum == 73 || trackNum == 146 || trackNum == 292 || trackNum == 584 || trackNum == 1168)
		_painter->setBrush(QBrush(qRgb(246, 74, 3), Qt::SolidPattern));
	else if(trackNum == 3 || trackNum == 6 || trackNum == 13 || trackNum == 27 || trackNum == 54 || trackNum == 109 || trackNum == 218 || trackNum == 437 || trackNum == 875 || trackNum == 1751 || trackNum == 1168) {
		_painter->setBrush(QBrush(qRgb(81, 207, 172), Qt::SolidPattern));
	}
	else {
		_painter->setBrush(QBrush(qRgb(172, 172, 172), Qt::SolidPattern));
	}*/

	// Get line width
	int lineWidth = UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TREEVIEW_TRACKLINEWIDTH).toInt();

	// Draw line
	int pixelsPerTimepoint = treeItem->getTreeView()->getTreeHeightFactor();
	if(UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TREEVIEW_TRACKLINESCONTINUOUS).toBool()) {
		int height = (track->getLastTimePoint() - track->getFirstTimePoint() + 1) * treeItem->getTreeView()->getTreeHeightFactor();

		// Draw one line from first to last timepoint
		if(height > 0) {
			_painter->drawRect(0, 0, lineWidth, height);
		}
	}
	else {
		// Current y position in local coordinates
		int curPosY = 0;

		// Draw point (i.e. a rectangle) for every trackpoint
		for(int tp = track->getFirstTimePoint(); tp <= track->getLastTimePoint(); ++tp) {
			// Get trackpoint
			ITrackPoint* curTrackPoint = track->getTrackPointByTimePoint(tp);

			// Draw
			if(curTrackPoint) {
				// Set color according to confidence level
				float conf = curTrackPoint->getConfidenceLevel();
				QColor col = TreeView::mapConfidenceOnColor(conf);
				_painter->setBrush(QBrush(col, Qt::SolidPattern));
				_painter->drawRect(0, curPosY, lineWidth, pixelsPerTimepoint);
			}

			curPosY += treeItem->getTreeView()->getTreeHeightFactor();
		}
	}

	// Draw WLS
	int curPosY = 0;
	int adjDist = UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TREEVIEW_TRACKLINEDISTANCE).toInt();
	for(int tp = track->getFirstTimePoint(); tp <= track->getLastTimePoint(); ++tp) {
		// Get trackpoint
		ITrackPoint* curTrackPoint = track->getTrackPointByTimePoint(tp);
		if(curTrackPoint) {
			for(int wl = 1; wl <= MAX_DISPLAYED_WAVELENGTHS; ++wl) {
				if(curTrackPoint->checkForWavelength(wl)) {
					QColor color = DEFAULT_WL_COLORS[wl-1];
					_painter->setBrush(QBrush(color, Qt::SolidPattern));
					_painter->drawRect(wl * lineWidth + wl * adjDist, curPosY, lineWidth, pixelsPerTimepoint);
				}
			}
		}

		curPosY += treeItem->getTreeView()->getTreeHeightFactor();
	}

	// Draw highlighting (vertical line right of track line with same width)
	if(m_hightlightStartTp > 0 && m_hightlightStopTp > 0 && m_hightlightStopTp >= m_hightlightStartTp) {
		_painter->setBrush(QBrush(Qt::magenta, Qt::SolidPattern));

		// Calculate relative position of highlight area
		int yStart = (m_hightlightStartTp - 1) * treeItem->getTreeView()->getTreeHeightFactor() - y();
		int yStop = m_hightlightStopTp * treeItem->getTreeView()->getTreeHeightFactor() - y();
		if(yStop > yStart)
			_painter->drawRect(lineWidth+1, yStart, lineWidth, yStop - yStart);
	}
}

int TreeViewTrackItem::calcWidth() const
{
	// Get distance between lines
	int adjDist = UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TREEVIEW_TRACKLINEDISTANCE).toInt();

	// Get line width
	int lineWidth = UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TREEVIEW_TRACKLINEWIDTH).toInt();

	// Calc max number of lines we will have (wl lines + main line)
	int maxNumLines = MAX_DISPLAYED_WAVELENGTHS + 1;

	// Calc width
	return maxNumLines * lineWidth + (maxNumLines - 1) * adjDist;
}

int TreeViewTrackItem::calcHeight() const
{
	// Every timepoint represents one unit in scene coordinates

	// Get first and last timepoint
	int ftp = track->getFirstTimePoint();
	int ltp = track->getLastTimePoint();

	// Return 0 if they are invalid
	if(ltp == -1 || ftp == -1 || ltp < ftp)
		return 0;

	return (ltp - ftp + 1) * treeItem->getTreeView()->getTreeHeightFactor();
}

void TreeViewTrackItem::notifyMouseReleaseEvent( Qt::MouseButton _button )
{
	// Left button means selection state change
	if(_button == Qt::LeftButton) {
		// Notify tree
		ITree *tree = treeItem->getTree();
		treeItem->getTreeView()->notifyCellSelection(treeItem, this);
	}
}

void TreeViewTrackItem::setSelectionState( bool _selected )
{
	// Nothing to do
	if(selected == _selected)
		return;

	// Change state and update display
	selected = _selected;
	cellCircle->updateDisplay();
}

void TreeViewTrackItem::updateDisplay()
{
	// Update cell circle
	cellCircle->updateDisplay();

	// Update track number
	trackNumberItem->setFont(QFont("Arial", UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TREEVIEW_TRACKNUMFONTSIZE).toInt()));
	trackNumberItem->setDefaultTextColor(UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TREEVIEW_TRACKNUMCOLOR).value<QColor>());

	// Position track number (lateral to cell circle)
	QRectF fontRect = trackNumberItem->boundingRect();
	QRectF cellRect = cellCircle->boundingRect();

	// Determine if track number is left or right of cell
	int num = track->getTrackNumber();
	if((num < 1 || num % 2 != 0) && num != 1) {
		// Right (add 10 to x-value to avoid a "number-sticking-to-cell" effect)
		trackNumberItem->setPos(cellRect.width() / 2 + 10, -fontRect.height() / 2);
	}
	else {
		// Left
		trackNumberItem->setPos(-cellRect.width() / 2 - fontRect.width(), -fontRect.height() / 2);
	}

	// Repaint track
	update();

	// Update symbols
	updateSymbols();
}

void TreeViewTrackItem::updateSymbols()
{
	// Remove old symbols
	for(int i = 0; i < symbols.size(); ++i) {
		QGraphicsTextItem* curItem = symbols[i];
		scene()->removeItem(curItem);
		delete curItem;
	}

	// Cell death item
	if(track->getStopReason() == TS_APOPTOSIS) {
		// Get settings
		QString text = UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TREEVIEW_SYMBOLCELLDEATH).toString();
		QColor col = UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TREEVIEW_SYMBOLCELLDEATHCOLOR).value<QColor>();
		int fontSize = UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TREEVIEW_SYMBOLSFONTSIZE).toInt();

		// Make sure symbol is not empty
		if(!text.isEmpty()) {
			// Create item
			QGraphicsTextItem* newItem = new QGraphicsTextItem(text, this);
			newItem->setFont(QFont("Arial", fontSize));
			newItem->setDefaultTextColor(col);

			// Set position
			newItem->setPos(0, calcHeight());
		}
	}
	else if(track->getStopReason() == TS_LOST) {
		// Get settings
		QString text = UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TREEVIEW_SYMBOLLOST).toString();
		QColor col = UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TREEVIEW_SYMBOLLOSTCOLOR).value<QColor>();
		int fontSize = UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TREEVIEW_SYMBOLSFONTSIZE).toInt();

		// Make sure symbol is not empty
		if(!text.isEmpty()) {
			// Create item
			QGraphicsTextItem* newItem = new QGraphicsTextItem(text, this);
			newItem->setFont(QFont("Arial", fontSize));
			newItem->setDefaultTextColor(col);

			// Set position
			newItem->setPos(0, calcHeight());
		}
	}
}

void TreeViewTrackItem::mousePressEvent( QGraphicsSceneMouseEvent *_event )
{
	// User clicked on track -> open movie window and show cell of this track at corresponding timepoint

	// Get timepoint
	int tp = _event->scenePos().y() / treeItem->getTreeView()->getTreeHeightFactor();
	if(tp < 1)
		return;

	// Get trackpoint
	ITrackPoint* trackPoint = track->getTrackPointByTimePoint(tp);
	if(!trackPoint) {
		// Try next 10 timepoints (track could be tracked only e.g. every 10th timepoint)
		for(int tmp = tp - 5; tmp <= tp + 5; ++tmp) {
			trackPoint = track->getTrackPointByTimePoint(tmp);
			if(trackPoint)
				break;
		}
		if(!trackPoint)
			return;
	}

	// Get position
	int posNum = trackPoint->getPositionNumber();
	if(posNum < 1)
		return;

	// Make sure position is initialized
	QString posNumString = Tools::convertIntPositionToString(posNum);
	TTTManager::getInst().frmPositionLayout->setPosition(posNumString);

	// Get position manager
	TTTPositionManager *tttpm = TTTManager::getInst().getPositionManager(posNumString);
	if(!tttpm)
		return;

	// Make sure image is loaded at tp
	int wl = std::max(0, tttpm->getWavelength());
	int zIndex = std::max(0, tttpm->getZIndex());
	if(tttpm->getPictures())
		tttpm->getPictures()->loadOnePicture(PictureIndex(tp, zIndex, wl));

	// Show movie window and center this track
	if(tttpm->frmMovie) {
		tttpm->frmMovie->show();
		tttpm->frmMovie->raise();

		tttpm->frmMovie->getMultiplePictureViewer()->centerTrack(track, tp);
	}

	// Change timepoint
	TTTManager::getInst().setTimepoint(tp);
}

void TreeViewTrackItem::setHighlighting( int startTp, int stopTp )
{
	// Apply
	m_hightlightStartTp = startTp;
	m_hightlightStopTp = stopTp;

	// Redraw track
	update();
}

void TreeViewTrackItem::setTrackNumberVisible(bool visible)
{
	if(trackNumberItem)
		trackNumberItem->setVisible(visible);
}