/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "treedisplay.h"

#include "tttdata/userinfo.h"
#include "tttbackend/tttmanager.h"
#include "tttloaddwellingpictures.h"

//Added by qt3to4:
#include <QContextMenuEvent>
#include <QPixmap>
#include <Q3PopupMenu>
#include <QMouseEvent>
//#include <iostream>


///the maximum horizontal size for the tree
//changed to 10000 for Hanna on 2007/11/24 for her tree contained more than 8000 cells
//const int TREEWINDOWWIDTHMAXIMUM = 12000*3;
const int TREEWINDOWWIDTHMAXIMUM = 2*12000;
//const int TreeWindowWidthMaximum = 5000;


TreeDisplay::TreeDisplay (QWidget* _container, QWidget *_parent, const char *_name)
 : Q3CanvasView (_parent, _name), zoomFactor (100)
{
	Container = _container;
	MiddleButtonPressed = false;
	timePointLine = 0;
	canvas.resize (10, 10);
	//@ todo try true/false!
	canvas.setDoubleBuffering (false);
	this->setCanvas (&canvas);
	drawContinuousCellLines = false;
	
	selectedTracks.setAutoDelete (false);
	selectedTrackCount = 0;
	
	tree = 0;
	treeTTTPM = 0;
	
	connect (this, SIGNAL (contentsMoving (int, int)), this, SLOT (moveDisplay (int, int)));
	
}

TreeDisplay::~TreeDisplay()
{
	//Do neither delete tree nor Container
}

void TreeDisplay::init (Tree *_tree, TTTPositionManager *_tttpm)
{
	if (! _tree)
		return;
	
	tree = _tree;
	
	//if (_tttpm->isInitialized()) // lol?
	//	_tttpm->initialize();
	
	treeTTTPM = _tttpm;
	
	//OTODO:
	//connect (tree, SIGNAL (TreeSizeChanged()), this, SLOT (updateSize()));
	QHash<int, TTTPositionManager*>& allPositions = TTTManager::getInst().getAllPositionManagers();
	for (auto iter = allPositions.begin(); iter != allPositions.end(); ++iter) 
		connect ( (*iter), SIGNAL (TWHFChanged (int)), this, SLOT (calcFactors (int)));
	
	updateSize();
}

//EVENT:
void TreeDisplay::contentsMouseReleaseEvent (QMouseEvent *ev)
{
	
	//calling this method is possible only if this view's tree is the currently active tree
	if (tree != TTTManager::getInst().getTree()) {
		ev->accept();
		return;
	}
	
	//@todo:
	//make possible that the user can select a region via mouse drawn rectangle
	//-> zoom exactly into it
	//=> the logical image must be of greater size to be able to zoom up to 1000%
	//mouse wheel or whatever zooms back
	
	if (ev->button() == Qt::LeftButton) {
		
		//hide the currently displayed comment, if there is one
		emit commentDeSelected();
		
		//if there is a tracking in process, no new cell can be selected
		if (! TTTManager::getInst().isTracking())  {
                        if (ev->modifiers() & Qt::ControlModifier) {
				//enable multiple selection of cells (the first selected stays the main selected)
				
				if (TTTManager::getInst().getCurrentTrack()) {
					//additional cell selected
					Track *track = getTrack (ev->pos());
					bool select = (isTrackSelected (track) == 0);
					selectTrack (track, select, true);
				}
				else {
					//first cell -> normal selection
					Track *track = selectTrack (ev->pos(), true, false);
					
					TTTManager::getInst().setCurrentTrack (track);
					emit trackSelected (TTTManager::getInst().getCurrentTrack());
				}
			}
			else {
				if (TTTManager::getInst().getCurrentTrack())
					deselectTrack();
				
				Track *track = selectTrack (ev->pos(), true, false);
				
				TTTManager::getInst().setCurrentTrack (track);
				emit trackSelected (TTTManager::getInst().getCurrentTrack());
			}
		}
		
		//check whether the user clicked on an asterisk and wants to see the timepoint comment
		checkCommentSelection (ev->pos());
		
	}
	else if (ev->button() == Qt::MidButton) {
		MiddleButtonPressed = false;
	}
	else if (ev->button() == Qt::RightButton) {
		
		//note: no multiple cell selection is possible with a right click!
		//right click + control button should open the context menu
		//thus, if control is held, no tracking should be started
                if ((ev->modifiers() == (Qt::ControlModifier) ))
			return;
		
		//note: no multiple cell selection is possible with a right click!
		if (TTTManager::getInst().getCurrentTrack())
			deselectTrack();
		
		Track *track = selectTrack (ev->pos());
		
		if (track) {
			
			triggerCellTracking();
		}
		
		
		//check whether the user clicked on an endomitosis symbol -> he wants to delete it
		track = checkEndomitosisSelection (ev->pos());
		if (track)
			draw (track, true, false, false, false);	//this track  should be redrawn
		else {
			//no track found -> thus...
			//...check whether the user clicked on an endomitosis symbol -> he wants to delete it
			track = checkEndomitosisSelection (ev->pos());
			if (track)
				draw (track, true, false, false, false);	//this track  should be redrawn
		}
	}
	ev->accept();
}

void TreeDisplay::contentsMousePressEvent (QMouseEvent *ev)
{
	MiddleButtonPressed = (ev->button() == Qt::MidButton);
	
	if (MiddleButtonPressed)
		oldMousePos = ev->pos();
	
	ev->accept();
}

void TreeDisplay::contentsMouseMoveEvent (QMouseEvent *)
{
	return;
	//shifting is done by QCanvasView itself
	
	
/*	if (MiddleButtonPressed) {
		//note: the shifting is PARALLEL to the mouse!
		//QPoint shift = tree->shiftDisplay (oldMousePos - ev->pos());		
		QPoint shift = ev->pos() - oldMousePos;
		emit treeShifted (shift, TTTManager::getInst().getBasePositionManager()->getTWHF());
		shift /=  (float)zoomFactor / 100.0f;
		oldMousePos = ev->pos();
		QWMatrix wm = this->worldMatrix();
		wm.translate (shift.x(), shift.y());
		if (wm.dx() > 0)
			wm.translate (-wm.dx(), 0);
		if (wm.dx() < this->geometry().width() - canvas.width() * (float)zoomFactor / 100.0f )
			wm.translate (this->geometry().width() - canvas.width() * (float)zoomFactor / 100.0f  - wm.dx(), 0);
		if (wm.dy() > 0)
			wm.translate (0, -wm.dy());
		if (wm.dy() < this->geometry().height() - canvas.height() * (float)zoomFactor / 100.0f)
			wm.translate (0, this->geometry().height() - canvas.height() * (float)zoomFactor / 100.0f - wm.dy());
		this->setWorldMatrix (wm);
		
		ev->accept();
	}*/
}

//start tracking for the selected cell
void TreeDisplay::contentsMouseDoubleClickEvent (QMouseEvent *ev)
{
	//*************************
	//handled with right click!
	//double clicks just confuse with normal left clicks
	//-> return
	ev->ignore();
	return;
	//*************************
}

void TreeDisplay::contextMenuEvent (QContextMenuEvent *_ev)
{
	_ev->ignore();
	
	//calling this method is possible only if this view's tree is the currently active tree
	if (tree != TTTManager::getInst().getTree())
		return;
	
	//display a context menu for the selected cell(s)
	
	if (! COMPFLAG_CELL_CONTEXT_MENU)
		return;
	
	//the normal right click is used for starting the tracking of a cell
	//so the context menu is only opened if additionally the control button is pressed
        if ((_ev->modifiers() == (Qt::ControlModifier & Qt::RightButton) ))
		return;
	
	//if currently no cell is selected, no context menu is displayed
	if (! TTTManager::getInst().getCurrentTrack())
		return;
	
	//note: 
	//the current cell can be reached via  TTTManager::getInst().getCurrentTrack()
	//the other selected cells are in the list  selectedTracks
	//this also is valid for the slots called from this contetxt menu
	
	Q3PopupMenu *popup = new Q3PopupMenu (this);
	popup->insertItem ("Track current cell", this, SLOT (triggerCellTracking()));
	
	if (COMPFLAG_LOAD_PICTURES_POSITONS)
		popup->insertItem ("Load dwelling pictures", this, SLOT (selectCellDwellingPictures()));
	
	popup->exec (QCursor::pos());
	
	if (popup)
		delete popup;
	
	_ev->accept();
}

//SLOT:
void TreeDisplay::selectCellDwellingPictures()
{
	//calling this method is possible only if this view's tree is the currently active tree
	if (tree != TTTManager::getInst().getTree())
		return;
	
	if (! COMPFLAG_LOAD_PICTURES_POSITONS)
		return;
	
	Track *track = TTTManager::getInst().getCurrentTrack();
	
	if (! track)
		return;
	
	TTTLoadDwellingPictures *tttlpp = new TTTLoadDwellingPictures();
	
	//display currently selected cells
	QString cells = QString ("[%1]").arg (track->getNumber());
	
	for (int i = 0; i < selectedTrackCount; i++) {
		if (selectedTracks.find (i)) {
			cells += QString (", %1").arg (selectedTracks.find (i)->getNumber());
		}
	}
	
	tttlpp->writeSelectedCells (cells);
	
	//find out last tracked position of the current cell
	TrackPoint trpo = track->getTrackPoint (track->getLastTrace());
	if (trpo.TimePoint != -1) {
		QString pos = "";
		
		if (trpo.Position > "") {
			pos = trpo.Position;
		}
		else {
			//old system
			
                        QPointF lastPoint = trpo.point();
			TTTPositionManager *tttpm = TTTManager::getInst().getPositionAt (lastPoint.x(), lastPoint.y());
			if (tttpm)
				pos = tttpm->positionInformation.getIndex();
		}
		
		if (pos > "")
			tttlpp->markPosition (pos);
	}
	
	
	tttlpp->exec();
	delete tttlpp;
}

//SLOT:
void TreeDisplay::triggerCellTracking()
{
	//calling this method is possible only if this view's tree is the currently active tree
	if (tree != TTTManager::getInst().getTree())
		return;
	
	//if there is a tracking in process, no other cell can be tracked
	if (TTTManager::getInst().isTracking()) 
		return;
	
	Track *track = TTTManager::getInst().getCurrentTrack();
	
	if (track)
		emit trackDoubleclicked (track);
}

void TreeDisplay::fit()
{
	setGeometry (Container->geometry());
}

//SLOT:
void TreeDisplay::shiftDisplay (QPoint _shift)
{
	//zoom is not yet included in _shift => do it...
	_shift /=  (float)zoomFactor / 100.0f;
	
	QMatrix wm = this->worldMatrix();
	QMatrix sec = wm;
	
	//set world matrix
	wm.translate (_shift.x(), _shift.y());
	if (wm.dx() > 0)
		wm.translate (-wm.dx(), 0);
	if (wm.dx() < this->geometry().width() - canvas.width() * (float)zoomFactor / 100.0f )
		wm.translate (this->geometry().width() - canvas.width() * (float)zoomFactor / 100.0f  - wm.dx(), 0);
	if (wm.dy() > 0)
		wm.translate (0, -wm.dy());
	if (wm.dy() < this->geometry().height() - canvas.height() * (float)zoomFactor / 100.0f)
		wm.translate (0, this->geometry().height() - canvas.height() * (float)zoomFactor / 100.0f - wm.dy());
	this->setWorldMatrix (wm);		
	
	
	_shift = QPoint ((int)(sec.dx() - wm.dx()), (int)(sec.dy() - wm.dy()));
	//0 as second parameter causes the tree map NOT to shift (otherwise, if the tree map is shifted directly, the shift would be doubled)!
	emit treeShifted (_shift, 0);
}

void TreeDisplay::setDisplayCenter (QPoint _newCenter)
{
	QPoint currentCenter = getDisplayRegion().center();
	
	QPoint shift = _newCenter - currentCenter;
	
	//shift display to desired center
	shiftDisplay (shift);	
}

//SLOT:
void TreeDisplay::updateSize()
{
	updateSize (TREEWINDOWWIDTHMAXIMUM, TREEWINDOWWIDTHMINIMUM);
}

void TreeDisplay::updateSize (int _max_width, int _min_width, bool _draw)
{
	if (! tree)
		return;
	
	//adapt the tree width to the maximal possible number of cells in the highest generation
	//this maintains enough horizontal space for each cell
	
	int mpt = (tree->getMaxPossibleTracks() + 1) / 2;
	//note: mpt counts ALL tracks, but we only need the cells in the highest generation => divide by 2
	int CellWidth = CellDiameterX + 5;
	int width = mpt * CellWidth;
	while (width > _max_width) {
		//if the calculated width would be too big, the horizontal cell space is reduced stepwise
		//not very intelligent :-)
		CellWidth--;
		width = mpt * CellWidth;
	}
	
	width = QMAX (width, _min_width);
	
	int height = treeTTTPM->getExperimentSeconds() / treeTTTPM->getTWHF();
	
	if ((width * height) == 0)
		return;
	
	//update size only if different from current size
	if ((width != TreeWindowWidthComplete) || (height != TreeWindowHeight)) {
		//very slow by QCanvas itself (controlable by setting a higher TreeWindowWidthMinimum)
		canvas.resize (width, height);
		
		//try different values if too slow
		canvas.retune (8, 100);
		TreeWindowWidthComplete = canvas.width();
		//TreeWindowWidth = (ShowSpeed ? TreeWindowWidthComplete - SpeedWindowWidth : TreeWindowWidthComplete);
		TreeWindowWidth = TreeWindowWidthComplete - SPEEDWINDOWWIDTH;
		TreeWindowHeight = canvas.height();
		canvas.update();
		calcFactors (treeTTTPM->getTWHF());
		
		if (_draw)
			draw();
		
		//redisplay timepoint line to stretch over the complete tree
		if (timePointLine) {
			delete timePointLine;
			timePointLine = 0;
			displayTimePoint();
		}
	}
}

//SLOT:
void TreeDisplay::calcFactors (int ) //_twhf)
{
	if (! tree)
		return;
	
	//the diameters are specified in real coordinates (to stay constant) and then transformed to logical
	CellDiameterX = 10; //pixel
	CellDiameterY = 10; //pixel
	QPoint tmp = mapToLogical (QPoint (CellDiameterX, CellDiameterY), true);
	CellDiameterX = tmp.x();
	CellDiameterY = tmp.y();
	
	//same for LineWidthFactor
	int LineWidth = 1; 	//pixel
	tmp = mapToLogical (QPoint (LineWidth, LineWidth), true);
	if (tmp.y() <= 0)
		tmp.setY (1);
	LineWidthFactor = treeTTTPM->getExperimentSeconds() / tmp.y();
	if (LineWidthFactor == 0)
		LineWidthFactor = 100000;
	
	//same for FontSize
	FontSize = treeTTTPM->getExperimentSeconds() / (LineWidthFactor / 10);
}

//SLOT:
void TreeDisplay::moveDisplay (int _newX, int _newY)
{
	int curX = this->contentsX();
	int curY = this->contentsY();
	QPoint shift (_newX - curX, (_newY - curY)); // * tree->getTWHF());
	emit treeShifted (shift, treeTTTPM->getTWHF());
}

void TreeDisplay::exportTree (QString _filename, bool _completeTree, bool _unstretched, bool _printIndex, const QString &_index, bool _redraw, const QString &_format)
{
	if (_redraw) {
		updateSize (EXPORTTREEWIDTH, TREEWINDOWWIDTHMINIMUM,  false);
		draw();
	}
	
	if (_completeTree) {
		if (_printIndex) {
			MyQCanvasText *text = new MyQCanvasText (_index, &canvas);
			text->setFont (QFont ("Arial", 10));
			text->setColor (Qt::black);
			
			text->setX (10);
			text->setY (10);
			text->setUsage (TextUsageTreeName);
			text->show();
		}
		
		bool must_reset = false;
		if ((! _unstretched) && TreeWindowWidthComplete > EXPORTTREEWIDTH) {
			must_reset = true;
			updateSize (EXPORTTREEWIDTH, TREEWINDOWWIDTHMINIMUM, true);
		}
		
		QPixmap pix (TreeWindowWidthComplete, TreeWindowHeight);
		QPainter p (&pix);
		//note: I (BS) do not know why const_cast is necessary, yet without, a compiler error occurs
		const_cast<Q3Canvas*>(&canvas)->drawArea (canvas.rect(), &p);
		p.end();
		pix.save (_filename, _format);
		
		if (must_reset)
			updateSize (TREEWINDOWWIDTHMAXIMUM, TREEWINDOWWIDTHMINIMUM, true);
	}
	else {
	
		if (_printIndex) {
			MyQCanvasText *text = new MyQCanvasText (_index, &canvas);
			if (zoomFactor <= 900)
				text->setFont (QFont ("Arial", 11 - zoomFactor / 100));
			else
				text->setFont (QFont ("Arial", 2));
			
			text->setColor (Qt::black);
			
			
			QPoint pos (contentsX()  + 10, contentsY()  + 10);
			pos = this->inverseWorldMatrix().map (pos);
			
			text->setX (pos.x());
			text->setY (pos.y());
			
			text->setUsage (TextUsageTreeName);
			text->show();
			this->repaintContents();
		}
		
		QPixmap pix (this->width(), this->height());
		pix = QPixmap::grabWindow (this->winId());
		pix.save (_filename, "BMP");
		
	}
	
	if (_printIndex) {
		clear (TextUsageTreeName);
		
		if (! _completeTree)
			this->repaintContents();
	}
}

void TreeDisplay::setCurrent (Track *_track)
{
	//calling this method is possible only if this view's tree is the currently active tree
	if (tree != TTTManager::getInst().getTree())
		return;
	
	if (TTTManager::getInst().getCurrentTrack())
		deselectTrack();
	
	TTTManager::getInst().setCurrentTrack (_track);
	
	if (TTTManager::getInst().getCurrentTrack())
		selectTrack (TTTManager::getInst().getCurrentTrack(), true, false);
}

void TreeDisplay::setDisplayRegion (QRect _rect)
{
	if (_rect.isNull())
		return;
	
	//create a world matrix so that exactly the provided _rect is visible
	//i.e. (_rect) * matrix == this->geometry()
	QMatrix wm; // = this->worldMatrix();
	int width = this->geometry().width();
	int height = this->geometry().height();
	wm.scale (width / _rect.width(), height / _rect.height());
	wm.translate (_rect.left(), _rect.top());
	this->setWorldMatrix (wm);
}

Track *TreeDisplay::getTrack (QPoint _pos) const
{
	if (! tree)
		return 0;
	
	return getPosTrack (mapToLogical (_pos));
}
	
int TreeDisplay::isTrackSelected (Track *_track) const
{
	//calling this method is possible only if this view's tree is the currently active tree
	if (tree != TTTManager::getInst().getTree())
		return 0;
	
	if (! _track)
		return 0;
	
	if (_track == TTTManager::getInst().getCurrentTrack())
		return 1;
	else {
		for (int i = 0; i < selectedTrackCount; i++) {
			if (selectedTracks.find (i)) {
				if (selectedTracks.find (i)->getNumber() == _track->getNumber()) {
					return 2;
				}
			}
		}
	}
	
	return 0;
}
	
Q3IntDict<Track> TreeDisplay::getSelectedTracks (bool _includeMain) const
{
	//calling this method is possible only if this view's tree is the currently active tree
	if (tree != TTTManager::getInst().getTree())
		return Q3IntDict<Track> (0);
	
	if ((selectedTrackCount > 0) || ((TTTManager::getInst().getCurrentTrack()) && _includeMain)) {
		Q3IntDict<Track> tmp (selectedTrackCount);
		
		if (! TTTManager::getInst().getCurrentTrack())
			_includeMain = false;
		
		int j = 0;
		
		if (_includeMain) {
			tmp.resize (selectedTrackCount + 1);
			tmp.insert (0, TTTManager::getInst().getCurrentTrack());
			j = 1;
		}
		
		
		for (uint i = 0; i < selectedTracks.size(); i++) {
			if (selectedTracks.find (i)) {
				tmp.insert (j, selectedTracks.find (i));
				j++;
			}
		}
		
		return tmp;
	}
	else {
		return Q3IntDict<Track> (0);
	}
}

int TreeDisplay::countSelectedTracks() const
{
	return selectedTrackCount;
}

bool TreeDisplay::checkCommentSelection (QPoint _pos) //const
{
	//transform coordinates
	QPoint tmpPos = mapToLogical (_pos, false);
	
	//get canvas items under position
	//QCanvasItemList l = canvas.collisions (_pos);
	int size = 2;
	Q3CanvasItemList l = canvas.collisions (QRect (_pos.x() - size, _pos.y() - size, size * 2, size * 2));
	for (Q3CanvasItemList::Iterator it = l.begin(); it != l.end(); ++it) {
		if ((*it)->rtti() == MyQCanvasText::RTTI) {
			TreeElementStyle tes = UserInfo::getInst().getStyleSheet().getElement (RTTI_COMMENTASTERISK);
			if (((MyQCanvasText*)(*it))->text() == tes.getSymbol()) {
				//asterisk found under mouse cursor - this is the desired object
				Track *track = ((MyQCanvasText*)(*it))->getTrack();
				if (track) {
					//int timepoint = tree->getFiles()->calcTimePoint (tree->getFirstTimePoint(), tmpPos.y() * tree->getTWHF(), true); 
					int timepoint = ((MyQCanvasText*)(*it))->getParam();
					if (timepoint > -1) {
						QString tmpComment = track->getComment (timepoint);
						emit commentSelected (tmpComment, _pos, timepoint);
						return true;
					}
				}
				return false;
			}
		}
	}
	
	//nothing found
	return false;	
}

Track* TreeDisplay::checkEndomitosisSelection (QPoint _pos)
{
	//transform coordinates
	QPoint tmpPos = mapToLogical (_pos, false);
	
	//get canvas items under position
	int size = 3;
	Q3CanvasItemList l = canvas.collisions (QRect (_pos.x() - size, _pos.y() - size, size * 2, size * 2));
	for (Q3CanvasItemList::Iterator it = l.begin(); it != l.end(); ++it) {
		if ((*it)->rtti() == MyQCanvasText::RTTI) {
			TreeElementStyle tes = UserInfo::getInst().getStyleSheet().getElement (RTTI_ENDOMITOSIS);
			if (((MyQCanvasText*)(*it))->text() == tes.getSymbol()) {
				//endomitosis symbol found under mouse cursor - this is the desired object
				Track *track = ((MyQCanvasText*)(*it))->getTrack();
				if (track) {
					int timepoint = ((MyQCanvasText*)(*it))->getParam();
					if (timepoint > -1) {
						//delete endomitosis symbol
						TrackPoint *tp = track->getRefTrackPoint (timepoint); 
						if (tp)
							tp->EndoMitosis = 0;
						return track;
					}
				}
				return 0;	//no endomitosis here
			}
		}
	}
	
	return 0;	//nothing found
}

void TreeDisplay::setShowColocation (bool _show, bool _allCells)
{
	if (_allCells)
		ShowColocationAll = _show;
	else
		ShowColocationSingle = _show;
	
	TreeElementStyle tes = UserInfo::getInst().getStyleSheet().getElement (RTTI_COLOCATION);
	tes.setVisible (_show);
	UserInfo::getInst().getRefStyleSheet().setElement (tes);
	
	if (! _show) {
		clear (LineUsageColocation);			//clear old colocation lines
		canvas.update();
	}
	else
		draw (0, true, false, true);
}

void TreeDisplay::setShowSpeed (bool _show)
{
	if (ShowSpeed == _show)
		return;
	
	ShowSpeed = _show;
	
	TreeElementStyle tes = UserInfo::getInst().getStyleSheet().getElement (RTTI_SPEEDBAR);
	tes.setVisible (_show);
	UserInfo::getInst().getRefStyleSheet().setElement (tes);
	tes = UserInfo::getInst().getStyleSheet().getElement (RTTI_CELLSPEED);
	tes.setVisible (_show);
	UserInfo::getInst().getRefStyleSheet().setElement (tes);
	
	if (! _show) {
		clear (LineUsageSpeedBar);
		clear (LineUsageSpeedCell);
		canvas.update();
	}
	else {
		draw (0, true, true, false);
	}
}

void TreeDisplay::displayTimePoint() // (int _timePoint)
{
	//calling this method is possible only if this view's tree is the currently active tree
	if (tree != TTTManager::getInst().getTree())
		return;
	
	if (! tree)
		return;
	
	if (! treeTTTPM)
		return;
	
	if (! treeTTTPM->getFiles())
		return;
	
	TreeElementStyle tes = UserInfo::getInst().getStyleSheet().getElement (RTTI_TIMEPOINTLINE);
	
	if (! timePointLine) {
		timePointLine = new MyQCanvasLine (&canvas, false, true);
		timePointLine->setZ (Z_TimepointLine);
		timePointLine->setUsage (LineUsageTimepoint);
		timePointLine->setPoints (0, 0, TreeWindowWidthComplete, 0);
	}
	timePointLine->setPen (QPen (tes.getColor (0), tes.getSize()));
	
	if (TTTManager::getInst().getTimepoint() >= treeTTTPM->getFirstTimePoint() && TTTManager::getInst().getTimepoint() <= treeTTTPM->getLastTimePoint()) {
		
		//draw a line for the current timepoint
		int dist = treeTTTPM->getFiles()->calcSeconds (treeTTTPM->getFirstTimePoint(), TTTManager::getInst().getTimepoint());
		dist /= treeTTTPM->getTWHF();
		if (dist >= 0) {
			if (timePointLine) {
				int prev = (int)timePointLine->y();
				timePointLine->moveBy (0, dist - prev); 
					
				if (! tes.isVisible())
					timePointLine->hide();
				else
					timePointLine->show();
				
				canvas.update();
			}
		}
	}
}

void TreeDisplay::setZoomFactor (int _zoomFactor)
{
	QMatrix wm = this->worldMatrix();
	wm.scale ((double)_zoomFactor / (double)zoomFactor, (double)_zoomFactor / (double)zoomFactor);
	zoomFactor = _zoomFactor;
	this->setWorldMatrix (wm);
}

void TreeDisplay::draw (Track *_track, bool _drawTracks, bool _speedOnly, bool _colocOnly, bool _drawCascade)
{
	if (! treeTTTPM)
		return;
	
	if (! tree)
		return;
	
	if (tree->getTrackCount() == 0) 
		return;		//there are no tracks to be painted
	
	if (_drawTracks) {	// odd: there is no memory qimage to be updated
		
		Track *track = _track;
		if (! track)
			track = tree->getBaseTrack();
		
		if (_speedOnly) {
			if (track == TTTManager::getInst().getCurrentTrack())
				clear (LineUsageSpeedBar);			//delete only old speed lines
			else
				clear (LineUsageSpeedCell);
			if (UserInfo::getInst().getStyleSheet().getElement (RTTI_SPEEDBAR).isVisible() || UserInfo::getInst().getStyleSheet().getElement (RTTI_CELLSPEED).isVisible()) {
				if (_drawCascade){
					drawTrackCascade (track, _speedOnly, _colocOnly);
				}
				else
					drawTrack (track, _speedOnly, _colocOnly);
			}
		}
		if (_colocOnly) {
			clear (LineUsageColocation);			//delete only old colocation lines
			if (UserInfo::getInst().getStyleSheet().getElement (RTTI_COLOCATION).isVisible()) {
				if (ShowColocationSingle) {
					drawTrack (track, false, true);
				}
				else
					drawTrackCascade (track, _speedOnly, _colocOnly);
			}
		}

		// Draw all elements of tree
		if (! _speedOnly && ! _colocOnly) {
			if (track == tree->getBaseTrack()) {
				if (_drawCascade)
					clear();				//delete the complete old tree
				else
					clear (track, false);
			}
			else {
				//clear all items that belong to _track and its children (children only if _drawCascade is true)
				clear (_track, _drawCascade);
			}
			
			if (_drawCascade)
				//recursive init call for drawTrackCascade()
				//only the specified track is drawn and all child tracks
				// (the generation cycles can have shifted)
				drawTrackCascade (track);
			else
				drawTrack (track);
		}
		
		// Draw time scale
		if ((track == tree->getBaseTrack()) & (! _speedOnly) & (! _colocOnly)) {
			TreeElementStyle tesDay = UserInfo::getInst().getStyleSheet().getElement (RTTI_DAYLINES);
			TreeElementStyle tesHour = UserInfo::getInst().getStyleSheet().getElement (RTTI_HOURLINES);
			//draw day/12h lines
			//set the correct pen width so that the lines are visible for any difference in seconds
			MyQCanvasLine *l = 0;
			MyQCanvasText *t = 0;
			QString x = "";
			int xstart = 20;
			int twhf = treeTTTPM->getTWHF();
			for (int i = 43200; i < treeTTTPM->getExperimentSeconds(); i += 43200) {
				if (i % 86400 == 0)	{		//complete day
					x = QString ("d%1").arg (i / 86400);
					if (tesDay.isVisible()) {
						l = new MyQCanvasLine (&canvas);
						l->setPen (QPen (tesDay.getColor (0), tesDay.getSize()));
						l->setPoints (xstart + 40, i / twhf, TreeWindowWidth, i / twhf);
						l->setUsage (LineUsageDaylines);
						l->setZ (Z_DayHourLine);
						l->show();
					
						t = new MyQCanvasText (x, &canvas);
						t->setFont (QFont ("Arial", FontSize)); //tesDay.getSize() * 10));
						t->setX (xstart);
						t->setY (i / twhf);
						t->setUsage (TextUsageDaylines);
						t->setZ (Z_DayHourLine);
						t->show();
					}
				}
				else {					//12 hours
					x = QString ("%1h").arg (i / 3600);
					if (tesHour.isVisible()) {	
						l = new MyQCanvasLine (&canvas);
						l->setPen (QPen (tesHour.getColor (0), tesHour.getSize()));
						l->setPoints (xstart + 40, i / twhf, TreeWindowWidth, i / twhf);
						l->setUsage (LineUsageDaylines);
						l->setZ (Z_DayHourLine);
						l->show();
						
						t = new MyQCanvasText (x, &canvas);
						t->setFont (QFont ("Arial", FontSize));	//tesHour.getSize() * 10));
						t->setX (xstart);
						t->setY (i / twhf);
						t->setUsage (TextUsageDaylines);
						t->setZ (Z_DayHourLine);
						t->show();
					}
				}
			}
		}


		//BS 2010/08/11: if the tree is finished, display a big check mark at the topleft (normally there is space left of the first cell)
		clear (LineUsageFinishedMark);
		if (tree->isFinished()) {
				MyQCanvasLine *l2 = new MyQCanvasLine (&canvas);
				l2->setPen (QPen (Qt::green, 5));
				l2->setPoints (0, 50, 20, 70);
				l2->setUsage (LineUsageFinishedMark);
				l2->setZ (Z_DayHourLine);
				l2->show();

				l2 = new MyQCanvasLine (&canvas);
				l2->setPen (QPen (Qt::green, 5));
				l2->setPoints (20, 70, 40, 0);
				l2->setUsage (LineUsageFinishedMark);
				l2->setZ (Z_DayHourLine);
				l2->show();
		}
		
		canvas.update();
	}
}

//recursive for all available tracks
//only this procedure is recursive, but not drawTrack() to save a lot of overhead
// and double variables
void TreeDisplay::drawTrackCascade (Track *_track, bool _speedOnly, bool _colocOnly)
{
	if (! _track)
		return;
	
	//draw the current track and its data
	drawTrack (_track, _speedOnly, _colocOnly);
	
	//call drawTrackCascade recursive for all children
	Track *tmpTrack = _track->getChildTrack (1);
	if (tmpTrack)
		if (tmpTrack->getNumber() > _track->getNumber())	//sometimes strange things happen => with this condition, endless loops are avoided
			drawTrackCascade (tmpTrack, _speedOnly, _colocOnly);
		
	tmpTrack = _track->getChildTrack (2);
	if (tmpTrack)
		if (tmpTrack->getNumber() > _track->getNumber())	//sometimes strange things happen => with this condition, endless loops are avoided
			drawTrackCascade (tmpTrack, _speedOnly, _colocOnly);
}

void TreeDisplay::drawTrack (Track *_track, bool _speedOnly, bool _colocOnly)
{
	if (_track) {
		
		//avoiding reading them a thousand times
		int firstTimePoint = treeTTTPM->getFirstTimePoint();
		int lastTimePoint = treeTTTPM->getLastTimePoint();
		int twhf = treeTTTPM->getTWHF();
		
		
		//for each item in the tree, a new object of one of these types has to be generated and inserted into the canvas
		MyQCanvasLine *l = 0;
		MyQCanvasEllipse *e = 0;
		MyQCanvasText *t = 0;
		
		TreeElementStyle tesCellSpeed = UserInfo::getInst().getStyleSheet().getElement (RTTI_CELLSPEED);
		TreeElementStyle tesSpeedBar = UserInfo::getInst().getStyleSheet().getElement (RTTI_SPEEDBAR);
		TreeElementStyle tesColocation = UserInfo::getInst().getStyleSheet().getElement (RTTI_COLOCATION);
		TreeElementStyle tesCellStateNormal = UserInfo::getInst().getStyleSheet().getElement (RTTI_CELLSTATE_NORMAL);
		TreeElementStyle tesCellStateSpyhop = UserInfo::getInst().getStyleSheet().getElement (RTTI_CELLSTATE_SPYHOP);
		TreeElementStyle tesCellStateFreeFloating = UserInfo::getInst().getStyleSheet().getElement (RTTI_CELLSTATE_FREEFLOATING);
		
		//holds the total covered distance of _track (summed up in pixel, conversions are before displaying it)
		int totalDistance = 0;
		
		//linePos.topLeft() contains the center of the cell circle;
		//linePos.bottom() is the begin of the child cell
		QRect linePos = linePosition (_track);
		
		if (! _speedOnly) {
			TreeElementStyle tes = UserInfo::getInst().getStyleSheet().getElement (RTTI_CELLCIRCLE_FREE);
			if (_track->getMarkCount() > 0)
				tes = UserInfo::getInst().getStyleSheet().getElement (RTTI_CELLCIRCLE_TRACKED);
			
			if (tes.isVisible()) {
				//tes.getSize()));
				e = new MyQCanvasEllipse (cellPosition (_track), &canvas);
				e->setPen (QPen (tes.getColor (0), tes.getSize()));
				e->setTrack (_track);
				e->setUsage (EllipseUsageCell);
				e->show();
			}
		}
		
		if (_track->getMarkCount() > 0) {
			//cell already tracked
			int ypos = 0;
			int midSpeed = (TreeWindowWidthComplete - TreeWindowWidth) / 2;
			int speed = 0;
			int top1 = 0;
			int oldYPos = 0;	//stores the old y position of the former trackpoint
			TrackPoint t1, t2;
			
			if (! _colocOnly) {
			
				//agenda:
				//- each bar is stamped, but created before the loop
				//- for each trackpoint, a middle point in the correct color is added to the lines
				//- finally, the lines are shown
				
				//create lines
				MyQCanvasLine *speedLine = 0;
				MyQCanvasLine *stateLineNormal = 0;
				MyQCanvasLine *stateLineSpyhop = 0;
				MyQCanvasLine *stateLineFreeFloating = 0;
 				Q3IntDict<MyQCanvasLine> waveLines (MAX_WAVE_LENGTH + 1);
				MyQCanvasLine *tissueLine = 0;
				MyQCanvasLine *generalTypeLine = 0;
				MyQCanvasLine *lineageLine = 0;
				MyQCanvasLineSpecial *addonLine = 0;
				bool addonLineVisible = false;
				
				//if (ShowSpeed) {
				if (tesCellSpeed.isVisible()) {
					speedLine = new MyQCanvasLine (&canvas, true);
					speedLine->setPoints (linePos.left() + CellDiameterX / 6 + 1, linePos.top(), linePos.left() + CellDiameterX / 6 + 1, linePos.bottom());
					speedLine->setPen (QPen (tesCellSpeed.getColor (0), tesCellSpeed.getSize()));
					speedLine->setWidth (tesCellSpeed.getSize());
					speedLine->setTrack (_track);
					speedLine->setUsage (LineUsageSpeedCell);
				}
				
				if (! _speedOnly) {
					if (tesCellStateNormal.isVisible()) {
						stateLineNormal = new MyQCanvasLine (&canvas, true, false, drawContinuousCellLines); //StampLine);
						stateLineNormal->setPoints (linePos.left() - CellDiameterX / 6, linePos.top(), linePos.left() - CellDiameterX / 6, linePos.bottom());
						//stateLineNormal->setPen (QPen (tesCellStateNormal.getColor (0), tesCellStateNormal.getSize()));
						stateLineNormal->setPen (QPen (tesCellStateNormal.getColor (0), 1));
						stateLineNormal->setWidth (tesCellStateNormal.getSize());
						stateLineNormal->setTrack (_track);
						stateLineNormal->setUsage (LineUsageState);
					}
					
					if (tesCellStateSpyhop.isVisible()) {
						stateLineSpyhop = new MyQCanvasLine (&canvas, true, false, drawContinuousCellLines); //StampLine);
						stateLineSpyhop->setPoints (linePos.left() - CellDiameterX / 6, linePos.top(), linePos.left() - CellDiameterX / 6, linePos.bottom());
						//stateLineSpyhop->setPen (QPen (tesCellStateSpyhop.getColor (0), tesCellStateSpyhop.getSize()));
						stateLineSpyhop->setPen (QPen (tesCellStateSpyhop.getColor (0), 1));
						stateLineSpyhop->setWidth (tesCellStateSpyhop.getSize());
						stateLineSpyhop->setTrack (_track);
						stateLineSpyhop->setUsage (LineUsageState);
					}
					
					if (tesCellStateFreeFloating.isVisible()) {
						stateLineFreeFloating = new MyQCanvasLine (&canvas, true, false, drawContinuousCellLines); //StampLine);
						stateLineFreeFloating->setPoints (linePos.left() - CellDiameterX / 6, linePos.top(), linePos.left() - CellDiameterX / 6, linePos.bottom());
						//stateLineFreeFloating->setPen (QPen (tesCellStateFreeFloating.getColor (0), tesCellStateFreeFloating.getSize()));
						stateLineFreeFloating->setPen (QPen (tesCellStateFreeFloating.getColor (0), 1));
						stateLineFreeFloating->setWidth (tesCellStateFreeFloating.getSize());
						stateLineFreeFloating->setTrack (_track);
						stateLineFreeFloating->setUsage (LineUsageState);
					}
					
					
					for (int i = 1; i <= MAX_WAVE_LENGTH; i++) {
						TreeElementStyle tes = UserInfo::getInst().getStyleSheet().getElement (RTTI_WAVELENGTHS [i]);
						if (tes.isVisible()) {
							MyQCanvasLine *l2 = new MyQCanvasLine (&canvas, true, false, drawContinuousCellLines);
							int xpos = linePos.left() + tes.getPositionX();
							l2->setPoints (xpos, linePos.top(), xpos, linePos.bottom());
							//l2->setPen (QPen (tes.getColor (0), tes.getSize()));
							l2->setPen (QPen (tes.getColor (0), 1));
							l2->setWidth (tes.getSize());
							l2->setTrack (_track);
							l2->setUsage (LineUsageWavelength);
							
 							waveLines.insert (i, l2);
						}
					}
					
					//addon line is always present
					//later it is decided whether it is visible at all or not
					addonLine = new MyQCanvasLineSpecial (&canvas, true);
					addonLine->setPoints (linePos.left() + CellDiameterX / 3, linePos.top(), linePos.left() + CellDiameterX / 3, linePos.bottom());
					//addonLine->setPen (QPen (tesCellStateNormal.getColor (0), tesCellStateNormal.getSize()));
					addonLine->setPen (QPen (Qt::black, 2));
					//addonLine->setWidth (tesCellStateNormal.getSize());
					addonLine->setTrack (_track);
					addonLine->setUsage (LineUsageAddOn);
					
				}
				
				int i = _track->getFirstTrace();
				
				t1 = _track->getTrackPoint (i);
				
				//walk through each trackpoint
				while ((i > -1) && (i <= _track->getLastTrace())) {
					//fetch the next actually existing trackpoint
					t2 = _track->getTrackPoint (i + 1, true);
					
					if (t1.TimePoint >= firstTimePoint) {
						
						top1 = treeTTTPM->getFiles()->calcSeconds (firstTimePoint, t1.TimePoint);
						ypos = treeTTTPM->getFiles()->calcSeconds (firstTimePoint, i) / twhf;
						if (oldYPos == 0)
							oldYPos = top1;
						
						
						if (! _speedOnly) {
							
							//display the wavelengths
							//-----------------------
							for (int i = 1; i <= MAX_WAVE_LENGTH; i++) {
								if (t1.WaveLength [i] > 0) {
									if (waveLines.find (i)) {
										TreeElementStyle tes = UserInfo::getInst().getStyleSheet().getElement (RTTI_WAVELENGTHS [i]);
										//int xpos = linePos.left() - CellDiameterX + (i - 1) * 2;
										int xpos = linePos.left() + tes.getPositionX();
										waveLines.find (i)->addMiddlePoint (xpos, ypos);
									}
								}
							}
							
							
							//middle third bar = adherent / floating / spyhop
							//-----------------------------------------------

							// Get coordinates
							int x = linePos.left() - CellDiameterX / 6,
								y = top1 / twhf;
								//y = (int)(((float)top1 / (float)twhf) + 0.5);

                            if ((t1.NonAdherent == 0) & (t1.FreeFloating == 0)) {			//normal == default
								if (tesCellStateNormal.isVisible()) {

									// Only mark point as normal, if not already marked as nonadherent or freefloating
									if(!stateLineSpyhop || !stateLineSpyhop->containsMiddlePointAt(x, y)) {
										if(!stateLineFreeFloating || !stateLineFreeFloating->containsMiddlePointAt(x, y)) {
											TreeElementStyle tes = UserInfo::getInst().getStyleSheet().getElement (RTTI_CELLSTATE_NORMAL); 
											stateLineNormal->addMiddlePoint (x, y, tes.getColor (0));
										}
									}
								}
							}
							else if (t1.FreeFloating > 0) {					//free floating
								if (tesCellStateFreeFloating.isVisible()) {
									// Only mark point as freefloating, if not already marked as normal or nonadherent
									if(!stateLineNormal || !stateLineNormal->containsMiddlePointAt(x, y)) {
										if(!stateLineSpyhop || !stateLineSpyhop->containsMiddlePointAt(x, y)) {
											TreeElementStyle tes = UserInfo::getInst().getStyleSheet().getElement (RTTI_CELLSTATE_FREEFLOATING); 
											stateLineFreeFloating->addMiddlePoint (x, y, tes.getColor (0));
										}
									}
								}
							}
                            else if ((t1.NonAdherent > 0) & (t1.FreeFloating == 0)) {			//spyhop
								if (tesCellStateSpyhop.isVisible()) {
									// Only mark point as nonadherent, if not already marked as normal or freefloating
									if(!stateLineNormal || !stateLineNormal->containsMiddlePointAt(x, y)) {
										if(!stateLineFreeFloating || !stateLineFreeFloating->containsMiddlePointAt(x, y)) {
											TreeElementStyle tes = UserInfo::getInst().getStyleSheet().getElement (RTTI_CELLSTATE_SPYHOP); 
											stateLineSpyhop->addMiddlePoint (x, y, tes.getColor (0));
										}
									}
								}
							}
							
							
							//another three bars - all other cell types (general type, tissue type, lineage)
							//------------------------------------------------------------------------------
							
							{//unconditioned block for having a local variable 'tes'
							
							TreeElementStyle tes;
							//tissue type
							if (t1.tissueType > 0) {
								tes = UserInfo::getInst().getStyleSheet().getElement (RTTI_TISSUETYPES [t1.tissueType - 1]);
								if (tes.isVisible()) {
									if (! tissueLine) {
										tissueLine = new MyQCanvasLine (&canvas, true); //StampLine);
										tissueLine->setPoints (linePos.left() + tes.getPositionX(), linePos.top(), linePos.left() + tes.getPositionX(), linePos.bottom());
										tissueLine->setPen (QPen (tes.getColor (0), tes.getSize()));
										tissueLine->setWidth (tes.getSize());
										tissueLine->setTrack (_track);
										tissueLine->setUsage (LineUsageTissue);
									}
									
									if (tissueLine) {
										tissueLine->addMiddlePoint (linePos.left() + tes.getPositionX(), top1 / twhf, tes.getColor (0));
									}
								}
							}
							
							//general type
							tes = UserInfo::getInst().getStyleSheet().getElement (RTTI_GENERALTYPES [t1.cellGeneralType - 1]);
							if (tes.isVisible()) {
								if (! generalTypeLine) {
									generalTypeLine = new MyQCanvasLine (&canvas, true); //StampLine);
									generalTypeLine->setPoints (linePos.left() + tes.getPositionX(), linePos.top(), linePos.left() + tes.getPositionX(), linePos.bottom());
									generalTypeLine->setPen (QPen (tes.getColor (0), tes.getSize()));
									generalTypeLine->setWidth (tes.getSize());
									generalTypeLine->setTrack (_track);
									generalTypeLine->setUsage (LineUsageGeneralType);
								}
								
								if (generalTypeLine) {
									//@ todo remove these!!!
/*									if (t1.cellLineage == 2)
										//ery
										generalTypeLine->addMiddlePoint (linePos.left() + tes.getPositionX(), top1 / twhf, Qt::red);
									else if (t1.cellLineage == 1)
										//mega
										generalTypeLine->addMiddlePoint (linePos.left() + tes.getPositionX(), top1 / twhf, Qt::blue);
									else*/
										generalTypeLine->addMiddlePoint (linePos.left() + tes.getPositionX(), top1 / twhf, tes.getColor (0));
								}
							}
							
							//lineage
							tes = UserInfo::getInst().getStyleSheet().getElement (RTTI_LINEAGES [t1.cellLineage - 1]);
							if (tes.isVisible()) {
								if (! lineageLine) {
									lineageLine = new MyQCanvasLine (&canvas, true); //StampLine);
									lineageLine->setPoints (linePos.left() + tes.getPositionX(), linePos.top(), linePos.left() + tes.getPositionX(), linePos.bottom());
									lineageLine->setPen (QPen (tes.getColor (0), tes.getSize()));
									lineageLine->setWidth (tes.getSize());
									lineageLine->setTrack (_track);
									lineageLine->setUsage (LineUsageLineage);
								}
								
								if (lineageLine) {
									lineageLine->addMiddlePoint (linePos.left() + tes.getPositionX(), top1 / twhf, tes.getColor (0));
								}
							}
							
							//endomitosis display
							if (t1.EndoMitosis) {
								tes = UserInfo::getInst().getStyleSheet().getElement (RTTI_ENDOMITOSIS);
								if (tes.isVisible()) {
									MyQCanvasText *endo = new MyQCanvasText (tes.getSymbol(), &canvas);
									endo->setFont (QFont ("Arial", tes.getSize()));
									endo->setColor (tes.getColor (0));
									endo->setX (linePos.left() + tes.getPositionX() - 5);
									endo->setY (top1 / twhf - tes.getSize() * 2);
									endo->setTrack (_track);
									endo->setParam (t1.TimePoint);	//store timepoint for detection
									endo->setUsage (TextUsageEndomitosis);
									endo->show();
								}
							}
							
							
							//display addon attributes
							//========================
							
							if (addonLine) {
								short int addonValue = t1.getAddOn1 (UserInfo::getInst().getAddOnBitpack());
								char key = TrackingKeys::getKeyFromValue (addonValue);
								tes = UserInfo::getInst().getStyleSheet().getAssociatedElement (key);
								if (tes.isVisible()) {
									addonLineVisible = true;
									addonLine->addMiddlePoint (linePos.left() + tes.getPositionX(), top1 / twhf, tes.getSize(), tes.getColor (0));
								}
							}
							
							} //unconditioned
							
						}
						
						//sum up the covered distance of this cell (over complete lifetime)
						totalDistance += (int)t1.distanceTo (t2);
						
						//if (ShowSpeed) {
						if (tesCellSpeed.isVisible() || tesSpeedBar.isVisible()) {
							if (t2.TimePoint > 0 && t2.TimePoint <= lastTimePoint) {
							
								speed = tree->calcSpeed (t1, t2, SpeedAmplifier);
								QColor speedColor = tree->calcSpeedColor (speed);
								
								if (tesCellSpeed.isVisible())
									//speedLine->addMiddlePoint (linePos.left() + CellDiameterX / 6 + 1, top1 / tree->getTWHF(), speedColor);
									speedLine->addMiddlePoint (linePos.left() + tesCellSpeed.getPositionX(), top1 / twhf, speedColor);
								
								
								if (tesSpeedBar.isVisible()) {
									if ((_speedOnly) && (_track == TTTManager::getInst().getCurrentTrack())) {
										//display speed in bar at the right side of the tree
										//the available region is (TreeWindowWidth,0) - (TreeWindowWidthComplete,TreeWindowHeight)
										
										//speed display: horizontal line, its length and color depending on the speed
										int speedWidth = speed; // / (midSpeed * 2);
										//truncate speed to fit into right window frame - the user can change the amplifier!
										speedWidth = QMIN (midSpeed * 2, speedWidth);
										//@todo use MyQCanvasLineSpecial here
										l = new MyQCanvasLine (&canvas, false);
										l->setPen (QPen (speedColor, 2));
										l->setPoints (	TreeWindowWidth + midSpeed - speedWidth / 2, ypos, 
												TreeWindowWidth + midSpeed + speedWidth / 2, ypos);
										l->setUsage (LineUsageSpeedBar);
										l->setTrack (_track);
										l->show();
									}
								}
							}
						}
						
						//for further selected attributes
						if (! _speedOnly) {
						}
						
					}
					
					if (t1.TimePoint == t2.TimePoint)
						i = lastTimePoint + 1;	//exit loop
					else {
						t1 = t2;
						i = t2.TimePoint;
					}
					
					oldYPos = top1;
				}
				
				//set lines visible
				if (! _speedOnly) {
					if (stateLineNormal)
						stateLineNormal->show();
					if (stateLineSpyhop)
						stateLineSpyhop->show();
					if (stateLineFreeFloating)
						stateLineFreeFloating->show();
					for (uint i = 0; i < waveLines.size(); i++)
						if (waveLines.find (i))
							waveLines.find (i)->show();
					
					if (tissueLine)
						tissueLine->show();
					if (generalTypeLine)
						generalTypeLine->show();
					if (lineageLine)
						lineageLine->show();
					
					if (addonLine) {
						if (addonLineVisible)
							addonLine->show();
						//else
						//	addonLine->hide();
					}
					
					
				}
				if (ShowSpeed)
					if (speedLine)
						speedLine->show();
			}
			
			
			if (tesColocation.isVisible()) {
                                if ((ShowColocationAll | (ShowColocationSingle & _colocOnly)) & (! _speedOnly)) {
					//all cells that are alive during the whole living period of the parameter cell have to be found out
					//the ones existing within the user-defined radius are connected via a horizontal line
					
					TrackPoint tmpTrackPoint, actTrackPoint;
					
					for (int i = _track->getFirstTrace(); i <= _track->getLastTrace(); i++) {
						if (treeTTTPM->getFiles()->exists (i, 1, -1)) {
							//only if there really is a trackpoint, a colocation can be displayed
							actTrackPoint = _track->getTrackPoint (i);
							if (actTrackPoint.X != -1) {
							
								Q3IntDict<Track> tmp = tree->coExistingTracks (i, _track);
								
								
								ypos = treeTTTPM->getFiles()->calcSeconds (firstTimePoint, i) / twhf;
								
								for (int j = 0; j < (int)tmp.size(); j++) {
									if ((tmp.find (j)) && (tmp.find (j) != _track)) {
										
										
										//calculate if the distance between the cells is less than the limit
										tmpTrackPoint = tmp.find (j)->getTrackPoint (i);
										
										int radius = (int)tmpTrackPoint.distanceTo (actTrackPoint);
	/*									int radius = (int)sqrt ( (tmpTrackPoint.X - actTrackPoint.X) * 
																(tmpTrackPoint.X - actTrackPoint.X) + 
																(tmpTrackPoint.Y - actTrackPoint.Y) * 
																(tmpTrackPoint.Y - actTrackPoint.Y));*/
										
										//note: radius, not diameter!
										if (radius <= ColocationRadius) {
											int zh = getTrackXPosition (tmp.find (j));
											
											//draw the line
											//set dots at both endpoints for clear distinction
											l = new MyQCanvasLine (&canvas);
											l->setPen (QPen (tesColocation.getColor (0), tesColocation.getSize()));
											l->setPointColors (COLOCATIONDOTCOLOR, COLOCATIONDOTCOLOR);
											l->setPoints (linePos.left(), ypos, zh, ypos);
											l->setZ (Z_ColocationLine);
											l->setTrack (_track);
											l->setUsage (LineUsageColocation);
											l->show();
										}
									}
								}
							}
						}
					}
				}
			}
		}
		
		QPoint pos;
		
		//display cell number above cell circle
		//=====================================
		TreeElementStyle tes = UserInfo::getInst().getStyleSheet().getElement (RTTI_CELLNUMBERS);
		if (tes.isVisible()) {
			if (! _speedOnly && ! _colocOnly) {
				//note: numbers should be readable even when zoom is very small	=> ZoomFactor has to influence font size
				t = new MyQCanvasText (QString().setNum (_track->getNumber()), &canvas);
				t->setFont (QFont ("Arial", tes.getSize()));
				
				t->setColor (tes.getColor (0));
				pos = cellPosition (_track).center();
				t->setX (pos.x() - 5);
				t->setY (pos.y() - tes.getSize() * 2);
				t->setTrack (_track);
				t->setUsage (TextUsageCellNumber);
				t->show();
			}
		}
		
		//comments
		//========
		tes = UserInfo::getInst().getStyleSheet().getElement (RTTI_COMMENTASTERISK);
		if (tes.isVisible()) {
			if (! _speedOnly && ! _colocOnly) {
				//for each timepoint that has a comment, an asterisk is displayed right of the heritage line
				//a click on this asterisk displays a small tooltip with the timepoint comment
				for (int i = _track->getFirstTrace(); i < _track->getLastTrace(); i++) {
					if (! _track->getComment (i).isEmpty()) {
						//show asterisk
						t = new MyQCanvasText ("*", &canvas);
						t->setFont (QFont ("Arial", tes.getSize()));
						t->setColor (tes.getColor (0));
						pos = QPoint (linePos.left() + tes.getPositionX(), (treeTTTPM->getFiles()->calcSeconds (firstTimePoint, i) / twhf) + (tes.getSize() / 2));
						t->setX (pos.x());
						t->setY (pos.y());
						t->setTrack (_track);
						t->setParam (i);		//store timepoint
						t->setUsage (TextUsageComment);
						t->show();
					}
				}
			}
		}
		
		//generation time
		//===============
		tes = UserInfo::getInst().getStyleSheet().getElement (RTTI_GENERATIONTIME);
		if (tes.isVisible()) {
			if (! _speedOnly && ! _colocOnly) {
				//display life time for each cell in vertical text, placed right of the heritage line
				int secs = treeTTTPM->getFiles()->calcSeconds (_track->getFirstTrace(), _track->getLastTrace());
				if (secs > 0) {
					QString text = "";
					if (tes.getSymbol() == "hm")
						text = QString ().sprintf ("%03d:%02d", secs / 3600,  (secs % 3600) / 60);
					else if (tes.getSymbol() == "h")
						text = QString ().sprintf ("%05.3fh", (float)secs / 3600.0f);
					else if (tes.getSymbol() == "m")
						text = QString ().sprintf ("%04dm", secs / 60);
					
					t = new MyQCanvasText (text, 270, &canvas);
					t->setFont (QFont ("Arial", tes.getSize()));
					t->setColor (tes.getColor (0));
					
					pos = linePos.topLeft();
					t->setX (pos.x() + CellDiameterX);
					t->setY (pos.y() + text.length() * 5 + CellDiameterY);
					t->setTrack (_track);
					t->setUsage (TextUsageGenerationTime);
					t->show();
				}
			}
		}
		
		//cell distance
		//=============
		tes = UserInfo::getInst().getStyleSheet().getElement (RTTI_CELLDISTANCE);
		if (tes.isVisible()) {
			if (! _speedOnly && ! _colocOnly) {
				if (totalDistance > 0) {
					//display covered distance for each cell in vertical text, placed right of the heritage line
					QString text = "";
					if (tes.getSymbol() == "pix")
						text = QString ().sprintf ("%04d " + tes.getSymbol(), totalDistance);
					
					t = new MyQCanvasText (text, 270, &canvas);
					t->setFont (QFont ("Arial", tes.getSize()));
					t->setColor (tes.getColor (0));
					
					pos = linePos.topLeft();
					t->setX (pos.x() + CellDiameterX);
					t->setY (pos.y() + text.length() * 5 + CellDiameterY);
					t->setTrack (_track);
					t->setUsage (TextUsageCellDistance);
					t->show();
				}
			}
		}
		
		
		//cluster ID (from mining results)
		//================================
		tes = UserInfo::getInst().getStyleSheet().getElement (RTTI_CLUSTERID);
		if (tes.isVisible()) {
			if (! _speedOnly && ! _colocOnly) {
				if (_track->getClusterID() > 0) {
					QString text = QString ("%1").arg (_track->getClusterID());
					t = new MyQCanvasText (text, 0, &canvas);
					t->setFont (QFont ("Arial", tes.getSize()));
					t->setColor (tes.getColor (0));
					
					pos = linePos.topLeft();
					t->setX (pos.x() - CellDiameterX);
					t->setY (pos.y() + tes.getPositionX());
					t->setTrack (_track);
					t->setUsage (TextUsageClusterID);
					t->show();
				}
			}
		}
		
			
		//stopreason drawing
		//==================
		if (! _speedOnly && ! _colocOnly) {
			int offset = TreeWindowWidth / (int)pow (2.0, _track->getGeneration() + 1);
			int top = linePos.bottom();
			
			QPoint cellPos = cellPosition (_track, true).center();
			switch (_track->getStopReason()) {
				case TS_DIVISION:
					//draw horizontal line for cell division 
					//(the two new cell circles are drawn in the next recursive call)
					
					tes = UserInfo::getInst().getStyleSheet().getElement (RTTI_DIVISIONLINE);
					if (tes.isVisible()) {
						if (_track->getChildTrack (1))
							top = treeTTTPM->getFiles()->calcSeconds (firstTimePoint, _track->getChildTrack (1)->getFirstTrace()) / twhf;
						else
							if (_track->getChildTrack (2))
								top = treeTTTPM->getFiles()->calcSeconds (firstTimePoint, _track->getChildTrack (2)->getFirstTrace()) / twhf;
						
						l = new MyQCanvasLine (&canvas);
						l->setPen (QPen (tes.getColor (0), tes.getSize()));
						l->setPoints (linePos.x() - offset / 2, top, linePos.x() + offset / 2, top);
						l->setTrack (_track);
						l->setUsage (LineUsageDivision);
						l->show();
					}
					
					break;
				case TS_APOPTOSIS:
					tes = UserInfo::getInst().getStyleSheet().getElement (RTTI_STOPREASON_APOPTOSIS);
					if (tes.isVisible()) {
						t = new MyQCanvasText (tes.getSymbol(), &canvas);
						t->setFont (QFont ("Arial", tes.getSize()));
						t->setColor (tes.getColor (0));
						t->setX (cellPos.x() - CellDiameterX);
						t->setY (cellPos.y());
						t->setTrack (_track);
						t->setUsage (TextUsageStopReason);
						t->show();
					}
					
					break;
				case TS_LOST:
					tes = UserInfo::getInst().getStyleSheet().getElement (RTTI_STOPREASON_LOST);
					if (tes.isVisible()) {
						t = new MyQCanvasText (tes.getSymbol(), &canvas);
						t->setFont (QFont ("Arial", tes.getSize()));
						t->setColor (tes.getColor (0));
						t->setX (cellPos.x() - CellDiameterX);
						t->setY (cellPos.y());
						t->setTrack (_track);
						t->setUsage (TextUsageStopReason);
						t->show();
					}
					break;
				case TS_NONE:
					tes = UserInfo::getInst().getStyleSheet().getElement (RTTI_STOPREASON_NONE);
					if (tes.isVisible()) {
						t = new MyQCanvasText (tes.getSymbol(), &canvas);
						t->setFont (QFont ("Arial", tes.getSize()));
						t->setColor (tes.getColor (0));
						t->setX (cellPos.x() - CellDiameterX);
						t->setY (cellPos.y());
						t->setTrack (_track);
						t->setUsage (TextUsageStopReason);
						t->show();
					}
					break;
				default:
					;
			}
		}
	}
}

//transforms the (mostly mouse) position to the logical coordinate system of the tree widget, including zoom
QPoint TreeDisplay::mapToLogical (const QPoint &_pos, bool, bool) const //bool _shift, bool _seconds) const
{	
	if (! tree)
		return QPoint();
	
	QMatrix wm = this->worldMatrix();
	if (wm.isInvertible()) {
		QPoint r =  wm.invert().map (_pos);
		return r;
	}
	else
		return QPoint();
}

QPoint TreeDisplay::mapToReal (const QPoint &_pos, bool) const //bool _shift) const
{
	if (! true)
		return QPoint();
	
	QMatrix wm = this->worldMatrix();
	QPoint r =  wm.map (_pos);
	return r;
}

QRect TreeDisplay::getDisplayRegion() const
{
	QRect rc = QRect (this->contentsX(), this->contentsY(), this->visibleWidth(), this->visibleHeight());
	return (this->inverseWorldMatrix().mapRect (rc));
}

int TreeDisplay::getTrackXPosition (Track *_track) const
{
	if (! _track)
		return -1;
	
	//note: all calculations are made with float variables to avoid rounding errors
	
	float stepCount = 1;
	float powInit = (float)(pow (2.0, _track->getGeneration()));
	//note: very important to use TreeWindowWidth, and not TreeWindowWidthComplete, because with this usage the right speed bar is possible easily
	float stepWidth = TreeWindowWidth / (powInit * 2);
	
	//cell's x position = (2*((cell number) - (highest number in the parental generation)) - 1) * stepWidth
	//highest number of parental generation = 2^generation - 1
	if (_track != tree->getBaseTrack())
		stepCount = 2.0f * ((float)_track->getNumber() - (powInit - 1.0f)) - 1.0f;
	
	float x = stepCount * stepWidth;
	return (int)x;
}

QRect TreeDisplay::cellPosition (Track *_track, bool _lastTrace) const
{
	//this function returns the QRect into which the cell ellipse completely fits into
	//as a consequence, the QRect must have its center at the cell position
	
	if (! _track) 
		return QRect();				//returns an invalid rectangle
	if (! tree)
		return QRect();
	
	int y = 0;
	
	//calculate seconds between track and first time point - this is the real ordinate
	
	if (_lastTrace && ((_track->getLastTrace() != -1))) {		
	//if last trace not yet set -> take first, see below in else-case
		y = treeTTTPM->getFiles()->calcSeconds (treeTTTPM->getFirstTimePoint(), _track->getLastTrace());
	}
	else {
		/*
		if (_track->getSecondsFromBegin() > 0)
			y = _track->getSecondsFromBegin();	//use stored value
		else {
		*/
			int firstTrace = _track->getFirstTrace();
			if (firstTrace > treeTTTPM->getLastTimePoint())
				firstTrace = treeTTTPM->getLastTimePoint();
			
			y = treeTTTPM->getFiles()->calcSeconds (treeTTTPM->getFirstTimePoint(), firstTrace);
			_track->setSecondsFromBegin (y);
		/*
		}
		*/
	}
	
	//stretch
	y /= treeTTTPM->getTWHF();
	
	int x = getTrackXPosition (_track);

	//qDebug() << "TreeDisplay::cellPosition(): x=" << x << ", y=" << y;
	
	return QRect (x - CellDiameterX / 2, y - CellDiameterY / 2, CellDiameterX, CellDiameterY);
}

QRect TreeDisplay::cellPositionReal (Track *_track, bool _lastTrace) const
{
	QRect cp = cellPosition (_track, _lastTrace);
	QPoint _posTL = mapToReal (cp.topLeft());
	QPoint _posBR = mapToReal (cp.bottomRight());
	return QRect (_posTL, _posBR);	
}

QRect TreeDisplay::linePosition (Track *_track) const
{
	if (! _track) 
		return QRect();			//returns an invalid rectangle
	if (! tree)
		return QRect();
	
	QPoint c = cellPosition (_track).center();
	//the length of the line is returned in the height of the rectangle returned
	int yo = 0;
	
	Track *x = _track->getChildTrack (1);
	if (! x)
		x = _track->getChildTrack (2);
	
	if (x) {
		yo = treeTTTPM->getFiles()->calcSeconds (_track->getFirstTrace(), x->getFirstTrace());
	}
	else if (_track->getLastTrace() > 0 && _track->getLastTrace() <= treeTTTPM->getLastTimePoint()) {
		yo = treeTTTPM->getFiles()->calcSeconds (_track->getFirstTrace(), _track->getLastTrace());
	}
	yo /= treeTTTPM->getTWHF();

	//float tmp = (float)yo / (float)treeTTTPM->getTWHF();
	//yo = (int)(tmp + 0.5f);
	
	return QRect (c.x(), c.y(), 1, yo);
}

//recursive!
Track* TreeDisplay::getPosTrack (QPoint _pos) const
{
	if (! tree)
		return 0;
	
	//get canvas items under position
	//QCanvasItemList l = canvas.collisions (_pos);
	int size = 2;
	Q3CanvasItemList l = canvas.collisions (QRect (_pos.x() - size, _pos.y() - size, size * 2, size * 2));
	for (Q3CanvasItemList::Iterator it = l.begin(); it != l.end(); ++it) {
		if ((*it)->rtti() == MyQCanvasEllipse::RTTI) {
			//track found under mouse cursor - this is the desired object
			return ((MyQCanvasEllipse*)(*it))->getTrack();
		}
	}
	
	//nothing found
	return 0;
}

Track* TreeDisplay::selectTrack (const QPoint &_mousePos, bool _select, bool _multiple)
{
	if (! tree)
		return 0;
	
	//calling this method is possible only if this view's tree is the currently active tree
	if (tree != TTTManager::getInst().getTree())
		return 0;
	
	QPoint xPos (mapToLogical (_mousePos));
	
	Track *track = getPosTrack (xPos);
	
	selectTrack (track, _select, _multiple);
	
	return track;
}

void TreeDisplay::selectTrack (Track *_track, bool _select, bool _multiple, bool _updateView)
{
	//calling this method is possible only if this view's tree is the currently active tree
	if (tree != TTTManager::getInst().getTree())
		return;
	
	if (! _track)
		return;
	
	int mark = -1;
	
	if (! _multiple) {
		selectedTrackCount = 0;
		selectedTracks.clear();
		if (_select) {
			selectedTrackCount = 1;
			TTTManager::getInst().setCurrentTrack (_track);
			mark = 1;
		}
		else
			TTTManager::getInst().setCurrentTrack (0);
	}
	else {
		if (_select) {
			//add track to the multiple selection list, if it is not yet present there
			
			bool alreadyIn = false;
			for (uint i = 0; i < selectedTracks.size(); i++) {
				if (selectedTracks.find (i)) {
					if (selectedTracks.find (i)->getNumber() == _track->getNumber()) {
						alreadyIn = true;
						break;
					}
				}
			}
			
			if (! alreadyIn) {
				selectedTrackCount++;
				if (selectedTrackCount >= (int)selectedTracks.size())
					selectedTracks.resize (selectedTrackCount + 10);
				
				selectedTracks.insert (selectedTrackCount - 1, _track);
				mark = 2;
			}
		}
		else {
/*			if (selectedTrackCount == 1) {
				selectedTrackCount--;
				_track = TTTManager::getInst().getCurrentTrack();
				TTTManager::getInst().setCurrentTrack ( 0;
			}
			else {*/
				//remove track from the multiple selection list
				for (uint i = 0; i < selectedTracks.size(); i++) {
					if (selectedTracks.find (i)) {
						if (selectedTracks.find (i)->getNumber() == _track->getNumber()) {
							_track = selectedTracks.find (i);
							selectedTracks.remove (i);		//note: element is not deleted!!
							selectedTrackCount--;
							break;
						}
					}
				}
//			}
			mark = 0;
		}
	}
	
	if (! _track)
		return;
	
	if (mark > -1) {
		//find circle in the tree that corresponds to _track
                //note: it is supposed that there is a correspondance - as this method is only called if _track != 0
		MyQCanvasEllipse *e = 0;
		Q3CanvasItemList l = canvas.allItems();
		for (Q3CanvasItemList::Iterator it = l.begin(); it != l.end(); ++it) {
			if ((*it)->rtti() == MyQCanvasEllipse::RTTI) {
				if (((MyQCanvasEllipse*)(*it))->getTrack() == _track) {
					e = (MyQCanvasEllipse*)(*it);
					break;
				}
			}
		}		
		
		if (e) {
			QBrush br = Qt::NoBrush;
			switch (mark) {
				case 0:	//deselect _track
					br = Qt::NoBrush;
					break;
				case 1:	//select main track
					br = QBrush (CELLSELECTCOLOR);
					break;
				case 2:	//select additional track
					br = QBrush (CELLSELECTCOLORADDITIONAL);
					break;
				default:
					;
			}
			e->setBrush (br);
			
			if (_updateView)
				canvas.update();
		}
	}
}

void TreeDisplay::deselectTrack()
{
	//calling this method is possible only if this view's tree is the currently active tree
	if (tree != TTTManager::getInst().getTree())
		return;
	
	//deselect all multiply selected cells
	for (uint i = 0; i < selectedTracks.size(); i++) {
		if (selectedTracks.find (i)) {
			selectTrack (selectedTracks.find (i), false, true);
		}
	}
	selectTrack (TTTManager::getInst().getCurrentTrack(), false, true);
	TTTManager::getInst().setCurrentTrack (0);
}

void TreeDisplay::clear (bool _speedOnly, bool _colocOnly)
{
	//remove all objects
	Q3CanvasItemList allItems = canvas.allItems();
	for (Q3CanvasItemList::Iterator iter = allItems.begin(); iter != allItems.end(); ++iter) {
		int rtti = (*iter)->rtti();
		if (rtti == MyQCanvasEllipse::RTTI) {
			switch (((MyQCanvasEllipse*)(*iter))->getUsage()) {
				case EllipseUsageCell:
					if (! _speedOnly && ! _colocOnly)
						delete (*iter);
					break;
				default:
					delete (*iter);
			}
		}
		else if (rtti == MyQCanvasLine::RTTI) {
			switch (((MyQCanvasLine*)(*iter))->getUsage()) {
				case LineUsageHeritage:
					if (! _speedOnly && ! _colocOnly)
						delete (*iter);
					break;
				case LineUsageDivision:
					if (! _speedOnly && ! _colocOnly)
						delete (*iter);
					break;
				case LineUsageSpeedCell:
					if (! _colocOnly)
						delete (*iter);
					break;
				case LineUsageSpeedBar:
					if (! _colocOnly)
						delete (*iter);
					break;
				case LineUsageState:
					if (! _speedOnly && ! _colocOnly)
						delete (*iter);
					break;
				case LineUsageWavelength:
					if (! _speedOnly && ! _colocOnly)
						delete (*iter);
					break;
				case LineUsageColocation:
					if (! _speedOnly)
						delete (*iter);
					break;
				case LineUsageTimepoint:
					//do not delete
					break;
				default:
					delete (*iter);
			}
		}
		else if (rtti == MyQCanvasText::RTTI) {
			switch (((MyQCanvasText*)(*iter))->getUsage()) {
				case TextUsageCellNumber:
					if (! _speedOnly && ! _colocOnly)
						delete (*iter);
					break;
				case TextUsageComment:
					if (! _speedOnly && ! _colocOnly)
						delete (*iter);
					break;
				case TextUsageGenerationTime:
					if (! _speedOnly && ! _colocOnly)
						delete (*iter);
					break;
				case TextUsageCellDistance:
					if (! _speedOnly && ! _colocOnly)
						delete (*iter);
					break;
				case TextUsageStopReason:
					if (! _speedOnly && ! _colocOnly)
						delete (*iter);
					break;
				case TextUsageEndomitosis:
					if (! _speedOnly && ! _colocOnly)
						delete (*iter);
					break;
				default:
					delete (*iter);
			}
		}
		else {
			delete (*iter);
		}
	}
}

void TreeDisplay::clear (int _usage)
{
	Q3CanvasItemList allItems = canvas.allItems();
	for (Q3CanvasItemList::Iterator iter = allItems.begin(); iter != allItems.end(); ++iter) {
		int rtti = (*iter)->rtti();
		if (rtti == MyQCanvasEllipse::RTTI) {
			if (((MyQCanvasEllipse*)(*iter))->getUsage() == _usage) 
				delete (*iter);
		}
		else if (rtti == MyQCanvasLine::RTTI) {
			if (((MyQCanvasLine*)(*iter))->getUsage() == _usage) 
				delete (*iter);
		}
		else if (rtti == MyQCanvasText::RTTI) {
			if (((MyQCanvasText*)(*iter))->getUsage() == _usage) 
				delete (*iter);
		}
		else {
			
		}
	}
}

//recursive
void TreeDisplay::clear (Track *_track, bool _deleteChildren, bool _speedOnly, bool _colocOnly)
{
	if (! _track)
		return;
	
	Q3CanvasItemList allItems = canvas.allItems();
	for (Q3CanvasItemList::Iterator iter = allItems.begin(); iter != allItems.end(); ++iter) {
		int rtti = (*iter)->rtti();
		if (rtti == MyQCanvasEllipse::RTTI) {
			if (((MyQCanvasEllipse*)(*iter))->getTrack() == _track) {
				switch (((MyQCanvasEllipse*)(*iter))->getUsage()) {
					case EllipseUsageCell:
						if (! _speedOnly && ! _colocOnly)
							delete (*iter);
						break;
					default:
						delete (*iter);
				}
				//delete (*iter);
			}
		}
		else if (rtti == MyQCanvasLine::RTTI) {
			if (((MyQCanvasLine*)(*iter))->getTrack() == _track) {
				switch (((MyQCanvasLine*)(*iter))->getUsage()) {
					case LineUsageHeritage:
						if (! _speedOnly && ! _colocOnly)
							delete (*iter);
						break;
					case LineUsageDivision:
						if (! _speedOnly && ! _colocOnly)
							delete (*iter);
						break;
					case LineUsageSpeedCell:
						if (! _colocOnly)
							delete (*iter);
						break;
					case LineUsageSpeedBar:
						if (! _colocOnly)
							delete (*iter);
						break;
					case LineUsageState:
						if (! _speedOnly && ! _colocOnly)
							delete (*iter);
						break;
					case LineUsageWavelength:
						if (! _speedOnly && ! _colocOnly)
							delete (*iter);
						break;
					case LineUsageColocation:
						if (! _speedOnly)
							delete (*iter);
						break;
					case LineUsageAddOn:
						if (! _speedOnly && ! _colocOnly)
							delete (*iter);
						break;
					case LineUsageTimepoint:
						//do not delete
						break;
					default:
						//delete (*iter);
						;
				}
				//delete (*iter);
			}
		}
		else if (rtti == MyQCanvasText::RTTI) {
			if (((MyQCanvasText*)(*iter))->getTrack() == _track) {
				switch (((MyQCanvasText*)(*iter))->getUsage()) {
					case TextUsageCellNumber:
						if (! _speedOnly && ! _colocOnly)
							delete (*iter);
						break;
					case TextUsageComment:
						if (! _speedOnly && ! _colocOnly)
							delete (*iter);
						break;
					case TextUsageGenerationTime:
						if (! _speedOnly && ! _colocOnly)
							delete (*iter);
						break;
					case TextUsageCellDistance:
						if (! _speedOnly && ! _colocOnly)
							delete (*iter);
						break;
					case TextUsageStopReason:
						if (! _speedOnly && ! _colocOnly)
							delete (*iter);
						break;
					case TextUsageEndomitosis:
						if (! _speedOnly && ! _colocOnly)
							delete (*iter);
						break;
					default:
						delete (*iter);
				}
				//delete (*iter);
			}
		}
		else {
			
		}
	}
	
	if (_deleteChildren) {
		clear (_track->getChildTrack (1), _deleteChildren); //, _allItems);
		clear (_track->getChildTrack (2), _deleteChildren); //, _allItems);
	}
}

void TreeDisplay::setShowGenerationTime (bool _show)
{
	TreeElementStyle tes = UserInfo::getInst().getStyleSheet().getElement (RTTI_GENERATIONTIME);
	tes.setVisible (_show);
	UserInfo::getInst().getRefStyleSheet().setElement (tes);
	draw();
}

bool TreeDisplay::deleteTrack (Track *_track, bool _noInteraction)
{
	if (! _track)
		return false;
	if (! tree)
		return false;
	
	Track *mother = _track->getMotherTrack();
	
	if (_track == TTTManager::getInst().getCurrentTrack())
		setCurrent (0);
	
	bool delete_them = false;
	
	if ((_track->getChildTrack (1) == 0) && (_track->getChildTrack (2) == 0)) {
		//no children
		delete_them = true;
	}
	else {
		//there are children - look whether they are already tracked or not
		
		bool ChildrenTracked = false;
		
		if (_track->getChildTrack (1))
			if (_track->getChildTrack (1)->tracked())
				ChildrenTracked = true;
		if (_track->getChildTrack (2))
			if (_track->getChildTrack (2)->tracked())
				ChildrenTracked = true;
		
		if (! ChildrenTracked) {
			delete_them = true;
		}
		else {
			if (! _noInteraction) {
				switch (QMessageBox::question (0, "Important note", "This cell has already tracked children. \n\nDo you really want to delete the cell with all children cascadingly?", "Yes", "No", QString::null, 1, 1)) {
					//case QMessageBox::Yes:
					case 0:
						delete_them = true;
						break;
					//case QMessageBox::No:
					case 1:
						break;
					default:
						;
				}
			}
			else {
				delete_them = true;
			}
		}
	}
	
	if (delete_them) {
		clear (_track);
		tree->deleteTrack (_track);
		draw (mother);
		selectedTracks.resize (0);
		selectedTrackCount = 0;
	}
	
	return true;
}

void TreeDisplay::update()
{
	canvas.update();
}

QImage* TreeDisplay::getTreeImage (int _topTP, int _bottomTP)
{
	QPixmap pix (TreeWindowWidthComplete, TreeWindowHeight);
	
	QPainter p (&pix);
	//note: I (BS) do not know why const_cast is necessary, yet without, a compiler error occurs
	const_cast<Q3Canvas*>(&canvas)->drawArea (canvas.rect(), &p);
	p.end();
	
	QImage *result = new QImage (pix.convertToImage());
	
	//clip unwanted regions
	//method: create white pictures of the size of the region to be deleted and bitblt them over the tree
	if ((_topTP > 0) | (_bottomTP > 0)) {
		int twhf = treeTTTPM->getTWHF();
		int height = 0;
		
		if (_topTP > 0) {
			height = treeTTTPM->getFiles()->calcSeconds (treeTTTPM->getFirstTimePoint(), _topTP);
			height /= twhf;
			
			QImage layer (TreeWindowWidthComplete, height, 32);
			layer.fill (QColor (Qt::white).pixel());
                        //bitBlt (result, 0, 0, &layer, 0, 0, TreeWindowWidthComplete, height, 0);
                        QPainter p (result);
                        p.drawImage (0, 0, layer, 0, 0, TreeWindowWidthComplete, height);
                        p.end();
                }
		
		if (_bottomTP > 0) {
			height = treeTTTPM->getFiles()->calcSeconds (_bottomTP, treeTTTPM->getLastTimePoint());
			height /= twhf;
			
			QImage layer (TreeWindowWidthComplete, height, 32);
			layer.fill (QColor (Qt::white).pixel());
                        //bitBlt (result, 0, TreeWindowHeight - height, &layer, 0, 0, TreeWindowWidthComplete, height, 0);
                        QPainter p (result);
                        p.drawImage (0, TreeWindowHeight - height, layer, 0, 0, TreeWindowWidthComplete, height);
                        p.end();
                }
	}
	
	
	return result;
	
}

void TreeDisplay::setDrawContinuousCellLines( bool _on )
{
	drawContinuousCellLines = _on;
}



