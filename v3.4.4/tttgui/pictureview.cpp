/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "pictureview.h"

// Project includes
#include "tttbackend/tttmanager.h"
#include "tttbackend/tttpositionmanager.h"
#include "multiplepictureviewer.h"
#include "tttdata/userinfo.h"
#include "tttdata/trackingkeys.h"
#include "ttttracking.h"
#include "tttbackend/tools.h"
#include "clickableqgraphicsellipseitem.h"
#include "clickableqgraphicspolygonitem.h"
#include "tttbackend/picturecontainer.h"
#include "movienextpositem.h"
#include "tttmovie.h"
#include "tttautotracking.h"
#include "tttautotrackingtreewindow.h"
#include "autotrackingcellcircle.h"

// Qt includes
#include <QElapsedTimer>
#include <QKeyEvent>
#include <QWheelEvent>



//definition of static member-variable drawOnlySelectedTracks
bool PictureView::drawOnlySelectedTracks = false;


PictureView::PictureView (MultiplePictureViewer *_multiPic, int _number, QWidget *_parent, TTTPositionManager *_positionManager, bool _overlay)
 : /*Paintable*/QGraphicsView (_parent), wlIndex (_number), isShown (false),
   shifting (false), allCellsUniform (false), placeHolderSet (false)
{
	positionManager = _positionManager;

	multiPicViewer = _multiPic;

	hide();		//ensure that the widget is invisible until it is explicitly shown

	setScene (new QGraphicsScene (this));

	resize (_parent->width(), _parent->height());

	setCacheMode (QGraphicsView::CacheBackground);
	//does not work satisfyingly, as it overrides any mouse cursor -> custom dragging
	//setDragMode (QGraphicsView::ScrollHandDrag);
	setViewportUpdateMode (QGraphicsView::FullViewportUpdate);

	//waitingForPaintEvent = false;
	//handleKeyReleaseAfterPaint = false;
	currentZoom = 100;
	currentZoomAccurate = 1.0;
	setBackground = false;
	leftMouseDown = false;
	rightMouseDown = false;
	radiusSelectionMode = false;
	symbolPositionMode = false;
	lastDisplayedTimePoint = 0;
	currentTimepoint = -1;
	drawPrevTrackPoitns = false;
	numPrevTrackPointsToDraw = 10;
	widthOfTails = 5;
	drawSegmentation = false;
	drawPrevTrackPointsTransparencyEffect = false;
	drawPrevTrackPointsOnlyOfSelectedCellBranch = false;
	showTracks = true;
	showTrackNumbers = true;

	mouseWidget = 0;
	currentColocationRadius = 0;
	singleBackgroundSelectionMode = false;

	overlayFrame = _overlay;
	wait_for_trackmark_set = false;

	picture = 0;
	placeHolderRect = 0;
	createBasicElements();

	setAlignment(Qt::AlignLeft | Qt::AlignTop);

	showBackground = true;

	// OH 140716: enable mouse tracking, because otherwise MouseMove events were not received in some experiments..
	setMouseTracking(true);
}

PictureView::~PictureView()
{
    if (orientationBox)
        delete orientationBox;

    if (wavelengthBox)
        delete wavelengthBox;

	if(outOfSyncBox)
		delete outOfSyncBox;
}

void PictureView::keyPressEvent (QKeyEvent *_ev)
{
        QGraphicsView::keyPressEvent (_ev);

        switch (_ev->key()) {
                case Qt::Key_Control:
                        //controlKeyPressed = true;
                        break;
                default:
                        ;
        }
}

//EVENT: 
void PictureView::keyReleaseEvent (QKeyEvent *_ev)
{
	this->QGraphicsView::keyReleaseEvent (_ev);

	_ev->ignore();



	//read mouse props
	if (! mouseWidget)
		mouseWidget = this;

	QPointF pictureMouseCoordinates (mouseWidget->mapFromGlobal (QCursor::pos()));
	pictureMouseCoordinates = mouseWidget->mapToScene (pictureMouseCoordinates.toPoint());

	QPointF mouseCoordinates = mouseWidget->positionManager->getDisplays().at (mouseWidget->wlIndex).calcAbsCoords (pictureMouseCoordinates, &mouseWidget->positionManager->positionInformation);

	int MouseButtons = 0;
	if (mouseWidget->leftMouseDown)
		MouseButtons |= 1;
	if (mouseWidget->rightMouseDown)
		MouseButtons |= 2;
	//if (MouseWidget->MiddleMouseDown)
	//	MouseButtons |= 4;

	float diameter = multiPicViewer->getCircleMouseRadius();

	if (! multiPicViewer->circleMouseCursorUsed()) {
		diameter = CELLCIRCLESIZE;
	}

	//divide by zoom factor
	if (currentZoom != 0)
		diameter /= currentZoomAccurate;
	else {
		//full picture display
		//for simplifying reasons, take the X value
		//(normally, if the cell circles have to be exact, they are not tracked in full picture)
		//@todo nothing to be done?
		//				if (positionManager->getDisplays().getZoomRatio (Index, true) != 0)
		//					diameter = diameter / positionManager->getDisplays().getZoomRatio (Index, true);
	}

	// DEBUG
	//qDebug() << QTime::currentTime().toString("hh:mm:ss.zzz") << "keyReleaseEvent(): waitingForPaintEvent: " << waitingForPaintEvent << ", handleKeyReleaseAfterPaint: " << handleKeyReleaseAfterPaint  << ", lastKeyReleaseEventTime: " << lastKeyReleaseEventTime.toString("hh:mm:ss.zzz");

	// Handle key event immediately
	lastKeyReleaseEventParameters = KeyReleaseEventParams(_ev->key(), mouseCoordinates, pictureMouseCoordinates, diameter, MouseButtons, _ev->modifiers());
	if(handleKeyReleaseEvent(lastKeyReleaseEventParameters))
		_ev->accept();

	/*
	// Remember parameters
	lastKeyReleaseEventParameters = KeyReleaseEventParams(_ev->key(), mouseCoordinates, pictureMouseCoordinates, diameter, MouseButtons, _ev->modifiers());

	
	// If contents were not painted yet, the key event cannot be handled now, but is 
	// remembered for processing after the next paint event (if another key event was
	// already remembered and not processed yet, it is ignored here)
	if(waitingForPaintEvent) {
		handleKeyReleaseAfterPaint = true;
		lastKeyReleaseEventTime = QDateTime::currentDateTime();

		// Assume that we will process this event
		_ev->accept();
	}
	else {
		// Handle key event immediately
		handleKeyReleaseAfterPaint = false;
		lastKeyReleaseEventTime = QDateTime();
		if(handleKeyReleaseEvent(lastKeyReleaseEventParameters))
			_ev->accept();
	}
	*/
}

/*
void PictureView::paintEvent(QPaintEvent *e)
{
	//qDebug() << QTime::currentTime().toString("hh:mm:ss.zzz") << "PaintEvent(): waitingForPaintEvent: " << waitingForPaintEvent << ", handleKeyReleaseAfterPaint: " << handleKeyReleaseAfterPaint << ", lastKeyReleaseEventTime: " << lastKeyReleaseEventTime.toString("hh:mm:ss.zzz");

	// Let Qt paint everything
	QGraphicsView::paintEvent(e);

	// Check if contents were changed but not painted yet
	if(waitingForPaintEvent) {
		waitingForPaintEvent = false;

		// Check if there is a KeyReleaseEvent to handle after paint
		if(handleKeyReleaseAfterPaint) {
			handleKeyReleaseAfterPaint = false;

			// Handle key event only if it was not received too long (>1 sec) ago, otherwise it is ignored
			if(lastKeyReleaseEventTime.isValid()) {
				qint64 msecsDiff = lastKeyReleaseEventTime.msecsTo(QDateTime::currentDateTime());
				if(msecsDiff >= 0 && msecsDiff <= 1000) {
					handleKeyReleaseEvent(lastKeyReleaseEventParameters);
				}
				lastKeyReleaseEventTime = QDateTime();
			}
		}
	}
}
*/

bool PictureView::handleKeyReleaseEvent(const KeyReleaseEventParams& params)
{
	// If key is -1, params has not been initialized properly
	if(params.key == -1)
		return false;

	//NOTE: as the Tracking mode is started for ALL instances of PictureView, it has to be decided in which one the key actually was pressed!
	//      This affects the mouse position and the pressed mouse buttons

	///tracking_now is true iff tTt is in tracking mode and a current track actually exists
	bool inTrackingMode = TTTManager::getInst().isTracking() && (TTTManager::getInst().getCurrentTrack() != 0);

	float mouseX = params.mouseCoordinates.x();
	float mouseY = params.mouseCoordinates.y();
	switch (params.key) {
	case Qt::Key_0:			//proceed picture (setting track mark)
		if (inTrackingMode) {
			if (wait_for_trackmark_set) {
				//check for trackmark setting delay
				return true;
			}

			TTTManager::getInst().addTrackpoint (positionManager->getTimepoint(), mouseX, mouseY, params.diameter,
				setBackground, params.mouseButtons, mouseWidget->positionManager->positionInformation.getIndex());

			emit TrackPointSet (TTTManager::getInst().getCurrentTrack(), positionManager->getTimepoint());

			// OH-120619
			emit PictureProceedDemand (true, 0, true);
		}
		else {
			// If autotracking window is open, start manual tracking with numpad0 button
			TTTAutoTracking* frmAutoTracking = TTTManager::getInst().frmAutoTracking;
			if(frmAutoTracking && frmAutoTracking->getTreeWindow()) {
				frmAutoTracking->getTreeWindow()->startManualTracking();
			}
			else {
				// OH-120619
				emit PictureProceedDemand (true, 0);
			}
		}

		/*
		OH-120619
		emit PictureProceedDemand (true, 0);
		*/
		break;
	case Qt::Key_2:			//preceed picture (setting track mark)  => tracking backwards
		if (inTrackingMode) {
			if (wait_for_trackmark_set) {
				//check for trackmark setting delay
				return true;
			}

			TTTManager::getInst().addTrackpoint (positionManager->getTimepoint(), mouseX, mouseY, params.diameter,
				setBackground, params.mouseButtons, mouseWidget->positionManager->positionInformation.getIndex());
			emit TrackPointSet (TTTManager::getInst().getCurrentTrack(), positionManager->getTimepoint());
		}

		emit PictureProceedDemand (false, 0, inTrackingMode);
		break;
	case Qt::Key_Period:
	case Qt::Key_Comma:		//preceed picture (deleting track mark)
		if (inTrackingMode) {
			TTTManager::getInst().getCurrentTrack()->deleteMark (positionManager->getTimepoint());
		}

		//move to previous picture
		emit PictureProceedDemand (false, 0, inTrackingMode);
		break;
	case Qt::Key_1:			//single picture backward (not deleting track mark)
		emit PictureProceedDemand (false, 0);
		break;
	case Qt::Key_3:			//single picture forward (no track mark is set)
		emit PictureProceedDemand (true, 0);
		break;
	case Qt::Key_4:
		//preceed 10% or change cell diameter
		if (! multiPicViewer->circleMouseCursorUsed())
			emit PictureProceedDemand (false, 10);
		else {
			//change cell diameter indirectly by emitting a mouse wheel event
			QWheelEvent *wheel_Event = new QWheelEvent (QPoint (0, 0), -120, 0);
			wheelEvent (wheel_Event);
			if (wheel_Event)
				delete wheel_Event;
		}
		break;
	case Qt::Key_6:
		//proceed 10% or change cell diameter
		if (! multiPicViewer->circleMouseCursorUsed())
			emit PictureProceedDemand (true, 10);
		else {
			//change cell diameter indirectly by emitting a mouse wheel event
			QWheelEvent *wheel_Event = new QWheelEvent (QPoint (0, 0), 120, 0);
			wheelEvent (wheel_Event);
			if (wheel_Event)
				delete wheel_Event;
		}
		break;
	case Qt::Key_7:
		//jump to first picture
		emit PictureProceedDemand (false, -1);
		break;
	case Qt::Key_9:
		//jump to last picture
		emit PictureProceedDemand (true, -1);
		break;
	//case Qt::Key_5:
	//	//center currently active cell
	//	emit centerCurrentTrackDemand();
	//	break;
	case Qt::Key_Escape:	//interrupt tracking
		if (inTrackingMode) {
			emit trackingInterrupted (TTTManager::getInst().getCurrentTrack());
		}

		break;

		//additional tracking keys
	case Qt::Key_Q:
	case Qt::Key_W:
	case Qt::Key_E:
	case Qt::Key_R:
	case Qt::Key_T:

	case Qt::Key_A:
	case Qt::Key_S:
	case Qt::Key_D:
	case Qt::Key_F:
	case Qt::Key_G:

	case Qt::Key_Z:
	case Qt::Key_X:
	case Qt::Key_C:
	case Qt::Key_V:
	case Qt::Key_B:

		// If ctrl or shift was pressed, do not consider these keys as tracking keys and let TTTMovie handle them
		if(params.modifiers & (Qt::ControlModifier | Qt::ShiftModifier)) {
			emit unhandledKeyReleased (params.key, params.modifiers & Qt::ShiftModifier, params.modifiers & Qt::ControlModifier);
			break;
		}
		else {
			if (inTrackingMode) {
				//effect of these keys:
				//- set usual trackpoint, but with the addon attribute
				//- proceed one picture forward

				if (wait_for_trackmark_set) {
					//check for trackmark setting delay
					return true;
				}

				TTTManager::getInst().addTrackpoint (positionManager->getTimepoint(), mouseX, mouseY, params.diameter,
					setBackground, params.mouseButtons, mouseWidget->positionManager->positionInformation.getIndex());
				{//variable definition block in switch
					TrackPoint *tp = TTTManager::getInst().getCurrentTrack()->getRefTrackPoint (positionManager->getTimepoint());
					if (tp) {
						tp->setAddOn1 (UserInfo::getInst().getAddOnBitpack(), TrackingKeys::getValue (params.key));
					}
				}
				emit TrackPointSet (TTTManager::getInst().getCurrentTrack(), positionManager->getTimepoint());
			}

			emit PictureProceedDemand (true, 0, inTrackingMode);
			break;
		}
	
	case Qt::Key_Alt:
		multiPicViewer->setOrientationBoxPosition (params.pictureMouseCoordinates);
		break;

	//case Qt::Key_Control: {
		//controlKeyPressed = false;
		//QPointF punktal = mapToScene (oldMousePos.toPoint());
		//multiPicViewer->setOrientationBoxPosition (pictureMouseCoordinates);
		//break;
		//				  }
	default:
		{
			//processed in TTTMovie
			bool shift = (params.modifiers & Qt::ShiftModifier) == Qt::ShiftModifier;
			bool ctrl = (params.modifiers & Qt::ControlModifier) == Qt::ControlModifier;

			emit unhandledKeyReleased (params.key, shift, ctrl);

			// Return, so unhandled key events are propagated to parent -> eventually parent TTTMovie
			return false;
			//break;
		}
	}

	return true;
}

void PictureView::mousePressEvent (QMouseEvent *_ev)
{
	this->QGraphicsView::mousePressEvent (_ev);

	_ev->ignore();

	if (! isShown)
		return;

	switch (_ev->button()) {
	case Qt::MidButton:
		/*
		OH121009: No more shifting with middle mouse button.
		shifting = true;
		shiftBackupCursor = cursor();
		*/ 
		break;
	case Qt::LeftButton:
		leftMouseDown = true;

		emit pictureViewChosen (wlIndex);

		break;

	case Qt::RightButton:
		shifting = true;
		shiftBackupCursor = cursor();

		rightMouseDown = true;
		break;
	default:
		;
	}

	oldMousePos = _ev->pos();
	//	utdOldMousePos = ev->pos();

	_ev->accept();
}

void PictureView::mouseMoveEvent (QMouseEvent *_ev)
{

        QGraphicsView::mouseMoveEvent (_ev);

	
        QPointF globalPos = mapToScene (_ev->pos());    //globalPos now contains the position within the picture


        //include offset (topleft corner in the layout)
        QPointF absCoords (positionManager->getDisplays().at (wlIndex).calcAbsCoords (globalPos, &positionManager->positionInformation));

        //emit a signal to update the coordinate display in frmMovie
        emit mouseMovedInPicture (absCoords);

	
        if (! isShown)
		return;
	
        if (radiusSelectionMode) {
		//@todo: replace line with circle
//@todo
//		//delete old line
//		drawRadiusLine (utdOldMousePos);
//		//draw new
//		drawRadiusLine (ev->pos());
	}
	
	
	
//@todo BS 2010/03/29 if the mouse pointer reaches the border of the frame and of the picture(!), an arrow in this direction indicates the next position there
/*	if (ev->pos().) {
		//mouse is at the border of the frame
		
	}*/
	
	
	
        if (shifting) {
		//the picture has to be shifted
                //all adaptions in other views are not to care for - QGraphicsView triggers the scrollContentsBy() event, there the other display shifts are mediated
		
                //drag picture - no scaling of mouse coordinates necessary, they are always appropriate for the current scrollbar setting

                setCursor (Qt::ClosedHandCursor);

                QPointF newPos = _ev->pos();
                QPoint shift = (newPos - oldMousePos).toPoint();

                QScrollBar *sb = horizontalScrollBar();
                sb->setValue (sb->value() - shift.x());
                sb = verticalScrollBar();
                sb->setValue (sb->value() - shift.y());

	}



        //if (_ev->modifiers() & Qt::ControlModifier) {
		if (_ev->modifiers() & Qt::AltModifier) {

                //shift orientation box
                //note: there is no need to care about zoom factors, displayed regions, etc.!
                //	- the box is simply displayed with the same (picturewise) offset in all instances, and that's it
                //	- the coordinates are not needed, and even if they were, they could be calculated easily

                QPointF punktal = mapToScene (_ev->pos());
                multiPicViewer->setOrientationBoxPosition (punktal);
        }

	
        oldMousePos = _ev->pos();
	
        _ev->accept();
}

void PictureView::mouseReleaseEvent (QMouseEvent *_ev)
{
        this->QGraphicsView::mouseReleaseEvent (_ev);

        _ev->ignore();		//initialization
	
	//the position is mapped to picture coordinates
        QPointF globalPos = mapToScene (_ev->pos());
        //globalPos now contains the position within the picture

        //include offset (topleft corner in the layout)
        QPointF pos (positionManager->getDisplays().at (wlIndex).calcAbsCoords (globalPos, &positionManager->positionInformation));
        //QPointF pos = positionManager->getDisplays().at (Index).calcAbsCoords (_ev->pos(), &positionManager->positionInformation);
	
        switch (_ev->button()) {
		case Qt::MidButton:
					/*
		OH121009: No more shifting with middle mouse button.
                        shifting = false;
                        setCursor (shiftBackupCursor);
						*/
                        break;
		case Qt::LeftButton:
			
                        leftMouseDown = false;
			
			//emit signal to set the current wavelength in frmMovie
                        emit displaySelected (wlIndex);
			
                        if (radiusSelectionMode) {
				//the old line is deleted
                                drawRadiusLine (_ev->pos());
				//a left click sets the starting point of the line for the distance
                                radiusLineStartPos = _ev->pos();
			}
                        else if (symbolPositionMode) {
				//note: the symbol is not deleted!
				
                                emit symbolPositionSelected (pos);
				
                                setCursor (otherBackupCursor);
                                symbolPositionMode = false;
			}
			
			if (singleBackgroundSelectionMode) {
				singleBackgroundSelectionMode = false;
				setCursor(QCursor (Qt::ArrowCursor));
				
				emit singleBackgroundSet (pos);
			}
			
			break;
		case Qt::RightButton:
				if(shifting) {
					shifting = false;
					setCursor (shiftBackupCursor);
				}
                
				rightMouseDown = false;

//@todo: radius, grahpics, symbol selection
//			if (RadiusSelectionMode) {
//				//a right click sets the end point of the line for the distance
//				RadiusLineEndPos = ev->pos();
//
//				//the radius is specified in pixel, but relative to 100%
//				//so if a zoom mode is active, the distance has to be transformed
//				QPointF a = RadiusLineStartPos;	//positionManager->getDisplays().at (Index).calcAbsCoords (RadiusLineStartPos);
//				QPointF b = RadiusLineEndPos;	//positionManager->getDisplays().at (Index).calcAbsCoords (RadiusLineEndPos);
//				b = b - a;
//				b = positionManager->getDisplays().at (Index).strechShift (b);
//				//int tmpx = b.x() - a.x();
//				//int tmpy = b.y() - a.y();
//				//int r = (int)(sqrt (tmpx * tmpx + tmpy * tmpy));
//				//r = r / (positionManager->getDisplays().at (Index).ZoomFactor / 100);
//				int r = (int)(sqrt (b.x() * b.x() + b.y() * b.y()));
//				r /= (positionManager->getDisplays().at (Index).ZoomFactor / 100);
//				emit RadiusSelected (r);
//				RadiusSelectionMode = false;
//			}

//BS 2008/../..
//currently switched off:
//it is not always meant to start tracking with a right click!
/*			//the user can start tracking a cell with clicking into it with right button
			if (! TTTManager::getInst().isTracking())
				if (tracks) 
					for (int i = 0; i < (int)trackCircles->size(); i++)
						if (! trackCircles->point (i).isNull())
							if (QRect (trackCircles->point (i).x() - trackCircleSize / 2, 
									trackCircles->point (i).y() - trackCircleSize / 2, 
									trackCircleSize, trackCircleSize).contains (ev->pos())) {
								
								emit trackSelected (tracks->find (i), true, true);
								break;
							}*/
			
			break;
		default:
			;
	}
	
	
	//emitted for different purposes
        emit mouseReleased (_ev, pos);
	
        _ev->accept();
}

void PictureView::mouseDoubleClickEvent (QMouseEvent *_ev)
{
        _ev->accept();
}

void PictureView::wheelEvent (QWheelEvent *_ev)
{
	//generally set to ignore this event; accept it only if it truly is processed here
	_ev->ignore();
	
    bool ctrlPressed = ((_ev->modifiers() & Qt::ControlModifier) == Qt::ControlModifier);

    if (multiPicViewer->circleMouseCursorUsed() && ctrlPressed) {
            //change size of the cell diameter
            //if (TTTManager::getInst().isTracking()) {
		int add = _ev->delta() / 120;
		emit cellSizeChanged (add);
		_ev->accept();
		
            //}
	}
	else if(!ctrlPressed) {
		///@todo set as user choice whether control needs to be pressed or not
		
		//zoom into/out of the picture
		//the zooming is done via in-/decreasing the current zoom factor by 25 percent
		//the anchor (central) point when zooming into is the current mouse pointer position
		
		int add = _ev->delta() / 120;
		
        QPointF mousepos (this->mapFromGlobal (_ev->globalPos()));		//map from screen to widget coordinates

        //QPointF zoomPosition = positionManager->getDisplays().at (Index).calcAbsCoords (mousepos, &positionManager->positionInformation);

        int zoomFactor;
		int curZoom = multiPicViewer->getCurrentZoom();
		if(curZoom) 
			zoomFactor = curZoom + add * 25;
		else {
			// Get accurate zoom in percent and round to the next number that is dividable by 25
			curZoom = currentZoomAccurate * 100;
			curZoom = ((curZoom + 12) / 25) * 25;

			zoomFactor = curZoom + add * 25;
		}
		
		if ((zoomFactor >= 50) && (zoomFactor <= 1000)) {

            //positionManager->frmMovie->setZoom (zoomFactor, mousepos.x(), mousepos.y()); //zoomPosition.x(), zoomPosition.y());
            multiPicViewer->setZoom (zoomFactor, mousepos.x(), mousepos.y());

			_ev->accept();
		}
	}
}

void PictureView::enterEvent (QEvent *)
{
	//called when the mouse pointer enters this widget
	
	//the widget which currently grabs the keyboard focus receives a pointer to this widget
	// (if that widget is also an instance of PictureView)
	//necessary to enable tracking in all instances
	
	QWidget *grabber = QWidget::keyboardGrabber();
	
	if (! grabber)
		return;
	
	//test whether the keyboard grabber is of class PictureView
	if (grabber->isA ("PictureView")) {
		((PictureView *)grabber)->setMouseWidget (this);
	}
}

void PictureView::resizeEvent (QResizeEvent *_ev)
{
        this->QGraphicsView::resizeEvent (_ev);

        //adjust graphic line sizes
//        qreal x1 = 1, x2 = 1;
//        qreal y1 = 0, y2 = viewport()->height();
//        blackLine->setLine (x1, y1, x2, y2);
//
//        x1 = viewport()->width() - 1;
//        x2 = x1;
//        whiteLine->setLine (x1, y1, x2, y2);
        //graphicsAdjustWidget->setGeometry (0, 0, parentWidget()->width(), parentWidget()->height());


        //note: a paintEvent() is triggered automatically directly afterwards, so do not call repaint() or update()
}

void PictureView::drawAdditions()
{
        drawTracks();

        drawCurrentSymbols();

        drawCircles (multiPicViewer->getCellPaths());

        if (multiPicViewer->isInStartingCellSelectionMode()) {
                clearView (GRAPHICSDATA_USAGE, GRAPHICSDATA_USAGE_SELECTED_COLONY_START);
                drawCircles (multiPicViewer->getStartingCells());
        }

//@todo
//	if (RadiusSelectionMode)
//		drawRadiusLine (mapFromGlobal (QCursor::pos()));
	
	
        currentColocationRadius = 0;

}

//@todo
//int PictureView::draw (PictureView *_pv, int _colorChannel)
//{
//	if (! positionManager->isInitialized())
//		return 0;
//
//	if (! Show) {
//		return 0;
//	}
//
//        int return_value = 0;
//
//	CurrentColocationRadius = 0;
//
//	if (Overlay) {
//		//draw the overlayed pictures (all are now in THIS->img)
//
//		if (img) {
//                        //bitBlt (this, 0, 0, img, 0, 0, img->width(), img->height(), 0);
//                        QPainter p (draw_img);
//                        p.drawImage (0, 0, *img, 0, 0, img->width(), img->height());
//                        p.end();
//
//                        return_value = 1;
//		}
//		else
//                        return_value = 0;
//	}
//	else {
//
//                //QPaintDevice *pd = (QPaintDevice*)this;
//		QImage *img = 0;
//
//		//if a PictureView object was specified, then into its img attribute is drawn instead of this
//		if (_pv) {
//			if (! _pv->img) {
//				//create img
//                                _pv->img = new QImage (_pv->geometry().width(), _pv->geometry().height(), QImage::Format_ARGB32);
//				//_pv->img->fill (qRgba (125, 125, 125, 125));
//				_pv->img->fill (0);
//			}
//			img = _pv->img;
//		}
//
//		//if a picture at the desired timepoint does not exist,
//		// the last displayed is reloaded so that the track circles can not accumulate in "silent" pictures
//                if (positionManager->getPictures()->drawPicture (positionManager->getTimepoint(), Index, draw_img, _colorChannel, img)) {
//			//picture exists
//
//			LastDisplayedTimePoint = positionManager->getTimepoint();		//picture exists
//
//			//send signal to notify about an update (only if wavelength > 0)
//			if (Index > 0) {
//				emit pictureUpdated();
//			}
//
//                        return_value = 1;
//		}
//		else {
//			//no picture exists => use last timepoint
//			//NOTE: this is not applied in overlay mode (cf. annotations in TTTMovie::showPicture())
//			if (! _pv)
//				if (LastDisplayedTimePoint > 0) {
//                                        positionManager->getPictures()->drawPicture (LastDisplayedTimePoint, Index, draw_img, _colorChannel, img);
//                                        return_value = 2;
//				}
//
//                        return_value = 0;
//		}
//	}
//
//        update();
//
//        return return_value;
//}

//void PictureView::clearX()
//{
//        if (! isShown)
//		return;
//	
//	//clear the display
//        clearView();
//
//        currentColocationRadius = 0;
////	internalCircles.clear();
//
//        picture = 0;
//
//        update();
//}

void PictureView::startRadiusSelectionMode()
{
        radiusSelectionMode = true;
}

void PictureView::receiveSymbolPosition (Symbol _symbol)
{
        //currentSymbol = _symbol;
        symbolPositionMode = true;

        //create mouse pointer resembling the current symbol
        otherBackupCursor = cursor();
        setCursor (Symbol::createSymbolMouseCursor (_symbol));
}

////SLOT:
//void PictureView::callKeyReleaseEvent (int)
//{
//	//keyReleaseEvent (&QKeyEvent (QEvent::KeyRelease, _key, 0, 0));
//	//keyReleaseEvent (ev);
//}

void PictureView::drawTails( const QPointF& trackPictureCoords, float diameter, ITrack* track, const ITrackPoint* trackpoint, const QColor& circleColor )
{
	//QPointF circlePos = trackPictureCoords;
	//circlePos.setX(circlePos.x() - diameter / 2.0f);
	//circlePos.setY(circlePos.y() - diameter / 2.0f);

	if(trackpoint) {
		if(track) {
			int counter = 0;
			const ITrackPoint* prevTrackPoint = 0;
			int curAlpha = 255;
			int alphaDelta = 255 / numPrevTrackPointsToDraw;
			int curTp = currentTimepoint + 1;
			while(counter++ < numPrevTrackPointsToDraw) {
				--curTp;

				// Check if curTp is too low for current track
				if(curTp < track->getFirstTimePoint()) {
					// Check if track has parent, but only if track has even track number to avoid painting of parent tail with several colors
					ITrack* mother = track->getMother();
					if(track->getTrackNumber() % 2 == 0 && mother)
						track = mother;
					else
						break;
				}

				// Get trackpoint
				const ITrackPoint* tmpTP = track->getTrackPointByTimePoint(curTp);
				if(!tmpTP)
					continue;

				// Draw line
				if(prevTrackPoint) {
					// Calc local pixel coordinates
					QPointF prevGlobal(prevTrackPoint->getX(), prevTrackPoint->getY());
					QPointF prevLocal = positionManager->getDisplays().at (wlIndex).calcTransformedCoords (prevGlobal, &positionManager->positionInformation);
					QPointF curGlobal(tmpTP->getX(), tmpTP->getY());
					QPointF curLocal = positionManager->getDisplays().at (wlIndex).calcTransformedCoords (curGlobal, &positionManager->positionInformation);

					/*
					// Check distance in pixels, to draw no lines inside the circle
					float distance1 = QLineF(circlePos, prevLocal).length();
					float distance2 = QLineF(circlePos, curLocal).length();
					if(distance1 > CELLCIRCLESIZE/2 && distance2 > CELLCIRCLESIZE/2) {
					*/

						QLineF finalLine = QLineF(prevLocal, curLocal);

						// Create line
						
						// OH-140703: re-use items
						//QGraphicsLineItem *newLineItem = new QGraphicsLineItem(finalLine);
						//newLineItem->setData (GRAPHICSDATA_USAGE, GRAPHICSDATA_USAGE_TRACKCIRCLE);	
						QGraphicsLineItem *newLineItem = getQGraphicsLineItem(finalLine);

						QPen pen(QColor(circleColor.red(), circleColor.green(), circleColor.blue(), curAlpha));
						pen.setCapStyle(Qt::RoundCap);
						//pen.setJoinStyle(Qt::RoundJoin);
						//pen.setCapStyle(Qt::FlatCap);
						pen.setJoinStyle(Qt::MiterJoin);
						pen.setWidth(widthOfTails);
						newLineItem->setPen(pen);
						newLineItem->setZValue (ZVALUE_TRACKCIRCLE);
						scene()->addItem(newLineItem);
						

						//++counter;
						if(drawPrevTrackPointsTransparencyEffect)
							curAlpha -= alphaDelta;
					//}
				}

				// Remember trackpoint
				prevTrackPoint = tmpTP;

										
			}
		}
	}
}


void PictureView::drawSegmentationToTrack(const unsigned char* segData, int segDataSizeX, int segDataSizeY, ITrack* track, const ITrackPoint* trackpoint, const QColor& color)
{
	if(!trackpoint)
		return;
	// Check if there is an image
	if(!picture)
		return;

	// If the centroid of trackpoint is within a blob in the segmentation mask, draw that blob's perimeter

	QPointF posGlobal(trackpoint->getX(), trackpoint->getY());
	QPointF posLocal = positionManager->getDisplays().at (wlIndex).calcTransformedCoords (posGlobal, &positionManager->positionInformation);
	int startX = std::min((int)posLocal.x(), segDataSizeX);
		int startY = std::min((int)posLocal.y(), segDataSizeY);
	if(segData[startY*segDataSizeX + startX]) {
		// Get image data
		QPixmap pixmap = picture->pixmap();
		if(pixmap.isNull())
			return;

		// Draw perimeters
		std::vector<int> neighborDeltasX;
		std::vector<int> neighborDeltasY;
		neighborDeltasX.push_back(-1);	// Left
		neighborDeltasY.push_back(0);
		neighborDeltasX.push_back(0);	// Top
		neighborDeltasY.push_back(-1);
		neighborDeltasX.push_back(1);	// Right
		neighborDeltasY.push_back(0);
		neighborDeltasX.push_back(0);	// Bottom
		neighborDeltasY.push_back(1);
		QList<int> pixelsStack;
		QSet<int> visited;
		pixelsStack.push_back(startX);
		pixelsStack.push_back(startY);
		visited.insert(startY*segDataSizeX + startX);
		QPainter painter(&pixmap);
		painter.setPen(QPen(color));
		while(pixelsStack.size()) {
			// Take next pixel
			int y = pixelsStack.takeLast();
			int x = pixelsStack.takeLast();
			int linIndex = y*segDataSizeX + x;

			// Check if background neighbor exists and add not visited neighbors to stack
			bool backgroundNeighborFound = false;
			for(int iNeighbor = 0; iNeighbor < neighborDeltasX.size(); ++iNeighbor) {
				int nx = x + neighborDeltasX[iNeighbor];
				int ny = y + neighborDeltasY[iNeighbor];
				int nLinear = ny * segDataSizeX + nx;
				if(nx >= 0 && ny >= 0 && nx < segDataSizeX && ny < segDataSizeY) {
					// Check if neighbor is background
					if(segData[nLinear] == 0)
						backgroundNeighborFound = true;
					else {
						// Neighbor is not background, so add to stack if not visited yet
						if(!visited.contains(nLinear)) {
							visited.insert(nLinear);
							pixelsStack.push_back(nx);
							pixelsStack.push_back(ny);
						}
					}
				}
			}

			// If background neighbor was found, pixel belongs to perimeter
			if(backgroundNeighborFound) {
				painter.drawPoint(x, y);
			}
		}
		
		// Update image data
		painter.end();
		picture->setPixmap(pixmap);
	}
}

void PictureView::drawTracks (/*QPaintDevice *_pd*/)
{
    if (! isShown)
		return;
	
    //delete all old track circle items
    clearView (GRAPHICSDATA_USAGE, GRAPHICSDATA_USAGE_TRACKCIRCLE);
	if (!showTracks && !showTrackNumbers && !drawPrevTrackPoitns && !drawSegmentation) {
            return;
    }

	// Load segmentation
	std::unique_ptr<unsigned char[]> segData;
	int segDataSizeX = 0, segDataSizeY = 0;
	if(drawSegmentation) {
		QString segFileName = positionManager->getSegmentationFile(currentTimepoint, wlIndex, std::max(positionManager->getZIndex(),1), 0);
		if(!QFile::exists(segFileName) && wlIndex != 0) {
			// Look for w0 image if nothing was found for wlIndex
			segFileName = positionManager->getSegmentationFile(currentTimepoint, 0, std::max(positionManager->getZIndex(),1), 0);
		}
		QImage segImage(segFileName);
		if(!segImage.isNull()) {
			// Ensure format is INDEXED8
			if(segImage.format() != QImage::Format_Indexed8)
				segImage = segImage.convertToFormat(QImage::Format_Indexed8);

			// Copy data into continuous buffer
			segDataSizeX = segImage.width();
			segDataSizeY = segImage.height();
			unsigned char* ptr = new unsigned char[segImage.width() * segImage.height()];
			segData = std::unique_ptr<unsigned char[]>(ptr);
			for(int y = 0; y < segImage.height(); ++y)
				memcpy(ptr + y * segImage.width(), segImage.scanLine(y), segImage.width());
		}
	}

    // -----------------Oliver---------------------------
    Q3IntDict<Track> tmpTracks;	// this is needed because TreeView->getSelectedTracks returns no pointer, but TTTManager::getInst().getCurrentTracks() does
    Q3IntDict<Track> *tracks = 0;

    // Draw only selected tracks?
    if(drawOnlySelectedTracks) {
        // Get tracking window
        const TTTTracking* frmTracking = TTTManager::getInst().frmTracking;
        if(!frmTracking)
                return;

        // Get tree view
        const TreeDisplay *TreeView = frmTracking->treeView;
        if(!TreeView)
                return;

        // Get selected tracks
        tmpTracks = TreeView->getSelectedTracks(true);
        tracks = &tmpTracks;
    }
    else {
        tracks = TTTManager::getInst().getCurrentTracks();
    }

    //bool showExternalTracks = TTTManager::getInst().getShowExternalTracks();
    //if (showExternalTracks)
    //    if (! TTTManager::getInst().getExternalTracks())
    //        showExternalTracks = false;

	// Overwrite track radius?
	bool bOverwriteTrackRadius = multiPicViewer->overwriteTrackRadiusForDisplay();
	int overwriteRadius = multiPicViewer->getTrackRadiusForDisplay();

	// Draw "normal" tracks from the currently opened tree
	if (tracks) {
		if (! tracks->isEmpty()) {

			for (int i = 0; i < (int)tracks->size(); i++) {
				//draw an ellipse for each track

				Track *tmpTrack = tracks->find (i);

				if (tmpTrack) {

					TrackPoint tmp = tmpTrack->getTrackPoint (positionManager->getTimepoint());

					if (tmp.isSet() && (Tools::floatEquals (tmp.X, -1.0f, 2) != 0)) {

						//if the background should be shown but was not set, the normal region is shown
						bool tmpShowBackground = showBackground && (tmp.XBackground != -1);

						//calculate the track position within the picture
						QPointF trackPictureCoords = positionManager->getDisplays().at (wlIndex).calcTransformedCoords (tmp.point(), &positionManager->positionInformation);

						if (tmpTrack == TTTManager::getInst().getCurrentTrack())
							currentTrackPosition = trackPictureCoords;

						float diameter = CELLCIRCLESIZE;
						if(bOverwriteTrackRadius) {
							diameter = overwriteRadius * 2;

							// No zoom desired, but ItemIgnoresTransformations cannot be used because then the tracks' positions would be wrong
							diameter /= currentZoomAccurate;
						}
						else if (/*multiPicViewer->circleMouseCursorUsed() & */(tmp.CellDiameter > 0.0f))
							diameter = tmp.CellDiameter;

						//set drawing pen, according the the current settings
						QColor circleColor = UserInfo::getInst().getStyleSheet().getCellColor();
						if(drawPrevTrackPoitns) {
							// Special effects
							circleColor = multiPicViewer->getSpecialEffectTrackColor(tmpTrack->getTrackNumber());
						}
						else
						{
							if (! allCellsUniform) {
								if (TTTManager::getInst().getCurrentTrack()) {
									//the active cell is painted in a different color from the rest
									if (tmpTrack->getNumber() == TTTManager::getInst().getCurrentTrack()->getNumber())
										circleColor = ACTIVE_CELL_COLOR;
									else if (tmpTrack->getNumber() == TTTManager::getInst().getCurrentTrack()->getNumber() / 2)
										circleColor = MOTHER_CELL_COLOR;
								}
							}
						}
						QPen pen (circleColor, UserInfo::getInst().getStyleSheet().getCellCircleThickness());

						//draw track circle, ...
						ClickableQGraphicsEllipseItem *elli = nullptr;
						if(showTracks) {
							// OH-140703: re-use items
							//elli = new ClickableQGraphicsEllipseItem (GRAPHICSDATA_USAGE_TRACKCIRCLE);
							//elli->setData (GRAPHICSDATA_USAGE, GRAPHICSDATA_USAGE_TRACKCIRCLE);
							elli = getClickableEllipseItem();

							elli->setRect (trackPictureCoords.x() - diameter / 2.0f, trackPictureCoords.y() - diameter / 2.0f, diameter, diameter);
							elli->setPen (pen);
							elli->setZValue (ZVALUE_TRACKCIRCLE);
							elli->setData (GRAPHICSDATA_CELLNUMBER, tmpTrack->getNumber());
							elli->setMousePressCallBackMethod (&PictureView::handleItemSelection);

							//if(bOverwriteTrackRadius)
							//	elli->setFlag(QGraphicsItem::ItemIgnoresTransformations);	// Ignore zoom

							scene()->addItem (elli);
						}

						//... its number ...
						if (showTrackNumbers) {

							// OH-140703: re-use items
							//QGraphicsTextItem *text = new QGraphicsTextItem (QString ("").setNum (tmpTrack->getNumber()));
							//text->setData (GRAPHICSDATA_USAGE, GRAPHICSDATA_USAGE_TRACKCIRCLE);
							QGraphicsSimpleTextItem *text = getQGraphicsTextItem(QString::number(tmpTrack->getNumber()));				
							QFont font ("Arial", UserInfo::getInst().getStyleSheet().getCellNumberFontSize());

							text->setPos (trackPictureCoords.x() + diameter / 2.0f, trackPictureCoords.y() - diameter / 2.0f);
							text->setFont (font);
							//text->setDefaultTextColor (UserInfo::getInst().getStyleSheet().getCellNumberColor());
							text->setBrush(UserInfo::getInst().getStyleSheet().getCellNumberColor());
							text->setZValue (ZVALUE_TRACKCIRCLE);
							
							text->setData (GRAPHICSDATA_CELLNUMBER, tmpTrack->getNumber());

							scene()->addItem (text);
						}

						//... and background track
						if (tmpShowBackground) {
							trackPictureCoords = positionManager->getDisplays().at (wlIndex).calcTransformedCoords (tmp.backgroundPoint(), &positionManager->positionInformation);
							pen = QPen (Qt::green);

							// OH-140703: re-use items
							//elli = new ClickableQGraphicsEllipseItem (GRAPHICSDATA_USAGE_TRACKCIRCLE);
							//elli->setData (GRAPHICSDATA_USAGE, GRAPHICSDATA_USAGE_TRACKCIRCLE);
							elli = getClickableEllipseItem();

							elli->setPen(pen);
							elli->setRect (trackPictureCoords.x() - diameter / 2.0f, trackPictureCoords.y() - diameter / 2.0f, diameter, diameter);
							elli->setZValue (ZVALUE_TRACKCIRCLE);
							elli->setData (GRAPHICSDATA_CELLNUMBER, tmpTrack->getNumber());
							elli->setMousePressCallBackMethod (&PictureView::handleItemSelection);

							//if(bOverwriteTrackRadius)
							//	elli->setFlag(QGraphicsItem::ItemIgnoresTransformations);	// Ignore zoom

							scene()->addItem (elli);

							if (showTrackNumbers) {
								// OH-140703: re-use items
								//QGraphicsTextItem *text = new QGraphicsTextItem (QString ("bg %1").arg (tmpTrack->getNumber()));
								//elli->setData (GRAPHICSDATA_USAGE, GRAPHICSDATA_USAGE_TRACKCIRCLE);
								QGraphicsSimpleTextItem *text = getQGraphicsTextItem(QString ("bg %1").arg (tmpTrack->getNumber()));
								QFont font ("Arial", UserInfo::getInst().getStyleSheet().getCellNumberFontSize());

								text->setPos (trackPictureCoords.x() + diameter / 2.0f, trackPictureCoords.y() - diameter / 2.0f);
								text->setFont (font);
								text->setBrush (Qt::green);
								text->setZValue (ZVALUE_TRACKCIRCLE);
								text->setData (GRAPHICSDATA_CELLNUMBER, tmpTrack->getNumber());

								scene()->addItem (text);
							}
						}
						// Draw prev tracks (tails)
						if(drawPrevTrackPoitns) {
							if(!drawPrevTrackPointsOnlyOfSelectedCellBranch)
								drawTails(trackPictureCoords, diameter, tmpTrack, &tmp, circleColor);
							else {
								// Draw only, of tmpTrack is selected track or one of its progenitors, using constant color
								circleColor = QColor(255, 255, 0);
								int trackNumber = TTTManager::getInst().getCurrentTrack() ? TTTManager::getInst().getCurrentTrack()->getTrackNumber() : -1;
								bool trackDrawn = false;
								while(trackNumber > 0) {
									if(tmpTrack->getTrackNumber() == trackNumber) {
										drawTails(trackPictureCoords, diameter, tmpTrack, &tmp, circleColor);
										trackDrawn = true;
										break;
									}
									trackNumber /= 2;
								}
								if(!trackDrawn)
									// Draw other tracks using gray
									drawTails(trackPictureCoords, diameter, tmpTrack, &tmp, QColor(255, 255, 255));
							}
						}
						// Draw segmentation
						if(segData) 
							drawSegmentationToTrack(segData.get(), segDataSizeX, segDataSizeY, tmpTrack, &tmp, circleColor);
					}

				}
			}
		}
	}

	// Draw "external tracks"
	bool useDifferentColours = UserInfo::getInst().getStyleSheet().getUseColorsForAllTracks();
	QColor singleTrackColor = Qt::white;
	if (positionManager->frmMovie && positionManager->frmMovie->getExternalTracksMode() != TTTMovie::OFF) {
		// Get external tracks display mode
		TTTMovie::DisplayExternalTracksMode mode = positionManager->frmMovie->getExternalTracksMode();

		int colonyIndex = 0;
		int colorIndex = 0;
		QPen pen;

		if (useDifferentColours)
			pen = QPen(ColonyColor [colorIndex], UserInfo::getInst().getStyleSheet().getCellCircleThickness());
		else
			pen = QPen(singleTrackColor, UserInfo::getInst().getStyleSheet().getCellCircleThickness());

		// DEBUG
		//qDebug() << "Drawing external track points for time point " << positionManager->getPictureIndex().TimePoint;
		//QSet<QString> treesNotIgnored;
		//bool foundTreeToIgnore = false;
		QString currrentlyOpenedTreeFileName;
		TTTPositionManager *baseTTTPM = TTTManager::getInst().getBasePositionManager();
		if(baseTTTPM)
			currrentlyOpenedTreeFileName = baseTTTPM->getBasename();

		QLinkedList<TrackPoint> externalTrackpoints = positionManager->getExternalTrackpoints(positionManager->getTimepoint());
		for (QLinkedList<TrackPoint>::const_iterator iter = externalTrackpoints.constBegin(); iter != externalTrackpoints.constEnd(); ++iter) {
				
				if ((*iter).X != -1) {
					const TrackPoint& tmp = *iter;
					ITrack* tmpTrack = tmp.getITrack();

					// Do not draw, if belongs to currently opened tree (already drawn above), but only in drawPrevTrackPoitns mode (users may still expect the other behavior)
					ITree* curTree = tmpTrack->getITree();
					if(drawPrevTrackPoitns && currrentlyOpenedTreeFileName.size() && (!curTree || curTree->getFilename().contains(currrentlyOpenedTreeFileName))) {
						//foundTreeToIgnore = true;
						continue;
					}
					//treesNotIgnored.insert(curTree->getFilename());

					QPointF trackPictureCoords = positionManager->getDisplays().at (wlIndex).calcTransformedCoords ((*iter).point(), &positionManager->positionInformation);

					float diameter = CELLCIRCLESIZE;
					if(bOverwriteTrackRadius) {
						diameter = overwriteRadius * 2;
						diameter /= currentZoomAccurate;
					}
					else if ((*iter).CellDiameter > 0.0f)
						diameter = (*iter).CellDiameter;

					// Draw track circle
					if(showTracks) {
						QGraphicsEllipseItem *elli = getQGraphicsEllipseItem();
			
						elli->setRect (trackPictureCoords.x() - diameter / 2.0f, trackPictureCoords.y() - diameter / 2.0f, diameter, diameter);
						elli->setPen (pen);
						elli->setZValue (ZVALUE_TRACKCIRCLE);

						//if(bOverwriteTrackRadius)
						//	elli->setFlag(QGraphicsItem::ItemIgnoresTransformations);	// Ignore zoom

						scene()->addItem (elli);
					}

					// Get description
					QString textString;
					if (showTrackNumbers) {
						if (mode == TTTMovie::FIRST_ALL_POS || mode == TTTMovie::FIRST_CUR_POS) {

							if ((*iter).tmpPositionIndex > 0)
								textString = QString ("Pos %1, Col %2, TP %3").arg ((*iter).tmpPositionIndex).arg ((*iter).tmpColonyNumber).arg ((*iter).TimePoint);
							else
								textString = QString ("Col %1, TP %2").arg ((*iter).tmpColonyNumber).arg ((*iter).TimePoint);
						}
						else {
							if ((*iter).tmpPositionIndex > 0)
								textString = QString ("Pos %1, Col %2, Cell %3").arg ((*iter).tmpPositionIndex).arg ((*iter).tmpColonyNumber).arg ((*iter).tmpCellNumber);
							else
								// display cell number within circle
								textString = QString ("Col %1, Cell %2").arg ((*iter).tmpColonyNumber).arg ((*iter).tmpCellNumber);
						}
					}
					if(!textString.isEmpty()) {
						// Draw description text
						// OH-140703: re-use items
						//QGraphicsTextItem *textItem = new QGraphicsTextItem (textString);
						//textItem->setData (GRAPHICSDATA_USAGE, GRAPHICSDATA_USAGE_TRACKCIRCLE);
						QGraphicsSimpleTextItem *textItem = getQGraphicsTextItem(textString);

						textItem->setPos (trackPictureCoords.x() + diameter / 2.0f, trackPictureCoords.y() - diameter / 2.0f);
						textItem->setBrush (Qt::green);
						textItem->setZValue (ZVALUE_TRACKCIRCLE);
						

						scene()->addItem (textItem);
					}

					// Draw prev tracks (tails)
					if(drawPrevTrackPoitns) {						
						if(!drawPrevTrackPointsOnlyOfSelectedCellBranch)
							drawTails(trackPictureCoords, diameter, tmpTrack, &tmp, pen.brush().color());
						else
							// Draw everything in gray, as it cannot be the currently selected cell
							drawTails(trackPictureCoords, diameter, tmpTrack, &tmp, QColor(255, 255, 255));
					}
				}
				else if ((*iter).TimePoint == -10)
					//special trackpoint that indicates a new colony
					colonyIndex++;
				if (useDifferentColours)
					pen = QPen (GraphicAid::mainColor (colonyIndex), UserInfo::getInst().getStyleSheet().getCellCircleThickness());
				else
					pen = QPen (singleTrackColor, UserInfo::getInst().getStyleSheet().getCellCircleThickness());
		}

		//// DEBUG
		//QList<QString> treesNotIgnored2 = treesNotIgnored.toList();
		//qSort(treesNotIgnored2);
		//for(auto it = treesNotIgnored2.begin(); it != treesNotIgnored2.end(); ++it)
		//	qDebug() << "Drawing track point of tree" << *it << " (current tree in cell editor: " << currrentlyOpenedTreeFileName << ")";
		//qDebug() << "Tree to ignore found: " << foundTreeToIgnore;
	}

	// Trees and TreeFragments from new TTTAutoTracking windows
	if(showTracks || drawPrevTrackPoitns) {
		TTTAutoTracking *frmAutoTracking = TTTManager::getInst().frmAutoTracking;
		QList<AutoTrackingCellCircle*> circles = frmAutoTracking->getCellCirclesForDisplay(currentTimepoint, positionManager, wlIndex, !showTrackNumbers);

		// Process circles and add them to scene
		int count = 0;
		for(QList<AutoTrackingCellCircle*>::const_iterator it = circles.constBegin(); it != circles.constEnd(); ++it) {
			AutoTrackingCellCircle* curCircle = *it;

			// Make PictureView::clearView() delete this item
			//curCircle->setData (GRAPHICSDATA_USAGE, GRAPHICSDATA_USAGE_TRACKCIRCLE);
			m_usedAutoTrackingCellCircles.push_back(curCircle);

			// Make everything yellow if previous trackpoints should be drawn
			QColor circleColor = QColor(255, 0, 0);
			if(drawPrevTrackPoitns) {
				// Use fragment number as track number to have different colors for every fragment
				int number;
				auto treeFragment = curCircle->getTreeFragment();
				if(treeFragment)
					number = treeFragment.toStrongRef()->getTreeFragmentNumber();
				else
					number = 1;
				circleColor = multiPicViewer->getSpecialEffectTrackColor(number);
				
				/** 
				* Hard coded special display of 2 tree fragments:
				* show two fragmetns in red and blue, used for export (4/2014)
				
				++count;
				if(count == 10)
					circleColor = Qt::red;
				else if(count == 20)
					circleColor = Qt::blue;
					*/

				curCircle->setColor(circleColor, circleColor);
			}
			
			// Set circle size if needed
			if(bOverwriteTrackRadius)
				curCircle->setRadius(overwriteRadius);

			// Add to scene
			if(showTracks)
				scene()->addItem(curCircle);

			// Draw prev tracks (tails)
			if(drawPrevTrackPoitns) {
				drawTails(curCircle->scenePos(), curCircle->getRadius()*2.0f, curCircle->getTrackPoint()->getITrack(), curCircle->getTrackPoint(), circleColor);
			}

			// Draw segmentation
			if(segData) 
				drawSegmentationToTrack(segData.get(), segDataSizeX, segDataSizeY, curCircle->getTrackPoint()->getITrack(), curCircle->getTrackPoint(), circleColor);
		}
	}
}

void PictureView::startTracking (bool _isInMainMovieWindow, bool _setBackground)
{
        if (_isInMainMovieWindow) {
                TTTManager::getInst().setKeyboardGrabber (this);
                TTTManager::getInst().getKeyboardGrabber()->grabKeyboard();		//makes this widget receive all keyboard events
        }

        mouseWidget = this;

        if (! multiPicViewer->circleMouseCursorUsed())
                setCursor (QCursor (Qt::CrossCursor));

        setBackground = _setBackground;
}

void PictureView::stopTracking ()
{
        if (TTTManager::getInst().getKeyboardGrabber()) {
                TTTManager::getInst().getKeyboardGrabber()->releaseKeyboard();
                TTTManager::getInst().setKeyboardGrabber (0);
        }

        if (! multiPicViewer->circleMouseCursorUsed())
                setCursor(QCursor (Qt::ArrowCursor));
}

QPointF PictureView::shift (QPointF _shift)
{
	//@todo
	//sometimes the shift for multiple wavelengths is not correct, especially
	// when the form is resized and the user also shifts via frmRegionSelection
	//=> shifting is synchronous, yet the pictures often have different positions
	
	
	//must also be executed for all non-visible frames
	// - if they are displayed, they have to be up to date
	//but for these the shift only needs to take place in the 
	// display settings and needs not cause a real picture shift!

//@todo
//	QRect disp = positionManager->getDisplays().at (Index).DisplayedRegion;
//	QRect loaded = positionManager->getDisplays().at (Index).LoadedRegion;
//
//	if (disp.left() + _shift.x() < loaded.left())
//		_shift.setX (loaded.left() - disp.left());
//	if (disp.right() + _shift.x() > loaded.right())
//		_shift.setX (loaded.right() - disp.right());
//	if (disp.top() + _shift.y() < loaded.top())
//		_shift.setY (loaded.top() - disp.top());
//	if (disp.bottom() + _shift.y() > loaded.bottom())
//		_shift.setY (loaded.bottom() - disp.bottom());
//
//	disp.moveBy ((int)_shift.x(), (int)_shift.y());
//
//	positionManager->getDisplays().setDisplayedRegion (Index, disp.left(), disp.top(), disp.width(), disp.height());
//
//	if (Show) {
//                draw();
//                drawAdditions();
//                //repaint();
//                update();
//	}
	
	return _shift;
}

void PictureView::setAllCellsUniform (bool _on)
{
        allCellsUniform = _on;
}

void PictureView::drawRadiusLine (QPointF _pos)
{
        if (! radiusSelectionMode)
		return;

//@todo
//        QPainter p (draw_img);
//	p.setPen (QPen (RadiusLineColor));
//        //p.setRasterOp (Qt::XorROP);
//        p.setCompositionMode (QPainter::RasterOp_SourceXorDestination);
//        p.drawLine (RadiusLineStartPos.toQPoint(), _pos.toQPoint());
//	p.end();
//
//        update();
}

//void PictureView::drawSymbol (Symbol _symbol, QPointF _pos)
//{
////        QPainter p (draw_img);
////	//problem: 	with gamma correction switched on, the symbols are hardly visible
////	//			but with CopyROP, they cannot be moved across the picture
////	//solution:	extra draw functions for dragging & final painting
////        //p.setRasterOp (Qt::CopyROP);
////        p.setCompositionMode (QPainter::CompositionMode_Source);
////        _symbol.draw (p, _pos, (int)CellRadius);
////	p.end();
////
////        update();
//
//}

//void PictureView::drawDragSymbol (Symbol _symbol, QPointF _pos)
//{
//@todo
//
//        QPainter p (draw_img);
//	//problem: 	with gamma correction switched on, the symbols are hardly visible
//	//			but with CopyROP, they cannot be moved across the picture
//	//solution:	extra draw functions for dragging & final painting
//        //p.setRasterOp (Qt::XorROP);
//        p.setCompositionMode (QPainter::RasterOp_SourceXorDestination);
//        _symbol.draw (p, _pos, (int)CellDiameter);
//	p.end();
//
//        update();
//}

void PictureView::drawCurrentSymbols()
{
        QPointF pos;
        bool display = false;

        //delete old symbols
        clearView (GRAPHICSDATA_USAGE, GRAPHICSDATA_USAGE_SYMBOL);

        QVector<Symbol> symbols = positionManager->getSymbolHandler().readSymbols (positionManager->getTimepoint(), wlIndex);

        for (	QVector<Symbol>::Iterator iter = symbols.begin(); iter != symbols.end(); ++iter) {

                if (( (iter->getPosition().x() != -1) | (iter->getTrack() != 0)) &               //position or associated track set
                        (iter->getFirstTimePoint() <= positionManager->getTimepoint()) &        //first display timepoint...
                        (iter->getLastTimePoint() >= positionManager->getTimepoint()) &         //...and last display timepoint are fine
                        (iter->getSize() > 0)) {                                                //not an empty symbol

                        display = true;
                        if (iter->getTrack())
                                if (! iter->getTrack()->aliveAtTimePoint (positionManager->getTimepoint()))
                                        display = false;

                        if (display) {
                                pos = positionManager->getDisplays().at (wlIndex).calcTransformedCoords (iter->getPosition(), &positionManager->positionInformation);

                                int cellDiameter = CELLCIRCLESIZE;
                                if (iter->getTrack()) {
                                        TrackPoint tp = iter->getTrack()->getTrackPoint (positionManager->getTimepoint());
                                        pos = positionManager->getDisplays().at (wlIndex).calcTransformedCoords (tp.point(), &positionManager->positionInformation);
                                        cellDiameter = tp.CellDiameter;
                                }

                                ClickableQGraphicsPolygonItem *symbi = new ClickableQGraphicsPolygonItem (*(iter->createPolygonItem (cellDiameter)), GRAPHICSDATA_USAGE_SYMBOL);

                                //set missing attributes (not known to Symbol)

                                //place at (pos, (int)CellRadius);
                                symbi->setPos (pos);
                                symbi->setZValue (ZVALUE_SYMBOL);
                                symbi->setData (GRAPHICSDATA_USAGE, GRAPHICSDATA_USAGE_SYMBOL);
                                symbi->setData (GRAPHICSDATA_SYMBOL_INDEX, QString ("%1").arg (iter->getIndex()));
                                if (iter->getTrack())
                                        symbi->setData (GRAPHICSDATA_CELLNUMBER, iter->getTrack()->getNumber());
                                symbi->setMousePressCallBackMethod (&PictureView::handleItemSelection);
                                scene()->addItem (symbi);
                        }
                }
        }
}

void PictureView::setMouseWidget (PictureView *_mouseWidget)
{
        mouseWidget = _mouseWidget;
}

void PictureView::showColocationRadius (int _radius)
{
	if (! TTTManager::getInst().getCurrentTrack())
		return;
	
	//note: _radius, not diameter!
	
//@todo
//	if (CurrentColocationRadius) {
//		//delete old circle
//		drawCircle (CurrentTrackPosition, CurrentColocationRadius * 2, ColocationCircleColor);
//	}
//
//	//draw circle
//	drawCircle (CurrentTrackPosition, _radius * 2, ColocationCircleColor);
//
//	CurrentColocationRadius = _radius;
}

//void PictureView::drawCircle (QPointF _pos, float _radius, QColor _color, QPainter::CompositionMode _rop)
//{
//        if (! isShown)
//		return;
//
//@todo
//
//        QPainter p (draw_img);
//	p.setPen (QPen (_color));
//        //p.setRasterOp (_rop);
//        p.setCompositionMode (_rop);
//        //std::cout << _radius << std::endl;
//	p.drawEllipse ((int)(_pos.x() - _radius / 2.0f), (int)(_pos.y() - _radius / 2.0f), (int)_radius, (int)_radius);
//	p.end();
//
//        update();
//}

//void PictureView::drawIndex (bool _draw)
//{
//	drawWavelengthIndex = _draw;
//
//        if (_draw)
//                wavelengthBox->show();
//        else
//                wavelengthBox->hide();
//}

//void PictureView::setCurrentCursor (bool _tracking)
//{
//	if (_tracking) {
//                if (multiPicViewer->circleMouseCursorUsed() & (TTTManager::getInst().isTracking())) {
//			setCursor (Qt::BlankCursor);
//			//drawMouseCircle (mapFromGlobal (QCursor::pos()));
//		}
//		else
//			setCursor (Qt::CrossCursor);
//	}
//	else
//		setCursor (Qt::ArrowCursor);
//}

//@todo ???
//void PictureView::drawCircles (Q3ValueVector<QPointF> _circles, float _radius, QColor _color, QPainter::CompositionMode _rop, bool _connect, bool _store)
//{
//	QPointF pos (0, 0);
//	QPointF posReal (0, 0);
//	QPointF posRealOld (0, 0);
//
//        //BS QPainter
//        //return;
//
//        QPainter p (draw_img);
//	p.setPen (QPen (_color));
//        //p.setRasterOp (_rop);
//        p.setCompositionMode (_rop);
//
//	//take one third of the radius as line thickness
//	int line_thickness = (int)(_radius / 3.0f);
//	//_radius = 3;
//
//	if (_store)
//		internalCircles.clear();
//
//        for (int i = 0; i < _circles.size(); i++) {
//		if (! _circles[i].isNull()) {
//			//real coordinates (mapped to viewport)
//			posReal = positionManager->getDisplays().at (Index).calcTransformedCoords (_circles[i], &positionManager->positionInformation);
//
//			p.drawEllipse ((int)(posReal.x() - _radius / 2.0f), (int)(posReal.y() - _radius / 2.0f), (int)_radius, (int)_radius);
//			//drawCircle (_circles[i], _radius, _color, _rop);
//			if (_connect && ! pos.isNull()) {
//				//draw line to connect this circle with its precessor
//				//current coordinates are in posReal, former coordinates are in posRealOld
//				p.setPen (QPen (_color, line_thickness, Qt::DotLine));
//				p.drawLine (posRealOld.toQPoint(), posReal.toQPoint());
//				p.setPen (QPen (_color, 1));
//			}
//
//			pos = _circles[i];		//picture global coordinates
//			posRealOld = positionManager->getDisplays().at (Index).calcTransformedCoords (pos, &positionManager->positionInformation);
//
//			if (_store) {
//				//note: the coordinates are stored in global picture coordinates, so with every drawing, they have to be mapped to the current viewport
//				MyCircle circ ((int)pos.x(), (int)pos.y(), (int)_radius, _color, _connect);
//				internalCircles.push_back (circ);
//			}
//		}
//		else {
//			pos = QPointF();
//			//_color = GraphicAid::mainColor (KApplication::random() % 13);
//		}
//	}
//
//	p.end();
//
//        update();
//}

void PictureView::drawCircles (QVector<MyCircle> _circles) //, QPainter::CompositionMode _compMode)
{
        QPointF pos (0, 0);
        QPointF posOld (0, 0);

        for (int i = 0; i < _circles.size(); i++) {
                if (! _circles[i].isNull()) {
                        //map (picture global) pos coordinates to current viewport
                        pos = positionManager->getDisplays().at (wlIndex).calcTransformedCoords (_circles[i].getMiddle(), &positionManager->positionInformation);

						// OH-140703: re-use items
						ClickableQGraphicsEllipseItem *elli;
						if(_circles[i].getUsageParameter() == GRAPHICSDATA_USAGE_TRACKCIRCLE)
							elli = getClickableEllipseItem();
						else
							elli = new ClickableQGraphicsEllipseItem (_circles[i].getUsageParameter());

                        elli->setRect (pos.x() - _circles[i].getRadius() / 2.0f, pos.y() - _circles[i].getRadius() / 2.0f, _circles[i].getRadius(), _circles[i].getRadius());
                        elli->setPen (_circles[i].getColor());
                        elli->setZValue (ZVALUE_CELLPATH);
                        elli->setData (GRAPHICSDATA_USAGE, _circles[i].getUsageParameter());

                        if (_circles[i].getUsageParameter() == GRAPHICSDATA_USAGE_SELECTED_COLONY_START) {
                                //special case... see MultiplePictureViewer::handlePictureMouseRelease() for details)
                                elli->setData (GRAPHICSDATA_COLONY_START_INDEX, _circles[i].getConnectLineThickness());
                        }

                        elli->setMousePressCallBackMethod (&PictureView::handleItemSelection);
                        scene()->addItem (elli);

                        if ((_circles[i].getConnect()) && (! posOld.isNull())) {
                                //draw line to connect this circle with its precessor
                                //the current coordinates are in pos, the former ones are in posOld
                                QGraphicsLineItem *line = new QGraphicsLineItem();
                                line->setPen (QPen (_circles[i].getColor(), _circles [i].getConnectLineThickness(), Qt::DotLine));
                                line->setLine (posOld.x(), posOld.y(), pos.x(), pos.y());
                                line->setZValue (ZVALUE_CELLPATH);
                                line->setData (GRAPHICSDATA_USAGE, _circles[i].getUsageParameter());
                                scene()->addItem (line);
                        }

                        if (_circles [i].getTimeStamp() > 0) {
                                //print the integer stored in the time stamp attribute
                                QGraphicsSimpleTextItem *text = getQGraphicsTextItem(QString::number (_circles [i].getTimeStamp()));
                                text->setFont (QFont ("Courier New", 10));
                                text->setBrush (_circles[i].getColor());
                                text->setPos (pos);
                                text->setZValue (ZVALUE_CELLPATH);
                                text->setData (GRAPHICSDATA_USAGE, _circles[i].getUsageParameter());
                                scene()->addItem (text);
                        }

                        posOld = pos; //positionManager->getDisplays().at (Index).calcTransformedCoords (_circles[i].getMiddle(), &positionManager->positionInformation);

//			if (_store)
//				internalCircles.push_back (_circles[i]);
                }
                else
                        //force breaking the connections
                        posOld = QPointF();
        }
}

//void PictureView::setToTimepoint (int, int)
//{
//	internalCircles.clear();
//}

void PictureView::startSingleBackgroundSelectionMode()
{
	singleBackgroundSelectionMode = true;
	setCursor (QCursor (Qt::CrossCursor));		//show cross pointer
}

void PictureView::setZoom (int _zoomPercent, float _zoomX, float _zoomY)
{
        if (_zoomPercent != currentZoom) {
                if (_zoomPercent == 0) {
						// -------- Oliver --------
						// Scene->size() returns size without zoom
						qreal sceneWidth = scene()->width() * currentZoomAccurate,
							sceneHeight = scene()->height() * currentZoomAccurate;


						//full picture
                        qreal factorx = width() / sceneWidth;		// 
                        qreal factory = height() / sceneHeight;


						//// DEBUG
						//if(wlIndex == 0)
						//	qDebug() << width() << " - " << height();

                        ////full picture
                        //qreal factorx = width() / scene()->width();		// 
                        //qreal factory = height() / scene()->height();

                        qreal factor = qMin (factorx, factory);
                        scale (factor, factor);

                        currentZoom = 0;
						currentZoomAccurate *= factor;
                }
                else {
                        //normal zoom

                        qreal factor = 0;
                        if (currentZoom > 0)
                                factor = (float)_zoomPercent / (float)currentZoom;
                        else {
                                factor = 1.0 / matrix().m11();                        //m11 = horizontal scaling; vertical is the same as we performed everything synchronously
                                factor *= (float)_zoomPercent / 100.0f;
                        }

                        if ((Tools::floatEquals (_zoomX, -1.0f, 1) != 0) && (Tools::floatEquals (_zoomY, 1.0f, 1) != 0)) {

								// Disable any anchor, centering on mouse is done manually, so we do not confuse qt
								// because we have multiple wavelength windows
                                setTransformationAnchor (QGraphicsView::NoAnchor);

								// Transform mouse viewport pixel coordinates to scene units
								QPointF sceneCenter = mapToScene(QPoint(_zoomX, _zoomY));

								// Zoom
                                scale (factor, factor);

								// Center on position where mouse was before zooming
								centerOn(sceneCenter);

								
								//centerOn(QPointF(_zoomX, _zoomY));
								//centerOn(QPointF(_zoomX, _zoomY));
        //@todo center on _zoomX/Y if provided, just as in google maps...
        //						  QRect dispRegion = positionManager->getDisplays().getDisplayedRegion();
        //
        //                        float old_width = dispRegion.width();
        //                        float old_height = dispRegion.height();
        //
        //                        //the selected region has to be set according to the zoom factor
        //                        if (Zoom == 100) {
        //                                //nothing to calculate -> faster
        //                                dispRegion.setWidth (PictureFrames.find (wavelength)->width());
        //                                dispRegion.setHeight (PictureFrames.find (wavelength)->height());
        //                        }
        //                        else {
        //                                dispRegion.setWidth ((int)(PictureFrames.find (wavelength)->width() * (100.0f / (float)Zoom)));
        //                                dispRegion.setHeight ((int)(PictureFrames.find (wavelength)->height() * (100.0f / (float)Zoom)));
        //                        }
        //
        //                        //the zoom box has to be positioned such that the object under the mouse before zooming
        //                        // is exactly there after zooming as well! (_zoomX/_zoomY usually is the mouse position)
        //                        //the repositioning is performed with shifting the topleft corner of the displayed region
        //                        //(coords are always in pixel)
        //
        //
        //                        QPointF currentCenter = positionManager->getDisplays().at (wavelength).calcCenter();
        //
        //                        if (_zoomX == -1)
        //                                _zoomX = (float)(currentCenter.x() - dispRegion.left());
        //                        else
        //                                _zoomX = _zoomX * 100.0f / (float)oldZoom;
        //
        //                        if (_zoomY == -1)
        //                                _zoomY = (float)(currentCenter.y() - dispRegion.top());
        //                        else
        //                                _zoomY = _zoomY * 100.0f / (float)oldZoom;
        //
        //
        //                        //width/height of old and new clip/visible region; resizing is already done
        //                        float new_width = dispRegion.width();
        //                        float new_height = dispRegion.height();
        //
        //                        float rel_x = new_width / old_width;
        //                        float rel_y = new_height / old_height;
        //
        //                        if (_zoomPercent > oldZoom) {
        //                                //zooming was inward (zoom factor increased)
        //                                //new displayed region must be completely within the old displayed region
        //
        //                                float shift_left = (_zoomX - rel_x * _zoomX);
        //                                float shift_top = (_zoomY - rel_y * _zoomY);
        //
        //                                dispRegion.moveBy ((int)shift_left, (int)shift_top);
        //                        }
        //                        else {
        //                                //zooming was outward (zoom factor decreased)
        //                                //note: rel_x/y is now > 1!
        //
        //                                float shift_left = _zoomX - rel_x * _zoomX;
        //                                float shift_top = _zoomY - rel_y * _zoomY;
        //
        //                                //shift box while regarding the picture/loading bounds
        //                                QSize picSize = positionManager->getDisplays().at (wavelength).LoadedRegion.size();
        //
        //
        //                                int shiftx = (int)shift_left;
        //                                int shifty = (int)shift_top;
        //
        //                                if (! (dispRegion.width() > picSize.width())) {
        //                                        if (dispRegion.right() + shiftx > picSize.width())
        //                                                shiftx = picSize.width() - dispRegion.right();
        //                                        if (dispRegion.left() + shiftx < 0)
        //                                                shiftx = 0 - dispRegion.left();
        //                                }
        //                                else {
        //                                        //leave position unchanged
        //                                        shiftx = 0;
        //                                }
        //                                if (! (dispRegion.height() > picSize.height())) {
        //                                        if (dispRegion.bottom() + shifty > picSize.height())
        //                                                shifty = picSize.height() - dispRegion.bottom();
        //                                        if (dispRegion.top() + shifty < 0)
        //                                                shifty = 0 - dispRegion.top();
        //                                }
        //                                else {
        //                                        //leave position unchanged
        //                                        shifty = 0;
        //                                }
        //
        //                                dispRegion.moveBy (shiftx, shifty);
        //                        }
                        }
                        else {
                                //zoom into the center, and ensure that the current center is visible after zooming as well

                                setTransformationAnchor (QGraphicsView::AnchorViewCenter);
                                //setTransformationAnchor (QGraphicsView::NoAnchor);
								//setTransformationAnchor( QGraphicsView::AnchorUnderMouse );
                                scale (factor, factor);
                        }

                        currentZoom = _zoomPercent;
						currentZoomAccurate = ((double)_zoomPercent) / 100.0;

						// Redraw tracks if track circle should be fixed (ignore zoom)
						if(multiPicViewer->overwriteTrackRadiusForDisplay()) 
							drawTracks();
                }

//@todo adjust region window
//                if ((Zoom > 0) & (Zoom <= 1000)) {
//                        if (positionManager->frmRegionSelection) {
//                                positionManager->frmRegionSelection->getCurrentBox()->setBox (dispRegion);
//                                positionManager->frmRegionSelection->drawBox();
//               {         }
//
//                        positionManager->getDisplays().setDisplayedRegion (-1, dispRegion); //ZoomBox.left(), ZoomBox.top(), ZoomBox.width(), ZoomBox.height());
//                }


        }
}

void PictureView::createBasicElements()
{
        //orientation box
        orientationBox = new QGraphicsRectItem (0, 0, 20, 20);
        orientationBox->setPen (QPen (Qt::magenta, 2));
        orientationBox->hide();
        orientationBox->setZValue (ZVALUE_ORIENTATION_BOX);
		orientationBox->setFlag(QGraphicsItem::ItemIgnoresTransformations);	// Disable zooming of orientation box
        scene()->addItem (orientationBox);

        //wavelength box
        wavelengthBox = new QGraphicsSimpleTextItem ("xxx");
        wavelengthBox->setZValue (ZVALUE_WAVELENGTH_BOX);

		if(wlIndex == MAX_WAVE_LENGTH + 1)
			// This pictureview is for overlay
			wavelengthBox->setText ("ov");
		else {
			// Check if comment or number should be used
			if(!UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TTTMOVIE_USECOMMENTASWL).toBool()) 
				// Use wl index
				wavelengthBox->setText (QString ("%1").arg (wlIndex));
			else {
				// Use comment (if set)
				QString comment = TATInformation::getInst()->getWavelengthInfo(wlIndex).getComment();
				if(!comment.isEmpty()) 
					wavelengthBox->setText (comment);
				else
					wavelengthBox->setText (QString ("%1").arg (wlIndex));
			}
		}

        QFont sa ("Arial", UserInfo::getInst().getStyleSheet().getWavelengthBoxFontSize(), 3);
        wavelengthBox->setFont (sa);
        wavelengthBox->setBrush (UserInfo::getInst().getStyleSheet().getWavelengthBoxColor());
        wavelengthBox->setPos (0, 0);
		wavelengthBox->setFlag(QGraphicsItem::ItemIgnoresTransformations);	// Disable zooming of wavlength box
        scene()->addItem (wavelengthBox);
        wavelengthBox->show();

		//out of sync box
		outOfSyncBox = new QGraphicsRectItem(OUT_OF_SYNC_DISTANCE_LEFT,OUT_OF_SYNC_DISTANCE_TOP,10,10);
		outOfSyncBox->setZValue (ZVALUE_OUTOFSYNC_BOX);
		outOfSyncBox->setFlag(QGraphicsItem::ItemIgnoresTransformations);	// Disable zooming of wavlength box
		outOfSyncBox->setBrush(Qt::red);
		outOfSyncBox->setToolTip("Picture is out of sync (you can disable this indicator in options -> movie layout)");
		scene()->addItem (outOfSyncBox);
		

        //placeholder rectangle
        //the true size is determined in drawPicture()
        placeHolderRect = new QGraphicsRectItem (0, 0, 100, 100);
        placeHolderRect->hide();
        placeHolderRect->setZValue (0);
        scene()->addItem (placeHolderRect);
}

void PictureView::clearView (int _dataKey, QString _dataValue)
{
	//// DEBUG
	//QElapsedTimer tumdidum;
	//tumdidum.start();
	//int dbgTimeRemoveItems = 0;
	//int dbgTimeDeletes = 0;

	// OH-140703: recycle items
	if(_dataValue == GRAPHICSDATA_USAGE_TRACKCIRCLE) {
		// Ellipse items
		for(int i = 0; i < m_usedEllipseItemsStack.size(); ++i) 
			scene()->removeItem (m_usedEllipseItemsStack[i]);
		m_freeEllipseItemsStack.append(m_usedEllipseItemsStack);
		m_usedEllipseItemsStack.clear();

		// Line items
		for(int i = 0; i < m_usedLineItems.size(); ++i) 
			scene()->removeItem (m_usedLineItems[i]);
		m_freeLineItems.append(m_usedLineItems);
		m_usedLineItems.clear();

		// Text items
		for(int i = 0; i < m_usedTextItems.size(); ++i) 
			scene()->removeItem (m_usedTextItems[i]);
		m_freeTextItems.append(m_usedTextItems);
		m_usedTextItems.clear();

		// QGraphicsEllipseItem
		for(int i = 0; i < m_usedQGraphicsEllipseItems.size(); ++i) 
			scene()->removeItem (m_usedQGraphicsEllipseItems[i]);
		m_freeQGraphicsEllipseItems.append(m_usedQGraphicsEllipseItems);
		m_usedQGraphicsEllipseItems.clear();

		// AutoTrackingCellCircle
		for(int i = 0; i < m_usedAutoTrackingCellCircles.size(); ++i) {
			scene()->removeItem (m_usedAutoTrackingCellCircles[i]);
			delete m_usedAutoTrackingCellCircles[i];
		}
		m_usedAutoTrackingCellCircles.clear();

		return;
	}

	//get all currently available items
	const QList<QGraphicsItem *> its = items();

	if (_dataKey >= 0) {
		foreach (QGraphicsItem *item,  its) {
			if (item->data (_dataKey) == _dataValue) {
				// DEBUG
				//int tmp = tumdidum.elapsed();
				scene()->removeItem (item);
				//dbgTimeRemoveItems += tumdidum.elapsed() - tmp;

				//tmp = tumdidum.elapsed();
				delete item;		// This statement takes quite some time, but elimination would in sum not improve runtime really notably
				//dbgTimeDeletes += tumdidum.elapsed() - tmp;
			}
		}
	}
	else {
		//note: deletes all items from memory
		scene()->clear();
	}

	//update pointers to 0 (those without heap allocation)
	if (_dataValue == GRAPHICSDATA_USAGE_PICTURE)
		picture = 0;

	//qDebug() << QString("Total: %1 ms (items: %2, _dataValue: %3, removeItems: %4 ms, deletes: %5 ms)").arg(tumdidum.elapsed()).arg(its.size()).arg(_dataValue).arg(dbgTimeRemoveItems).arg(dbgTimeDeletes);
}

void PictureView::setOrientationBoxVisible (bool _visible)
{
	if (! orientationBox)
		return;

	if (_visible) {
		orientationBox->show();
	}
	else
		orientationBox->hide();
}

void PictureView::setOrientationBoxPosition (QPointF _pos)
{
	if (orientationBox)
		orientationBox->setPos (_pos);
}

void PictureView::setWavelengthBoxVisible (bool _visible)
{
	if (! wavelengthBox)
		return;

	if (_visible) {
		wavelengthBox->show();
	}
	else
		wavelengthBox->hide();
}

void PictureView::scrollContentsBy (int _dx, int _dy)
{
        //without this line, the contents would not be updated
        this->QGraphicsView::scrollContentsBy (_dx, _dy);

        //keep wavelength index and out of sync box visible in the top left corner
		const QPointF topLeft = getCurrrentlyVisibleRegion().topLeft();
        wavelengthBox->setPos (topLeft);
		outOfSyncBox->setPos(topLeft.x() + OUT_OF_SYNC_DISTANCE_LEFT, topLeft.y() + OUT_OF_SYNC_DISTANCE_TOP);

        //shift all other picture views as well -> done externally
        //emit scrolled (wlIndex, _dx, _dy);
		emit scrolled (wlIndex, horizontalScrollBar()->value(), verticalScrollBar()->value());
}

QRectF PictureView::getCurrrentlyVisibleRegion() const
{
        //the clue to receive the currently visible area is to map the widgets bounds to the scene

        QPointF topLeft = mapToScene (0, 0);
        QPointF bottomRight = mapToScene (this->width() - 10, this->height() - 10);

        //limit the range: when no picture is loaded yet, the ranges are not fine
        if (topLeft.x() < 0)
                topLeft.setX (0);
        if (topLeft.y() < 0)
                topLeft.setY (0);

        return QRectF (topLeft, bottomRight);
}

void PictureView::drawPicture (const PictureContainer *_picContainer, bool _overlay)
{
	/**
	 * OH 27.03.2015: the whole way in which images are being displayed (including the code
	 * of this function) is messy and should be re-designed at some point to make maintenance
	 * easier in the future.
	 */

    if (! positionManager->isInitialized())
            return;

    if (_picContainer) {
		// We have a picture, so make sure out of sync box is hidden
		outOfSyncBox->hide();

		// Update current timepoint
		currentTimepoint = _picContainer->getTimePoint();

		scaleX = _picContainer->getScalingFactorX();
		scaleY = _picContainer->getScalingFactorY();
        if (! _overlay) {
			// Get image and adjust graphics
			pictureData = _picContainer->getImage();
			if(pictureData.data) {
				QImage imageDisplay = GraphicAid::adjustImageGraphicsForDisplay(pictureData, positionManager->getDisplays().at (wlIndex));
				QPixmap pix = QPixmap::fromImage (imageDisplay);
				if (! pix.isNull()) {

					// Init QGraphicsView picture item
					picture = new QGraphicsPixmapItem (pix);
					picture->setZValue (ZVALUE_BACKGROUND_PICTURE);
					picture->setData (GRAPHICSDATA_USAGE, GRAPHICSDATA_USAGE_PICTURE);

					// Set scaling
					picture->setTransform(QTransform::fromScale(scaleX, scaleY), true);
					scene()->addItem (picture);

					// Save pic size
					picGeometry = pix.rect();
				}
			}
        }
        else {
			// Overlay
	
			// In overlay scaling cannot be set via picture->setTransform as pictures of different
			// wls could have different scaling factors so we need to scale at this point

            if (! picture) {
				// Get the first picture for overlay
				pictureData = _picContainer->getImage();

				// Set alpha and so on
				QImage img = GraphicAid::adjustImageGraphicsForDisplay (pictureData, positionManager->getDisplays().at (_picContainer->getWaveLength()));

				// Scale if neccessary
				if(abs(scaleX - 1.0) > 0.001 || abs(scaleY - 1.0) > 0.001) {
					// Scale
					img = img.scaled(img.width() * scaleX, img.height() * scaleY);
					scaleX = scaleY = 1.0;
				}

				// Set alpha and so on
				//GraphicAid::adjustImageGraphics (&img, positionManager->getDisplays().at (wlIndex));
					
                    
				QPixmap pix = QPixmap::fromImage (img);
                if (! pix.isNull()) {
                    //pix.setAlphaChannel(

					//if(!picture) {
						picture = new QGraphicsPixmapItem (pix);
						picture->setZValue (ZVALUE_BACKGROUND_PICTURE);
						picture->setData (GRAPHICSDATA_USAGE, GRAPHICSDATA_USAGE_PICTURE);

						//// Set scaling
						//picture->setTransform(QTransform::fromScale(scaleX, scaleY), true);

						scene()->addItem (picture);
					//}
					//else
					//	picture->setPixmap(pix);

                    picGeometry = pix.rect();
                }

            }
            else {
				const cv::Mat& overlayImage = _picContainer->getImage();
				
				QImage img2draw = GraphicAid::adjustImageGraphicsForDisplay (overlayImage, positionManager->getDisplays().at (_picContainer->getWaveLength()));
				
				

				// Scale if neccessary
				if(abs(scaleX - 1.0) > 0.001 || abs(scaleY - 1.0) > 0.001) {
					// Scale
					img2draw = img2draw.scaled(img2draw.width() * scaleX, img2draw.height() * scaleY);
					scaleX = scaleY = 1.0;
				}

                QImage imgBase = picture->pixmap().toImage();

                clearView (GRAPHICSDATA_USAGE, GRAPHICSDATA_USAGE_PICTURE);
                GraphicAid::overlay (&imgBase, &img2draw, true);

				//if(!picture) {
					picture = new QGraphicsPixmapItem (QPixmap::fromImage (imgBase));
					picture->setZValue (ZVALUE_BACKGROUND_PICTURE);
					picture->setData (GRAPHICSDATA_USAGE, GRAPHICSDATA_USAGE_PICTURE);

					//// Set scaling, if scaling is != 1.0
					//picture->setTransform(QTransform::fromScale(scaleX, scaleY), true);

					scene()->addItem (picture);
				//}
				//else {
				//	picture->setPixmap(QPixmap::fromImage (imgBase));
				//}
            }
        }

		// Check if we have a picGeometry that makes sense
		if(picGeometry.bottom() > 0 && picGeometry.right() > 0) {
			// Get area of scene that is visible
			//QRectF sceneArea = sceneRect ();
			QRectF sceneArea(picGeometry);

			// Additional space required for "map of positions" buttons
			int addSpace = MovieNextPosItem::WIDTH + 2*MovieNextPosItem::BAR_DISTANCE_PIC;

			// Mind scaling 
			sceneArea.setBottom(sceneArea.bottom() * scaleY + addSpace);
			sceneArea.setRight(sceneArea.right() * scaleX + addSpace);

			// Make sure no part of the scene left from or above the picture is rendered (that means, 
			// make sure, that no white bar appears left from or above the picture)
			sceneArea.setTopLeft(QPointF(-addSpace, -addSpace));

			// Just to make sure, set it in scene and in view
			setSceneRect(sceneArea);
			scene()->setSceneRect(sceneArea);
		}
    }
    else {
		// We have no picture for this timepoint/WL, so show out of sync box, if enabled
		if(UserInfo::getInst().getStyleSheet().getShowOutOfSyncBox() && !multiPicViewer->getHideOutOfSyncBox())
			outOfSyncBox->show();
		else 
			outOfSyncBox->hide();

        if ((! pictureLoaded()) && (! placeHolderSet)) {
            //scene is still empty, no picture ever loaded
            //=> add a rectangle with the same size as the pictures to enable concurrent scrolling even without a loaded picture

            if (placeHolderRect) {
                QRect imageRect = positionManager->positionInformation.getImageRects (0);
                placeHolderRect->setRect (0, 0, imageRect.width(), imageRect.height());
                placeHolderSet = true;
            }
        }
		else {
			if (picture && pictureData.data) {
				//there was already a picture loaded before - adjust its graphics only

				QImage img = GraphicAid::adjustImageGraphicsForDisplay (pictureData, positionManager->getDisplays().at (wlIndex));
				QPixmap pix = QPixmap::fromImage (img);
				if (! pix.isNull()) {
					clearView (GRAPHICSDATA_USAGE, GRAPHICSDATA_USAGE_PICTURE);

					//if(!picture) {
						picture = new QGraphicsPixmapItem (pix);
						picture->setZValue (ZVALUE_BACKGROUND_PICTURE);
						picture->setData (GRAPHICSDATA_USAGE, GRAPHICSDATA_USAGE_PICTURE);

						// Set scaling, if scale factor is != 1.0
						//if(abs(scaling - 1.0) > 0.001) 
						//	picture->setScale(scaling);
						picture->setTransform(QTransform::fromScale(scaleX, scaleY), true);

						scene()->addItem (picture);
					//}
					//else
					//	picture->setPixmap(pix);
				}
			}
		}
    }

	//// Make sure, we get a paint event and wait for it
	//update();
	//waitingForPaintEvent = true;
}

void PictureView::setShowTracks (bool _showTracks)
{
        if (showTracks == _showTracks)
                return;

        showTracks = _showTracks;

        drawTracks();
}

void PictureView::setShowTrackNumbers (bool _showTrackNumbers)
{
        if (showTrackNumbers == _showTrackNumbers)
                return;

        showTrackNumbers = _showTrackNumbers;

        drawTracks();
}

void PictureView::centerAbsolutePosition (QPointF _position)
{
        //transform to picture coordinates
        _position = positionManager->getDisplays().at (wlIndex).calcTransformedCoords (_position, &positionManager->positionInformation);

        centerOn (_position);
}

void PictureView::handleItemSelection (const QString &_parameter, void *_item, QMouseEvent *_ev)
{

	if (! _item)
		return;

	QGraphicsItem *item = (QGraphicsItem *)_item;

	//find out the PictureView object which originally triggered this call (the view in which the item was selected)
	//the result is not really a PictureView instance as additional attributes are random - but does not matter for sending (most) signals
	PictureView *pv = (PictureView*)(item->scene()->views().first());

	if (_parameter == GRAPHICSDATA_USAGE_TRACKCIRCLE) {
		//the user can choose a cell with clicking into it
		//(only available when not in tracking mode (otherwise side effects are not controllable))
		//(note: as described in drawTracks(), a click on the background track or cell number has the same effect)
		if ((! TTTManager::getInst().isTracking()) && (! TTTManager::getInst().getCurrentTracks()->isEmpty())) {
			if (item->data (GRAPHICSDATA_USAGE) == GRAPHICSDATA_USAGE_TRACKCIRCLE) {
				//a track (or attached item) is found
				int cellNumber = item->data (GRAPHICSDATA_CELLNUMBER).toInt();
				Track *track = TTTManager::getInst().getTree()->getTrack (cellNumber);

				emit pv->trackSelected (track, false, true);
			}
		}
	}
	else if (_parameter == GRAPHICSDATA_USAGE_SYMBOL) {
		//the user selected a symbol - if right-clicked, delete it

		if (_ev->button() == Qt::RightButton) {
			int symbolIndex = item->data (GRAPHICSDATA_SYMBOL_INDEX).toInt();
			emit pv->symbolDeleteDemand (-1, symbolIndex);
		}
	}
	else if (_parameter == GRAPHICSDATA_USAGE_SELECTED_COLONY_START) {
		if (_ev->button() == Qt::RightButton) {
			//delete this colony start
			int index = item->data (GRAPHICSDATA_COLONY_START_INDEX).toInt();
			emit pv->colonyStartDeleteDemand (index);
		}
	}

}

bool PictureView::pictureLoaded() const
{
        return (picture != 0);
}

void PictureView::applyStyleSheet (const StyleSheet &_ss)
{
    QFont sa ("Arial", _ss.getWavelengthBoxFontSize(), 3);
    wavelengthBox->setFont (sa);
    wavelengthBox->setBrush (_ss.getWavelengthBoxColor());

	// Check if comment or number should be used
	if(!UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TTTMOVIE_USECOMMENTASWL).toBool()) 
		// Use wl index
		wavelengthBox->setText (QString ("%1").arg (wlIndex));
	else {
		// Use comment (if set)
		QString comment = TATInformation::getInst()->getWavelengthInfo(wlIndex).getComment();
		if(!comment.isEmpty()) 
			wavelengthBox->setText (comment);
		else
			wavelengthBox->setText (QString ("%1").arg (wlIndex));
	}
}

void PictureView::debugTest()
{
	//if(!TATInformation::getInst()->wavelengthInformationExistsFor(wlIndex))
	//	return;
	//
	//float mmpp = TATInformation::getInst()->getWavelengthInfo (wlIndex).getMicrometerPerPixel();

	//int myLeft = positionManager->positionInformation.getLeft();
	//int myTop = positionManager->positionInformation.getTop();

	////const int threshold = 0;

	//// Get width and heigth of pic in pixels
	//int widthPx = TATInformation::getInst()->getWavelengthInfo (wlIndex).getWidth();
	//int heightPx = TATInformation::getInst()->getWavelengthInfo (wlIndex).getHeight();

	//if(widthPx == -1 || heightPx == -1)
	//	return;

	//// Create four rects that will be used for intersecting other positions
	//QVector<QRect> vRects(4);

	//// Top
	//vRects[0] = QRect( QPoint(-MovieNextPosItem::BAR_DISTANCE_PIC - MovieNextPosItem::WIDTH,
	//					-MovieNextPosItem::WIDTH-MovieNextPosItem::BAR_DISTANCE_PIC),
	//					QPoint(widthPx+MovieNextPosItem::BAR_DISTANCE_PIC+MovieNextPosItem::WIDTH,
	//					-MovieNextPosItem::BAR_DISTANCE_PIC));	

	//// Right
	//vRects[1] = QRect(QPoint(widthPx + MovieNextPosItem::BAR_DISTANCE_PIC, -MovieNextPosItem::BAR_DISTANCE_PIC+1), QPoint(widthPx + MovieNextPosItem::BAR_DISTANCE_PIC + MovieNextPosItem::WIDTH, heightPx+MovieNextPosItem::BAR_DISTANCE_PIC-1));

	//// Left
	//vRects[2] = QRect(QPoint(-MovieNextPosItem::BAR_DISTANCE_PIC - MovieNextPosItem::WIDTH, -MovieNextPosItem::BAR_DISTANCE_PIC+1), QPoint(-MovieNextPosItem::BAR_DISTANCE_PIC, heightPx+MovieNextPosItem::BAR_DISTANCE_PIC-1));

	//// Bottom
	//vRects[3] = QRect(QPoint( -MovieNextPosItem::BAR_DISTANCE_PIC - MovieNextPosItem::WIDTH,
	//	heightPx + MovieNextPosItem::BAR_DISTANCE_PIC),
	//	QPoint(widthPx + MovieNextPosItem::BAR_DISTANCE_PIC + MovieNextPosItem::WIDTH,
	//	heightPx + MovieNextPosItem::BAR_DISTANCE_PIC + MovieNextPosItem::WIDTH));	


	////QRect myRect(myLeft - threshold, myTop - threshold, width + 2*threshold, height + 2*threshold);

	//// Iterate over all positions
	//Q3DictIterator<TTTPositionManager> iter (TTTManager::getInst().getAllPositionManagers());
	//for( ; iter.current(); ++iter ) {
	//	if(iter.current() == positionManager)
	//		continue;

	//	int curLeft = iter.current()->positionInformation.getLeft();
	//	int curTop = iter.current()->positionInformation.getTop();

	//	QString curIndex = iter.current()->positionInformation.getIndex();
	//	QRect curRect(curLeft, curTop, widthPx*mmpp, heightPx*mmpp);
	//	QRect curRectPixel = MovieNextPosItem::transformRectToPixel(curRect, myLeft, myTop, mmpp);

	//	// Check for overlap
	//	for(int i = 0; i < vRects.size(); ++i) {
	//		if(curRectPixel.intersects(vRects[i])) {
	//			QRect intersection = curRectPixel.intersected(vRects[i]);


	//			MovieNextPosItem* foo = new MovieNextPosItem("Pos " + curIndex, curIndex);
	//			foo->setPos(0, 0);
	//			foo->setRect(intersection);

	//			foo->setData (GRAPHICSDATA_USAGE, GRAPHICSDATA_USAGE_TRACKCIRCLE);
	//			scene()->addItem(foo);

	//			qDebug() << curIndex << " - " << i;
	//		}
	//	}
	//}
}

void PictureView::addPositionButton( MovieNextPosItem* _posButton )
{
	// Add to scene (scene becomes owner, so button will be deleted when scene will be deleted)
	scene()->addItem(_posButton);

	// Add to list
	posButtons.append(_posButton);
}

void PictureView::showPositionButtons( bool _show )
{
	for(QVector<MovieNextPosItem*>::iterator it = posButtons.begin(); it != posButtons.end(); ++it) {
		(*it)->setVisible(_show);
	}
}

void PictureView::setShowPreviousTrackPoints( bool _on, int _numPrevTrackPointsToDraw, int _widthOfTails, bool _drawPrevTrackPointsTransparencyEffect, bool _drawPrevTrackPointsOnlyOfSelectedCellBranch )
{
	drawPrevTrackPoitns = _on;
	drawPrevTrackPointsTransparencyEffect = _drawPrevTrackPointsTransparencyEffect;
	numPrevTrackPointsToDraw = _numPrevTrackPointsToDraw;
	widthOfTails = _widthOfTails;
	drawPrevTrackPointsOnlyOfSelectedCellBranch = _drawPrevTrackPointsOnlyOfSelectedCellBranch;

	// Redraw tracks
	drawTracks();
}

void PictureView::setDrawSegmentation(bool _on)
{
	// Change and redraw tracks (also draws segmentation)
	drawSegmentation = _on;
	drawTracks();
}

void PictureView::setHighlightingOn(bool on)
{
	if(on)
		setStyleSheet("QGraphicsView {border: 1px solid red}");
	else
		setStyleSheet("QGraphicsView {border: 1px solid white}");
}