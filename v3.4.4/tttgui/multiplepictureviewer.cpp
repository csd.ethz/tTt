/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "multiplepictureviewer.h"

// Project includes
#include "pictureview.h"
#include "tttbackend/tttpositionmanager.h"
#include "tttgammaadjust.h"
#include "tttdata/userinfo.h"
#include "tttbackend/picturearray.h"
#include "tttmovie.h"
#include "tttbackend/tttmanager.h"
#include "tttbackend/tools.h"
#include "moviemenubar.h"
#include "movienextpositem.h"
#include "tttautotracking.h"
#include "tttautotrackingtreewindow.h"

// Qt includes
#include <QScrollBar>



MultiplePictureViewer::MultiplePictureViewer (QWidget *_containingFrame, int _wlCount, TTTPositionManager *_tttpm)
{
	// Init variables
	containingFrame = _containingFrame;
	positionManager = _tttpm;
	wlCount = _wlCount;
	overlayIndex = wlCount;
	hideOutOfSyncBox = false;
	circleCursorColor = Qt::blue;

	// Init picViews
	picViews.resize (wlCount + 1);
	for (int i = 0; i < picViews.size(); i++) {
		bool overlay = (i == picViews.size() - 1);              //only the last one is for overlay
		picViews[i] = new PictureView (this, i, containingFrame, _tttpm, overlay);
		connect ( picViews[i], SIGNAL (scrolled (int,int,int)), this, SLOT (setScrollBarValues (int,int,int)));
		connect ( picViews[i], SIGNAL (mouseMovedInPicture (QPointF)), this, SLOT (propagateAbsoluteMousePosition (QPointF)));
		connect ( picViews[i], SIGNAL (pictureViewChosen(int)), this, SLOT (setWavelength (int)));
		//note: signals can be connected to signals, operating only as a relay
		connect ( picViews[i], SIGNAL (PictureProceedDemand (bool, int, bool)), this, SIGNAL (PictureProceedDemand (bool, int, bool)));

		//connect ( picViews[i], SIGNAL (blackWhitePointChosen (int, int)), this, SLOT (setBlackWhitepoint (int, int)));
		connect ( picViews[i], SIGNAL (symbolDeleteDemand (int, int)), this, SLOT (deleteSymbol (int, int)));
		connect ( picViews[i], SIGNAL (pictureUpdated()), this, SLOT (notifyPictureUpdate()));
		connect ( picViews[i], SIGNAL (mouseReleased (QMouseEvent *, QPointF)), this, SLOT (handlePictureMouseRelease (QMouseEvent *, QPointF)));
		//connect ( picViews[i], SIGNAL (centerCurrentTrackDemand()), this, SLOT (centerCurrentTrack()));
		connect ( picViews[i], SIGNAL (symbolPositionSelected (QPointF)), this, SLOT (setSymbolPosition (QPointF)));
		connect ( picViews[i], SIGNAL (singleBackgroundSet (QPointF)), this, SLOT (setSingleBackground (QPointF)));
		connect ( picViews[i], SIGNAL (colonyStartDeleteDemand (int)), this, SLOT (deleteColonyStartingCell (int)));

		//forwarded signals
		connect ( picViews[i], SIGNAL (trackSelected (Track *, bool, bool)), this, SIGNAL (trackSelected (Track *, bool, bool)));
		connect ( picViews[i], SIGNAL (trackingInterrupted (Track *)), this, SIGNAL (trackingInterrupted (Track *)));
		connect ( picViews[i], SIGNAL (TrackPointSet (Track *, int)), this, SIGNAL (trackPointSet (Track *, int)));
		connect ( picViews[i], SIGNAL (cellSizeChanged (int)), this, SIGNAL (cellSizeChanged(int)));
		connect ( picViews[i], SIGNAL (RadiusSelected (int)), this, SIGNAL (radiusSelected (int)));
		connect ( picViews[i], SIGNAL (unhandledKeyReleased (int, bool, bool)), this, SIGNAL (unhandledKeyReleased (int, bool, bool)));
	}

	connect ( _tttpm->frmGammaAdjust, SIGNAL (graphicsAdjusted(int)), this, SLOT (adjustGraphics (int)));
	//connect ( _tttpm->frmGammaAdjust, SIGNAL (hidden()), this, SLOT ());



	multipleView = false;
	editCellBackground = false;

	picViews[0]->setShow (true);

	//updates the frame geometries and visibilities
	layout (multipleView);

	processScrollViews = true;
	circleMouseRadius = -1;
	circleMouseOn = false;

	overlayPictures = false;

	cellPathTPStart = -1;
	cellPathTPEnd = -1;

	WaveLengthsSelected = 1;

	startingCellSelectionMode = false;
	selectedStartingCells.clear();

	trackRadiusForDisplay = -1;

	movieMenu = 0;

	showOrientationBox = false;

	// Add map of positions buttons
	initMapsOfPositions();
}

QColor MultiplePictureViewer::getSpecialEffectTrackColor(int trackNumber)
{
	if(specialEffectsTracksColors.contains(trackNumber))
		return specialEffectsTracksColors.value(trackNumber);

	/*
	Code was used for Methods Paper (09/2013):
			
		
	// Highlight certain cells with red and blue (hardcoded)
	if(trackNumber == 1 
		|| trackNumber == 2
		|| trackNumber == 4
		|| trackNumber == 8
		|| trackNumber == 16
		|| trackNumber == 33
		|| trackNumber == 66
		|| trackNumber == 132
		|| trackNumber == 264) {
			specialEffectsTracksColors.insert(trackNumber, QColor(255, 0, 0));
			return QColor(255, 0, 0);
	}
	if(trackNumber == 3 
		|| trackNumber == 7
		|| trackNumber == 14
		|| trackNumber == 28
		|| trackNumber == 57
		|| trackNumber == 114
		|| trackNumber == 228
		|| trackNumber == 456) {
			specialEffectsTracksColors.insert(trackNumber, QColor(0, 0, 255));
			return QColor(0, 0, 255);
	}
			
	// Ohters are gray
	return QColor(178, 178, 178);
	*/
	//return QColor(255, 255, 0);

	// No color assigned, assign new
	QColor newCol;

	// Take over parent color (only for one daughter)
	if(trackNumber % 2 == 0 && trackNumber > 1) {
		int parentNumber = trackNumber / 2;
		newCol = specialEffectsTracksColors.value(parentNumber, QColor());
	}

	// If no color was found, create a new color
	if(!newCol.isValid()) {
		if(specialEffectsTracksColors.size() == 0)
			// First color is always yellow
			newCol = QColor(0, 255, 0);
		else
			// Random
			newCol = QColor(qrand() % 256, qrand() % 256, qrand() % 256);
	}

	specialEffectsTracksColors.insert(trackNumber, newCol);
	return newCol;
}

void MultiplePictureViewer::resize()
{
        layout (multipleView);
}

void MultiplePictureViewer::drawAdditions()
{
        if (! multipleView)
                picViews[positionManager->getWavelength()]->drawAdditions();
        else {
                for (int i = 0; i < picViews.size(); i++)
                        picViews[i]->drawAdditions();
        }
}

void MultiplePictureViewer::exportPics()
{
        //export what is currently visible

        //hide scrollbars
        for (int wl = 0; wl < picViews.size(); wl++) {
                picViews[wl]->horizontalScrollBar()->hide();
                picViews[wl]->verticalScrollBar()->hide();
        }

        QPixmap pix = QPixmap::grabWidget (containingFrame);

        //show scrollbars
        for (int wl = 0; wl < picViews.size(); wl++) {
                picViews[wl]->horizontalScrollBar()->show();
                picViews[wl]->verticalScrollBar()->show();
        }

        int activeWL = positionManager->getWavelength();
        //cut off scrollbars at the bottom and right edge (there is a place reserved for them, no matter if they are visible or not)
        QPixmap exportPix = pix.copy (0, 0, pix.width() - picViews[activeWL]->verticalScrollBar()->width(), pix.height() - picViews[activeWL]->horizontalScrollBar()->height());

        //save
        exportPix.save ("test.png", "PNG", 100);


}

//SLOT:
void MultiplePictureViewer::setWavelength (int _wl)
{
        if (_wl == positionManager->getWavelength())
                return;

        //drawPictures (timepoint, positionManager->getWavelength());
        setWaveLength (_wl, true, false);

        //emit signal to trigger button display update in TTTMovie
        emit wavelengthSelected (_wl);
}

void MultiplePictureViewer::setWaveLength (int _waveLength, bool _show, bool _callShowPicture)
{
	if ((! multipleView) && (! overlayPictures)) {
		//neither multiple view nor overlay

		if (_waveLength == positionManager->getWavelength())
			return;

		picViews[positionManager->getWavelength()]->setShow (false);
		positionManager->setWavelength (_waveLength);
		//activeWL = _waveLength;
		picViews[_waveLength]->setShow (true);

		// Make sure we have no border in single view
		picViews[_waveLength]->setHighlightingOn(false);
	}
	else {
		if (multipleView) {
			//the selected wavelength has to be displayed additionally - or hidden
			if (_waveLength > MAX_WAVE_LENGTH)
				return;	

			if (_show)
				WaveLengthsSelected++;
			else
				WaveLengthsSelected--;

			//a wavelength can also be switched off!
			picViews[_waveLength]->setShow (_show);

			// Mark active picture view with a red boarder
			int oldWl = positionManager->getWavelength();
			if(oldWl >= 0 && oldWl < picViews.size()) {
				picViews[oldWl]->setHighlightingOn(false);
			}
			picViews[_waveLength]->setHighlightingOn(true);

			if (! _show) {
				if (_waveLength == positionManager->getWavelength()) {
					//if the current wavelength was hidden, there is none...
					//-> find another displayed wavelength
					for (int i = 0; i <= MAX_WAVE_LENGTH; i++)
						if (picViews[i]->getShow()) {
							positionManager->setWavelength (i);
							break;
						}
				}
			}
			else
				positionManager->setWavelength (_waveLength);


		}
		else if (overlayPictures) {
			//the selected wavelength should be in-/excluded in the overlay picture - but not the box itself shown
			if (_waveLength > MAX_WAVE_LENGTH)
				return;		//should not occur!

			if (_show)
				WaveLengthsSelected++;
			else
				WaveLengthsSelected--;

			//a wavelength can also be switched off!
			picViews[_waveLength]->setShow (_show);
		}
	}

	//display graphic settings
	positionManager->frmGammaAdjust->setDisplay (positionManager, _waveLength);

	layout (multipleView, _callShowPicture);

}

void MultiplePictureViewer::layout (bool _multipleView, bool _updateView)
{
	//layout policy: custom
	//step 1: calculate width/height for all visible views
	//step 2: apply the calculated values

	int counter = 0;
	//if multiple mode is false, all views got the whole size so they can easily be displayed!
	int width = containingFrame->width(), height = containingFrame->height();
	int xCount = 1;


	multipleView = _multipleView;

	if (multipleView) {

		//count all frames that should be shown
		for (int i = 0; i < picViews.size(); i++)
			if (picViews[i]->isVisible())
				counter++;

		counter = QMAX (counter, 1);

		UserInfo::getInst().getStyleSheet().getMapping (xCount);
		//now: xCount  == user choice for pics per row
		//     counter == number of visible pictures
		//all pictures should have equal height

		//problem with 3 pics per row and 4 wavelengths => user is responsible

		if (counter <= xCount) {
			xCount = counter;
			width = containingFrame->width() / counter;
			height = containingFrame->height();
		}
		else {
			width = containingFrame->width() / xCount;
			//counter / xCount + 1 == number of rows necessary
			int div = counter / xCount;
			if (counter % xCount != 0)
				div += 1;	//add corrector
			height = containingFrame->height() / div;
		}


		counter = 0;
	}

	int xStep = 0, yStep = 0;

	for (int i = 0; i < picViews.size(); i++) {
		int index = UserInfo::getInst().getStyleSheet().getWavelength (i);

		if (i == MAX_WAVE_LENGTH + 1)
			//overlay frame
			index = i;
		else {
			if ((index < 0) || (index > picViews.size()))
				continue;
			else if (! picViews[index])
				continue;
		}

		if ((! multipleView)/* || overlayPictures*/)
			//only one picture in the view
			picViews [index]->setGeometry (0, 0, width, height);

		//draw all normal pictures
		if (picViews[index]->getShow()) {

			if (multipleView)
				picViews[index]->setGeometry (xStep, yStep, width, height);

			counter++;

			xStep += width;
			if (counter % xCount == 0) {
				xStep = 0;
				yStep += height;
			}

			picViews[index]->show();
		}
		else
			picViews[index]->hide();
	}

	// Update marking of active pic view
	int wl = positionManager->getWavelength();
	if(wl >= 0 && wl < picViews.size()) {
		if(multipleView)
			picViews[wl]->setHighlightingOn(true);
		else
			picViews[wl]->setHighlightingOn(false);
	}


	if (_updateView) {
		positionManager->frmGammaAdjust->setDisplay (positionManager, positionManager->getWavelength());

		drawPictures();
		drawAdditions();
	}
}

void MultiplePictureViewer::drawPictures (int _timepoint, int _wavelength)
{
	if (! positionManager)
		return;
	if (! positionManager->getPictures())
		return;

	if (_timepoint == -1)
		_timepoint = positionManager->getTimepoint();


	if (_timepoint == 0) {
		//still no sensible call yet - set timepoint to the first available picture
		if (positionManager->getPictures()->getLoadedPictures() > 0) {
			PictureIndex pi = positionManager->getPictures()->getFirstLoadedPictureIndex();
			_timepoint = pi.TimePoint;
		}
	}


	//delete old pictures from view before drawing new ones! happens in clearView() which is called below


	bool drawOverlay = overlayPictures; // && (_wavelength == -1);

	//by default, redraw all wavelengths
	int start = 0; //positionManager->getWavelength();
	int stop = MAX_WAVE_LENGTH; //positionManager->getWavelength();

	// If overlay is on, we have to redraw everything as we cannot only redraw one picture
	if (_wavelength > -1 && !drawOverlay) {	
		start = _wavelength;
		stop = _wavelength;
	}
	else if (multipleView) {
		start = 0;
		stop = wlCount - 1;
	}

	if (drawOverlay) {
		picViews[overlayIndex]->clearView (GRAPHICSDATA_USAGE, GRAPHICSDATA_USAGE_PICTURE);
	}

	for (int i = start; i <= stop; i++) {
		const PictureContainer *pc = positionManager->getPictures()->getPictureContainer (_timepoint, positionManager->getZIndex(), i);
		if (pc) {

			if (pc->isLoaded()) {
				picViews[i]->clearView (GRAPHICSDATA_USAGE, GRAPHICSDATA_USAGE_PICTURE);
				picViews[i]->drawPicture (pc);


				//additionally... but only if all wavelengths are drawn
				//if (drawOverlay && picViews[i]->isVisible()) {
				if (drawOverlay && positionManager->frmMovie->useWlForOverlay(i)) {
					picViews[overlayIndex]->drawPicture (pc, true);
				}
			}
		}
		else {
			//if pc is empty (no picture exists at this tp/wl), but a picture is visible from former timepoints (e.g. in wl1 if every 10th tp exists),
			// the graphics have to be updated nonetheless
			picViews[i]->drawPicture (pc);
		}

		if (! picViews[i]->pictureLoaded()) {
			//scene is still empty, no picture ever loaded
			//=> add a rectangle with the same size as the pictures to enable concurrent scrolling even without a loaded picture
			picViews[i]->drawPicture (0);
		}
	}
}

void MultiplePictureViewer::setScrollBarValues (int _callingIndex, int _x, int _y)
{
        if (! processScrollViews)
            return;

        processScrollViews = false;
        for (int i = 0; i < picViews.size(); i++) {
            if (i != _callingIndex) {
                    //QScrollBar *sb = picViews[i]->horizontalScrollBar();
                    //sb->setValue (sb->value() - _dx);
                    //sb = picViews[i]->verticalScrollBar();
                    //sb->setValue (sb->value() - _dy);
				QScrollBar *sb = picViews[i]->horizontalScrollBar();
				sb->setValue (_x);
				sb = picViews[i]->verticalScrollBar();
				sb->setValue (_y);
            }
        }
        processScrollViews = true;
}

void MultiplePictureViewer::propagateAbsoluteMousePosition (QPointF _globalAbsolutePosition)
{

        //the provided coordinates are already global
        emit pictureMousePositionSet (_globalAbsolutePosition);
}

void MultiplePictureViewer::setZoom (int _zoomPercent, float _zoomX, float _zoomY)
{
        if (_zoomPercent > 0) {
                //only necessary if setZoom() was called by PictureView (via mousewheel change); otherwise it does no harm
                positionManager->frmMovie->setZoomSpinboxValue (_zoomPercent, false);
        }

		// Disable processing of signal that is emitted by pictureviews when centering on _zoomY and _zoomY
		// as we call setZoom for alle wavelengths anyways and to prevent scrolling-errors caused by not shown wavelengths
		processScrollViews = false;

		// Zoom in all wavelengths
        for (int i = 0; i <= picViews.size() - 1; i++) {
            picViews[i]->setZoom (_zoomPercent, _zoomX, _zoomY);
        }

		processScrollViews = true;
}

int MultiplePictureViewer::getCurrentZoom() const
{
        for (int i = 0; i <= picViews.size() - 1; i++) {
                double zoom = picViews[i]->getCurrentZoomAccurate();
                if (zoom > 0)
                        return zoom * 100;
        }

        return -1;
}

void MultiplePictureViewer::setCircleMouse (bool _on)
{
	//now, there is no more distinction between displaying/saving the circle size!

	//only update mouse pointer if really changed (either shape or size)
	if ((! _on) &&  (! circleMouseOn))
		return;

	if (_on) {
		//ensure that the cursor exists
		if (circleMouseRadius == -1)
			setCircleMouseRadius (CELLCIRCLESIZE);

		for (int i = 0; i <= picViews.size() - 1; i++) {
			picViews[i]->setCursor (circleCursor);
			picViews[i]->viewport()->setCursor(circleCursor);
		}
	}
	else {
		for (int i = 0; i <= picViews.size() - 1; i++) {
			picViews[i]->setCursor (Qt::ArrowCursor);
			picViews[i]->viewport()->setCursor(Qt::ArrowCursor);
		}
	}

	circleMouseOn = _on;
}

void MultiplePictureViewer::updateCircleMousePointer(bool _trackingMode)
{
	// Check if we have a circle mouse pointer
	if (circleMouseRadius <= 0)
		return;

	// Update color of circle mouse pointer according to if tracking mode is on
	if(_trackingMode)
		circleCursorColor = Qt::red;
	else
		circleCursorColor = Qt::blue;

	circleCursor = Tools::createCircleCursor (circleMouseRadius, circleCursorColor, 2);
	setCircleMouse (circleMouseOn);
}

void MultiplePictureViewer::setCircleMouseRadius (int _radius)
{
	if (_radius == circleMouseRadius)
		return;

	//create new mouse cursor with the provided radius
	circleMouseRadius = _radius;

	circleCursor = Tools::createCircleCursor (circleMouseRadius, circleCursorColor, 2);

	setCircleMouse (circleMouseOn);
}

void MultiplePictureViewer::setOrientationBoxVisible (bool _visible)
{
	for (int i = 0; i <= picViews.size() - 1; i++) {
		picViews[i]->setOrientationBoxVisible (_visible);
	}
	showOrientationBox = _visible;
}

void MultiplePictureViewer::setOrientationBoxPosition (QPointF _pos, bool _isaQCursorPos)
{
	if (_isaQCursorPos) {
		//calculate picture position from screen mouse position
		PictureView *pv = picViews[positionManager->getWavelength()];
		if (pv) {
			_pos = pv->mapFromGlobal (_pos.toPoint());
			_pos = pv->mapToScene (_pos.toPoint());
		}
	}

	//redistribute the position to all wavelengths
	for (int i = 0; i <= picViews.size() - 1; i++) {
		picViews[i]->setOrientationBoxPosition (_pos);
	}
}

void MultiplePictureViewer::setOverlay (bool _on, bool _callShowPicture)
{
	//@todo how to handle? new style (overlay + others) or old (overlay XOR others)?

	if (_on == overlayPictures)
		return;

	overlayPictures = _on;

	if (_on)
		//mandatory
		multipleView = true;
	else
		multipleView = false;


	//new politics: overlay is just another frame that can be shown additionally
	picViews[MAX_WAVE_LENGTH + 1]->setShow (_on);

	layout (multipleView, _callShowPicture);
}

void MultiplePictureViewer::adjustGraphics (int _wavelength)
{
	// If _wavelength is valid, try to get the actually displayed timepoint in corresponding PictureView (if pic is out of sync, displayed timepoint  
	// could differ from the current global timepoint -> no pic at global timepoint -> gamma changes not displayed if we use it)
	if(_wavelength >= 0 && _wavelength < picViews.size() && picViews[_wavelength]->getCurrentTimePoint() > 0)
		drawPictures (picViews[_wavelength]->getCurrentTimePoint(), _wavelength);
	else
		drawPictures (positionManager->getTimepoint(), _wavelength);
}

void MultiplePictureViewer::setSymbolPosition (QPointF _pos)
{
	positionManager->getSymbolHandler().getCurrentSymbol().setPosition (_pos);
}

//SLOT:
void MultiplePictureViewer::startSymbolPositionSelection (Symbol _symbol)
{
	positionManager->getSymbolHandler().setCurrentSymbol (_symbol);
	//positionManager->getSymbolHandler().getCurrentSymbol().setPosition (QPointF());

	if (picViews[positionManager->getWavelength()])
		picViews[positionManager->getWavelength()]->receiveSymbolPosition (_symbol);
}

//SLOT:
void MultiplePictureViewer::setCellBackgroundDisplay (bool _on)
{
	for (int i = 0; i < picViews.size(); i++)
		picViews[i]->setShowBackground (_on);

	drawPictures (positionManager->getTimepoint(), -1);
}

//SLOT:
void MultiplePictureViewer::setAllCellsUniform (bool _on)
{
	//set the parameter in all picture frames
	for (int i = 0; i < picViews.size(); i++)
		picViews[i]->setAllCellsUniform (_on);

	drawPictures (positionManager->getTimepoint(), -1);
}

////SLOT:
//void TTTMovie::setTransColorThreshold (int _threshold)
//{
//	if (_threshold < 256 && _threshold > 0) {
//		positionManager->getDisplays().at (positionManager->getWavelength()).TransColorThreshold = _threshold;
//		positionManager->getDisplays().at (positionManager->getWavelength()).adjustGammaValues();
//		showPicture(positionManager->getTimepoint(), -1);
//	}
//}

//SLOT:
void MultiplePictureViewer::setCellPathStartTP (int _startTp)
{
	cellPathTPStart = _startTp;
}

//SLOT:
void MultiplePictureViewer::setCellPathEndTP (int _endTp)
{
	cellPathTPEnd = _endTp;
}

//SLOT:
void MultiplePictureViewer::addCellPath()
{
	//add current cell to list of cells with cell paths
	if (TTTManager::getInst().getCurrentTrack()) {
		cellPathCells.push_back (CellPathCell (TTTManager::getInst().getCurrentTrack(), cellPathTPStart, cellPathTPEnd));
		drawAdditions();
	}
}

//SLOT:
void MultiplePictureViewer::clearCellPaths()
{
	for (int i = 0; i < picViews.size(); i++)
		picViews[i]->clearView (GRAPHICSDATA_USAGE, GRAPHICSDATA_USAGE_CELLPATH);

	cellPathCells.clear();
	cellPaths.clear();
}

QVector<MyCircle> MultiplePictureViewer::calcCellPaths()
{
	QVector<MyCircle> positions (0);

	if (! TTTManager::getInst().frmTracking)
		return positions;
	if (! TTTManager::getInst().getTree())
		return positions;

	int cpt = UserInfo::getInst().getStyleSheet().getCellPathThickness();
	QColor cpc = UserInfo::getInst().getStyleSheet().getCellPathColor();
	int cp_ti = UserInfo::getInst().getStyleSheet().getCellPathTimeInterval();

	int timeStamp = -1;

	//QVector<QPair<int, QPointF> > tmppos;
	

	for (QVector<CellPathCell>::iterator iter = cellPathCells.begin(); iter != cellPathCells.end(); ++iter) {
		if ((*iter).track) {
			if (((*iter).tpStart <= positionManager->getTimepoint()) & (positionManager->getTimepoint() <= (*iter).tpEnd)) {

				positions.push_back (MyCircle());		//delimiter for connections

				//tmppos = (*iter).track->getPositions();
				QHash<int, QPointF> tmppos = (*iter).track->getPositions(); 

				int cc = 0;
				//for (int i = 0; i < tmppos.size(); i++) {
				QList<int> timePointsOfTrack = tmppos.keys();
				std::sort(timePointsOfTrack.begin(), timePointsOfTrack.end());
				for(auto it = timePointsOfTrack.constBegin(); it != timePointsOfTrack.constEnd(); ++it) {
					int tp = *it;
					cc++;
					if (cp_ti > 0) {
						if (cc % cp_ti == 0)
							timeStamp = tp;
						else
							timeStamp = -1;
					}

					//store positions in experiment global coordinates (they are mapped to viewport during drawing)
					//MyCircle mc ((int)tmppos [i].second.x(), (int)tmppos [i].second.y(), 3, cpc, true, cpt, timeStamp);
					MyCircle mc ((int)tmppos [tp].x(), (int)tmppos [tp].y(), 3, cpc, true, cpt, timeStamp);
					mc.setUsageParameter (GRAPHICSDATA_USAGE_CELLPATH);
					positions.push_back (mc);
				}
			}
		}
	}

	return positions;
}

//SLOT:
void MultiplePictureViewer::centerCurrentTrack()
{
	// Try track in new tree window
	Track* track = nullptr;
	if(TTTAutoTracking* at = TTTManager::getInst().frmAutoTracking) {
		if(TTTAutoTrackingTreeWindow* tw = at->getTreeWindow())
			track = tw->getCurrentTrack();
	}

	// Try main track from TTTManager
	if(!track) 
		track = TTTManager::getInst().getCurrentTrack();
	if (! track)
		return;

	int timepoint = positionManager->getTimepoint();


	if (! track->tracked()) {
		//if there is no track point yet, use the mother cell's last track point

		if (track->getMotherTrack()) {
			track = track->getMotherTrack();
			timepoint = track->getLastTrace();
		}
		else
			track = 0;
	}

	if (track) {
		centerTrack(track, timepoint);
	}
}

bool MultiplePictureViewer::centerCurrentTrackIncludingPrecursors()
{
	// Find track
	int timepoint = positionManager->getTimepoint();
	ITree* tree = TTTManager::getInst().getTree();
	if(!tree)
		return false;
	ITrack* track = TTTManager::getInst().getCurrentTrack();
	if(!track)
		return false;
	int trackNumber = track->getTrackNumber();
	while(!track->getTrackPointByTimePoint(timepoint)) {
		trackNumber /= 2;
		if(trackNumber <= 0)
			return false;
		track = tree->getTrackByNumber(trackNumber);
		if(!track)
			return false;
	}

	// If we got here, we have a track that is alive at time point, so check if it is in this position
	ITrackPoint* trackPoint = track->getTrackPointByTimePoint(timepoint);
	if(trackPoint->getPositionNumber() != positionManager->positionInformation.getIndex().toInt())
		return false;

	// Cell is alive at timePoint and in this position
	centerTrack(track, timepoint);

	return true;
}

//SLOT:
void MultiplePictureViewer::addFluorescenceLine()
{
	//@todo
	//        if (! TTTManager::getInst().frmTextbox)
	//                return;
	//
	//
	//        if (TTTManager::getInst().frmTextbox->getUsage() == TFUNone) {
	//                TTTManager::getInst().frmTextbox->setUsage (TFUFluorescenceIntegrals);
	//                TTTManager::getInst().frmTextbox->setCaption ("Division fluorescence integrals");
	//
	//                //add header line to textbox
	//                QString line = "Tree name;Cell numbers;Integral Cell 1;Integral Cell 2;Integral Background;Integral Cell 1 Normalized;Integral Cell 2 Normalized";
	//                TTTManager::getInst().frmTextbox->addLine (line);
	//
	//                TTTManager::getInst().frmTextbox->show();
	//        }
	//
	//        if (! PictureFrames.find (positionManager->getWavelength()))
	//                return;
	//
	//        if (! TTTManager::getInst().getCurrentTrack())
	//                return;
	//
	//        //start background cell selection
	//        //as soon as the user has clicked, the signal singleBackgroundSet (QPointF) is emitted
	//        PictureFrames.find (positionManager->getWavelength())->startSingleBackgroundSelectionMode();
}

//SLOT:
void MultiplePictureViewer::setSingleBackground (QPointF _posBg)
{
	//@todo
	//        if (! TTTManager::getInst().frmTextbox)
	//                return;
	//
	//        QString line = "";
	//        int wavelength = positionManager->getWavelength();
	//        int timepoint = positionManager->getTimepoint();
	//
	//        //tree file name
	//        QString filename = TTTManager::getInst().getTree()->getFilename();
	//        filename = filename.mid (filename.findRev ("/") + 1);		//trim to filename without path
	//        line = filename + ";";
	//
	//        Track *track1 = TTTManager::getInst().getCurrentTrack();
	//        Track *track2 = 0;
	//
	//        //cell numbers
	//        int cellNumber = TTTManager::getInst().getCurrentTrack()->getNumber();
	//        if (cellNumber % 2 == 0) {	//even track number -> sibling has number x+1
	//                line += QString ("%1 & %2").arg (cellNumber).arg (cellNumber + 1) + ";";
	//                track2 = track1->getSibling();
	//        }
	//        else {				//odd track number -> sibling has number x-1
	//                line += QString ("%1 & %2").arg (cellNumber - 1).arg (cellNumber) + ";";
	//                track2 = track1;
	//                track1 = track2->getSibling();
	//        }
	//
	//        if ((! track1) || (! track2))
	//                return;
	//
	//        //calculate integrals (both cells and the background
	//        //==================================================
	//        int integrals [4];	//1: left cell; 2: right cell; 3: background
	//
	//        QPointF middle;
	//        int radius = 0;
	//        for (int i = 1; i <= 3; i++) {
	//
	//                if (i < 3) {
	//                        TrackPoint trackPoint = track1->getTrackPoint (timepoint);
	//                        if (i == 2)
	//                                trackPoint = track2->getTrackPoint (timepoint);
	//
	//                        middle.setX (trackPoint.X);
	//                        middle.setY (trackPoint.Y);
	//
	//                        middle = positionManager->getDisplays().at (wavelength).calcTransformedCoords (middle, &positionManager->positionInformation);
	//                        radius = (int)(trackPoint.CellDiameter / 2.0f);
	//                }
	//                else {
	//                        middle = _posBg;
	//                        //radius does not change!
	//
	//                        middle = positionManager->getDisplays().at (wavelength).calcTransformedCoords (middle, &positionManager->positionInformation);
	//                }
	//
	//                int integralCell = 0;
	//                int dx = 0, dy = 0;
	//                for (int x = (int)middle.x() - radius; x <= (int)middle.x() + radius; x++) {
	//                        for (int y = (int)middle.y() - radius; y <= (int)middle.y() + radius; y++) {
	//
	//                                //check if point is inside the region of the cell (circle!) via Pythagoras
	//                                dx = x - (int)middle.x();
	//                                dy = y - (int)middle.y();
	//
	//                                if ((dx*dx + dy*dy) <= radius*radius) {
	//                                        int PixelColor = positionManager->getPictures()->readPixel (timepoint, wavelength, x, y);
	//
	//                                        integralCell += PixelColor;
	//                                }
	//                        }
	//                }
	//
	//                integrals [i] = integralCell;
	//
	//                line += QString ("%1").arg (integralCell) + ";";
	//        }
	//
	//        //append normalized integrals to output
	//        line += QString ("%1").arg (integrals [1] - integrals [3]) + ";";
	//        line += QString ("%1").arg (integrals [2] - integrals [3]); // + ";";
	//
	//        TTTManager::getInst().frmTextbox->addLine (line);
}

////SLOT:
//void MultiplePictureViewer::displayAllTracks (bool _show, bool _firstOnly, bool _reallyAll)
//{
//        positionManager->setATPfirstTrackPointsOnly (_firstOnly);
//
//
//        if (_show) {
//                positionManager->readAllTrackPoints (_firstOnly, _reallyAll, positionManager->getTimepoint(), true);
//        }
//        else {
//                //clear the array of all trackpoints in the current experiment folder
//                positionManager->getAllTrackPoints().clear();
//        }
//
//        drawPictures (positionManager->getTimepoint(), -1);
//		drawAdditions();
//}

////SLOT:
//void MultiplePictureViewer::setDisplayFirstTracks( bool _on )
//{
//	positionManager->setATPfirstTrackPointsOnly (_on);
//}

////SLOT:
//void MultiplePictureViewer::setDisplayAllTracksOfCurPos( bool _on )
//{
//	if(_on)
//		positionManager->readAllTrackPoints (positionManager->getATPfirstTrackPointsOnly(), false, positionManager->getTimepoint(), true);
//	else
//		positionManager->getAllTrackPoints().clear();
//}

////SLOT:
//void MultiplePictureViewer::setDisplayAllTracksOfAllPos( bool _on )
//{
//	if(_on)
//		positionManager->readAllTrackPoints (positionManager->getATPfirstTrackPointsOnly(), true, positionManager->getTimepoint(), true);
//	else
//		positionManager->getAllTrackPoints().clear();
//}

//SLOT:
void MultiplePictureViewer::selectStartCells (bool _on)
{
	if (TTTManager::getInst().isTracking())
		return;

	//if there is no change
	if (startingCellSelectionMode == _on)
		return;

	if (! _on) {
		//stop the selection mode

		//if there are already selections, the user is asked if he really wants to lose them
		if (selectedStartingCells.size() > 0) {
			if (QMessageBox::question (0, "tTt", "You have already chosen some cells.\nDo you really want to dismiss them?", "Yes", "No") == 0) {
				//throw 'em away
				clearStartingCells();
				drawPictures (positionManager->getTimepoint(), -1);
			}
			else {
				drawPictures (positionManager->getTimepoint(), -1);

				if(movieMenu)
					movieMenu->actFile_COLONYCREATION_SELECT->setChecked (true);

				return;		//do not change the mode!
			}
		}
	}
	else if (_on) {
		//start the selection mode

		clearStartingCells();
	}

	startingCellSelectionMode = _on;
}

//SLOT:
void MultiplePictureViewer::createColonies()
{
	if (TTTManager::getInst().isTracking())
		return;
	if (! startingCellSelectionMode)
		return;
	if (! TTTManager::getInst().frmTracking)
		return;

	bool failed = false;

	Tree tmpTree;
	Track *track = 0;

	QString firstSuffix = "", lastSuffix = "";
	QPointF pos;

	bool addUserSign = false;

	int ret = QMessageBox::question (0, "Colony creation...", "Would you like your user sign to be appended to each colony filename (pattern: " + TTTManager::getInst().getExperimentBasename() + "_p001" + UserInfo::getInst().getUsersign() + TTT_FILE_SUFFIX + ")?", "Yes", "No");
	if (ret == 0) //yes
		addUserSign = true;

	//create directory for ttt files (if not present)
	SystemInfo::checkNcreateDirectory (positionManager->getTTTFileDirectory (true), true, true);

	//save positions by creating a new colony (= ttt file) for each
	for (int i = 0; i < selectedStartingCells.size(); i++) {

		if (selectedStartingCells[i].isNull())
			//do not create colonies for deleted cells
			continue;

		//note: with Tree::reset() all tracks are deleted from heap!
		tmpTree.reset();

		//create Tree object with one cell, starting at the current timepoint
		track = new Track (0, 0, positionManager->getLastTimePoint(), positionManager->getTimepoint());
		track->setNumber (1);

		//set first trackpoint
		pos = QPointF (selectedStartingCells [i].getX(), selectedStartingCells [i].getY());
		track->addMark (positionManager->getTimepoint(), pos.x(), pos.y(), CELLCIRCLESIZE, false, 0, positionManager->positionInformation.getIndex());

		track->setStopReason (TS_NONE);
		tmpTree.insert (track);

		//find out next possible name for a colony (regard already existing ones)
		QString suffix = TTTManager::getInst().getNextFreeFile (positionManager, true);

		QString filename = positionManager->getTTTFileDirectory() + positionManager->getBasename() + suffix;

		if (addUserSign) {
			//insert user sign before file ending
			filename += UserInfo::getInst().getUsersign();
		}
		filename += TTT_FILE_SUFFIX;

		//SystemInfo::report (filename, false, true);

		//save first and last created suffix (without leading "-") => colony numbers
		if (i == 0)
			firstSuffix = suffix.right (3);
		if (i == selectedStartingCells.size() - 1)
			lastSuffix = suffix.right (3);

		//save tree to ttt file
		failed |= ! TTTFileHandler::saveFile (filename, &tmpTree, positionManager->getFirstTimePoint(), positionManager->getLastTimePoint());
		//                if (! failed)
		//                        TTTManager::getInst().frmTracking->updateColonyList (filename, false);
	}

	if (! failed) {
		QString message = QString ("%1 colonies were successfully created, \nwith colony numbers from %2 to %3.")
			.arg (selectedStartingCells.size())
			.arg (firstSuffix)
			.arg (lastSuffix);
		QMessageBox::information (0, "Success message", message, "Ok");
	}
	else {
		QString message = "Something failed while creating the files. Please retry.";
		QMessageBox::warning (0, "Failure message", message, "Yes, I want that!");
	}

	startingCellSelectionMode = false;
	if(movieMenu)
		movieMenu->actFile_COLONYCREATION_SELECT->setChecked (false);
	clearStartingCells();
}

//SLOT:
void MultiplePictureViewer::clearStartingCells()
{
	for (int i = 0; i < picViews.size(); i++)
		picViews[i]->clearView (GRAPHICSDATA_USAGE, GRAPHICSDATA_USAGE_SELECTED_COLONY_START);

	selectedStartingCells.clear();
}


//SLOT:
void MultiplePictureViewer::handlePictureMouseRelease (QMouseEvent *_ev, QPointF _pos)
{
	if (startingCellSelectionMode) {

		switch (_ev->button()) {

		case Qt::LeftButton: {
			//note: coordinates are already experiment global
			//store position and draw circle

			//the index of the new start cell in the array (will be encoded in its MyCircle attributes to enable deletion)
			int newIndex = selectedStartingCells.size();
			MyCircle mc (_pos.x(), _pos.y(), CELLCIRCLESIZE, Qt::white, false, newIndex, -1);
			mc.setUsageParameter (GRAPHICSDATA_USAGE_SELECTED_COLONY_START);
			selectedStartingCells.push_back (mc);
			break;
							 }
		default:
			;
		}

		drawAdditions();
	}
}

//SLOT:
void MultiplePictureViewer::deleteColonyStartingCell (int _index)
{
	if (! startingCellSelectionMode)
		return;

	if ( (_index >=0) && (_index < selectedStartingCells.size()) ) {
		//note: do not remove()/erase() the item, rather mark it as deleted - otherwise the index scheme gets fuzzy...
		//selectedStartingCells.remove (_index);
		selectedStartingCells[_index] = MyCircle();
		drawAdditions();
	}
}


//SLOT:
void MultiplePictureViewer::notifyPictureUpdate()
{
	//let the mouse cursor blink three times
	//(switch between two cursor shapes)


	connect ( &universalTimer, SIGNAL (timeout()), this, SLOT (blinkMouse()));

	if (! sender())
		//this->setCursor (Qt::PointingHandCursor);
		;
	else {
		for (int i = 0; i < picViews.size(); i++)
			if (picViews[i])
				picViews[i]->setCursor (Qt::PointingHandCursor);
	}

	universalTimer.start (500, true);

}

//SLOT:
//started in notifyPictureUpdate()
void MultiplePictureViewer::blinkMouse()
{
	//this->unsetCursor();
	for (int i = 0; i < picViews.size(); i++) {
		if (picViews[i]) {
			if (circleMouseOn)
				picViews[i]->setCursor (circleCursor);//setCurrentCursor (TTTManager::getInst().isTracking());
			else
				picViews[i]->setCursor (Qt::ArrowCursor);
		}
	}

	disconnect ( &universalTimer, 0, 0, 0);
	universalTimer.stop();
}

//SLOT:
void MultiplePictureViewer::deleteSymbol (int, int _index)
{
	positionManager->getSymbolHandler().deleteSymbol (_index);
	drawAdditions();
}

void MultiplePictureViewer::setShowTracks (bool _show, int _startWL, int _endWL)
{
	for (int wl = _startWL; wl <= _endWL; wl++)
		picViews[wl]->setShowTracks (_show);
}

void MultiplePictureViewer::setShowTrackNumbers (bool _on, int _startWL, int _endWL)
{
	for (int wl = _startWL; wl <= _endWL; wl++)
		picViews[wl]->setShowTrackNumbers (_on);
}

void MultiplePictureViewer::setShowWavelengthBox (bool _shown, int _startWL, int _endWL)
{
	for (int wl = _startWL; wl <= _endWL; wl++)
		picViews[wl]->setWavelengthBoxVisible (_shown);
}

void MultiplePictureViewer::setMultipleMode (bool _on)
{
	if (! _on)
		WaveLengthsSelected = 1;

	layout (_on, true);

}

void MultiplePictureViewer::centerPosition (QPointF _newCenter) const
{
	//center the current view, all others follow automatically
	//_newCenter is in absolute coordinates
	picViews[positionManager->getWavelength()]->centerAbsolutePosition (_newCenter);
}

//void MultiplePictureViewer::setShowBackground (bool)
//{
//}

void MultiplePictureViewer::setBackgroundTracking (bool _on)
{
	editCellBackground = _on;

	picViews[positionManager->getWavelength()]->setBackgroundTracking(_on);
}

void MultiplePictureViewer::setToTimepointWavelength (int _newTimepoint, int _newZIndex, int _newWavelength)
{
	PictureIndex pi (_newTimepoint, _newZIndex, _newWavelength);

	if (! positionManager->getPictures()->isLoaded (_newTimepoint)) {
		//no picture is loaded at _newTimepoint => try the next one loaded
		pi = positionManager->getPictures()->nextLoadedPicture (PictureIndex (_newTimepoint, -1, -1));
	}

	//recalculate cell paths
	cellPaths = calcCellPaths();

	drawPictures (pi.TimePoint, pi.WaveLength);
	drawAdditions();
}

bool MultiplePictureViewer::startTracking (bool _isMainTrackingInstance)
{
	//it seems that only every 10th trackpoint is stored - maybe this comes from another wavelength than the default being active
	//accordingly, the active wavelength is set to 0 before the tracking mode is started
	//=> a jump of only one picture per set trackpoint is guaranteed
	//?? setCurrentWaveLength (0);

	// Put track in center if this is the main window
	if(_isMainTrackingInstance)
		centerCurrentTrack();

	picViews[positionManager->getWavelength()]->startTracking (_isMainTrackingInstance, editCellBackground);

	// If circle mouse cursor is off, set cross cursor
	if (! circleMouseOn) {
		for (int i = 0; i < (int)picViews.size(); i++) {
			if (i != positionManager->getWavelength())
				picViews[i]->setCursor (QCursor (Qt::CrossCursor));
		}
	}

	// Update color of circle mouse pointer
	updateCircleMousePointer(true);

	return true;
}

bool MultiplePictureViewer::stopTracking()
{
	for (int i = 0; i < (int)picViews.size(); i++) {
		picViews[i]->stopTracking();//setCursor (Qt::ArrowCursor);
	}

	// Update color of circle mouse pointer
	updateCircleMousePointer(false);

	return true;
}

void MultiplePictureViewer::applyStyleSheet (const StyleSheet &_ss)
{
	//apply movie layout of _ss (could be different from current UserInfo.styleSheet!)

	layout (multipleView, true);
	for (int i = 0; i < picViews.size(); i++)
		picViews[i]->applyStyleSheet (_ss);

}

//void MultiplePictureViewer::releaseKeyboard()
//{
//}
//
//void MultiplePictureViewer::grabKeyboard()
//{
//}

void MultiplePictureViewer::showHideScrollbars(bool _hide) 
{
	Qt::ScrollBarPolicy newPolicy = _hide ? Qt::ScrollBarAlwaysOff : Qt::ScrollBarAsNeeded;

	for (int i = 0; i < picViews.size(); i++) {
		picViews[i]->setHorizontalScrollBarPolicy(newPolicy);
		picViews[i]->setVerticalScrollBarPolicy(newPolicy);
	}
}

bool MultiplePictureViewer::exportPicture(QPaintDevice *_pd, int _waveLength, int left, int top, int w, int h) const
{
	// Check parameters
	if(!_pd || _waveLength < 0 || _waveLength >= picViews.size())
		return false;

	// Get scene
	QGraphicsScene *scene = picViews[_waveLength]->scene();
	if(!scene)
		return false;

	// Create painter
	QPainter painter(_pd);

	// Draw picture
	scene->render(&painter, QRectF(), QRectF(left, top, w, h));

	return true;
}

void MultiplePictureViewer::toggleOverwriteTrackRadiusForDisplay(bool _on, int _trackRadius)
{
	if(_on) {
		// Do nothing if trackradius is not valid
		if(_trackRadius > 0 && trackRadiusForDisplay != _trackRadius) {
			trackRadiusForDisplay = _trackRadius;

			// Redraw tracks
			drawAdditions();
		}
	}
	else {
		// Disable feature
		if( trackRadiusForDisplay != -1) {
			trackRadiusForDisplay = -1;

			// Redraw tracks
			drawAdditions();
		}
	}
}

void MultiplePictureViewer::debugTest()
{
	for (int i = 0; i < picViews.size(); i++) {
		picViews[i]->debugTest();
	}
}

void MultiplePictureViewer::centerTrack( const ITrack *track, int _timepoint )
{
	if(_timepoint == -1)
		_timepoint = positionManager->getTimepoint();

	if (track) {
		ITrackPoint* tmpTP = track->getTrackPointByTimePoint(_timepoint);
		if(tmpTP) {
			QPointF trackPoint = QPointF(tmpTP->getX(), tmpTP->getY());

			centerPosition (trackPoint);
		}
	}
}

void MultiplePictureViewer::initMapsOfPositions()
{
	/**
	* Problem: Positons can be arranged in any way, we don't know, how many neighbours we have and where they are
	* Idea:	Construct 4 rectangles completely covering the area where the buttons for the next positions should appear,
	*			like a ribbon around this position. Then intersect the rectangles of all other positions with them. Then we
	*			know, that at every intersection comes a neighbor position -> use intersected areas as button areas.
	*/

	if(!TATInformation::getInst()->wavelengthInformationExistsFor(0))
		return;

	// Check if coordinates are inverted
	const bool coorinateSystemInverted = TTTManager::getInst().coordinateSystemIsInverted();

	float mmpp = TATInformation::getInst()->getWavelengthInfo (0).getMicrometerPerPixel();

	// Get left and top of this position in micrometers
	int myLeft = positionManager->positionInformation.getLeft();
	int myTop = positionManager->positionInformation.getTop();

	// Get width and height of all pictures in pixels
	int widthPx = TATInformation::getInst()->getWavelengthInfo (0).getWidth();
	int heightPx = TATInformation::getInst()->getWavelengthInfo (0).getHeight();

	if(widthPx == -1 || heightPx == -1)
		return;

	// Create four rects that will be used for intersecting other positions
	QVector<QRect> vRects(4);

	// Top
	vRects[0] = QRect( QPoint(-MovieNextPosItem::BAR_DISTANCE_PIC - MovieNextPosItem::WIDTH,
		-MovieNextPosItem::WIDTH-MovieNextPosItem::BAR_DISTANCE_PIC),
		QPoint(widthPx+MovieNextPosItem::BAR_DISTANCE_PIC+MovieNextPosItem::WIDTH,
		-MovieNextPosItem::BAR_DISTANCE_PIC));	

	// Right
	vRects[1] = QRect(QPoint(widthPx + MovieNextPosItem::BAR_DISTANCE_PIC, -MovieNextPosItem::BAR_DISTANCE_PIC+1), QPoint(widthPx + MovieNextPosItem::BAR_DISTANCE_PIC + MovieNextPosItem::WIDTH, heightPx+MovieNextPosItem::BAR_DISTANCE_PIC-1));

	// Left
	vRects[2] = QRect(QPoint(-MovieNextPosItem::BAR_DISTANCE_PIC - MovieNextPosItem::WIDTH, -MovieNextPosItem::BAR_DISTANCE_PIC+1), QPoint(-MovieNextPosItem::BAR_DISTANCE_PIC, heightPx+MovieNextPosItem::BAR_DISTANCE_PIC-1));

	// Bottom
	vRects[3] = QRect(QPoint( -MovieNextPosItem::BAR_DISTANCE_PIC - MovieNextPosItem::WIDTH,
		heightPx + MovieNextPosItem::BAR_DISTANCE_PIC),
		QPoint(widthPx + MovieNextPosItem::BAR_DISTANCE_PIC + MovieNextPosItem::WIDTH,
		heightPx + MovieNextPosItem::BAR_DISTANCE_PIC + MovieNextPosItem::WIDTH));	

	// Iterate over all positions
	QHash<int, TTTPositionManager*>& allPositions = TTTManager::getInst().getAllPositionManagers();
	for (auto iter = allPositions.begin(); iter != allPositions.end(); ++iter) {
		// Skip current. As there is only one pos manager per position we can do this by comparing pointers
		if((*iter) == positionManager)
			continue;

		// Get left and top of cur position in micrometers
		int curLeft = (*iter)->positionInformation.getLeft();
		int curTop = (*iter)->positionInformation.getTop();

		// Construct micrometer rect describing current position
		QRect curRect(curLeft, curTop, widthPx*mmpp, heightPx*mmpp);

		// Convert to pixel rect relative to the position of this picture viewer
		QRect curRectPixel = MovieNextPosItem::transformRectToPixel(curRect, myLeft, myTop, mmpp);

		// Gotta do a little translation if coordinate system is inverted
		if(coorinateSystemInverted)
			curRectPixel.translate(2 * -curRectPixel.left(), 2 * -curRectPixel.top());

		// Check for overlap on every side
		for(int i = 0; i < vRects.size(); ++i) {
			if(curRectPixel.intersects(vRects[i])) {
				// Get overlapping area, this will be the button area
				// Note: buttons can and will overlap, but this is not a problem
				QRect intersection = curRectPixel.intersected(vRects[i]);

				// Get index now
				QString curIndex = (*iter)->positionInformation.getIndex();

				// Insert button into all picture views
				for(int j = 0; j < picViews.size(); ++j) {
					// Create button
					MovieNextPosItem* posButton = new MovieNextPosItem(curIndex);
					posButton->setPos(0, 0);
					posButton->setRect(intersection);

					// Add button to picture view 
					//picViews[j]->scene()->addItem(posButton);
					picViews[j]->addPositionButton(posButton);
				}
			}
		}
	}
}

void MultiplePictureViewer::showPositionButtons( bool _show )
{
	// Simply redirect to picture views
	for(int j = 0; j < picViews.size(); ++j) 
		picViews[j]->showPositionButtons(_show);
}

void MultiplePictureViewer::setShowPreviousTrackPoints( bool _on, int _numPrevTrackPointsToDraw, int _widthOfTails, bool _drawPrevTrackPointsTransparencyEffect, bool _drawPrevTrackPointsOnlyOfSelectedCellBranch )
{
	// Simply redirect to picture views
	for(int j = 0; j < picViews.size(); ++j) {
		if(picViews[j]->isVisible()) {
			picViews[j]->setShowPreviousTrackPoints(_on, _numPrevTrackPointsToDraw, _widthOfTails, _drawPrevTrackPointsTransparencyEffect, _drawPrevTrackPointsOnlyOfSelectedCellBranch);
			
			// Enable only the first active picture view
			if(_on)
				break;
		}
	}
}


void MultiplePictureViewer::setDrawSegmentation( bool _on )
{
	// Simply redirect to picture views
	for(int j = 0; j < picViews.size(); ++j) 
		picViews[j]->setDrawSegmentation(_on);
}

void MultiplePictureViewer::setMarkActivePictureView(bool on)
{
	// Update marking of active pic view
	int currentWl = positionManager->getWavelength();
	for(int wl = 0; wl < picViews.size(); ++wl) {
		if(picViews[wl]) {
			picViews[wl]->setHighlightingOn(on && wl == currentWl && multipleView);
		}
	}
}


