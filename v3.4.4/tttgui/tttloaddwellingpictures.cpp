/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * DEPRECATED!
 */

#include "tttloaddwellingpictures.h"

// Project
#include "tttgui/tttmovie.h"
#include "tttgui/tttpositionlayout.h"
#include "tttbackend/picturearray.h"



TTTLoadDwellingPictures::TTTLoadDwellingPictures(QWidget *parent, const char *name)
    :QDialog(parent, name)
{
	
	///@todo:
	///- multiple cell selection => multiple last timepoints: ask Philipp how to solve; currently only the "current cell" is considered
	///- wavelength selection?
        setupUi (this);
	
	//load settings for the current user
	
	liePicturesBefore->setText (QString ("%1").arg (UserInfo::getInst().getStyleSheet().getPicturesToLoadBeforeStop()));
	liePicturesAfter->setText (QString ("%1").arg (UserInfo::getInst().getStyleSheet().getPicturesToLoadAfterStop()));
	calcTimepoints();
	
	chkShowMovieWindows->setChecked (true);
	
	active_count = 0;
	
	connect ( pbtCancel, SIGNAL (clicked()), this, SLOT (cleanup_n_close()));
	connect ( pbtLoad, SIGNAL (clicked()), this, SLOT (startLoading()));
	
	connect ( liePicturesBefore, SIGNAL (textChanged (const QString &)), this, SLOT (calcTimepoints()));
	connect ( liePicturesAfter, SIGNAL (textChanged (const QString &)), this, SLOT (calcTimepoints()));
	
	
	
	//load position layout
	//positionView = new PositionDisplay ((QWidget *)grbPositionLayout);
	connect ( positionView, SIGNAL (positionLeftClicked (const QString&)), this, SLOT (activatePosition (const QString&)));
	connect ( positionView, SIGNAL (positionRightClicked (const QString&)), this, SLOT (deactivatePosition (const QString&)));
	
	
	//we need to backup the pointers to position thumbnails in the position layout, as they are overwritten here
	//the connections between the class PositionThumbnail and TTTPositionManager are possibly badly programmed, so it wouldn't be bad to rewrite the thumbnail display
	
	positionView->initialize(true, false);
	
	//PositionThumbnail *pt = 0;
	//Q3DictIterator<TTTPositionManager> iter (TTTManager::getInst().getAllPositionManagers());
	//for ( ; iter.current(); ++iter) {
	//	
	//	if (! iter.current()->isAvailable())
	//		continue;
	//	
	//	QString picfilename = "";
	//	
	//	if (FASTLOAD_WITHOUT_PICTURES)
	//		picfilename = "";
	//	else
	//		picfilename = iter.current()->getPictureDirectory() + "/" + iter.current()->getBasename (true) + "00001_w0.jpg";
	//	
	//	pt = new PositionThumbnail (positionView->viewport(), iter.current()->positionInformation.getIndex(), picfilename, iter.current()->positionInformation.getComment(), true);
	//	
	//	if (iter.current()->getPictures())
	//		if (iter.current()->getPictures()->getLoadedPictures() > 0)
	//			pt->markPicturesLoaded (true);
	//	
	//	positionView->addChildLocal (pt, iter.current()->positionInformation.getLeft(), iter.current()->positionInformation.getTop());
	//	//if (iter.current()->positionThumbnail)
	//	//	iter.current()->backupPositionThumbnail = iter.current()->positionThumbnail;
	//	//iter.current()->positionThumbnail = pt;
	//}
	//
	////positionView->setConnections();
	//positionView->setFactors (TTTManager::getInst().getOcularFactor(), TTTManager::getInst().getTVFactor());
	//positionView->setInverted (TTTManager::getInst().coordinateSystemIsInverted());
	//positionView->refresh();

	windowLayoutLoaded = false;
	
}

void TTTLoadDwellingPictures::resizeEvent (QResizeEvent *)
{
	//positionView->fit();
}

void TTTLoadDwellingPictures::closeEvent (QCloseEvent *_ev)
{
	// Save window layout
	UserInfo::getInst().saveWindowPosition(this, "TTTLoadDwellingPictures");

	cleanup_n_close();
	_ev->accept();
}

//EVENT:
void TTTLoadDwellingPictures::showEvent (QShowEvent *_ev)
{
	if(!_ev->spontaneous() && !windowLayoutLoaded) {
		// Load window layout
		UserInfo::getInst().loadWindowPosition(this, "TTTLoadDwellingPictures");

		windowLayoutLoaded = true;
	}

	_ev->accept();
}

//SLOT:
void TTTLoadDwellingPictures::cleanup_n_close()
{
	////restore position thumbnail pointers in all position managers
	//Q3DictIterator<TTTPositionManager> iter (TTTManager::getInst().getAllPositionManagers());
	//for ( ; iter.current(); ++iter) {
	//	if (iter.current()->backupPositionThumbnail) {
	//		iter.current()->positionThumbnail = iter.current()->backupPositionThumbnail;
	//		iter.current()->backupPositionThumbnail = 0;
	//	}
	//}
	
	accept();
}

void TTTLoadDwellingPictures::markPosition (const QString& _pos, bool _active)
{
	PositionThumbnail *tn = positionView->getThumbnail (_pos);
	
	if (tn) {
		tn->setSelected (_active);
		tn->updateDisplay();
		active_count++;
	}
}

void TTTLoadDwellingPictures::writeSelectedCells (const QString& _cells)
{
	lblSelectedCells->setText (_cells);
}

void TTTLoadDwellingPictures::activatePosition (const QString& _index)
{
	if (positionView->selectPosition(_index))
		active_count++;
}

void TTTLoadDwellingPictures::deactivatePosition (const QString& _index)
{
	if (positionView->deSelectPosition(_index))
		active_count--;
}

//SLOT:
void TTTLoadDwellingPictures::calcTimepoints()
{
	int bef = liePicturesBefore->text().toInt();
	int aft = liePicturesAfter->text().toInt();
	
	if ((bef * aft) > 0) {
		if (TTTManager::getInst().getCurrentTrack()) {
			int tp = TTTManager::getInst().getCurrentTrack()->getLastTrace();
			if (tp == -1)		//not yet tracked
				tp = TTTManager::getInst().getCurrentTrack()->getFirstTrace();
			
			lblTimepoints->setText (QString ("Timepoints: %1 - %2").arg (tp - bef).arg (tp + aft));
		}
	}
}
	
//SLOT:
void TTTLoadDwellingPictures::startLoading()
{
	//save settings (picture counts)
	UserInfo::getInst().getRefStyleSheet().setPicturesToLoadBeforeStop (liePicturesBefore->text().toInt());
	UserInfo::getInst().getRefStyleSheet().setPicturesToLoadAfterStop (liePicturesAfter->text().toInt());
	
	
	//trigger loading procedures
	//find out positions that are selected
	//determine timepoints
	
	int tp = TTTManager::getInst().getCurrentTrack()->getLastTrace();
	int bef = liePicturesBefore->text().toInt();
	int aft = liePicturesAfter->text().toInt();
	
	fraStatusBar->show();
	StatusBar sb (fraStatusBar, "Loading pictures...", 1, active_count);
	sb.display (true);

	// Iterate over selected positions
	QList<TTTPositionManager*> positions = positionView->getActivePositions();
	int counter = 0;
	for(QList<TTTPositionManager*>::iterator it = positions.begin(); it != positions.end(); ++it) {
		// Get position manager
		TTTPositionManager* tttpm = *it;

		sb.update();

		////NOTE:
		////if this position was not yet clicked/activated in the position layout, all the activation routines need to be done here!
		////the simplest way of detecting is to test whether the attributes tttpm->getPictures() already points to a defined region
		//if (! initializePositionManager (tttpm))
		//	continue;

		if(!TTTManager::getInst().frmPositionLayout->setPosition(tttpm->positionInformation.getIndex()/*, true*/))
			continue;

		if (tttpm->isLocked() || (! tttpm->isAvailable()))
			//position is not available due to some reason
			continue;

		for (int timepoint = tp - bef; timepoint <= tp + aft; timepoint++) {
			//currently only for wavelength 0
			tttpm->getPictures()->setLoading (timepoint, 1, 0, true);
		}

		//load all pictures selected for loading
		tttpm->getPictures()->loadPictures (false, false);

		if (chkShowMovieWindows->isChecked()) {
			//show movie windows for this position
			//note: do not change the "current position"
			if (tttpm->frmMovie) {
				tttpm->frmMovie->show();
			}
		}

		//mark this position in tttpositionlayout to have loaded pictures
		if (tttpm->posThumbnailInTTTPosLayout)
			tttpm->posThumbnailInTTTPosLayout->markPicturesLoaded (true);
	}	
	
	sb.display (false);
	fraStatusBar->hide();
	
	cleanup_n_close();
}

bool TTTLoadDwellingPictures::initializePositionManager (TTTPositionManager *_tttpm)
{
	///@todo change to TTTPM->initialize() + additional functions
	
	//prepares a position manager for loading pictures by initializing all the necessary variables
	
	
	if (! _tttpm)
		return false;
	
	if (_tttpm->getPictures())
		//already initialized!
		return true;
	
	QString posIndex = _tttpm->positionInformation.getIndex();
	
	_tttpm->setBasename (TTTManager::getInst().getExperimentBasename() + POSITION_MARKER + posIndex);
	QString suffix = "";
	suffix = TTTManager::getInst().frmPositionLayout->getImageSize (false, posIndex);
	TTTManager::getInst().frmPositionLayout->initLoadingRegion (posIndex);
	
	//TTTManager::getInst().frmPositionLayout->initPositionManager (_tttpm, suffix);

	_tttpm->initialize(/*suffix*/);
	
	
	RectBox loadingRegion = _tttpm->positionInformation.getLoadingRegion();
	_tttpm->getDisplays().setLoadingRegion (-1, loadingRegion.left(), loadingRegion.top(), loadingRegion.width(), loadingRegion.height());
//	_tttpm->getDisplays().setZoomFactor (-1, 100);		//100 %
//	_tttpm->getDisplays().setDisplayedRegion (-1, loadingRegion.left(), loadingRegion.top(), _tttpm->frmMovie->fraPictures->width(), _tttpm->frmMovie->fraPictures->height());
	
	
	//initialize all pictures from the folder
	QRect imageRect = _tttpm->positionInformation.getImageRects (0);
	
        QVector<QString> pics = SystemInfo::listFiles (_tttpm->getPictureDirectory(), QDir::Files, "*.jpg;*.tif", false);
        QVector<QString>::const_iterator iterFiles = pics.constBegin();
	

		while (iterFiles != pics.constEnd()) {
			const QString picFilename = *iterFiles;
			
			int wavelength = FileInfoArray::CalcWaveLengthFromFilename (picFilename);
			int timepoint = FileInfoArray::CalcTimePointFromFilename (picFilename);
			
			//_tttpm->getPictures()->initPicture (timepoint, wavelength, picFilename, false, imageRect);
			
			iterFiles++;
		}
	
	connect ( _tttpm->getPictures(), SIGNAL (LoadingComplete()), _tttpm->frmMovie, SLOT (updateView()));
	connect ( _tttpm->getPictures(), SIGNAL (LoadedPicture (unsigned int, int, bool)), _tttpm->frmMovie, SLOT (updateView()));
	//connect ( _tttpm->getPictures(), SIGNAL (LoadedPicture (unsigned int, int, bool)), TTTManager::getInst().frmTracking, SLOT (updateTimeScale (unsigned int, int)));
	
	return true;
}

//#include "tttloadpicturepositions.moc"
