/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MOVIEMENUBAR_H
#define MOVIEMENUBAR_H

// Qt includes
#include <QMenuBar>

// Forward declarations
class TTTMovie;
class QSignalMapper;


/**
 * Menu of TTTMovie window
 */
class MovieMenuBar : public QMenuBar {
public:
	MovieMenuBar(TTTMovie* _parent);

	~MovieMenuBar();

private:
	///creates all menus
	void createMenus();

	///connects actions
	void connectActions();

///Private data

	///movie window this menu belongs to
	TTTMovie* movieWindow;

	///Menus
	QMenu *fileMenu;
	QMenu *fileSubMenuColonyCreation;

	//QMenu *editMenu;

	QMenu *viewMenu;
	QMenu *viewSubMenuTracks;
	QMenu *viewSubMenuCellPath;

	QMenu *trackingMenu;
	QMenu *trackingSubMenuStopReason;
	QMenu *trackingSubMenuCellProperties;
	QMenu *trackingSubMenuSelectOtherCell;
	QMenu *trackingSubMenuFluorescence;
	QMenu *trackingSubMenuDifferentiation;
	QMenu *trackingSubMenuDifferentiationBlood;
	QMenu *trackingSubMenuDifferentiationBloodMyeloid;
	QMenu *trackingSubMenuDifferentiationBloodLymphoid;
	QMenu *trackingSubMenuDifferentiationBloodHSC;
	QMenu *trackingSubMenuAdherence;
	// Blood-Primitive	-> QAction
	QMenu *trackingSubMenuDifferentiationStroma;
	QMenu *trackingSubMenuDifferentiationStromaPrimary;
	QMenu *trackingSubMenuDifferentiationStromaCellLine;
	// Endothelium	-> QAction
	// Heart -> QAction
	QMenu *trackingSubMenuDifferentiationNerve;
	// Endoderm	-> QAction
	// Mesoderm	-> QAction

	QMenu *exportMenu;
	QMenu *exportSubMenuTree; 
	QMenu *exportSubMenuData;

	QMenu *toolsMenu;

	QMenu *helpMenu;

///Public data
public:

	///Actions

    ///file menu
    QAction *actFile_COLONYCREATION;
    QAction *actFile_COLONYCREATION_SELECT;
    QAction *actFile_COLONYCREATION_CREATE;
    QAction *actFile_DELETECOLONY;
    QAction *actFile_CLOSE_WINDOW;

    /////edit menu
    //QAction *actEdit_CELLPROPS;
    //QAction *actEdit_CELLPROPS_WAVELENGTH;
    //QAction *actEdit_CELLPROPS_ADHERENCE;
    //QAction *actEdit_CELLPROPS_TISSUETYPE;
    //QAction *actEdit_CELLPROPS_GENERALTYPE;
    //QAction *actEdit_CELLPROPS_LINEAGE;
    //QAction *actEdit_CELLPROPS_CELLCYCLE;
    //QAction *actEdit_CELLPROPS_CELLCYCLE_G0;
    //QAction *actEdit_CELLPROPS_CELLCYCLE_G1;
    //QAction *actEdit_CELLPROPS_CELLCYCLE_G2;
    //QAction *actEdit_CELLPROPS_CELLCYCLE_S;
    //QAction *actEdit_CELLPROPS_COMMENT;
    //QAction *actEdit_CELLPROPS_WINDOW;
    //QAction *actEdit_SELECTCELLS_AUTOMATED_DESELECTION;
    //QAction *actEdit_SELECTCELLS;
    //QAction* actEdit_CELL_COMMENT;
    //QAction *actEdit_UNDO;
    //QAction *actEdit_REDO;

    ///view menu
    //QAction *actView_TIME;
    QAction *actView_CIRCLEMOUSE;
    //QAction *actView_CELLS;
    //QAction *actView_CELLS_GATE;
    //QAction *actView_CELLS_NUMBERS;
    //QAction *actView_CELLS_APPLYTOALLWL;
    QAction *actView_HIDECONTROLS;
    QAction *actView_SYMBOLS;
    //QAction *actView_RULER;
    //QAction *actView_SCALE;
    //QAction *actView_CELL_PATH;

	QAction *actView_TRACKS_TRACKS;
	QAction *actView_TRACKS_TRACKNUMBERS;
	QAction *actView_TRACKS_SETFORALLWLS;
	QAction *actView_TRACKS_POSITION_TRACKS;
	QAction *actView_TRACKS_ALL_POSITIONS_TRACKS;
	QAction *actView_TRACKS_FIRST_TRACKS;
	QAction *actView_TRACKS_BACKGROUND_TRACKS;

    QAction *actView_CELL_PATH_ADDCURRENT;
    QAction *actView_CELL_PATH_CLEARPATHS;
    //QAction *actView_ZOOM;
    //QAction *actView_ZOOM_FULL;
    //QAction *actView_ZOOM_50;
    //QAction *actView_ZOOM_100;
    //QAction *actView_ZOOM_200;
    //QAction *actView_ZOOM_PLUS25;
    //QAction *actView_ZOOM_MINUS25;
    //QAction *actView_TREE_OVERVIEW;
    QAction *actView_CUSTOMIZE;
    //QAction *actView_WLINFO;
    //QAction *actView_CELLDATA;
    QAction *actView_CENTERCELL;
    QAction *actView_GAMMA;
    //QAction *actView_COLOCATION;o
    //QAction *actView_COLOCATION_SELECTRADIUS;
    //QAction *actView_COLOCATION_CURRENTCELL;
    //QAction *actView_COLOCATION_ALLCELLS;
    //QAction *actView_SPEED;
    //QAction *actView_RESET_TREE_DISPLAY;

    ///tracking menu
    QAction *actTracking_START;
    //QAction *actTracking_STOPREASON;
    QAction *actTracking_STOPREASON_APOPTOSIS;
    QAction *actTracking_STOPREASON_DIVISION;
    QAction *actTracking_STOPREASON_LOST;
	QAction *actTracking_STOPREASON_INTERRUPT;
	QAction *actTracking_STOPREASON_DIVISION_CONTINUETRACKING;

	QVector<QAction*> actTracking_PROPERTY_WLS;

	QAction *actTracking_PROPERTY_DIFF_BLOOD_MYLOID_MEGA;
	QAction *actTracking_PROPERTY_DIFF_BLOOD_MYLOID_ERY;
	QAction *actTracking_PROPERTY_DIFF_BLOOD_MYLOID_MAKRO;
	QAction *actTracking_PROPERTY_DIFF_BLOOD_MYLOID_NEUTRO;
	QAction *actTracking_PROPERTY_DIFF_BLOOD_MYLOID_EOSINO;
	QAction *actTracking_PROPERTY_DIFF_BLOOD_MYLOID_BASO;
	QAction *actTracking_PROPERTY_DIFF_BLOOD_MYLOID_MAST;
	QAction *actTracking_PROPERTY_DIFF_BLOOD_MYLOID_DENDRITIC;
	QAction *actTracking_PROPERTY_DIFF_BLOOD_MYLOID_NONE;
	QAction *actTracking_PROPERTY_DIFF_BLOOD_LYMPHOID_B;
	QAction *actTracking_PROPERTY_DIFF_BLOOD_LYMPHOID_T;
	QAction *actTracking_PROPERTY_DIFF_BLOOD_LYMPHOID_NK;
	QAction *actTracking_PROPERTY_DIFF_BLOOD_LYMPHOID_NONE;
	QAction *actTracking_PROPERTY_DIFF_BLOOD_HSC_LTR;
	QAction *actTracking_PROPERTY_DIFF_BLOOD_HSC_STR;
	QAction *actTracking_PROPERTY_DIFF_BLOOD_HSC_MPP;
	QAction *actTracking_PROPERTY_DIFF_BLOOD_HSC_CMP;
	QAction *actTracking_PROPERTY_DIFF_BLOOD_HSC_GMP;
	QAction *actTracking_PROPERTY_DIFF_BLOOD_HSC_MEP;
	QAction *actTracking_PROPERTY_DIFF_BLOOD_HSC_NONE;
	QAction *actTracking_PROPERTY_DIFF_BLOOD_PRIMITIVE;
	QAction *actTracking_PROPERTY_DIFF_BLOOD_NONE;
	QAction *actTracking_PROPERTY_DIFF_STROMA_PRIM_WBM;
	QAction *actTracking_PROPERTY_DIFF_STROMA_PRIM_OB;
	QAction *actTracking_PROPERTY_DIFF_STROMA_PRIM_EC;
	QAction *actTracking_PROPERTY_DIFF_STROMA_PRIM_NONE;
	QAction *actTracking_PROPERTY_DIFF_STROMA_CELLLINE_AFT;
	QAction *actTracking_PROPERTY_DIFF_STROMA_CELLLINE_2018;
	QAction *actTracking_PROPERTY_DIFF_STROMA_CELLLINE_BFC;
	QAction *actTracking_PROPERTY_DIFF_STROMA_CELLLINE_OP9;
	QAction *actTracking_PROPERTY_DIFF_STROMA_CELLLINE_PA6;
	QAction *actTracking_PROPERTY_DIFF_STROMA_CELLLINE_NONE;
	QAction *actTracking_PROPERTY_DIFF_STROMA_NONE;
	QAction *actTracking_PROPERTY_DIFF_ENDOTHELIUM;
	QAction *actTracking_PROPERTY_DIFF_HEART;
	QAction *actTracking_PROPERTY_DIFF_NERVE_NEURO;
	QAction *actTracking_PROPERTY_DIFF_NERVE_GLIA;
	QAction *actTracking_PROPERTY_DIFF_NERVE_NONE;
	QAction *actTracking_PROPERTY_DIFF_ENDODERM;
	QAction *actTracking_PROPERTY_DIFF_MESODERM;
	QAction *actTracking_PROPERTY_DIFF_TISSUE_NONE;		// No tissue/general/lineage type set
	QActionGroup differentiationGroup;
	QSignalMapper *differentiationMapper;
	QAction *actTracking_PROPERTY_ADHERENCE_ADHERENT;
	QAction *actTracking_PROPERTY_ADHERENCE_FREEFLOATING;
	QAction *actTracking_PROPERTY_ADHERENCE_SEMIADHERENT;
	QAction *actTracking_PROPERTY_ADHERENCE_NONE;
	QActionGroup adherenceGroup;
	QAction *actTracking_PROPERTY_ENDOMITOSIS;
    //QAction *actTracking_STOPREASON_TRIVISION;
    //QAction *actTracking_STOPREASON_NECROSIS;
    //QAction *actTracking_QUANTIFICATION;
    //QAction *actTracking_QUANTIFICATION_BACKGROUNDTRACKING;
    //QAction *actTracking_QUANTIFICATION_OVERVIEW;
    //QAction *actTracking_QUANTIFICATION_;
    //QAction *actTracking_DELETECELL;
    //QAction *actTracking_DELETESTOPREASON;
    //QAction *actTracking_DELETECOLONY;
	//QAction *actTracking_DELETE_TRACKPOINTS;
	QAction* actTracking_SELECTCELL_SISTER;
	QAction* actTracking_SELECTCELL_MOTHER;
	QAction* actTracking_SELECTCELL_DAUGHTER1;
	QAction* actTracking_SELECTCELL_DAUGHTER2;
	QSignalMapper *selectCellMapper;

    ///export menu
    //QAction *actExport_MOVIE_OPTIONS;
    QAction *actExport_MOVIE;
	QAction *actExport_MOVIESPECIAL;
	QAction *actExport_DIVISION;
    //QAction *actExport_TREE;
    //QAction *actExport_TREE_CURRENT_VIEW;
    //QAction *actExport_TREE_COMPLETE_TREE;
    //QAction *actExport_TREE_COMPLETE_TREE_UNSTRETCHED;
    //QAction *actExport_TREE_ALL_TREES_OF_POSITION;
    //QAction *actExport_DATA;
    //QAction *actExport_DATA_DIVISION_INTERVALS;
    //QAction *actExport_DATA_FIRST_DIVISION_TIME;
    //QAction *actExport_DATA_APOPTOSIS_TIME;
    //QAction *actExport_DATA_CELL_SPEED;
    //QAction *actExport_DATA_ALL_SELECTED_CELLS;
    //QAction *actExport_DATA_CELL_COUNT;
    //QAction *actExport_DATA_FLUORESCENCE;
    //QAction *actExport_;

    ///tools menu
	QAction *actTools_PLAYER;
	QAction *actTools_STATTTS;
	QAction *actTools_AMT;
	QAction *actTools_SPECIALEFFECTS;
    //QAction *actTools_STATISTICS;
    //QAction *actTools_STATISTICS_EASY;
    //QAction *actTools_STATISTICS_ADVANCED;
    //QAction *actTools_AMT;
    //QAction *actTools_MOVIEPLAYER;
    //QAction *actTools_POSITIONHISTORY;
    //QAction *actTools_MATLABINTEGRATION;
    //QAction *actTools_CUSTOMIZETTT;
    //QAction *actTools_STOPWATCH;
    //QAction *actTools_MININGTOOLS;
    //QAction *actTools_TREELEGEND;

    ///help menu
    //QAction *actHelp_SEARCH;
    //QAction *actHelp_TUTORIAL;
    //QAction *actHelp_TOTD;	//tip of the day
    //QAction *actHelp_EDITORIAL;
    //QAction *actHelp_COPYLEFT;
    //QAction *actHelp_ABOUTQT;
	QAction *actHelp_SHOWLOGFILEPATH;
};


#endif