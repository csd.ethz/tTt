/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CLICKABLEQGRAPHICSVIEW_H
#define CLICKABLEQGRAPHICSVIEW_H

#include "callbackitem.h"

#include <QGraphicsView>
#include <QWidget>

class ClickableQGraphicsView : public QGraphicsView, public CallbackItem
{
        Q_OBJECT

public:

        ClickableQGraphicsView (QWidget *_parent = 0, const QString &_parameter = "");

        void mousePressEvent (QMouseEvent *_ev);
        void mouseReleaseEvent (QMouseEvent *_ev);
        void dragEnterEvent (QDragEnterEvent *_ev);
        void dragMoveEvent (QDragMoveEvent *_ev);
        void dragLeaveEvent (QDragLeaveEvent *_ev);
        void dropEvent (QDropEvent *_ev);

private:

        ///whether dragging is currently in progress over this view
        bool dragOver;
};

#endif // CLICKABLEQGRAPHICSVIEW_H
