/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "myqchecklistitem.h"


MyQCheckListItem::~MyQCheckListItem()
{
}


void MyQCheckListItem::paintCell (QPainter *_p, const QColorGroup &_cg, int _column, int _width, int _align)
{
        _p->save();
        //_p->setFont (QFont ("Arial", 12));
        QColorGroup grp(_cg);
        grp.setColor (QColorGroup::Text, color);
        Q3CheckListItem::paintCell (_p, grp, _column, _width, _align);
        _p->restore();
}


