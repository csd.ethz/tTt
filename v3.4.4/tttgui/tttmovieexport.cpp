/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
// tTt
#include "tttmovie.h"
#include "tttmovieexport.h"
#include "tttdata/stylesheet.h"
#include "tttbackend/tttexception.h"
#include "tttdata/userinfo.h"
#include "tttbackend/tttpositionmanager.h"
#include "tttbackend/tttmanager.h"
#include "tttgammaadjust.h"

// QT
#include <QMessageBox>
#include <QFileDialog>

// STL
#include <exception>


// Static member definitions
const char* TTTMovieExport::SETTINGS_FILENAME = "exportSettings.bin";


TTTMovieExport::TTTMovieExport(TTTMovie *_parent, int _minTimePoint, int _maxTimePoint) : QDialog(_parent)
{
	setupUi(this);

	// Disable context-help button
	setWindowFlags( (windowFlags() | Qt::CustomizeWindowHint) & (~Qt::WindowContextHelpButtonHint));

	movieWindow = _parent;
	minTimePoint = _minTimePoint;
	maxTimePoint = _maxTimePoint;

	// Signals/Slots
	connect(pbtStart, SIGNAL(clicked()), this, SLOT(startExport()));
	connect(pbtCancel, SIGNAL(clicked()), this, SLOT(cancelExport()));
	connect(pbtClose, SIGNAL(clicked()), this, SLOT(close()));
	connect(pbtTimeboxPositionHelp, SIGNAL(clicked()), this, SLOT(displayTimePositionHelp()));
	connect(pbtStackRepeatsHelp, SIGNAL(clicked()), this, SLOT(displayStackRepeatsHelp()));
	connect(lieStartTp, SIGNAL(textEdited(const QString&)), this, SLOT(firstExportTimePointEdited(const QString&)));
	connect(pbtStartTpCurrent, SIGNAL(clicked()), this, SLOT(setTpToCurrent()));
	connect(pbtStopTpCurrent, SIGNAL(clicked()), this, SLOT(setTpToCurrent()));
	connect(pbtRelativeTpCurrent, SIGNAL(clicked()), this, SLOT(setTpToCurrent()));
	connect(chkDisplayTimebox, SIGNAL(toggled(bool)), this, SLOT(displayTimeBoxToggled(bool)));
	connect(optJPEG, SIGNAL(toggled(bool)), this, SLOT(formatJpegSelected(bool)));
	connect(optAVI, SIGNAL(toggled(bool)), this, SLOT(formatAviSelected(bool)));
	connect(optCenterOnTrackspoints, SIGNAL(toggled(bool)), this, SLOT(centerOnTrackpointsToggled(bool)));
	connect(pbtImportSettings, SIGNAL(clicked()), this, SLOT(importSettings()));

	// Gui initialization
	pbtCancel->setVisible(false);

	lieStartTp->setText(QString::number(minTimePoint));
	lieTimeStartTP->setText(QString::number(minTimePoint));
	//lieEndTp->setText(QString::number(movieWindow->LastExportTimePoint));

	// Load settings
	loadExportOptionsFromUserSettings();

	exporter = 0;
}

void TTTMovieExport::startExport()
{
	// If we have an exporter, an export is currently running
	if(exporter) {
		QMessageBox::critical(this, "Error", "Cannot start export while another export is running.");
		return;
	}

	// Validate input
	if(!validateInput())
		return;

	// Save options
	writeExportOptionsToUserSettings();

	// Get format
	if(optJPEG->isChecked())
		format = MovieExporter::JPEG;
	else if(optBMP->isChecked())
		format = MovieExporter::BMP;
	else if(optAVI->isChecked())
		format = MovieExporter::AVI;
	else
		format = MovieExporter::PNG;

	// Get bitrate
	int bitrate = 200000;
	if(format == MovieExporter::AVI) {
		bitrate = spbAviBitrate->value() * 1000;
	}

	//// Set options in movie window (ToDo: remove export tab from movie window and pass options as parameters or so)
	//movieWindow->setFirstExportTP(lieStartTp->text().toInt());
	//movieWindow->setLastExportTP(lieEndTp->text().toInt());
	//movieWindow->setTimeFontSize(spbFontSize->value());
	//movieWindow->setMovieTimeboxBackground(chkTimeOnBlack->isChecked());
	//movieWindow->setMovieTimeboxPosition(spbTimeboxPosition->value());
	//movieWindow->chkMovieExportTimebox->setChecked(chkDisplayTimebox->isChecked());
	//movieWindow->chkExportTimeBold->setChecked(chkTimeBold->isChecked());
	//movieWindow->spbExportTPRelative->setValue(lieTimeStartTP->text().toInt());
	//movieWindow->chkMovieExportAutoSize->setChecked(chkAutoSetSize->isChecked());
	//movieWindow->lieStackRepeat->setText(lieStackRepeat->text());

	// Specify export mode 
	if(optHideTrackpoints->isChecked())
		mode = MovieExporter::HIDE_TRACKS;
	else if(optCompleteNormal->isChecked())
		mode = MovieExporter::COMPLETE_NORMAL;
	else if(optCompleteAll->isChecked())
		mode = MovieExporter::COMPLETE_ALL;
	else if(optCompleteFirst->isChecked())
		mode = MovieExporter::COMPLETE_FIRST;
	else if(optCenterOnTrackspoints->isChecked())
		mode = MovieExporter::CENTER_ON_TRACKPOINTS;
	else
		mode = MovieExporter::NORMAL;
	
	//// Warn user about senseless choices of export mode
	//if(chkHideTracks->isChecked() && (chkCompleteAll->isChecked() || chkCompleteFirst->isChecked())) {
	//	QMessageBox::information(this, "Note", "Cannot show all tracks and hide all tracks at the same time -> hiding all.");
	//}
	//else if(chkCompleteAll->isChecked() && chkCompleteFirst->isChecked()) {
	//	QMessageBox::information(this, "Note", "Cannot show first only and all tracks at the same time -> showing all.");
	//}

	// Get timebox relative start tp and font size
	int timeboxRelativeTp = lieTimeStartTP->text().toInt(),
		timeboxFontSize = spbFontSize->value();

	// Get window size for center on trackpoints
	int centerOnTrackpointsPicSizeX = spbCenterPicSizeX->value();
	int centerOnTrackpointsPicSizeY = spbCenterPicSizeY->value();
	//if(mode == MovieExporter::CENTER_ON_TRACKPOINTS) {
	//	if(centerOnTrackpointsPicSize <= 0 || centerOnTrackpointsPicSize > 3000) {
	//		QMessageBox::critical(this, "Error", "Invalid value for \"Picture size\"");
	//		return;
	//	}
	//}

	// Get fps
	int fps = 1;
	if(format == MovieExporter::AVI) {
		fps = lieFPS->text().toInt();
		if(fps <= 0){
			QMessageBox::critical(this, "Error", "Invalid value for \"Frames per second\"");
			return;
		}
	}

	// Now start export
	try {
		// Show cancel button
		pbtCancel->setVisible(true);

		// If new throws an exception, exporter will remain 0
		exporter = new MovieExporter(movieWindow, 
								lieStartTp->text().toInt(), 
								lieEndTp->text().toInt(),
								format,
								spbJPGQuality->value(),
								mode,
								chkAutoSetSize->isChecked(),
								chkDisplayTimebox->isChecked(),
								chkTimeBold->isChecked(),
								timeboxFontSize,
								timeboxRelativeTp,
								chkDrawWavelength->isChecked(),
								lieStackRepeat->text(),
								QSize(centerOnTrackpointsPicSizeX, centerOnTrackpointsPicSizeY),
								fps,
								bitrate);

		// Perform export
		exporter->runExport();

		// Export settings
		exportSettings(exporter->getDirectory());
	}
	catch(const TTTException& err) {
		QMessageBox::critical(movieWindow, "Error", "Could not export movie: " + err.what());
	}
	catch(std::exception& err) {
		QMessageBox::critical(movieWindow, "Error", QString("Could not export movie: STL-Error: ") + err.what());
	}

	if(exporter) {
		delete exporter;
		exporter = 0;
	}

	// Hide cancel button
	pbtCancel->setVisible(false);
}

bool TTTMovieExport::validateInput()
{
	// Validate data
	if(lieStartTp->text().toInt() < minTimePoint || lieEndTp->text().toInt() > maxTimePoint) {
		QMessageBox::warning(this, "Error", "Invalid timepoints.");
		return false;
	}

	if(optAVI->isChecked()) {
		if(lieFPS->text().toInt() < 3) {
			if(QMessageBox::warning(this, "Warning", "Note: frame rates below 3 can cause severe problems (e.h. exported movie cannot be played).", QMessageBox::Ok, QMessageBox::Cancel) != QMessageBox::Ok)
				return false;
		}
	}

	return true;
}

void TTTMovieExport::cancelExport()
{
	//movieWindow->cancelMovieExport();
	if(exporter)
		exporter->abortExport();
}

void TTTMovieExport::displayTimePositionHelp()
{
	QMessageBox::information(this, "Timebox position", "1 = top left\n2 = top middle\n3 = top right\n4 = middle left\n"
				"5 = middle middle\n6=middle right\n7 = bottom left\n8 = bottom middle\n9 = bottom right.");
}

void TTTMovieExport::displayStackRepeatsHelp()
{
	QString stackHelp = "Repeat some frames. Format: \"[tp1-tp2:repeats]\" x n.\n\n"
		"E.g. \"[10-15:3]\" to repeat each frame between timepoints 10 and 15 3 times or "
		"\"[20-20:5]\" to repeat only timepoint 20 5 times.\nCan be combined: \"[10-15:3][20-20:5]\".";

	QMessageBox::information(this, "Stack repeats help", stackHelp);
}

void TTTMovieExport::loadExportOptionsFromUserSettings()
{
	StyleSheet& ss = UserInfo::getInst().getRefStyleSheet();

	int bg = ss.getTimeboxBackground();
	chkTimeOnBlack->setChecked(bg == 1);

	// ToDo: Load other options too?
}

void TTTMovieExport::firstExportTimePointEdited( const QString& _text )
{
	//lieTimeStartTP->setText(_text);
}

void TTTMovieExport::writeExportOptionsToUserSettings()
{
	StyleSheet& ss = UserInfo::getInst().getRefStyleSheet();

	int bg = chkTimeOnBlack->isChecked() ? 1 : 0;
	ss.setTimeboxBackground (bg);

	movieWindow->setMovieTimeboxPosition(spbTimeboxPosition->value());

	// ToDo: Save other options too?
}

TTTMovieExport::~TTTMovieExport()
{
	if(exporter) {
		delete exporter;
		exporter = 0;
	}
}

void TTTMovieExport::setTpToCurrent()
{
	// Get position manager
	if(!movieWindow)
		return;
	TTTPositionManager* posManager = movieWindow->getPositionManager();
	if(!posManager)
		return;

	// Get current timepoint
	int curTP = posManager->getPictureIndex().TimePoint;

	// Set text field corresponding to sender to current timepoint
	if(curTP <= maxTimePoint && curTP >= minTimePoint) {
		QString sCurTP = QString("%1").arg(curTP);

		if(sender() == pbtStartTpCurrent)
			lieStartTp->setText(sCurTP);
		else if(sender() == pbtStopTpCurrent)
			lieEndTp->setText(sCurTP);
		else if(sender() == pbtRelativeTpCurrent)
			lieTimeStartTP->setText(sCurTP);
	}
}

void TTTMovieExport::displayTimeBoxToggled( bool _on )
{
	// Enable/disable related controls
	chkTimeBold->setEnabled(_on);
	chkTimeOnBlack->setEnabled(_on);
	spbFontSize->setEnabled(_on);
	spbTimeboxPosition->setEnabled(_on);
	lieTimeStartTP->setEnabled(_on);
	pbtRelativeTpCurrent->setEnabled(_on);
}

void TTTMovieExport::formatAviSelected( bool _on )
{
	lieFPS->setEnabled(_on);
	spbAviBitrate->setEnabled(_on);

	// Enable jpeg also for avi
	spbJPGQuality->setEnabled(_on || optJPEG->isChecked());
}

void TTTMovieExport::formatJpegSelected( bool _on )
{
	// Enable jpeg also for avi
	spbJPGQuality->setEnabled(_on || optAVI->isChecked());
}

void TTTMovieExport::centerOnTrackpointsToggled( bool _on )
{
	spbCenterPicSizeX->setEnabled(_on);	
	spbCenterPicSizeY->setEnabled(_on);
}

void TTTMovieExport::importSettings()
{
	// Proposed folder
	QString movieFolder = TTTManager::getInst().getNASDrive() + MOVIE_EXPORT_FOLDER;

	// Get file
	QString fileName = QFileDialog::getOpenFileName(this, "Select staTTs executable", movieFolder, QString("Settings file (%1)").arg(SETTINGS_FILENAME));

	// get pos manager
	TTTPositionManager* tttpm = movieWindow->getPositionManager();
	if(!tttpm)
		return;

	// Get BDisplay instance for current wavelength
	int wl = tttpm->getWavelength();
	if(wl < 0 || wl > MAX_WAVE_LENGTH) {
		QMessageBox::information(this, "Error", "Note: Could not import settings.");
		qWarning() << "tTt Warning: " << "No wavelength.";
		return;
	}
	BDisplay& bDisplay = tttpm->getDisplays().at(wl);

	// Open file
	QFile f(fileName);
	if(!f.open(QIODevice::ReadOnly)) {
		qWarning() << "tTt Warning: " << "Cannot open file for reading: " << fileName;
		return;
	}

	// Open stream
	QDataStream in(&f);
	in.setVersion(QDataStream::Qt_4_6);
	in.setByteOrder(QDataStream::LittleEndian);
	in.setFloatingPointPrecision(QDataStream::SinglePrecision);

	// Check file version
	quint32 version;
	in >> version;
	if(version > SETTINGS_FILEVERSION) {
		QString error = QString("File %1 has newer file version (%2) than supported (%3).").arg(fileName).arg(version).arg(SETTINGS_FILEVERSION);
		QMessageBox::critical(this, "Error", error);
		return;
	}

	// Settings
	quint32 startTp, endTp, tmpFormat, jpgQuality, tmpMode, autoSetSize, dispTimeBox, 
		timeBold, fontSize, timeStartTp, drawWL, FPS, bitrate, timeboxPosition,
		timeBoxOnBlack;
	QSize centerPicSize;
	QString stackRepeats;

	// Read settings
	if(version == 2) {
		// V2
		in >> startTp >> endTp >> tmpFormat >> jpgQuality >> tmpMode >> autoSetSize >> dispTimeBox >>
			timeBold >> fontSize >> timeStartTp >> drawWL >> stackRepeats >> centerPicSize >> FPS >> bitrate >> 
			timeboxPosition >> timeBoxOnBlack;
	}
	else {
		// V1
		quint32 tmp;
		in >> startTp >> endTp >> tmpFormat >> jpgQuality >> tmpMode >> autoSetSize >> dispTimeBox >>
			timeBold >> fontSize >> timeStartTp >> drawWL >> stackRepeats >> tmp >> FPS >> bitrate >> 
			timeboxPosition >> timeBoxOnBlack;
		centerPicSize = QSize(tmp, tmp);
	}

	// Apply settings
	lieStartTp->setText(QString::number(startTp));
	lieEndTp->setText(QString::number(endTp));
	spbJPGQuality->setValue(jpgQuality);
	chkAutoSetSize->setChecked(autoSetSize != 0);
	chkDisplayTimebox->setChecked(dispTimeBox != 0);
	chkTimeBold->setChecked(timeBold != 0);
	chkDrawWavelength->setChecked(drawWL != 0);
	spbFontSize->setValue(fontSize);
	lieTimeStartTP->setText(QString::number(timeStartTp));
	lieStackRepeat->setText(stackRepeats);
	spbCenterPicSizeX->setValue(centerPicSize.width());
	spbCenterPicSizeY->setValue(centerPicSize.height());
	lieFPS->setText(QString::number(FPS));
	spbAviBitrate->setValue(bitrate);
	spbTimeboxPosition->setValue(timeboxPosition);
	chkTimeOnBlack->setChecked(timeBoxOnBlack != 0);

	// Fileformat
	switch((MovieExporter::FORMAT)tmpFormat) {
		case MovieExporter::JPEG:
			optJPEG->setChecked(true);
			break;
		case MovieExporter::PNG:
			optPNG->setChecked(true);
			break;
		case MovieExporter::AVI:
			optAVI->setChecked(true);
			break;
		case MovieExporter::BMP:
			optBMP->setChecked(true);
			break;
		default:
			break;
	}

	// Export mode
	switch((MovieExporter::MODE)tmpMode) {
	case MovieExporter::HIDE_TRACKS:
		optHideTrackpoints->setChecked(true);
		break;
	case MovieExporter::COMPLETE_ALL:
		optCompleteAll->setChecked(true);
		break;
	case MovieExporter::COMPLETE_FIRST:
		optCompleteFirst->setChecked(true);
		break;
	case MovieExporter::CENTER_ON_TRACKPOINTS:
		optCenterOnTrackspoints->setChecked(true);
		break;
	default:
		optNormal->setChecked(true);
	}

	// Load gamma settings
	quint32 tmp;
	float tmpF;
	QColor tmpC;
	in >> tmp;
	bDisplay.BlackPoint = tmp;
	in >> tmp;
	bDisplay.WhitePoint = tmp;
	in >> tmp;
	bDisplay.Contrast = tmp;
	in >> tmpF;
	bDisplay.Brightness = tmpF;
	in >> tmp;
	bDisplay.Alpha = tmp;
	in >> tmpC;
	bDisplay.TransColor = tmpC;
	in >> tmp;
	bDisplay.TransColorIndex = tmp;

	// GammaValues
	for(int i = 0; i < BDisplay::NUM_GAMMA_VALUES; ++i) {
		in >> tmp;
		bDisplay.GammaValues[i] = tmp;
	}

	// GammaAnchor
	for(int i = 0; i < GAMMA_ANCHOR_POINTS; ++i) {
		for(int j = 0; j < BDisplay::NUM_GAMMA_ANCHOR_COORDS; ++j) {
			in >> tmp;
			bDisplay.GammaAnchor[i][j] = tmp;
		}
	}

	// Update frmGammaAdjust
	if(movieWindow->frmGammaAdjust)
		movieWindow->frmGammaAdjust->setDisplay(tttpm, wl);

	// Update TTTMovie
	movieWindow->getMultiplePictureViewer()->adjustGraphics(wl);
}

void TTTMovieExport::exportSettings( const QString& _folder )
{
	// Check path
	if(_folder.isEmpty() || !QDir(_folder).exists()) {
		qWarning() << "tTt Warning: " << "TTTMovieExport::exportSettings() - invalid path.";
		return;
	}

	// Filename
	QString fileName = _folder + SETTINGS_FILENAME;

	// get pos manager
	TTTPositionManager* tttpm = movieWindow->getPositionManager();
	if(!tttpm)
		return;

	// Get BDisplay instance for current wavelength
	int wl = tttpm->getWavelength();
	if(wl < 0 || wl > MAX_WAVE_LENGTH) {
		QMessageBox::information(this, "Error", "Note: Could not save export settings.");
		qWarning() << "tTt Warning: " << "No wavelength.";
		return;
	}
	const BDisplay& bDisplay = tttpm->getDisplays().at(wl);

	// Open file
	QFile f(fileName);
	if(!f.open(QIODevice::WriteOnly)) {
		qWarning() << "tTt Warning: " << "Cannot open file for writing: " << fileName;
		return;
	}

	// Open stream
	QDataStream out(&f);
	out.setVersion(QDataStream::Qt_4_6);
	out.setByteOrder(QDataStream::LittleEndian);
	out.setFloatingPointPrecision(QDataStream::SinglePrecision);

	// File version
	out << (quint32)SETTINGS_FILEVERSION;

	// Save settings
	QSize centerSize(spbCenterPicSizeX->value(), spbCenterPicSizeY->value());
	out << (quint32)lieStartTp->text().toInt()
		<< (quint32)lieEndTp->text().toInt()
		<< (quint32)format
		<< (quint32)spbJPGQuality->value()
		<< (quint32)mode
		<< (quint32)chkAutoSetSize->isChecked()
		<< (quint32)chkDisplayTimebox->isChecked()
		<< (quint32)chkTimeBold->isChecked()
		<< (quint32)spbFontSize->value()
		<< (quint32)lieTimeStartTP->text().toInt()
		<< (quint32)chkDrawWavelength->isChecked()
		<< lieStackRepeat->text()
		<< centerSize
		<< (quint32)lieFPS->text().toInt()
		<< (quint32)spbAviBitrate->value()
		<< (quint32)spbTimeboxPosition->value()
		<< (quint32)chkTimeOnBlack->isChecked();

	// Save gamma
	out << (quint32)bDisplay.BlackPoint
		<< (quint32)bDisplay.WhitePoint
		<< (quint32)bDisplay.Contrast
		<< (float)bDisplay.Brightness
		<< (quint32)bDisplay.Alpha
		<< (QColor)bDisplay.TransColor
		<< (quint32)bDisplay.TransColorIndex;

	// GammaValues
	for(int i = 0; i < BDisplay::NUM_GAMMA_VALUES; ++i)
		out << (quint32)bDisplay.GammaValues[i];

	// GammaAnchor
	for(int i = 0; i < GAMMA_ANCHOR_POINTS; ++i) {
		for(int j = 0; j < BDisplay::NUM_GAMMA_ANCHOR_COORDS; ++j) {
			out << (quint32)bDisplay.GammaAnchor[i][j];
		}
	}
}
