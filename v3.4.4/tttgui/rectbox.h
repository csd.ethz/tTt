/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RECTBOX_H
#define RECTBOX_H


#include <qrect.h>
#include <qpainter.h>
#include <qcolor.h>
#include <qpaintdevice.h>

/**
@author Bernhard

	The core of this class is a simple rectangle, but RectBox provides functions
	for drawing on special grounds, shifting the box while heeding bounds, and therefore
	makes use simple for common display functions
*/

class RectBox
{
public:

	/**
	 * constructs a default box (initialized() == false)
	 */
	RectBox ()
	 : Initialized (false) 
	 	{}
	
	/**
	 * creates a box which can be drawn in _parent, has initial coordinates as _rect and with color _color
	 * _zoom is only necessary for drawing the rectangle, yet its coordinates are stored as provided
	 * afterwards, initialized() == true
	 * @param _parent the parent widget into which the box is drawn
	 * @param _rect the original rectangle defining the initial size of the box
	 * @param _color the color of the outline
	 * @param _zoom the zoom factor (used for drawing a too large box)
	 */
	void initRectBox (QPaintDevice *_parent, const QRect &_rect, QColor _color, float _zoom = 1.0f);
	
	~RectBox();
	
	/**
	 * draws the box into the widget specified in the constructor
	 */
	void draw();
	
        /* *
	 * draws the box into the widget specified in the constructor, but with the window in the parameters
	 * @param _left the left border
	 * @param _top the top border
	 * @param _width the width of the drawing window
	 * @param _height the height of the drawing window
         */
        //void drawWindow (int _left, int _top, int _width, int _height);
	
        /* *
	 * draws the ellipse that fits inside the box into the widget specified in the constructor
	 */
        //void drawEllipse();
	
	/**
	 * resets the box to its original size
	 */
	void reset();
	
	/**
	 * truncates the box to fit into the original QRect
	 */
	void truncate();
	
	/**
	 * moves the box with the given offset
	 * if any bounds are specified (!= -1), the box is maintained within this region
	 * (i.e. the shift is cancelled if the box would fall out of the given region)
	 * @param _x the x offset
	 * @param _y the y offset
	 * @param _boundLeft the left border bound
	 * @param _boundTop the top border bound
	 * @param _boundRight the right border bound
	 * @param _boundBottom the bottom border bound
	 */
	void shift (int _x = 0, int _y = 0, int _boundLeft = -1, int _boundTop = -1, int _boundRight = -1, int _boundBottom = -1);
	
	/**
	 * shifts the box, not changing its size
	 * usage of the bounds: if a shift would set the box' coords further than the bounds, then the shift is cut off at exactly that point
	 * (this is different from shift(): that method then does not shift at all, while shiftMax() shifts until the bound)
	 * @param _xShift the shift in x
	 * @param _yShift the shift in y
	 * @param _boundLeft the left max shift bound
	 * @param _boundTop the top max shift bound
	 * @param _boundRight the right...
	 * @param _boundBottom the bottom ...
	 */
	void shiftMax (int _xShift, int _yShift, int _boundLeft = -1, int _boundTop = -1, int _boundRight = -1, int _boundBottom = -1);
	
	
	/**
	 * resizes the box 
	 * moves de facto the bottom right edge
	 * @param _width the new width of the box
	 * @param _height the new height of the box
	 */
	void resize (int _width = -1, int _height = -1);
	
	/**
	 * shifts the top left corner
	 * note: this changes the size of the box
	 * @param _x the offset to be added to the left border
	 * @param _y the offset to be added to the top border
	 */
	void shiftTopLeft (int _x, int _y);
	
	/**
	 * shifts the bottom right corner
	 * note: this changes the size of the box
	 * @param _x the offset to be added to the right border
	 * @param _y the offset to be added to the bottom border
	 */
	void shiftBottomRight (int _x, int _y);
	
	/**
	 * sets the top left corner (leaves bottom right unchanged)
	 * @param _x the left border
	 * @param _y the top border
	 */
	void setTopLeft (int _x, int _y);
	
	/**
	 * sets the bottom right corner (leaves top left unchanged)
	 * note: this changes the size of the box
	 * @param _x the right border
	 * @param _y the bottom border
	 */
	void setBottomRight (int _x, int _y);
	
	/**
	 * sets the position of the box via the top left corner
	 * note: does not change the size of the box, because it also moves the bottomright corner
	 * @param _x the left border
	 * @param _y the top border
	 */
	void setPosition (int _x, int _y);
	
	/**
	 * sets the complete box to the specified rectangle
	 * @param _rect the rectangle that should be set
	 */
	void setBox (QRect _rect);
	
	/**
	 * sets the zoom factor for the display
	 * (as usually the real size of the box is far too large to handle efficiently)
	 * @param _zoomFactorX the stretch in horizontal direction
	 * @param _zoomFactorY the stretch in vertical direction 
	 */
	void setZoomFactor (float _zoomFactorX, float _zoomFactorY);
	
	/**
	 * @return the left border of the current rectangle
	 */
	int left() const
		{return Rect.left();}
	
	/**
	 * @return the right border of the current rectangle
	 */
	int right() const
		{return Rect.right();}
	
	/**
	 * @return the top border of the current rectangle
	 */
	int top() const
		{return Rect.top();}
	
	/**
	 * @return the bottom border of the current rectangle
	 */
	int bottom() const
		{return Rect.bottom();}
	
	/**
	 * @return the width of the current rectangle
	 */
	int width() const
		{return Rect.width();}
	
	/**
	 * @return the height of the current rectangle
	 */
	int height() const
		{return Rect.height();}
	
	/**
	 * @return the normalized QRect (i.e. top <= bottom  && left <= right)
	 */
	QRect box() const
		{return Rect.normalized();}
	
	/**
	 * @return whether the box was already initialized (initRectBox() was called)
	 */
	bool initialized() const
		{return Initialized;}
	
        /**
          * returns the current color
          */
        const QColor &color() const
                {return Color;}

        /**
          * returns the current zoom factor
          */
        float zoom() const
                {return ZoomFactor;}

private:
	
	///whether the box was already initialized
	bool Initialized;
	
	///the original (starting) rectangle (necessary for resetting the box)
	QRect OriginalRect;
	
	///the parent widget into which should be painted (stored as QPaintDevice)
	QPaintDevice *Parent;
	
	///the current rectangle
	QRect Rect;
	
	///the outline color of the rectangle
	QColor Color;
	
	///the x zoom factor (as ratio)
	float ZoomFactor;
	
	///the y zoom factor (as ratio)
	float ZoomFactorY;
};

#endif
