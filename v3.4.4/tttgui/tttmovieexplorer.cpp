/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "tttmovieexplorer.h"

// Qt
#include <QMessageBox>
#include <QDir>
#include <QStringList>
#include <QFileDialog>
#include <QXmlStreamReader>

// Project
#include "tttio/fastdirectorylisting.h"
#include "tttbackend/changecursorobject.h"
#include "tttdata/userinfo.h"


TTTMovieExplorer::TTTMovieExplorer( QWidget* _parent /*= 0*/, bool _selectable )
	: QWidget(_parent), selectable(_selectable)
{
	// Setup ui
	ui.setupUi(this);

	// Init variables
	initialized = false;
	foundItemsIndex = -1;

	// Signals/slots
	connect(ui.pbtLoad, SIGNAL(clicked()), this, SLOT(parseFiles()));
	connect(ui.pbtSelectPath, SIGNAL(clicked()), this, SLOT(selectFolder()));
	connect(ui.pbtFindNext, SIGNAL(clicked()), this, SLOT(findText()));
	connect(ui.pbtFindPrev, SIGNAL(clicked()), this, SLOT(findText()));
	connect(ui.pbtLoadExperiments, SIGNAL(clicked()), this, SLOT(close()));
	if(selectable) 
		connect(ui.pbtLoadExperiments, SIGNAL(clicked()), this, SLOT(loadExperiments()));

	// Hide button if not selectable
	if(!selectable)
		ui.pbtLoadExperiments->hide();

	// Set folder
	ui.liePath->setText(UserInfo::getInst().getUserSettingByKey(UserInfo::KEY_TTTMOVIEEXPLORER_PATH).toString());
}

void TTTMovieExplorer::initialize()
{
	// Column headers
	QStringList columnHeaders;

	// File name column
	columnHeaders << "Filename";

	// ToDo: Get from setting if available
	// Other columns 
	QStringList keys, dispNames;
	getDefaultSettings(keys, dispNames);
	for(int i = 0; i < keys.size() && i < dispNames.size(); ++i) {
		// Add column
		columnHeaders.append(dispNames[i]);

		// Add entriesToDisplay entry
		entriesToDisplay.insert(keys[i], i + 1);
	}

	// Set headers
	ui.tbtTable->setColumnCount(columnHeaders.size());
	ui.tbtTable->setHorizontalHeaderLabels(columnHeaders);

	// Resize to fit headers
	ui.tbtTable->resizeColumnsToContents();

	initialized = true;
}


void TTTMovieExplorer::parseFiles()
{

	// Change cursor
	ChangeCursorObject cc;

	// Disable sorting
	ui.tbtTable->setSortingEnabled(false);

	// for some reason QTableWidget must be hidden when loading for second time
	// otherwise it takes a lot much longer to update the table
	// a bug?
	this->hide();
	ui.tbtTable->hide();

	// Initialize if necessary
	if(!initialized)
		initialize();

	// Clear table
	ui.tbtTable->clearContents();

	// Clear found items
	foundItemsIndex = -1;
	foundItems.clear();

	// Get path
	QString dir = QDir::fromNativeSeparators(ui.liePath->text());
	if(dir.isEmpty() || !QDir(dir).exists()) {
		QMessageBox::information(this, "Error", "The specified directory ('%1') is invalid.");
		return;
	}
	if(dir.right(1) != "/")
		dir += '/';


	// Get files
	QStringList extensions;
	extensions.append("_TATexp.xml");
	QMap<QString,QString> files = findFiles(dir, extensions); 
	

	// Set row count
	ui.tbtTable->setRowCount(files.size());

	// Create a progressDialog
	QProgressDialog progress("Please wait while your experiments are being loaded...", 
		"Cancel", 0, files.size(), this);	
	progress.setWindowModality(Qt::WindowModal);	
	progress.setCancelButton(0);
	progress.setMinimumDuration(0);	
	progress.show();
	

	// Parse files and populate table
	int rowCounter = 0;
	int fileNameColumn = (selectable) ? 1 : 0;
	QMap<QString,QString>::const_iterator i;
	for(i = files.constBegin(); i != files.constEnd(); ++i, ++rowCounter)
	{
		QString expBasename = i.key().left(i.key().indexOf("_"));
		// File name
		QTableWidgetItem *fileName = new QTableWidgetItem(expBasename);
		fileName->setData(Qt::UserRole, i.value());	// Save the path also in the item
		if(selectable) fileName->setData(Qt::CheckStateRole, Qt::Unchecked); // If selectable show a checkbox
		ui.tbtTable->setItem(rowCounter, 0, fileName);
				
		// Parse file
		parseSingleFile(i.value() + i.key(), rowCounter);


		// Count trees in experiment and add column
		extensions.clear();
		extensions.append(".ttt");
		
		QString numberOfTrees = QString::number(findNumberOfTrees(expBasename, extensions));
		// Add item
		int colIndex = entriesToDisplay.value("TreesTracked", -1);
		QTableWidgetItem *newItem = new QTableWidgetItem(numberOfTrees);
		ui.tbtTable->setItem(rowCounter, colIndex, newItem);
		
		progress.setValue(rowCounter);
		/*if(progress.wasCanceled())
			break;*/
	}

	// Enable sorting
	ui.tbtTable->setSortingEnabled(true);
	ui.tbtTable->sortByColumn(0, Qt::AscendingOrder);

	// Resize first column
	ui.tbtTable->resizeColumnToContents(0);

	// Laura Skylaki
	// Resize to contents
	ui.tbtTable->verticalHeader()->setResizeMode(QHeaderView::ResizeToContents);
	ui.tbtTable->setColumnWidth(1,  400);
	ui.tbtTable->setColumnWidth(3,  200);
	ui.tbtTable->show();	
	this->show();
}

void TTTMovieExplorer::getDefaultSettings( QStringList& _keys, QStringList& _displayNames )
{
	// Laura Skylaki
	_keys << "TATSettings.CellsAndConditions.MediumAdditions.value0"
		<< "TATSettings.CellsAndConditions.Serum.value0"
		<< "TATSettings.PositionData.PositionInformation.PosInfoDimension.comments0"
		<< "TreesTracked"
		<< "TATSettings.WavelengthData.WavelengthInformation.WLInfo.Comment0"
		<< "TATSettings.WavelengthData.WavelengthInformation.WLInfo.Comment1"
		<< "TATSettings.WavelengthData.WavelengthInformation.WLInfo.Comment2"
		<< "TATSettings.WavelengthData.WavelengthInformation.WLInfo.Comment3"
		<< "TATSettings.WavelengthData.WavelengthInformation.WLInfo.Comment4"
		<< "TATSettings.WavelengthData.WavelengthInformation.WLInfo.Comment5"
		<< "TATSettings.CellsAndConditions.HoursBeforeMovieStart.value0";

	_displayNames << "MediumAdditions"
		<< "Serum"
		<< "Position Info"
		<< "# of Tracked Trees"
		<< "WL0 Comment"
		<< "WL1 Comment"
		<< "WL2 Comment"
		<< "WL3 Comment"
		<< "WL4 Comment"
		<< "WL5 Comment"
		<< "HoursBeforeMovieStart";


	/*_keys << "TATSettings.CellsAndConditions.Flask.value0"
		<< "TATSettings.CellsAndConditions.HoursBeforeMovieStart.value0"
		<< "TATSettings.CellsAndConditions.Medium.value0"
		<< "TATSettings.CellsAndConditions.MediumAdditions.value0"
		<< "TATSettings.CellsAndConditions.MediumVolumeInUl.value0"
		<< "TATSettings.CellsAndConditions.Serum.value0"
		<< "TATSettings.CellsAndConditions.SerumPercentage.value0"
		<< "TATSettings.CellsAndConditions.TVAdapter.value0"
		<< "TATSettings.CurrentObjectiveMagnification.value0"
		<< "TATSettings.CurrentSetupNumber.value0"
		<< "TATSettings.PositionCount.count0"
		<< "TATSettings.WavelengthData.WavelengthInformation.WLInfo.Comment0"
		<< "TATSettings.WavelengthData.WavelengthInformation.WLInfo.Comment1"
		<< "TATSettings.WavelengthData.WavelengthInformation.WLInfo.Comment2"
		<< "TATSettings.WavelengthData.WavelengthInformation.WLInfo.Comment3"
		<< "TATSettings.WavelengthData.WavelengthInformation.WLInfo.Comment4"
		<< "TATSettings.WavelengthData.WavelengthInformation.WLInfo.Comment5";

	_displayNames << "Flask"
		<< "HoursBeforeMovieStart"
		<< "Medium"
		<< "MediumAdditions"
		<< "MediumVolumeInUl"
		<< "Serum"
		<< "SerumPercentage"
		<< "TVAdapter"
		<< "CurrentObjectiveMagnification"
		<< "CurrentSetupNumber"
		<< "PositionCount"
		<< "WL0 Comment"
		<< "WL1 Comment"
		<< "WL2 Comment"
		<< "WL3 Comment"
		<< "WL4 Comment"
		<< "WL5 Comment";*/
}

QMap<QString,QString> TTTMovieExplorer::findFiles(const QString& _dir, const QStringList& _extensions) const
{
	QDir dir(_dir);
	QMap<QString,QString> files;

	// Find all files and add them in the result map
	foreach(QString file, FastDirectoryListing::listFiles(dir, _extensions))
	{
		files.insert(file, _dir);
	}

	
	// If recursive check all subfolders
	if(ui.ckbRecursive->isChecked())
	{
		QStringList subfolders = FastDirectoryListing::listFiles(dir, QStringList(), true);
		foreach(QString subfolder, subfolders)
		{
			if((subfolder == ".")||(subfolder == ".."))
				continue;
			
			// Ignore position folders, there are too many and there are normally no experiment xmls.
			if(subfolder.contains(POSITION_MARKER))
				continue;

			files.unite(findFiles(_dir + "/" + subfolder + "/", _extensions));
			
		}
		
	}

	return files;
}

int TTTMovieExplorer::findNumberOfTrees(const QString& _dir, const QStringList& _extensions){
	QDir dir(_dir);
	int numberOfTrees = 0;
	// find position subfolders

	// Attention only works with new folder structure!
	// get the TTTfiles from the network access directory
	QString tttFolder = SystemInfo::getTTTFileFolderFromBaseName (_dir + "_p000", false);
	tttFolder = tttFolder.left (tttFolder.findRev ("/", -2));	//remove "000" position
	
	// list all directories in the experiment folder
	QStringList subfolders = FastDirectoryListing::listFiles(tttFolder, QStringList(), true);
	foreach(QString subfolder, subfolders)
		{
			if((subfolder == ".")||(subfolder == ".."))
				continue;
			
			// Position folders will contain the tree files from tTt
			if(subfolder.contains(POSITION_MARKER)) {
				QStringList subfolders = FastDirectoryListing::listFiles(tttFolder +"/"+subfolder,_extensions);
				numberOfTrees += subfolders.size();
			}
		}
	return numberOfTrees;
}

void TTTMovieExplorer::loadExperiments()
{
	QStringList experimentFolders;

	// Check each row if the experiment is selected
	for(int row = 0; row < ui.tbtTable->rowCount(); ++row)
	{
		QTableWidgetItem* item = ui.tbtTable->item(row, 0);
		// a table cell can be NULL when the user clicked CANCEL while loading
		// TODO: add a function that corrects the table after CANCEL to avoid the check here
		if (item!=NULL) {
			if(item->checkState() != Qt::Checked)
				continue;

			// The folder is saved as UserRole
			experimentFolders.append(item->data(Qt::UserRole).toString());
		}
	}

	// Emit the right signal
	emit loadExperiments(experimentFolders);
}

void TTTMovieExplorer::selectFolder()
{
	// Get folder
	QString newFolder = QFileDialog::getExistingDirectory(this, "Select input folder", ui.liePath->text());
	if(newFolder.isEmpty())
		return;

	// Call setFolder
	setFolder(newFolder);

	// Remember folder
	QSettings* userSettings = UserInfo::getInst().getUserSettings();
	if(userSettings)
		userSettings->setValue(UserInfo::KEY_TTTMOVIEEXPLORER_PATH, newFolder);
}

void TTTMovieExplorer::parseSingleFile( const QString& _fileName, int _rowIndex )
{
	// Open file
	QFile f(_fileName);
	if(!f.open(QIODevice::ReadOnly))
		return;

	// Read data
	const QString data = f.readAll();

	// Hash for counting keys
	 QHash<QString, int> keyCount;
	 keyCount.clear();

	// Open stream
	QXmlStreamReader xml(data);
	QString curKey;
	while(!xml.atEnd()) {
		// Read next
		QXmlStreamReader::TokenType type = xml.readNext();
		if(type == QXmlStreamReader::StartElement) {
			// Update key
			if(!curKey.isEmpty())
				curKey.append('.');
			curKey.append(xml.name());

			// Parse attributes
			parseAttributes(_rowIndex, curKey, xml.attributes(), keyCount);
		}
		else if(type == QXmlStreamReader::EndElement) {
			// Element end

			// Update key
			int idx = curKey.lastIndexOf('.');
			if(idx != -1)
				curKey = curKey.left(idx);
		}
	}
	xml.clear();
	f.close();
}


void TTTMovieExplorer::parseAttributes( int _curRow, const QString& _key, const QXmlStreamAttributes& _attributes, QHash<QString, int>& _keyCount )
{
	// Iterate over attributes
	for(QXmlStreamAttributes::const_iterator it = _attributes.constBegin(); it != _attributes.constEnd(); ++it) {
		// Get full name
		QString fullKeyTmp = _key + ".";
		fullKeyTmp += it->name();

		// Add count
		int count = _keyCount.value(fullKeyTmp, 0);
		QString fullKey = fullKeyTmp + QString::number(count);

		// Check if attribute should be displayed
		int colIndex = entriesToDisplay.value(fullKey, -1);
		if(colIndex >= 0) {
			// check if the table cell exists, if no, add a new cell, if yes, update the cell contents
			QString oldText = "";
			if ( ui.tbtTable->item(_curRow, colIndex)==NULL){
				oldText = it->value().toString();
				// Fix text for MediumAdditions
				if (fullKey.compare("TATSettings.CellsAndConditions.MediumAdditions.value0")==0){					
					oldText = oldText.replace(QRegExp("\\\\{2,}"),QString("\n"));
				} 
				
				// Add item
				QTableWidgetItem *newItem = new QTableWidgetItem(oldText);
				ui.tbtTable->setItem(_curRow, colIndex, newItem);

				// Increase count
				if (fullKey.compare("TATSettings.PositionData.PositionInformation.PosInfoDimension.comments0")!=0){					
					_keyCount.insert(fullKeyTmp, ++count);
				} else {
					_keyCount.insert(fullKeyTmp, count);
				}

			} else {
				oldText = ui.tbtTable->item(_curRow, colIndex)->text();
				if (!oldText.contains(it->value().toString())) {
					QString newText = ui.tbtTable->item(_curRow, colIndex)->text() + "\n" + it->value().toString();
					ui.tbtTable->item(_curRow, colIndex)->setText(newText);
				}
			}
		}
	}
}

void TTTMovieExplorer::findText()
{
	// Get text
	QString text = ui.lieFind->text();
	if(text.isEmpty())
		return;

	// Deselect old
	if(foundItemsIndex >= 0 && foundItemsIndex < foundItems.size())
		foundItems[foundItemsIndex]->setSelected(false);

	// Find text
	if(foundItemsIndex < 0 || oldSearchTerm != text) {
		foundItems = ui.tbtTable->findItems(text, Qt::MatchContains);
		if(foundItems.isEmpty()) {
			foundItemsIndex = -1;
			return;
		}
	}

	// Get next index
	if(sender() == ui.pbtFindNext) {
		if(++foundItemsIndex >= foundItems.size())
			foundItemsIndex = 0;
	}
	else {
		if(--foundItemsIndex < 0) 
			foundItemsIndex = foundItems.size() - 1;
	}

	// Select item and make visible
	ui.tbtTable->setCurrentItem(foundItems[foundItemsIndex], QItemSelectionModel::Select);

	// Remember search term
	oldSearchTerm = text;
}

/**
 * Sets the folder
 */
void TTTMovieExplorer::setFolder(QString _folder)
{
	// Change lieInputFolder
	ui.liePath->setText(_folder);
}

/**
 * Gets the currently selected folder
 */
QString TTTMovieExplorer::getFolder() const
{
	return ui.liePath->text();
}

/**
 * Sets if the search for xml files is done recursively
 */
void TTTMovieExplorer::setRecursive(bool _recursive)
{
	ui.ckbRecursive->setChecked(_recursive);
}

/**
 * Gets the current value of recursive
 */
bool TTTMovieExplorer::getRecursive() const
{
	return ui.ckbRecursive->isChecked();
}
