/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "tttlogfileconverter.h"

// Qt
#include <QMessageBox>

// Project
#include "tttbackend/tttmanager.h"
#include "tttio/fastdirectorylisting.h"
#include "tttgui/tttpositionlayout.h"


TTTLogFileConverter::TTTLogFileConverter( QWidget* _parent /*= 0*/ )
	: QWidget(_parent)
{
	ui.setupUi(this);

	// Init gui
	setWindowFlags( windowFlags() | Qt::CustomizeWindowHint | Qt::WindowMinMaxButtonsHint);
	
	// Make window modal, to prevent user from closing the application while conversion runs or attempting to load positions or ...
	setWindowModality(Qt::ApplicationModal);

	// Init variables
	conversionRunning = false;

	// Signals/slots
	connect(ui.pbtClose, SIGNAL(clicked()), this, SLOT(close()));
	connect(ui.pbtStart, SIGNAL(clicked()), this, SLOT(startConversion()));
	connect(ui.bgrMissingXmlOption, SIGNAL(buttonClicked(int)), this, SLOT(missingXmlOptionChanged()));
	connect(ui.chkForceFormatString, SIGNAL(toggled(bool)), this, SLOT(forceFormatStringToggled(bool)));
}

void TTTLogFileConverter::closeEvent( QCloseEvent *_event )
{
	// Closing is not allowed while conversion is running
	if(conversionRunning) {
		QMessageBox::information(this, "Note", "Please wait for the conversion to finish, before closing this window.");

		_event->ignore();
		return;
	}

	// Accept close event
	_event->accept();
}

void TTTLogFileConverter::startConversion()
{
	// Update ui
	ui.grbOptions->setEnabled(false);
	ui.pbtStart->setEnabled(false);
	ui.pbtClose->setEnabled(false);
	ui.pbtCancel->setEnabled(true);
	conversionRunning = true;
	
	// Clear errors
	errors.clear();

	// Run conversion
	if(runConversionInternal()) {
		out("\nLogfile conversion completed successfully!");
	}
	else {
		out("\nErrors occurred during the logfile conversion!");
	}

	// Check for errors
	if(errors.size() > 0) {
		// Save error log to file
		QString expDir = TTTManager::getInst().frmPositionLayout->getExperimentDir().path();
		bool errorLogSavedSuccessfully = false;
		if(!expDir.isEmpty()) {
			if(expDir.right(1) != "/")
				expDir.append('/');

			// Try to open error log
			QString errorLogFileName = expDir + "TTTLogFileConverterErrorLog.txt";
			QFile errFile(errorLogFileName);
			if(errFile.open(QIODevice::WriteOnly)) {
				// Export log
				QTextStream out(&errFile);

				// Header
				out << expDir << "  ---  Errors during logfile conversion:\r\n\r\n";

				for(int i = 0; i < errors.size(); ++i) {
					out << errors[i] << "\r\n";
				}

				out << "\r\nYou must check and correct these errors immediately\r\n!!! RIGHT NOW - NOT tomorrow !!!";
				errorLogSavedSuccessfully = true;
			}
		}

		if(!errorLogSavedSuccessfully) 
			out(QString("\nError: Could not save error log to TTTLogFileConverterErrorLog.txt in the experiment folder!\n"));

		// Messagebox
		QString msg = QString("Warning: %1 errors occurred during the logfile conversion!\n\nBy clicking Ok you confirm to check and correct these errors immediately\n!!! RIGHT NOW - NOT tomorrow !!!").arg(errors.size());
		QMessageBox::warning(this, "Errors occurred during conversion", msg, QMessageBox::Ok);
	}

	// Finished, update ui
	conversionRunning = false;
	ui.grbOptions->setEnabled(true);
	ui.pbtStart->setEnabled(true);
	ui.pbtCancel->setEnabled(false);
	ui.pbtClose->setEnabled(true);
}

void TTTLogFileConverter::missingXmlOptionChanged()
{
	ui.lieInterval->setEnabled(ui.optInterval->isChecked());
}

bool TTTLogFileConverter::runConversionInternal()
{
	/**
	 * How log file conversion works:
	 * - iterate over all positions
	 * - use the first position with images to determine the date format
	 *   (a valid format has to work for all images in the position)
	 * - use this format to convert xml files for all images
	 */

	out("Starting conversion..");

	// Get parameters
	int intervalSeconds = -1;
	QDateTime startTime;
	QString directoryForImageFilesWithNoXML;
	QString directoryWithEmptyPositions;

	if(ui.optInterval->isChecked()) {
		// Get interval
		bool ok;
		intervalSeconds = ui.lieInterval->text().toInt(&ok);

		// Invalid interval
		if(!ok || intervalSeconds <= 0) {
			QMessageBox::critical(this, "Invalid parameters", QString("The specified interval value ('%1') is invalid.").arg(ui.lieInterval->text()));
			return false;
		}

		// Get start time
		startTime = QDateTime::currentDateTime();
	}

	// Get directoryForImageFilesWithNoXML and directoryWithEmptyPositions
	QString tmp = TTTManager::getInst().frmPositionLayout->getExperimentDir().path();

	// Check experiment directory
	if(tmp.isEmpty() || !QDir(tmp).exists()) {
		QString errMsg = QString("The experiment directory seems to be invalid: '%1'.\n\nThis is probably an internal error, please report it as soon as possible.").arg(tmp);

		// Report
		out(errMsg);
		QMessageBox::critical(this, "Internal Error", errMsg);

		return false;
	}

	// Construct directoryForImageFilesWithNoXML by appending suffix
	if(tmp.right(1) != "/")
		tmp.append('/');
	directoryForImageFilesWithNoXML = tmp + "Pictures with missing logfiles/";
	directoryWithEmptyPositions = tmp + "EmptyPositions/";
	

	// Get all positions
	QHash<int, TTTPositionManager*>& allPositions = TTTManager::getInst().getAllPositionManagers();

	// Update gui
	int totalNumberPositions = allPositions.size();
	ui.pbrProgress->setRange(0, totalNumberPositions + 1);	// +1 because one position will be used twice to determine the format string

	// Image file extensions
	QStringList imageExtensions;
	imageExtensions << ".jpg" << ".tif" << ".bmp" << ".png";

	// Images with no corresponding xml file
	QStringList imagesWithNoXMLFile;

	// Positions with no image files
	QList<TTTPositionManager*> positionsWithNoFiles;

	// At first, the image format string is determined and the regular expression to extract it
	int errorCount = 0;		// Number of images, for which no date information could be found
	QString dateFormatString;
	QString dateRegExp;
	bool determineFormatString = true;	// Indicates if format strings have to be determined
	int imageLastIndexConversionFailed = 0; // Index of image at which format string failed last time when testing a new format string
	QVector<QString> testedFormats;		// Format strings that were tested already
	bool fatalError = false;	// If set to true after image loop, processing will be stopped
	bool expectingYouScopeXmlFiles = false;	// Set to true if a YouScope xml file was found during detection of date format -> then YouScope xml files are expected for all images
	int count = 0;
	if(ui.chkForceFormatString->isChecked()) {
		// Use user-specified date format string
		determineFormatString = false;
		dateFormatString = ui.lieForcedFormatString->text();
		if(dateFormatString.isEmpty()) {
			QMessageBox::information(this, "Note", "Please specify date format string or uncheck 'Force Date Format' checkbox.");
			return false;
		}
		dateRegExp = "<V%1>\\s*(.{18,22})\\s*</V%2>";	// Extract date strings with 18 to 22 characters (skipping leading and trailing white spaces)

		ui.pbrProgress->setValue(++count);
	}
	else {
		out("Determining date format..");
	}

	// Iterate over all positions
	auto iter = allPositions.begin();
	while(iter != allPositions.end()) {
		// If valid format string was found in this iteration and if all xml files have invalid format,
		// only relevant if determineFormatString is true
		bool foundValidFormatString = true;
		bool allXmlFilesInvalid = true;
		

		// Update progress bar
		ui.pbrProgress->setValue(++count);

		// Process events to show messages
		QApplication::processEvents();

		// Get current pos manager
		TTTPositionManager* curPM = (*iter);
		if(!curPM) {
			++iter;
			continue;
		}
		
		// Get directory and basename
		QString directory = curPM->getPictureDirectory();
		QString basename = curPM->getBasename();
		QDir directoryQDir(directory);
		if(directory.isEmpty() || !directoryQDir.exists()) {
			// Report
			QString errMsg = QString("Warning: the picture directory for position '%1' seems to be invalid: '%2' - skipping position.").arg(curPM->positionInformation.getIndex()).arg(directory);
			errors.append(errMsg);
			out(errMsg);

			++iter;
			continue;
		}

		if(basename.isEmpty()) {
			// Report
			QString errMsg = QString("Warning: Could not detect basename of position '%1': %2 - skipping position").arg(curPM->positionInformation.getIndex()).arg(basename);
			errors.append(errMsg);
			out(errMsg);

			++iter;
			continue;
		}

		// Make sure directory ends with '/'
		if(directory.right(1) != "/")
			directory.append('/');

		// List image files
		QStringList imageFiles = FastDirectoryListing::listFiles(QDir(directory), imageExtensions);
		imageFiles.sort();
		if(!imageFiles.isEmpty()) {
			// Get log file path
			QString logFile = directory + basename + ".log";

			// Check if it exists already
			if(QFile::exists(logFile)) {
				// Check if it should be skipped
				if(ui.chkSkipExistingLogFiles->isChecked()) {
					out(QString("Skipping position %1 as log file already exists.").arg(curPM->positionInformation.getIndex()));

					++iter;
					continue;
				}
			}

			// Open logfile for writing
			QFile logF(logFile);
			if(!determineFormatString) {
				if(!logF.open(QIODevice::WriteOnly)) {
					// Opening failed
					QString errMsg = QString("Error: could not open logfile '%1' for writing: '%2' - skipping position").arg(logFile).arg(logF.errorString());
					out(errMsg);
					errors.append(errMsg);

					++iter;
					continue;
				}
			}

			// Statistics
			QList<int> foundWavelengths;
			int maxTP = -1;

			// Entries (will be saved after the loop so we have their count)
			QList<InternalLogFileEntry> foundEntries;
			foundEntries.reserve(imageFiles.size());

			// Iterate over images
			int imageCount = -1;
			QDateTime dateOfLastTimePoint;	// Used when determining format string to make sure, months never change before days
			int oldTimePoint = -1;
			for(QStringList::const_iterator it = imageFiles.constBegin(); it != imageFiles.constEnd(); ++it) {
				++imageCount;

				// Update status
				if(imageCount % 10 == 9) {
					QString statusMsg = QString("Processing file %1 of %2 in position %3 of %4 + 1").arg(imageCount + 1).arg(imageFiles.size()).arg(count).arg(totalNumberPositions);
					ui.lblStatus->setText(statusMsg);
					QApplication::processEvents();
				}

				// Check if canceled
				if(ui.pbtCancel->isChecked()) {
					out("Error: Operation canceled.");
					errors.append("Error: Operation canceled.");
					ui.pbtCancel->setChecked(false);

					return false;
				}

				// Extract timepoint, wavelength and z-index from filename
				int tp = FileInfoArray::CalcTimePointFromFilename(*it);
				int wl = FileInfoArray::CalcWaveLengthFromFilename(*it);
				int zIndex = FileInfoArray::CalcZIndexFromFileName(*it);

				// Check timepoint and wavelength (z-index can be missing)
				if(tp <= 0 || wl < 0) {
					// Skip image
					QString errMsg = QString("Warning: detected invalid image file in folder '%1': %2 - skipping file.").arg(directory).arg(*it);
					out(errMsg);
					errors.append(errMsg);

					continue;
				}

				// Image acquisition time (what all this is about)
				QDateTime imageCreationTime;

				// We seem to have a valid image file -> try to open the meta xml file
				QString metaXMLFileNameBase = directory + *it;
				metaXMLFileNameBase = metaXMLFileNameBase.left(metaXMLFileNameBase.lastIndexOf("."));
				QString metaXMLFileName = metaXMLFileNameBase + ".xml";
				QString metaXMLFileNameTAT = directory + *it + "_meta.xml";
				bool fileNotFoundOrError = true;	// If parsing failed, only relevant if determineFormatString is false
				if(QFile::exists(metaXMLFileName)) {
					// YouScope XML found
					if(determineFormatString) {
						// Detect format: YouScope XML files always have fixed format
						dateRegExp = "<date>(.{21,23})\\sUTC</date>";	// Extract date strings with 21 to 23 characters, e.g. from <date>2015-03-24 14:29:11.630 UTC</date>
						dateFormatString = "yyyy-MM-dd hh:mm:ss.z";						
						expectingYouScopeXmlFiles = true;

						break;
					}
					else {
						// Make sure YouScope xmls are expected
						if(!expectingYouScopeXmlFiles) {
							// Error
							QString errMsg = QString("Error: found unexpected (merging TAT and YouScope XML files is not supported) YouScope xml file for image %1.").arg(*it);
							errors.append(errMsg);
							out(errMsg);

							oldTimePoint = tp;
							continue;
						}

						// Open file and read content
						QFile xmlFile(metaXMLFileName);
						if(!xmlFile.open(QIODevice::ReadOnly)) {
							// Error
							QString errMsg = QString("Error: xml file (%1) for image '%2' found, but cannot be opened: %3").arg(metaXMLFileName).arg(*it).arg(xmlFile.errorString());
							errors.append(errMsg);
							out(errMsg);

							oldTimePoint = tp;
							continue;
						}
						QString xml = xmlFile.readAll();

						// Extract date
						int ret = parseXmlContents(xml, imageCreationTime, dateRegExp, dateFormatString, false);

						// Check for error
						if(ret != 0 || !imageCreationTime.isValid()) {
							// Error
							QString errMsg = QString("Error: xml file (%1) for image '%2' found, but cannot read contents (error code: %3) - skipping file").arg(metaXMLFileName).arg(*it).arg(ret);
							errors.append(errMsg);
							out(errMsg);
						}
						else {
							// Date parsed successfully
							fileNotFoundOrError = false;
						}
					}
				}
				else if(QFile::exists(metaXMLFileNameTAT)) {
					// Make sure YouScope xmls are not expected
					if(expectingYouScopeXmlFiles) {
						// Error
						QString errMsg = QString("Error: found unexpected (merging TAT and YouScope XML files is not supported) TAT xml file for image %1.").arg(*it);
						errors.append(errMsg);
						out(errMsg);

						oldTimePoint = tp;
						continue;
					}

					// TAT-XML found, open it
					QFile xmlFile(metaXMLFileNameTAT);
					if(!xmlFile.open(QIODevice::ReadOnly)) {
						// Error
						QString errMsg = QString("Error: xml file (%1) for image '%2' found, but cannot be opened: %3").arg(metaXMLFileNameTAT).arg(*it).arg(xmlFile.errorString());
						errors.append(errMsg);
						out(errMsg);

						oldTimePoint = tp;
						continue;
					}
					QString xml = xmlFile.readAll();

					if(determineFormatString) {
						bool badString = false;
						if(!checkFormatString(dateFormatString, dateRegExp, xml, badString, testedFormats, dateOfLastTimePoint, *it, oldTimePoint, tp)) {
							// Current format string is invalid or xml string has unsupported format
							
							if(badString) {
								// XML string has unsupported format -> go to next file
								QString errMsg = QString("Error: xml file (%1) for image '%2' seems to have invalid format.").arg(metaXMLFileNameTAT).arg(*it);
								out(errMsg);

								// Abort if too many errors
								if(++errorCount > 100) {
									out(QString("Fatal: error count (%1) exceeds maximum number of errors. Stopping.").arg(errorCount));
									fatalError = true;
									break;
								}

								oldTimePoint = tp;
								continue;
							}

							// Format string was not empty but wrong -> now we should have a new format string that
							// has to be tested again using all images of the position (to avoid endless loop, make sure
							// we have come further than the last time this happened).
							foundValidFormatString = false;
							if(imageCount < imageLastIndexConversionFailed) {
								// Not possible to find format working for all files
								QString errMsg = QString("Error: unable to find date format working for all images.");
								out(errMsg);
								errors.append(errMsg);
								allXmlFilesInvalid = true;
								break;
							}
							imageLastIndexConversionFailed = imageCount;
							allXmlFilesInvalid = false;
							break;
						}
						allXmlFilesInvalid = false;

					}
					else {
						// Extract date
						int ret = parseXmlContents(xml, imageCreationTime, dateRegExp, dateFormatString, true);

						// Check for error
						if(ret != 0 || !imageCreationTime.isValid()) {
							// Error
							QString errMsg = QString("Error: xml file (%1) for image '%2' found, but cannot read contents (error code: %3) - skipping file").arg(metaXMLFileName).arg(*it).arg(ret);
							errors.append(errMsg);
							out(errMsg);
						}
						else {
							// Date parsed successfully
							fileNotFoundOrError = false;
						}
					}

					//// Debug
					//qDebug() << imageCreationTime.toString();
				}
			
				// Check if Metaxml file not found or invalid
				if(fileNotFoundOrError && !determineFormatString) {
					// Check if images should be moved
					if(ui.optMovePictures->isChecked()) {
						// Move picture

						// Make sure destination dir exists
						if(!QDir(directoryForImageFilesWithNoXML).exists()) {
							// Try to create
							if(!QDir(directoryForImageFilesWithNoXML).mkdir(directoryForImageFilesWithNoXML)) {
								// Error
								QString errMsg = QString("Error: xml file for image '%1' not found, but cannot create directory (%2) to move image to.").arg(*it).arg(directoryForImageFilesWithNoXML);
								errors.append(errMsg);
								out(errMsg);

								oldTimePoint = tp;
								continue;
							}
						}

						// Move file
						QFile image(directory + *it);
						QString newName = directoryForImageFilesWithNoXML + *it;

						if(!image.copy(newName)) {
							// Copying failed
							QString errMsg = QString("Error: Cannot copy file '%1' to folder '%2': %3").arg(*it).arg(directoryForImageFilesWithNoXML).arg(image.errorString());
							errors.append(errMsg);
							out(errMsg);

							oldTimePoint = tp;
							continue;
						}
						else {
							// Copying successful, remove source file
							if(!image.remove()) {
								// Removing source failed
								QString errMsg = QString("Error: copied image %1 to %2, but could not remove source file: %3").arg(*it).arg(directoryForImageFilesWithNoXML).arg(image.errorString());
								errors.append(errMsg);
								out(errMsg);

								oldTimePoint = tp;
								continue;
							}

							// Image moved successfully
							imagesWithNoXMLFile.append(*it);

							oldTimePoint = tp;
							continue;
						}
						
					}
					else {
						// Apply interval
						if(foundEntries.isEmpty())
							imageCreationTime = startTime;
						else {
							// If last pic has same timepoint index, we just take the same time value (i.e. different wls and z pics get same time)
							const InternalLogFileEntry& lastEntry = foundEntries.last();

							if(lastEntry.tp == tp)
								imageCreationTime = foundEntries.last().timeStamp;
							else
								imageCreationTime = foundEntries.last().timeStamp.addSecs(intervalSeconds);
						}

						// interval applied
						imagesWithNoXMLFile.append(*it);
					}
				
				}
				if(determineFormatString) {
					if(foundValidFormatString) {
						// Test format string on remaining images
						oldTimePoint = tp;
						continue;
					}

					// Format string was not valid -> test new format string which should be in dateFormatString on all images
					break;
				}

				// Remember entry
				foundEntries.append(InternalLogFileEntry(tp, wl, zIndex, imageCreationTime));

				// Update stats
				if(!foundWavelengths.contains(wl))
					foundWavelengths.append(wl);
				if(tp > maxTP)
					maxTP = tp;

				oldTimePoint = tp;
			}	// image loop

			// If fatal error, stop
			if(fatalError)
				break;

			// If position was used for finding format string, it has to be "done" again to check new format string or to start actual conversion
			if(determineFormatString) {
				if(foundValidFormatString) {
					// Now we have a format string and can convert files
					out(QString("Found format string (%1) - starting actual conversion..").arg(dateFormatString));
					determineFormatString = false;

					continue;
				}

				if(allXmlFilesInvalid) {
					// No working format string could be found, stop
					QString msg = QString("FATAL ERROR: unable to parse image acquisition date format. Quitting.");
					out(msg);
					errors.append(msg);
					
					return false;
				}

				// Format string failed on some image, we should have a new one now that has to be tested again on all images
				continue;
			}

			// Finally, write logfile
			QDataStream out(&logF);
			out.setByteOrder(QDataStream::LittleEndian);
			out.setFloatingPointPrecision(QDataStream::SinglePrecision);

			// Signature
			out << quint32(0x66676F6C);

			// Fileversion
			out << quint32(1);

			// Max tp + 1
			out << quint32(maxTP + 1);

			// WLs
			out << quint32(foundWavelengths.size());

			// Num entries
			out << quint32(foundEntries.size());

			// Save entries
			for(int i = 0; i < foundEntries.size(); ++i) {
				// Get entry
				const InternalLogFileEntry& curEntry = foundEntries[i];

				// Timepoint
				out << quint32(curEntry.tp);

				// WL
				out << quint16(curEntry.wl);

				// Z-Index
				out << qint16(curEntry.zIndex);

				// Timestamp
				//QString test = curEntry.timeStamp.toString(Qt::ISODate);
				//if(curEntry.tp >= 2993)
				//	qDebug() << curEntry.tp << ": " << test;
				//int test2 = curEntry.timeStamp.toTime_t();
				quint64 msSecsSinceEpoche = curEntry.timeStamp.toMSecsSinceEpoch();
				out << quint64(msSecsSinceEpoche);
			}
		}
		else {
			// Position is empty
			positionsWithNoFiles.append(curPM);
		}	// if(positionFiles.isEmpty())

		++iter;

	}	// position loop

	// Check if imagesWithNoXMLFile is not empty
	if(!imagesWithNoXMLFile.isEmpty()) {
		QString msg;
		if(ui.optMovePictures->isChecked()) {
			msg = QString("The following picture files with missing metainformation were moved to: '%1'\n\n").arg(directoryForImageFilesWithNoXML);
		}
		else {
			msg = QString("A fixed interval of %1 seconds was used for the following picture files with missing metainformation:\n\n").arg(intervalSeconds);
		}
		
		for(int i = 0; i < imagesWithNoXMLFile.size(); ++i) 
			msg += imagesWithNoXMLFile[i] + "\n";

		errors.append(msg);
		out(msg);
	}

	// Check if positionsWithNoFiles is not empty
	if(!positionsWithNoFiles.isEmpty()) {
		// Offer to delete them
		QString msg = "Position(s)\n\n";
		for(int i = 0; i < positionsWithNoFiles.size(); ++i)
			msg += positionsWithNoFiles[i]->getPictureDirectory() + "\n";

		msg += QString("\n\ncontain(s) no image files.\n\nDo you want to move the position folder(s) to %1?").arg(directoryWithEmptyPositions);

		if(QMessageBox::question(this, "Move position folders?", msg, QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes) {
			//if(QMessageBox::warning(this, "ARE YOU SURE?", "Are you sure you want to delete these position(s)?\n\nThis cannot be undone!", QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes) {
				
			// Make sure directoryWithEmptyPositions exists
			if(!QDir(directoryWithEmptyPositions).exists()) {
				if(!QDir(directoryWithEmptyPositions).mkdir(directoryWithEmptyPositions)) {
					QString errMsg = QString("Failed to create directory '%1'.").arg(directoryWithEmptyPositions);
					out(errMsg);
					errors.append(errMsg);
				}
			}
			
			// Move positions
			for(int i = 0; i < positionsWithNoFiles.size(); ++i) {
				// Get folder
				QString curFolder = positionsWithNoFiles[i]->getPictureDirectory();
				QString folderName = QDir(curFolder).dirName();
				QString newFolder = directoryWithEmptyPositions + folderName;

				// Attempt to move (only if parameters appear to be valid and destination does not exist already)
				if(folderName.isEmpty() || directoryWithEmptyPositions.isEmpty() || QDir(newFolder).exists() || !QDir(curFolder).rename(curFolder, newFolder)) {
					// Moving failed
					QString errMsg = QString("Failed to move position '%1' to '%2'.").arg(curFolder).arg(newFolder);
					out(errMsg);
					errors.append(errMsg);
				}

				// Position not available
				positionsWithNoFiles[i]->setAvailability(false);
				positionsWithNoFiles[i]->posThumbnailInTTTPosLayout = 0;
				//}

				// Update position layout
				TTTManager::getInst().frmPositionLayout->reInitializePositionDisplay();
			}
		}

	}

	if(fatalError) {
		out("Conversion FAILED. Please report this problem immediately.");
		return false;
	}

	ui.lblStatus->setText("Conversion completed.");

	// Success
	return true;
}

void TTTLogFileConverter::out( const QString& _what )
{
	ui.pteOutPut->appendPlainText(_what);
}

int TTTLogFileConverter::parseXmlContents( const QString& _xml, QDateTime& _creationTime, const QString& dateRegExp, const QString& dateFormatString, bool useTatXmlTagNumber )
{
	QString dateString; 
	if(useTatXmlTagNumber) {
		// Use regular expression to extract number of tag for creation time
		QRegExp re("<I(\\d+)>\\s*1025\\s*</I\\1>");
		int index = _xml.indexOf(re);
		if(index == -1)
			return 1;

		// Extract tag number
		QString tagNum = re.cap(1);
		if(tagNum.isEmpty())
			return 2;

		// Use tag expression to extract date string
		QRegExp tagRe(dateRegExp.arg(tagNum).arg(tagNum));
		index = _xml.indexOf(tagRe);
		if(index == -1)
			return 3;
		dateString = tagRe.cap(1);
	}
	else {
		// Use dateRegExp directly to extract date string
		QRegExp tagRe(dateRegExp);
		int index = _xml.indexOf(tagRe);
		if(index == -1)
			return 4;
		dateString = tagRe.cap(1);
	}
	if(dateString.isEmpty())
		return 5;

	// Try to transform to QDateTime
	_creationTime = QDateTime::fromString(dateString, dateFormatString);
	if(!_creationTime.isValid())
		return 6;
	else {
		_creationTime.setTimeSpec(Qt::UTC);
		return 0;
	}
}

bool TTTLogFileConverter::checkFormatString( QString& dateFormatString, QString& dateRegExp, const QString& xml, bool& unsupportedFormat, QVector<QString>& testedFormats, QDateTime& dateOfLastTimePoint, const QString& imageFileName, int prevTimePoint, int curTimePoint )
{
	// Backup dateFormatString and dateRegExp
	QString backupDateFormatString = dateFormatString;
	QString backupDateRegExp = dateRegExp;

	// Use regular expression to extract number of tag for creation time
	QRegExp re("<I(\\d+)>\\s*1025\\s*</I\\1>");
	int index = xml.indexOf(re);
	if(index == -1) {
		unsupportedFormat = true;
		return false;
	}
	QString tagNum = re.cap(1);
	if(tagNum.isEmpty()){
		unsupportedFormat = true;
		return false;
	}

	// Test current format strings
	if(!dateFormatString.isEmpty() && !dateRegExp.isEmpty()) {
		QString tagExpr = dateRegExp.arg(tagNum).arg(tagNum);
		QRegExp tagRe(tagExpr);
		index = xml.indexOf(tagRe);

		if(index != -1) {
			// Extract date string
			QString dateString = tagRe.cap(1);
			if(!dateString.isEmpty()) {
				// Convert to QDateTime
				QDateTime test = QDateTime::fromString(dateString, dateFormatString);
				if(test.isValid()) {
					// Important: treat times as UTC to avoid problems with daylight saving time
					test.setTimeSpec(Qt::UTC);

					// If timepoint changed, make sure new date is bigger than old date
					if(curTimePoint > prevTimePoint) {
						if(checkDateTime(test, dateOfLastTimePoint)) {
							// Provided format string and regExp seem to work
							dateOfLastTimePoint = test;

							// Debug
							//qDebug() << "Time:" << test.toString() << " --- " << QDateTime::fromMSecsSinceEpoch(test.toMSecsSinceEpoch()).toString();
							//QDateTime dbg;
							//dbg.setTimeSpec(Qt::UTC);
							//dbg.setMSecsSinceEpoch(test.toMSecsSinceEpoch());
							//qDebug() << dateString << " --- " << test.toString() << " --- " << dbg.toString();
					
							return true;
						}
					}
					else
						// Same time point --> do not enforce date > prevDate as e.g. this does not always apply to z-indexes
						return true;
				}
			}
		}
	}

	// We need to find a working format string, so test format strings beginning with tag expression 
	// for 19 characters date string (e.g. "2011-05-20 15:34:03")
	bool stringsWereEmpty = dateFormatString.isEmpty() && dateRegExp.isEmpty();
	QString dateString;
	QDateTime dateTest;
	dateRegExp = QString("<V%1>\\s*(.{19})\\s*</V%2>");
	QRegExp tagRe(dateRegExp.arg(tagNum).arg(tagNum));
	index = xml.indexOf(tagRe);
	dateString = tagRe.cap(1);

	if(index != -1 && !dateString.isEmpty()) {
		// Try to transform to QDateTime
		dateFormatString = "yyyy-MM-dd HH:mm:ss";
		if(!testedFormats.contains(dateFormatString))
			dateTest = QDateTime::fromString(dateString, dateFormatString);

		// Try other format if invalid
		if(!dateTest.isValid()) {
			dateFormatString = "dd.MM.yyyy HH:mm:ss";
			if(!testedFormats.contains(dateFormatString))
				dateTest = QDateTime::fromString(dateString, dateFormatString);
		}

		// Try other format if still invalid
		if(!dateTest.isValid()) {
			dateFormatString = "yyyy/MM/dd HH:mm:ss";
			if(!testedFormats.contains(dateFormatString))
				dateTest = QDateTime::fromString(dateString, dateFormatString);
		}

		// Try other format if still invalid
		if(!dateTest.isValid()) {
			dateFormatString = "yyyy.MM.dd HH:mm:ss";
			if(!testedFormats.contains(dateFormatString))
				dateTest = QDateTime::fromString(dateString, dateFormatString);
		}

		// Try other format if still invalid
		if(!dateTest.isValid()) {
			dateFormatString = "dd/MM/yyyy HH:mm:ss";
			if(!testedFormats.contains(dateFormatString))
				dateTest = QDateTime::fromString(dateString, dateFormatString);
		}

		// Try other format if still invalid
		if(!dateTest.isValid()) {
			dateFormatString = "MM/dd/yyyy HH:mm:ss";
			if(!testedFormats.contains(dateFormatString))
				dateTest = QDateTime::fromString(dateString, dateFormatString);
		}
	}

	// If date is still invalid, try other dateRegExp
	if(!dateTest.isValid()) {
		// Date could have "2011-05-20 5:34:03" form (18 or 19 characters)
		dateRegExp = QString("<V%1>\\s*(.{18,19})\\s*</V%2>");
		tagRe = QRegExp(dateRegExp.arg(tagNum).arg(tagNum));
		index = xml.indexOf(tagRe);
		dateString = tagRe.cap(1);

		if(index != -1 && !dateString.isEmpty()) {
			// Try to transform to QDateTime
			dateFormatString = "yyyy-MM-dd H:mm:ss";
			if(!testedFormats.contains(dateFormatString))
				dateTest = QDateTime::fromString(dateString, dateFormatString);
		}
	}

	// If date is still invalid, try other dateRegExp
	if(!dateTest.isValid()) {
		// Date could have "2011-08-18 3:39:46 PM" form (21 or 22 characters)
		dateRegExp = QString("<V%1>\\s*(.{21,22})\\s*</V%2>");
		tagRe = QRegExp(dateRegExp.arg(tagNum).arg(tagNum));
		index = xml.indexOf(tagRe);
		dateString = tagRe.cap(1);

		if(index != -1 && !dateString.isEmpty()) {
			// Try to transform to QDateTime
			dateFormatString = "yyyy-MM-dd h:mm:ss AP";
			if(!testedFormats.contains(dateFormatString))
				dateTest = QDateTime::fromString(dateString, dateFormatString);

			if(!dateTest.isValid()) {
				dateFormatString = "MM/dd/yyyy h:mm:ss AP";
				if(!testedFormats.contains(dateFormatString))
					dateTest = QDateTime::fromString(dateString, dateFormatString);
			}
		}
	}

	if(!dateTest.isValid()) {
		// No valid format found (that was not already used before)
		unsupportedFormat = true;

		// Restore original dateFormatString and dateRegExp
		dateFormatString = backupDateFormatString;
		dateRegExp = backupDateRegExp;

		return false;
	}

	// Found new format string
	out(QString("Attempting: '%1' (from image: '%2') - '%3'").arg(dateFormatString).arg(imageFileName).arg(dateRegExp));
	testedFormats.append(dateFormatString);
	dateOfLastTimePoint = dateTest;

	// If format strings were not set before, return true
	return stringsWereEmpty;
}

bool TTTLogFileConverter::checkDateTime( const QDateTime& dateTime, QDateTime& prevDateTime )
{
	// Check if dateTime is valid
	if(!dateTime.isValid())
		return false;

	// Check if prev date is valid
	if(!prevDateTime.isValid())
		return true;

	//// Debug
	//qDebug() << prevDateTime.toString() << " ---> " << dateTime.toString();

	// Check dateTime
	QDate oldDate = prevDateTime.date();
	QDate newDate = dateTime.date();
	if(prevDateTime.secsTo(dateTime) < 0												// dateTime must be > prevDateTime
		|| (newDate.month() != oldDate.month() && newDate.day() == oldDate.day()))		// 
		return false;

	return true;
}

void TTTLogFileConverter::forceFormatStringToggled( bool on )
{
	ui.lieForcedFormatString->setEnabled(on);
}
