/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef tttautotrackingconnectfragments_h__
#define tttautotrackingconnectfragments_h__

#include "ui_frmAutoTrackingConnectTreeFragments.h"

// Project
#include "tttbackend/treefragment.h"

// Qt
#include <QHash>
#include <QList>
#include <QVector>

// QWT
#include <qwt_plot_histogram.h>
#include <qwt_plot_picker.h>


class QwtPlotHistogram;
class TTTAutoTrackingTreeWindow;

/**
 * Tool to connect all pairs of ending tracks of tree fragments and root tracks
 * of tree fragments if temporal and spatial distance are below a user defined
 * threshold.
 */
class TTTAutotrackingConnectFragments : public QDialog {

Q_OBJECT

public:

	/**
	 * Constructor.
	 */
	TTTAutotrackingConnectFragments(TTTAutoTrackingTreeWindow* parent = 0);

private slots:

	/**
	 * Update histogram.
	 */
	void updateHistogram();

	/**
	 * Connect tree fragments.
	 */
	void connectTreeFragments();

	///**
	// * User clicked into histogram.
	// */
	//void histogramClicked(const QPointF& pos);

protected:

	/**
	 * Event filter to detect clicks into histogram.
	 */
	bool eventFilter(QObject *obj, QEvent *event);

private:

	/**
	 * Create histogram of cell movement.
	 * @param trees tree fragments.
	 * @return vector with histogram data.
	 */
	QVector<QwtIntervalSample> createHistogram(const QHash<int, QList<QSharedPointer<TreeFragment>>>& trees);

	/**
	 * Check if there is a tree fragment in treeFragments that can be prepended to the root track of the provided
	 * tree fragment, with the provided temporal and spatial search range. If a tree fragment has been found,
	 * it is removed from treeFragments.
	 * @param treeFragments tree fragments to search in.
	 * @param fragment provided tree fragment.
	 * @param outFoundTrack track number of track of found tree where provided fragment can be appended will be saved here or -1 if nothing was found.
	 * @return pointer to shared pointer of found tree or nullptr if nothing was found.
	 */
	QSharedPointer<TreeFragment>* findPrependableTreeFragment(QHash<int, QList<QSharedPointer<TreeFragment>>>& treeFragments, TreeFragment* fragment, int& outFoundTrack);

	/**
	 * Status output.
	 */
	void say(const QString& what);

	/**
	 * QWT histogram.
	 */
	QwtPlotHistogram* m_histogram;
	float m_minValue;
	float m_maxValue;

	///**
	// * Plot picker to detect clicks in histogram.
	// */
	//QwtPlotPicker* m_plotPicker;

	// Ui
	Ui::FrmAutotrackingConnectFragments ui;
};


#endif // tttautotrackingconnectfragments_h__
