/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
// ttt
#include "tttcelldivisionoverviewexport.h"
#include "tttio/celldivisionoverviewexporter.h"
#include "tttbackend/tttexception.h"
#include "tttgui/tttmovie.h"
#include "tttbackend/tttmanager.h"

// QT
#include <QFileDialog>
#include <QMessagebox>
#include <QStringList>

TTTCellDivisionOverviewExport::TTTCellDivisionOverviewExport( TTTMovie *_parent/*, int _minTimePoint, int _maxTimePoint */) : QDialog(_parent)
{
	setupUi(this);

	exporter = 0;
	movieWindow = _parent;

	// Signals/Slots
	connect(pbtStart, SIGNAL(clicked()), this, SLOT(startExport()));
	connect(pbtClose, SIGNAL(clicked()), this, SLOT(close()));
	connect(pbtSelectDir, SIGNAL(clicked()), this, SLOT(selectDir()));

	// Default values
	lieDirectory->setText(QDir::homePath());
}

void TTTCellDivisionOverviewExport::startExport()
{
	// If we have an exporter, an export is currently running
	if(exporter) {
		QMessageBox::critical(this, "Error", "Cannot start export while another export is running.");
		return;
	}

	try {
		// Get and validate input
		getInput();

		// Run export
		exporter = new CellDivisionOverviewExporter(movieWindow, motherTrack, tpsBefore, tpsAfter, interval, picSizeX, picSizeY, directory, wavelengths, hideTrackpoints, format);
		exporter->runExport();
	}
	catch(const TTTException& err) {
		QMessageBox::critical(movieWindow, "Error", "Could not export overview: " + err.what());
	}
	catch(std::exception& err) {
		QMessageBox::critical(movieWindow, "Error", QString("Could not export overview: STL-Error: ") + err.what());
	}

	if(exporter) {
		delete exporter;
		exporter = 0;
	}
}

void TTTCellDivisionOverviewExport::selectDir()
{
	// Ask user for directory ans set lieDirectory to it
	QString selection = QFileDialog::getExistingDirectory(this, "Select directory");
	if(!selection.isEmpty())
		lieDirectory->setText(selection);
}

void TTTCellDivisionOverviewExport::getInput()
{
	// Format
	if(optJPEG->isChecked())
		format = CellDivisionOverviewExporter::JPEG;
	else if(optBMP->isChecked())
		format = CellDivisionOverviewExporter::BMP;
	else
		format = CellDivisionOverviewExporter::PNG;

	hideTrackpoints = chkHideTrackpoints->isChecked();

	// Integer paramters
	interval = lieInterval->text().toInt();
	tpsBefore = lieTPsBefore->text().toInt();
	tpsAfter = lieTPsAfter->text().toInt();
	picSizeX = liePicSizeX->text().toInt();
	picSizeY = liePicSizeY->text().toInt();

	// Check them
	if(interval < 1)
		throw TTTException("Interval must be greater than 0!");
	if(tpsBefore < 0 || tpsAfter < 0)
		throw TTTException("Timepoints before and after must be greater or equal to 0!");
	if(picSizeX < 0 || picSizeY < 0)
		throw TTTException("Picture sizes must be greater than 0!");

	// Directory
	directory = lieDirectory->text();
	if(!SystemInfo::checkNcreateDirectory(directory, true, false))
		throw TTTException("Invalid directory!");

	// Mother cell
	motherTrack = 0;
	QString mother = lieMotherCell->text();
	if(mother.isEmpty()) {
		motherTrack = TTTManager::getInst().getCurrentTrack();
	}
	else {
		// Get tree
		Tree* curTree = TTTManager::getInst().getTree();

		if(curTree) {
			// Get track by number
			int number = mother.toInt();
			motherTrack = curTree->getTrack(number);
		}
	}

	// Check mother track
	if(!motherTrack)
		throw TTTException("Cannot get mother track!");
	if(!motherTrack->getChildTrack(1) || !motherTrack->getChildTrack(2))
		throw TTTException("Mother cell must have two daughter cells!");

	// Wavelengths
	QString wls = lieWLs->text();
	wavelengths.clear();

	QStringList splitWls = wls.split(",", QString::SkipEmptyParts);
	for(int i = 0; i < splitWls.size(); ++i) {
		int curWL = splitWls[i].toInt();
		if(curWL < 0 || curWL > MAX_WAVE_LENGTH)
			throw TTTException(QString("Invalid wavelength: %1!").arg(curWL));

		wavelengths.append(curWL);
	}

	if(wavelengths.size() <= 0) 
		throw TTTException("No wavelengths specified!");
}