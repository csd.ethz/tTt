/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef treeviewclickableiconitem_h__
#define treeviewclickableiconitem_h__

// Qt
#include <QGraphicsPixmapItem>

// Forward declarations
class TreeViewTreeItem;


/**
 * @author Oliver Hilsenbeck
 *
 * Represents a pixmap item that can be clicked on
 */
class TreeViewClickableIconItem : public QGraphicsPixmapItem {

public:

	// Button type (alternative to signals/slots which are not available as this is no Q_OBJECT)
	enum BUTTON_TYPE {
		SAVE_BUTTON,
		CLOSE_BUTTON
	};

	/**
	 * Constructor
	 */
	TreeViewClickableIconItem(TreeViewTreeItem* _treeViewTreeItem, QPixmap _image, BUTTON_TYPE _type);

protected:

	// Mouse pressed (needs to be reimplemented in order to be able to receive mouse release events!)
	void mousePressEvent(QGraphicsSceneMouseEvent *_event);

	// Mouse button released event
	void mouseReleaseEvent(QGraphicsSceneMouseEvent* _event);

private:

	// The TreeViewTreeItem instance this item belongs to
	TreeViewTreeItem* treeViewTreeItem;

	// Type of this button
	BUTTON_TYPE type;
};


#endif // treeviewclickableiconitem_h__
