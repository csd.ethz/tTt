/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MYQCANVASELLIPSE_H
#define MYQCANVASELLIPSE_H

#include "tttdata/track.h"
#include "tttdata/littletree.h"

#include <q3canvas.h>
#include <qpainter.h>
#include <q3pointarray.h>
#include <qpoint.h>
#include <qrect.h>

/**
	Subclass of QCanvasEllipse that provides also outline-only ellipses 
	(QCanvasEllipse only draws filled ellipses)
	
	@author Bernhard
*/

class MyQCanvasEllipse : public Q3CanvasPolygonalItem
{
public:
	/**
	 * constructs a default ellipse with radius 0 around point (0/0)
	 * @param _canvas the canvas this ellipse is associated to
	 */
	MyQCanvasEllipse (Q3Canvas* _canvas)
	: Q3CanvasPolygonalItem (_canvas), mx (0), my (0), mwidth (0), mheight (0), track (0), usage (0)
		{setZ (1);}
	
	/**
	 * constructs an ellipse around a given point with a width and height
	 * @param _x the abscisse of the middle point
	 * @param _y the ordinate of the middle point
	 * @param _width the width of the ellipse (horizontal stretch)
	 * @param _height the height of the ellipse (vertical stretch)
	 * @param _canvas the canvas this ellipse is associated to
	 */
	MyQCanvasEllipse (int _x, int _y, int _width, int _height, Q3Canvas* _canvas);
	
	/**
	 * constructs an ellipse that fits into the provided rectangle
	 * @param _rect the rectangle that is the bounding box for the ellipse
	 * @param _canvas the canvas this ellipse is associated to
	 */
	MyQCanvasEllipse (QRect _rect, Q3Canvas* _canvas);
	
	~MyQCanvasEllipse()
		{hide();}
	
	/**
	 * sets the position and size of this ellipse
	 * @param _x the abscisse of the middle point
	 * @param _y the ordinate of the middle point
	 * @param _width the width of the ellipse (horizontal stretch)
	 * @param _height the height of the ellipse (vertical stretch)
	 */
	void setEllipse (int _x, int _y, int _width, int _height);
	
	/**
	 * @return the QPointArray surrounding this line (obligatory for any subclass of QCanvasPolygonalItem) 
	 */
	Q3PointArray areaPoints() const;
	
	/**
	 * sets the usage for this object
	 * @param _usage the usage (cf. int constants defined in treedisplay.h)
	 */
	void setUsage (int _usage)
		{usage = _usage;}
	
	/**
	 * @return the usage for this object
	 */
	int getUsage() const
		 {return usage;}
	
	/**
	 * sets the associated Track object
	 * @param _track a pointer to an (existing) Track object
	 */
	void setTrack (Track *_track)
		{track = _track;}
	
	/**
	 * @return the Track object associated with this text object
	 */
	Track *getTrack() const
		{return track;}
	
	
	LittleTree *getLittleTree() const
		{return ltree;}
	
	void setLittleTree (LittleTree *_ltree)
		{ltree = _ltree;}
	
	/**
	 * @return the RTTI value for this class
	 */
	int rtti() const
		{return RTTI;}
	
	///the RTTI value for this class (1006)
	static int RTTI;

protected:
	void drawShape (QPainter&);

private:
	
	///the position of the ellipse
	int mx;
	int my;
	
	///the width and height of the ellipse
	int mwidth;
	int mheight;
	
	///the points that are the ellipse (constructed when the size is changed or in the constructor)
	Q3PointArray points;
	
	///the associated track (often very useful for detecting correspondencies)
	Track *track;
	
	///can be used for any association in the creating module
	///best way of using this attribute: define some constants that distinguish different line types and query them
	int usage;
	
	///the associated LittleTree object
	///only used in TTTFileTranslator
	LittleTree *ltree;
};

#endif
