/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TTTTEXTBOX_H
#define TTTTEXTBOX_H

#include "ui_frmTextbox.h"

#include "tttdata/systeminfo.h"
#include "tttbackend/tools.h"
#include "tttdata/userinfo.h"

#include <q3filedialog.h>
#include <qmessagebox.h>
#include <qpushbutton.h>
#include <q3textedit.h>
//Added by qt3to4:
#include <QHideEvent>
#include <QCloseEvent>
#include <QWidget>

enum TextFormUsage {TFUNone = 0, TFUFluorescenceIntegrals = 1, TFUChildrenDistances = 2};

class TTTTextbox: public QWidget, public Ui::frmTextbox {

Q_OBJECT

public:
	
	TTTTextbox (QWidget *parent = 0, const char *name = 0);
	
	void hideEvent (QHideEvent *);
	
	void closeEvent (QCloseEvent *);
	
	/**
	 * adds a line of text to the textbox
	 * @param _text the text to add
	 */
	void addLine (QString &_text);
	
	void setUsage ( const TextFormUsage& theValue )
	{
		usage = theValue;
	}
	
	TextFormUsage getUsage() const
	{
		return usage;
	}
	
	
private slots:

	void save();
	
	void clearText();
	
	void clearLastLine();
	
private:

	///how this form is currently used
	TextFormUsage usage;
	
	///whether the text was changed after the last save/clear action
	bool changed;
};

#endif
