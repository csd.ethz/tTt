/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "treeview.h"

// Qt
#include <QMessageBox>
#include <QDebug>
#include <QMouseEvent>
#include <QScrollBar>

// Project
//#include "tttbackend/treeviewsettings.h"
#include "treeviewtreeitem.h"
#include "tttbackend/tttexception.h"
#include "treeviewtimeline.h"
#include "treeviewtrackitem.h"
#include "tttbackend/itrack.h"


// Min/max scaling
const float TreeView::MIN_SCALE = 0.01f;		// 1%
const float TreeView::MAX_SCALE = 2.0f;		// 200%

// Z-values
const float TreeView::Z_TIME_SCALE = 10.0f;
const float TreeView::Z_CELL_NUMBER = 5.0f;
const float TreeView::Z_CELL_CIRCLE = 4.0f;
const float TreeView::Z_TRACK_LINE = 3.0f;
const float TreeView::Z_DIVISION_LINE = 2.0f;
const float TreeView::Z_TIME_VERTICAL_LINE = -10.0f;
const float TreeView::Z_TIME_CURRENT_TP_LINE = -5.0f;

// Default zoom: 15%
const float TreeView::DEFAULT_SCALING = 0.10f;



TreeView::TreeView(QWidget *_parent /*= 0*/, int _treeViewHeightFactor )
	: QGraphicsView(_parent)
{
	// Init variables
	/*displaySettings = _displaySettings;*/
	scrolling = false;
	ignoreNextMousePressEvent = false;
	currentlySelectedTree = 0;
	cellSelectionExclusive = false;
	maxY = -1;
	treeHeightFactor = _treeViewHeightFactor;

	// Enable anti aliasing
	setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);

	// Every TreeView has its own scene
	setScene (new QGraphicsScene (this));

	// Align top, center
	setAlignment(Qt::AlignLeft | Qt::AlignTop);

	// Make zoom like in google maps
	setTransformationAnchor(AnchorUnderMouse);

	// Create timeline 
	timeLine = new TreeViewTimeLine(this);
	timeLine->initialize();

	// Default zoom
	scale(DEFAULT_SCALING, DEFAULT_SCALING);
	scalingFactor = DEFAULT_SCALING;
}

void TreeView::addTree( ITree* _tree, bool _displayCloseIcon, bool _displaySaveIcon )
{
	try {
		// If there are already trees, move the existing trees to the right scene end
		int posX = 0;

		/*
		if(!trees.isEmpty())
			posX = sceneRect().right();
		*/

		// Create new TreeViewTreeItem and add it to the list
		TreeViewTreeItem *newItem = new TreeViewTreeItem(this, _tree, posX, _displayCloseIcon, _displaySaveIcon);

		// Add tree (+ children) to scene and to trees
		scene()->addItem(newItem);
		trees.append(newItem);

		// Move old trees to right (new tree is now last element in trees)
		if(trees.size() > 1) {
			int deltaX = newItem->sizeIncludingChildren().width();
			for(int i = 0; (i < trees.size()-1); ++i) {
				trees[i]->setPos(trees[i]->x() + deltaX, trees[i]->y());
			}
		}

		// Update scene area
		updateSceneRect();

	}
	catch(const TTTException& _e) {
		// Report error
		QString technical = QString("TTTException at TreeView::addTree(): ") + _e.what();
		qWarning() << technical;
		QMessageBox::critical(this, "Error", QString("An error occurred in the tree display. Please report this as soon as possible.\n\nTechincal details: %1").arg(technical));

		// Try to remove tree
		removeTree(_tree);
	}
}

bool TreeView::removeTree( ITree* _tree )
{
	// Find tree
	for(trees_iterator it = trees.begin(); it != trees.end(); ++it) {
		TreeViewTreeItem* curItem = *it;

		// Check if it represents _tree
		if(curItem->getTree() == _tree) {
			// Check if it is selected
			if(curItem == currentlySelectedTree)
				currentlySelectedTree = 0;

			// Remove all selected cells of this tree
			deselectCellsByTree(_tree);

			// Remove from scene
			scene()->removeItem(curItem);

			// Remove from our list
			trees.erase(it);

			// Delete from memory
			delete curItem;

			// Update scene area
			updateSceneRect();

			// Done
			return true;
		}
	}

	// Tree not found
	return false;
}

bool TreeView::redrawTree( ITree* _tree )
{
	// Find tree item
	int index = findTreeItem(_tree);
	if(index == -1)
		return false;

	// Update display
	trees[index]->updateDisplay();

	// Update scene area
	updateSceneRect();

	// Done
	return true;
}

bool TreeView::redrawAllTrees()
{
	// Redraw all trees
	for(int i = 0; i < trees.size(); ++i) 
		trees[i]->updateDisplay();

	// Update scene area
	updateSceneRect();

	// Done
	return true;
}

bool TreeView::updateDisplayName( ITree* _tree )
{
	// Find tree item
	int index = findTreeItem(_tree);
	if(index == -1)
		return false;

	// Update name
	trees[index]->updateTreeNameDisplay();

	// Update scene rect
	updateSceneRect();

	// Done
	return true;
}

bool TreeView::selectTree( ITree* _tree )
{
	// Find tree item
	int index = findTreeItem(_tree);
	if(index == -1)
		return false;

	// Deselect old tree
	if(currentlySelectedTree)
		deselectTree();

	// Set selected tree
	currentlySelectedTree = trees[index];
	currentlySelectedTree->setSelectionState(true);
	return true;
}

void TreeView::deselectTree()
{
	// Deselect current tree
	if(currentlySelectedTree) {
		currentlySelectedTree->setSelectionState(false);
		currentlySelectedTree = 0;
	}
}

bool TreeView::clearDisplay()
{
	// Remove all trees
	for(trees_iterator it = trees.begin(); it != trees.end(); ++it) {
		TreeViewTreeItem* curItem = *it;

		// Remove from scene and delete
		scene()->removeItem(curItem);
		delete curItem;
	}
	trees.clear();

	// No tree and no cells selected
	currentlySelectedTree = 0;
	currentlySelectedCells.clear();

	// Update scene area
	updateSceneRect();

	return true;
}


void TreeView::wheelEvent( QWheelEvent *_event )
{
	// Zoom
	scaleView(pow((double)2, _event->delta() / 400.0));
}

void TreeView::scaleView( qreal _scaleFactor )
{
	// Check boundaries
	if(scalingFactor * _scaleFactor < MIN_SCALE) 
		_scaleFactor =  MIN_SCALE / scalingFactor;
	else if(scalingFactor * _scaleFactor > MAX_SCALE)
		_scaleFactor = MAX_SCALE / scalingFactor;

	// Set zoom
	scale(_scaleFactor, _scaleFactor);

	// Emit zoomChanged
	scalingFactor *= _scaleFactor;
	emit zoomChanged(scalingFactor);
}

void TreeView::mousePressEvent( QMouseEvent *_event )
{
	// Default implementation
	QGraphicsView::mousePressEvent(_event);

	// Start mouse navigation
	if(!ignoreNextMousePressEvent) {
		mouseMoveStartX = _event->globalX();
		mouseMoveStartY = _event->globalY();
		scrolling = true;
	}
	else 
		ignoreNextMousePressEvent = false;
}

void TreeView::mouseReleaseEvent( QMouseEvent *_event )
{
	// Default implementation
	QGraphicsView::mouseReleaseEvent(_event);

	// Stop mouse navigation
	scrolling = false;
}

void TreeView::mouseMoveEvent( QMouseEvent *_event )
{
	// Default implementation
	QGraphicsView::mouseMoveEvent(_event);

	if(!scrolling)
		return;

	// Get mouse pos
	int x = _event->globalX(),
		y = _event->globalY();

	// Get delta
	int deltaX = mouseMoveStartX - x,
		deltaY = mouseMoveStartY - y;

	// Get new scrollbar positions
	int scrollX = horizontalScrollBar()->value() + deltaX,
		scrollY = verticalScrollBar()->value() + deltaY;

	// Set scrollbar values
	horizontalScrollBar()->setValue(scrollX);
	verticalScrollBar()->setValue(scrollY);

	// Set start positions
	mouseMoveStartX = x;
	mouseMoveStartY = y;
}

void TreeView::updateSceneRect()
{
	int maxHeight = maxY,
		maxWidth = 0;

	// Find required space for tree that has biggest width and height
	for(const_trees_iterator it = trees.constBegin(); it != trees.constEnd(); ++it) {
		TreeViewTreeItem* curTree = *it;
		int curWidth = curTree->x() + curTree->sizeIncludingChildren().width();
		int curHeight = curTree->y() + curTree->sizeIncludingChildren().height();

		maxHeight = qMax(maxHeight, curHeight);
		maxWidth = qMax(maxWidth, curWidth);
	}

	// Add margins
	maxHeight += MARGIN_RIGHT_BOTTOM + MARGIN_TOP_LEFT + TreeViewTimeLine::TOP_MARGIN;
	maxWidth += MARGIN_RIGHT_BOTTOM + MARGIN_TOP_LEFT;

	// Add timeline width (right actually means width in this case)
	maxWidth += timeLine->getWidth();

	// Finally update scene rect
	scene()->setSceneRect(-timeLine->getWidth() - MARGIN_TOP_LEFT, -MARGIN_TOP_LEFT - TreeViewTimeLine::TOP_MARGIN, maxWidth, maxHeight);
}

void TreeView::notifyCellSelection(TreeViewTreeItem* _tree, TreeViewTrackItem* _track)
{
	// Check if cell is already selected
	QPair<TreeViewTreeItem*, int> cell(_tree, _track->getTrack()->getTrackNumber());
	bool select;
	if(currentlySelectedCells.contains(cell)) {
		// Deselect cell
		select = false;
		//_track->setSelectionState(false);
		//currentlySelectedCells.removeOne(cell);
	}
	else {
		//// If exclusive, deselect all other cells first
		//if(cellSelectionExclusive)
		//	deselectAllCells();

		// Select cell
		select = true;
		//_track->setSelectionState(true);
		//currentlySelectedCells.append(cell);
	}

	// Emit signal
	if(select)
		emit cellSelected(_tree->getTree(), _track->getTrack());
	else
		emit cellDeSelected(_tree->getTree(), _track->getTrack());
}

void TreeView::notifyTreeSaveButtonClicked( ITree* _tree )
{
	emit saveTreeClicked(_tree);
}

void TreeView::notifyTreeCloseButtonClicked( ITree* _tree )
{
	emit closeTreeClicked(_tree);
}

void TreeView::notifyTreeLeftClicked( ITree* _tree )
{
	emit treeLeftClicked(_tree);
}

int TreeView::findTreeItem( ITree* _tree )
{
	// Iterate over items
	for(int i = 0; i < trees.size(); ++i) {
		// Check if item represents _tree
		if(trees[i]->getTree() == _tree) 
			return i;
	}

	// Item not found
	return -1;
}

void TreeView::setCellSelectionMode( bool _exclusive )
{
	// Nothing to do
	if(_exclusive == cellSelectionExclusive)
		return;

	// Change
	cellSelectionExclusive = _exclusive;

	// Deselect all cells
	deselectAllCells();
}

void TreeView::deselectAllCells()
{
	// Deselect all cells
	for(QList<QPair<TreeViewTreeItem*, int> >::iterator it = currentlySelectedCells.begin(); it != currentlySelectedCells.end(); ++it) {
		TreeViewTreeItem* curItem = it->first;
		int trackNum = it->second;

		// Try to get track item
		TreeViewTrackItem* trackItem = curItem->getTrackItem(trackNum);
		if(trackItem) {
			trackItem->setSelectionState(false);

			// Emit signal
			emit cellDeSelected(curItem->getTree(), trackItem->getTrack());
		}
	}
	currentlySelectedCells.clear();
}

void TreeView::deselectCellsByTree( ITree* _tree )
{
	// Iterate over selected cells
	QList<QPair<TreeViewTreeItem*, int> >::iterator it = currentlySelectedCells.begin();
	while(it != currentlySelectedCells.end()) {
		TreeViewTreeItem* curItem = it->first;
		int trackNum = it->second;

		// Check if cell belongs to _tree
		if(curItem->getTree() == _tree) {
			// Try to get track item
			TreeViewTrackItem* trackItem = curItem->getTrackItem(trackNum);
			if(trackItem)
				trackItem->setSelectionState(false);

			// Remove item and continue
			it = currentlySelectedCells.erase(it);
			continue;
		}

		// Go to next
		++it;
	}
}

void TreeView::updateCellSelectionStatesForTreeItem( TreeViewTreeItem* _treeItem ) const
{
	// Iterate over cells
	for(QList<QPair<TreeViewTreeItem*, int> >::const_iterator it = currentlySelectedCells.begin(); it != currentlySelectedCells.end(); ++it) {
		TreeViewTreeItem* curItem = it->first;

		// Check if it belongs to _treeItem
		if(curItem == _treeItem) {
			// Try to get track item
			int trackNum = it->second;
			TreeViewTrackItem* trackItem = curItem->getTrackItem(trackNum);
			if(trackItem)
				// Call setSelectionState
				trackItem->setSelectionState(true);
		}
	}
}

void TreeView::setMaxY( int _maxY )
{
	maxY = _maxY;

	updateSceneRect();
}

bool TreeView::selectCell( ITree* _tree, ITrack* _track, bool _selected )
{
	// Find tree item
	int index = findTreeItem(_tree);
	if(index == -1)
		return false;

	// Get track item
	TreeViewTrackItem* trackItem = trees[index]->getTrackItem(_track->getTrackNumber());
	if(!trackItem)
		return false;

	// Check if cell is already selected
	QPair<TreeViewTreeItem*, int> cell(trees[index], _track->getTrackNumber());
	if(currentlySelectedCells.contains(cell)) {
		// Check if it should be deselected
		if(!_selected) {
			// Deselect cell
			trackItem->setSelectionState(false);
			currentlySelectedCells.removeOne(cell);
		}
	}
	else {
		// Check if it should be selected
		if(_selected) {
			// If exclusive, deselect all other cells first
			if(cellSelectionExclusive)
				deselectAllCells();

			// Select cell
			trackItem->setSelectionState(true);
			currentlySelectedCells.append(cell);
		}
	}

	return true;
}

QColor TreeView::mapConfidenceOnColor( float _confidence )
{
	// Map [0, 1] on color code
	if(_confidence >= 0.0f && _confidence <= 1.0f) {
		int tmp = _confidence * (2 * 255);
		if(tmp <= 255)
			return qRgb(255, tmp, 0);
		else
			return qRgb(255 - (tmp - 255), 255, 0);
	}
	else if(_confidence == 2.0f)
		// 2 indicates: manually inspected
		return QColor(Qt::magenta);
	else
		// Not set or invalid
		return Qt::black;

}

void TreeView::setTimeLineTimePoint( int _tp )
{
	// Delegate to timeLine
	if(timeLine)
		timeLine->setCurrentTimePoint(_tp);
}

void TreeView::updateTimeLinePosition( int positionIndex )
{
	// Delegate
	if(timeLine)
		timeLine->setCurrentPosition(positionIndex);
}

void TreeView::highlightTrack( ITree* _tree, int _trackNumber, int _startTp, int _stopTp )
{
	// Get tree item
	int index = findTreeItem(_tree);
	if(index == -1)
		return;

	// Get track item
	TreeViewTrackItem* trackItem = trees[index]->getTrackItem(_trackNumber);
	if(!trackItem)
		return;

	// Set highlighting
	trackItem->setHighlighting(_startTp, _stopTp);
}

void TreeView::stopHighlightingTrack( ITree* _tree, int _trackNumber )
{
	// Get tree item
	int index = findTreeItem(_tree);
	if(index == -1)
		return;

	// Get track item
	TreeViewTrackItem* trackItem = trees[index]->getTrackItem(_trackNumber);
	if(!trackItem)
		return;

	// Stop highlighting
	trackItem->setHighlighting(-1, -1);
}

void TreeView::timeLineSetHighlighting( int startTp, int stopTp )
{
	// Delegate
	if(timeLine)
		timeLine->setHighlighting(startTp, stopTp);
}

void TreeView::setTreeHeightFactor( int _treeHeightFactor )
{
	treeHeightFactor = _treeHeightFactor;

	// Update timeline
	if(timeLine)
		timeLine->updateDisplay();

	// Update trees
	for(int i = 0; i < trees.size(); ++i)
		trees[i]->updateDisplay();
	updateSceneRect();
}

void TreeView::setShowRealTimeInTimeScale( bool setShowRealTime )
{
	// Delegate
	if(timeLine)
		timeLine->setShowRealTime(setShowRealTime);
}

void TreeView::redrawTimeScale()
{
	// Just redraw
	if(timeLine)
		timeLine->update();
}

bool TreeView::exportView(const QString& fileName)
{
	QGraphicsScene* s = scene();

	// Prepare export mode
	//timeLine->setCurrentTimePointBarVisible(false);
	timeLine->setVisible(false);
	for(int i = 0; i < trees.size(); ++i) {
		trees[i]->setShowTreeNameAndIcons(false);
	}
	QBrush defaultBrush = s->backgroundBrush();
	s->setBackgroundBrush(QBrush(Qt::black));	// Black for poster

	// Export, but limit width to 20000 pixels
	float width = s->width();
	float height = s->height();
	if(width > 6200.0f) {
		float f = 6200.0f / width;
		width *= f;
		height *= f;
	}
	QImage img(width, height, QImage::Format_ARGB32);
	img.fill(Qt::white);
	QPainter p(&img);
	s->render(&p);

	// Restore previous mode
	//timeLine->setCurrentTimePointBarVisible(true);
	timeLine->setVisible(true);
	for(int i = 0; i < trees.size(); ++i) {
		trees[i]->setShowTreeNameAndIcons(true);
	}
	s->setBackgroundBrush(defaultBrush);	// Black for poster


	// Save image and return
	return img.save(fileName);
}

void TreeView::setTrackNumbersVisible(bool visible)
{
	// Show or hide the track numbers in all trees
	for(int i = 0; i < trees.size(); ++i) {
		trees[i]->setTrackNumbersVisible(visible);
	}
}

//void TreeView::scrollContentsBy( int _dx, int _dy )
//{
//	// Update contents
//	QGraphicsView::scrollContentsBy (_dx, _dy);
//
//	// Update timeline
//	timeLine->updateDisplay();
//}
