/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CALLBACKITEM_H
#define CALLBACKITEM_H

#include <QString>
#include <QGraphicsItem>
#include <QMouseEvent>

/**
 * @author Bernhard
 *
 * This class provides the feature to call a specific callback function; it can be inherited by any object that needs this function
 * Two callback methods are provided, as intended for mousePressEvent() and mouseReleaseEvent()
 * The user can set a string parameter (same for both) for these functions.
 */
class CallbackItem
{

public:
        CallbackItem (const QString &_parameter = "");

        void setMousePressCallBackMethod (void (*_callBackMP) (const QString &, void *, QMouseEvent *));
        void setMouseReleaseCallBackMethod (void (*_callBackMR) (const QString &, void *, QMouseEvent *));

        void setParameter (const QString &_param)
                {parameter = _param;}
        const QString getParameter() const
                {return parameter;}

protected:

        //call the callback methods (the parameter is passed from the calling object, usually its THIS-pointer)
        void callCBMP (void *, QMouseEvent *);
        void callCBMR (void *, QMouseEvent *);

private:

        ///the callback function for the mouse press event
        void (*callBackMP) (const QString &, void *, QMouseEvent *);

        ///the callback function for the mouse release event
        void (*callBackMR) (const QString &, void *, QMouseEvent *);

        ///the int parameter for the callback methods
        QString parameter;
};


#endif // CALLBACKITEM_H

