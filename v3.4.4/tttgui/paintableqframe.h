/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PAINTABLEQFRAME_H
#define PAINTABLEQFRAME_H

#include <QFrame>
#include <QPixmap>

/**
     @author Bernhard

     This subclass of QFrame is necessary to keep bitblt-ing in this widget within the graphical display chain of qt4.
     Thus, its main function is to bitblt the locally referenced pixmap into the complete frame.
     The pixmap itself is set from outside, further options (clip region, composition mode, ...) are currently not available, but could be implemented.

  */

class PaintableQFrame : public QFrame
{
        Q_OBJECT

public:
        PaintableQFrame (QWidget *_parent);

        virtual void paintEvent (QPaintEvent *);

        void setPixmap (QPixmap *_pixmap);

private:

        ///the pixmap to be bitblt-ed in paintEvent()
        QPixmap *pix;
};

#endif // PAINTABLEQFRAME_H
