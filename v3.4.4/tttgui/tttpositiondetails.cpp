/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "tttpositiondetails.h"

// Project includes
#include "tttbackend/tools.h"
#include "tttdata/userinfo.h"
#include "tttgui/tttmovie.h"


#include "tttbackend/tttmanager.h"
//Added by qt3to4:
#include <QPixmap>
#include <QResizeEvent>
#include <QShowEvent>
#include <Q3ValueList>
#include <QCloseEvent>
#include <QPaintEvent>

TTTPositionDetails::TTTPositionDetails(QWidget *parent, const char *name)
    :QWidget(parent, name), img (0), pix (0), wl0width (0), wl0height (0)
{
        setupUi (this);
	connect ( pbtClose, SIGNAL (clicked()), this, SLOT (close()));
	
	connect ( cboWavelength, SIGNAL (activated (const QString &)), this, SLOT (setWavelength (const QString &)));
	
//	connect ( tabInformation, SIGNAL (selected (const QString &)), this, SLOT (draw (const QString &)));

        fraPicture = new PaintableQFrame (wdgContainer);

        thumbnail = 0;

	windowLayoutLoaded = false;
}

TTTPositionDetails::~TTTPositionDetails()
{
	if (pix)
		delete pix;
	if (img)
		delete img;

}


void TTTPositionDetails::setPositionThumbnail (PositionThumbnail* _thumbnail)
{
	if (! _thumbnail)
		return;

	thumbnail = _thumbnail;
	
	//load all available wavelengths for this position
	//Q3ValueList<int> availableWavelengths = TTTManager::getInst().getCurrentPositionManager()->getAvailableWavelengths();
	//Q3ValueList<int>::const_iterator iter;
	cboWavelength->clear();
	const TTTPositionManager* tttpm = TTTManager::getInst().getCurrentPositionManager();
	if(!tttpm)
		return;
	const QSet<int>& availableWavelengths = tttpm->getAvailableWavelengthsByRef();
	for (QSet<int>::const_iterator iter = availableWavelengths.constBegin(); iter != availableWavelengths.constEnd(); ++iter) {
		cboWavelength->insertItem (QString ("%1").arg (*iter));
	}
        
	
	// ToDo:
	////NOTE: img must not be the same picture (in memory) as in the thumbnail => copy() is necessary
	//if (_thumbnail->getOriginalImage()) {
	//	img = new QImage();
	//	*img = _thumbnail->getOriginalImage()->copy();
	//	
	//	//assumes that the set picture is always of wavelength 0
	//	wl0width = img->width();
	//	wl0height = img->height();
	//}
	img = 0;
	
	if (! img) {
		//image was not loaded (due to the fast loading process limitations in TTTPositionLayout)
		//=> seek for first picture in the current folder and display it
		
		QVector<QString> pics = SystemInfo::listFiles (TTTManager::getInst().getCurrentPositionManager()->getPictureDirectory(), QDir::Files, "*.jpg;*.tif", false);
		QVector<QString>::const_iterator iterFiles = pics.constBegin();
		
		if (iterFiles != pics.constEnd()) {
			QString picfilename = *iterFiles;
			
			img = new QImage (picfilename);
			if (img->isNull()) {
				delete img;
				img = 0;
			}
			else {
				//assumes that the first picture is always of wavelength 0
				wl0width = img->width();
				wl0height = img->height();

				if(!thumbnail->getFoundFirstImage()) {
					thumbnail->setFirstImage(QPixmap::fromImage(*img));
					thumbnail->updateDisplay();
				}
			}
			
		}
	}
	
	
	
	if (img)
		pix = new QPixmap();
	
	
	
	
	//read first tracks for this position
	//are available afterwards in TTTManager::getInst().getCurrentPositionManager()->getAllTrackPoints()
	//TTTManager::getInst().getCurrentPositionManager()->setATPfirstTrackPointsOnly (true);
	//TTTManager::getInst().getCurrentPositionManager()->readAllTrackPoints (TTTManager::getInst().getCurrentPositionManager()->getTTTFileDirectory(), true);
	//TTTManager::getInst().getCurrentPositionManager()->readAllTrackPoints (TTTManager::getInst().getCurrentPositionManager()->getTTTFileDirectory (false, true), true, true);
	
	
	
}

void TTTPositionDetails::resizeEvent (QResizeEvent *)
{

    //adopt the fraPicture frame to fit exactly into its containing widget
    fraPicture->setGeometry (0, 0, wdgContainer->width(), wdgContainer->height());

	if (img) {
		if (pix) {
			int width = fraPicture->geometry().width();
			int height = fraPicture->geometry().height();
			
			pix->convertFromImage (img->smoothScale (width, height));
			
			//paint first tracks, too, if they are available
			TTTPositionManager *positionManager = TTTManager::getInst().getCurrentPositionManager();
			if (positionManager->frmMovie && positionManager->frmMovie->getExternalTracksMode() != TTTMovie::OFF) {
				TTTMovie::DisplayExternalTracksMode mode = positionManager->frmMovie->getExternalTracksMode();
				QPainter p (pix);
				
				int colonyIndex = 0;
				int colorIndex = 0;
				p.setPen (ColonyColor [colorIndex]);
				
				//regard current display size
				float ZoomFactorX = (float)width / (float)img->width();
				float ZoomFactorY = (float)height / (float)img->height();
				
				//regard different sizes of phase contrast and fluorescent pictures
				ZoomFactorX *= (float)img->width() / (float)wl0width;
				ZoomFactorY *= (float)img->height() / (float)wl0height;
				
				QLinkedList<TrackPoint> externalTrackpoints = positionManager->getExternalTrackpoints(positionManager->getTimepoint());
				for (QLinkedList<TrackPoint>::const_iterator iter = externalTrackpoints.constBegin(); iter != externalTrackpoints.constEnd(); ++iter) {
					
					if ((*iter).X != -1) {
						
                                                //QPointF tmpP = positionManager->getDisplays().at (Index).calcTransformedCoords ((*iter).point());
						//transform coordinates from real ones to fit the current picture size
						float x = (*iter).point().x();
						float y = (*iter).point().y();
						if (TTTManager::getInst().USE_NEW_POSITIONS())
							//@todo
							;
						else 
							x = x * ZoomFactorX;
							y = y * ZoomFactorY;
						
                                                QPointF tmpP (x, y);
						
						float radius = CELLCIRCLESIZE;
						
						p.drawEllipse ((int)(tmpP.x() - radius / 2.0f), (int)(tmpP.y() - radius / 2.0f), (int)radius, (int)radius);
						
						if (mode == TTTMovie::FIRST_ALL_POS || mode == TTTMovie::FIRST_CUR_POS)
							p.drawText ((int)(tmpP.x() + radius / 2.0f), (int)(tmpP.y() - radius / 2.0f), QString ("Col %1, TP %2").arg ((*iter).tmpColonyNumber).arg ((*iter).TimePoint));
						
					}
					else if ((*iter).TimePoint == -10)
						//special trackpoint that indicates a new colony
						p.setPen (GraphicAid::mainColor (++colonyIndex));
				}
				
				p.end();
			}
			
            //draw();
            fraPicture->setPixmap (pix);
            draw();
		}
	}
}

void TTTPositionDetails::paintEvent (QPaintEvent *)
{
        //drawTimer.singleShot (0, (QObject*)this, SLOT (draw()));
        //draw();
}

void TTTPositionDetails::closeEvent (QCloseEvent *_ev)
{
	//emit positionStatisticClosed (thumbnail);

	// Save window layout
	UserInfo::getInst().saveWindowPosition(this, "TTTPositionDetails");
	
	////must be cleared to avoid their automatic display in frmMovie
	//if (TTTManager::getInst().getCurrentPositionManager())
	//	TTTManager::getInst().getCurrentPositionManager()->getAllTrackPoints().clear();
	
	_ev->accept();
}

//EVENT:
void TTTPositionDetails::showEvent (QShowEvent *_ev)
{
	resizeEvent (0);

	if(!_ev->spontaneous() && !windowLayoutLoaded) {
		// Load window layout
		UserInfo::getInst().loadWindowPosition(this, "TTTPositionDetails");

		windowLayoutLoaded = true;
	}

	_ev->accept();
}

//SLOT:
void TTTPositionDetails::draw ()
{
        //if (pix)
                //bitBlt (fraPicture, 0, 0, pix, 0, 0, pix->width(), pix->height(), QPainter::CompositionMode_Source);
                //bitBlt (this, 0, 0, pix, 0, 0, pix->width(), pix->height(), QPainter::CompositionMode_Source);

        fraPicture->repaint();
}

//SLOT:
//void TTTPositionDetails::draw (const QString &)
//{
//	drawTimer.singleShot (0, (QObject*)this, SLOT (draw()));
//}
//
//SLOT:
void TTTPositionDetails::setWavelength (const QString &_wavelength)
{
	int wl = _wavelength.toInt();
	
	QString pattern = QString ("*_w%1*.").arg (wl);
	pattern = pattern + "jpg" + ";" + pattern + "tif";
	
        QVector<QString> pics = SystemInfo::listFiles (TTTManager::getInst().getCurrentPositionManager()->getPictureDirectory(), QDir::Files, pattern, false);
        QVector<QString>::const_iterator iterFiles = pics.constBegin();
	
	if (iterFiles != pics.constEnd()) {
		const QString picfilename = *iterFiles;
		
		if (img)
			delete img;
		
		img = new QImage (picfilename);
		if (img->isNull()) {
			delete img;
			img = 0;
		}
		
		resizeEvent (0);
                //repaint();
                draw();
	}
}



//#include "tttpositiondetails.moc"
