/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TTTREGIONSELECTION_H
#define TTTREGIONSELECTION_H

//#include "tttmain.h"
#include "ui_frmRegionSelection.h"
#include "rectbox.h"
#include "tttdata/track.h"
#include "tttbackend/tttmanager.h"

#include <qpushbutton.h>

#include <qpainter.h>
#include <qpen.h>
#include <qpaintdevice.h>
#include <qcursor.h>
#include <q3popupmenu.h>
#include <qevent.h>
#include <qpoint.h>
#include <q3intdict.h>
//Added by qt3to4:
#include <QPaintEvent>
#include <QMouseEvent>
#include <QKeyEvent>
#include <QContextMenuEvent>
#include <QCloseEvent>
#include <QDialog>
#include <QPixmap>

class TTTPositionManager;

/**
@author Bernhard
*	
*	The class for the loading and displaying region selection GUI
*/

class TTTRegionSelection: public QDialog, public Ui::frmRegion_Selection {
Q_OBJECT
public:
	TTTRegionSelection (TTTPositionManager *_positionManager, QWidget *parent = 0, const char *name = 0);
	
	~TTTRegionSelection ();
	
	/**
	 * initializes the form
	 * @param _scale the real size of the first phase contrast picture
	 * @param _zoom the zoom factor (only relevant for this form!)
	 * @param parent the parent form
	 * @param name the name of this form
	 */
	void init (QRect &_scale, float _zoom = 1.0, QWidget *parent = 0, const char *name = 0);
	
	///allocates the attribute CurrentBox with the (externally constructed) _box
        //void setBox (RectBox *_box);
        void setBox (const QRect &_rect, QColor _color, float _zoom);

	/**
	 * @return the currently displayed box
	 */
	RectBox* getCurrentBox() const
		{return CurrentBox;}
	
	///sets the boundaries for shifting and resizing the current box
	void setBoundingBox (const RectBox &_boundingBox, bool _consider = true);
	
	///see above, yet more attributes to screw on
	void setBoundingRect (QRect _boundingRect, QColor _color, float _zoom, bool _consider = true);
	
	///allocates the attribute Scale with _scale
	///(Scale contains the size of an image)
	void setScale(QRect &_scale);
	
	void mousePressEvent (QMouseEvent *);
	void mouseMoveEvent (QMouseEvent *);
	void mouseReleaseEvent (QMouseEvent *);
	void keyPressEvent (QKeyEvent *);
	
	void closeEvent (QCloseEvent *);

	void showEvent (QShowEvent *_ev);
	
	void paintEvent (QPaintEvent *);
	
	void contextMenuEvent (QContextMenuEvent *);
	
        void resizeEvent (QResizeEvent *);

	///getter & setter for showing cells in this zoom windows
	void setShowCells (bool _on) 
		{ShowCells = _on;}
	bool getShowCells() const
		{return ShowCells;}
	
        ///draws the box and refreshes the display
        void drawBox();

signals:
	
	///emitted when the user resizes or moves the current box
	// /the parameter is the current box
	void boxSizeChanged ();//RectBox *);
        void boxPositionChanged (QPointF);//RectBox *);
	
public slots:
	
	///resets the current box to its original bounds (specified in the constructor)
	///called after the user selected the "reset" context menu entry
	void resetCurrentBox();
	
private:
	
	///the position manager that is responsible for this instance
	///is set in the constructor by TTTPositionManager itself
	TTTPositionManager *positionManager;
	
        ///the image in which all painting is performed and which is drawn on paintEvent() - necessary due to Qt4's new painting system
        QImage *img;

	///internal storage of the pressed mouse button
	char mouseButton;
	
	///the old mouse position
	int posMouseX;
	int posMouseY;
	
	///the factor with which the form size should be stretched 
	///(basis is the original dimension specified in the constructor)
	float ZoomFactor;
	
	///contains the internal scale (the size of an image)
	QRect Scale;
	
	///the current box
	///at the moment, there exists only one...
	RectBox *CurrentBox;
	
	///the box in which the current box should be inside
        RectBox *BoundingBox;
	
	///if true, the current box cannot be dragged outside the bounding box
	bool ConsiderBounds;
	
	///if true, the cells/tracks of the current timepoint/selection are displayed
	bool ShowCells;
	
	///window layout already loaded
	bool windowLayoutLoaded;

	///displays the cell points for the current timepoint
	void showCells();
};

#endif
