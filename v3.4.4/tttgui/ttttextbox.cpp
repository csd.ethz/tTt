/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ttttextbox.h"

#include "tttbackend/tttmanager.h"
//Added by qt3to4:
#include <QHideEvent>
#include <QCloseEvent>

TTTTextbox::TTTTextbox(QWidget *parent, const char *name)
    :QWidget(parent, name)
{
        setupUi (this);

	setUsage (TFUNone);
	
	connect ( pbtClose, SIGNAL (clicked()), this, SLOT (close()));
	connect ( pbtClear, SIGNAL (clicked()), this, SLOT (clearText()));
	connect ( pbtClearLastLine, SIGNAL (clicked()), this, SLOT (clearLastLine()));
	connect ( pbtSave, SIGNAL (clicked()), this, SLOT (save()));
	
	changed = false;
}

void TTTTextbox::save()
{
	//QDir nas (SystemInfo::getTTTFileFolderFromBaseName (TTTManager::getInst().getBasePositionManager()->getBasename(), true));
	QDir nas (TTTManager::getInst().getBasePositionManager()->getTTTFileDirectory (true));
	
	//ask for filename
	QString filename = "";
	switch (usage) {
		case TFUFluorescenceIntegrals: {
			QString suggestion = nas.absPath() + "/" + UserInfo::getInst().getUsersign() + "_fluor_integrals.csv";
			filename = Q3FileDialog::getSaveFileName (suggestion, "CSV files (*.csv)", this, 0, "Enter a filename");
			break;
		}
		default: {
			QString suggestion = nas.absPath() + "/" + UserInfo::getInst().getUsersign() + "_text.txt";
			filename = Q3FileDialog::getSaveFileName (suggestion, "Text files (*.txt)", this, 0, "Enter a filename");
		}
	}
	
	if (! filename.isEmpty()) {
		if (! Tools::writeTextFile (filename, txtTextbox->text()))
			QMessageBox::information (this, "Problem while saving", "Saving " + filename + " failed. Check your permissions.", "Ok");
		else
			changed = false;
	}

}

void TTTTextbox::clearText()
{
	if (changed) {
		if (QMessageBox::question (this, "Text not saved", "Do you want to save the changes?", "Yes", "No") == 0)
			save();
		else
			changed = false;
		
		if (changed)
			//saving failed
			return;
	}
	
	txtTextbox->clear();
	changed = false;
}

void TTTTextbox::addLine (QString &_text)
{
	txtTextbox->insertParagraph (_text, -1);
	changed = true;
}

void TTTTextbox::clearLastLine()
{
	txtTextbox->undo();
	changed = true;
}

void TTTTextbox::hideEvent (QHideEvent *)
{
}

void TTTTextbox::closeEvent (QCloseEvent *_ev)
{
	if (changed) {
		if (QMessageBox::question (this, "Text not saved", "Do you want to save the changes?", "Yes", "No") == 0)
			save();
		else
			changed = false;
		
		if (changed) {
			//saving failed
			_ev->ignore();
			return;
		}
	}
	
	txtTextbox->clear();
	changed = false;
	setUsage (TFUNone);
	
	_ev->accept();
}


//#include "ttttextbox.moc"

