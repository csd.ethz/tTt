/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "trackingmenubar.h"

// Project includes
#include "tttgui/TTTTracking.h"
#include "tttbackend/tttmanager.h"
#include "tttgui/tttpositionlayout.h"

// Qt includes
#include <QSignalMapper>



TrackingMenuBar::TrackingMenuBar(TTTTracking* _parent) : QMenuBar(_parent), actTracking_INHERIT_WLS(15)
{
	trackingWindow = _parent;

	// Create menus
	createMenus();

	// Connect actions
	connectActions();

	//// Necessary to prevent the application from breaking the menu apart
	//setMinimumSize (0xFFFFFF, 25);
}
//
void TrackingMenuBar::createMenus()
{
	// File
	fileMenu = addMenu("&File");

	fileSubMenuLoadTrees = fileMenu->addMenu("&Open");

	fileSubMenuLoadPictures = fileMenu->addMenu("Load &Pictures");

	actFile_OPEN_EXISTINGCOLONY = fileSubMenuLoadTrees->addAction("&Existing Colony");
	actFile_OPEN_EXISTINGCOLONY->setShortcut (Qt::CTRL + Qt::Key_O);

	actFile_OPEN_ADDNEWCOLONY = fileSubMenuLoadTrees->addAction("&New Colony");
	actFile_OPEN_ADDNEWCOLONY->setShortcut (Qt::CTRL + Qt::Key_N);

	fileSubMenuLoadTrees->addSeparator();

	actFile_OPEN_NEXT_TREE = fileSubMenuLoadTrees->addAction("Ne&xt Tree of Position");
	actFile_OPEN_NEXT_TREE->setShortcut(Qt::CTRL + Qt::Key_X);
	actFile_OPEN_NEXT_TREE->setEnabled(false);

	actFile_OPEN_PREV_TREE = fileSubMenuLoadTrees->addAction("&Previous Tree of Position");
	actFile_OPEN_PREV_TREE->setShortcut(Qt::CTRL + Qt::Key_P);
	actFile_OPEN_PREV_TREE->setEnabled(false);

	fileSubMenuLoadTrees->addSeparator();

	actFile_OPEN_LAST_AUTOSAVE = fileSubMenuLoadTrees->addAction("Last &Autosave");

	actFile_LOADING_PICTURES_TOGGLE_SELECTION = fileSubMenuLoadPictures->addAction("&Toggle Selection");
    actFile_LOADING_PICTURES_TOGGLE_SELECTION->setShortcut (Qt::Key_F5);
    actFile_LOADING_PICTURES_TOGGLE_SELECTION->setCheckable (true);     
	
	actFile_LOADING_PICTURES_SELECT = fileSubMenuLoadPictures->addAction("&Select");
	actFile_LOADING_PICTURES_SELECT->setShortcut (Qt::Key_F6);
      
	actFile_LOADING_PICTURES_DESELECT = fileSubMenuLoadPictures->addAction("&Deselect");
	actFile_LOADING_PICTURES_DESELECT->setShortcut (Qt::Key_F7);

	actFile_LOADING_PICTURES_LOAD = fileSubMenuLoadPictures->addAction("&Load Selected");
	actFile_LOADING_PICTURES_LOAD->setShortcut (Qt::Key_F8);

	actFile_LOADING_PICTURES_UNLOAD = fileSubMenuLoadPictures->addAction("&Unload Selected");
	actFile_LOADING_PICTURES_UNLOAD->setShortcut (Qt::Key_F9);

	fileSubMenuLoadPictures->addSeparator();

	actFile_LOADING_PICTURES_CURTREE = fileSubMenuLoadPictures->addAction("&Load All for Current Tree");

	actFile_SAVING_TREES_SAVE = fileMenu->addAction("&Save Current Tree");
	actFile_SAVING_TREES_SAVE->setShortcut (Qt::Key_F10);

	actFile_SAVING_TREES_SAVEAS = fileMenu->addAction("Save Current Tree &as...");

	actFile_MARK_TREE_FINISHED = fileMenu->addAction("Mark Tree &Finished");
	actFile_MARK_TREE_FINISHED->setCheckable (true);

    actFile_DELETECOLONY = fileMenu->addAction("Delete Colony");

	actFile_CLOSE_WINDOW = fileMenu->addAction("&Close Current Tree");
	actFile_CLOSE_WINDOW->setShortcut (Qt::ALT + Qt::Key_F4);

	// Edit
	editMenu = addMenu("&Edit");

    actEdit_CELL_COMMENT = editMenu->addAction("Cell Comment...");

	// View
	viewMenu = addMenu("&View");

	viewSubMenuCellPath = viewMenu->addMenu("Cell &Paths");

	actView_TREE_OVERVIEW = viewMenu->addAction("Tree &Overview...");
    actView_TREE_OVERVIEW->setShortcut (Qt::Key_F11);
    
	actView_CUSTOMIZE = viewMenu->addAction("Custo&mize...");

	actView_CENTERCELL = viewMenu->addAction("Ce&nter Cell");

	actView_RESET_TREE_DISPLAY = viewMenu->addAction("Reset Tree Display");
	actView_RESET_TREE_DISPLAY->setShortcut (Qt::Key_F3);

	// Tracking
	trackingMenu = addMenu("&Tracking");

	trackingSubMenuInheritProperties = trackingMenu->addMenu("Inherit &Properties from Mother Cells"); 
	trackingSubMenuInheritWLs = trackingSubMenuInheritProperties->addMenu("Inherit &Wavelengths");
	for(unsigned int i = 0; i < actTracking_INHERIT_WLS.size(); ++i) {
		actTracking_INHERIT_WLS[i] = trackingSubMenuInheritWLs->addAction(QString("Wavelength &%1").arg(i+1));
		actTracking_INHERIT_WLS[i]->setCheckable(true);
	}

	actTracking_INHERIT_ADHERENCE = trackingSubMenuInheritProperties->addAction("Inherit &Adherence");
	actTracking_INHERIT_ADHERENCE->setCheckable(true);

	actTracking_INHERIT_DIFFERENTIATION = trackingSubMenuInheritProperties->addAction("Inherit &Differentiation");
	actTracking_INHERIT_DIFFERENTIATION->setCheckable(true);

	actTracking_INHERIT_ALL = trackingSubMenuInheritProperties->addAction("&Inherit All Properties");
	actTracking_INHERIT_ALL->setCheckable(true);

	actTracking_START = trackingMenu->addAction("&Start Tracking");
	actTracking_START->setShortcut (Qt::Key_F2);

	actTracking_DELETESTOPREASON = trackingMenu->addAction("Remove &Stop Reason");

	actTracking_DELETECELL = trackingMenu->addAction("&Delete Cell");

    actTracking_DELETE_TRACKPOINTS = trackingMenu->addAction("Delete &Trackpoints");

	
	// Export
	exportMenu = addMenu("Export");

	//exportSubMenuMovie = exportMenu->addMenu("Export &movie");

	exportSubMenuTree = exportMenu->addMenu("Export &Tree"); 

	exportSubMenuData = exportMenu->addMenu("Export &Data");

    actExport_TREE_CURRENT_VIEW = exportSubMenuTree->addAction("&Current View");

    actExport_TREE_COMPLETE_TREE = exportSubMenuTree->addAction("Complete &Tree");

    actExport_TREE_COMPLETE_TREE_UNSTRETCHED = exportSubMenuTree->addAction("Complete Tree, Unstretched");

    actExport_TREE_ALL_TREES_OF_POSITION = exportSubMenuTree->addAction("&All Trees of this Position");

    actExport_DATA_DIVISION_INTERVALS = exportSubMenuData->addAction("&Division Intervals");

    actExport_DATA_FIRST_DIVISION_TIME = exportSubMenuData->addAction("Division &Time");

    actExport_DATA_APOPTOSIS_TIME = exportSubMenuData->addAction("&Apoptosis Time");

    actExport_DATA_CELL_SPEED = exportSubMenuData->addAction("Cell &Speed");

    actExport_DATA_ALL_SELECTED_CELLS = exportSubMenuData->addAction("Selected &Cells");

    actExport_DATA_CELL_COUNT = exportSubMenuData->addAction("Cell C&ount");

    actExport_DATA_FLUORESCENCE = exportSubMenuData->addAction("&Fluorescence");

	actExport_CELLIMAGEPATCHES = exportMenu->addAction("Export &Cell Image Patches");

	// Tools
	toolsMenu = addMenu("T&ools");

	actTools_AUTOTRACKING = toolsMenu->addAction("&Autotracking");
	actTools_AUTOTRACKING->setShortcut(Qt::CTRL + Qt::Key_A);
	actTools_MOVIEEXPLORER = toolsMenu->addAction("Movie &Explorer");
	actTools_CONVERTOLDTREES = toolsMenu->addAction("&Convert Old Trees");

	toolsMenu->addSeparator();

	actTools_STATTTS = toolsMenu->addAction("&StaTTTs");
	actTools_AMT = toolsMenu->addAction("A&MT");

	
	// Help
	helpMenu = addMenu("&Help");

	actHelp_CHANGELOG = helpMenu->addAction("tTt Changelog (What's New)");

#ifdef DEBUGMODE 

	actHelp_TEST = helpMenu->addAction("Test");

#else

	actHelp_TEST = nullptr;

#endif
}

void TrackingMenuBar::connectActions()
{
    connect (actFile_OPEN_EXISTINGCOLONY, SIGNAL (triggered()), trackingWindow, SLOT (selectColony()));
    connect (actFile_OPEN_ADDNEWCOLONY, SIGNAL (triggered()), trackingWindow, SLOT (addColony()));
    connect (actFile_OPEN_LAST_AUTOSAVE, SIGNAL (triggered()), trackingWindow, SLOT (loadLastAutosavedColony()));
	connect (actFile_OPEN_NEXT_TREE, SIGNAL(triggered()), trackingWindow, SLOT (openNextTree()));
	connect (actFile_OPEN_PREV_TREE, SIGNAL(triggered()), trackingWindow, SLOT (openPreviousTree()));

    QSignalMapper *fileSaveTree_SM = new QSignalMapper (this);
            connect (actFile_SAVING_TREES_SAVE, SIGNAL (triggered()), fileSaveTree_SM, SLOT (map()));
            fileSaveTree_SM->setMapping (actFile_SAVING_TREES_SAVE, 0);
            connect (actFile_SAVING_TREES_SAVEAS, SIGNAL (triggered()), fileSaveTree_SM, SLOT (map()));
            fileSaveTree_SM->setMapping (actFile_SAVING_TREES_SAVEAS, 1);
    connect ( fileSaveTree_SM, SIGNAL (mapped (int)), trackingWindow, SLOT (saveCurrentFile (int)));

    connect (actFile_LOADING_PICTURES_TOGGLE_SELECTION, SIGNAL (triggered (bool)), trackingWindow, SLOT (selectRegion (bool)));

    QSignalMapper *fileLoadPicturesSelect_SM = new QSignalMapper (this);
            connect (actFile_LOADING_PICTURES_SELECT, SIGNAL (triggered()), fileLoadPicturesSelect_SM, SLOT (map()));
            fileLoadPicturesSelect_SM->setMapping (actFile_LOADING_PICTURES_SELECT, 0);
            connect (actFile_LOADING_PICTURES_DESELECT, SIGNAL (triggered()), fileLoadPicturesSelect_SM, SLOT (map()));
            fileLoadPicturesSelect_SM->setMapping (actFile_LOADING_PICTURES_DESELECT, 1);
    connect ( fileLoadPicturesSelect_SM, SIGNAL (mapped (int)), trackingWindow, SLOT (selectPictures (int)));

    QSignalMapper *fileLoadPicturesLoad_SM = new QSignalMapper (this);
            connect (actFile_LOADING_PICTURES_LOAD, SIGNAL (triggered()), fileLoadPicturesLoad_SM, SLOT (map()));
            fileLoadPicturesLoad_SM->setMapping (actFile_LOADING_PICTURES_LOAD, 0);
            connect (actFile_LOADING_PICTURES_UNLOAD, SIGNAL (triggered()), fileLoadPicturesLoad_SM, SLOT (map()));
            fileLoadPicturesLoad_SM->setMapping (actFile_LOADING_PICTURES_UNLOAD, 1);
    connect ( fileLoadPicturesLoad_SM, SIGNAL (mapped (int)), trackingWindow, SLOT (loadPictures (int)));

	connect ( actFile_LOADING_PICTURES_CURTREE, SIGNAL (triggered ()), trackingWindow, SLOT (loadAllImagesOfCurrentTree ()));

    connect ( actFile_MARK_TREE_FINISHED, SIGNAL (triggered (bool)), trackingWindow, SLOT (markTreeFinished (bool)));

    //connect ( actFile_DELETECOLONY, SIGNAL (triggered()), trackingWindow, SLOT (deleteColony()));

    connect ( actFile_CLOSE_WINDOW, SIGNAL (triggered()), trackingWindow, SLOT (close()));
    //connect (act, SIGNAL (triggered()), trackingWindow, SLOT ());


    //edit menu ---------------------------------------------------------------------------------------------
    connect ( actEdit_CELL_COMMENT, SIGNAL (triggered()), trackingWindow, SLOT (editCellComment()));

    //view menu ---------------------------------------------------------------------------------------------

    //connect ( actView_CUSTOMIZE, SIGNAL (triggered()), trackingWindow, SLOT (customizeTTT()));
	connect ( actView_CUSTOMIZE, SIGNAL (triggered()), &TTTManager::getInst(), SLOT (showCustomizeDialog()));
    connect ( actView_TREE_OVERVIEW, SIGNAL (triggered()), trackingWindow, SLOT (showTreeMap()));
    connect ( actView_CENTERCELL, SIGNAL (triggered()), trackingWindow, SLOT (centerCell()));
    connect ( actView_RESET_TREE_DISPLAY, SIGNAL (triggered()), trackingWindow, SLOT (resetGraphics()));

    //tracking menu ------------------------------------------------------------------------------------------
	
	for(unsigned int i = 0; i < actTracking_INHERIT_WLS.size(); ++i) {
		connect(actTracking_INHERIT_WLS[i], SIGNAL (triggered(bool)), trackingWindow, SLOT(toggleWavelengthInhertiance(bool)));
	}

    connect ( actTracking_START, SIGNAL (triggered()), trackingWindow, SLOT (startCellTracking ()));
    connect ( actTracking_DELETESTOPREASON, SIGNAL (triggered()), trackingWindow, SLOT (deleteStopReason()));
    connect ( actTracking_DELETECELL, SIGNAL (triggered()), trackingWindow, SLOT (deleteTrack()));
    connect ( actTracking_DELETE_TRACKPOINTS, SIGNAL (triggered()), trackingWindow, SLOT (deleteAllTrackPoints()));
	connect ( actTracking_INHERIT_ALL, SIGNAL (triggered(bool)), trackingWindow, SLOT (setInheritAllProperties(bool)));


    //export menu --------------------------------------------------------------------------------------------

    connect ( actExport_TREE_CURRENT_VIEW, SIGNAL (triggered()), trackingWindow, SLOT (exportTree()));
    connect ( actExport_TREE_COMPLETE_TREE, SIGNAL (triggered()), trackingWindow, SLOT (exportTree()));
    connect ( actExport_TREE_COMPLETE_TREE_UNSTRETCHED, SIGNAL (triggered()), trackingWindow, SLOT (exportTree()));
    connect ( actExport_TREE_ALL_TREES_OF_POSITION, SIGNAL (triggered()), trackingWindow, SLOT (exportAllTreeSnapshots()));

    connect ( actExport_DATA_DIVISION_INTERVALS, SIGNAL (triggered()), trackingWindow, SLOT (exportTreeData()));
    connect ( actExport_DATA_FIRST_DIVISION_TIME, SIGNAL (triggered()), trackingWindow, SLOT (exportTreeData()));
    connect ( actExport_DATA_APOPTOSIS_TIME, SIGNAL (triggered()), trackingWindow, SLOT (exportTreeData()));
    connect ( actExport_DATA_CELL_SPEED, SIGNAL (triggered()), trackingWindow, SLOT (exportTreeData()));
    connect ( actExport_DATA_ALL_SELECTED_CELLS, SIGNAL (triggered()), trackingWindow, SLOT (exportTreeData()));
    connect ( actExport_DATA_CELL_COUNT, SIGNAL (triggered()), trackingWindow, SLOT (exportTreeData()));
    connect ( actExport_DATA_FLUORESCENCE, SIGNAL (triggered()), trackingWindow, SLOT (exportFluorescenceCells()));
	connect ( actExport_CELLIMAGEPATCHES, SIGNAL (triggered()), TTTManager::getInst().frmPositionLayout, SLOT (exportCellImagePatches()));

	// tools menu --------------------------------------------------------------------------------------------
	connect ( actTools_MOVIEEXPLORER, SIGNAL (triggered()), trackingWindow, SLOT (openMovieExplorer()));
	connect ( actTools_CONVERTOLDTREES, SIGNAL (triggered()), trackingWindow, SLOT (convertOldTrees()));
	connect ( actTools_STATTTS, SIGNAL (triggered()), trackingWindow, SLOT (launchTTTStats()));
	connect ( actTools_AMT, SIGNAL (triggered()), trackingWindow, SLOT (launchAMT()));
	connect ( actTools_AUTOTRACKING, SIGNAL (triggered()), trackingWindow, SLOT (startAutoTrackingAssistant()));

	// help menu
	connect ( actHelp_CHANGELOG, SIGNAL (triggered()), trackingWindow, SLOT (displayTTTChangelog()));

//#ifdef DEBUGMODE 
	if(actHelp_TEST)
		connect ( actHelp_TEST, SIGNAL (triggered()), trackingWindow, SLOT (test()));

//#endif
}