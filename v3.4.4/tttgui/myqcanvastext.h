/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MYQCANVASTEXT_H
#define MYQCANVASTEXT_H

#include "tttdata/track.h"

#include <q3canvas.h>

#include <qstring.h>
#include <qpainter.h>

/**
	@author Bernhard
	
	Subclass of QCanvasText to provide rotated text and additional attributes.
	Additional attributes: text angle, Track association, usage, arbitrary int parameter
*/

class MyQCanvasText : public Q3CanvasText
{
public:
	/**
	 * constructs a usual QCanvasText object
	 * @param _canvas the canvas to which this object belongs
	 */
	MyQCanvasText (Q3Canvas* _canvas)
	: Q3CanvasText (_canvas), angle (0), usage (0), track (0), param (0)
		{setZ (1);}
	
	/**
	 * constructs a usual QCanvasText object
	 * @param _text the text to be displayed
	 * @param _canvas the canvas to which this object belongs
	 */
	MyQCanvasText (const QString& _text, Q3Canvas* _canvas)
	: Q3CanvasText (_text, _canvas), angle (0), usage (0), track (0), param (0)
		{setZ (1);}
	
	/**
	 * constructs a usual QCanvasText object
	 * @param _text the text to be displayed
	 * @param _angle the writing angle for this text (default = 0 => 3h on clock)
	 * @param _canvas the canvas to which this object belongs
	 */
	MyQCanvasText (const QString& _text, int _angle, Q3Canvas* _canvas)
	: Q3CanvasText (_text, _canvas), angle (_angle), usage (0), track (0), param (0)
		{setZ (1);}
	
	~MyQCanvasText()
		{}
	
	/**
	 * sets the writing angle for this text (default = 0)
	 * @param _angle the angle in degrees
	 */
	void setAngle (int _angle)
		{angle = _angle;}
	/**
	 * @return the writing angle of this text (default = 0 => 3h on clock)
	 */
	int getAngle()
		{return angle;}
	
	/**
	 * sets the usage for this object
	 * @param _usage the usage (cf. int constants defined in treedisplay.h)
	 */
	void setUsage (int _usage)
		{usage = _usage;}
	
	/**
	 * @return the usage for this object
	 */
	int getUsage() const
		 {return usage;}
	
	/**
	 * sets the associated Track object
	 * @param _track a pointer to an (existing) Track object
	 */
	void setTrack (Track *_track)
		{track = _track;}
	
	/**
	 * @return the Track object associated with this text object
	 */
	Track *getTrack() const
		{return track;}
	
	/**
	 * sets the arbitrary int parameter to the value
	 * @param _param the value for the parameter
	 */
	void setParam (int _param)
		{param = _param;}
	
	/**
	 * @return the arbitrary int parameter
	 */
	int getParam() const
		{return param;}
	
	
	///the RTTI value for this class (1007)
	static int RTTI;
	
	/**
	 * @return the RTTI value for this class
	 */
	int rtti() const
		{return RTTI;}
	
protected:
	
	/**
	 * draws the text with the stored angle
	 * @param _p the QPainter on which should be painted
	 */
	virtual void draw (QPainter& _p);
	
private:
		
	///the rotation angle (in degrees)
	int angle;
	
	///can be used for any association in the creating module
	///best way of using this attribute: define some constants that distinguish different line types and query them
	int usage;

	///the associated track (often very useful for detecting correspondencies)
	Track *track;
	
	///parameter, can be used for any purposes
	int param;
};

#endif
