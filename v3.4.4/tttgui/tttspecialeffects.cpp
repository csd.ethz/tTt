/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "tttspecialeffects.h"

// Project
#include "tttmovie.h"
#include "multiplepictureviewer.h"



TTTSpecialEffects::TTTSpecialEffects( TTTMovie* _parent )
	: QDialog(_parent)
{
	// Init gui
	ui.setupUi(this);

	// Init variables
	movieWindow = _parent;

	// Signals/slots
	connect(ui.pbtClose, SIGNAL(clicked()), this, SLOT(close()));
	connect(ui.pbtApply, SIGNAL(clicked()), this, SLOT(apply()));
}

void TTTSpecialEffects::apply()
{
	// Get multiple pic viewer
	MultiplePictureViewer* mpv = movieWindow->getMultiplePictureViewer();

	// Enable
	mpv->setShowPreviousTrackPoints(ui.chkDrawPrevTrackPoints->isChecked(), ui.spbNumPrevTPS->value(), ui.spbWidthOfTails->value(), ui.chkTransparency->isChecked(), ui.chkDrawPrevTrackPointsOnlySelectedCell->isChecked());
	mpv->setDrawSegmentation(ui.chkDrawSegmentation->isChecked());
}
