/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef tttmovieexportwizard_h__
#define tttmovieexportwizard_h__

#include "ui_frmMovieExport.h"

// Project
#include "tttio/movieexporter.h"

/**
*	@author Oliver Hilsenbeck
*	
*	Dialog for movie exports.
*/

class TTTMovie;

class TTTMovieExport : public QDialog, public Ui::frmMovieExport {

Q_OBJECT

public:
	/**
	 * Constructor
	 * @param _parent the corresponding movie window
	 * @param _minTimePoint first timepoint. Smaller starting timepoints will not be allowed
	 * @param _maxTimePoint like above, but last.
	 */
	TTTMovieExport(TTTMovie *_parent, int _minTimePoint, int _maxTimePoint);

	~TTTMovieExport();

private slots:
	// Start export
	void startExport();

	// Cancel clicked
	void cancelExport();

	// Display timebox position help
	void displayTimePositionHelp();

	// Display stack repeats help
	void displayStackRepeatsHelp();

	// First export timepoint changed
	void firstExportTimePointEdited(const QString& _text);

	// Set timepoint textfields to current timepoint
	void setTpToCurrent();

	// Display timebox toggled
	void displayTimeBoxToggled(bool _on);

	// AVI format selected
	void formatAviSelected(bool _on);

	// JPG format selected
	void formatJpegSelected(bool _on);

	// Center on trackpoints option toggled
	void centerOnTrackpointsToggled(bool _on);
	
	// Import settings (including gamma) from file
	void importSettings();

private:
///private methods

	// Load options form user settings
	void loadExportOptionsFromUserSettings();

	// Write options to user settings
	void writeExportOptionsToUserSettings();

	// Check if options entered by user are valid
	bool validateInput();

	/**
	 * Export current settings (including gamma) to file
	 * @param _folder directory to save settings in
	 */
	void exportSettings(const QString& _folder);

///private data
	// Corresponding movie form
	TTTMovie* movieWindow;

	// Movie exporter
	MovieExporter* exporter;

	// min / max possible timepoints
	int minTimePoint;
	int maxTimePoint;

	// Settings
	MovieExporter::FORMAT format;
	MovieExporter::MODE mode;

	// Settings file name
	static const char* SETTINGS_FILENAME;

	// Settings file version
	static const unsigned int SETTINGS_FILEVERSION = 2;
};

#endif // tttmovieexportwizard_h__