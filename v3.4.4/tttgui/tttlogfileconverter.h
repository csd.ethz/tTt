/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef tttlogfileconverter_h__
#define tttlogfileconverter_h__

#include "ui_frmLogFileConverter.h"

// QT
#include <QWidget>
#include <QDateTime>


/**
 * Logfileconverter for tTt
 * Extracts information from picture meta.xml files and saves it in a single binary log file
 */
class TTTLogFileConverter : public QWidget {

	Q_OBJECT

public:

	/**
	 * Constructor
	 * @param _parent parent widget
	 */
	TTTLogFileConverter(QWidget* _parent = 0);

protected:

	// Reimplemented
	void closeEvent(QCloseEvent *_event);

private slots:

	/**
	 * Start conversion
	 */
	void startConversion();

	/**
	 * Missing xml option changed
	 */
	void missingXmlOptionChanged();

	/**
	 * Force format string checked/unchecked.
	 */
	void forceFormatStringToggled(bool on);

private:

	/**
	 * Helper function of startConversion()
	 * @return true if successful
	 */
	bool runConversionInternal();

	/**
	 * Append output string to pteOutPut
	 * @param _what the string to append
	 */
	void out(const QString& _what);

	/**
	 * Check format string or find working format string for given xml string.
	 * @param _xml contents of complete image xml file as string
	 * @param imageFileName only given for debug output.
	 * @return true if format strings were not set or work properly for the xml string. Otherwise, false is returned and
	 * new format strings are set that work for the xml string and should be tested again on all xml files. If no working
	 * format can be found, unsupportedFormat is set to true and the format strings are not changed.
	 */
	bool checkFormatString(QString& dateFormatString, QString& dateRegExp, const QString& xml, bool& unsupportedFormat, QVector<QString>& testedFormats, QDateTime& dateOfLastTimePoint, const QString& imageFileName, int prevTimePoint, int curTimePoint);

	/**
	 * Check datetime with regard to last month and day.
	 * @return false if month of datetime is different from lastmonth and day of datetime is equal to lastday.
	 */
	bool checkDateTime(const QDateTime& dateTime, QDateTime& prevDateTime);

	/**
	 * Parse image meta xml file contents
	 * @param _xml contents of complete image xml file as string
	 * @param _creationTime in this variable the creation time will be saved
	 * @param useTatXmlTagNumber if date tag has to be located first (only for TAT XML)
	 * @return 0 if successful, or a specific error number that is greater than 0 if not successful (see implementation for more details)
	 */
	int parseXmlContents(const QString& _xml, QDateTime& _creationTime, const QString& dateRegExp, const QString& dateFormatString, bool useTatXmlTagNumber);

	// List of errors during conversion
	QStringList errors;

	// Conversion currently running
	bool conversionRunning;

	// Ui
	Ui::FrmLogFileConverter ui;

	/**
	 * Internally used structure for log file entries
	 */
	struct InternalLogFileEntry {
		// Constructor
		InternalLogFileEntry(int _tp, int _wl, int _zIndex, const QDateTime& _timeStamp) 
			: tp(_tp), wl(_wl), zIndex(_zIndex), timeStamp(_timeStamp)
		{}
		
		// Timepoint
		int tp;

		// Wavelength number
		int wl;

		// Z-Index, can be -1 if not available
		int zIndex;

		// Date and time of picture acquisition
		QDateTime timeStamp;
	};

};


#endif // tttlogfileconverter_h__
