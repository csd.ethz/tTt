/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef POSITIONTHUMBNAIL_H
#define POSITIONTHUMBNAIL_H

// Qt
#include <QGraphicsPixmapItem>
#include <QImage>

// Project
#include "tttbackend/tttpositionmanager.h"

// Forward declarations
class PositionDisplay;



/**
	@author Bernhard
	
	This class represents a thumbnail of an experiment position and is used in PositionDisplay to create a layout of all positions.
	
	Note: it is assumed that all resizing action is done from outside
*/

class PositionThumbnail : public QGraphicsPixmapItem
{
	
public:
	
	// Constructor
	PositionThumbnail (TTTPositionManager* _posManager, bool _drawFrame, PositionDisplay *_positionDisplay, float _zValue, int _timePoint, int _waveLength);
		
	/**
	* sets a mark in the picture frame if there are pictures loaded. Updates display
	* @param _loaded 
	*/
	void markPicturesLoaded (bool _loaded = true);
		
	/**
	 * Update display according to current tv-factor, ocular factor and other display relevant settings
	 * @param _width thumbnail width in pixels (according to scene coordinate system)
	 * @param _height thumbnail height in pixels
	 */
	void updateDisplay(int _width, int _height);

	/**
	 * Overloaded function. Works only if thumbnail size has been set correctly (which is the case
	 * after the actual updateDisplay function has been called)
	 */
	void updateDisplay();

	/**
	 * displays this position as not available (draw a red cross on the picture)
	 * useful, if for example the log file is not available. Does NOT update display automatically
	 */
	void lock();

	/**
	 * Sets this position to be the (in)active one (displayed with a green frame). Does NOT update display automatically!
	 * @param _active whether this position is the active one (default)
	 */
	void setSelected (bool _active = true);

	/**
	 * @return if this position thumbnail is in selected state
	 */
	bool isSelected() const {
		return selected;
	}

	/**
	 * @return the index of the position associated with this thumbnail
	 */
	QString getIndex() const {
		return posManager->positionInformation.getIndex();
	}

	/**
	 * @return position manager for the position that is associated with this thumbnail
	 */
	TTTPositionManager* getPosManager() const {
		return posManager;
	}

	/**
	 * @return whether the first image was found
	 */
	bool getFoundFirstImage() const {
		return foundFirstImage;
	}

	/**
	 * Set the first image. Does NOT update display automatically
	 * @param _image the new first image to be used
	 */
	void setFirstImage(const QPixmap& _image);

	// Z-values are increased for every image (so that positions overlap each other in a defined way) by Z_IMAGE_DELTA
	static const float Z_IMAGE_DELTA;

	// Frame will have corresponding image z-value + Z_FRAME_DELTA_TO_IMAGE
	static const float Z_FRAME_DELTA_TO_IMAGE;

	// Text will have corresponding image z-value + Z_TEXT_DELTA_TO_IMAGE
	static const float Z_TEXT_DELTA_TO_IMAGE;

	// Frame width in pixels (at 100% zoom)
	static const int FRAME_WIDTH = 25;

	// Color for active and inactive thumbnails (i.e. for text and frame)
	static const QColor colorSelected;
	static const QColor colorNotSelected;

	// Font point size
	static const int FONT_POINT_SIZE = 120;

protected:

	// Mouse pressed (needs to be reimplemented in order to be able to receive mouse release and mouse double click events!)
	void mousePressEvent(QGraphicsSceneMouseEvent *_event);

	// Mouse button released event
	void mouseReleaseEvent(QGraphicsSceneMouseEvent* _event);

	// Double click event
	void mouseDoubleClickEvent(QGraphicsSceneMouseEvent* _event);

private:

	// Corresponding position manager
	TTTPositionManager* posManager;

	// First picture (white picture if first picture is not available)
	QPixmap firstImage;
	bool foundFirstImage;

	// Desired picture or placeholder size in pixels (=scene coordinates) or 0 if not set
	int thumbNailSizeX, thumbNailSizeY;

	// The PositionDisplay instance this thumbnail belongs to
	PositionDisplay *positionDisplay;

	///whether there are pictures loaded currently
	bool picturesLoaded;

	///whether this position is selected
	bool selected;

	// Thumbnail frame (objects are owned by scene)
	QList<QGraphicsRectItem*> frame;

	// Text in the middle (object is owned by scene)
	QGraphicsTextItem *midText;

	// Lines for cross of locked positions
	QGraphicsLineItem *line1;
	QGraphicsLineItem *line2;

	///whether this position is locked
	bool locked;
	


	//void paintEvent (QPaintEvent *);
	//void resizeEvent (QResizeEvent *);
	//void mouseReleaseEvent (QMouseEvent *);
	//void mouseDoubleClickEvent (QMouseEvent *);
	//void mouseMoveEvent (QMouseEvent *);
	
//	/**
//	 * draws the thumbnail
//	 */
//	void draw();
//	
//	/**
//	 * @return the currently set image (0, if the picture could not be loaded)
//	 */
//	QImage* getOriginalImage();
//	
//	/**
//	 * sets the image (as QImage pointer) of the first picture (if the one in the constructor was invalid)
//	 * @param _image the pointer to a QImage instance that contains the first picture
//	 */
//	void setPicture (const QImage* _image);
//	


//	
//	/**
//	 * @return whether THIS position is currently the active one
//	 */
//	bool isActive() const {
//		return active;
//	}
//	

//	
//	/**
//	 * sets whether a frame around each position thumbnail should be drawn or not
//	 * does not trigger a redraw!
//	 * @param _draw true => frame is drawn; false => frame is not drawn
//	 */
//	void setDrawFrame (bool _draw)
//		{drawFrame = _draw;}
//	
//	
//signals:
//	
//	//self-explaining
//	void mouseLeftClick (const QString &, PositionThumbnail *);
//	
//	void mouseRightClick (const QString &, PositionThumbnail *);
//	
//	void mouseDoubleClick (const QString &, PositionThumbnail *);
//	
//	void mouseMove (const QString &, int, int, PositionThumbnail *);
//	
//private:
//	
//	///the index of the position
//	QString index;
//	
//	///the filename of the first picture (usually wavelength 0) in the position folder
//	QString picFilename;
//	
//	///the first image (read from picFilename)
//	QImage imgOriginal;
//	
//	///the first image, but scaled to the current widget size (for faster drawing)
//	QPixmap pixScaled;
//	
//	///whether the picture could be loaded
//	bool picAvailable;
//	

//	
//	///whether there are pictures loaded currently
//	bool picturesLoaded;
//	
//	///whether the border frame should be drawn
//	bool drawFrame;
};

#endif
