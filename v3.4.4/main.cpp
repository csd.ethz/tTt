/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
///**********************************************************************************************
/// COMPILE tTt WITH "QT3_SUPPORT" SYMBOL DEFINED
/// AND WITH "DEBUGMODE" FOR DEBUG BUILDS, BUT WITHOUT "UNICODE" or "_UNICODE"
///
/// Add "libjpeg-turbo\lib\debug\jpeg-staticd.lib" for debug and
/// "libjpeg-turbo\lib\release\jpeg-static.lib" for release to the linker.
/// Also add libpng libraries.
///**********************************************************************************************

//#ifdef _DEBUG
//#include "vld.h"
//#endif

// Project includes
#include "tttgui/tttmainwindow.h"
#include "tttgui/tttsplashscreen.h"
#include "tttgui/tttpositionlayout.h"
#include "tttdata/userinfo.h"
#include "tttbackend/tttmanager.h"
#include "tttio/logging.h"
#include "tttio/xmlhandler.h"
#include "tttbackend/tools.h"

// Qt includes
#include <QDateTime>
#include <QMessageBox>

// STL includes
#include <stdexcept>


int main(int argc, char **argv)
{
	QApplication app (argc, argv);

	// Check if "TTTLauncher.exe" exists and if so, check if it was used
	if(QFile::exists("./TTTLauncher.exe")) {
		bool commandLineArgumentFound = false;
		for(int i = 0; i < argc; ++i) {
			QString curArg(argv[i]);
			if(curArg == "-tttlauncher") {
				commandLineArgumentFound = true;
				break;
			}
		}
		if(!commandLineArgumentFound) {
			QMessageBox::information(0, "Error", "You must use TTTLauncher.exe to start this application.");
			return 0;
		}
	}




	// Init logging module
	bool logInitOk = Logging::init();

	// If we are in debugmode, just use default message handling (in windows: output is sent to debugger)
#ifndef DEBUGMODE	

	//if(logInitOk) {
	//	// Install message handler
	//	qInstallMsgHandler(Logging::tttMessageHandler);
	//}

#endif
	
	TTTSplashScreen *splasher = 0;
	TTTMainWindow *mainWin = 0;
	

		// Not good: makes QDesktopServices::storageLocation() return other path than before..
		//// Init these so we can use QSettings for locally stored settings everywhere conveniently
		//QCoreApplication::setOrganizationName("tTt");
		//QCoreApplication::setApplicationName("tTt");

		//create (unique) TTTManager instance, thus it is available everywhere
		mainWin = TTTManager::getInst().frmMainWindow;
		
		//splasher is shown modal for some seconds (see const int ... in tttsplashscreen.h)
		splasher = new TTTSplashScreen();
		splasher->exec();

        //read general configuration file
		QString configTTTPath;
		QString userAppData = SystemInfo::getUserAppDataPath();
		if(!userAppData.isEmpty())
			configTTTPath = userAppData + "ttt_config.xml";
		else {
			// Try app path
            QString app_path = QCoreApplication::applicationFilePath();
			app_path = app_path.left (app_path.findRev ("/") + 1);
			configTTTPath = app_path + "ttt_config.xml";
		}

        QDomDocument configDOM ("ttt_config");
		if(!XMLHandler::readXML2DOM (configTTTPath, configDOM)) {
			// ------- Oliver ------------
			// Create new config file
			QDomElement root = configDOM.createElement("NASPath");
			QDomAttr val = configDOM.createAttribute("Value");
			root.setAttribute("Value", SystemInfo::defaultNAS());

			configDOM.appendChild(root);
		}

        TTTManager::getInst().setTTTConfigDocument (configDOM);
        TTTManager::getInst().setTTTConfigPath (configTTTPath);

        mainWin->loadNASDrive();
		
		//get user sign
		bool cancelled = false;
		UserInfo::getInst().getUsersign (&cancelled, SystemInfo::getUserAppDataPath());
		//std::cout << UserInfo::getInst().getUsersign() << std::endl;
		
		if (cancelled) {
			//quit program
			QMessageBox::information (0, "tTt", "No username selected.\nTTT will now close.", QMessageBox::Ok);
			
			delete mainWin;
			delete splasher;
			
			//exit (1);
			return false;
		}

		qDebug() << "tTt: Starting application. User: " << UserInfo::getInst().getUsersign() << " Version: " << tttVersionLong;


                //load the default style sheet
		UserInfo::getInst().readUserStyleSheet();
		

		//set main window (when this is closed, the application quits)
                //BS: no longer used in qt4
                //app.setMainWidget (TTTManager::getInst().frmMainWindow); // -------- Konstantin: frmMainWindow instead of frmPositionLayout. In TTTTracking::closeEvent and in TTTStatistics::closeEvent frmMainWindow->close()
		
		mainWin->show();

		// Check if experiment directory has been passed as command line argument
		if(argc > 1) {
			for(int i = 1; i < argc; ++i) {
				QString expDirectory = argv[i];
				if(expDirectory.size() && expDirectory[0] != '-') {		// Ignore all arguments beginning with '-'
					// Make sure path does not end with '/'
					QString clPath = QDir::fromNativeSeparators(expDirectory);
					if(!clPath.isEmpty()) {
						if(clPath[clPath.length()-1] == '/')
							clPath = clPath.left(clPath.length()-1);
						if(QDir(clPath).exists()) {
							// Load specified experiment
							mainWin->loadExperiment(clPath);
							break;
						}
					}
				}
			}
		}
		
		
		// ----------- Oliver -------------
		// Manually cleanup
		int retCode = app.exec();

		// Cleanup (todo: maybe do this differently, cp. void QCoreApplication::aboutToQuit () descriptin in qt manual)
		//delete mainWin;
		delete splasher;
		
		TTTManager::getInst().destroy();

        return retCode;
/*
OH 140707: removed to find crash location in Release build with symbols
#ifndef DEBUGMODE	
        }
//	catch (exception& exc) {
//		//QMessageBox::critical (0, "tTt", "An error occurred (Code 0):\n" + exc.what() + "\nPlease contact timm.schroeder@gsf.de about this.", "Quit");
//		QMessageBox::critical (0, "tTt", "An error occurred (Code 0):\n\nPlease contact timm.schroeder@helmholtz-muenchen.de about this.", "Quit");
//	}
	catch (...) {
		QMessageBox::critical (0, "tTt", QString("A severe error has occurred and tTt needs to shut down.\n\nPlease report this error as soon as possible providing this information:\nError code = %1").arg(g_debugInformation), "Quit");
	}
#endif
        //some error occurred...
        return 1;
*/
}

