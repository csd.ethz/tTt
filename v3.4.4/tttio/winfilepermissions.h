/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
// Exclude rarely used stuff from windows.h
#define WIN32_LEAN_AND_MEAN

#include <Windows.h>

/**
 * @author Oliver Hilsenbeck
 *
 * Taken from setup dll
 *
 * Unfortunately, QFile:setPermissions() did not work on windows 7.
 * Therefore we have to use this ugly windows specific function.
 */

//
// This function will grant the specified access rights for the specified file to everyone
//
// The code has been taken from an example from msdn and modified to grant access to
// everyone instead of a specified user.
// This code is very complicated, finding a better solution would be very good.
//
BOOL AddAccessRights(const TCHAR *lpszFileName, DWORD dwAccessMask);