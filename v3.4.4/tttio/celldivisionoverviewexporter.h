/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef celldivisionoverviewexporter_h__
#define celldivisionoverviewexporter_h__

#include <QString>
#include <QVector>

// Forward declarations
class TTTMovie;
class MultiplePictureViewer;
class QPixmap;
class TTTException;
class Track;
class TTTPositionManager;
class QPainter;

/**
 * @author Oliver Hilsenbeck
 * 
 * ...
 */
class CellDivisionOverviewExporter {
public:
	// Output format
	enum FORMAT {
		JPEG,
		PNG,
		BMP
	};

	/**
	 * Constructor.
	 */
	CellDivisionOverviewExporter(TTTMovie* _movieWindow, 
								Track* _motherTrack, 
								int _timePointsBefore,
								int _timePointsAfter,
								int _interval,
								int _picSizeX,
								int _picSizeY,
								QString _exportDirectory,
								QVector<int> _waveLengths,
								bool _hideTrackPoints,
								FORMAT _format) throw(TTTException);

	/**
	 * perform export
	 */
	void runExport() throw(TTTException);

	~CellDivisionOverviewExporter();

private:

	// Init export
	void initExport() throw(TTTException);

	// Init picture
	void initPicture() throw(TTTException);

	// Dots at ends of tracklines
	void drawDots( QPainter& p, int _posX, int _posY );

	// Draw lines for tracks
	void drawTreeLines();

	// Draw info box
	void drawInfoBox();

	// Export main loop
	void performExport() throw(TTTException);

	// Get y-position for picture
	// _wlCounter: not wavelength, but number of this wavelength 
	// _type: 0 = mother, 1 = child1, 2 = child2
	int getPicPosY(int _wlCounter, int _type);

	// Add image to picture
	void addImage(Track* _track, int _posX, int _posY);

	// Finish export (cleanup)
	void finishEport();

	// Display summary message
	void displayFinishMessage();

	// Movie window and related objects
	TTTMovie* movieWindow;
	MultiplePictureViewer* multiPicViewer;
	TTTPositionManager* posManager;

	// Mother track of division and childs
	Track* motherTrack; 
	Track* childTrack1;
	Track* childTrack2;

	// Timepoint of division
	int divTimepoint;

	// Parameters
	int timePointsBefore;
	int timePointsAfter;
	int interval;
	int picSizeX;
	int picSizeY;
	QString exportDirectory;
	QVector<int> waveLengths;
	bool hideTrackpoints;

	// QPixmap object used for rendering
	QPixmap* targetPixmap;
	int fullSizeX, fullSizeY;

	// y-coordinates of child1 and child2 pics
	int yChild1;
	int yChild2;

	// x-coordinate where childs start
	int childsX;

	// Exported timepoints
	QString exportedTPs;

	// Complete filename with path
	QString filename;

	// Extension
	QString fileExtension;

///Constants

	// Horizontal space between pics in px
	static const int spaceBetweenPicsX = 20;

	// Vertical space between pics in px
	static const int spaceBetweenPicsY = 5;

	// Vertical space betwenn child branches in pixels
	static const int verticalSpaceBetweenChilds = 100; 

	// Width of lines under pictures
	static const int treeBarWidth = 10;

	// Vertical space between lines of text
	static const int textVerticalSpace = 15;

	// Font size
	static const int fontSize = 14;

	// Space left/right of tree
	static const int spaceLeft = 20;
	static const int spaceRight = 200;	// Space after lines excluding dots

	// info box width
	static const int infoBoxWidth = 100;
	static const int infoBoxHeight = 80;

	// Number of dots at ends of child tracklines
	static const int numDots = 3;
};





#endif // celldivisionoverviewexporter_h__