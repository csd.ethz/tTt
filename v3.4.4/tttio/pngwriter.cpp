/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "pngwriter.h"

// LibPNG
#include "png.h"


// Actual internal saving function
int saveImageAsPng( unsigned char* imageData, int width, int height, LPCSTR fileName, int bitDepth, bool littleEndian );


int save16BitImageAsPng( unsigned char* imageData, int width, int height, LPCSTR fileName, bool littleEndian )
{
	// Delegate
	return saveImageAsPng(imageData, width, height, fileName, 16, littleEndian);
}

int save8BitImageAsPng( unsigned char* imageData, int width, int height, LPCSTR fileName )
{
	// Delegate
	return saveImageAsPng(imageData, width, height, fileName, 8, false);
}

int saveImageAsPng( unsigned char* imageData, int width, int height, LPCSTR fileName, int bitDepth, bool littleEndian )
{
	// Check parameters
	if(!imageData || width < 0 || height < 0 || !fileName || (bitDepth != 8 && bitDepth != 16))
		return 8;

	// PNG settings
	png_byte bit_depth = static_cast<png_byte>(bitDepth);
	png_byte color_type = PNG_COLOR_TYPE_GRAY;

	// Init variables
	int errorCode = 0;
	FILE* fp = 0;
	png_structp png_ptr = 0;
	png_infop info_ptr = 0;
	png_bytep* row_pointers = 0;

	// Init row_pointers
	row_pointers = static_cast<png_bytep*>(malloc(sizeof(png_bytep) * height));
	for(int y = 0; y < height; ++y)
		row_pointers[y] = static_cast<png_byte*>(imageData + width*y*(bitDepth/8));

	// Open file
	errno_t err;
	if( (err = fopen_s(&fp, fileName, "wb")) != 0 ) {
		errorCode = 1;
		goto Cleanup;
	}

	// Initialize libPNG
	png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if(!png_ptr) {
		errorCode = 2;
		goto Cleanup;
	}
	info_ptr = png_create_info_struct(png_ptr);
	if (!info_ptr) {
		errorCode = 3;
		goto Cleanup;
	}
	if (setjmp(png_jmpbuf(png_ptr))) {
		errorCode = 4;
		goto Cleanup;
	}
	png_init_io(png_ptr, fp);

	// Write header
	if (setjmp(png_jmpbuf(png_ptr))) {
		errorCode = 5;
		goto Cleanup;
	}
	png_set_IHDR(png_ptr, info_ptr, width, height,
		bit_depth, color_type, PNG_INTERLACE_NONE,
		PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
	png_write_info(png_ptr, info_ptr);

	// Write bytes
	if(littleEndian && bit_depth == 16)
		png_set_swap(png_ptr);
	if (setjmp(png_jmpbuf(png_ptr))) {
		errorCode = 6;
		goto Cleanup;
	}
	png_write_image(png_ptr, row_pointers);

	// End write
	if (setjmp(png_jmpbuf(png_ptr))) {
		errorCode = 7;
		goto Cleanup;
	}
	png_write_end(png_ptr, NULL);

Cleanup:

	// Free png memory, row pointers memory and close file
	png_destroy_write_struct(&png_ptr, &info_ptr);
	if(row_pointers) 
		free(row_pointers);
	if(fp != 0)
		fclose(fp);

	return errorCode;
}

int openPngGrayScaleImage(LPCSTR fileName, unsigned char** imageData, int& bitDepth, unsigned int& width, unsigned int& height, bool littleEndian)
{
	// Check parameters
	if(!imageData || !fileName)
		return 8;

	// Init variables
	int errorCode = 0;
	FILE* fp = 0;
	png_structp png_ptr = 0;
	png_infop info_ptr = 0;
	png_bytep* row_pointers = 0;

	// Open file
	errno_t err;
	if( (err = fopen_s(&fp, fileName, "rb")) != 0 ) {
		errorCode = 1;
		goto Cleanup;
	}

	// Check signature
	unsigned char sig[8];
	fread(sig, 1, 8, fp);
	if (!png_check_sig(sig, 8)) {
		errorCode = 2;
		goto Cleanup;
	}

	// Initialize libPNG
	png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if(!png_ptr) {
		errorCode = 3;
		goto Cleanup;
	}
	info_ptr = png_create_info_struct(png_ptr);
	if (!info_ptr) {
		errorCode = 4;
		goto Cleanup;
	}
	if (setjmp(png_jmpbuf(png_ptr))) {
		errorCode = 5;
		goto Cleanup;
	}
	png_init_io(png_ptr, fp);
	if (setjmp(png_jmpbuf(png_ptr))) {
		errorCode = 6;
		goto Cleanup;
	}

	// Start reading
	png_set_sig_bytes(png_ptr, 8);
	png_read_info(png_ptr, info_ptr);

	// Read info
	int color_type;
	png_get_IHDR(png_ptr, info_ptr, &width, &height, &bitDepth, &color_type, NULL, NULL, NULL);

	// Check if image is grayscale
	if(color_type != PNG_COLOR_TYPE_GRAY){
		errorCode = 7;
		goto Cleanup;
	}

	// Check if bitdepth is supported and calc bytes per pixel
	int bytesPerPixel;
	if(bitDepth == 1 || bitDepth == 8)
		bytesPerPixel = 1;
	else if(bitDepth == 16)
		bytesPerPixel = 2;
	else {
		errorCode = 8;
		goto Cleanup;
	}

	// Create output image and init row pointers
	*imageData = new unsigned char[width * height * bytesPerPixel];
	row_pointers = static_cast<png_bytep*>(malloc(sizeof(png_bytep) * height));
	for(int y = 0; y < height; ++y)
		row_pointers[y] = static_cast<png_byte*>(*imageData + width*y*bytesPerPixel);

	// Get background color and fill image
	if (png_get_valid(png_ptr, info_ptr, PNG_INFO_bKGD)) {
		png_color_16p pBackground;
		png_get_bKGD(png_ptr, info_ptr, &pBackground);
		if(bitDepth == 1) {
			// Fill image with 0 or 255
			for(unsigned char* itImageData = *imageData; itImageData != *imageData + width*height; ++itImageData)
				*itImageData = pBackground->gray? 255 : 0;
		}
		else if(bitDepth == 8) {
			// Convert to 8 bit
			for(unsigned char* itImageData = *imageData; itImageData != *imageData + width*height; ++itImageData)
				*itImageData = pBackground->gray >> 8;
		}
		else if(bitDepth == 16) {
			// Use as is
			for(unsigned short* itImageData = reinterpret_cast<unsigned short*>(*imageData); itImageData != reinterpret_cast<unsigned short*>(*imageData) + width*height; ++itImageData)
				*itImageData = pBackground->gray;
		}
	}

	// Inform libPNG to expand 1bit grayscale values to 8 bit
	if(bitDepth == 1)
		png_set_expand(png_ptr);

	// Expand any tRNS chunk data into a full alpha channel
	if (png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS))
		png_set_tRNS_to_alpha(png_ptr);	

	// Gamma correction
	//double  gamma;
	//if (png_get_gAMA(png_ptr, info_ptr, &gamma))
	//	png_set_gamma(png_ptr, display_exponent, gamma);

	// Actual read
	if(littleEndian)
		png_set_swap(png_ptr);
	png_read_image(png_ptr, row_pointers);

Cleanup:

	// Free png memory, row pointers memory and close file
	png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
	if(row_pointers) 
		free(row_pointers);
	if(fp != 0)
		fclose(fp);

	return errorCode;
}
