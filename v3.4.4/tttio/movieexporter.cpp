/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
// tTt
#include "movieexporter.h"
#include "tttgui/multiplepictureviewer.h"
#include "tttgui/ttttracking.h"
#include "tttbackend/tttpositionmanager.h"
#include "tttbackend/tttmanager.h"
#include "tttbackend/tttexception.h"
#include "tttgui/pictureview.h"
#include "tttdata/userinfo.h"
#include "tttgui/treedisplay.h"
#include "tttbackend/tools.h"

// STL
#include <limits>
#include <sstream>
#include <exception>

// QT
#include <QMessageBox>
#include <QProcess>
#include <QDesktopWidget>



MovieExporter::MovieExporter(TTTMovie* _movieWindow, 
							 int _firstTP, 
							 int _lastTP, 
							 FORMAT _format, 
							 int _jpegQuality,
							 MODE _exportMode,
							 bool _autoSizeAndPosition,
							 bool _drawTimebox,
							 bool _drawTimeboxFontBold,
							 int _drawTimeboxFontSize,
							 int _drawTimeboxRelativeToTp,
							 bool _drawWavelength,
							 QString _repeats,
							 QSize _centerModePicSize,
							 int _fps,
							 int _avibitrate)
{
	movieWindow = _movieWindow;
	if(!movieWindow)
		throw TTTException("Internal error (_movieWindow = 0)");

	// Get positionmanager
	positionManager = _movieWindow->getPositionManager();
	if(!positionManager)
		throw TTTException("Internal error (positionManager = 0)");

	// Get MultiplePictureViewer
	multiPicViewer = movieWindow->getMultiplePictureViewer();
	if(!multiPicViewer)
		throw TTTException("Internal error (multiPicViewer = 0)");

	// Get tracking window
	frmTracking = TTTManager::getInst().frmTracking;
	if(!frmTracking) 
		throw TTTException("Internal error (frmTracking = 0)");

	// Check position manager
	if (! positionManager->isInitialized())
		throw TTTException("Internal error (positionManager not initialized)");

	firstTimepoint = _firstTP;
	lastTimepoint = _lastTP;
	format = _format;
	exportMode = _exportMode;
	autoSizeAndPosition = _autoSizeAndPosition;
	drawTimebox = _drawTimebox;
	drawTimeboxFontBold = _drawTimeboxFontBold;
	drawTimeboxFontSize = _drawTimeboxFontSize;
	drawTimeboxRelativeToTp = _drawTimeboxRelativeToTp;
	drawWavelength = _drawWavelength;
	stackRepeatString = _repeats;
	centerModePicSize = _centerModePicSize;
	fps = _fps;
	pictureQuality = _jpegQuality;
	avibitrate = _avibitrate;

	picCount = 0;
	cancelExport = false;
	controlsHaveBeenHidden = false;

	// Detect export direction
	directionForward = _lastTP >= _firstTP;

	// Set file extension
	switch(format) {
		case JPEG:
			fileExtension = ".jpg";
			break;
		case PNG:
			fileExtension = ".png";
			break;
		case AVI:
			fileExtension = ".png";
			break;
		default:
			fileExtension = ".bmp";
	}

	// Check video settings
	if(format == AVI) {
		if(fps <= 0)
			throw TTTException("Invalid value for frames per second!");
	}

	// Check jpeg settings
	if(format == JPEG || format == AVI) {
		if(pictureQuality < 0 || pictureQuality > 100)
			throw TTTException("Error: quality for jpg must be between 0 and 100!");
	}
	else
		// -1 indicated to use default value
		pictureQuality = -1;

	// Remember current external tracks mode
	oldExternalTracksMode = movieWindow->getExternalTracksMode();
}


void MovieExporter::runExport()
{

	try {
		// Initialization
		initExport();

		// Do export
		performExport();

		// Convert image files to video, if video format is set
		if(format == AVI)
			convertImagesToVideo();
	}
	catch(const TTTException& err) {
		// Cleanup anyways
		finishEport();

		// Let caller decide what to do
		throw;
	}

	// Cleanup and show message
	finishEport();
	displayFinishMessage();
}

void MovieExporter::initExport()
{	
	if(positionManager->getLastTimePoint() == -1 || 
			positionManager->getFirstTimePoint() == -1)
		throw TTTException("Invalid timepoints for this position (no pictures loaded?)");

	//Create destination folder
	movieFolder = getExportDirectory();
	if(!SystemInfo::checkNcreateDirectory (movieFolder, true, true)) {
		QString msg = QString("Could not create destination folder (%1)").arg(movieFolder);
		throw TTTException(msg);
	}

	// Parse repeats
	parseRepeatsString();

	if (exportMode == COMPLETE_ALL)
		//show all tracks at the current timepoint
		movieWindow->setExternalTracksMode(TTTMovie::ALL_ALL_POS);
	else if (exportMode == COMPLETE_FIRST)
		//show first tracks from all colonies
		movieWindow->setExternalTracksMode(TTTMovie::FIRST_ALL_POS);

	// Hide controls to allow small window sizes
	if(autoSizeAndPosition || exportMode == CENTER_ON_TRACKPOINTS) {
		if(!movieWindow->getControlsHidden()) {
			movieWindow->hideShowControls();
			controlsHaveBeenHidden = true;
		}
	}

	// If processEvents() is not called now, we will not be able to reduce window size below the
	// size needed by the controls, although they are hidden
	qApp->processEvents();

	// Automatic size adjust
	if(autoSizeAndPosition) {
		adjustSizeAndPosition();
	}
	else {
		// just set timepoint to export start picture
		TTTManager::getInst().setTimepoint (firstTimepoint);
	}

	// Hide scrollbars and out of sync and position buttons 
	multiPicViewer->showHideScrollbars(true);
	multiPicViewer->setHideOutOfSyncBox(true);
	multiPicViewer->showPositionButtons(false);

	// Hide orientation box
	if(multiPicViewer->getOrientationBoxVisible()) {
		multiPicViewer->setOrientationBoxVisible(false);
		orientationBoxWasVisible = true;
	}
	else
		orientationBoxWasVisible = false;

	// Set wavelength visibility
	wavelengthBoxWasVisible = movieWindow->getWavelengthBoxVisible();
	multiPicViewer->setShowWavelengthBox (drawWavelength, 0, MAX_WAVE_LENGTH + 1);

	// Hide tracks if desired
	if(exportMode == HIDE_TRACKS)
		movieWindow->setShowTracks(false, true);

	// Set desired window size for CENTER_ON_TRACKPOINTS
	if(exportMode == CENTER_ON_TRACKPOINTS)
		movieWindow->setPictureSize(centerModePicSize.width(), centerModePicSize.height());

	// Must disable PictureView highlighting at last (because e.g. setPictureSize can change it again)
	QApplication::processEvents();
	multiPicViewer->setMarkActivePictureView(false);
}


void MovieExporter::adjustSizeAndPosition()
{
	// Get min/max position values
	float minX, maxX, minY, maxY, maxCellRadius;
	getMinAndMaxCoordsOfSelectedCells(minX, minY, maxX, maxY, maxCellRadius);

	// Calc center position (needed for setPictureDisplay())
	QPointF centerPos((minX + maxX) / 2, (minY + maxY) / 2);

	// Set PictureView to only draw selected tracks
	PictureView::setDrawOnlySelectedTracks(true);

	// Get size of space between fraPictures and actual borders of this window
	QSize sizeFraPictures = movieWindow->getPictureDisplaySize();
	QSize windowSize = movieWindow->size();

	// Get window size in pixels
	QPointF pointMin = positionManager->getDisplays().at (0).calcTransformedCoords (QPointF(minX, minY), &positionManager->positionInformation, false);
	QPointF pointMax = positionManager->getDisplays().at (0).calcTransformedCoords (QPointF(maxX, maxY), &positionManager->positionInformation, false);

	QPointF optimalSize = pointMax - pointMin;
	optimalSize.setX(qAbs(optimalSize.x()));
	optimalSize.setY(qAbs(optimalSize.y()));

	//minimum distance between trackpoints and frame margins in pixels
	const int movieExportFrameSpacing = 30;

	// Convert to int, add cell-radius and spacer
	int sizeX = optimalSize.x() + maxCellRadius + movieExportFrameSpacing;
	int sizeY = optimalSize.y() + maxCellRadius + movieExportFrameSpacing;;

	// Check if multiple wavelengths are displayed
	if(multiPicViewer->isInMultipleView() && !multiPicViewer->isInOverlayMode()) {
		// Get number of displayed wavelengths per row
		int maxPicsPerRow = 1;
		UserInfo::getInst().getStyleSheet().getMapping (maxPicsPerRow);

		// Calculate how many pictures are actually displayed per row and column
		int factorX, factorY;
		factorX = std::min(multiPicViewer->getWaveLengthsSelected(), (unsigned int)maxPicsPerRow);
		factorY = multiPicViewer->getWaveLengthsSelected() / maxPicsPerRow;
		if(multiPicViewer->getWaveLengthsSelected() % maxPicsPerRow > 0)
			factorY += 1;

		sizeX *= factorX;
		sizeY *= factorY;
	}

	// Additional size needed by the window and maybe timebox and WLbox
	QPointF delta(windowSize.width() - sizeFraPictures.width(), windowSize.height() - sizeFraPictures.height());

	if(drawWavelength)
		delta += QPointF(0, 25);

	if(drawTimebox)
		delta += QPointF(0, 25);

	// Get max window size
	float maxSizeX = 0, maxSizeY = 0;

	QDesktopWidget* desktop = QApplication::desktop();
	if(desktop) {
		// Get available geometry of primary screen
		const QRect& available = desktop->availableGeometry();

		maxSizeX = available.width() - available.left();
		maxSizeY = available.height() - available.top(); 

		// Decrease maxSizeY as the value returned by availableGeometry() is too big (does not include task bar obviously)
		maxSizeY -= 30;

		// If we have more than one screen, assume that every screen is as big as the primary screen
		if(desktop->numScreens() > 1)
			maxSizeX *= desktop->numScreens();
	}

	// Transform optimal size of fraPicture to optimal size of whole window by adding delta width and height
	sizeX += delta.x();
	sizeY += delta.y();

	// Calc zoomfactor: if optimal window size is bigger than screen, reduce zoom to make optimal size fit in screen
	int zoomPercent = 100;
	if(maxSizeX > 0 && maxSizeY > 0) {
		int overflowX = sizeX - maxSizeX,
			overflowY = sizeY - maxSizeY;

		float factorX = 1, factorY = 1;

		if(overflowX > 0 || overflowY > 0) {
			sizeX -= delta.x();
			sizeY -= delta.y();

			factorX = static_cast<float>(sizeX - overflowX) / sizeX;
			factorY = static_cast<float>(sizeY - overflowY) / sizeY;

			float zoomFactor = std::min(factorX, factorY);
			if(zoomFactor < 1.0f) {
				//Change from factor to percent and floor zoom by casting to int
				zoomPercent = (int)(zoomFactor * 100.0f);

				sizeX *= zoomFactor;
				sizeY *= zoomFactor;
			}

			sizeX += delta.x();
			sizeY += delta.y();
		}
	}

	//std::cout << "\nOptimalsize: x=" << sizeX << ", y=" << sizeY << "\n";

	// Set window size
	movieWindow->resize(QSize(sizeX, sizeY));

	// Switch to first export picture and set center position
	movieWindow->setPictureDisplay(firstTimepoint, centerPos, zoomPercent, -1);

	// Not needed when not using grabWindow()
	// 	// Make sure region selection form is hidden
	// 	if (positionManager->frmRegionSelection)
	// 		positionManager->frmRegionSelection->hide();	
}

void MovieExporter::getMinAndMaxCoordsOfSelectedCells(float& _minX, float& _minY, float& _maxX, float& _maxY, float& _maxCellRadius)
{
	// Get tree view
	const TreeDisplay *treeView = frmTracking->getTreeView();
	if(!treeView)
		throw TTTException("Could not get treeview.");

	// Get selected tracks
	const Q3IntDict<Track> tracks = treeView->getSelectedTracks(true);

	// Make sure any tracks have been selected
	// 	if(tracks.count() == 0) {
	if(tracks.isEmpty())
		throw TTTException("Cannot calculate best window size: no tracks selected.");

	// Get minimum  and maximum coordinates that have to be visible
	_minX = std::numeric_limits<float>::max();
	_minY = std::numeric_limits<float>::max();
	_maxX = -std::numeric_limits<float>::max();
	_maxY = -std::numeric_limits<float>::max();
	bool foundTrackPoint = false;

	Q3IntDictIterator<Track> itTrack(tracks);
	for(; itTrack.current(); ++itTrack) {
		// Iterate over Trackpoints of current track
		//const Q3PtrVector<TrackPoint> trackPoints = (itTrack.current())->getAllTrackpoints();
		const QVector<TrackPoint*>& trackPoints = (itTrack.current())->getAllTrackpoints();

		for(unsigned int i = 0; i < trackPoints.size(); ++i) {
			// Make sure we have a valid trackpoint
			if(TrackPoint* currentTrackPoint = trackPoints[i]) {

				//Use trackpoint if timepoint is in selected time range
				if(currentTrackPoint->TimePoint >= firstTimepoint && currentTrackPoint->TimePoint <= lastTimepoint) {
					foundTrackPoint = true;

					// Update max/min values
					_minX = std::min(_minX, currentTrackPoint->X);
					_minY = std::min(_minY, currentTrackPoint->Y);
					_maxX = std::max(_maxX, currentTrackPoint->X);
					_maxY = std::max(_maxY, currentTrackPoint->Y);
					// TODO:
					//_maxCellRadius = std::max(_maxCellRadius, currentTrackPoint->CellRadius);
					_maxCellRadius = 20;
				}
			}
		}
	}

	if(!foundTrackPoint)
		throw TTTException("Cannot calculate best window size: could not find any trackpoints within selected time range.");

}


QString MovieExporter::getExportDirectory()
{
	QString usersign = UserInfo::getInst().getUsersign();
	if (usersign.isEmpty())
		throw TTTException("Could not get usersign.");

	// Get destination folder
	QString movieFolder = TTTManager::getInst().getNASDrive() + MOVIE_EXPORT_FOLDER;
	QString treeName = "";

	//BS 2010/02/02: Dirk's wish:
	//use the tree name as a superfolder, if a tree exists
	if (TTTManager::getInst().getTree()) {
		//get absolute path to tree name
		treeName = TTTManager::getInst().getTree()->getFilename();

		if (treeName > "") {

			//BS 2010/03/29 if the treename contains BACKUP_FILENAME, then the user clicked on "load last autosave colony" - this treename is not desired, thus omit it
			if (! treeName.contains (BACKUP_FILENAME)) {

				//extract pure filename
				treeName = treeName.mid (treeName.findRev ('/') + 1);
				//without suffix (.ttt)
				treeName = treeName.left (treeName.length() - 4);

				movieFolder += "/" + treeName;
			}
		}
		//else: treename not yet known -> cannot be used for export
	}

	//create subdirectory for current experiment
	QString basUS = positionManager->getBasename() + usersign;
	movieFolder += "/" + basUS;

	//create subfolder with current date and time to avoid older exports being overwritten
	QDateTime cdt = QDateTime::currentDateTime();
	QString date = cdt.toString ("yyyyMMdd_hhmmss");
	movieFolder += "/" + date + "/";

	return movieFolder;
}


void MovieExporter::finishEport()
{
	//drawWavelengthIndex = dwi_backup;

	//switch on the orientation box again
	if(orientationBoxWasVisible)
		multiPicViewer->setOrientationBoxVisible(true);

	if(autoSizeAndPosition) {
		// Tell PictureView to draw all tracks again
		PictureView::setDrawOnlySelectedTracks(false);
	}

	// unhide controls
	if(controlsHaveBeenHidden && movieWindow->getControlsHidden())
		movieWindow->hideShowControls();

	// Show tracks, if we hid them earlier
	if(exportMode == HIDE_TRACKS)
		movieWindow->setShowTracks(true, true);

	// Un-hide scrollbars and out of sync box and position buttons and active view
	multiPicViewer->showHideScrollbars(false);
	multiPicViewer->setHideOutOfSyncBox(false);
	multiPicViewer->showPositionButtons(true);
	multiPicViewer->setMarkActivePictureView(true);

	// Restore wavelengthbox status
	multiPicViewer->setShowWavelengthBox (wavelengthBoxWasVisible, 0, MAX_WAVE_LENGTH + 1);

	// Restore external tracks mode
	movieWindow->setExternalTracksMode(oldExternalTracksMode);
}

void MovieExporter::abortExport()
{
	cancelExport = true;
}


void MovieExporter::parseRepeatsString()
{
	//parse stack repeat settings
	//format: [tp1-tp2:repeats]       x n
	//===================================
	
	// Check if repeats are available
	if(stackRepeatString.isEmpty())
		return;

	// Split string by ']'
	QStringList stackRepeats = QStringList::split ("]", stackRepeatString);

	//// Create entry for eacht extracted part
	//repeats = QVector<QPair <QPair <int, int>, int> >(stackRepeats.size() + 1); <-- bug?

	// inform user if format is wrong
	bool formatError = false;

	for (QStringList::Iterator iter = stackRepeats.begin(); iter != stackRepeats.end() && !formatError; ++iter ) {
		QString tmps = (*iter);
	
		bool conversionOK;

		//remove opening bracket
		int pos = tmps.find ("[");
		tmps = tmps.mid (pos + 1);	
		formatError |= (pos == -1);

		// extract tp1
		pos = tmps.find ("-");
		int tp1 = tmps.left (pos).toInt(&conversionOK);
		tmps = tmps.mid (pos + 1);
		formatError |= (pos == -1) || !conversionOK;
		
		// extract tp2
		pos = tmps.find (":");
		int tp2 = tmps.left (pos).toInt(&conversionOK);
		tmps = tmps.mid (pos + 1);
		formatError |= (pos == -1) || !conversionOK;

		// extract repeats
		int rep = tmps.toInt(&conversionOK);
		formatError |= !conversionOK;

		repeats.append (QPair <QPair <int, int>, int> (QPair<int, int> (tp1, tp2), rep));
	}

	if(formatError)
		throw TTTException("Format for repeats is invalid.");
}

void MovieExporter::addTimeBox( QPixmap* _pix, int _currentTimepoint )
{
	QString timeString = "";
	int secsPassed = 0;
	int days = 0, hours = 0, minutes = 0;
	
	int fontWidth = drawTimeboxFontSize;
		
	//add time in top left corner			
	//the time is calculated in relation to the timepoint specified in the spinbox
	if (drawTimeboxRelativeToTp <= _currentTimepoint) {
		secsPassed = positionManager->getFiles()->calcSeconds (drawTimeboxRelativeToTp, _currentTimepoint);
	}
	else {
		secsPassed = positionManager->getFiles()->calcSeconds (_currentTimepoint, drawTimeboxRelativeToTp);
	}
	days = secsPassed / 86400;
	secsPassed %= 86400;
	hours = secsPassed / 3600;
	secsPassed %= 3600;
	minutes = secsPassed / 60;
	secsPassed %= 60;
	timeString = QString ().sprintf ("%01d - %02d:%02d:%02d", days, hours, minutes, secsPassed);
	
	if (drawTimeboxRelativeToTp > _currentTimepoint) 
		timeString = "- " + timeString;
	
	
	QPainter p;
	////the painter is set to paint on the first available picture frame 
	////(instance of PictureView), because on fraPictures it would be covered
	////needs to be calculated every timepoint because the number of pictures can change between timepoints
	//int index = 0;
	//for ( ; index < (int)PictureFrames.size(); index++) 
	//	if (PictureFrames.find (index)->getShow())
	//		break;
	
	int xpos = -1, ypos = -1;
	
	//set y position
	switch (UserInfo::getInst().getStyleSheet().getTimeboxPosition() / 10) {
		case 0:				//invisible
			break;
		case 1:				//y: top
// 			ypos = 0;
			// --------------Oliver------------
			if(drawWavelength)
				ypos = 20;
			else
				ypos = 0;
			break;
		case 2:				//y: middle
			ypos = _pix->height() / 2 - 20;
			break;
		case 3:				//y: bottom
			ypos = _pix->height() - 40;
			break;
		default:
			;
	}
	
	//set x position
	switch (UserInfo::getInst().getStyleSheet().getTimeboxPosition() % 10) {
		case 0:				//invisible
			break;
		case 1:				//x: left
			xpos = 0;
			break;
		case 2:				//x: middle
			xpos = _pix->width() / 2 - fontWidth * 8;
			break;
		case 3:				//x: right
			xpos = _pix->width() - fontWidth * 8;
			break;
		default:
			xpos = 0;
	}
	
	if (xpos >= 0 & ypos >= 0) {
			p.begin(_pix);
			
			int tw = fontWidth * 8;
			int th = fontWidth * 2;


			switch (UserInfo::getInst().getStyleSheet().getTimeboxBackground()) {
				case 0:
					//transparent - nothing special
					break;
				case 1:
					//black background - draw black box
					p.save();
					p.setBrush (QBrush (Qt::black));
					p.drawRect (xpos, ypos, tw, th);
					p.restore();
					break;
				default:
					;
			}
			
			p.setFont (QFont ("Arial", fontWidth, drawTimeboxFontBold ? QFont::Bold : QFont::Normal));
			p.setPen (QPen (Qt::white, fontWidth));
			
			//prints the time string and fits it into the provided rectangle
			p.drawText (xpos, ypos, tw, th, Qt::AlignJustify | Qt::SingleLine, timeString);
			
			p.end();
//		}
	}
}

void MovieExporter::performExport()
{
	//bool dwi_backup = drawWavelengthIndex;
	//drawWavelengthIndex = UserInfo::getInst().getStyleSheet().getWavelengthVisibility();

	//int counter = 0;
	//QPixmap pix (fraPictures->width(), fraPictures->height());
	//if ((sender() == pbtExportCompleteAllTracks) || (sender() == pbtExportCompleteFirstTracks)) {
	//	pix.resize (positionManager->getDisplays().at (positionManager->getWavelength()).CompleteSize);
	//}

	QPixmap pix;
	PictureIndex currentPictureIndex(firstTimepoint, -1, -1);

	//switch off the orientation box, as it would disturb true grayscale pictures
	//setShowOrientationBox (false);

	//int stackRepeat = spbStackRepeat->value();
	int repTP1 = -1, repTP2 = -1, repRepeat = 1;

	while(1) {
		bool doNotExportFrame = false;
		if(exportMode == CENTER_ON_TRACKPOINTS) {
			if(!multiPicViewer->centerCurrentTrackIncludingPrecursors())
				doNotExportFrame = true;
		}

		//BS, 02.02.2010: events need to be processed to be able to cancel the export
		QApplication::processEvents();

		currentPictureIndex = positionManager->getPictureIndex();

		//find correct stack repeat value
		if ( !(currentPictureIndex.TimePoint >= repTP1 && currentPictureIndex.TimePoint <= repTP2) ) {
			//find new timepoint range and stack repeat value
			repRepeat = 1;	//default

			for (QVector<QPair <QPair <int, int>, int> >::Iterator iter = repeats.begin(); iter != repeats.end(); ++iter) {
				int tp1 = (*iter).first.first;
				int tp2 = (*iter).first.second;
				int rep = (*iter).second;

				if (currentPictureIndex.TimePoint >= tp1 && currentPictureIndex.TimePoint <= tp2) {
					repRepeat = rep;
					repTP1 = tp1;
					repTP2 = tp2;
					break;
				}
			}
		}


		//file format (due to Michael, 2008/07/07):
		//(pos.basename)_(6 digits tp)(4 digits stack index)
		//e.g. 20080707_p001_0001990001

		if (exportMode == COMPLETE_ALL || exportMode == COMPLETE_FIRST || exportMode == COMPLETE_NORMAL) {
			// Pix needs to have the correct size in this case
			int w = -1, h = -1;
			QSize completeSize = positionManager->getDisplays().at (positionManager->getWavelength()).CompleteSize;
			w = completeSize.width();
			h = completeSize.height();
			if(w <= 0 || h <= 0) {
				w = positionManager->positionInformation.getImageRects(0).width();
				h = positionManager->positionInformation.getImageRects(0).height();
			}

			pix.resize(w, h);

			// Draw complete picture
			multiPicViewer->exportPicture(&pix, positionManager->getWavelength(), 0, 0, w, h);
		}
		else {			
			//grab the display with everything on and in it
			//fraPictures contains all PictureView-instances currently displayed, so all wavelengths are grabbed
			//pix = QPixmap::grabWindow (fraPictures->winId());
			//equivalent: pix = QPixmap::grabWidget (fraPictures);
			pix = movieWindow->grabCurrentPicture();
		}

		if (drawTimebox) // -----------  Konstantin -----------
			addTimeBox(&pix, currentPictureIndex.TimePoint);
		//	printMovieExportTime (RelativeTimePoint, positionManager->getTimepoint(), &pix);

		// --------------Oliver-------------
		// Put call to ProceedPicture here to make sure picture will actually have changed until next call of grabWindow
		//proceed in current direction and wavelength, regarding multiple mode
		movieWindow->ProceedPicture(directionForward, 0);

		for (int rep = 1; rep <= repRepeat; rep++) {
			QString filename;

			if(format == AVI) {
				filename = movieFolder + getVideoFileName(picCount);
				if(!doNotExportFrame)
					++picCount;
			}
			else {
				// Normal filenames
				filename  = movieFolder + QString().sprintf (positionManager->getBasename() + "_t%06d_r%04d", currentPictureIndex.TimePoint, rep);
			}
			
			// Add file extension
			filename += fileExtension;

			// Try to save file
			if(!doNotExportFrame && !pix.save (filename, 0, pictureQuality))
				throw TTTException(QString("Could not save image %1").arg(filename));
		}

		// Check if we reached end or beyond end, or export was cancelled, or picture index did not change
		if((directionForward && currentPictureIndex.TimePoint >= lastTimepoint) || 
				(!directionForward && currentPictureIndex.TimePoint <= lastTimepoint) ||
				cancelExport ||
				positionManager->getPictureIndex() == currentPictureIndex)
			break;
	} 
}

void MovieExporter::displayFinishMessage()
{
	// Show messagebox with possibility to open folder
	QString message;

	if(!cancelExport) {
		message = QString("Export completed successfully to \"%1\".").arg(movieFolder);

		if(format == AVI)
			Tools::displayMessageBoxWithOpenFolder(message, "", videoFileName, true);
		else
			Tools::displayMessageBoxWithOpenFolder(message, "", movieFolder);
	}
	else {
		message = QString("Export aborted. Pictures may have been exported to \"%1\" and not deleted automatically.").arg(movieFolder);
		Tools::displayMessageBoxWithOpenFolder(message, "", movieFolder);
	}
}

void MovieExporter::convertImagesToVideo()
{
	QString filename = QString("%1_%2_%3.avi").arg(positionManager->getBasename()).arg(firstTimepoint, 5, 10, QChar('0')).arg(lastTimepoint, 5, 10, QChar('0')); 

	// Commandline arguments for ffmpeg.exe. 
	// See http://www.ffmpeg.org/ffmpeg-doc.html#SEC6 for more.
	QString commandLineArguments;
	QTextStream out(&commandLineArguments);

	// Framerate
	out << " -r " << fps;

	// Bitrate
	out << " -b " << avibitrate;

	// Input
	out << " -i " << "%04d" << fileExtension;

	// Video codec (should be playable on fresh windows xp installation)
	out << " -vcodec msmpeg4v2";
	//out << " -vcodec copy";
	//out << " -vcodec rawvideo";

	// Output
	out << " " << filename;

	// Init ffmpeg process object
	QProcess converterProcess;
	converterProcess.setWorkingDirectory(movieFolder);

	// Start process and wait 3600secs for it to finish
	QString procString = "\"" + QApplication::applicationDirPath() + "/ffmpeg.exe\"" + commandLineArguments;
	converterProcess.start(procString);
	if(converterProcess.waitForFinished(3600000)) {
		if(converterProcess.exitCode() != 0) {
			// As we have already exported the pictures, just display a messagebox instead of throwing an exception
			// and aborting the export. So user can open the directory and do whatever with the image files.
			QMessageBox::critical(movieWindow, "Error", "Images exported successfully, but conversion to video failed (exit code != 0).");

			// Set aborted flag to indicate error
			cancelExport = true;

			// Debug output
			qDebug() << "tTt: Movie conversion failed (exit code). Exit code: " << converterProcess.exitCode() 
				<< ". Errorcode: " << converterProcess.error()
				<< ". StdErr: " << converterProcess.readAllStandardError();
		}
		else {
			// Everything went fine, so save videoFileName
			videoFileName = movieFolder + filename;
		}
	}
	else {
		QMessageBox::critical(movieWindow, "Error", "Images exported successfully, but conversion to video failed (timeout).");
		cancelExport = true;

		// Debug
		qDebug() << "tTt: Movie conversion failed (timeout). Exit code: " << converterProcess.exitCode() << ". Error: " << converterProcess.error();
	}

	// Remove image files
	for(int i = 0; i < picCount; ++i) {
		QString picFile = movieFolder + getVideoFileName(i) + fileExtension;
		if(!QFile::remove(picFile))
			break;	// Just abort..
	}
}

QString MovieExporter::getVideoFileName( int _num )
{
	QString filename;

	// ffmpeg is used to create the mpeg file from picture files.
	// for this, the files are called like 0000.jpg, 0001.jpg and so on.
	QTextStream stream(&filename);

	// File name
	stream.setFieldWidth(4);
	stream.setPadChar('0');
	stream << _num;

	return filename;
}

//void MovieExporter::encodeVideo()
//{
//	Revel_GetApiVersion();
//}
