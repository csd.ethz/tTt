/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef logging_h__
#define logging_h__

// QT
#include <QApplication>
#include <QVector>
#include <QFile>

/**
 * @author Oliver Hilsenbeck
 *
 * Class for logging. Logging in tTt currently works by using the QT message handling system.
 * Thus, messages from QT are logged, too. QT degbug messages can be disabled by compiling ttt
 * with QT_NO_DEBUG_OUTPUT, qt warnings with QT_NO_WARNING_OUTPUT.
 *
 * To log something, include <QDebug> and use the functions qDebug(), qWarning() and qCritical().
 * Use them like this: qDebug() << "tTt Debug: " << someVar << " ... ";
 * Be careful with qFatal() as this will kill the process.
 *
 * Log file will be stored in <appdata_path>\tTt. On win7 this is usually "C:\ProgramData\tTt".
 *
 * Note: Logging is not thread safe.
 */

class Logging {
private:
	// Max size in megabytes
	const static int MAX_SIZE = 3;

public:
	/**
	 * Init logging
	 * Returns true if successful. Should be called only once on initialization in main.cpp.
	 */
	static bool init();

	/**
	 * Get path to log file (including file name)
	 */
	static QString getLogFilePath();

	/**
	 * Logging function for QT-logging message handling system. Used only for RELEASE builds
	 * Important: Do not use this function directly. Instead use qDebug(), qWarning(), qCritical() or qFatal()
	 *
	 * @param _type the message type (debug, warning, critical, fatal)
	 * @param _msg the message
	 */
	static void tttMessageHandler(QtMsgType _type, const char *_msg);


private:
	// Read all lines of _file and return as string vector
	static QVector<QString> readAllLines(QFile& _file);

	// Filename with path
	static QString filename;

	// System specific new line sequence for files
	const static QString NEW_LINE;

	// Log file name
	const static QString LOG_FILE;

	// Currently logging something
	static bool currentlyLogging;
};


#endif // logging_h__