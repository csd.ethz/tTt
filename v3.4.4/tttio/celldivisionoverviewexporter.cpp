/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
// Project includes
#include "celldivisionoverviewexporter.h"
#include "tttbackend/tttexception.h"
#include "tttgui/tttmovie.h"
#include "tttbackend/tttmanager.h"
#include "tttgui/ttttracking.h"
#include "tttbackend/tools.h"

// QT includes
#include <QPixmap>
#include <QPainter>


CellDivisionOverviewExporter::CellDivisionOverviewExporter(TTTMovie* _movieWindow, 
														   Track* _motherTrack, 
														   int _timePointsBefore,
														   int _timePointsAfter,
														   int _interval,
														   int _picSizeX,
														   int _picSizeY,
														   QString _exportDirectory,
														   QVector<int> _wavelengths,
														   bool _hideTrackpoints,
														   FORMAT _format)
{
	targetPixmap = 0;

	interval = _interval;
	hideTrackpoints = _hideTrackpoints;

	// Get pointers to moviewindow and multi pic viewer
	movieWindow = _movieWindow;
	if(!movieWindow || !motherTrack)
		throw TTTException("Internal error: !movieWindow");

	multiPicViewer = movieWindow->getMultiplePictureViewer();
	if(!multiPicViewer)
		throw TTTException("Internal error: !multiPicViewer");

	posManager = movieWindow->getPositionManager();
	if(!posManager)
		throw TTTException("Internal error: !posManager");

	// Get mother track
	motherTrack = _motherTrack;
	if(!motherTrack)
		throw TTTException("No track specified.");

	// Get child tracks
	childTrack1 = motherTrack->getChildTrack(1);
	childTrack2 = motherTrack->getChildTrack(2);
	if(!childTrack1 || !childTrack2)
		throw TTTException("Specified cell does not have two child tracks!");

	// Get number of timepoints
	timePointsBefore = _timePointsBefore;
	timePointsAfter = _timePointsAfter;
	if(timePointsBefore < 0 || timePointsAfter < 0)
		throw TTTException("timepoints before and after must be > 0!");

	// Get division timepoints
	divTimepoint = motherTrack->getLastTrace();
	if(divTimepoint < posManager->getFirstTimePoint() || divTimepoint > posManager->getLastTimePoint())
		throw TTTException("timepoint of division cannot be detected.");

	// Get pics size
	picSizeX = _picSizeX;
	picSizeY = _picSizeY;
	if(picSizeX <= 0 || picSizeY <= 0)
		throw TTTException("Invalid pic size!");

	// Get wavelengths
	waveLengths = _wavelengths;
	if(waveLengths.size() == 0)
		throw TTTException("No wavelengths specified!");

	// Export dir
	exportDirectory = _exportDirectory;
	if(exportDirectory.right(1) != QString("/"))
		exportDirectory += "/";


	// Set file extension
	switch(_format) {
		case JPEG:
			fileExtension = ".jpg";
			break;
		case PNG:
			fileExtension = ".png";
			break;
		default:
			fileExtension = ".bmp";
	}
}


void CellDivisionOverviewExporter::runExport() throw(TTTException)
{
	try {
		// Initialization
		initExport();

		// Do export
		performExport();
	}
	catch(const TTTException& err) {
		// Cleanup anyways
		finishEport();

		// Let caller decide what to do
		throw;
	}

	// Cleanup and show message
	finishEport();
	displayFinishMessage();
}


void CellDivisionOverviewExporter::initExport() throw(TTTException)
{
	// Init picture
	initPicture();

	// Disable split view
	if(multiPicViewer->isInMultipleView())
		multiPicViewer->setMultipleMode(false);

	// Disable overlay
	if(multiPicViewer->isInOverlayMode())
		multiPicViewer->setOverlay(false, true);

	// Hide controls to allow small window sizes
	if(!movieWindow->getControlsHidden())
		movieWindow->hideShowControls();

	// Hide scrollbars
	multiPicViewer->showHideScrollbars(true);

	// Disable wavelength
	multiPicViewer->setShowWavelengthBox (false, 0, MAX_WAVE_LENGTH + 1);

	// If processEvents() is not called now, we will not be able to reduce window size below the
	// size needed by the controls, although they are hidden
	qApp->processEvents();

	//// Make sure movie window is at least as big as desired
	//QSize nettoSize = movieWindow->getPictureDisplaySize();
	//if(nettoSize.width() < picSizeX || nettoSize.height() < picSizeY)
	//	movieWindow->setPictureSize(picSizeX, picSizeY);

	movieWindow->setPictureSize(picSizeX+5, picSizeY+5);

	// Hide trackpoints if desired
	if(hideTrackpoints)
		movieWindow->setShowTracks(false, true);
}

void CellDivisionOverviewExporter::initPicture() throw(TTTException)
{
	// Calc pic size
	int sumOfPics = timePointsBefore + timePointsAfter + 1;
	int numWavelengths = waveLengths.size();

	fullSizeX = spaceBetweenPicsX * (sumOfPics + 1) + picSizeX * sumOfPics + spaceRight;
	fullSizeY = verticalSpaceBetweenChilds + numWavelengths * picSizeY * 2 + (numWavelengths-1)*spaceBetweenPicsY*2;

	// Create pixmap
	targetPixmap = new QPixmap(fullSizeX, fullSizeY);
	if(!targetPixmap || targetPixmap->isNull())
		throw TTTException("Cannot create picture.");

	// Make white background
	targetPixmap->fill(Qt::white);

	// Draw tree lines
	drawTreeLines();

	// Info
	drawInfoBox();
}

void CellDivisionOverviewExporter::drawTreeLines()
{
	// Draw mother track line
	QPainter painter(targetPixmap);
	int x = 0,
		y = fullSizeY / 2 - treeBarWidth / 2,
		width = (timePointsBefore+1) * spaceBetweenPicsX + timePointsBefore*picSizeX + (picSizeX/2 - treeBarWidth / 2);

	painter.setBrush(Qt::SolidPattern);
	painter.drawRect(x, y, width, treeBarWidth);

	// Calc coordinates of childs
	childsX = width+1;
	yChild1 = fullSizeY / 4;
	yChild2 = yChild1 * 3;

	// Draw child 1 lines
	x = childsX;
	width = (fullSizeX-spaceRight) - childsX;
	y = yChild1 - treeBarWidth / 2;
	painter.drawRect(x, y, width, treeBarWidth);
	drawDots(painter, x + width, y);

	// Draw child 2 lines
	y = yChild2 - treeBarWidth / 2;
	painter.drawRect(x, y, width, treeBarWidth);
	drawDots(painter, x + width, y);

	// draw vertical division line
	x = childsX;
	y = yChild1 - treeBarWidth / 2;
	int height = picSizeY - yChild1 - (picSizeY - yChild2);
	painter.drawRect(x, y, treeBarWidth, height);
}


void CellDivisionOverviewExporter::drawDots( QPainter& p, int _posX, int _posY )
{
	_posX += treeBarWidth;

	for(int i = 0; i < numDots; ++i) {
		p.drawRect(_posX, _posY, treeBarWidth, treeBarWidth);
		_posX += 2 * treeBarWidth;
	}
}


void CellDivisionOverviewExporter::performExport() throw(TTTException)
{
	// Current x position of pic
	int posX = spaceBetweenPicsX;

	// Go through all timepoints
	for(int curTP = divTimepoint - interval * timePointsBefore; curTP <= divTimepoint + interval * timePointsAfter; curTP += interval) {
		// Go to tp
		TTTManager::getInst().setTimepoint(curTP);

		// Go through wavelengths
		for(int i = 0; i < waveLengths.size(); i++) {
			
			// Set wl
			int curWl = waveLengths[i];
			multiPicViewer->setWaveLength(curWl, true, true);

			if(curTP <= divTimepoint) {
				// add mother image
				addImage(motherTrack, posX, getPicPosY(i, 0));
			}
			else {
				// add child1 image
				addImage(childTrack1, posX, getPicPosY(i, 1));

				// add child2 image
				addImage(childTrack2, posX, getPicPosY(i, 2));
			}
		}

		// Add to timepoints string for status message
		exportedTPs += QString::number(curTP) + ", ";

		posX += picSizeX + spaceBetweenPicsX;
	}

	// Cut off last ", "
	if(exportedTPs.length() >= 2)
		exportedTPs = exportedTPs.left(exportedTPs.length()-2);

	// save pic
	QString experimentName = TTTManager::getInst().getExperimentBasename();
	QString cellsString = QString("Cell_%1_Division").arg(motherTrack->getNumber());
	filename = exportDirectory + experimentName + "_" + cellsString + fileExtension;

	if(!targetPixmap->save(filename, 0, 100))
		throw TTTException(QString("Could not save file %1!").arg(filename));
}

int CellDivisionOverviewExporter::getPicPosY( int _wlCounter, int _type )
{
	// Get pic size
	int fullSizeY = targetPixmap->size().height();

	// Get starting position
	int pos;
	if(_type == 0)
		// Mother track
		pos = fullSizeY / 2;
	else if(_type == 1)
		// Child 1
		pos = fullSizeY / 4;
	else
		// Child 2
		pos = (fullSizeY * 3) / 4;

	// Now pic is exactly under its trackline

	// Consider wavelengths
	int numWavelengths = waveLengths.size();
	int delta = -(numWavelengths * picSizeY + (numWavelengths-1)*spaceBetweenPicsY) / 2;

	delta += _wlCounter * (picSizeY + spaceBetweenPicsY);

	// Apply delta
	pos += delta;

	// finished
	return pos;
}


void CellDivisionOverviewExporter::addImage( Track* _track, int _posX, int _posY )
{
	// Center on track
	multiPicViewer->centerTrack(_track);

	// Grab pic
	QPixmap pic = movieWindow->grabCurrentPicture();
	if(pic.isNull())
		throw TTTException("Cannot get picture.");

	// convert to qimage
	QImage img = pic.toImage();
	if(img.isNull())
		throw TTTException("Cannot convert picture.");

	// Take the middle of the pic
	QSize srcSize = img.size();
	int sx = (srcSize.width() / 2) - (picSizeX / 2),
		sy = (srcSize.height() / 2) - (picSizeY / 2);

	// Add pic
	QPainter p(targetPixmap);
	p.drawImage(_posX, _posY, img, sx, sy, picSizeX, picSizeY);
}

void CellDivisionOverviewExporter::finishEport() throw(TTTException)
{
	// Un-hide scrollbars
	multiPicViewer->showHideScrollbars(false);

	// Re-enable wavelength
	multiPicViewer->setShowWavelengthBox (true, 0, MAX_WAVE_LENGTH + 1);

	// Show controls
	if(movieWindow->getControlsHidden())
		movieWindow->hideShowControls();

	// Un-hide trackpoints
	if(hideTrackpoints)
		movieWindow->setShowTracks(true, true);
}

void CellDivisionOverviewExporter::displayFinishMessage() throw(TTTException)
{
	QString msg = QString("File saved as \"%1\".\n\nExported the following timepoints:\n%2").arg(filename).arg(exportedTPs);

	// Open folder with file selected
	Tools::displayMessageBoxWithOpenFolder(msg, "Export completed", filename, true);
}

CellDivisionOverviewExporter::~CellDivisionOverviewExporter()
{
	if(targetPixmap)
		delete targetPixmap;
}

void CellDivisionOverviewExporter::drawInfoBox()
{
	// Get info strings
	QString experimentName = TTTManager::getInst().getExperimentBasename();
	//QString treeName = TTTManager::getInst().getTree()->getFilename();
	QString treeName = TTTManager::getInst().frmTracking->getFilenameFormCaption();
	QString cellsString = QString("Cell %1/%2").arg(childTrack1->getNumber())
											.arg(childTrack2->getNumber());

	// Remove path, XX.ttt and experiment name from treename
	if(!treeName.isEmpty()) {
		treeName = treeName.mid(treeName.lastIndexOf("/") + 1);
		treeName = treeName.left(treeName.length() - 6);
		treeName = treeName.mid(treeName.lastIndexOf("_") + 1);
	}

	// Set font size
	QPainter p(targetPixmap);
	QFont font = p.font();
	font.setPointSize(fontSize);
	p.setFont(font);

	// Draw text
	int y = textVerticalSpace;
	p.drawText(spaceLeft, y, infoBoxWidth, infoBoxHeight, Qt::AlignCenter, experimentName + "\n" + treeName + "\n" + cellsString);
}
