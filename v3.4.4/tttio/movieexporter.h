/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef movieexporter_h__
#define movieexporter_h__

// QT
#include <QPair>
#include <QString>
#include <QVector>

// Project
#include "tttgui/tttmovie.h"

// Forward declarations
class TTTPositionManager;
class MultiplePictureViewer;
class TTTTracking;
class QPixmap;
class TTTException;


/**
*	@author Oliver Hilsenbeck
*	
*	Class to perform a single movie export. Code was located in TTTMovie
*	before and was moved into its own class for better code structure.
*	Can be used by gui-thread as QApplication::processEvents() is called
*	every once in a while during export.
*/
class MovieExporter {
public:
	// Supported file formats
	enum FORMAT {
		JPEG = 0,
		PNG = 1,
		BMP = 2,
		AVI = 3
	};

	// Export mode
	enum MODE {
		// Do not display tracks
		HIDE_TRACKS = 0,

		// Normal: As in movie window
		NORMAL = 1,

		// Export complete pictures and draw tracks from all colonies
		COMPLETE_ALL = 2,

		// Export complete pictures and draw tracks from the first point of time on each frame
		COMPLETE_FIRST = 3,

		// As in normal, but center on each trackpoint
		CENTER_ON_TRACKPOINTS = 4,

		// Export complete picutres, but do not change the displayed tracks
		COMPLETE_NORMAL = 5
	};

	/**
	 * Constructor (Warning: can throw TTTException)
	 * Create object for one export. Note: not all settings are provided via the constructor
	 * but via user stylesheet settings.
	 * @param _movieWindow pointer to movie window for pictures to be exported
	 * @param _firstTP first point of time to be exported
	 * @param _lastTP last point of time to be exported
	 * @param _format output format
	 * @param _jpegQuality quality for jpeg files. Between [0,100]. Ignored for other formats
	 * @param _exportMode export mode
	 * @param _autoSizeAndPosition will resize movie window to exactly fit all exported tracks							 
	 * @param _drawTimebox if time should be displayed in exported frames
	 * @param _drawTimeboxFontBold time font bold
	 * @param _drawTimeboxFontSize time font size
	 * @param _drawTimeboxRelativeToTp point of time to which drawn time is relative
	 * @param _drawWavelength if wavelength should be drawn
	 * @param _repeats repeats string
	 * @param _centerModePicSize picture size if mode is CENTER_ON_TRACKPOINTS
	 * @param _fps frames per scond. Only for video formats
	 * @param _avibitrate avi bit rate
	 */
	MovieExporter(
		TTTMovie* _movieWindow, 
		int _firstTP, 
		int _lastTP, 
		FORMAT _format = JPEG,
		int _jpegQuality = 75, 
		MODE _exportMode = NORMAL,
		bool _autoSizeAndPosition = false,
		bool _drawTimebox = true,
		bool _drawTimeboxFontBold = false,
		int _drawTimeboxFontSize = 10,
		int _drawTimeboxRelativeToTp = 0,
		bool _drawWavelength = true,
		QString _repeats = "",
		QSize _centerModePicSize = QSize(),
		int _fps = 1,
		int _avibitrate = 200000) throw(TTTException);
	// Other parameters saved in user-stylesheet: 
	// - draw timbox on black background
	// - timebox position


	/**
	 * Execute export (Warning: can throw TTTException)
	 */
	void runExport() throw(TTTException);

	/**
	 * Cancel running export
	 */
	void abortExport();


	/**
	 * Get export folder (only valid after runExport() has finished)
	 * @return folder to which movie has been exported
	 */
	QString getDirectory() const {
		return movieFolder;
	}


private:
	
	// Init export
	void initExport() throw(TTTException);

	// Export main loop, exports image files
	void performExport() throw(TTTException);

	// Convert image files to video file
	void convertImagesToVideo();

	// Finish export (cleanup)
	void finishEport();

	// Display summary message
	void displayFinishMessage();

	// Parse repeats string
	void parseRepeatsString();

	// Add timebox to a single frame
	void addTimeBox(QPixmap* _pix, int _currentTimepoint);

	// Adjust size and position
	void adjustSizeAndPosition();

	// Get export directory
	QString getExportDirectory();

	// Get minimal and maximal coordinates of all currently selected cells within the timepoints that are to be exported
	void getMinAndMaxCoordsOfSelectedCells(float& _minX, float& _minY, float& _maxX, float& _maxY, float& _maxCellRadius);

	//void encodeVideo();

	// Get filenames of single pictures for video export (AVI or MPEG ..) without path and without file extension
	static QString getVideoFileName(int _num);

	// Moviewindow of the movie to be exported and corresponding positionmanager
	TTTPositionManager* positionManager;
	TTTMovie* movieWindow;
	MultiplePictureViewer* multiPicViewer;
	const TTTTracking* frmTracking;

	// Timepoints
	int firstTimepoint, lastTimepoint;

	// Export directory
	QString movieFolder;

	// Parameters
	FORMAT format;
	MODE exportMode;
	bool autoSizeAndPosition;
	bool drawTimebox;
	bool drawTimeboxFontBold;
	int drawTimeboxFontSize;
	int drawTimeboxRelativeToTp;
	bool drawWavelength;
	QSize centerModePicSize;
	int fps;
	int pictureQuality;
	int avibitrate;
	
	// repeats
	QString stackRepeatString;
	QVector<QPair <QPair <int, int>, int> > repeats;

	// If export should be cancelled
	bool cancelExport;

	// Direction of export
	bool directionForward;

	// Internal status variables
	bool disableShowAllTracksWhenFinished;
	TTTMovie::DisplayExternalTracksMode oldExternalTracksMode;
	QString fileExtension;
	bool orientationBoxWasVisible;
	bool controlsHaveBeenHidden;
	bool wavelengthBoxWasVisible;

	// For video output: path + filename of video file. Only set when video has been created successfully.
	QString videoFileName;

	// Only needed if we will create an mpeg
	int picCount;
};

#endif // movieexporter_h__