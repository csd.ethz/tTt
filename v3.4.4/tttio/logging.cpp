/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
// tTt
#include "tttio/logging.h"
#include "tttdata/systeminfo.h"
#include "tttio/winFilePermissions.h"

// QT
#include <QTextStream>
#include <QMessageBox>
#include <QDateTime>



// Static definitions
QString Logging::filename;
const QString Logging::LOG_FILE = "log.txt";
const QString Logging::NEW_LINE = "\r\n";
bool Logging::currentlyLogging = false;

void Logging::tttMessageHandler( QtMsgType _type, const char *_msg )
{
	// Avoid infinite recursion
	if(currentlyLogging)
		return;

	currentlyLogging = true;

	// Open file
	QFile file(filename);
	if(!file.open(QIODevice::Append))
		return;

	// Build msg
	QString qMsg;
	switch (_type) {
	case QtDebugMsg:
		qMsg = QString("Debug: ") + _msg;
		break;
	case QtWarningMsg:
		qMsg = QString("Warning: ") + _msg;
		break;
	case QtCriticalMsg:
		qMsg = QString("Critical: ") + _msg;
		break;
	case QtFatalMsg:
		qMsg = QString("Fatal: ") + _msg;
	}

	// Add date
	QDateTime date = QDateTime::currentDateTime();
	QString stringDate = date.toString("dd.MM.yyyy-hh:mm:ss");

	// Write msg
	QTextStream out(&file);
	out << "[" << stringDate << "] - " << qMsg << NEW_LINE;

	currentlyLogging = false;
}

bool Logging::init()
{
	// Get path
	QString path = SystemInfo::getAppDataPath();
	if(path.isEmpty()) {
		QMessageBox::critical(0, "tTt Error", "Could not initialize logging module: cannot get user app data path.");
		return false;
	}

	// Get complete filename
	filename = path + LOG_FILE;

	// Open file
	QFile file(filename);
	if(!file.open(QIODevice::ReadWrite)) {
		QMessageBox::critical(0, "tTt Error", QString("Could not initialize logging module: cannot open file. (%1)").arg(filename));
		return false;
	}

	// Make sure file can be opened by anyone
	//file.setPermissions( 
	//	QFile::ReadOwner |
	//	QFile::WriteOwner |
	//	QFile::ExeOwner |
	//	QFile::ReadUser |
	//	QFile::WriteUser |
	//	QFile::ExeUser |
	//	QFile::ReadGroup |
	//	QFile::WriteGroup |
	//	QFile::ExeGroup |
	//	QFile::ReadOther |
	//	QFile::WriteOther |
	//	QFile::ExeOther);
	std::string stdFile = filename.toStdString();
	if(AddAccessRights(stdFile.c_str(), GENERIC_ALL) == FALSE ) {
		QMessageBox::critical(0, "tTt Error", "Log file initialization: log file could be created, but granting access rights failed. Logging will probably not work for other users on this system.");
	}

	// Check size
	qint64 size = file.size() / 1024;
	if(size > MAX_SIZE*1024) {
		QVector<QString> lines = readAllLines(file);
		if(lines.isEmpty()) {
			// Some error must have occurred
			QMessageBox::critical(0, "tTt Error", "Could not initialize logging module: File truncation failed (1).");
			return false;
		}
		file.close();

		// Open for writing
		if(!file.open(QIODevice::WriteOnly)) {
			QMessageBox::critical(0, "tTt Error", "Could not initialize logging module: File truncation failed (2).");
			return false;
		}

		// Copy half of lines
		QTextStream out(&file);
		for(int i = lines.size() / 2; i < lines.size(); ++i) {
			out << lines[i] << NEW_LINE;
		}
	}

	file.close();

	return true;
}

QVector<QString> Logging::readAllLines( QFile& _file )
{
	// Read all lines of the file
	QTextStream in(&_file);
	QVector<QString> allLines;
	while(!in.atEnd()) 
	{
		QString curLine = in.readLine();
		if(!curLine.isNull())
			allLines.append(curLine);
		else
			// Reached end
			break;
	}

	return allLines;
}

QString Logging::getLogFilePath()
{
	return filename;
}
