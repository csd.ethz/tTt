/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "userinfoxmlparser.h"

UserInfoXMLParser::UserInfoXMLParser()
 : QXmlDefaultHandler()
{
}

UserInfoXMLParser::~UserInfoXMLParser()
{
}

bool UserInfoXMLParser::startDocument()
{
	return true;
}
	
//bool UserInfoXMLParser::startElement (const QString &_namespaceURI, const QString &_localName, const QString &_name, const QXmlAttributes &_attrs)
bool UserInfoXMLParser::startElement (const QString &, const QString &, const QString &, const QXmlAttributes &)
{

	return true;
}

//bool UserInfoXMLParser::endElement (const QString &_namespaceURI, const QString &_localName, const QString &_name)
bool UserInfoXMLParser::endElement (const QString &, const QString &, const QString &)
{
	return true;
}

