/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "exporthandler.h"

// Project includes
#include "tttbackend/tttmanager.h"
#include "tttbackend/tools.h"

#ifndef TREEANALYSIS
#include "tttgui/ttttracking.h"
#include "tttbackend/picturearray.h"
#endif

// Qt includes
#include <QProcess>



ExportHandler::ExportHandler()
{
	trackList.setAutoDelete (false);
	if (BREAKING_CELLNUMBER > 0)
		trackList.resize (BREAKING_CELLNUMBER + 1);
}

ExportHandler::~ExportHandler()
{
}

bool ExportHandler::exportData (const QString &_fileName, const QVector<QString> &_data)
{
	ExportHandler eh;
	
	return eh.exportDataset (_fileName, _data);
}

bool ExportHandler::exportDataset (const QString &_fileName, const QVector<QString> &_data)
{
	if (_fileName.isEmpty())
		return false;
	
	file.setName (_fileName);
	if (! file.open (QIODevice::WriteOnly)) 
		return false;		//opening the file for output failed
	
	//associate the stream with the file
	writer.setDevice (&file);
	
        for (QVector<QString>::const_iterator iter = _data.constBegin(); iter != _data.constEnd(); ++iter) {
		writer << (*iter) << "\n";
	}
	
	file.close();

	Tools::displayMessageBoxWithOpenFolder("Export completed.", "tTt", _fileName, true);
	
	return true;
}

bool ExportHandler::exportFluorescence (const QString &_fileName, /*PictureArray *_picArray,*/ Tree *_tree, int _zIndex, int _wavelength, const QString _intensityMatrixFilename)
{
	//Pictures = _picArray;
	
	if (_fileName.isEmpty())
		return false;
	
	if ((! _tree)) // || (! _picArray))
		return false;
	
	file.setFileName (_fileName);
	if (! file.open (QIODevice::WriteOnly)) 
		return false;		//opening the file for output failed
	
	tree = _tree;
	wavelength = _wavelength;
	zIndex = _zIndex;
	
	intensityMatrix.clear();
	if (! _intensityMatrixFilename.isEmpty()) {
		//load intensity matrix
		intensityMatrix.readMatrix (_intensityMatrixFilename);
	}
	
	//associate the stream with the file
	writer.setDevice (&file);
	
	LastTrackedTimePoint = tree->getMaxTrackedTimePoint();
	FirstTrackedTimePoint = tree->getMinTrackedTimePoint();
	//currentTimePoint = FirstTrackedTimePoint;
	
	tttpm = TTTManager::getInst().getBasePositionManager();
	if ((! tttpm) || (! tttpm->getFiles()) || (! tttpm->getPictures())) {
		QMessageBox::information (0, "tTt", "Cell export failed.", "Ok");
		return false;
	}
	//std::cout << tttpm->positionInformation.getIndex() << std::endl;
	
	//find out number of really exported cells
	exportableTracks = 0;
	Track *track = 0;
	for (int i = 1; i <= tree->getMaxPossibleTracks(); i++) {
		track = tree->getTrack (i);
		if (track)
			
			if ((track->getLastTrace() > 0) && (track->backgroundTracked()))
			     exportableTracks++;
	}
	
	exportedTracks = 0;
	Max_Current_TrackNumber = 0;
	writeHeader();
	
	connect ( &tmr, SIGNAL (timeout()), this, SLOT (writeTimePoint()));
	
	// Indicate that export is going on
#ifndef TREEANALYSIS
	TTTManager::getInst().frmTracking->setPermanentStatusbarMessage("Exporting fluorescence data..");
#endif

	tmr.start (0);
	
	return true;
	
}

void ExportHandler::writeHeader()
{
	//2008/10/13 BS: new header lines were not written into a new line
	if (Max_Current_TrackNumber > 0)
		writer << "\n\n";
	
	//caption line
	QString caption = "abs Time" + DELIMITER + "rel Time [s]" + DELIMITER + "rel Time [h]" + DELIMITER;
	writer << caption;
	
	trackList.clear();
	Track *track = 0;
	currentTimePoint = FirstTrackedTimePoint;
	int cc = 0;
	for (int i = Max_Current_TrackNumber + 1; i <= tree->getMaxPossibleTracks(); i++) {
		track = tree->getTrack (i);
		if (track)
			
			if ((track->getLastTrace() > 0) && (track->backgroundTracked())) {
			     	
				cc++;
			     	
			    if (BREAKING_CELLNUMBER == 0 || (BREAKING_CELLNUMBER > 0 && (cc % BREAKING_CELLNUMBER != 0))) {
				     	
				    trackList.insert (cc - 1, track);
					
					writer << "Cell " << i << " Integral normalized" << DELIMITER;
					writer << "Cell " << i << " Average normalized" << DELIMITER;
					writer << "Cell " << i << " StdDev normalized" << DELIMITER;
					writer << "Area[pixel]" << DELIMITER;
					writer << "Integral" << DELIMITER;
					writer << "Average" << DELIMITER;
					writer << "Min" << DELIMITER;
					writer << "Max" << DELIMITER;
					writer << "BckGrIntegral" << DELIMITER;
					writer << "BckGrAverage" << DELIMITER;
					writer << "BckGrMin" << DELIMITER;
					writer << "BckGrMax" << DELIMITER;
					
					Max_Current_TrackNumber = i;
					
					exportedTracks++;
				}
			     	else
			     		break;
			}
	}
}

void ExportHandler::writeTimePoint()
{
#ifndef TREEANALYSIS
	if (! tttpm->getPictures())
		return;
	
	bool valuesExported = false;
	
	//!DO NOT CALL return within the method
	//!-> otherwise the time point is not incremented
	
	QString exportLine = "";
	QString dateString = "";
	
	QString old_position_key = "";
	QString position_key = "";
	
	//if the desired wavelength for the current timepoint exists, it is exportable
	//otherwise nothing is written to disk
	//if (tttpm->getFiles()->exists (currentTimePoint, wavelength)) {
		
		//writer << "\n";
		//dateString += "\r\n";
		
		int secDiff = tttpm->getFiles()->calcSeconds (FirstTrackedTimePoint, currentTimePoint, true, 1, wavelength);
		
		//writer << (TTTManager::getInst().getBasePositionManager()->getFiles()->getRealTime (currentTimePoint, 1)).toString () << ";";
		dateString += (tttpm->getFiles()->getRealTime (currentTimePoint, zIndex, wavelength)).toString () + DELIMITER;
		//writer << secDiff << ";";
		dateString += QString ("%1").arg (secDiff);
		dateString += DELIMITER;
		//writer << secDiff / 3600 << ";";
		dateString += QString ("%1").arg (secDiff / 3600);
		dateString += DELIMITER;
		
		Track *tmpTrack = 0;
		TrackPoint tmpTrackPoint;
		bool picLoaded = false;
		int PixelColor = 0;
		int PixelCount = 0;
		int dx = 0, dy = 0;
		
		int MaxValueCell = 0;
		int MinValueCell = 255;
		int ValueIntegralCell = 0;
		int SumOfSquaresCell = 0;
		float StdDevCell = 0.0f;
		float AvgCell = 0.0f;
		
		int MaxValueBackground = 0;
		int MinValueBackground = 255;
		int ValueIntegralBackground = 0;
		int SumOfSquaresBackground = 0;
		float StdDevBackground = 0.0f;
		float AvgBackground = 0.0f;
		
		
		//only the really tracked tracks with their lifecycle containing the current timepoint are exported
		for (int i = 0; i < (int)trackList.size(); i++) {
			tmpTrack = trackList.find (i);
			if (tmpTrack) {
				if (tmpTrack->aliveAtTimePoint (currentTimePoint)) {
					
					//check if cell really exists at the current timepoint 
					// (if a real trackpoint exists)
					tmpTrackPoint = tmpTrack->getTrackPoint (currentTimePoint, false);
					
                    if ((tmpTrackPoint.CellDiameter > 0.0f) &
						(tmpTrackPoint.X != -1) &
						(tmpTrackPoint.XBackground != -1)) {
						
						//trackpoint exists -> export
						// agenda:
						// 1) load picture with desired wavelength at current timepoint into memory
						// 2) read pixel values for cell region in wavelength picture
						// 3) read pixel values for background region in wavelength picture
						// 4) unload picture (if not loaded before)
						// 5) write results to file
						
						// 1)
						bool pointInside = true;
						
						//new version (Bernhard, 2009/11/23):
						//the position where the trackpoint was set is stored, so we can use it directly
						//yet, for older ones, this is not stored, so we have to calculate the position
						// NOTE: set as comment for old movies without real positions
						position_key = tmpTrackPoint.Position;
						if (position_key == "" || position_key == "000") {
							pointInside = TTTManager::getInst().pointIsInPosition (tttpm, tmpTrackPoint.point());
						}
						else {
							//position is stored in trackpoint
							if (old_position_key != position_key) {
								//a change of position -> load pictures of new position now
								pointInside = false;
								old_position_key = position_key;
							}
						}
						
						
						if (! pointInside) {
							
							if (position_key == "" || position_key == "000") {
								tttpm = TTTManager::getInst().getPositionAt (tmpTrackPoint.X, tmpTrackPoint.Y);
							}
							else {
								tttpm = TTTManager::getInst().getPositionManager (position_key);
							}
							
							
							if (! tttpm->getPictures()) {
								//user must load at least the first picture of this position
								QMessageBox::warning (0, "Pictures missing, export stopped...", "Please load (at least) the first picture of wl 0 for position " + tttpm->positionInformation.getIndex() + " and redo the export.", "Ok");
								tmr.stop();
								return;
							}
							else {
								dateString = "";
								
								int secDiff = tttpm->getFiles()->calcSeconds (FirstTrackedTimePoint, currentTimePoint, true, 1, wavelength);
								
								dateString += (tttpm->getFiles()->getRealTime (currentTimePoint, zIndex, wavelength)).toString () + DELIMITER;
								dateString += QString ("%1").arg (secDiff);
								dateString += DELIMITER;
								dateString += QString ("%1").arg (secDiff / 3600);
								dateString += DELIMITER;
							}
						}
						
						if (! tttpm->getFiles()->exists (currentTimePoint, 1, wavelength)) {
							valuesExported = false;
							break;
						}
						
						picLoaded = tttpm->getPictures()->isLoaded (currentTimePoint, wavelength, 1);
						if (! picLoaded) {
							tttpm->getPictures()->setLoading (currentTimePoint, zIndex, wavelength, true);
							tttpm->getPictures()->loadPictures (false);
						}
						
						
						// 2)
						PixelCount = 0;
						MaxValueCell = 0;
						MinValueCell = 255;
						ValueIntegralCell = 0;
						SumOfSquaresCell = 0;
						StdDevCell = 0.0f;
						AvgCell = 0.0f;
						
						//calculate picture coordinates
                        QPointF middle (tmpTrackPoint.X, tmpTrackPoint.Y);
                        middle = tttpm->getDisplays().at (wavelength).calcTransformedCoords (middle, &tttpm->positionInformation);

                        int radius = (int)(tmpTrackPoint.CellDiameter / 2.0f);
						
						for (	int x = (int)(middle.x() - radius); 
								x <= (int)(middle.x() + radius); 
								x++) {
							for (	int y = (int)(middle.y() - radius);
									y <= (int)(middle.y() + radius);
									y++) {
								
								//check if point is inside the region of the cell (circle!) via Pythagoras
								dx = x - (int)middle.x();
								dy = y - (int)middle.y();
								if ((dx*dx + dy*dy) <= radius*radius) {
									PixelCount++;
									
									PixelColor = tttpm->getPictures()->readPixel (currentTimePoint, zIndex, wavelength, x, y);
									if (! intensityMatrix.isEmpty()) {
										float correction_factor = intensityMatrix.getValue (x, y);
										PixelColor = (int)((float)PixelColor * correction_factor);
									}
									
									if (PixelColor > MaxValueCell)
										MaxValueCell = PixelColor;
									if (PixelColor < MinValueCell)
										MinValueCell = PixelColor;							
									
									ValueIntegralCell += PixelColor;
									SumOfSquaresCell += Tools::pow (PixelColor, 2);
								}
							}
						}
						if (PixelCount > 0)
							AvgCell = (float)ValueIntegralCell / (float)PixelCount;
						//StdDevCell = (Sqr((SumOfSquares / UBound(PixelValues) - (PixelValueAverage ^ 2)))) / PixelValueAverage
						if (AvgCell > 0.0f)
							StdDevCell = (sqrt ((float)SumOfSquaresCell / (float)PixelCount - AvgCell*AvgCell) ) / AvgCell;
						
						
						// 3)
						PixelCount = 0;
						MaxValueBackground = 0;
						MinValueBackground = 255;
						ValueIntegralBackground = 0;
						SumOfSquaresBackground = 0;
						StdDevBackground = 0.0f;
						AvgBackground = 0.0f;
						
						//calculate picture coordinates
						middle.setX (tmpTrackPoint.XBackground);
						middle.setY (tmpTrackPoint.YBackground);
                                                middle = tttpm->getDisplays().at (wavelength).calcTransformedCoords (middle, &tttpm->positionInformation);
						
						for (	int x = (int)(middle.x() - radius); 
								x <= (int)(middle.x() + radius); 
								x++) {
							for (	int y = (int)(middle.y() - radius);
									y <= (int)(middle.y() + radius);
									y++) {
								
								//check if point is inside the region of the cell (circle!) via Pythagoras
								dx = x - (int)middle.x();
								dy = y - (int)middle.y();
								if ((dx*dx + dy*dy) <= radius*radius) {
									PixelCount++;
									
									PixelColor = tttpm->getPictures()->readPixel (currentTimePoint, zIndex, wavelength, x, y);
									if (! intensityMatrix.isEmpty()) {
										float correction_factor = intensityMatrix.getValue (x, y);
										PixelColor = (int)((float)PixelColor * correction_factor);
									}
									
									if (PixelColor > MaxValueBackground)
										MaxValueBackground = PixelColor;
									if (PixelColor < MinValueBackground)
										MinValueBackground = PixelColor;							
									
									ValueIntegralBackground += PixelColor;
									SumOfSquaresBackground += Tools::pow (PixelColor, 2);
								}
							}
						}
						if (PixelCount > 0)
							AvgBackground = (float)ValueIntegralBackground / (float)PixelCount;
						//StdDevCell = (Sqr((SumOfSquares / UBound(PixelValues) - (PixelValueAverage ^ 2)))) / PixelValueAverage
						if (AvgBackground > 0.0f)
							StdDevBackground = (sqrt ((float)SumOfSquaresBackground / (float)PixelCount - AvgBackground*AvgBackground) ) / AvgBackground;
						
						
						// 4)
						if (! picLoaded) {
							tttpm->getPictures()->setLoading (currentTimePoint, zIndex, wavelength, false);
							tttpm->getPictures()->loadPictures (false);
						}
						
						// 5)
						//writer << ValueIntegralCell - ValueIntegralBackground << ";";
						exportLine += QString ("%1").arg (ValueIntegralCell - ValueIntegralBackground);
						exportLine += DELIMITER;
						//writer << AvgCell - AvgBackground << ";";
						exportLine += QString ("%1").arg (AvgCell - AvgBackground);
						exportLine += DELIMITER;
						//writer << StdDevCell * AvgCell << ";";
						exportLine += QString ("%1").arg (StdDevCell * AvgCell);
						exportLine += DELIMITER;
						//writer << PixelCount << ";";
						exportLine += QString ("%1").arg (PixelCount);
						exportLine += DELIMITER;
						//writer << ValueIntegralCell << ";";
						exportLine += QString ("%1").arg (ValueIntegralCell);
						exportLine += DELIMITER;
						//writer << AvgCell << ";";
						exportLine += QString ("%1").arg (AvgCell);
						exportLine += DELIMITER;
						//writer << MinValueCell << ";";
						exportLine += QString ("%1").arg (MinValueCell);
						exportLine += DELIMITER;
						//writer << MaxValueCell << ";";
						exportLine += QString ("%1").arg (MaxValueCell);
						exportLine += DELIMITER;
						//writer << ValueIntegralBackground << ";";
						exportLine += QString ("%1").arg (ValueIntegralBackground);
						exportLine += DELIMITER;
						//writer << AvgBackground << ";";
						exportLine += QString ("%1").arg (AvgBackground);
						exportLine += DELIMITER;
						//writer << MinValueBackground << ";";
						exportLine += QString ("%1").arg (MinValueBackground);
						exportLine += DELIMITER;
						//writer << MaxValueBackground << ";";
						exportLine += QString ("%1").arg (MaxValueBackground);
						exportLine += DELIMITER;
						
						valuesExported = true;
						
					}
					else {
						//writer << ";;;;;;;;;;;;";
						//exportLine += ";;;;;;;;;;;;";
						for(int i = 0; i < 12; ++i)
							exportLine += DELIMITER;
					}
				}
				else {
					//writer << ";;;;;;;;;;;;";
					//exportLine += ";;;;;;;;;;;;";
					for(int i = 0; i < 12; ++i)
						exportLine += DELIMITER;
				}
				
			}
		}		
	//}
	
	if (valuesExported)
		writer << "\r\n" << dateString << exportLine;
	
	
	//set next timepoint
	currentTimePoint++;
	
	if (currentTimePoint > LastTrackedTimePoint) {
		//there are no more exportable timepoints for this turn
		
		if (exportedTracks < exportableTracks) {
			//there are still exportable tracks - find them and write them to disk
			tmr.stop();
			writeHeader();
			tmr.start (0);
		}
		else {
			//all tracks are now written to disk
			//-> the timer is stopped
			//-> the file is closed
			tmr.stop();
			file.close();
			//QMessageBox::information (0, "tTt", "Cell export in background is now finished.\nCells were exported to " + file.name() + ".", "Ok");
			
			// Show messagebox with possibility to open folder
			//QMessageBox msgBox;
			QString message;

			// Add buttons
			//QPushButton *okButton = msgBox.addButton("Ok", QMessageBox::AcceptRole);
			//msgBox.setDefaultButton(okButton);
			//QPushButton *openFolderButton = msgBox.addButton("Open Folder", QMessageBox::ActionRole);
			
			message = QString("Cell export in background is now finished.\nCells were exported to %1.").arg(file.fileName());
			//
			//// Include message
			//msgBox.setText(message);

			//// Display messagebox
			//msgBox.exec();

			//// Open folder if desired
			//if(msgBox.clickedButton() == openFolderButton) {
			//	QString nativeMovieFolder = QDir::toNativeSeparators(QFileInfo(file).canonicalPath());
			//	QProcess::execute("Explorer.exe", QStringList(nativeMovieFolder));
			//}

			Tools::displayMessageBoxWithOpenFolder(message, "tTt", file.fileName(), true);

			// Export finished..
			TTTManager::getInst().frmTracking->resetPermanentStatusbarMessage();
		}
	}
#endif
}


