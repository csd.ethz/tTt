/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef STYLESHEETXMLPARSER_H
#define STYLESHEETXMLPARSER_H

#include "tttdata/stylesheet.h"

#include <qxml.h>
#include <QXmlDefaultHandler>

/**
	@author Bernhard Schauberger <bernhard.schauberger@campus.lmu.de>
*/

class StyleSheetXMLParser : public QXmlDefaultHandler
{
public:
	StyleSheetXMLParser (StyleSheet *_styleSheet);
	
	~StyleSheetXMLParser();
	
	/**
	 * called when the reader starts to parse, before any tags are processed
	 * @return must return true, otherwise the reader stops
	 */
	bool startDocument();
	
  	/**
  	 * called when the reader parses a begin tag
  	 * @param _namespaceURI the namespace
  	 * @param _localName the name without the namespace
  	 * @param _name the name of the tag
  	 * @param _atts 
  	 * @return must return true, otherwise the reader stops
  	 */
  	bool startElement (const QString &_namespaceURI, const QString &_localName, const QString &_name, const QXmlAttributes &_attrs);
  	
	/**
	 * called when the reader parses an end tag
	 * always called with the same arguments as the last startElement() call
  	 * @param _namespaceURI the namespace
  	 * @param _localName the name without the namespace
	 * @param _name the name of the tag
  	 * @return must return true, otherwise the reader stops
	 */
	bool endElement (const QString &_namespaceURI, const QString &_localName, const QString &_name);
	
private:
	
	///the stylesheet that should be set
	StyleSheet *styleSheet;
	
	///for the tracking key map: a key entry (representing a key on the keyboard)
	Q_INT8 key;
	
	///following bools: whether the reading is currently in the named section
	bool inStyleSheetSettings;
	bool inTreestyle;
	bool inMoviestyle;
	bool inTracking_Keys;
	bool inVarious_Settings;
	bool inStartUp_Settings;
	bool inTrackingKeyEntry;
// 	bool in;
	
};

#endif
