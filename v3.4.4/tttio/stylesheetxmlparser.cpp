/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "stylesheetxmlparser.h"

#include "xmlhandler.h"

StyleSheetXMLParser::StyleSheetXMLParser (StyleSheet *_styleSheet)
 : QXmlDefaultHandler(), styleSheet (_styleSheet)
{
}


StyleSheetXMLParser::~StyleSheetXMLParser()
{
}

bool StyleSheetXMLParser::startDocument()
{
	inStyleSheetSettings = false;
	inTreestyle = false;
	inMoviestyle = false;
	inTracking_Keys = false;
	inVarious_Settings = false;
	inTrackingKeyEntry = false;
	inStartUp_Settings = false;
// 	in = false;
	
	return true;
}

bool StyleSheetXMLParser::startElement(const QString & /*_namespaceURI*/, const QString & /*_localName*/, const QString & _name, const QXmlAttributes & _attrs)
{
	QString tmp = "";
//tmp = XMLHandler::getAttributeValue (_attrs, "");
		
	if (_name == "StyleSheetSettings")
		inStyleSheetSettings = true;
	else if (inStyleSheetSettings) {
		//not used...
		//tmp = XMLHandler::getAttributeValue (_attr, "Version");
		
		if (_name == "Treestyle")
			inTreestyle = true;
		else if (_name == "Moviestyle")
			inMoviestyle = true;
		else if (_name == "Tracking_Keys")
			inTracking_Keys = true;
		else if (_name == "Various_Settings")
			inVarious_Settings = true;
		else if (_name == "Startup_Settings")
			inStartUp_Settings = true;
		
		else if (inTreestyle) {
			if (_name == "TreeElementStyle") {
				TreeElementStyle tes;
				
				tes.setFromXMLAttributes (_attrs);
				
				styleSheet->setElement (tes);
				
			}
		}
		else if (inMoviestyle) {
			
			//not used here...
			//tmp = XMLHandler::getAttributeValue (_attrs, "WLLayout_count");
			
			if (_name == "WLLayout") {
				tmp = XMLHandler::getAttributeValue (_attrs, "Index");
				int index = tmp.toInt();
				tmp = XMLHandler::getAttributeValue (_attrs, "Value");
				int value = tmp.toInt();
				
				styleSheet->wavelengthLayout [index] = value;
			}
			else {
				
				if (_name == "PicsPerRow") {
					tmp = XMLHandler::getAttributeValue (_attrs, "value");
					styleSheet->picsPerRow = tmp.toInt();
				}
				else if (_name == "MovieExport_WavelengthVisible") {
					tmp = XMLHandler::getAttributeValue (_attrs, "value");
					styleSheet->MovieExport_WavelengthVisible = (tmp.toInt() == 1);
				}
				else if (_name == "MovieExport_TimeboxPosition") {
					tmp = XMLHandler::getAttributeValue (_attrs, "value");
					styleSheet->MovieExport_TimeboxPosition = tmp.toInt();
				}
				else if (_name == "MovieExport_TimeboxBackground") {
					tmp = XMLHandler::getAttributeValue (_attrs, "value");
					styleSheet->MovieExport_TimeboxBackground = tmp.toInt();
				}
				else if (_name == "CellColor") {
					tmp = XMLHandler::getAttributeValue (_attrs, "color");
					styleSheet->cellColor = GraphicAid::stringToColor (tmp);
				}
				else if (_name == "CellNumberColor") {
					tmp = XMLHandler::getAttributeValue (_attrs, "color");
					styleSheet->cellNumberColor = GraphicAid::stringToColor (tmp);
				}
				else if (_name == "CellCircleThickness") {
					tmp = XMLHandler::getAttributeValue (_attrs, "value");
					styleSheet->cellCircleThickness = tmp.toInt();
				}
				else if (_name == "CellPathColor") {
					tmp = XMLHandler::getAttributeValue (_attrs, "color");
					styleSheet->cellPathColor = GraphicAid::stringToColor (tmp);
				}
				else if (_name == "CellNumberFontSize") {
					tmp = XMLHandler::getAttributeValue (_attrs, "value");
					styleSheet->cellNumberFontSize = tmp.toInt();
				}
				else if (_name == "CellPathThickness") {
					tmp = XMLHandler::getAttributeValue (_attrs, "value");
					styleSheet->cellPathThickness = tmp.toInt();
				}
				else if (_name == "CellPathTimeInterval") {
					tmp = XMLHandler::getAttributeValue (_attrs, "value");
					styleSheet->cellPathTimeInterval = tmp.toInt();
				}
				else if(_name == "ShowOutOfSyncBox") {
					tmp = XMLHandler::getAttributeValue (_attrs, "value");
					styleSheet->setShowOutOfSyncBox(tmp.toInt() == 1);
				}
				else if (_name == "WavelengthBoxColor") {
					tmp = XMLHandler::getAttributeValue (_attrs, "color");
					styleSheet->wavelengthBoxColor = GraphicAid::stringToColor (tmp);
				}
				else if (_name == "WavelengthBoxFontSize") {
					tmp = XMLHandler::getAttributeValue (_attrs, "value");
					styleSheet->wavelengthBoxFontSize = tmp.toInt();
				}
			}
		}
		else if (inTracking_Keys) {
			if (_name == "Key") {
				inTrackingKeyEntry = true;
				tmp = XMLHandler::getAttributeValue (_attrs, "value");
				key = tmp.toInt();
			}
			else if (inTrackingKeyEntry) {
				if (_name == "TreeElementStyle") {
					TreeElementStyle tes;
					tes.setFromXMLAttributes (_attrs);
					styleSheet->setAssociatedElement (key, tes);
				}
			}
		}
		else if (inVarious_Settings) {
			if (_name == "AddUserSignToTTTFiles") {
				tmp = XMLHandler::getAttributeValue (_attrs, "value");
				styleSheet->addUserSignToTTTFiles = (tmp.toInt() == 1);
			}
			else if (_name == "UseNewTTTFileFolderStructure") {
				tmp = XMLHandler::getAttributeValue (_attrs, "value");
				styleSheet->useNewTTTFileFolderStructure = (tmp.toInt() == 1);
			}
			else if (_name == "ExportWavelength") {
				tmp = XMLHandler::getAttributeValue (_attrs, "value");
				styleSheet->exportWavelength = tmp.toInt();
			}
			else if (_name == "PicturesToLoadBeforeStop") {
				tmp = XMLHandler::getAttributeValue (_attrs, "value");
				styleSheet->picturesToLoadBeforeStop = tmp.toInt();
			}
			else if (_name == "PicturesToLoadAfterStop") {
				tmp = XMLHandler::getAttributeValue (_attrs, "value");
				styleSheet->picturesToLoadAfterStop = tmp.toInt();
			}
			else if (_name == "LoadPicturesAsynchronous") {
				tmp = XMLHandler::getAttributeValue (_attrs, "value");
				styleSheet->loadPicturesAsynchronous = tmp.toInt();
			}
			else if (_name == "ShowTreeCountInPositionLayout") {
				tmp = XMLHandler::getAttributeValue (_attrs, "value");
				styleSheet->showTreeCountInPositionLayout = tmp.toInt();
			}
			else if (_name == "UseColorsForAllTracks") {
				tmp = XMLHandler::getAttributeValue (_attrs, "value");
				styleSheet->useColorsForAllTracks = tmp.toInt();
			}
			else if (_name == "NumPicsToLoadByNextPosItem") {
				tmp = XMLHandler::getAttributeValue (_attrs, "value");
				styleSheet->numPicsToLoadByNextPosItem = tmp.toInt();
			}
			else if (_name == "TakeOverDisplaySettingsNextPosItem") {
				tmp = XMLHandler::getAttributeValue (_attrs, "value");
				styleSheet->takeOverDisplaySettingsNextPosItem = (tmp.toInt() == 1);
			}
			else if (_name == "LastTTTChangelogVersion") {
				styleSheet->lastTTTChangelogVersion = XMLHandler::getAttributeValue (_attrs, "value");
			}
		}
		else if (inStartUp_Settings) {
			if (_name == "StartUpMoveToFirstFluor") {
				tmp = XMLHandler::getAttributeValue (_attrs, "value");
				styleSheet->startUpMoveToFirstFluor = (tmp.toInt() == 1);
			}
			if (_name == "StartUpBgCircleVisible") {
				tmp = XMLHandler::getAttributeValue (_attrs, "value");
				styleSheet->startUpBgCircleVisible = (tmp.toInt() == 1);
			}
			if (_name == "StartUpFullPicture") {
				tmp = XMLHandler::getAttributeValue (_attrs, "value");
				styleSheet->startUpFullPicture = (tmp.toInt() == 1);
			}
			if (_name == "StartUpAlternateNormalBgTracking") {
				tmp = XMLHandler::getAttributeValue (_attrs, "value");
				styleSheet->startUpAlternateNormalBgTracking = (tmp.toInt() == 1);
			}
			
		}
	}
	
	return true;
}

bool StyleSheetXMLParser::endElement(const QString & /*_namespaceURI*/, const QString & /*_localName*/, const QString & _name)
{
	if (_name == "StyleSheetSettings")
		inStyleSheetSettings = false;
	if (_name == "Treestyle")
		inTreestyle = false;
	if (_name == "Moviestyle")
		inMoviestyle = false;
	if (_name == "Tracking_Keys")
		inTracking_Keys = false;
	if (_name == "Various_Settings")
		inVarious_Settings = false;
	if (_name == "Key")
		inTrackingKeyEntry = false;
	if (_name == "Startup_Settings")
		inStartUp_Settings = false;
/*	if (_name == "")
		in = false;*/
	
	
		
	return true;
}


