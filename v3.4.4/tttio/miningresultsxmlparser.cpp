/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "miningresultsxmlparser.h"

#include "xmlhandler.h"

MiningResultsXMLParser::MiningResultsXMLParser (MiningResults *_miningResults)
 : QXmlDefaultHandler(), miningResults (_miningResults)
{
}

MiningResultsXMLParser::~MiningResultsXMLParser()
{
}

bool MiningResultsXMLParser::startDocument()
{
	//initialize all tag variables
/*	inTATSetting = false;
	inPositionData = false;
	inWavelengthData = false;*/
	
	inTypeExplanation = false;
	inTreeIDs = false;
	
	return true;
}

bool MiningResultsXMLParser::startElement (const QString &/*_namespaceURI*/, const QString &/*_localName*/, const QString &_name, const QXmlAttributes &_attrs)
{
	if ((! inTypeExplanation) && _name == "TypeExplanation") {
		inTypeExplanation = true;
	}
	else if (inTypeExplanation) {
		
		if (_name == "TreeIDs") {
			inTreeIDs = true;
		}
		else if (inTreeIDs) {
			//read assignments of tree filenames to tree numbers
			
			if (_name == "Tree") {
				QString treeID = XMLHandler::getAttributeValue (_attrs, "id");
				currentTreeID = treeID.toInt();
			}
			else if (_name == "Filename") {
				QString filename = XMLHandler::getAttributeValue (_attrs, "path");
				filename = filename.mid (filename.findRev ('/') + 1);
				
				//set map entry
				miningResults->filename_to_index [filename] = currentTreeID;
				miningResults->index_to_filename [currentTreeID] = filename;
			}
		}
	}
	
	return true;
}


bool MiningResultsXMLParser::endElement (const QString &/*_namespaceURI*/, const QString &/*_localName*/, const QString &_name)
{
	if (inTypeExplanation && _name == "TypeExplanation") {
		inTypeExplanation = false;
	}
	else if (inTreeIDs && _name == "TreeIDs") {
		inTreeIDs = false;
	}
	
	return true;
}

