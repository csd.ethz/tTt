/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef EXPORTHANDLER_H
#define EXPORTHANDLER_H

// Project includes
#include "tttbackend/matrix.h"
#include "tttdata/track.h"

// Qt includes
#include <QString>
#include <QTimer>
#include <QFile>
#include <QTextStream>

// Qt3 includes
#include <Q3IntDict>

// Forward declarations
class Tree;
template <class T> class QVector;
class TTTPositionManager;


///the number of exported cells after which a "line break" should be inserted
///necessary because Excel/SpreadSheet can only have columns up to "IV", which means 22 cells, all above are truncated
///so after this number of cells, a new section (with the same headers) is started some rows below
///if == 0, no lines are wrapped
const int BREAKING_CELLNUMBER = 20;

const QString DELIMITER = ",";

// /the wavelength on which the cell and its background are measured
//now in style sheet!
//const int EXPORT_WAVELENGTH = 1;

/**
@author Bernhard
	This class handles the export of cells with different data (e.g. fluorescence, time differences)
	It also provides the methods for writing a QValueVector<QString> to a file.
*/

class ExportHandler : public QObject {

Q_OBJECT

public:
	/**
	 * constructs default export handler
	 */
	ExportHandler();
	
	~ExportHandler();
	
	/**
	 * creates the *.tttexport file with the fluorescence data of the cells
	 * @param _fileName the filename of the export file
	 * @ param _picArray the picture array containing the pictures that need to be loaded for pixel access
	 * @param _tree the tree object containing the tracks to be exported
	 * @param _zIndex the zIndex for export
	 * @param _wavelength the wavelength for which the export should be done
	 * @param _intensityMatrixFilename the filename of the intensity correction matrix, if available
	 * @return success (file was writable)
	 * 
	 * currently, the format of the file is fixed and was once determined by Michael's spreadsheet macros
	 * @todo future plan: the user can configure the export format via a table, selecting only the necessary attributes
	 */
	bool exportFluorescence (const QString &_fileName, /*PictureArray *_picArray,*/ Tree *_tree, int _zIndex, int _wavelength, const QString _intensityMatrixFilename = "");
	
	/**
	 * creates a new file and writes the provided string list linewise into it
	 * just calls exportDataset(), was introduced to have a static method
	 * @param _fileName the filename of the export file
	 * @param _data the string array that should be written (a \n is inserted after each item)
	 * @return success (file was writable) 
	 */
        static bool exportData (const QString &_fileName, const QVector<QString> &_data);
	
private slots:
	
	/**
	 * writes a single timepoint to the internal QTextStream writer
	 */
	void writeTimePoint();
	
private:
	
	// /the local pointers to the necessary attributes
	///the current picture array
	///set anew if the position changed
	//PictureArray *pictures;
	//Tree *tree;
	
	///the tracks to be exported (tracked and background tracked)
	Q3IntDict<Track> trackList;
	
	///timer is necessary for loading pictures asynchronous
	QTimer tmr;
	
	///the file associated with the export
	QFile file;
	
	///the text stream for output
        QTextStream writer;
	
	///an internal tree instance, is not changed
	Tree *tree;
	
	///the wavelength which should be exported
	int wavelength;

	///the z-index which should be exported
	int zIndex;
	
	///the current timepoint to be written
	int currentTimePoint;
	
	///the highest track number currently exported
	///tracks are not written at once, but in sections of Breaking_CellNumber cells to avoid clipping in Excel
	int Max_Current_TrackNumber;
	
	///the number of tracks that are eventually exported (is calculated BEFORE the actual export)
	int exportableTracks;
	
	///the number of already exported tracks (always lower or equal to exportableTracks)
	int exportedTracks;
	
	///the last & first tracked timepoint in the complete tree
	int LastTrackedTimePoint;
	int FirstTrackedTimePoint;
	
	///the intensity correction matrix
	Matrix<float> intensityMatrix;
	
	///the current position pointer
	///if a trackpoint is located outside, it is updated
	///initialized with the base position
	TTTPositionManager *tttpm;


//private methods
	
	///writes the header line for fluorescence export and finds the next round of tracks to be exported
	void writeHeader();
	
	
	/**
	 * creates a new file and writes the provided string list linewise into it
	 * @param _fileName the filename of the export file
	 * @param _data the string array that should be written (a \n is inserted after each item)
	 * @return success (file was writable) 
	 */
        bool exportDataset (const QString &_fileName, const QVector<QString> &_data);
	
};

#endif
