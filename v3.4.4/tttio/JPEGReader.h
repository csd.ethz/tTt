/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
@author Oliver Hilsenbeck
*/

//////////////////////////////////////////////////////////////////////////
// Read grayscale images using libjpeg-turbo directly into a QImage buffer.
// This is about 3 times faster than using QImage(filename), which relies
// on the normal libjpeg (at least at the time this was written).
// It would be possible to add support for reading of jpegs that are not
// grayscale, but that is not needed for ttt.
//
// How to compile libjpeg-turbo on windows:
//		-> update 30/03/2011: libjpeg-turbo now contains windows precompiled files
//							  to use them, set /NODEFAULTLIB:libcmt.lib linker option
// - unpack files
// - move ./win/jconfig.h to .
// - install nasm and make sure nasm.exe path is in PATH variable
// - install gnu make for windows
// - edit ./win/Makerules to use shared c-library (MD or MDd) (e.g. replace 
//   LIBCSTATIC at bottom with LIBCDLL)
//   (or shared library if this is done in ttt. Not sure if this is necessary, 
//	 but avoids linker warning)
//		-> update 30/03/2011: or add /NODEFAULTLIB:libcmt.lib to linker options
// - goto . and execute "make.exe -f win/Makefile"
// - do not forget to add ./windows/jpeg-static.lib to linker input files and  
//   . to include directories in project options
//
// The code is based on the example function readJPEGFile() provided by libjpeg-turbo
// in example.c, on the function read_jpeg_image in qjpeghandler.cpp and on
// ensureValidImage() in qjpeghandler.cpp.
//////////////////////////////////////////////////////////////////////////

#ifndef JPEGReader_h__
#define JPEGReader_h__

#include <QImage>


//////////////////////////////////////////////////////////////////////////
// Error code declarations
//////////////////////////////////////////////////////////////////////////
enum RJERROR {
	RJERROR_NO_ERROR,
	RJERROR_CANNOT_OPEN_FILE,
	RJERROR_JPEG_ERROR,
	RJERROR_FILE_NOT_GRAYSCALE,
	RJERROR_READ_FAILED,
	RJERROR_QIMAGE_ERROR
};

/**
 * Read jpeg file specified by filename and return a QImage object.
 * This function ONLY supports gray-scale images (component_count must
 * be 1). 
 * @param filename Name of the jpeg file
 * @param errorType Error information will be stored in this variable 
 * (see RJERROR_* declarations)
 * @return The created QImage object. Return value is undefined if 
 * function fails (not necessarily a null-image).
 */
QImage readJPEGGrayscaleFile(const QString& filename, RJERROR& errorType);


#endif // JPEGReader_h__