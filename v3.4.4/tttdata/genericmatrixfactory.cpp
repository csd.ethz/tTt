/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "genericmatrixfactory.h"

FloatMatrix GenericMatrixFactory::loadFloatMatrix( const QString& _fileName, bool _horizontalHeader /*= false*/, bool _verticalHeader /*= false*/, QChar _delim/*=','*/, QChar _textDelim/* = '"'*/ )
{
	// Redirect to generic function
	return loadFromFile<float>(_fileName, FloatConverter(), _horizontalHeader, _verticalHeader, _delim, _textDelim);
}

DoubleMatrix GenericMatrixFactory::loadDoubleMatrix( const QString& _fileName, bool _horizontalHeader /*= false*/, bool _verticalHeader /*= false*/, QChar _delim/*=','*/, QChar _textDelim/* = '"'*/ )
{
	// Redirect to generic function
	return loadFromFile<double>(_fileName, DoubleConverter(), _horizontalHeader, _verticalHeader, _delim, _textDelim);
}

//StringMatrix GenericMatrixFactory::loadStringMatrix( const QString& _fileName, bool _horizontalHeader /*= false*/, bool _verticalHeader /*= false*/, QChar _delim/*=','*/ )
//{
//	// Redirect to generic function
//	return loadFromFile<QString>(_fileName, StringConverter(), _horizontalHeader, _verticalHeader, _delim);
//}

QString GenericMatrixFactory::removeTextDelimiter(QString _string, QChar &_textDelim) 
{
	// Remove the delimiter at the begining
	if( _string[0] == _textDelim ) 
	{
		_string = _string.mid(1);
	}

	// Remove the delimiter at the end
	if(_string[_string.length()-1] == _textDelim)
	{
		_string = _string.left( _string.length()-1 );
	}

	return _string;
}