/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CELLSANDCONDITIONS_H
#define CELLSANDCONDITIONS_H

#include <qstring.h>
#include <q3valuevector.h>


/**
 * @author Bernhard
 * contains the cell types written in the tat file
 * for explanation of the values, please refer to the TAT documentation
*/
struct CNC_CellType {
	bool PrimaryCell;
	QString CellName;
	QString Species;
	QString Sex;
	QString Organ;
	
	///the age of the cell
	///for primary cells (PrimaryCell == true)
	///- until 04/2010: age is months, thus multiply by 30
	///- from 04/2010 on: age is in days
	///for all other cells:
	///- age is the number of sorting passages
	int Age;
	
	QString Purification;
	QString Comment;
	
//methods
	
	CNC_CellType();
	
	void reset();
	
	const QString toString() const;
};


/**
 * @author Bernhard
 * contains the cells and condition information written in the tat file
 * for explanation of the values, please refer to the TAT documentation
*/
struct CellsAndConditions {
	int NumberOfCellTypes;
	int Temperature;
	int CO2_Percentage;
	QString Flask;
	QString Medium;
	QString Incubators;
	QString Serum;
	int SerumPercentage;
	QString MediumAdditions;
	QString ReflectedLightSource;
	int HalogenVoltage;
	int NeutralFilter;
	QString IlluminationComment;
	int AgeFluorescentLamp;
	int HoursBeforeMovieStart;
	
	///the cell types (of arbitrary number)
	uint cellTypeCount;
	//Q3ValueVector<CNC_CellType> cellTypes;
	QVector<CNC_CellType> cellTypes;
	
	
//methods
	
	CellsAndConditions();
	
	/**
	 * adds a new cell type to the list
	 * @param _cellType 
	 */
	void addCellType (const CNC_CellType &_cellType);
		
	/**
	 * assignment operator
	 */
	CellsAndConditions& operator= (const CellsAndConditions& _cacs);
	
	/**
	 * @return the cells and conditions as printable string
	 */
	const QString toString() const;
};

#endif
