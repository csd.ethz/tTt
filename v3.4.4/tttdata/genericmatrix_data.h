/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef genericmatrix_data_h__
#define genericmatrix_data_h__

// STL includes
#include <vector>

// Qt
#include <QString>
#include <QDebug>
#include <QVector>

// Project includes
#include "tttbackend/exception.h"


/**
 * @author Oliver Hilsenbeck
 *
 * Generic matrix data class. Used for implementing matrices with shared memory access.
 * See genericmatrix.h for more information.
 */
template <typename T>
class matrix_data {
public:
	// Row type
	typedef std::vector<T> t_row;

	// Constructor
	// Creates matrix with of given dimensions without header columns
	matrix_data(unsigned int _rows, unsigned int _cols, bool _horizontalHeader, bool _verticalHeader, const T& _val);

	// Add reference
	matrix_data* addRef();

	// Remove reference
	void removeRef();

	// Access element
	T& elem(unsigned int _row, unsigned int _col);

	// Access header
	QString& horizontalHeader(int _col);
	QString& verticalHeader(int _row);

	// Deep copy, creates new matrix_data object giving ownership to the caller, decrements refCount
	matrix_data<T> *deepCopy();

	// Get reference count
	unsigned int getRefCount() const;

	/**
	 * Get number of rows
	 * @return the number of rows
	 */
	unsigned int numRows() const;

	/**
	 * Get number of columns
	 * @return the number of columns
	 */
	unsigned int numColumns() const;

	/**
	 * Extract row. Runs in O(n)
	 * @param _row desired row
	 * @return QVector (because of implicit memory sharing) containing specified row data
	 */
	QVector<T> getRow(unsigned int _row) const;

	/**
	 * Extract column. Runs in O(n)
	 * @param _col desired column
	 * @return QVector (because of implicit memory sharing) containing specified column data
	 */
	QVector<T> getColumn(unsigned int _col) const;

	/**
	 * Replaces all occurences of the _search value with the _replace value
	 * @param _search the value which is replaced
	 * @param _replace the new value
	 */
	void replaceValue(T _search, T _replace);

private:
	// Destructor, does nothing
	~matrix_data();

	// Constructor for creating a deep copy
	matrix_data(const matrix_data<T>& _otherData);

	// Data
	std::vector<t_row> data;

	// Headers
	std::vector<QString> *d_verticalHeader;
	std::vector<QString> *d_horizontalHeader;

	// Reference counter
	unsigned int refCount;
};


template <typename T>
matrix_data<T>::matrix_data( const matrix_data<T>& _otherData ) : 
		data(_otherData.data)		// Copy matrix
{
	qDebug() << "copy-constructor called.";

	// Init ref counter
	refCount = 1;

	// Copy horizontal header
	if(_otherData.d_horizontalHeader) 
		d_horizontalHeader = new std::vector<QString>(*_otherData.d_horizontalHeader);	// Copy
	else
		d_horizontalHeader = 0;

	// Copy vertical header
	if(_otherData.d_verticalHeader) 
		d_verticalHeader = new std::vector<QString>(*_otherData.d_verticalHeader);	// Copy
	else
		d_verticalHeader = 0;
}

template <typename T>
matrix_data<T>::matrix_data( unsigned int _rows, unsigned int _cols,  bool _horizontalHeader, bool _verticalHeader, const T& _val )
{
	qDebug() << "std constructor called. _rows: " << _rows << " _cols: " << _cols << " hHeader: " << _horizontalHeader << " vHeader: " << _verticalHeader;

	// Init ref counter
	refCount = 1;

	// Init horizontal header
	if(_horizontalHeader)
		d_horizontalHeader = new std::vector<QString>(_cols);
	else
		d_horizontalHeader = 0;

	// Init vertical header
	if(_verticalHeader)
		d_verticalHeader = new std::vector<QString>(_rows);
	else
		d_verticalHeader = 0;

	// Init data matrix
	data.resize(_rows);
	for(unsigned int i = 0; i < _rows; ++i)
		data[i].resize(_cols, _val);
}

template <typename T>
matrix_data<T>::~matrix_data()
{
	qDebug() << "destructor called.";

	// Cleanup
	if(d_horizontalHeader)
		delete d_horizontalHeader;
	if(d_verticalHeader)
		delete d_verticalHeader;
}

template <typename T>
unsigned int matrix_data<T>::getRefCount() const
{
	return refCount;
}

template <typename T>
matrix_data<T> * matrix_data<T>::deepCopy()
{
	qDebug() << "deepCopy called. refCount: " << refCount;
	if(refCount <= 1) 
		throw ttt_Exception("Internal Error: matrix_data<T>::deepCopy() - refCount error.");

	--refCount;
	return new matrix_data<T>(*this);
}

template <typename T>
matrix_data<T>* matrix_data<T>::addRef()
{
	qDebug() << "addRef called.";
	++refCount;
	return this;
}

template <typename T>
void matrix_data<T>::removeRef()
{
	qDebug() << "removeRef called.";
	if(--refCount == 0)
		delete this;
}

template <typename T>
T& matrix_data<T>::elem(unsigned int _row, unsigned int _col)
{
	if(_row >= data.size() || _col >= data[0].size())
		throw ttt_Exception("Internal Error: matrix_data<T>::elem(unsigned int _row, unsigned int _col) - index out of bounds.");

	return data[_row][_col];
}

template <typename T>
QString& matrix_data<T>::horizontalHeader( int _col )
{
	if(!d_horizontalHeader || _col >= d_horizontalHeader->size())
		throw ttt_Exception("Internal error: matrix_data<T>::horizontalHeader( int _col ) - no header or index out of bounds.");

	return (*d_horizontalHeader)[_col];
}

template <typename T>
QString& matrix_data<T>::verticalHeader( int _row )
{
	if(!d_verticalHeader || _row >= d_verticalHeader->size())
		throw ttt_Exception("Internal error: matrix_data<T>::verticalHeader( int _row ) - no header or index out of bounds.");

	return (*d_verticalHeader)[_row];
}

template <typename T>
unsigned int matrix_data<T>::numColumns() const
{
	if(data.size()>0)
		return data[0].size();

	return 0;
}

template <typename T>
unsigned int matrix_data<T>::numRows() const
{
	return data.size();
}

template <typename T>
QVector<T> matrix_data<T>::getColumn( unsigned int _col ) const
{
	// Check index
	if(data.size() == 0 || _col >= data[0].size())
		throw ttt_Exception("matrix_data<T>::getColumn( unsigned int _col ): Index out of bounds.");

	// We need to copy the column manually (this requires T to have a default constructor)
	QVector<T> ret(data.size());
	for(unsigned int i = 0; i < data.size(); ++i)
		ret[i] = data[i][_col];

	return ret;
}

template <typename T>
QVector<T> matrix_data<T>::getRow( unsigned int _row ) const
{
	// Check index
	if(_row >= data.size())
		throw Exception("matrix_data<T>::getRow( unsigned int _row ): Index out of bounds.");

	// Create QVector
	return QVector<T>::fromStdVector(data[_row]);
}

template <typename T>
void matrix_data<T>::replaceValue(T _search, T _replace)
{
	// Go over all cells
	for(int row = 0; row < numRows(); row++)
	{
		for(int col = 0; col < numColumns(); col++)
		{
			// Get the value
			T& value = elem(row, col);

			// if the value is _search replace it with _replace
			if(value == _search)
				value = _replace;
		}
	}

}

#endif // genericmatrix_data_h__