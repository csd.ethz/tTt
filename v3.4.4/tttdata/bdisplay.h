/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef BDISPLAY_H
#define BDISPLAY_H

//#include "fileinfoarray.h"
#include "tttbackend/pictureindex.h"
#include "tttdata/positioninformation.h"

#include <QColor>
#include <QPointF>
#include <QRect>
#include <QSize>

///contains the number of anchor points in the gamma function which the user can shift by mouse
///the more points are available, the more exact the interpolation is
const int GAMMA_ANCHOR_POINTS = 3;


/**
        BDisplay: @author Bernhard

        This class contains all attributes that are necessary for adjusting the graphical
        display of the pictures and to manage the internal stuff, concerning size and
        stretching, gamma correction, ...

        For each wavelength in each (loaded) position exactly one BDisplay object exists.
*/


struct BDisplay
{
        /**
         * constructs a null display setting (100% zoom, no regions set)
         */
        BDisplay ()
                : DataChanged (true)
                {
                        reset();	//init attributes
                }

        /**
         * assignment operator, simply copying all values
         */
        BDisplay& operator= (const BDisplay &_bdisp);


        /**
         * sets the dimensions of the complete picture in pixel
         * @param _width the width of the picture in pixel
         * @param _height the height of the picture in pixel
         */
        void setCompleteSize (int _width, int _height)
                { CompleteSize = QSize (_width, _height);}

        /**
         * sets the dimension of the loaded region of the picture
         * at the moment this dimension cannot be altered once it was set while loading
         * @param _left the left edge
         * @param _top the top edge
         * @param _width the width of the region
         * @param _height the height of the region
         */
        void setLoadedRegion (int _left, int _top, int _width, int _height)
                { LoadedRegion = QRect (_left, _top, _width, _height);}

        /**
         * @return true iff the loaded region was already set
         */
        bool LoadedRegionSet() const
                {return (LoadedRegion.isValid());}

        /**
         * returns the absolute coordinates within the complete picture (eliminates loading and displayed region and zoom)
         * @param _transformedCoords the screen coordinates (depending on movie shift, loaded region, ...)
         * @param _positionInformation the position information object (necessary for the position location within the experiment)
         // * @param _noDisplayStuff whether the zoom and screen picture offset should be regarded (false, default) or not (true)
         * old@return the logical coordinates (now fitting to the image's coordinate system)
         * @return the experiment relative coordinates in micrometer
         *
         * new: these two methods only care about the picture translation (left/top corner), not at all about the display stuff
         */
        QPointF calcAbsCoords (QPointF _transformedCoords, PositionInformation *_positionInformation) const;

        /**
         * returns the relative coordinates within the current display (i.e. within the current position) (transforms the point with loaded and displayed region and zoom)
         * old@param _absCoords the logical coordinates (in the current picture coordinate system)
         * @param _absCoords the experiment relative coordinates in micrometer
         * @param _positionInformation the position information object (necessary for the position location within the experiment)
         // * @param _withoutDisplayShift whether the coordinates should NOT be shifted additionally by the bounds of the displayed region (default = false)
         * @param _inMicrometer whether the coordinates should be returned in micrometer (works correct only in combination with _withoutDisplayShift=true)
         * @param _oldInvertedSystemReading whether the old (wrong!) inverted system coordinates should be read (used only for converting the incorrect files)
         * @return the screen coordinates, regarding the current movie player settings (shift, zoom factor, displayed region, ...)
         *
         * new: these two methods only care about the picture translation (left/top corner), not at all about the display stuff
         */
        QPointF calcTransformedCoords (QPointF _absCoords, PositionInformation *_positionInformation, bool _inMicrometer = false, bool _oldInvertedSystemReading = false) const;

        /**
         * resets all values (graphical corrections), but leaves the currently displayed region and zoom untouched
         */
        void reset();

        /**
         * returns the difference between black and white point
         * @param _min the minimum return value
         * @return WhitePoint - BlackPoint in pixel
         */
        int BWDiff (int _min = 1) const
                {return qMax (_min, WhitePoint - BlackPoint);}

        /**
         * fits the current gamma function to fit into black/white point and the anchor values
         */
        void adjustGammaValues();

        /* *
         * calculates the center point of the currently displayed region
         * @return a QPointF objects that holds the center of the current display
         */
//        QPointF calcCenter() const;

        /**
         * @param _inMicrometer whether the center should be given in micrometer (default = false)
         * @return the center point of the complete picture
         */
        QPointF calcPictureCenter (bool _inMicrometer = false) const;

        /**
         * copies the values of all attributes that the user can change in the movie window (e.g. bw points)
         *  in the provided _destBD
         * the difference from the normal assignment operator is that some critical components are not copied (LoadingRegion, ...)
         * @param _destBD the BDisplay object that should take the values of THIS
         */
        void copyDisplayVariables (BDisplay &_destBD);


//attributes

        ///the black and white point
        int BlackPoint;
        int WhitePoint;

        ///the contrast, stored as Contrast:100
        ///at the moment not really working
        int Contrast;

        ///the brightness, stored as relative value
        ///for the calculation/transformation scheme see PictureContainer::adjustImage()
        float Brightness;

        ///the alpha (transparency) value for the picture
        ///takes values from 0 (completely transparent) to 255 (not transparent)
        int Alpha;

        ///the values of the user set gamma function
        ///-> just a simple function, consisting of [GAMMA_ANCHOR_POINTS] separate linear functions projecting an input color on an output color
		static const int NUM_GAMMA_VALUES = 256;
        int GammaValues [NUM_GAMMA_VALUES];

        ///the coord values for the anchor points
		static const int NUM_GAMMA_ANCHOR_COORDS = 2;
        int GammaAnchor [GAMMA_ANCHOR_POINTS][NUM_GAMMA_ANCHOR_COORDS];

        ///the size of the complete picture in pixel
        QSize CompleteSize;

        ///the region that is currently (selected to be) loaded
        ///in memory either only this region or the complete picture exists, depending on the setting of KEEPCOMPLETEIMAGES in PictureArray.h
        ///always in pixel!
        QRect LoadedRegion;

        ///whether the settings were changed
        bool DataChanged;

        ///the color channel (only red,green or blue at the moment) which should be
        ///displayed (all colors are shown only in this channel)
        ///in order to have better contrast
        ///see also TransColorIndex
        QColor TransColor;

        ///current politics:
        ///the transcolor just specifies the channel which should be extracted from grayscale
        ///-> between -1 and 2, where
        ///-1 == none
        ///0 == red
        ///1 == green
        ///2 == blue
        int TransColorIndex;

        ///the value from which all points above are translated to TransColor
        ///@deprecated currently not used
        //int TransColorThreshold;
};


#endif // BDISPLAY_H
