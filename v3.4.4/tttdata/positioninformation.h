/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef POSITIONINFORMATION_H
#define POSITIONINFORMATION_H

// Project
#include "tttgui/rectbox.h"
#include "systeminfo.h"

// Qt
#include <QRect>
#include <QSize>

// Stl
#include <vector>

/**
	@author Bernhard
	
	This class contains information about each position (microscope window), including size and 
	 absolute position (given the complete experiment)
	
	The unit of the measures is micrometer or pixel, depending on when the experiment was made.
*/

class PositionInformation{

public:
	PositionInformation();
	
	~PositionInformation();
	
	
	void setMeasures (int _left, int _top)
	{	left = _left;
		top = _top;
		coordinates_set = true;
	}
		 
	
	void setIndex (const QString &_index)
		{index = _index;}
	
	void setPath (const QString &_path)
		{folderPath = _path;}
	
	int getLeft() const
		{return left;}
	
	int getTop() const
		{return top;}
	
	const QString& getIndex() const
		{return index;}
	
	const QString& getPath() const
		{return folderPath;}
	
	/**
	 * @return whether the coordinates of this position were set (while reading the TAT file)
	 */
	bool coordinatesSet() const;
	
	
	void setComment (const QString &_comment)
		{comment = _comment;}
	
	QString getComment() const
		{return comment;}
	
	// ------ Konstantin ----------
	void setIs_New_Position(bool _set)
	{
		is_new_position = _set;
	}
	// ------ Konstantin ----------
	bool is_New_Position()
	{
		return is_new_position;
	}
	
	RectBox getLoadingRegion() const
		{return LoadingRegion;}
	
	QRect getImageRects (int _wavelength) const
		{return ImageRect [_wavelength];}

	void setImageRect(int _wl, const QRect& _rect) {
		if(_wl <= MAX_WAVE_LENGTH) {
			ImageRect[_wl] = _rect;
			availableImageRects[_wl] = true;
		}
	}

	/**
	 * @param _wl the wavelength number
	 * @return true if a ImageRect has been set for _wl with setImageRect
	 */
	bool isImageRectSet(int _wl) {
		if(_wl <= MAX_WAVE_LENGTH)
			return availableImageRects[_wl];
		return false;
	}

	/**
	 * Get width in pixels.
	 */
	int getWidth(int wl = 0) const {
		return ImageRect[wl].width();
	}
	
	/**
	 * Get height in pixels.
	 */
	int getHeight(int wl = 0) const {
		return ImageRect[wl].height();
	}

private:
	
	// /the coordinates of the position window (in relation to the complete experiment)
	//QRect dimension;
	
	///the location of this position (in micrometers)
	long left;
	long top;
	
	///whether the coordinates (left/top) were set
	///if this is not the case after the TAT file was read, this position exists as a folder, but not in the TAT file
	/// which implies that the position is not desired and can be deleted
	bool coordinates_set;
	
	///contains the unique position identifier within the experiment
	///e.g. "001"
	QString index;
	
	///the complete path to the images of this position
	QString folderPath;
	
	///the comment that the experimentator entered in TAT for this position
	QString comment;
	
	///attributes necessary for loading pictures (used by TTTPositionLayout, not here)
	
	///contains the sizes of the images for each wavelength in pixel
	QRect ImageRect [MAX_WAVE_LENGTH + 1];

	// Vector describing for which wavelength ImageRect contains a valid value
	std::vector<bool> availableImageRects;

	
	///a rectangular box, provided for frmRegion
	///containing the region the user selects to be loaded
	RectBox LoadingRegion;
	
	// ------ Konstantin ----------	
	///whether the new experiment relative coordinates should be used (only if the tat-xml file is present) (=> true)
	///or not (=> false)
	///set in TTTStatistics when the tat-xml file is read (if available => true)
	bool is_new_position;
	
	
	
	friend class TTTPositionLayout;
};

inline bool PositionInformation::coordinatesSet() const
{
	return coordinates_set;
}


#endif
