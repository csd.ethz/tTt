/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "bdisplay.h"

#include "tttbackend/tttmanager.h"
#include "tttdata/tatinformation.h"

BDisplay& BDisplay::operator= (const BDisplay &_bdisp)
{
        BlackPoint = _bdisp.BlackPoint;
        WhitePoint = _bdisp.WhitePoint;
        Contrast = _bdisp.Contrast;
        Brightness = _bdisp.Brightness;
        Alpha = _bdisp.Alpha;

        for (int i = 0; i < 256; i++)
                GammaValues [i] = _bdisp.GammaValues [i];

        for (int i = 0; i < GAMMA_ANCHOR_POINTS; i++) {
                GammaAnchor [i][0] = _bdisp.GammaAnchor [i][0];
                GammaAnchor [i][1] = _bdisp.GammaAnchor [i][1];
        }

        CompleteSize = _bdisp.CompleteSize;
        LoadedRegion = _bdisp.LoadedRegion;
        TransColor = _bdisp.TransColor;
        TransColorIndex = _bdisp.TransColorIndex;
//        TransColorThreshold = _bdisp.TransColorThreshold;

        DataChanged = _bdisp.DataChanged;

        return *this;
}

void BDisplay::reset()
{
        BlackPoint = 0;
        WhitePoint = 255;
        Alpha = 255;
        Brightness = 1.0f;
        Contrast = 100;
        for (int i = 0; i < 256; i++)
                GammaValues [i] = i;
        for (int i = 0; i < GAMMA_ANCHOR_POINTS; i++) {
                GammaAnchor [i][1] = GammaAnchor [i][0] = (i + 1) * 255 / (GAMMA_ANCHOR_POINTS + 1);
        }
        TransColor = Qt::white;
        TransColorIndex = -1;
//                        TransColorThreshold = 255;
}



void BDisplay::copyDisplayVariables (BDisplay &_destBD)
{
        _destBD.BlackPoint = BlackPoint;
        _destBD.WhitePoint = WhitePoint;
        _destBD.Contrast = Contrast;
        _destBD.Brightness = Brightness;
        _destBD.Alpha = _destBD.Alpha;

        for (int i = 0; i < 256; i++)
                _destBD.GammaValues [i] = GammaValues [i];

        for (int i = 0; i < GAMMA_ANCHOR_POINTS; i++) {
                _destBD.GammaAnchor [i][0] = GammaAnchor [i][0];
                _destBD.GammaAnchor [i][1] = GammaAnchor [i][1];
        }

//	_destBD.ZoomFactor = ZoomFactor;
//	_destBD.ZoomX = ZoomX;
//	_destBD.ZoomY = ZoomY;
}

QPointF BDisplay::calcAbsCoords (QPointF _transformedCoords, PositionInformation *_positionInformation) const
{

        //BS 2010/07/22: new graphics system -> display settings (zoom, region, ...) not anymore necessary

        if (TTTManager::getInst().USE_NEW_POSITIONS()) {
                if (! _positionInformation)
                        return QPointF (-1, -1);

                //use new coordinate scheme
                //unit is micormeter and the coordinates of all tracks are global in the experiment

                float x = 0, y = 0;

                float left = (float)_positionInformation->getLeft();
                float top = (float)_positionInformation->getTop();


                //two steps:
                //1) calculate the coordinates in pixel as before (the coordinate origin is the topleft corner of the position)
                //2) transform the pixel coordinates to micrometers with the information about the position location

//		if (! _noDisplayStuff) {
//			//1)
//			switch (ZoomFactor) {
//				case 0:		//full picture => loaded region == displayed region
//					if (ZoomX * ZoomY) {
//						x = (float)LoadedRegion.left() + _transformedCoords.x() / ZoomX;
//						y = (float)LoadedRegion.top() + _transformedCoords.y() / ZoomY;
//					}
//					break;
//				default:
//					if ((ZoomFactor > 10) & (ZoomFactor < 1000)) {
//						x = (float)DisplayedRegion.left() + _transformedCoords.x() * 100.0f / (float)ZoomFactor;
//						y = (float)DisplayedRegion.top() + _transformedCoords.y() * 100.0f / (float)ZoomFactor;
//					}
//			}
//		}
//		else {
                        x = _transformedCoords.x();
                        y = _transformedCoords.y();
//		}

                //now we have picture global coordinates in pixel => step ...

                //... 2)

                //note: wavelength 0 can be used as all other wavelengths are stretched to have the same size as 0, if necessary
                //      thus the picture global coordinates always refer (at least indirectly) to wavelength 0
                //      -> we do not need to care about other binning factor of the different wavelengths
                float mmpp = TATInformation::getInst()->getWavelengthInfo (0).getMicrometerPerPixel();
                x = x * mmpp;
                y = y * mmpp;

                //add position offsets, but regard if coordinate system is inverted
                if (TTTManager::getInst().coordinateSystemIsInverted()) {
                        //inverted style: x/y increase to left/top
                        x = left - x;
                        y = top - y;
                }
                else {
                        //normal style: x/y increase to right/bottom
                        x += left;
                        y += top;
                }

                return QPointF (x, y);

        }
        else {
                //use old coordinate scheme
                //unit is pixel, the world bounds for each track are the picture bounds

                float x = 0, y = 0;

//		if (! _noDisplayStuff) {
//			switch (ZoomFactor) {
//				case 0:		//full picture => loaded region == displayed region
//					if (ZoomX * ZoomY) {
//						x = (float)LoadedRegion.left() + _transformedCoords.x() / ZoomX;
//						y = (float)LoadedRegion.top() + _transformedCoords.y() / ZoomY;
//					}
//					break;
//				default:
//					if ((ZoomFactor > 10) & (ZoomFactor < 1000)) {
//						x = (float)DisplayedRegion.left() + _transformedCoords.x() * 100.0f / (float)ZoomFactor;
//						y = (float)DisplayedRegion.top() + _transformedCoords.y() * 100.0f / (float)ZoomFactor;
//					}
//			}
//		}
//		else {
                        x = _transformedCoords.x();
                        y = _transformedCoords.y();
//		}

                return QPointF (x, y);
        }
}

QPointF BDisplay::calcTransformedCoords (QPointF _absCoords, PositionInformation *_positionInformation, bool _inMicrometer, bool _oldInvertedSystemReading) const
{

        //BS 2010/07/22: new graphics system -> display settings (zoom, region, ...) not anymore necessary

        if (TTTManager::getInst().USE_NEW_POSITIONS()) {
                if (! _positionInformation)
                        return QPointF (-1, -1);

                //use new coordinate scheme
                //unit is micrometer and the coordinates of all tracks are global in the experiment

                float left = _positionInformation->getLeft();
                float top = _positionInformation->getTop();


                //two steps:
                //1) transform the micrometer coordinates to picture global pixels with the information about the position location
                //2) calculate the coordinates in pixel as before (the coordinate origin is the topleft corner of the position)

                //1)
                //subtract position offsets, but regard if coordinate system is inverted
                float x = 0;
                float y = 0;


                ///if an elder experiment is inverted, and the track points are stored in the wrong way
                /// (x/y mess with inverted systems), then
                ///  - read the file in the old mode
                ///  - calculate transcoords for each trackpoint in old mode (without the if-query here)
                ///  - calculate abscoords for each trackpoint in new mode (no change in program)
                ///  - save the file

                if (TTTManager::getInst().coordinateSystemIsInverted() && (! _oldInvertedSystemReading)) {
                        //inverted style: x/y increase to left/top
                        x = left - _absCoords.x();
                        y = top - _absCoords.y();
                }
                else {
                        //normal style: x/y increase to right/bottom
                        x = _absCoords.x() - left;
                        y = _absCoords.y() - top;
                }

                if (_inMicrometer)
                        return QPointF (x, y);


                //note: wavelength 0 can be used as all other wavelengths are stretched to have the same size as 0, if necessary
                //      thus the picture global coordinates always refer (at least indirectly) to wavelength 0
                //      -> we do not need to care about other binning factor of the different wavelengths
                float mmpp = TATInformation::getInst()->getWavelengthInfo (0).getMicrometerPerPixel();
                x = x / mmpp;
                y = y / mmpp;

                //now we have picture global coordinates as before => step ...

//		if (_withoutDisplayShift)
//                        return QPointF (x, y);
//
//		//... 2)
//		switch (ZoomFactor) {
//			case 0:
//				x = (x - (float)LoadedRegion.left()) * ZoomX;
//				y = (y - (float)LoadedRegion.top()) * ZoomY;
//				break;
//			default:
//				if ((ZoomFactor > 10) & (ZoomFactor < 1000)) {
//					x = ((x - (float)DisplayedRegion.left()) * (float)ZoomFactor) / 100.0f;
//					y = ((y - (float)DisplayedRegion.top()) * (float)ZoomFactor) / 100.0f;
//				}
//		}

                return QPointF (x, y);
        }
        else {
                //use old coordinate scheme
                //unit is pixel, the world bounds for each track are the picture bounds

                return _absCoords;

//		if (_withoutDisplayShift)
//			return _absCoords;
//
//		float x = 0;
//		float y = 0;
//
//		switch (ZoomFactor) {
//			case 0:
//				x = ( _absCoords.x() - (float)LoadedRegion.left()) * ZoomX;
//				y = ( _absCoords.y() - (float)LoadedRegion.top()) * ZoomY;
//				break;
//			default:
//				if ((ZoomFactor > 10) & (ZoomFactor < 1000)) {
//					x = (_absCoords.x() - (float)DisplayedRegion.left()) * (float)ZoomFactor / 100.0f;
//					y = (_absCoords.y() - (float)DisplayedRegion.top()) * (float)ZoomFactor / 100.0f;
//				}
//		}
//
//                return QPointF (x, y);
        }
}

void BDisplay::adjustGammaValues()
{
        //the y values of the three anchor points are not changed!
        //but the whole function has to be stretched => GammaValues[] has to be adjusted

        //the blackpoint (bp,0) and whitepoint (wp,255) are also treated as anchor points
        int anchorX [GAMMA_ANCHOR_POINTS + 2];
        anchorX [0] = BlackPoint;
        anchorX [GAMMA_ANCHOR_POINTS + 1] = WhitePoint;

        //calculate the x positions of the anchor points
        float areasize = float(WhitePoint - BlackPoint) / float(GAMMA_ANCHOR_POINTS + 1);
        for (int i = 1; i <= GAMMA_ANCHOR_POINTS; i++) {
                anchorX [i] = BlackPoint + int(areasize * (i));
                //these values are stored for later use
                GammaAnchor [i - 1][0] = anchorX [i];
        }

        //the connections between the anchor points are linear
        //the equation of the linear function f [= m*x + t] between the two points
        // (a,b) and (c,d) is received as follows:
        // 		(1)  m = (d - b) / (c - a)
        // 		(2)  t = b - m * a
        //this function has to be calculated for each new pair of points
        //with this equation, the intermediate values between those points are calculated
        //parameters:
        //		(a,b) == (anchorX[i], GammaAnchor[i])
        //		(c,d) == (anchorX[i+1], GammaAnchor[i+1])

        float m = 0;
        int t = 0;
        int a = 0, b = 0, c = 0, d = 0;
        int area = -1;
        bool new_area = true;			//true <=> a new function has to be calculated and used for interpolation

        for (int i = BlackPoint; i <= WhitePoint; i++) {
                if (new_area) {
                        area++;

                        //set parameters
                        a = anchorX [area];
                        if (area == 0)
                                b = 0;
                        else
                                b = GammaAnchor [area - 1][1];
                        c = anchorX [area + 1];
                        if (area == GAMMA_ANCHOR_POINTS)
                                d = 255;
                        else
                                d = GammaAnchor [area][1];		//GammaAnchor[0] is not at blackpoint!

                        //calculate m & t   => function is defined
                        m = float(d - b) / float(c - a);
                        t = b - (int)(m * (float)a);

                        new_area = false;

                }

                //calculate the values of the points within the area
                GammaValues [i] = int(m * (float)i) + t;

                //if i crosses an anchor point, a new area is reached
                if (area < GAMMA_ANCHOR_POINTS)
                        if (i >= anchorX [area+1])
                                new_area = true;
        }
}

//QPointF BDisplay::calcCenter() const
//{
////@todo
////	float x = (DisplayedRegion.right() + DisplayedRegion.left()) / 2.0f;
////	float y = (DisplayedRegion.bottom() + DisplayedRegion.top()) / 2.0f;
////        return QPointF (x,y);
//}

QPointF BDisplay::calcPictureCenter (bool _inMicrometer) const
{
        float x = CompleteSize.width() / 2.0f;
        float y = CompleteSize.height() / 2.0f;

        if (_inMicrometer & (TTTManager::getInst().USE_NEW_POSITIONS())) {
                float mmpp = TATInformation::getInst()->getWavelengthInfo (0).getMicrometerPerPixel();
                x = x * mmpp;
                y = y * mmpp;
        }

        return QPointF (x,y);
}



