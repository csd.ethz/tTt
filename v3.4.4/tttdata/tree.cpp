/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "tree.h"

#ifndef TREEANALYSIS	
#include "tttgui/ttttracking.h"
#include "tttgui/treedisplay.h"
#endif

#include "tttbackend/tttmanager.h"
#include "tttbackend/tttpositionmanager.h"
//Added by qt3to4:
#include <Q3ValueList>

#include "userinfo.h"

Tree::Tree()
	: /*QObject (0, 0),*/ BaseTrack (0)
{
	//TrackArray.setAutoDelete (true);
	
	NumTracks = 0;
	
	MaxTrackNumber = 0;
	
	filename = "";
	
	setMaxWavelength (MAX_WAVE_LENGTH);
        setFinished (false);
}

Tree::~Tree()
{
	//qDebug() << "Tree will be desroyed!";
}

void Tree::insert (Track *_track, bool _sendSignal)
{
	// Delegate
	insert(QSharedPointer<Track>(_track), _sendSignal);
}

void Tree::insert (QSharedPointer<Track> _track, bool _sendSignal)
{
	//NOTE: shallow copy, the data is not copied, only the pointer

#ifndef TREEANALYSIS	
	// TODO:
	if (!_track && _sendSignal)
		if(TTTManager::getInst().frmTracking && TTTManager::getInst().getTree() == this)
			TTTManager::getInst().frmTracking->updateTreeSize();
	//	emit TreeSizeChanged();
#endif

	if (! _track)
		return;

	_sendSignal = false;
	//if ((_track->getNumber() >= (int)TrackArray.size()) || (TrackArray.isEmpty())) {
	//	//allocate more memory than necessary for faster later updates
	//	//note (if QIntDict): size should be a prime number to have a balanced hash tree
	//	TrackArray.resize (_track->getNumber() * 2 + 1);
	//	_sendSignal = true;
	//}

	//TrackArray.insert (_track->getNumber(), _track);

	int trackNumber = _track->getNumber();

	// If this is a new track, increade trackcount
	if(!tracks.contains(trackNumber))
		NumTracks++;

	// Insert
	tracks.insert(trackNumber, _track);

	// Check if this is root
	if (trackNumber == 1)
		BaseTrack = _track.data();

	// Set this to _tracks tree
	_track->setTree(this);


	if ((uint)_track->getNumber() > MaxTrackNumber) {
		int fmpt = getMaxPossibleTracks();

		MaxTrackNumber = _track->getNumber();

		if (getMaxPossibleTracks() != fmpt)
			//sending the size update signal is only of worth, if the canvas size really changes
			//this is not the case, if the value of getMaxPossibleTracks() is not changed
			//note: the result of getMaxPossibleTracks() depends on the value of MaxTrackNumber
			_sendSignal = true;
	}

#ifndef TREEANALYSIS
	// OTODO:
	if (_sendSignal)
		if(TTTManager::getInst().frmTracking && TTTManager::getInst().getTree() == this)
			TTTManager::getInst().frmTracking->updateTreeSize();
	//	emit TreeSizeChanged();
#endif
}

bool Tree::deleteTrack (Track *_track)
{
	//note: deleting a track is different from deleting a stop reason!
	
	//children - there are two cases:
	// - no children (including not tracked children) -> delete track immediately
	// - living children                              -> ask for confirmation
	
	if (! _track)
		return false;
	
	//if this is the base track, the tree has to be reset afterwards
	if (_track == BaseTrack) {
		delTrack (_track);
		reset();
	}
	else
		delTrack (_track);
	
#ifndef TREEANALYSIS
	// TODO:
	//emit TreeSizeChanged();
	//emit tracksDeleted();
	if(TTTManager::getInst().frmTracking && TTTManager::getInst().getTree() == this)
		TTTManager::getInst().frmTracking->updateTreeSize();
#endif

	return true;
}

bool Tree::insertComplexTrack (QSharedPointer<Track> _track, bool _sendSignal)
{
	if (! _track)
		return true;
	
	insert (_track, _sendSignal);
	
	//insert this track's children as well (if there are none, the recursion stops)
	bool success = insertComplexTrack (_track->getChildTrackShared (1), _sendSignal);
	success &= insertComplexTrack (_track->getChildTrackShared (2), _sendSignal);
	
	return success;
}

Q3IntDict<Track>* Tree::timePointTracks (int _timePoint) const
{
	if (tracks.isEmpty()) 
		return 0;
	
	Q3IntDict<Track> *tmp = new Q3IntDict<Track>();
	tmp->setAutoDelete (false);

	int j = 0;
	bool anything = false;
	
	//for (int i = 0; i < (int)TrackArray.size(); i++) {
	//	if (TrackArray [i]) 
	//		if (TrackArray [i]->aliveAtTimePoint (_timePoint)) {
	//			anything = true;
	//			tmp->insert (j, TrackArray [i]);
	//			j++;
	//			if (j >= (int)tmp->size())
	//				tmp->resize (j + 10);			//allocate more than necessary for faster inserting
	//		}
	//}

	for(tracks_const_iterator it = tracks.constBegin(); it != tracks.constEnd(); ++it) {
		Track* curTrack = it->data();
		if (curTrack->aliveAtTimePoint (_timePoint)) {
			anything = true;
			tmp->insert (j, curTrack);
			j++;
			if (j >= (int)tmp->size())
				tmp->resize (j + 10);			//allocate more than necessary for faster inserting
		}
	}
	
	if (anything)
		return tmp;
	else {
		delete tmp;
		return 0;
	}
}

Q3ValueList<TrackPoint>* Tree::timePointTrackPoints (int _timePoint) const
{
	if (tracks.isEmpty())
		return 0;
	
	Q3ValueList<TrackPoint> *tmp = new Q3ValueList<TrackPoint>();
	
	TrackPoint tmpTrackPoint;
	bool anything = false;
	
	//for (int i = 0; i < (int)TrackArray.size(); i++) {
	//	anything = true;
	//	if (TrackArray [i]) {
	//		tmpTrackPoint = TrackArray [i]->getTrackPoint (_timePoint);
	//		if (tmpTrackPoint.TimePoint > -1) {
	//			tmpTrackPoint.tmpCellNumber = TrackArray [i]->getNumber();
	//			tmp->append (tmpTrackPoint);
	//		}
	//	}
	//}

	for(tracks_const_iterator it = tracks.constBegin(); it != tracks.constEnd(); ++it) {
		Track* curTrack = it->data();
		anything = true;
		tmpTrackPoint = curTrack->getTrackPoint (_timePoint);
		tmpTrackPoint.track = curTrack;
		if (tmpTrackPoint.TimePoint > -1) {
			tmpTrackPoint.tmpCellNumber = curTrack->getNumber();
			tmp->append (tmpTrackPoint);
		}
	}
	
	if (anything)
		return tmp;
	else {
		delete tmp;
		return 0;
	}
}

bool Tree::trackExists (int _timePoint) const
{
	//for (int i = 0; i < (int)TrackArray.size(); i++) 
	//	if (TrackArray [i])
	//		if (TrackArray [i]->trackPointExists (_timePoint))
	//			return true;

	for(tracks_const_iterator it = tracks.constBegin(); it != tracks.constEnd(); ++it) {
		Track* curTrack = it->data();

		if (curTrack->trackPointExists (_timePoint))
			return true;
	}
	
	return false;		//no track exists at that timepoint
}

int Tree::getMaxPossibleTracks() const
{
        return getMaxPossibleTracks (MaxTrackNumber);
}

int Tree::getMaxPossibleTracks (int _maxTrackNumber)
{
        //return the next highest power of 2 minus 1, greater or equal to MaxTrackNumber
        //e.g. if the highest track number in the tree is 13, 15 is returned
        //but if MaxTrackNumber == 15, also 15 is returned

        //note: each change in here also affects the ttt file handling routines!
/*	double max = 0.0;
        modf(log(MaxTrackNumber)/  log(2), &max);
        if ((int)pow(2, max) == MaxTrackNumber)
                return (int)pow(2, max) - 1;
        else
                return (int)pow(2, max + 1.0) - 1;*/

        for (int i = 0; ; i++)
                if ((int)pow (2.0, (double)i) > _maxTrackNumber)
                        return (int)(pow (2.0, (double)i)) - 1;

        return 0;
}

int Tree::recalcTrackCountInternal (Track *_track)
{
	if (! _track)
		return 0;
	else {
		int sum1 = recalcTrackCountInternal (_track->getChildTrack (1));
		int sum2 = recalcTrackCountInternal (_track->getChildTrack (2));
		
		//TrackArray.insert (_track->getNumber(), _track);
		
		return 1 + sum1 + sum2; 
	}
}

int Tree::recalculateNumberOfTracks()
{
	//count true number of tracks and also reorganize the TrackArray
	
	NumTracks = recalcTrackCountInternal (BaseTrack);
	
/*	//reorganize...
	
	QPtrVector<Track> TrackArrayBackup (TrackArray);
	
	TrackArray.setAutoDelete (false);
	for (uint i = 0; i < TrackArray.size(); i++)
		TrackArray.insert (i, 0);
	//TrackArray.clear();
	TrackArray.setAutoDelete (true);
	
	TrackArray.resize (MaxTrackNumber + 10);
	
	//reorder track vector (begin with the highest number)
	//method: copy each track to the array position its number indicates
	//not anymore relevant: //as we start with the highest number, the new place is always free and no tracks are overwritten
	for (int i = TrackArrayBackup.size() - 1; i > 0; i--) {
		Track *track = TrackArrayBackup.take (i);
		if (track) {
			TrackArray.insert (track->getNumber(), track);
		}
	}*/
	
	return NumTracks;
}

Track* Tree::getTrack (int _trackNumber) const
{
	//if (tracks.isEmpty())
	//	return 0;
	//
 //   if (((uint)_trackNumber > 0) & ((uint)_trackNumber < TrackArray.size()))
	//	return TrackArray [_trackNumber];
	//else
	//	return 0;

	if(tracks.contains(_trackNumber))
		return tracks[_trackNumber].data();

	// Not found
	return 0;
}

//recursive
void Tree::delTrack (Track *_track)
{
	if (! _track)
		return;
	
	//if the children do not exist, the control is immediately returned to this call
	delTrack (_track->getChildTrack (1));
	delTrack (_track->getChildTrack (2));
	
	if (_track->getMotherTrack())
		//delete the mother's reference to this track - very important!
		_track->getMotherTrack()->setChildTrack (_track->getChildIndex(), 0);
	
	
	//@ todo this is not yet correct, but its purpose is fulfilled hopefully ;-)
	if ((uint)_track->getNumber() == MaxTrackNumber)
		MaxTrackNumber--;
	
	////note: _track is also deleted on the heap, as autoDelete is enabled
	//TrackArray.remove (_track->getNumber());

	// note: _track is also deleted on the heap, because of QSharedPointer
	tracks.remove(_track->getNumber());
	
	NumTracks--;
}

void Tree::reset()
{
	if (tracks.isEmpty())
		//tree is still in reset state
		return;
	
	//delete all tracks cascadeously
	delTrack (BaseTrack);

	// OTODO:
	//emit tracksDeleted();
	
	//TrackArray.resize (17);
	//TrackArray.setAutoDelete (true);
	
	NumTracks = 0;
	
	MaxTrackNumber = 0;
	BaseTrack = 0;

	changeLog.clear();

	// New tree
	modified = true;
}

Q3IntDict<Track> Tree::coExistingTracks (int _timePoint, Track *_track) const
{
	
	Q3IntDict<Track> tmp;
	tmp.setAutoDelete (false);
	
	if (tracks.isEmpty()) 
		return tmp;
	if (! _track)
		return tmp;
	
	tmp.resize (MaxTrackNumber);
	int number = _track->getNumber();
	
	//for (int i = 0; i < (int)TrackArray.size(); i++) {
	//	Track * tmpTrack = TrackArray [i];
	//	if (tmpTrack) 
	//		if ( (tmpTrack->getFirstTrace() <= _timePoint) &&
	//		 	 (tmpTrack->getLastTrace()  >= _timePoint) ) {
	//			
	//			if (tmpTrack->getNumber() < number)
	//				//insert ascending
	//				tmp.insert (tmpTrack->getNumber(), tmpTrack);
	//			else if (tmpTrack->getNumber() > number)
	//				//insert descending
	//				tmp.insert (MaxTrackNumber + number - tmpTrack->getNumber(), tmpTrack);
	//		}
	//}

	for(tracks_const_iterator it = tracks.constBegin(); it != tracks.constEnd(); ++it) {
		Track * tmpTrack = it->data();

		if ( (tmpTrack->getFirstTrace() <= _timePoint) &&
			(tmpTrack->getLastTrace()  >= _timePoint) ) {

			if (tmpTrack->getNumber() < number)
				//insert ascending
				tmp.insert (tmpTrack->getNumber(), tmpTrack);
			else if (tmpTrack->getNumber() > number)
				//insert descending
				tmp.insert (MaxTrackNumber + number - tmpTrack->getNumber(), tmpTrack);
		}
	}
	
	return tmp;	
}

//recursive
void Tree::assignNewNumber (Track *_startTrack, int _newNumber)
{
	if (! _startTrack)
		return;
		
	_startTrack->setNumber (_newNumber);
	
	if ((uint)_newNumber > MaxTrackNumber)
		MaxTrackNumber = _newNumber;
	
	//recursive loop through all children
	//the new number for the child tracks is calculated by their parent number
	//-> children of track x have numbers x*2 & x*2+1
	
	if (_startTrack->getChildTrack (1))
		assignNewNumber (_startTrack->getChildTrack (1), _newNumber * 2);
	if (_startTrack->getChildTrack (2))
		assignNewNumber (_startTrack->getChildTrack (2), _newNumber * 2 + 1);
}

bool Tree::shiftTrackNumbers (Track *_startTrack, int _newNumber)
{
	if (! _startTrack)
		return false;
	
	//assign new number to each track recursively
	//the association between mother and child tracks is kept and no tracks are written to a new position in the array
	assignNewNumber (_startTrack, _newNumber);
	
	//reorder the number |-> track hash
	//---------------------------------
	//1) copy current vector to backup
	//Q3PtrVector<Track> TrackArrayBackup (TrackArray);
	QHash<int, QSharedPointer<Track> > tracksBackup(tracks);
	
	//as the track vector is a hash which has the track number as key, the association must be updated
	//=> resize the hash, either smaller or larger
	
	////clear old values of track array
	//TrackArray.setAutoDelete (false);
	//for (uint i = 0; i < TrackArray.size(); i++)
	//	TrackArray.insert (i, 0);
	////TrackArray.clear();
	//TrackArray.setAutoDelete (true);
	//
	//TrackArray.resize (MaxTrackNumber + 10);
	tracks.clear();
	
	//reorder track vector (begin with the highest number)
	//method: copy each track to the array position its number indicates
	//not anymore relevant: //as we start with the highest number, the new place is always free and no tracks are overwritten
	//for (int i = TrackArrayBackup.size() - 1; i > 0; i--) {
	//	Track *track = TrackArrayBackup.take (i);
	//	if (track) {
	//		TrackArray.insert (track->getNumber(), track);
	//	}
	//}

	for(tracks_const_iterator it = tracksBackup.constBegin(); it != tracksBackup.constEnd(); ++it) {
		QSharedPointer<Track> track = *it;
		tracks.insert(track->getNumber(), track);
	}
	
	return true;
}

int Tree::calcSpeed (const TrackPoint &_t1, const TrackPoint &_t2, int _speedAmplifier) const
{
	if (! TTTManager::getInst().getBasePositionManager())
		return 0;
	if (! TTTManager::getInst().getBasePositionManager()->getFiles())
		return 0;
	
	int elapsedTime = TTTManager::getInst().getBasePositionManager()->getFiles()->calcSeconds (_t1.TimePoint, _t2.TimePoint);
	int dist = (int)_t1.distanceTo (_t2);		//distance between the trackpoints
	
	int speed = 0;
	if (elapsedTime > 0)
		speed = _speedAmplifier * dist / elapsedTime;
	
	return speed;
}


float Tree::calcTrueSpeed (const TrackPoint &_t1, const TrackPoint &_t2, const TTTPositionManager *_tttpm) const
{
	if (! _tttpm)
		_tttpm = TTTManager::getInst().getBasePositionManager();
	
	if (! _tttpm)
		return 0;
	if (! _tttpm->getFiles())
		return 0;
	
	int elapsedTime = _tttpm->getFiles()->calcSeconds (_t1.TimePoint, _t2.TimePoint);
	float dist = _t1.distanceTo (_t2);		//distance between the trackpoints
	
	if (elapsedTime > 0)
		return dist / (float)elapsedTime;
	else
		return 0.0f;
}

QColor Tree::calcSpeedColor (int _speed) const
{
	
	//note: if _speed is not normalized to a maximum of 255, the exceeds are truncated
	
	//_speed is doubled for color calculation
	if (_speed > 127)	//just for security
		_speed = 127;
	if (_speed < 0)
		_speed = 0;
	
	//simple version: returns more or less blue
	//int speedColor = 255 - 255 / SpeedWindowWidth * _speed;
	//return QColor (speedColor, speedColor, speedColor);
	
	//heat map version: slow == blue, fast == red
	//=> from rgb(0,0,255) to rgb(255,0,0) in 255 steps
	//=> no change of green component!
	
	return QColor (_speed * 2, 0, 255 - _speed * 2);
}

int Tree::getMaxTrackedTimePoint() const
{
	//loop through all tracks; the highest LastTrace is the result
	//note: if written without loop (values are updated with insert() and delete()), then
	//       the tree has to be notified about every change in a Track object (which can take
	//       place outside the Tree's methods!)
	
	int max = 0;
	
	//for (int i = 0; i < (int)TrackArray.size(); i++) {
	//for (uint i = 1; i <= MaxTrackNumber; i++) {
	//	if (TrackArray [i])
	//		if (TrackArray [i]->getLastTrace() > max)
	//			max = TrackArray [i]->getLastTrace();
	//}
	for(tracks_const_iterator it = tracks.constBegin(); it != tracks.constEnd(); ++it) {
		Track *curTrack = it->data();
		if (curTrack->getLastTrace() > max)
			max = curTrack->getLastTrace();
	}
	
	return max;
}

int Tree::getMinTrackedTimePoint () const
{
	//loop through all tracks; the lowest FirstTrace is the result
	//cf. note in getMaxTrackedTimePoint()
	
	// -------------Konstantin: comment the next 3 lines, because 'TTTManager::getInst().getBasePositionManager()' is always false and 'min' stay 0
// 	int min = 0;
// 	if (TTTManager::getInst().getBasePositionManager()) 
// 		min = TTTManager::getInst().getBasePositionManager()->getLastTimePoint();

	////for (int i = 0; i < (int)TrackArray.size(); i++) {
	//int min = TrackArray [1]->getFirstTrace(); // --------------- Konstantin -----------------
	//for (uint i = 2; i <= MaxTrackNumber; i++) { // ------------- Konstantin: i = 2 instead of i = 1 ----------------
	//	if (TrackArray [i])
	//		if (TrackArray [i]->getFirstTrace() < min)
	//			min = TrackArray [i]->getFirstTrace();
	//}

	int min = 0xFFFFFF;
	for(tracks_const_iterator it = tracks.constBegin(); it != tracks.constEnd(); ++it) {
		Track *curTrack = it->data();
		if (curTrack->getFirstTrace() < min)
			min = curTrack->getFirstTrace();
	}
	
	return min;
}

// Disabled, because getTreeProperty in TreeWrapper has the same functionality
//--------------------- Konstantin ------------------------
//int Tree::getOptValue(const QString _name, const TTTPositionManager *_tttpm) const
//{
//	if ( _name == TREE_OPT_LIFETIME) {
//		return _tttpm->getFiles()->calcSeconds (getMinTrackedTimePoint(), getMaxTrackedTimePoint());
//	}
//	if ( _name == TREE_OPT_START_TP ) {
//		return _tttpm->getFiles()->calcSeconds (_tttpm->getFirstTimePoint(), getMinTrackedTimePoint());
//	}
//	if ( _name == TREE_OPT_STOP_TP ) {
//		return _tttpm->getFiles()->calcSeconds (_tttpm->getFirstTimePoint(), getMaxTrackedTimePoint());
//	}
//	if ( _name == TREE_OPT_NUMBER_OF_CELLS )
//		return getTrackCount();
//
//    if ( _name == TREE_OPT_NUMBER_OF_GENERATIONS )
//        return getNumberOfGenerations();
//
//	return 0;
//}

Q3IntDict<Track> Tree::getAllTracks (int _generations) const
{
	Q3IntDict<Track> tmp (17); 
	uint number = 0;
	
	////data starts with 1
	//for (uint i = 1; i <= MaxTrackNumber; i++) {
	//	if (TrackArray [i]) {
	//		
	//		if ((_generations == -1) ||
	//			(childrenGenerationsCount (TrackArray [i]) == _generations)) {
	//		
	//			number = i; 
	//			if (number >= tmp.size())
	//				tmp.resize (number + 10);
	//			tmp.replace (number, TrackArray [i]);
	//		}
	//	}
	//}

	for(tracks_const_iterator it = tracks.constBegin(); it != tracks.constEnd(); ++it) {
		Track *curTrack = it->data();

		if ((_generations == -1) ||
			(childrenGenerationsCount (curTrack) == _generations)) {

				number = curTrack->getNumber(); 
				if (number >= tmp.size())
					tmp.resize (number + 10);
				tmp.replace (number, curTrack);
		}
	}
	
	return tmp;
}

//recursive
int Tree::childrenGenerationsCount (Track *_track, int _count) const
{
	if (! _track)
		return 0;
	
	int depth1 = _count, depth2 = _count;
	
	if (_track->getChildTrack (1))
		depth1 = childrenGenerationsCount (_track->getChildTrack (1), _count + 1);
	if (_track->getChildTrack (2))
		depth2 = childrenGenerationsCount (_track->getChildTrack (2), _count + 1);
	
	return QMAX (depth1, depth2);
}

int Tree::getNumberOfGenerations() const
{
	if (! BaseTrack)
		return 0;
	else
		return childrenGenerationsCount (BaseTrack);
}

bool Tree::eraseStoredPositions()
{
	bool result = true;
	
	//for (uint i = 1; i <= MaxTrackNumber; i++) {
	//	if (TrackArray [i]) {
	//		result &= TrackArray [i]->eraseStoredPositions();
	//		
	//		if (! result)
	//			break;
	//	}
	//}

	for(tracks_const_iterator it = tracks.constBegin(); it != tracks.constEnd(); ++it) {
		Track* curTrack = it->data();

		result &= curTrack->eraseStoredPositions();

		if (! result)
			break;
	}
	
	return result;
}

const QPair<QDate,QByteArray>* Tree::getLastChangelogEntry() const
{
	// Check if we have one
	if(changeLog.size() == 0)
		return 0;

	// Get last one
	QList<QPair<QDate,QByteArray> >::const_iterator it = changeLog.constEnd();
	--it;

	// Return pointer
	return &(*it);
}

QString Tree::getTreeName() const
{
	// Return filename without path
	QString ret = filename;
	int i = ret.lastIndexOf('/');
	if(i != -1) {
		ret = ret.mid(i + 1);
	}

	return ret;
}

QString Tree::getPositionIndex() const
{
	// Return position name
	return this->positionIndex;
}


QString Tree::getExperimentName() const
{
	// Return position name
	return this->experimentName;
}


QList<ITrackPoint*> Tree::getTrackPointsAtTimePoint( int _timePoint ) const
{
	QList<ITrackPoint*> ret;

	// Iterate over all tracks
	for(tracks_const_iterator it = tracks.constBegin(); it != tracks.constEnd(); ++it) {
		Track* curTrack = it->data();

		// Check if _timepoint exists at provided _timePoint
		if(TrackPoint* tp = curTrack->getTrackPointByTimePoint(_timePoint))
			ret.append(tp);
	}

	return ret;
}



long Tree::getCellTypes() const
{
	if (! BaseTrack)
		return CT_NOCELLTYPE;
	else
		return getCellTypesRecursive (BaseTrack, CT_NOCELLTYPE);
}

long Tree::getCellTypesRecursive (Track *_track, long _result) const
{
	if (! _track)
		return _result;

	for (long ct = CT_DIVIDINGTYPE; ct <= CT_NOLOSTCHILDRENTYPE; ct *= 2) {
		CellType cellType = Track::longToCellType (ct);
		if (_track->isOfCellType (cellType))
			_result |= ct;
	}

	_result |= getCellTypesRecursive (_track->getChildTrack (1), _result);
	_result |= getCellTypesRecursive (_track->getChildTrack (2), _result);

	return _result;
}
