/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef USERINFO_H
#define USERINFO_H

#include "systeminfo.h"
#include "stylesheet.h"

#include <qstring.h>
#include <qinputdialog.h>
#include <qmessagebox.h>
#include <qdir.h>
#include <qfile.h>
#include <QSettings>


/**
	@author Bernhard
	
	This class holds information about the current user and user
	specific settings.
*/

class UserInfo {

protected:
	
	///the (single) instance of this class; can be obtained via getInst()
	static UserInfo *inst;
	
public:
	
	UserInfo();
	
	~UserInfo();
	
	/**
	 * @return (the) instance of UserInfo; constructs a new one, if none exists
	 */
	static UserInfo& getInst();


	/**
	 * Delete userinfo instance
	 */
	static void cleanup();
	
	
	
	/**
	 * opens an input box where the user can enter a text of either two or three letters, which is his sign
	 * if this has already happened before, the current user sign is returned
	 * @param _cancelled false, if the user entered a valid sign; true if he cancelled
	 * @param _path the path of the file with the user names
	 * @return a short sign (e.g. BS) of the user
	 */
	QString getUsersign (bool *_cancelled = 0, const QString &_path = "", bool neverOpenQueryWindow = false);
	
	/**
	 * sets the user sign (called by the user sign query form)
	 * @param _userSign the user sign (should be in uppercase, only two or three letters)
	 */
	void setUsersign (const QString &_userSign);
	
	int getAddOnBitpack()
		{ return addOn_bitpack;}
	
	/**
	 * reads the configuration file for the current user or creates a new
	 * config dom object, if file does not exist
	 * @return success
	 */
	bool readUserConfigFile();
	
	/**
	 * writes the configuration file for the current user
	 * @return success
	 */
	bool writeUserConfigFile();
	
	///sets the style sheet (a graphical option packet for the tree display)
	///does NOT repaint the tree!
	///@param _ss the StyleSheet object which should be used for further drawings
	void setStyleSheet (StyleSheet &_ss);
	
	///returns the currently set style sheet
	const StyleSheet& getStyleSheet() const
		{return styleSheet;}
	
	///returns the currently set style sheet as reference
	StyleSheet& getRefStyleSheet()
		{return styleSheet;}
	
	/**
	 * @return the path of the user specific config file
	 */
	const QString& getUserConfigFilePath() {
		return userConfigFilePath;
	}
	
	/**
	 * @return the absolute path of the directory with the user configuration file (with terminating slash)
	 */
	const QString getUserConfigDirectory();
	
	/**
	 * @return the path of the default user style file
	 */
	const QString getUserStyleSheetPath();
	
	/**
	 * Loads the user-specific style sheet from its default location
	 * @return loading success
	 */
	bool readUserStyleSheet();
	
	/**
	 * Saves the user-specific style sheet to its default location
	 * @return writing success
	 */
	bool saveUserStyleSheet();


	/**
	 * Save window position and size to a user specific config file
	 * @param _window the window whose pos and size are to be saved
	 * @param _windowName name of window, necessary to make loading of settings possible 
	 * @return true if successful
	 */
	bool saveWindowPosition(const QWidget* _window, const QString& _windowName);

	/**
	 * Load window position and size from a user specific config file
	 * @param _window the window whose pos and size are to be loaded
	 * @param _windowName name of window
	 * @return true if successful
	 */
	bool loadWindowPosition(QWidget* _window, const QString& _windowName);

	/**
	 * Get QSettings instance for user settings (stored in NAS for each user)
	 * @return Pointer to QSettings object for user settings. Can be 0 if no user sign has been chosen yet
	 */
	QSettings* getUserSettings() const;

	/**
	 * Get value of user setting specified by provided key
	 * @param _key key of the setting
	 * @return corresponding value or invalid QVariant if no setting with the provided key exists or if settings are not available
	 */
	QVariant getUserSettingByKey(const QString& _key) const;

	/**
	 * Get local settings (stored locally for all users)
	 * @return Pointer to QSettings instance. Cannot be 0.
	 */
	QSettings* getLocalSettings() const;

	/**
	 * Helper function for easy retrieval of user settings. 
	 */
	static QVariant get(const QString& _key, const QVariant& _defaultValue) {
		UserInfo& ins = getInst();
		if(ins.userSettings)
			return ins.userSettings->value(_key, _defaultValue);
		return _defaultValue;
	}

	/**
	 * Helper function for easy changing of user settings. 
	 */
	static bool set(const QString& _key, const QVariant& _value) {
		UserInfo& ins = getInst();
		if(ins.userSettings) {
			ins.userSettings->setValue(_key, _value);
			return true;
		}
		return false;
	}

	//////////////////////////////////////////////////////////////////////////
	// USER SETTINGS KEYS
	//////////////////////////////////////////////////////////////////////////

	static const char* KEY_TREEVIEW_TRACKLINEWIDTH;					// width of tree lines 
	static const char* KEY_TREEVIEW_TRACKLINEDISTANCE;				// distance between two tree lines (i.e. between lines for different wavelengths and main track line)
	static const char* KEY_TREEVIEW_MAINTRACKLINESDISTANCE;			// distance between two adjacent tree lines (only main tree lines, not wavelength lines)
	static const char* KEY_TREEVIEW_TIMELINEFONTSIZE;				// TreeViewTimeLine font size (point size)
	static const char* KEY_TREEVIEW_TIMELINEFONTCOL;				// TreeViewTimeLine font color
	static const char* KEY_TREEVIEW_TIMELINELINEWIDTH;				// line width of vertical TreeViewTimeLine lines
	static const char* KEY_TREEVIEW_TIMELINEBACKGROUNDCOLOR;		// background color of timeline
	static const char* KEY_TREEVIEW_TIMELINELINECOLOR;				// color of vertical TreeViewTimeLine lines
	static const char* KEY_TREEVIEW_CELLCIRCLERADIUS;				// radius of TreeViewCellCircle in scene units
	static const char* KEY_TREEVIEW_CELLCIRCLEOUTLINEWIDTH;			// width of pen for TreeViewCellCircle outline
	static const char* KEY_TREEVIEW_CELLCIRCLEDEFAULTOUTERCOLOR;	// color of outline of TreeViewCellCircle 
	static const char* KEY_TREEVIEW_CELLCIRCLEDEFAULTINNERCOLOR;	// color of inner area of TreeViewCellCircle 
	static const char* KEY_TREEVIEW_CELLCIRCLESELECTEDOUTERCOLOR;	// color of outline of a selected TreeViewCellCircle 
	static const char* KEY_TREEVIEW_CELLCIRCLESELECTEDINNERCOLOR;	// color of inner area of a selected TreeViewCellCircle 
	static const char* KEY_TREEVIEW_CELLCIRCLEMOUSEHOVEROUTERCOLOR;	// color of inner area of a TreeViewCellCircle when mouse is moving over it
	static const char* KEY_TREEVIEW_TRACKLINESCONTINUOUS;			// if track lines should always be drawn as continuous lines, even if they contain gaps
	static const char* KEY_TREEVIEW_TRACKNUMFONTSIZE;				// track number font size (point size)
	static const char* KEY_TREEVIEW_TRACKNUMCOLOR;					// track number color
	static const char* KEY_TREEVIEW_TREENAMEFONTSIZE;				// tree name font size
	static const char* KEY_TREEVIEW_TREENAMECOLOR;					// tree name color
	static const char* KEY_TREEVIEW_SYMBOLSFONTSIZE;				// font size for symbols
	static const char* KEY_TREEVIEW_SYMBOLCELLDEATH;				// symbol (i.e. string) used to indicate cell death
	static const char* KEY_TREEVIEW_SYMBOLCELLDEATHCOLOR;			// Color of cell death symbol
	static const char* KEY_TREEVIEW_SYMBOLLOST;						// symbol (i.e. string) to indicate 'lost' cell fate
	static const char* KEY_TREEVIEW_SYMBOLLOSTCOLOR;				// color of 'lost' symbol
	static const char* KEY_TREEVIEW_SHOWREALTIME;					// show real time in scala
	static const char* KEY_TREEVIEW_HEIGHTFACTOR;					// pixels per time point used in tree view
	static const char* KEY_STATS_LAST_LOGFILE_FOLDER;				// last used logfile folder for tttstats
	static const char* KEY_STATS_LAST_EXPERIMENTSFILES_FOLDER;		// last used experiments files folder (with the original images) for tttstats
	static const char* KEY_STATS_LAST_NAS_FOLDER;					// last used nas folder for tttstats
	static const char* KEY_STATS_LAST_SHOWN_CHANGELOG;				// Contains the last version the changelog was shown for

	static const char* KEY_TTTMOVIE_USECOMMENTASWL;					// If comment to wl from tatxml should be displayed instead of wl number	
	static const char* KEY_TTTMOVIEEXPLORER_PATH;					// Path to xml files to display in movie explorerprivate:

	static const char* KEY_TTTAUTOTRACKING_SELECTCONNECTED;			// State of TTTAutotracking::ui::chkSelectOverlapping

	static const char* KEY_TTTEXPORTFLUORESCENCEPATCHES_TARGETDIR;			// Target directory

	//////////////////////////////////////////////////////////////////////////
	// LOCAL SETTINGS KEYS
	//////////////////////////////////////////////////////////////////////////
	static const char* KEY_TTTCONVERTTREES_INPATH;
	static const char* KEY_TTTCONVERTTREES_OUTPATH;

private:

	/**
	 * Set default values for settings with no value yet
	 */
	void setDefaultSettings();

	/**
	 * Set default value for the specified setting if it is not contained yet in userSettings
	 * @param _key key of settings
	 * @param _value value for setting
	 */
	void setDefaultSetting(const char* _key, const QVariant& _value);
	
	///the current user's short sign (like BS), used for various things
	///- jpg export (to have unique directory names)
	///- tracking configurations
	QString usersign;
	
	///the index of the bitpack in the additional attribute
	int addOn_bitpack;

	///the internal style sheet for drawing the tree, movie layout, ...
	StyleSheet styleSheet;
	
	///the path to the config file
	///is set in readUserConfigFile()
	QString userConfigFilePath;

	///Dom document for user settings
	QDomDocument userConfig;

	// User config QSettings instance
	QSettings* userSettings;

	// Local settings
	QSettings* localSettings;

	///the default config file path
	static const char* USER_CONFIG_FILENAME;
	static const char* NEW_USER_CONFIG_FILENAME;
};

#endif
