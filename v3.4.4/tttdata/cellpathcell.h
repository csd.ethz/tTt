/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CELLPATHCELL_Hs
#define CELLPATHCELL_H

#include "track.h"

/**
	@author Bernhard
	
	This struct holds a track pointer and two timepoints for displaying cell paths.
*/

struct CellPathCell{
public:
	CellPathCell()
		: track (0), tpStart (0), tpEnd (0)
		{}
	
	CellPathCell (Track *_track, int _tpStart, int _tpEnd)
		: track (_track), tpStart (_tpStart), tpEnd (_tpEnd)
		{}
	
	~CellPathCell()
		{}
	
	Track *track;
	
	int tpStart;
	int tpEnd;
};

#endif
