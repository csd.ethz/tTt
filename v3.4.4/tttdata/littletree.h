/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LITTLETREE_H
#define LITTLETREE_H

#include <qstring.h>
#include <qstringlist.h>
#include <q3intdict.h>
#include <q3ptrstack.h>
#include <qmap.h>
#include <QPair>


struct LittleCell {
	
	LittleCell()
		: number (-1), cluster_index (-1), parent (0), childIndex (-1), marked (false)
		{
			kids.setAutoDelete (true);
		}
	
	LittleCell (int _number, int _cluster_index, LittleCell *_parent = 0)
		: number (_number), cluster_index (_cluster_index), parent (_parent), childIndex (-1), marked (false)
		{
			kids.setAutoDelete (true);
		}
	
	LittleCell (int _number, int _cluster_index, LittleCell *_parent, int _childIndex)
		: number (_number), cluster_index (_cluster_index), parent (_parent), childIndex (_childIndex), marked (false)
		{
			if (parent)
				parent->kids.insert (_childIndex, this);
			
			kids.setAutoDelete (true);
		}
	
	~LittleCell()
		{}
	
	
	bool hasChildren() const
		{return (kids.count() > 0);}
	
	
	///the cell number
	int number;
	
	///the id of the cluster
	int cluster_index;
	
	///the parent cell
	LittleCell *parent;
	
	///the own child index
	int childIndex;
	
	///the kids
	Q3IntDict<LittleCell> kids;
	
	///for various purposes
	bool marked;
	
	
	///@return the generation of this cell
	int getGeneration();
	
	/**
	 * @return the number of generations following this cell (including this)
	 */
	int getGenerationCount();
	
	/**
	 * @return a list of all offspring cells, direct and indirect
	 */
	Q3IntDict<LittleCell> getAllChildren();
};





/**
	@author Bernhard Schauberger <bernhard.schauberger@campus.lmu.de>
	
	This class represents a tree.
	It is usually generated from a TreeMiner format string.
*/

class LittleTree {
public:
	LittleTree();
	
	/**
	 * constructs a tree from the provided tree in TreeMiner format (DFS)
	 * this is the counterpart to TreeTranslator::translateTree; yet it produces a LittleTree instead of a Tree object
	 * @param _tmTree the tree in TM format
	 * @param _setCellNumbers whether the cell numbers should be set (obtained while traversing the tree); only works for binary trees!!
	 */
	LittleTree (const QString &_tmTree, bool _setCellNumbers = false);
	
	~LittleTree();
	
	/**
	 * sets THIS tree from the provided tree in TreeMiner format (DFS)
	 * this is the counterpart to TreeTranslator::translateTree; yet it produces a LittleTree instead of a Tree object
	 * @param _tmTree the tree in TM format
	 * @param _setCellNumbers whether the cell numbers should be set (obtained while traversing the tree); only works for binary trees!!
	 * @return whether building the tree was successful (could fail due to not-binary trees or an empty string)
	 */
	bool set (const QString &_tmTree, bool _setCellNumbers = false);
	
	/**
	 * locates the embedded tree in this tree (also multiple times)
	 * @param _emb the subtree to be searched
	 * @param _count set to the number of instances found (0 if no times found)
	 * @param _induced whether the subtree is induced
	 * @return the starting cells of all subtree instances (the mother cells of the subtrees)
	 */
	Q3IntDict<LittleCell> locateEmbeddedTree (const LittleTree *_emb, int &_count, bool _induced);
	
	/**
	 * checks if the provided cell hierarchy is not violated in this tree
	 * @param _hierarchy the hierarchy to be tested
	 * @param _mark whether a violation should be marked (cells red)
	 * @return true = the hierarchy rules were not violated; false = the rules were violated
	 */
	bool checkHierarchy (LittleTree *_hierarchy, bool _mark = true);
	
	void resetCells (LittleCell *_cell = 0, bool _marked = false);
	
	void setBaseCell (LittleCell* _bC)
		{baseCell = _bC;}
	
	LittleCell* getBaseCell() const
		{return baseCell;}
	
	/**
	 * @return the little cells as an arrray in left order
	 */
	Q3IntDict<LittleCell> getCellsSerial() const;
	
	/**
	 * @return the number of generations in the tree (-> height)
	 */
	int getGenerationCount() const;

	void setTreeID ( int _ )
		{treeID = _;}
	
	int getTreeID() const
		{return treeID;}
	
	void setFrequency (int _freq)
		{frequency = _freq;}
	
	int getFrequency() const
		{return frequency;}
	
	
	/* *
	 * @return the number of branchings in the tree (-> width, maximum number of cells in a row)
	 */
	//int getBranchingWidth() const;
	
	///with automatic rule inference!
	void setHierarchyTree (bool _isOana = true);
	
	bool isHierarchyTree() const
		{return hierarchyTree;}
	
private:
	
	///the base cell of the tree; the rest is recursive
	LittleCell *baseCell;
	
	///any id of this tree, assigned from outside
	int treeID;
	
	///the absolute frequency of a subtree
	int frequency;
	
	///the original TreeMiner string from which this tree was generated
	///assigned in set()
	///very useful for searching of subtrees!
	QString tmString;
	
	///keeps a map for the position of a cell within the string to its corresponding LittleCell created from it
	QMap<int, LittleCell*> posToCell;
	
	///whether this is a hierarchy tree (default = false)
	bool hierarchyTree;
	
	///the hierarchy rules
	///only available if this is a hierarchy tree
	///usage: [cluster ID x cluster ID] |-> [generation comparison]
	QMap<QPair<int, int>, char> *hierarchy_rules;

	
	int testAllCellsForSubtree (LittleCell *_cell, const LittleTree *_emb, Q3IntDict<LittleCell> &_result);
	
	bool testOneCellForSubtree (LittleCell *_cell, const LittleCell *_embCell);
	
	void addHierarchyRule (int _nr1, int _nr2, char _comp);
	
	void addHierarchyRules (LittleCell *_cell);
	
	bool checkHierarchyForCell (LittleCell *_cell, LittleTree *_hierarchy, bool _mark = true);
	
	//LittleCell* getTreeAndMark (int _position, const LittleTree *_subtree);
};

#endif
