/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TREE_H
#define TREE_H

// Qt
#include <QSharedPointer>

#include "track.h"
#include "tttbackend/fileinfoarray.h"
#include "tttbackend/mathfunctions.h"

#include "tttbackend/itree.h"

#include <qapplication.h>

#include <qwidget.h>
#include <q3ptrvector.h>
#include <q3valuevector.h>
#include <qpainter.h>
#include <qpixmap.h>
#include <qimage.h>
#include <qpaintdevice.h>
#include <qcolor.h>
#include <qpoint.h>
#include <qpen.h>
#include <qbrush.h>
#include <qrect.h>
#include <qmessagebox.h>
#include <qmap.h>
#include <qsize.h>
//Added by qt3to4:
#include <Q3ValueList>
#include <q3intdict.h>
#include <math.h>
#include <QString>
#include <QVector>

///the number of days which causes the program to abort, if the log file suggests a time of the experiment longer than this
///necessary, for otherwise the program would crash without a note, if the log file is erroneous
const int CANCEL_DAYS = 30;


const QString TREE_OPT_START_TP = "StartTP"; //--------------------- Konstantin ------------------------
const QString TREE_OPT_STOP_TP = "StopTP"; //--------------------- Konstantin ------------------------
const QString TREE_OPT_LIFETIME = "TreeLifetime"; //--------------------- Konstantin ------------------------
const QString TREE_OPT_NUMBER_OF_CELLS = "NumberOfCells"; //--------------------- Konstantin ------------------------
const QString TREE_OPT_NUMBER_OF_GENERATIONS = "NumberOfGenerations"; //--------------------- Konstantin ------------------------

class TTTManager;
class TTTPositionManager;

/**
@author Bernhard

	The class for the stem cell stem tree in memory, containing all Track pointers to the 
	actual tracks and their data.
	All central cell access demands are managed by this class.
	Most routines are very efficient, only receiving the current tracks 
	 or trackpoints for a timepoint could be a bit slower.
	Must be a subclass of QObject for signal/slot handling.
*/


class Tree : public ITree
{

public:
	
	/**
	 * constructs an empty default tree
	 */
	Tree();
	
	~Tree();
	
	/**
	 * inserts _track into the tree, taking ownership of _track. If a track with the same number already exists, it will be replaced!
	 * @param _track the track that should be inserted (its mother track is already stored inside)
	 * @param _sendSignal whether the TreeSizeChanged() signal should be emitted
	 */
	void insert (Track *_track, bool _sendSignal = true);

	/**
	 * inserts _track, overloaded function
	 */
	void insert (QSharedPointer<Track> _track, bool _sendSignal = true);
	
	///deletes the provided track and all its children recursively
	///NOTE: deleteTrack() must not be called, as long as there are QIntDict<Track> copies around,
	///       cos those get worthless then as the Track objects are deleted from the heap, too
	///@param _track the track that should be deleted
	// /@param _noInteraction whether the user should be asked no security question, if the track has tracked children
	///@return success
	bool deleteTrack (Track *_track); //, bool _noInteraction = false);
	
	/**
	 * recursively inserts a track and all of its children into the tree
	 * @param _track the track to be inserted
	 * @param _sendSignal whether the TreeSizeChanged() signal should be emitted
	 * @return success
	 */
	bool insertComplexTrack (QSharedPointer<Track> _track, bool _sendSignal = true);
	
	///returns the mother cell of the whole tree
	///@return the Track object representing the base track of all tracks in this tree
	Track* getBaseTrack() const;
	
	///returns true if a track at _timePoint exists
	///@param _timePoint the query timepoint, if outside the tree's span, false is returned
	///@return true, if at _timePoint any track has a trackpoint set
	bool trackExists (int _timePoint) const;
	
	//@todo next two methods need to be faster (maybe create another hash (difficult!))
	///returns a list of all tracks at _timePoint
	///@param _timePoint the query timepoint
	///@return a QIntDict<Track> object which contains all Track objects alive at _timePoint
	Q3IntDict<Track>* timePointTracks (int _timePoint) const;
	
	///returns a list of all TrackPoints at _timePoint
	///@param _timePoint the query timepoint
	///@return a QValueList<TrackPoint> object that contains all track points from any Track at _timePoint
	Q3ValueList<TrackPoint>* timePointTrackPoints (int _timePoint) const;
	
	///returns the current number of tracks
	///@return the number of tracks in this tree
	int getTrackCount() const;
	
	///returns the number of tracks that are possible within the current number of generations
	///e.g. #(generations) = 5  =>  return == 63
	///@return the highest cell number that is possible, given the current generation number
	int getMaxPossibleTracks() const;
	
        ///returns the maximum number of tracks that are possible for the given max. track number
        ///e.g. #(generations) = 5  =>  return == 63
        ///@return the highest cell number that is possible, given the provided max. track number
        static int getMaxPossibleTracks (int _maxTrackNumber);

	/**
	 * recalculates the number of tracks in the tree (necessary if external changes occurred)
	 * @return the number of tracks (which is also stored in the tree)
	 */
	int recalculateNumberOfTracks();
	
	///returns the highest tracked trackpoint
	///@return the latest timepoint where the user has set a track
	int getMaxTrackedTimePoint() const;
	
	///returns the lowest tracked trackpoint
	///@return the first timepoint where the user has set a track
	int getMinTrackedTimePoint() const;
	
	//--------------------- Konstantin ------------------------
	///returns the value of tree attributes (cf. constants above)
	///@param _name name of the tree attribute
	/// @return value of tree attribute (in seconds except of the case TREE_OPT_NUMBER_OF_CELLS and TREE_OPT_NUMBER_OF_GENERATIONS)
	//int getOptValue(const QString _name, const TTTPositionManager *_tttpm) const;
	
	///returns the track pointer to the desired track with number _trackNumber
	///@param _trackNumber the track/cell number
	///@return a Track object representing the cell with number _trackNumber
	Track* getTrack (int _trackNumber) const;
	
	///deletes all tracks and recreates an empty tree
	///WARNING: assumes that the current data is saved or obsolete!
	void reset();
	
	///shifts the track numbers, starting at _startTrack
	///this one receives new number _newNumber, all numbers of its children 
	/// are calculated sequently (recursive)
	///@param _startTrack the track whose number should be shifted (is set to _newNumber)
	///@param _newNumber the new number for _startTrack
	///@return success
	bool shiftTrackNumbers (Track *_startTrack, int _newNumber);
	
	//@todo must be a lot faster (the whole process)
	///returns a list of tracks that exist at _timePoint
	///the routine simply checks if _timePoint lies between FirstTrace and LastTrace for each track
	///additional function:
	///all tracks with lower number than the provided _track are stored ascending,
	///all tracks with higher number than             _track are stored descending
	///=> the colocation lines are drawn from the farthest to the nearest track (view from the provided _track)
	///=> the red dots at the end of each line are not overdrawn and all lines are uniquely identifyable
	///    (at least in single cell colocation mode)
	///@param _timePoint the timepoint for which the analysis should be performed
	///@param _track the track from whose view the colocated tracks should be calculated
	///@return a QIntDict<Track> object that contains all tracks living in range with _track
	Q3IntDict<Track> coExistingTracks (int _timePoint, Track *_track) const;
	
	///calculates the normalized cell speed from the distance in pixel and the elapsed time
	///note: still needs to be multiplied by a speed amplifier for better visibility
	///@param _t1 the first trackpoint
	///@param _t2 the second trackpoint (of course, _t1 and _t2 should be of the same cell)
	///@param _speedAmplifier the factor with which any speed is multiplied for better visibility
	///@return an integer that describes the cell speed (calculated from time and distance between _t1 and _t2)
	int calcSpeed (const TrackPoint &_t1, const TrackPoint &_t2, int _speedAmplifier) const;
	
	///calculates the true (pix/sec or um/sec, resp.) cell speed from the distance and the elapsed time
	///@param _t1 the first trackpoint
	///@param _t2 the second trackpoint (of course, _t1 and _t2 should be of the same cell)
	///@param _tttpm the current position manager (if 0, the base position manager is assumed)
	///@return a float that describes the cell speed
	float calcTrueSpeed (const TrackPoint &_t1, const TrackPoint &_t2, const TTTPositionManager *_tttpm = 0) const;
	
	///calculates the color of the speed
	///@param _speed the speed (calculated by calcSpeed() and multiplied by a factor)
	///@return the color that should be assigned to the provided speed
	QColor calcSpeedColor (int _speed) const;
	
	///returns a dictionary of all tracks in the tree, index == number
	///@param _generation: how many generations/divisions away from the final fate
	///						should the returned cells be? (-1 == ALL)
	///@return a QIntDict<Track> object that contains all tracks that fulfill the specifications
	Q3IntDict<Track> getAllTracks (int _generations = -1) const;
	
	///returns the number of generations of children
	/// => == the number of generations away from the bottom
	///@param _track the query track
	///@param _count the accumulated count (for an end-recursive function); do not set when calling!!
	///@return the number of children generations (how many generations this cell really lives)
	int childrenGenerationsCount (Track *_track, int _count = 0) const;


	/**
	 * @return the number of generations in the tree (equal to the highest generation number)
	 */
	int getNumberOfGenerations() const;

	/**
	 * @Deprecated (still used in TTTPositionStatistics)
	 *
	 * returns the cell types that occur in this tree \n
	 * the value of 4 bytes has to be interpreted:
	 * each type has a dual value corresponding to one bit in the long; so if you want to know if e.g. apoptotic cells are contained,
	 *  then check for ([result] & CT_APOPTOTICTYPE != 0)\n
	 * @return a long value coding the cell types in this tree (see above) 
	 */
	long getCellTypes() const;
	
	/**
	 * @return the (absolute) filename currently associated with this tree
	 */
	QString getFilename() const;
	
	/**
	 * sets the filename associated with this tree
	 * currently only called in TTTFileManager while saving + loading
	 * @param _filename the absolute file path
	 */
	void setFilename (const QString &_filename);

	// --------------------- Konstantin -----------------------------
	/// returns name of experiment this tree belongs to
	const QString& getExperimentname() const
	{
		return experimentName;
	}
	// --------------------- Konstantin -----------------------------
	void setExperimentname(const QString &_expName)
	{
		experimentName = _expName;
	}

	void setPositionIndex(const QString &_positionName)
	{
		positionIndex = _positionName;
	}

	/**
	 * erases all position indices stored while tracking
	 * note: this does not affect the coordinates, the positions are then calculated as before
	 * necessary if by some other error the positions where the trackpoint was set is not correct anymore
	 */
	bool eraseStoredPositions();
	
	/**
	 * sets the maximum wavelength for this tree
	 * @param _maxWL the maximum wavelength
	 */
	void setMaxWavelength (int _maxWL)
		{maxWavelength = _maxWL;}
	
	/**
	 * @return the maximum wavelength
	 */
	int getMaxWavelength() const
		{return maxWavelength;}
	
	/**
	 * sets the ttt file version for this tree
	 * only called by TTTFileHandler
	 * @param _tttFileVersion the ttt file version number
	 */
	void setFileVersion (int _tttFileVersion)
		{fileVersion = _tttFileVersion;}
	
	/**
	 * @return the ttt file version number obtained when reading the tree
	 */
	int getFileVersion() const
		{return fileVersion;}

    /**
    * @return whether this tree is finished
    */
    bool isFinished() const
            {return treeFinished;}

    /**
    * sets whether this tree is finished
    */
    void setFinished (bool _finished)
            {treeFinished = _finished;}

	/**
	 * @return change log of this tree
	 */
	const QList<QPair<QDate,QByteArray> >& getChangelog() const {
		return changeLog;
	}

	/**
	 * @return pointer to last entry of changelog or NULL if the list is empty
	 */
	const QPair<QDate,QByteArray>* getLastChangelogEntry() const;

	/**
	 * Append new entry to changelog
	 * @param _newEntry the new entry. Contains current date and username as Ascii string (e.g. obtained from QString::toLatin1())
	 */
	void appendToChangelog(const QPair<QDate, QByteArray>& _newEntry) {
		changeLog.append(_newEntry);
	}

	/**
	 * set change log
	 */
	void setChangelog(const QList<QPair<QDate,QByteArray> >& _changeLog) {
		changeLog = _changeLog;
	}

	//////////////////////////////////////////////////////////////////////////
	// ITree interface
	//////////////////////////////////////////////////////////////////////////
	
	/**
	 * @return pointer to root node or 0 if root node is not available
	 */
	Track* getRootNode() const {
		// Look for track with number 1
		if(tracks.contains(1))
			return tracks[1].data();
		return 0;
	}

	/**
	 * @param _number the track number, can be invalid
	 * @return track with specified number or 0 if not available
	 */
	Track* getTrackByNumber(int _number) const {
		// Look for track with _number
		if(tracks.contains(_number))
			return tracks[_number].data();
		return 0;
	}

	/**
	 * @return max track number or -1 if not available
	 */
	int getMaxTrackNumber() const {
		if(MaxTrackNumber)
			return MaxTrackNumber;
		return -1;
	}

	/**
	 * @return number of tracks in this tree or -1 if not available
	 */
	int getNumberOfTracks() const {
		return tracks.size();
	}

	/**
	 * @return tree name or description (whatever this is) or empty string if not available
	 */
	QString getTreeName() const;

	/**
	 * @return experiment name, the experiment this tree belongs to
	 */
	QString getExperimentName() const;

	/**
	 * @returns the position index of the current tree
	 */
	QString getPositionIndex() const;


	/**
	 * Change modified state of tree
	 * @param _modified false if tree has been saved
	 */
	void setModified(bool _modified) {
		modified = _modified;
	}

	/**
	 * @return modified state of this tree
	 */
	bool getModified() const {
		return modified;
	}

	/**
	 * Get trackpoints at specified timepoint
	 * @param _timePoint the timepoint
	 * @return list of pointers to ITrackPoint objects (remain valid only as long as tree object exists)
	 */
	QList<ITrackPoint*> getTrackPointsAtTimePoint(int _timePoint) const;

	// OTODO:
//signals:
//	
//	///emitted when a track is inserted or deleted or anything else that makes the tree's size enlarge or shrink
//	/// (both in width and height)
//	void TreeSizeChanged();
//	
//	///emitted when a track is deleted
//	///all Track* instances somewhere else have to be reset to be assigned to 0 
//	/// (especially local QIntDict<Track> instances!)
//	///=> all objects that contain a local copy of some tracks, in any way, have to receive and process this signal
//	void tracksDeleted();
	

	/**
	 * Get a specific propertyy of this tree, not implemented here but in treewrapper
	 */
	QVariant getTreeProperty(QString _propName) const 
	{
		return QVariant();
	}

	/**
	 * Get a list of all possible properties, return only an empty list
	 * the real implementation is in treewrapper.cpp
	 */
	QVector<QString> getAvailableProperties() const
	{
		return QVector<QString>();
	}

	/**
	 * Specify names of additional attributes stored in each track point.
	 */
	void setAdditionalAttributeNames(const QVector<QString>& columns) 
	{
		additionalAttributeNames = columns;
	}

	/**
	 * Get names of additional attributes set in each track point (note that not every attribute has to be set in every track point)
	 */
	const QVector<QString>& getAdditionalAttributeNames() const
	{
		return additionalAttributeNames;
	}
	const QVector<QString>& getAdditionalAttributeNamesConst() const
	{
		return additionalAttributeNames;
	}
	QVector<QString>& getAdditionalAttributeNames()
	{
		return additionalAttributeNames;
	}


private:

	///the central array of tracks
	///this is the only storage place of all tracks 
	/// -> all other classes operate via Tree methods!
	/// OH 06.07.2011: now QHash with tracknumbers as keys
	QHash<int, QSharedPointer<Track> > tracks;

	///Typedefs for tracks iteration
	typedef QHash<int, QSharedPointer<Track> >::const_iterator tracks_const_iterator;
	typedef QHash<int, QSharedPointer<Track> >::iterator tracks_iterator;

	
	///the size (number of tracks) of the tree
	int NumTracks;
	
	///contains the highest track number in the tree
	///(updated every time a new track is inserted, but not necessarily when a track is removed!)
	uint MaxTrackNumber;
	
	///the base track, mother of all cells
	Track *BaseTrack;
	
	///the maximal cell speed
	int Max_Cell_Speed;
	
	///the filename currently associated with this tree (set while loading or save a Tree instance)
	///can be empty => new colony
	QString filename;

	//--------------------- Konstantin ------------------------
	/// name of experiment to which this tree belongs
	/// NOTE: this value is not stored in ttt files and is set in TTTStatistics::updateView()
	///       to make the calculations faster
	QString experimentName;


	// The position the tree belongs to
	QString positionIndex;


	///holds the maximum wavelength for this tree
	///set while loading (in tttfilehandler) or when creating a new tree
	///currently always set to MAX_WAVE_LENGTH
	int maxWavelength;
	
	///the file version of this tree (is only set in TTTFileHandler)
	int fileVersion;
	
    ///BS 2010/08/11: a finished marker for the current tree
    ///user setable, is stored in the ttt files
    bool treeFinished;

	///OH 2011/04/26: change log of tree
	QList<QPair<QDate,QByteArray> > changeLog;

	// If tree has been modified and changes have not been saved yet or is a new tree
	// IMPORTANT: there is absolutely no guarantee that this variable is always set correctly!
	bool modified;
	
///private methods:	
	
	
	///deletes the track with all its children recursively
	///@param _track the track to be deleted
	void delTrack (Track *_track);
	
	///assigns a new number to a track
	///its children are updated recursively
	///@param _startTrack the track to be updated
	///@param _newNumber the new number for _startTrack
	void assignNewNumber (Track *_startTrack, int _newNumber);

	/**
	 * @Deprecated (still used in TTTPositionStatistics)
	 *
	 * returns the cell types contained in this tree (for the coding in one long value, confer @see getCellTypes() above)
	 * is recursively called for the children of the provided track
	 * @param _track the track which should be analyzed
	 * @param _result the result up to now
	 * @return the result OR-ed together with the fates of the provided cell
	 */
	long getCellTypesRecursive (Track *_track, long _result) const;
	
	/**
	 * recursive internal embedded method for counting the number of tracks
	 * @param _track the starting track
	 * @return the total number of tracks, if called with BaseTrack
	 */
	int recalcTrackCountInternal (Track *_track);

	// Additional (arbitrary) data columns (per track point, each TrackPoint can contain corresponding double values),
	// are stored in separate quantification file if set
	QVector<QString> additionalAttributeNames;

	// Forbidden:
	Tree(const Tree&);
	Tree& operator=(const Tree&);

	// Friend classes
	friend Track;
};


inline Track* Tree::getBaseTrack() const
{
	return BaseTrack;
}

inline int Tree::getTrackCount() const
{
	return NumTracks;
}

inline QString Tree::getFilename() const
{
	return filename;
}

inline void Tree::setFilename (const QString &_filename)
{
	filename = _filename;
}




#endif
