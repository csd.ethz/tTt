/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "userinfo.h"

#include "tttio/xmlhandler.h"
#include "tttbackend/tttmanager.h"
#include "tttgui/tttuserqueryform.h"

// Static member variable definitions
UserInfo * UserInfo::inst (0);
const char* UserInfo::USER_CONFIG_FILENAME = "tttconfig.xml";
const char* UserInfo::NEW_USER_CONFIG_FILENAME = "tttconfig.ini";

// Settings keys
const char* UserInfo::KEY_TREEVIEW_TRACKLINEWIDTH = "treeview.treelinewidth";
const char* UserInfo::KEY_TREEVIEW_TREENAMECOLOR = "treeview.treenamecol";
const char* UserInfo::KEY_TREEVIEW_TREENAMEFONTSIZE = "treeview.treenamefontsize";
const char* UserInfo::KEY_TREEVIEW_TRACKNUMCOLOR = "treeview.tracknumcol";
const char* UserInfo::KEY_TREEVIEW_TRACKNUMFONTSIZE = "treeview.tracknumfontsize";
const char* UserInfo::KEY_TREEVIEW_TRACKLINESCONTINUOUS = "treeview.tracklinescont";
const char* UserInfo::KEY_TREEVIEW_CELLCIRCLEMOUSEHOVEROUTERCOLOR = "treeview.cellcirclesmousehoveroutercol";
const char* UserInfo::KEY_TREEVIEW_CELLCIRCLESELECTEDINNERCOLOR = "treeview.cellcirclesselectedinnercol";
const char* UserInfo::KEY_TREEVIEW_CELLCIRCLESELECTEDOUTERCOLOR = "treeview.cellcirclesselectedoutercol";
const char* UserInfo::KEY_TREEVIEW_CELLCIRCLEDEFAULTINNERCOLOR = "treeview.cellcirclesdefaultinnercol";
const char* UserInfo::KEY_TREEVIEW_CELLCIRCLEDEFAULTOUTERCOLOR = "treeview.cellcirclesdefaultoutercol";
const char* UserInfo::KEY_TREEVIEW_CELLCIRCLEOUTLINEWIDTH = "treeview.cellcirclesoutlinewidth";
const char* UserInfo::KEY_TREEVIEW_CELLCIRCLERADIUS = "treeview.cellcircleradius";
const char* UserInfo::KEY_TREEVIEW_TIMELINELINECOLOR = "treeview.timelinelinecol";
const char* UserInfo::KEY_TREEVIEW_TIMELINEBACKGROUNDCOLOR = "treeview.timelinebackgroundcol";
const char* UserInfo::KEY_TREEVIEW_TIMELINELINEWIDTH = "treeview.timelinelinewidth";
const char* UserInfo::KEY_TREEVIEW_TIMELINEFONTCOL = "treeview.timelinefontcol";
const char* UserInfo::KEY_TREEVIEW_TIMELINEFONTSIZE = "treeview.timelinefontsize";
const char* UserInfo::KEY_TREEVIEW_MAINTRACKLINESDISTANCE = "treeview.maintracklinedistance";
const char* UserInfo::KEY_TREEVIEW_TRACKLINEDISTANCE = "treeview.tracklinedistance";
const char* UserInfo::KEY_TREEVIEW_SYMBOLCELLDEATH = "treeview.symbolcelldeath";
const char* UserInfo::KEY_TREEVIEW_SYMBOLCELLDEATHCOLOR = "treeview.symbolcelldeathcolor";
const char* UserInfo::KEY_TREEVIEW_SYMBOLLOST = "treeview.symbollost";
const char* UserInfo::KEY_TREEVIEW_SYMBOLLOSTCOLOR = "treeview.symbollostcolor";
const char* UserInfo::KEY_TREEVIEW_SYMBOLSFONTSIZE = "treeview.symbolsfontsize";
const char* UserInfo::KEY_TREEVIEW_SHOWREALTIME = "treeview.showrealtime";
const char* UserInfo::KEY_TREEVIEW_HEIGHTFACTOR = "treeview.heightfactor";
const char* UserInfo::KEY_STATS_LAST_LOGFILE_FOLDER = "stats.logfilefolder";
const char* UserInfo::KEY_STATS_LAST_EXPERIMENTSFILES_FOLDER = "stats.experimentsfilesfolder";
const char* UserInfo::KEY_STATS_LAST_NAS_FOLDER = "stats.nasfolder";
const char* UserInfo::KEY_STATS_LAST_SHOWN_CHANGELOG = "stats.lastchangelogversion";
const char* UserInfo::KEY_TTTMOVIE_USECOMMENTASWL = "tttmovie.usecommentaswl";
const char* UserInfo::KEY_TTTMOVIEEXPLORER_PATH = "tttmovieexplorer.path";
const char* UserInfo::KEY_TTTCONVERTTREES_INPATH = "tttconverttrees.in";
const char* UserInfo::KEY_TTTCONVERTTREES_OUTPATH = "tttconverttrees.out";
const char* UserInfo::KEY_TTTAUTOTRACKING_SELECTCONNECTED = "tttautotracking.autoselection";
const char* UserInfo::KEY_TTTEXPORTFLUORESCENCEPATCHES_TARGETDIR = "tttexportfluorescencepatches.targetdir";

UserInfo::UserInfo()
	: usersign (""), addOn_bitpack (0), userConfigFilePath ("")
{
	userSettings = 0;

	localSettings = new QSettings("tTt", "tTt");
}

UserInfo::~UserInfo()
{
	writeUserConfigFile();
}

UserInfo& UserInfo::getInst()
{
	if (! inst)
		inst = new UserInfo();
	
	return (*inst);
}

void UserInfo::cleanup()
{
	// user settings
	if(inst && inst->userSettings) {
		delete inst->userSettings;
		inst->userSettings = 0;
	}

	// local settings
	if(inst && inst->localSettings) {
		delete inst->localSettings;
		inst->localSettings = 0;
	}

	if (inst) {
		delete inst;
		inst = 0;
	}
}


QString UserInfo::getUsersign (bool *_cancelled, const QString &_path, bool neverOpenQueryWindow )
{
	if (! usersign.isEmpty() || neverOpenQueryWindow )
		return usersign;
	else {
		//query usersign
		if (_cancelled)
			*_cancelled = false;
		
		//BS 2010/03/16 new method
		//open a form where the user can only select his sign in a combobox
		TTTUserQueryForm *uqf = new TTTUserQueryForm (this, _path);
		uqf->exec();

		if (! usersign.isEmpty())
			readUserConfigFile();
		else
			if (_cancelled)
				*_cancelled = true;
		
		return usersign;
	}
}

void UserInfo::setUsersign (const QString &_userSign)
{
	usersign = _userSign.upper();
}

bool UserInfo::readUserConfigFile()
{
	//load configuration file for the current user
	//note: an endless loop is already avoided by not providing _cancelled
	QString usersignL = getUsersign();
	
        //SystemInfo::checkNcreateDirectory (TTTManager::getInst().getNASDrive() + "/Configs", true, false);
	QString nasDrive = TTTManager::getInst().getNASDrive();
	if(nasDrive.right(1) != "/")
		nasDrive.append('/');

    QDir nas ( nasDrive + "Configs");
	
	//if (nas.exists()) {
	if( SystemInfo::checkNcreateDirectory(nas.absolutePath(), true, false) ) {
		//bool folderexists = nas.cd (usersignL);
		//if (! folderexists) {
		//	nas.mkdir (usersignL);
		//	nas.cd (usersignL);
		//}
		userConfigFilePath = nas.absPath() + "/" + usersignL;
		QDir dirUserConfigFilePath(userConfigFilePath);
		if(!dirUserConfigFilePath.exists())
			dirUserConfigFilePath.mkpath(dirUserConfigFilePath.absolutePath());

		// OH-05-10-11: New configuration ini file based on QSettings used in parallel
		if(!userSettings) {
			// Load userSettings
			QString newSettingsFile = userConfigFilePath + "/" + NEW_USER_CONFIG_FILENAME;
			userSettings = new QSettings(newSettingsFile, QSettings::IniFormat);

			// Load defaults for not contained settings
			setDefaultSettings();
		}

		userConfigFilePath = userConfigFilePath + "/" + USER_CONFIG_FILENAME;
		
		//bool userinfo_read = XMLHandler::readUserInfoXML (userConfigFilePath);
		bool userinfo_read = XMLHandler::readXML2DOM(userConfigFilePath, userConfig);

		if (! userinfo_read) {
			//display warning?
			//QMessageBox::warning (0, "tTt warning", "Your configuration file was not found.\nPlease contact your favourite programmer.", "OK");

			// Create new one
			QDomDocument doc ("UserInfo");
			QDomElement root = doc.createElement ("UserInformation");
			doc.appendChild (root);
			
			//write user name
			QDomElement de = doc.createElement ("user");
			de.setAttribute ("name", UserInfo::getInst().getUsersign());
			root.appendChild (de);

			// Set new dom document
			userConfig = doc;
		}

		return true;
	}
	
	return false;
}

bool UserInfo::writeUserConfigFile()
{
	return XMLHandler::writeDOMDocument(userConfigFilePath, userConfig);
}

const QString UserInfo::getUserConfigDirectory()
{
	QString usersignL = getUsersign();
	QDir nas (TTTManager::getInst().getNASDrive() + "/Configs");
	if (nas.exists())
		return nas.absPath() + "/" + usersignL + "/";
	else
		return "";
}

void UserInfo::setStyleSheet (StyleSheet &_ss)
{
	//creates a deep copy!
	styleSheet = _ss;
}

const QString UserInfo::getUserStyleSheetPath()
{
	QString usersignL = getUsersign();
	
	QString filename = "";
	
	QDir nas (TTTManager::getInst().getNASDrive() + "/Configs");
	if (nas.exists()) {
		filename = nas.absPath() + "/" + STYLESHEET_FILENAME.arg (usersignL);
	}
	else {
		filename = SystemInfo::homeDirectory() + STYLESHEET_FILENAME.arg (usersignL);
	}
	
	return filename;
}

bool UserInfo::readUserStyleSheet()
{
	QString filename = getUserStyleSheetPath();
	
	if (! filename.isEmpty())
		return styleSheet.load (filename);
	else
		return false;
}

bool UserInfo::saveUserStyleSheet()
{
	QString filename = getUserStyleSheetPath();
	
	if (! filename.isEmpty())
		return styleSheet.save (filename);
	else
		return false;
}


bool UserInfo::saveWindowPosition(const QWidget* _window, const QString& _windowName)
{
	// Check params
	if(!_window || _windowName.isEmpty())
		return false;

	// Check dom object
	if(userConfig.isNull())
		return false;

	// Get dom object
	QDomElement windowDomElement;
	QDomElement root = userConfig.documentElement();
	if(root.isNull())
		return false;

	// Look for element for our window
	QDomNodeList children = root.childNodes();
	for(int i = 0; i < children.count(); ++i) {
		QDomNode n = children.item (i);
		QDomElement e = n.toElement();
		if(e.tagName() == _windowName) {
			windowDomElement = e;
			break;
		}
	}

	// Create element if not found
	if(windowDomElement.isNull()) {
		windowDomElement = userConfig.createElement(_windowName);
		root.appendChild(windowDomElement);
	}

	// Get position
	QPoint pos = _window->pos();

	// Get size
	QSize size = _window->size();
	bool showMaximized = _window->isMaximized();

	//// Change dom object
	//windowDomElement.setAttribute("PosX", pos.x());
	//windowDomElement.setAttribute("PosY", pos.y());
	//windowDomElement.setAttribute("SizeX", size.width());
	//windowDomElement.setAttribute("SizeY", size.height());
	//windowDomElement.setAttribute("Maximized", showMaximized);

	QByteArray geometry = _window->saveGeometry();
	QString asText(geometry.toHex());
	windowDomElement.setAttribute("WindowGeometry", asText);

	return true;
}

bool UserInfo::loadWindowPosition(QWidget* _window, const QString& _windowName)
{
	// Check params
	if(!_window || _windowName.isEmpty())
		return false;

	// Check dom object
	if(userConfig.isNull())
		return false;

	// Get dom object
	QDomElement windowDomElement;
	QDomElement root = userConfig.documentElement();
	if(root.isNull())
		return false;

	// Look for element for our window
	QDomNodeList children = root.childNodes();
	for(int i = 0; i < children.count(); ++i) {
		QDomNode n = children.item (i);
		QDomElement e = n.toElement();
		if(e.tagName() == _windowName) {
			windowDomElement = e;
			break;
		}
	}

	if(windowDomElement.isNull())
		return false;

	// Get data
	QString strGeom = windowDomElement.attribute("WindowGeometry");

	if(!strGeom.isEmpty()) {
		QByteArray tmp(strGeom.toAscii());
		QByteArray geometry = QByteArray::fromHex(tmp);

		return _window->restoreGeometry(geometry);
	}

	return false;
}

QSettings* UserInfo::getUserSettings() const
{
	// Write to log file if something is wrong
	if(userSettings && userSettings->status() != QSettings::NoError)
		qWarning() << "tTt Warning: " << __FUNCTION__ << " userSettings are in error state: " << userSettings->status();

	return userSettings;
}

QVariant UserInfo::getUserSettingByKey( const QString& _key ) const
{
	// Write to log file if something is wrong
	if(!userSettings)
		qWarning() << "tTt Warning: " << __FUNCTION__ << " no userSettings.";
	else if(userSettings->status() != QSettings::NoError)
		qWarning() << "tTt Warning: " << __FUNCTION__ << " userSettings are in error state: " << userSettings->status();

	// Check if userSettings is available
	if(userSettings) 
		return userSettings->value(_key);
	else
		return QVariant();
}

void UserInfo::setDefaultSettings()
{
	// Check for errors
	if(!userSettings) {
		qWarning() << "tTt Warning: " << __FUNCTION__ << " no userSettings.";
		return;
	}
	else if(userSettings->status() != QSettings::NoError) {
		qWarning() << "tTt Warning: " << __FUNCTION__ << " userSettings are in error state: " << userSettings->status();
		return;
	}

	// Set default values
	setDefaultSetting(KEY_TREEVIEW_TRACKLINEWIDTH, 30);
	setDefaultSetting(KEY_TREEVIEW_TREENAMECOLOR, Qt::black);
	setDefaultSetting(KEY_TREEVIEW_TREENAMEFONTSIZE, 70);
	setDefaultSetting(KEY_TREEVIEW_TRACKNUMCOLOR, Qt::black);
	setDefaultSetting(KEY_TREEVIEW_TRACKNUMFONTSIZE, 80);
	setDefaultSetting(KEY_TREEVIEW_TRACKLINESCONTINUOUS, false);
	setDefaultSetting(KEY_TREEVIEW_CELLCIRCLEMOUSEHOVEROUTERCOLOR, Qt::blue);
	setDefaultSetting(KEY_TREEVIEW_CELLCIRCLESELECTEDINNERCOLOR, Qt::blue);
	setDefaultSetting(KEY_TREEVIEW_CELLCIRCLESELECTEDOUTERCOLOR, Qt::blue);
	setDefaultSetting(KEY_TREEVIEW_CELLCIRCLEDEFAULTINNERCOLOR, Qt::white);
	setDefaultSetting(KEY_TREEVIEW_CELLCIRCLEDEFAULTOUTERCOLOR, Qt::black);
	setDefaultSetting(KEY_TREEVIEW_CELLCIRCLEOUTLINEWIDTH, 15);
	setDefaultSetting(KEY_TREEVIEW_CELLCIRCLERADIUS, 50);
	setDefaultSetting(KEY_TREEVIEW_TIMELINELINECOLOR, QColor(0xE0, 0xE0, 0xE0));
	setDefaultSetting(KEY_TREEVIEW_TIMELINEBACKGROUNDCOLOR, Qt::white);
	setDefaultSetting(KEY_TREEVIEW_TIMELINELINEWIDTH, 10);
	setDefaultSetting(KEY_TREEVIEW_TIMELINEFONTCOL, Qt::black);
	setDefaultSetting(KEY_TREEVIEW_TIMELINEFONTSIZE, 80);
	setDefaultSetting(KEY_TREEVIEW_MAINTRACKLINESDISTANCE, 400);
	setDefaultSetting(KEY_TREEVIEW_TRACKLINEDISTANCE, 1);
	setDefaultSetting(KEY_TREEVIEW_SYMBOLCELLDEATH, QString("X"));
	setDefaultSetting(KEY_TREEVIEW_SYMBOLCELLDEATHCOLOR, Qt::red);
	setDefaultSetting(KEY_TREEVIEW_SYMBOLLOST, QString("?"));
	setDefaultSetting(KEY_TREEVIEW_SYMBOLLOSTCOLOR, Qt::red);
	setDefaultSetting(KEY_TREEVIEW_SYMBOLSFONTSIZE, 70);
	setDefaultSetting(KEY_TTTMOVIE_USECOMMENTASWL, false);
	setDefaultSetting(KEY_TTTMOVIEEXPLORER_PATH, QString(""));
	setDefaultSetting(KEY_TTTAUTOTRACKING_SELECTCONNECTED, false);
}

void UserInfo::setDefaultSetting( const char* _key, const QVariant& _value )
{
	// Set value if not set yet
	if(!userSettings->contains(_key))
		userSettings->setValue(_key, _value);
}

QSettings* UserInfo::getLocalSettings() const
{
	return localSettings;
}

