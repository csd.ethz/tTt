/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TRACKINGKEYS_H
#define TRACKINGKEYS_H

#include <qstringlist.h>
#include <qmap.h>

const int NUMBER_OF_TRACKING_KEYS = 15;

/**
	@author Bernhard
*/
class TrackingKeys{
public:
	static TrackingKeys *inst;
	
	TrackingKeys();
	
	~TrackingKeys();
	
	///returns all keys that can be manipulated arbitrarily
	///@return a QStringList object with all key names (character codes)
	static const QStringList getKeys();
	
	/**
	 * returns the unique value that is assigned to this key (is hidden from the user of tTt!)
	 * @param _key the key code, e.g. 'Q'
	 * @return the value assigned to this key
	 */
	static short int getValue (const char _key);

        // ------------- Konstantin -----------------
        /**
         * overloaded function
         */
        static short int getValue (const QChar _key);
	
	/**
	 * returns the unique key that is assigned with the provided value
	 * @param _value the value (usually coming from TrackPoint)
	 * @return the key associated with this value
	 */
	static char getKeyFromValue (short int _value);
	
private:
	
	///holds the mapping of keys to values
	QMap<QString, Q_INT8> map;
};

#endif
