/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/***************************************************************************
 *   Copyright (C) 2008 by Bernhard Schauberger   *
 *   bernhard.schauberger@campus.lmu.de   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "littletree.h"

#include <iostream>

int LittleCell::getGeneration()
{
	LittleCell *up = this;
	int gen = 0;
	
	while ((up = up->parent))	//assignment!
		gen++;
	
	return gen;
}

int LittleCell::getGenerationCount()
{
	if (! hasChildren())
		return 1;
	else {
		int max = 0;
		
		for (uint kid = 1; kid <= kids.count(); kid++) {
			if (kids.find (kid))
				max = QMAX (max, 1 + kids.find (kid)->getGenerationCount());
		}
		
		return max;
	}
}

Q3IntDict<LittleCell> LittleCell::getAllChildren()
{
	Q3IntDict<LittleCell> result;
	uint count = 0;
	
	for (uint kid = 1; kid <= kids.count(); kid++) {
		if (kids.find (kid)) {
			
			if (count >= result.size()) {
				result.resize (count * 2 + 1);
			}
			result.insert (count, kids.find (kid));
			count++;
			
			//recursive for all kids
			Q3IntDict<LittleCell> tmp = kids.find (kid)->getAllChildren();
			
			if (tmp.count() > 0) {
				//add children results
				for (uint x = 0; x < tmp.count(); x++) {
					if (tmp.find (x)) {
						if (count >= result.size()) {
							result.resize (count * 2 + 1);
						}
						result.insert (count, tmp.find (x));
						count++;
					}
				}
			}
		}
	}
	
	return result;
}






LittleTree::LittleTree()
	: hierarchyTree (false), hierarchy_rules (0)
{
}

LittleTree::LittleTree (const QString &_tmTree, bool _setCellNumbers)
	: hierarchyTree (false), hierarchy_rules (0)
{
	set (_tmTree, _setCellNumbers);
}

LittleTree::~LittleTree()
{
}

bool LittleTree::set (const QString &_tmTree, bool _setCellNumbers)
{
	//The TreeMiner format is a DFS format
	//e.g.: 5 9 3 -1 7 -1 -1 8
	//where continuing numbers always branch down to the left, and -1 means "one level up"
	
	
	//NOTE: as sometimes trees are found by TreeMiner which are not binary (but ternary, ...), and there is currently no way to deal with them, we omit these trees
	
	posToCell.clear();
	
	QStringList cells = QStringList::split (" ", _tmTree.stripWhiteSpace());
	
	//keeps the current position within the string
	//the string provided to this method contains the tree only, e.g. "2 4 -1 14"
	int posInString = 0;
	
	int cc = 0;
	int cell_number = -1;
	
	bool start = true;
	LittleCell *start_cell = 0;
	LittleCell *cur_cell = 0;
    	for ( QStringList::Iterator iter = cells.begin(); iter != cells.end(); ++iter) {
    		cc++;
    		int cell_index = (*iter).toInt();
    		
    		if (start) {
    			//cell with no parent
    			
    			if (_setCellNumbers)
    				cell_number = 1;
    			
    			start_cell = new LittleCell (cell_number, cell_index, 0);
    			cur_cell = start_cell;
    			start = false;
    			
    			posToCell [posInString] = start_cell;
    		}
    		else {
    			if (cell_index == -1) {
    				//one level up
    				cur_cell = cur_cell->parent;
    				
    				if (_setCellNumbers)
    					cell_number /= 2;
    			}
    			else if (cell_index > 0) {
    				//new cell
    				
    				//create a new kid and set the new cell the current cell
    				//kid indices start with 1
    				int kidcount = cur_cell->kids.count();
    				
    				if (_setCellNumbers) {
					cell_number *= 2;
    					if (kidcount > 0)
    						cell_number += 1;
    				}
    				
    				cur_cell = new LittleCell (cell_number, cell_index, cur_cell, kidcount + 1);
    				
    				posToCell [posInString] = cur_cell;
    			}
    			
    			if (! cur_cell) {
    				//std::cout << "0 reached!" << std::endl;
    				break;
    			}
    		}
    		
    		//move forward in string (virtually only)
    		//length of currently read cell id + space length
    		posInString += (*iter).length() + 1;
    	}
    	
    	baseCell = start_cell;
    	
    	tmString = _tmTree;
    	
    	if (! start_cell)
    		return false;
    	else
    		return true;
}

Q3IntDict<LittleCell> LittleTree::getCellsSerial() const
{
	Q3IntDict<LittleCell> result;
	
	if (! baseCell)
		return result;
	
	uint cc = 0;
	Q3PtrStack<LittleCell> stackl;
	stackl.push (baseCell);
	
	while (! stackl.isEmpty()) {
		
		//"zein" is how "zelle" is pronounced in Bavarian tongue...
		LittleCell *zein = stackl.pop();
		
		result.insert (cc, zein);
		cc++;
		if (cc >= result.size())
			result.resize (cc * 2);
		
		for (uint kid = 1; kid <= zein->kids.count(); kid++) {
			if (zein->kids.find (kid))
				stackl.push (zein->kids.find (kid));
		}
	}
	
	return result;
}

int LittleTree::getGenerationCount() const
{
	if (baseCell)
		return baseCell->getGenerationCount();
	else
		return -1;
}

/*int LittleTree::getBranchingWidth() const
{
	
}
*/

//recursive
bool LittleTree::testOneCellForSubtree (LittleCell *_cell, const LittleCell *_embCell)
{
	if ((_cell != 0) & (_embCell != 0)) {
		if (_cell->cluster_index == _embCell->cluster_index) {
			
			//it is not yet sure if the marking is correct, but if not, it is just reset afterwards, which is much easier than traversing the tree again
			_cell->marked = true;
			
			if (! _embCell->hasChildren()) {
				return true;
			}
			else if (_embCell->kids.count() <= 2) {
				
				LittleCell *kid1 = _cell->kids.find (1);
				LittleCell *kid2 = _cell->kids.find (2);
				LittleCell *emb_kid1 = _embCell->kids.find (1);
				LittleCell *emb_kid2 = _embCell->kids.find (2);
				
				//now test all possible mappings of the children onto the subtree - if one works, the tree is contained
				if (_embCell->kids.count() == 1) {
					if (testOneCellForSubtree (kid1, emb_kid1) || testOneCellForSubtree (kid2, emb_kid1))
						return true;
				}
				else if (_embCell->kids.count() == 2) {
					if (testOneCellForSubtree (kid1, emb_kid1) && testOneCellForSubtree (kid2, emb_kid2))
						return true;
					else if (testOneCellForSubtree (kid2, emb_kid1) && testOneCellForSubtree (kid1, emb_kid2))
						return true;
				}
			}
			//embedded trees with more kids do not work yet
		}
	}
	
	return false;
}

//recursive
int LittleTree::testAllCellsForSubtree (LittleCell *_cell, const LittleTree *_emb, Q3IntDict<LittleCell> &_result)
{
	int sum = 0;
	if ((_cell != 0) & (_emb != 0)) {
		if (_cell->cluster_index == _emb->baseCell->cluster_index) {
			//check if this cell supports the complete tree
			if (testOneCellForSubtree (_cell, _emb->baseCell)) {
				sum = 1;
				
				uint cc = _result.count();
				if (cc >= _result.size())
					_result.resize (cc * 2 + 1);
				
				_result.insert (cc, _cell);
			}
			else
				//delete all marks downwards
				//this works as the cells are tested downwards for subtree containment
				resetCells (_cell);
		}
		
		//test also the daughter cells recursively
		for (uint kid = 1; kid <= _cell->kids.count(); kid++)
			if (_cell->kids.find (kid))
				sum += testAllCellsForSubtree (_cell->kids.find (kid), _emb, _result);
	}
	
	return sum;
}

Q3IntDict<LittleCell> LittleTree::locateEmbeddedTree (const LittleTree *_emb, int &_count, bool _induced)
{

	Q3IntDict<LittleCell> result;
	
	int count = 0;
	
	if (_emb) {
		if (_induced) {
			count = testAllCellsForSubtree (baseCell, _emb, result);
		}
		//for embedded trees it's much harder...
	}
	
	_count = count;
	return result;
}

void LittleTree::resetCells (LittleCell *_cell, bool _marked)
{
	if (! _cell)
		///NOTE danger of infinite recursion due to default parameter
		_cell = baseCell;
	
	
	if (_cell) {
		_cell->marked = _marked;
		
		for (uint kid = 1; kid <= _cell->kids.count(); kid++)
			if (_cell->kids.find (kid))
				resetCells (_cell->kids.find (kid), _marked);
		
	}
}

void LittleTree::setHierarchyTree (bool _isOana)
{
	hierarchyTree = _isOana;
	
	if (hierarchyTree) {
		//infer rules from this tree
		//a rule says: the generation of cluster ID a must be lower than the generation of cluster ID b
		//simple method for gaining all rules:
		//all pairs of cells/clusterIDs are scanned
		// - if cell b is a descendant of cell a, then the rule "gen[ID a] < gen[ID b]" is added
		
		//perform the test for all cells recursively
		addHierarchyRules (baseCell);
		
		//debug
		QMap<QPair<int, int>, char>::Iterator iter;
		for (iter = hierarchy_rules->begin(); iter != hierarchy_rules->end(); ++iter) {
			qDebug ("key = %d,%d; value = %d", iter.key().first, iter.key().second, iter.data());
		}
	}
	
}

//recursive
void LittleTree::addHierarchyRules (LittleCell *_cell) 
{
	if (! _cell)
		return;
	
	int nr1 = _cell->cluster_index;
	
	//for all offspring cells (direct or in higher generations): add rule <nr1,nr_offspring> |-> "<"
	Q3IntDict<LittleCell> kids = _cell->getAllChildren();
	for (uint i = 0; i < kids.count(); i++) {
		if (kids.find (i)) {
			addHierarchyRule (nr1, kids.find (i)->cluster_index, '<');
			addHierarchyRule (kids.find (i)->cluster_index, nr1, '>');
		}
	}
	
	
	//call recursively for all direct offspring cells
	for (uint kid = 1; kid <= _cell->kids.count(); kid++)
		if (_cell->kids.find (kid))
			addHierarchyRules (_cell->kids.find (kid));
}

void LittleTree::addHierarchyRule (int _nr1, int _nr2, char _comp)
{
	if (! hierarchy_rules) {
		hierarchy_rules = new QMap<QPair<int, int>, char>();
	}
	
	if (hierarchy_rules)
		(*hierarchy_rules) [QPair<int, int> (_nr1, _nr2)] = _comp;
	
}

bool LittleTree::checkHierarchy (LittleTree *_hierarchy, bool _mark)
{
	//check if this tree fulfills the constraints/rules of the given hiearchy
	
	if (! _hierarchy)
		return false;
	
	return checkHierarchyForCell (baseCell, _hierarchy, _mark);
}

bool LittleTree::checkHierarchyForCell (LittleCell *_cell, LittleTree *_hierarchy, bool _mark)
{
	if (! _cell)
		return false;
	if (! _hierarchy)
		return false;
	
	int nr1 = _cell->cluster_index;
	int gen1 = _cell->getGeneration();
	
	bool result = true;
	
	//for all offspring cells (direct or in higher generations): check if rule <nr1,nr_offspring> exists and if yes, check for fulfillment
	Q3IntDict<LittleCell> kids = _cell->getAllChildren();
	for (uint i = 0; i < kids.count(); i++) {
		if (kids.find (i)) {
			int nr2 = kids.find (i)->cluster_index;
			QPair<int, int> pair = QPair<int, int> (nr1, nr2);
			
			if (_hierarchy->hierarchy_rules->contains (pair)) {
				char comp = (*_hierarchy->hierarchy_rules) [pair];
				int gen2 = kids.find (i)->getGeneration();
				
				//check for rule violations
				switch (comp) {
					case '<':
						if (gen1 >= gen2)
							result = false;
						break;
					case '>':
						if (gen1 <= gen2)
							result = false;
						break;
					default:
						;
				}
				
				if (! result)
					break;
			}
		}
	}
	
	if (! result) {
		if (_mark)
			resetCells (baseCell, true);
	}
	else {
		//call recursively for all direct offspring cells
		for (uint kid = 1; kid <= _cell->kids.count(); kid++)
			if (_cell->kids.find (kid))
				result &= checkHierarchyForCell (_cell->kids.find (kid), _hierarchy, _mark);
	}
	
	return result;
}

