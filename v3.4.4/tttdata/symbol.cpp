/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "symbol.h"


Symbol::~Symbol()
{
}

int Symbol::getTurningAngle() const
{
        int angle2 = angle;

        switch (symbol) {
                //the angles are added up, depending on the jump-in location
                case SYMB_ARROWDOWN:
                        angle2 += 90;    //finally 270
                case SYMB_ARROWRIGHT:
                        angle2 += 90;    //finally 180
                case SYMB_ARROWUP:
                        angle2 += 90;    //finally 90
                case SYMB_ARROWLEFT:

                        break;


                case SYMB_TRIANGLEDOWN:
                        angle2 += 90;
                case SYMB_TRIANGLERIGHT:
                        angle2 += 90;
                case SYMB_TRIANGLEUP:
                        angle2 += 90;
                case SYMB_TRIANGLELEFT:

                        break;

                default:
                        ;
        }

        return angle2;
}

QPolygonF Symbol::drawPolygon (int _cellDiameter) const
{
        QPolygonF poly;

        switch (symbol) {
                case SYMB_ARROWLEFT:
                case SYMB_ARROWDOWN:
                case SYMB_ARROWRIGHT:
                case SYMB_ARROWUP:

                        poly.clear();
                        poly << QPointF (0, size / 4) << QPointF (size / 3, 0) << QPointF (size / 3, size / 6) << QPointF (size, size / 6) << QPointF (size, size / 3)
                             << QPointF (size / 3, size / 3) << QPointF (size / 3, size / 2) << QPointF (0, size  /4);

                        //shift out of the cell middle
                        poly.translate (_cellDiameter / 2, - size / 4);

                        break;


                case SYMB_TRIANGLELEFT:
                case SYMB_TRIANGLEDOWN:
                case SYMB_TRIANGLERIGHT:
                case SYMB_TRIANGLEUP:

                        poly.clear();
                        poly << QPointF (0, size / 2) << QPointF (size / 2, 0) << QPointF (size / 2, size) << QPointF (0, size / 2);

                        //shift out of the cell middle
                        poly.translate (_cellDiameter / 2, - size / 2);

                        break;

                default:
                        ;
        }


        return poly;
}

QGraphicsPolygonItem* Symbol::createPolygonItem (int _cellDiameter) const
{
        QPolygonF poly = drawPolygon (_cellDiameter);

        //create QGraphicsPolygonItem
        QGraphicsPolygonItem *polyItem = new QGraphicsPolygonItem (poly);
        polyItem->setPen (QPen (color, 1));
        polyItem->setBrush (color);
        polyItem->rotate (getTurningAngle());

        return polyItem;
}

void Symbol::setTimePoints (int _start, int _end)
{
	if (_start >= 0)
                timePointStart = _start;
	if (_end >= 0)
                timePointEnd = _end;
}

QCursor Symbol::createSymbolMouseCursor (const Symbol &_symbol)
{
        //there seems to be no restriction on cursor icon sizes... (neither Windows nor Linux)
        //thus, simply create a cursor with the desired width
        //add 2 to receive a nice circle; otherwise flattened at the right & bottom margin
        int width = _symbol.getSize() * 2;

        QPolygonF poly = _symbol.drawPolygon (0);

        //create pixmap with a transparency mask
        QPixmap mousePix (width, width);
        mousePix.fill (Qt::transparent);
        QPainter p (&mousePix);
        p.setPen (QPen (_symbol.getColor(), 1));
        p.drawPolygon (poly);
        p.end();

        //set all pixels transparent but these on the circle line with the given radius
        QBitmap mask (width, width);
        mask.fill (Qt::color0); //transparent
        p.begin (&mask);
        p.setPen (QPen (Qt::color1, 1));
        p.drawPolygon (poly);
        p.end();

        mousePix.setMask (mask);

        //the hot spot is set in the middle automatically
        return QCursor (mousePix);

}

QDataStream& operator<< (QDataStream& _str, const Symbol& _symbol)
{
	//write _symbol to a data stream (normally associated with a file)
	
	//_str << _symbol.Index;
        _str << _symbol.timePointStart << _symbol.timePointEnd;
	_str << _symbol.color;
        _str << _symbol.size << _symbol.pos.toPoint();
	_str << (int)_symbol.symbol;
	if (_symbol.track)
		_str << _symbol.track->getNumber();
	else
		_str << (int)0;			//Track::getNumber() is of type int
	
	return _str;
}

QDataStream& operator>> (QDataStream& _str, Symbol& _symbol)
{
	//read symbol from a data stream (normally associated with a file)
	int tmp;
	
	//_str >> _symbol.Index;
        _str >> _symbol.timePointStart;
        _str >> _symbol.timePointEnd;
	_str >> _symbol.color;
	_str >> _symbol.size;
	QPoint tmpPoint;
	_str >> tmpPoint;
        _symbol.pos = QPointF (tmpPoint);
	_str >> tmp;
	
	//type conversion from char/int to enum is illegal (only the other way is possible)
	//=> loop is necessary
	for (int k = 0; k < 7 /*Symbols.count*/; k++)
                if (tmp == SymbolTypes(k))
                        _symbol.symbol = SymbolTypes (k);
		
	_str >> tmp;
	if (tmp > 0)
                _symbol.trackNumber = tmp;
	else
                _symbol.trackNumber = 0;
	
	return _str;
}
