/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SYMBOL_H
#define SYMBOL_H

//#include "clickableqgraphicspolygonitem.h"

#include <qpainter.h>
#include <qcolor.h>
#include <qpoint.h>
#include <QCursor>
#include "math.h"
#include <QGraphicsPolygonItem>
#include <QPolygonF>
#include <QBitmap>
#include <QPixmap>

#define PI 3.14159265

#include "track.h"

enum SymbolTypes {SYMB_NOSYMBOL=0, SYMB_ARROWUP=1, SYMB_ARROWDOWN=2, SYMB_ARROWRIGHT=3, SYMB_ARROWLEFT=4,
              SYMB_ASTERISKO=5/*, SYMB_CIRCLE=10*/,
              SYMB_TRIANGLEUP=6, SYMB_TRIANGLEDOWN=7, SYMB_TRIANGLERIGHT=8, SYMB_TRIANGLELEFT=9};

/**
@author Bernhard
	
	This class provides a simple access for different symbols that can be added to
	a picture, for example for movie export or marking interesting regions.
	A symbol can easily be stored with the provided << and >> operators.
*/

class Symbol {// : public ClickableQGraphicsPolygonItem {

public:
	
	/**
	 * constructs a default symbol (invisible, no type)
	 */
	Symbol ()
                : index (-1), symbol (SYMB_NOSYMBOL), color (Qt::black), size (-1),
                track (0), trackNumber (0), angle (0)
		{}
	
	/**
	 * constructs a Symbol with the specified parameters
	 * @param _index the index of this symbol (note that they have to be unique on the user's responsibility)
	 * @param _symbol the symbol type (see enum defined above)
	 * @param _color the color of the symbol
	 * @param _size the relative of this symbol
	 */
        Symbol (int _index, SymbolTypes _symbol, QColor _color, int _size)
                : index (_index), symbol (_symbol), color (_color), size (_size),
                track (0), trackNumber (0), angle (0)
		{}
	
	~Symbol();
	
	/**
	 * sets the specified symbol by copying
	 * @param _symb the symbol to be copied
	 */
	void set (Symbol _symb)
		{	symbol = _symb.symbol; 
			color = _symb.color; 
			size = _symb.size;
			angle = _symb.angle;
                        timePointStart = _symb.timePointStart;
                        timePointEnd = _symb.timePointEnd;
			pos = _symb.pos;
			track = _symb.track;
                        trackNumber = _symb.trackNumber;
		}
	
	/**
	 * sets the specified symbol by setting all provided attributes
	 * @param _symbol the symbol type (see enum defined above)
	 * @param _color the color of the symbol
	 * @param _size the relative of this symbol
	 */
        void set (SymbolTypes _symbol, QColor _color, int _size)
		{
			symbol = _symbol;
			color = _color;
			size = _size;
		}
	
	/**
	 * sets the current symbol
	 * @param _symbol the symbol type (see enum defined above)
	 */
        void setSymbol (SymbolTypes _symbol)
		{symbol = _symbol;}
	
	/**
	 * @return the current symbol
	 */
        SymbolTypes getSymbol() const
		{return symbol;}
	
	
	/**
	 * sets the current size
	 * @param _size the relative of this symbol
	 */
	void setSize (int _size)
		{size = _size;}
	
	/**
	 * @return the current size
	 */
	int getSize() const
		{return size;}
	
	/**
	 * sets the current color
	 * @param _color the QColor for the filling of the symbol
	 */
	void setColor (QColor _color)
		{color = _color;}
	
	/** 
	 * @return the current color
	 */
	QColor getColor() const
		{return color;}
	
	/**
	 * sets the display timepoints (where the symbol is visible)
	 * @param _start the first timepoint where the symbol should be visible
	 * @param _end the last timepoint where the symbol should be visible
	 */
	void setTimePoints (int _start = -1, int _end = -1);
	
	/**
	 * associates this symbol with a track
	 * @param _track the Track object to which this symbol belongs and which it should follow
	 */
	void setTrack (Track *_track)
		{track = _track;}
	
	/**
	 * @return the currently associated track (or 0, if this symbol is a stand-alone)
	 */
	Track* getTrack() const
		{return track;}
	
	/**
	 * @return the first timepoint where this symbol should be visible
	 */
	int getFirstTimePoint() const
                {return timePointStart;}
	
	/**
	 * @return the last timepoint where this symbol should be visible
	 */
	int getLastTimePoint() const
                {return timePointEnd;}
	
	/**
	 * sets the position of the symbol
	 * @param _pos 
	 */
        void setPosition (QPointF _pos)
		{pos = _pos;}
	
	/**
	 * @return the current position
	 */
        QPointF getPosition() const
		{return pos;}
	
        /* *
	 * draws the currently set symbol with the provided QPainter at _pos \n
	 * this method leaves the painter unchanged
	 * @param _p the (active) painter which should draw this symbol
	 * @param _pos the position of the "hotspot" of this symbol
	 * @param _cellRadius the radius of the associated cell (if there is one, otherwise -1)
	 */
        //void drawX (QPainter &_p, QPointF _pos = QPointF(), int _cellRadius = -1);
	
        /**
         * draws the polygon representing this symbol
         * @param _cellDiameter the diameter of the cell (necessary for translating the polygon)
         * @return a (position independent) polygon
         */
        QPolygonF drawPolygon (int _cellDiameter) const;

        /**
         * draws the polygon representing this symbol
         * @param _cellDiameter the diameter of the cell (necessary for translating the polygon)
         * @return a polygon item, with the correct color, turning angle and shape
         */
        QGraphicsPolygonItem* createPolygonItem (int _cellDiameter) const;

	/**
	 * returns the read track number (from a datastream)\n
	 * if != 0, track is set via mapping (number -> pointer)
	 * @return the track number
	 */
	int getTrackNumber() const
                {return trackNumber;}
	
	/**
	 * returns a rectangular region around the hotspot of this symbol
	 * @param _pos the position of the "hotspot" of this symbol (arrow tip, ...)
	 * @return a QRect with the hotspot in the middle and width/height 5 pixels
	 */
//        QRect getRect (QPointF _pos) const;
	
	/**
	 * sets the index of this symbol
	 * @param _index the index (the user is responsible for keeping the indices of two symbols distinct!)
	 */
	void setIndex (int _index)
                {index = _index;}
	
	/**
	 * @return the index of this symbol
	 */
	int getIndex() const
                {return index;}
	
	/**
	 * sets the angle of this symbol (in degrees)
	 * 0 is the default, and a positive angle means a counter-clockwise rotation
	 * @param _angle the angle in degrees
	 */
	void setAngle (int _angle)
		{angle = _angle;}
	
	/**
	 * @return the angle of this symbol (in degrees)
	 */
	int getAngle() const
		{return angle;}
	
        /**
         * @return the turning angle with respect to the basic polygon returned by drawPolygon
         */
        int getTurningAngle() const;

        /**
         * creates a mouse cursor that looks like the provided symbol (in size, color, shape)
         */
        static QCursor createSymbolMouseCursor (const Symbol &_symbol);

private:
	
	///the internal index of this symbol
	///necessary for identifying each symbol in a unique way
	///the user is responsible for keeping the indices unique
        int index;
	
	///the current symbol, see enum Symbols
        SymbolTypes symbol;
	
	///the current color
	QColor color;
	
	///the current size of the symbol
	///handled differently for each symbol (circle -> radius, arrow -> line width, ...)
	int size;
	
	///the timepoints for which the symbol should be displayed
        int timePointStart;
        int timePointEnd;
	
        ///the position of the symbol (in experiment coordinates)
        QPointF pos;
	
	///the track which the symbol should follow (can be 0 very well!)
	Track *track;
	
	///necessary for reading a symbol from a data stream, since the track number is stored
        int trackNumber;
	
	///the angle of this symbol
	int angle;
	
	
	
	
	friend QDataStream& operator<< (QDataStream&, const Symbol&);
	friend QDataStream& operator>> (QDataStream&, Symbol&);

};

QDataStream& operator<< (QDataStream&, const Symbol&);
QDataStream& operator>> (QDataStream&, Symbol&);

#endif
