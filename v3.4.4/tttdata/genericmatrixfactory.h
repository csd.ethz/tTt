/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef genericmatrixfactory_h__
#define genericmatrixfactory_h__

// Qt includes
#include <QString>
#include <QChar>
#include <QFile>
#include <QTextStream>
#include <QList>
#include <QStringList>

// Project includes
#include "genericmatrix.h"
#include "tttbackend/exception.h"


/**
 * @author Oliver Hilsenbeck
 *
 * GenericMatrix factory class. Contains only static methods to load GenericMatrix objects from files.
 */
class GenericMatrixFactory {
public:

	/**
	 * Load a GenericMatrix<T> object from a CSV-file.
	 * @param _fileName path to CSV-file
	 * @param _converter a conversion functor object. This is needed to convert a single data cell from the
	 *			csv-file from QString to T. This is done by using the operator()(QString) function. See FloatConverter for an example.
	 * @param _horizontalHeader if the data in the file includes a horizontal header (first line)
	 * @param _verticalHeader if the data in the file includes a vertical header (first column)
	 * @param _delim the character used for separating fields
	 * @return a GenericMatrix<T> object
	 */
	template <typename T, typename U>
	static GenericMatrix<T> loadFromFile(const QString& _fileName, const U& _converter, bool _horizontalHeader = false, bool _verticalHeader = false, QChar _delim=',', QChar _textDelim = '"');

	/**
	 * Load a double matrix from a CSV-file. This is just a redirect to the general loadFromFile() function.
	 * @param _fileName path to CSV-file
	 * @param _horizontalHeader if the data in the file includes a horizontal header (first line)
	 * @param _verticalHeader if the data in the file includes a vertical header (first column)
	 * @param _delim the character used for separating fields
	 * @return a DoubleMatrix (GenericMatrix<double>) object
	 */
	static DoubleMatrix loadDoubleMatrix(const QString& _fileName, bool _horizontalHeader = false, bool _verticalHeader = false, QChar _delim=',', QChar _textDelim = '"');

	/**
	 * See loadDoubleMatrix()
	 */
	static FloatMatrix loadFloatMatrix(const QString& _fileName, bool _horizontalHeader = false, bool _verticalHeader = false, QChar _delim=',', QChar _textDelim = '"');
	
	/**
	 * Removes the start and end delimiter from a string
	 * @param _string the text to remove the delimiter from
	 * @param _textDelim the delimiter for text
	 * @return the text without the start and end delimiter
	 */
	static QString removeTextDelimiter(QString _string, QChar &_textDelim);

	//////////////////////////////////////////////////////////////////////////
	// Predefined functors for loadFromFile
	// (see http://en.wikipedia.org/wiki/Function_object)
	//////////////////////////////////////////////////////////////////////////
public:
	/**
	 * Functor to convert a QString to float. Can be used for loadFromFile() 
	 * NA will be converted to NaN
	 */
	class FloatConverter {
	public:
		float operator()(QString _s) const {
			if(_s == "NA") return std::numeric_limits<float>::quiet_NaN();
			return _s.toFloat();
		}
	};

	/**
	 * Functor to convert a QString to double. Can be used for loadFromFile() 
	 * NA will be converted to NaN
	 */
	class DoubleConverter {
	public:
		double operator()(QString _s) const {
			if(_s == "NA") return std::numeric_limits<double>::quiet_NaN();
			return _s.toDouble();
		}
	};
};

template <typename T, typename U>
static GenericMatrix<T> GenericMatrixFactory::loadFromFile( const QString& _fileName, const U& _converter, bool _horizontalHeader, bool _verticalHeader, QChar _delim, QChar _textDelim)
{
	// Try to open file
	QFile inFile(_fileName);
	if(!inFile.open(QIODevice::ReadOnly))
		throw ttt_Exception("Cannot open file.");

	// Parse file
	QTextStream inStream(&inFile);

	// Start by using QLists as growing is not so expensive
	QList<QStringList> tmpMatrix;
	QStringList hHeader, vHeader;

	// Read first line
	QString curLine = inStream.readLine();

	// Number of column in last line (including vertical header column)
	int lastColCount = -1;
	
	// Number of rows so far (without horizontal header count)
	int rowCount = 0;

	if(_horizontalHeader) {
		// Read horizontal header
		hHeader = curLine.split(_delim);
		lastColCount = hHeader.size();

		// Horizontal header has no vertical header column
		if(_verticalHeader)
			lastColCount++;
	}

	while(!inStream.atEnd()) {
		// Read next line
		curLine = inStream.readLine();

		// Ignore empty lines
		if(curLine.isEmpty())
			continue;

		// Split line
		QStringList splitString = curLine.split(_delim);

		// Check column count
		if(lastColCount != -1 && lastColCount != splitString.size())
			throw ttt_Exception("Rows have different numbers of columns.");
		lastColCount = splitString.size();

		// Extract vertical header
		if(_verticalHeader) {
			vHeader.append(splitString.first());
			splitString.removeFirst();
		}

		// Add row to matrix
		tmpMatrix.append(splitString);
		rowCount++;
	}

	if(rowCount == 0)
		throw ttt_Exception("File contains no data.");

	// Number of columns (without vertical header column)
	int numCols = lastColCount;
	if(_verticalHeader)
		--numCols;

	// Number of rows (also without header row)
	int numRows = tmpMatrix.size();

	// Create matrix
	GenericMatrix<T> ret(numRows, numCols, _horizontalHeader, _verticalHeader);

	// Copy data from tmpMatrix
	int row = 0;
	for(QList<QStringList>::const_iterator rowIt = tmpMatrix.begin(); rowIt != tmpMatrix.end(); ++rowIt, ++row) {
		int col = 0;
		for(QStringList::const_iterator colIt = rowIt->begin(); colIt != rowIt->end(); ++colIt, ++col) {
			ret(row, col) = _converter(*colIt);
		}
	}

	// Copy horizontal header
	if(_horizontalHeader) {
		int i = 0;
		for(QStringList::const_iterator it = hHeader.begin(); it!=hHeader.end(); ++it, ++i)
			ret.horizontalHeader(i) = removeTextDelimiter(*it, _textDelim);
	}

	// Copy vertical header
	if(_verticalHeader) {
		int i = 0;
		for(QStringList::const_iterator it = vHeader.begin(); it!=vHeader.end(); ++it, ++i)
			ret.verticalHeader(i) = removeTextDelimiter(*it, _textDelim);
	}

	return ret;
}

#endif // genericmatrixfactory_h__