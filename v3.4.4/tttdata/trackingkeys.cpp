/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "trackingkeys.h"

TrackingKeys * TrackingKeys::inst (0);

TrackingKeys::TrackingKeys()
{
	//15 keys, see arrangement on English keyboard
	
	map ["Q"] = 1;
	map ["W"] = 2;
	map ["E"] = 3;
	map ["R"] = 4;
	map ["T"] = 5;
	
	map ["A"] = 6;
	map ["S"] = 7;
	map ["D"] = 8;
	map ["F"] = 9;
	map ["G"] = 10;
	
	map ["Z"] = 11;
	map ["X"] = 12;
	map ["C"] = 13;
	map ["V"] = 14;
	map ["B"] = 15;
}

TrackingKeys::~TrackingKeys()
{}

const QStringList TrackingKeys::getKeys()
{
	if (! inst)
		inst = new TrackingKeys();
	

	
	return (QStringList)inst->map.keys();
}


short int TrackingKeys::getValue (const char _key)
{
	if (! inst)
		inst = new TrackingKeys();
	
	//QString key = _key;
	return inst->map [QString().append (_key)];
}

// ------------- Konstantin -----------------
short int TrackingKeys::getValue (const QChar _key)
{
        if (! inst)
                inst = new TrackingKeys();

        //QString key = _key;
        return inst->map [QString().append (_key)];
}

char TrackingKeys::getKeyFromValue (short int _value)
{
	if (! inst)
		inst = new TrackingKeys();
	
	for (QMap<QString, Q_INT8>::Iterator iter = inst->map.begin(); iter != inst->map.end(); ++iter)
		if ((short int)iter.data() == _value)
			return iter.key().ascii() [0];
	
	return 0;
}
