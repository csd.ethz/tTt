/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef STYLESHEET_H
#define STYLESHEET_H

#include "tttbackend/treeelementstyle.h"
#include "tttbackend/fileinfoarray.h"

#include <qfile.h>
#include <qdatastream.h>
#include <QVector>
#include <qstring.h>
#include <qstringlist.h>
#include <qmap.h>
#include <QDomDocument>

///the color for the cell circles, if all should have the same
const QColor UNIFORM_CELL_COLOR = Qt::white;

///the suffix for the style files
const QString STYLEFILE_SUFFIX =  ".xml";

///the default user-specific style sheet path
const QString STYLESHEET_FILENAME = "%1_styles" + STYLEFILE_SUFFIX;



///the style file version numbe
///increment it with every change in the number of attributes
///for a detailed version history, see method load()
///from 9 on stored as XML, thus it is not changed anymore
const int StyleFileVersion = 9;



class TTTTreeStyleEditor;
class StyleSheetXMLParser;
class XMLHandler;


/**
	@author Bernhard
	
	The object which handles custom layout for the tree, the wavelengths, ...
*/

class StyleSheet
{

public:
	/**
	 * constructs a default style sheet (all elements are inserted with the default values)
	*/
	StyleSheet();
	
	///copy constructor
	///@param _ss the style sheet to be copied
	StyleSheet (const StyleSheet &_ss);
	
	~StyleSheet();
	
	///assignment operator
	///@param _ss the style sheet to be copied
	///@return this object
	StyleSheet &operator=(const StyleSheet &_ss);
	
	
//methods for the tree graphics layout
	
	///returns all tree elements that are known to this object
	///@return a QStringList object with all TreeElementStyle objects
	const QStringList elements() const;
	
	///returns the desired TreeElementStyle object
	///@param _elementRTTI the RTTI value of the tree element object (cf. constants in TreeElementStyle)
	///@return the TreeElementStyle object with the provided RTTI value
	const TreeElementStyle getElement (int _elementRTTI) const;
	
	///returns the desired TreeElementStyle object
	///@param _displayedText the displayed text of the tree element object (cf. reset() in TreeElementStyle)
	///@return the TreeElementStyle object with the provided RTTI value
	const TreeElementStyle getElement (const QString &_displayedText) const;
	
	///inserts the provided tree element style into its position (dep. from RTTI value) in the array
	///@param _tes the TreeElementStyle object
	void setElement (const TreeElementStyle &_tes);
	
	///sets all values to fit grayscale
	///note: only the green component of the current color is used
	void setGrayScale();
	
	
	
//methods for the wavelength layout	
	
	///sets the wavelength layout to _map
	///@param _map the new wavelength layout
	///@param _picsPerRow the number of pictures per row
        void setWLMapping (const QVector<int>& _map, int _picsPerRow);
	
	///returns the current mapping
	///@param _picsPerRow a reference parameter which is set to the stored value
	///@return a const reference to the current wavelength layout mapping scheme
        const QVector<int>& getMapping (int &_picsPerRow) const
		{	_picsPerRow = picsPerRow;
			return wavelengthLayout;
		}
	
	///returns the _index-th wavelength from the current layout
	///@param _index the position index of the wavelength
	///@return an integer value describing the wavelength at position _index
	int getWavelength (int _index) const;
	
	///the inversion of getWavelength: finds the position of a wavelength
	///@param _wavelength the wavelength to which the position should be found out
	///@return the position within the layout for the specified wavelength
	int getWLPosition (int _wavelength) const;
	
	///@return the wavelength that is located in the top right corner
	int getTopRightWavelength() const;
	
	///@return the wavelength that is located in the bottom right corner
	int getBottomRightWavelength() const;
	
	/* *
	 * sets the pictures per row for the movie layout
	 * @param _ppr the pics per each row
	 */
/*	void setPicsPerRow (int _ppr)
		{picsPerRow = _ppr;}
	
	/ **
	 * @return the number of pictures per each movie row
	 * /
	int getPicsPerRow()
		{return picsPerRow;}
	*/
	
//methods for the movie export options
	
	/**
	 * sets the background for the timebox 
	 * @param _value the background value (possible values: cf. declaration of attribute)
	 */
	void setTimeboxBackground (int _value)
		{MovieExport_TimeboxBackground = _value;}
	
	/**
	 * @return the color index of the timebox background (possible values: cf. declaration of attribute)
	 */
	int getTimeboxBackground() const
		{return MovieExport_TimeboxBackground;}
	
	/**
	 * sets the position of the timebox
	 * @param _position the position, coded as integer (possible values: cf. declaration of attribute)
	 */
	void setTimeboxPosition (int _position)
		{MovieExport_TimeboxPosition = _position;}
	
	/**
	 * @return the position of the timebox (possible values: cf. declaration of attribute)
	 */
	int getTimeboxPosition() const
		{return MovieExport_TimeboxPosition;}
	
	/**
	 * sets the visibility of the wavelength box
	 * @param _visible whether the wavlength box should be visible
	 */
	void setWavelengthVisibility (bool _visible)
		{MovieExport_WavelengthVisible = _visible;}
	
	/**
	 * @return whether the wavlength box is visible
	 */
	bool getWavelengthVisibility() const
		{return MovieExport_WavelengthVisible;}


//methods for movie display options

	/**
	 * set the current cell color
	 * @param _color the cell circle (for the outline of the cell circles)
	 */
	void setCellColor (QColor& _color)
		{cellColor = _color;}
	
	/**
	 * @return the current cell circle outline color
	 */
	QColor getCellColor() const
		{return cellColor;}
	
	/**
	 * sets the current color for the cell numbers
	 * @param _color the color for the cell numbers (can differ from circle color)
	 */
	void setCellNumberColor (QColor _color)
		{cellNumberColor = _color;}
	
	/**
	 * @return the current cell number color
	 */
	QColor getCellNumberColor() const
		{return cellNumberColor;}
	
        /**
         * sets the current color for the wavelength box
         * @param _color the color for the wavelength box
         */
        void setWavelengthBoxColor (QColor _color)
                {wavelengthBoxColor = _color;}

        /**
         * @return the current wavelength box color
         */
        QColor getWavelengthBoxColor () const
                {return wavelengthBoxColor;}

        /**
         * sets the font size of the wavelength box
         * @param _size the wavelength box font size
         */
        void setWavelengthBoxFontSize (int _size)
                {wavelengthBoxFontSize = _size;}

        /**
         * @return the current wavelength box font size
         */
        int getWavelengthBoxFontSize() const
                {return wavelengthBoxFontSize;}

        /**
	 * sets the thickness (line width) of the cell circle outline
	 * @param _thickness the cell circle outline line width
	 */
	void setCellCircleThickness (int _thickness)
		{cellCircleThickness = _thickness;}
	
	/**
	 * @return the current cell circle outline thickness
	 */
	int getCellCircleThickness() const
		{return cellCircleThickness;}
	
	/**
	 * sets the current color for the cell path
	 * @param _color the color for the cell paths
	 */
	void setCellPathColor (QColor _color)
		{cellPathColor = _color;}
	
	/**
	 * @return the current cell number color
	 */
	QColor getCellPathColor() const
		{return cellPathColor;}
	
	/**
	 * sets the font size of the cell numbers
	 * @param _size the cell number font size
	 */
	void setCellNumberFontSize (int _size)
		{cellNumberFontSize = _size;}
	
	/**
	 * @return the current cell number font size
	 */
	int getCellNumberFontSize() const
		{return cellNumberFontSize;}
	
	/**
	 * sets the thickness (line width) of the cell path lines
	 * @param _cpt the cell path line width
	 */
	void setCellPathThickness (int _cpt)
		{cellPathThickness = _cpt;}
	
	/**
	 * @return the cell path line thickness
	 */
	int getCellPathThickness() const
		{return cellPathThickness;}
	
	/**
	 * sets the timepoint interval for which the timepoint/real time should be displayed
	 * @param _cp_ti the interval of the timepoints
	 */
	void setCellPathTimeInterval (int _cp_ti)
		{cellPathTimeInterval = _cp_ti;}
	
	/**
	 * @return the timepoint interval for which the timepoint/real time should be displayed
	 */
	int getCellPathTimeInterval() const
		{return cellPathTimeInterval;}
	
	/**
	 * Set if out of sync symbol should be shown
	 * @param _val on or off
	 */
	void setShowOutOfSyncBox(bool _val) 
		{ showOutOfSyncBox = _val; }

	/**
	 * @return if out of sync symbol should be shown
	 */
	bool getShowOutOfSyncBox() const
		{ return showOutOfSyncBox; }
	
	
//methods for the tracking key associations
	
	
	/**
	 * returns the mapping of key to value
	 * @param _key the character for which the value should be returned
	 * @return the value that is stored in the additional attribute if _key is pressed while tracking
	 */
        const TreeElementStyle getAssociatedElement (char _key) const;
	
	/**
	 * sets an association of a key to a value for setting attributes while tracking
	 * @param _key the key (character)
	 * @param _tes the element settings
	 */
	void setAssociatedElement (char _key, const TreeElementStyle& _tes);
	
	/**
	 * @return the complete mapping of keys to values
	 */
	QMap<Q_INT8, TreeElementStyle> getTrackingKeyMap() const
		{return trackingKeyMap;}
	
	/**
	 * 
	 * @param _map 
	 */
	void setTrackingKeyMap (QMap<Q_INT8, TreeElementStyle> _map)
		{trackingKeyMap = _map;}
	
	
	
//methods for various settings
	
	void setAddUserSignToTTTFiles (bool _addUSTTF)
		{addUserSignToTTTFiles = _addUSTTF;}
	
	bool getAddUserSignToTTTFiles() const
		{return addUserSignToTTTFiles;}
	
	void setUseNewTTTFileFolderStructure (bool _uNTFFS)
		{useNewTTTFileFolderStructure = _uNTFFS;}
	
	bool getUseNewTTTFileFolderStructure() const
		{return useNewTTTFileFolderStructure;}
	
	void setExportWavelength (int _ewl)
		{exportWavelength = _ewl;}
	
	int getExportWavelength() const
		{return exportWavelength;}
	
	void setPicturesToLoadBeforeStop(const int& theValue)
		{ picturesToLoadBeforeStop = theValue;}
	
	int getPicturesToLoadBeforeStop() const
		{return picturesToLoadBeforeStop;}

	void setNumPicsToLoadByNextPosItem(int theValue)
		{ numPicsToLoadByNextPosItem = theValue;}

	int getNumPicsToLoadByNextPosItem() const
		{ return numPicsToLoadByNextPosItem;}

	void setTakeOverDisplaySettingsNextPosItem(bool theValue)
		{ takeOverDisplaySettingsNextPosItem = theValue; }

	bool gettakeOverDisplaySettingsNextPosItem() const 
		{ return takeOverDisplaySettingsNextPosItem; }
	
	void setPicturesToLoadAfterStop(const int& theValue)
		{picturesToLoadAfterStop = theValue;}
	
	int getPicturesToLoadAfterStop() const
		{return picturesToLoadAfterStop;}
	
	// (this should actually be called Synchronously, ie not in an extra thread)
	void setLoadPicturesAsynchronous ( bool theValue )
		{loadPicturesAsynchronous = theValue;}
	
	// (this should actually be called Synchronously, ie not in an extra thread)
	bool getLoadPicturesAsynchronous() const
		{return loadPicturesAsynchronous;}
	
	void setShowTreeCountInPositionLayout(bool theValue)
		{ showTreeCountInPositionLayout = theValue;}
	
	bool getShowTreeCountInPositionLayout() const
		{return showTreeCountInPositionLayout;}
	
	void setUseColorsForAllTracks ( bool theValue )
		{useColorsForAllTracks = theValue;}
	
	bool getUseColorsForAllTracks() const
		{return useColorsForAllTracks;}
	
	QString getLastTTTChangelogVersion() const
		{return lastTTTChangelogVersion;}

	void setLastTTTChangelogVersion(QString _version)
		{lastTTTChangelogVersion = _version;}

//startup settings methods
		
	
	
	
//common methods	
	
	///resets the default values
	///@param _mode what should be reset 
	///(0 = everything, 1 = tree graphics, 2 = wavelength layout, 3 = movie export options, 4 = cell display options, 5 = tracking key associations, 6 = various settings, 7 = startup settings)
	void reset (int _mode = 0);
	
	///saves the current style sheet in the style file
	///@param _filename the filename of the style file
	///@return success
	bool save (QString _filename) const;
	
	///loads the style file and sets the stored settings to THIS
	///if loading is possible (the style file is correct), this object is changed, otherwise not
	///@param _filename the filename of the style file
	///@return success (whether loading was succesful)
	bool load (QString _filename);
	
	/**
	 * Builds a XML document from this style sheet
	 * @return a new QDomDocument which can directly be written to file
	 */
	QDomDocument toDomDocument() const;
	
	
	
	
private:
	

//tree display options	
	///contains one entry for each tree element (cf. defined RTTI values in class TreeElementStyle)
        QVector<TreeElementStyle> elementStyles;
	

//wavelength layout options	
	///contains the custom layout of the wavelengths in TTTMovie
	///it is a simple hash map which assigns the index of the position to a wavelength
        QVector<int> wavelengthLayout;
	
	///the number of pictures per row for the wavelength layout in TTTMovie
	int picsPerRow;
	

//movie export options	
	
	///whether the top left box indicating the wavelength is visible during export
	bool MovieExport_WavelengthVisible;
	
	///the position of the time box
	///values:
	///0 = invisble
	///11 = top left
	///12 = top middle
	///13 = top right
	///21 = middle left
	///22 = middle middle
	///23 = middle right
	///31 = bottom left
	///32 = bottom middle
	///33 = bottom right
	int MovieExport_TimeboxPosition;
	
	///the background of the time box 
	///values:
	///0 = transparent/normal picture
	///1 = black
	int MovieExport_TimeboxBackground;


//movie display options
	
	///the color for the track circles
	QColor cellColor;
	
	///the color for the cell numbers
	QColor cellNumberColor;
	
    ///the font size of the cell numbers
    int cellNumberFontSize;

    ///the color for the wavelength box
    QColor wavelengthBoxColor;

    ///the font size of the wavelength box
    int wavelengthBoxFontSize;

    ///the thickness of the cell circles
	int cellCircleThickness;
	
	///the color for the cell paths
	QColor cellPathColor;
	
	///the thickness of the lines of the cell paths
	int cellPathThickness;
	
	///the timepoint interval for which the timepoint/real time should be displayed
	int cellPathTimeInterval;
	
	///show little red box to indicate out of sync pictures
	bool showOutOfSyncBox;

//tracking key associations for various attributes
	
	QMap<Q_INT8, TreeElementStyle> trackingKeyMap;
	
	
//various settings
	
	///whether the user sign should be added to all ttt files automatically
	bool addUserSignToTTTFiles;
	
	///whether the ttt file should be stored in the new folder system
	bool useNewTTTFileFolderStructure;
	
	///the wavelength that is used for fluorescence export and display of the integrals
	int exportWavelength;
	
	///the pictures to be loaded before the cell's stop reason (on right-click)
	int picturesToLoadBeforeStop;
	///the pictures to be loaded after the cell's stop reason (on right-click)
	int picturesToLoadAfterStop;
	
	///whether the pictures should be loaded asynchronously (this should actually be called Synchronously, ie not in an extra thread)
	bool loadPicturesAsynchronous;
	
	///whether the number of trees should be displayed in each position in the layout window
	bool showTreeCountInPositionLayout;
	
	///whether the first/all tracks in the movie window should be displayed with different colors
	bool useColorsForAllTracks;

	///number of pictures to load when opening a new position from within the movie window via a MovieNextPosItem button
	int numPicsToLoadByNextPosItem;

	///if the current display settings (gamma etc) should be taken over when a new position is loaded via a MovieNextPosItem button
	bool takeOverDisplaySettingsNextPosItem;

	///version for which tTt changelog has been displayed (will only be displayed again for a newer version)
	QString lastTTTChangelogVersion;
	
//startup settings
	
	///@todo
	///whether the movie should proceed to the first timepoint where at least two wavelengths are loaded
	bool startUpMoveToFirstFluor;
	
	///whether the background circles should be visible by default
	bool startUpBgCircleVisible;
	
	///whether at movie startup the full picture should be displayed (instead of 100%)
	bool startUpFullPicture;
	
	///whether the tracking should be alternating with setting normal and background trackmarks
	bool startUpAlternateNormalBgTracking;
	
	
//friend declarations	
	friend class TTTTreeStyleEditor;
	friend class StyleSheetXMLParser;
	friend class XMLHandler;
};

#endif
