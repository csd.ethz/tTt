/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef stringmatrix_h__
#define stringmatrix_h__

// Qt includes
#include <QString>
#include <QVector>

// Project includes
#include "genericmatrix_data.h"
#include "tttbackend/exception.h"


// Typedefs of common matrix types
template <typename T> class GenericMatrix;
typedef GenericMatrix<double> DoubleMatrix;
typedef GenericMatrix<float> FloatMatrix;



/**
 * @author Oliver Hilsenbeck
 *
 * This is a generic matrix class. It can store arbitrary m x n matrices of type T.
 * Optionally it can include a horizontal and/or a vertical header of type QString (i.e. unicode string).
 * It uses shared memory (see matrix_data.h), so copying GenericMatrix objects, e.g. by returning
 * one from a function, is very fast. The data will only be copied if you change data of a matrix with
 * more than one reference pointing to it.
 * At the moment this class is only used to load data and display it, so it does not (yet) support
 * any mathematical operations.
 */
template <typename T>
class GenericMatrix {
public:
	/**
	 * Create an instance of GenericMatrix<T>
	 * @param _rows number of rows
	 * @param _cols number of cols
	 * @param _horizontalHeader if the matrix should have a horizontal string header
	 * @param _verticalHeader if the matrix should have a vertical string header
	 * @param _val the value to initialize the matrix with. Default is T() which is 0 for built-in types like int, float, double
	 */
	GenericMatrix(unsigned int _rows=0, unsigned int _cols=0, bool _horizontalHeader = false, bool _verticalHeader = false, const T& _val=T());

	/**
	 * Create a shallow copy of _other
	 */
	GenericMatrix(const GenericMatrix& _other);

	/**
	 * Create a shallow copy of _other
	 */
	GenericMatrix& operator=(const GenericMatrix& _other);

	// Destructor
	~GenericMatrix();

	/**
	 * Element access to retrieve or edit data. Calling this function 
	 * causes a deep copy if there are other references to this matrix
	 * which requires linear time
	 * @param _row the row number starting with 0
	 * @param _col the column number starting with 0
	 * @return a reference of type T to the specified element
	 */
	T& operator()(unsigned int _row, unsigned int _col);

	/**
	 * Element access to retrieve data only.
	 * Runs in O(1) always.
	 * @param _row the row number starting with 0
	 * @param _col the column number starting with 0
	 * @return a reference of type T to the specified element
	 */
	const T& operator()(unsigned int _row, unsigned int _col) const;

	/**
	 * Access horizontal header cells to retrieve or edit data.
	 * @param _col the column index starting with 0
	 * @return a reference to the QString object of the header cell
	 */
	QString& horizontalHeader(int _col);

	/**
	 * Access vertical header cells to retrieve or edit data.
	 * @param _row the row index starting with 0
	 * @return a reference to the QString object of the header cell
	 */
	QString& verticalHeader(int _row);

	/**
	 * Access horizontal header cells to retrieve data.
	 * @param _col the column index starting with 0
	 * @return a reference to the QString object of the header cell
	 */
	const QString& horizontalHeader(int _col) const;

	/**
	 * Access vertical header cells to retrieve data.
	 * @param _row the row index starting with 0
	 * @return a reference to the QString object of the header cell
	 */
	const QString& verticalHeader(int _row) const;

	/**
	 * Get number of rows
	 * @return the number of rows
	 */
	unsigned int numRows() const;

	/**
	 * Get number of columns
	 * @return the number of columns
	 */
	unsigned int numColumns() const;

	/**
	 * Extract row. Runs in O(n)
	 * @param _row desired row
	 * @return QVector (because of implicit memory sharing) containing specified row data
	 */
	QVector<T> getRow(unsigned int _row) const;

	/**
	 * Extract column. Runs in O(n)
	 * @param _col desired column
	 * @return QVector (because of implicit memory sharing) containing specified column data
	 */
	QVector<T> getColumn(unsigned int _col) const;

	/**
	 * Replaces all occurences of the _search value with the _replace value
	 * Can cause a deepCopy of the data!
	 * @param _search the value which is replaced
	 * @param _replace the new value
	 */
	void replaceValue(T _search, T _replace);

private:
	// Pointer to data of this matrix
	matrix_data<T> *d;
};

template <typename T>
GenericMatrix<T>::GenericMatrix(unsigned int _rows, unsigned int _cols, bool _horizontalHeader, bool _verticalHeader, const T& _val)
{
	// Create new data object
	d = new matrix_data<T>(_rows, _cols, _horizontalHeader, _verticalHeader, _val);
}

template <typename T>
GenericMatrix<T>::GenericMatrix( const GenericMatrix<T>& _other )
{
	// Start referencing other matrix
	d = _other.d->addRef();
}

template <typename T>
GenericMatrix<T>::~GenericMatrix()
{
	// This reference has been removed
	d->removeRef();
}

template <typename T>
T& GenericMatrix<T>::operator()( unsigned int _row, unsigned int _col )
{
	// Deep copy if there are other references
	if(d->getRefCount() != 1)
		d = d->deepCopy();

	// Return element reference
	return d->elem(_row, _col);
}

template <typename T>
const T& GenericMatrix<T>::operator()( unsigned int _row, unsigned int _col ) const
{
	// Return element reference
	return d->elem(_row, _col);
}

template <typename T>
GenericMatrix<T>& GenericMatrix<T>::operator=( const GenericMatrix<T>& _other )
{
	// No more pointing at current matrix
	d->removeRef();

	// Now pointing to other matrix
	d = _other.d->addRef();

	return *this;
}

template <typename T>
QString& GenericMatrix<T>::verticalHeader( int _row )
{
	// Deep copy if there are other references
	if(d->getRefCount() != 1)
		d = d->deepCopy();

	// Return element reference
	return d->verticalHeader(_row);
}

template <typename T>
QString& GenericMatrix<T>::horizontalHeader( int _col )
{
	// Deep copy if there are other references
	if(d->getRefCount() != 1)
		d = d->deepCopy();

	// Return element reference
	return d->horizontalHeader(_col);
}

template <typename T>
const QString& GenericMatrix<T>::verticalHeader( int _row ) const
{
	// Return element reference
	return d->verticalHeader(_row);
}

template <typename T>
const QString& GenericMatrix<T>::horizontalHeader( int _col ) const
{
	// Return element reference
	return d->horizontalHeader(_col);
}

template <typename T>
unsigned int GenericMatrix<T>::numColumns() const
{
	return d->numColumns();
}

template <typename T>
unsigned int GenericMatrix<T>::numRows() const
{
	return d->numRows();
}

template <typename T>
QVector<T> GenericMatrix<T>::getColumn( unsigned int _col ) const
{
	return d->getColumn(_col);
}

template <typename T>
QVector<T> GenericMatrix<T>::getRow( unsigned int _row ) const
{
	return d->getRow(_row);
}

template <typename T>
void GenericMatrix<T>::replaceValue(T _search, T _replace)
{
	// Deep copy if there are other references
	if(d->getRefCount() != 1)
		d = d->deepCopy();

	d->replaceValue(_search, _replace);
}


#endif // stringmatrix_h__