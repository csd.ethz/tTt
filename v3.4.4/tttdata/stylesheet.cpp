/**
 Copyright (c) 2016 ETH Zurich, 2004-2016 Oliver Hilsenbeck, Bernhard Schauberger, Stavroula Skylaki, Timm Schroeder
  
 This file is part of "The Tracking Tool" (tTt).
  
 tTt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "stylesheet.h"

#include "tttio/xmlhandler.h"

StyleSheet::StyleSheet()
{
	reset();
}

StyleSheet::StyleSheet (const StyleSheet &_ss)
{
	//indirectly calls operator=
	*this = _ss;
}

StyleSheet::~StyleSheet()
{
}

StyleSheet& StyleSheet::operator=(const StyleSheet &_ss)
{
	this->elementStyles = _ss.elementStyles;
	
	this->wavelengthLayout = _ss.wavelengthLayout;
	this->picsPerRow = _ss.picsPerRow;
	this->MovieExport_TimeboxBackground = _ss.MovieExport_TimeboxBackground;
	this->MovieExport_TimeboxPosition = _ss.MovieExport_TimeboxPosition;
	this->MovieExport_WavelengthVisible = _ss.MovieExport_WavelengthVisible;
	this->cellColor = _ss.cellColor;
	this->cellNumberColor = _ss.cellNumberColor;
	this->cellCircleThickness = _ss.cellCircleThickness;
	this->cellPathColor = _ss.cellPathColor;
	this->cellNumberFontSize = _ss.cellNumberFontSize;
	this->cellPathThickness = _ss.cellPathThickness;
	this->cellPathTimeInterval = _ss.cellPathTimeInterval;
    this->wavelengthBoxColor = _ss.wavelengthBoxColor;
    this->wavelengthBoxFontSize = _ss.wavelengthBoxFontSize;
	showOutOfSyncBox = _ss.showOutOfSyncBox;

	this->trackingKeyMap = _ss.trackingKeyMap;
	
	this->addUserSignToTTTFiles = _ss.addUserSignToTTTFiles;
	this->useNewTTTFileFolderStructure = _ss.useNewTTTFileFolderStructure;
	this->exportWavelength = _ss.exportWavelength;
	this->picturesToLoadBeforeStop = _ss.picturesToLoadBeforeStop;
	this->picturesToLoadAfterStop = _ss.picturesToLoadAfterStop;
	this->loadPicturesAsynchronous = _ss.loadPicturesAsynchronous;
	this->showTreeCountInPositionLayout = _ss.showTreeCountInPositionLayout;
	this->useColorsForAllTracks = _ss.useColorsForAllTracks;
	this->numPicsToLoadByNextPosItem = _ss.numPicsToLoadByNextPosItem;
	this->takeOverDisplaySettingsNextPosItem = _ss.takeOverDisplaySettingsNextPosItem;
	this->lastTTTChangelogVersion = _ss.lastTTTChangelogVersion;
	
	this->startUpMoveToFirstFluor = _ss.startUpMoveToFirstFluor;
	this->startUpBgCircleVisible = _ss.startUpBgCircleVisible;
	this->startUpFullPicture = _ss.startUpFullPicture;
	this->startUpAlternateNormalBgTracking = _ss.startUpAlternateNormalBgTracking;
	
	
	return (*this);
}


const QStringList StyleSheet::elements() const
{
	QStringList result;
        for (	QVector<TreeElementStyle>::const_iterator iter = elementStyles.constBegin();
			iter != elementStyles.constEnd(); 
			++iter) {
		
		if ((*iter).rtti() > 0)
			result.append ((*iter).getText());
	}
	
	return result;
}

const TreeElementStyle StyleSheet::getElement (int _elementRTTI) const
{
        if ((_elementRTTI >= TES_RTTI_MIN) & (_elementRTTI <= TES_RTTI_MAX))
		return elementStyles [_elementRTTI - TES_RTTI_MINUS];
	else
		return TreeElementStyle();		//empty element
}

const TreeElementStyle StyleSheet::getElement (const QString &_displayedText) const
{
        for (	QVector<TreeElementStyle>::const_iterator iter = elementStyles.constBegin();
			iter != elementStyles.constEnd(); 
			++iter) {
		
		if ((*iter).getText() == _displayedText)
			return (*iter);
	}
	
	return TreeElementStyle();	//return empty element
}

void StyleSheet::setElement (const TreeElementStyle &_tes)
{
        if ((_tes.rtti()  >= TES_RTTI_MIN) & (_tes.rtti() <= TES_RTTI_MAX)) {
		elementStyles [_tes.rtti() - TES_RTTI_MINUS] = _tes;
	}
}

void StyleSheet::setGrayScale()
{
	if (elementStyles.empty())
		return;
	
        for (	QVector<TreeElementStyle>::Iterator iter = elementStyles.begin();
			iter != elementStyles.end(); 
			++iter) {
		
		(*iter).setGrayscale();
	}
}





void StyleSheet::setWLMapping (const QVector<int>& _map, int _picsPerRow)
{
	//@todo set ordenador - only then this works!
	return;
	
	if ((int)_map.size() == MAX_WAVE_LENGTH + 1) {
		wavelengthLayout = _map;
		picsPerRow = _picsPerRow;
	}
}
	
int StyleSheet::getWavelength (int _index) const
{
        if ((_index >= 0) & (_index < (int)wavelengthLayout.size())) {
                if ((wavelengthLayout [_index] >= 0) & (wavelengthLayout [_index] <= MAX_WAVE_LENGTH))
			return wavelengthLayout [_index];
		else
			return -1;
	}
	else
		return -1;
}

int StyleSheet::getWLPosition (int _wavelength) const
{
        if ((_wavelength >= 0) & (_wavelength < MAX_WAVE_LENGTH)) {
		for (int i = 0; i < (int)wavelengthLayout.size(); i++) {
			if (wavelengthLayout [i] == _wavelength)
				return i;
		}
	}
	
	return -1;		//not found
}

int StyleSheet::getTopRightWavelength() const
{
	if (picsPerRow < (int)wavelengthLayout.size())
		return wavelengthLayout [picsPerRow];
	else
		return wavelengthLayout [wavelengthLayout.size() - 1];
}

int StyleSheet::getBottomRightWavelength() const
{
	return wavelengthLayout [wavelengthLayout.size() - 1];
}





const TreeElementStyle StyleSheet::getAssociatedElement (char _key) const
{
	return trackingKeyMap[_key];
}
	
void StyleSheet::setAssociatedElement (char _key, const TreeElementStyle& _tes)
{
	trackingKeyMap[_key] = _tes;
}



void StyleSheet::reset (int _mode)
{
        if ((_mode == 0) | (_mode == 1)) {
		//reset tree graphics
		
		//creation mode of the tree element styles (tes):
		//1) all tes elements are created only with their RTTI value
		//2) each created element is reset then
		
		if (elementStyles.empty()) {
			//fill vector with all available elements (loop through RTTI values defined in treeelementstyle.h)
			elementStyles.resize (TES_RTTI_MAX - TES_RTTI_MINUS + 1);
			for (int i = TES_RTTI_MIN; i <= TES_RTTI_MAX; i++) {
				//construct element with RTTI == i
				TreeElementStyle tmpTES (i);
				
				tmpTES.reset();
				
				//with this assignment, the elements are directly accessible via their RTTI value
				elementStyles [i - TES_RTTI_MINUS] = tmpTES;
			}
		}
		else {
                        for (	QVector<TreeElementStyle>::Iterator iter = elementStyles.begin();
					iter != elementStyles.end(); 
					++iter) {
				
				(*iter).reset();
			}
		}
	}
	
    if ((_mode == 0) | (_mode == 2)) {
		//reset wavelength layout
		//note: the overlay wavelength is NOT included
		wavelengthLayout.resize (MAX_WAVE_LENGTH + 1);
		for (int i = 0; i < (int)wavelengthLayout.size(); i++) {
			wavelengthLayout [i] = i;
		}
		picsPerRow = 3;
	}
	
    if ((_mode == 0) | (_mode == 3)) {
		//reset movie export options
		
		MovieExport_TimeboxBackground = 0;	//transparent
		MovieExport_TimeboxPosition = 11;	//upper left
		MovieExport_WavelengthVisible = false;	//hide wl index
	}
	
	if ((_mode == 0) | (_mode == 4)) {
		//reset cell display options
		
		cellColor = UNIFORM_CELL_COLOR;
		cellNumberColor = cellColor;
		cellCircleThickness = 1;
		cellPathColor = Qt::red;
		cellNumberFontSize = 10;
		cellPathThickness = 1;
		cellPathTimeInterval = 0;
        wavelengthBoxColor = Qt::red;
        wavelengthBoxFontSize = 20;
		showOutOfSyncBox = true;
	}
	
    if ((_mode == 0) | (_mode == 5)) {
		//reset tracking keys
		//for all letters, set default style
		for (char c = 'A'; c <= 'Z'; c++)
			trackingKeyMap[c] = TreeElementStyle (c);
	}
	
    if ((_mode == 0) | (_mode == 6)) {
		//reset various settings
		
		addUserSignToTTTFiles = true;
		useNewTTTFileFolderStructure = false;
		exportWavelength = 1;
		picturesToLoadBeforeStop = 10;
		picturesToLoadAfterStop = 25;
		loadPicturesAsynchronous = false;
		showTreeCountInPositionLayout = true;
		useColorsForAllTracks = true;
		numPicsToLoadByNextPosItem = 300;
		takeOverDisplaySettingsNextPosItem = true;
		lastTTTChangelogVersion = "";
	}
	
    if ((_mode == 0) | (_mode == 7)) {
		startUpMoveToFirstFluor = false;
		startUpBgCircleVisible = false;
		startUpFullPicture = false;
		startUpAlternateNormalBgTracking = false;
	}
}

bool StyleSheet::save (QString _filename) const
{
	if (_filename.isEmpty())
		return false;
	
	return XMLHandler::writeStyleSheet (_filename, *this);
}

bool StyleSheet::load (QString _filename)
{
	if (_filename.isEmpty())
		return false;
	
	return XMLHandler::readStyleSheet (_filename, this);
}

QDomDocument StyleSheet::toDomDocument() const
{
	QDomDocument result ("StyleSheet");
	
	QDomElement root = result.createElement ("StyleSheetSettings");
	root.setAttribute ("Version", StyleFileVersion);
	result.appendChild (root);
	
/*	de = result.createElement ("");
	de.setAttribute ("", );
	root.appendChild (de);*/
	
	
	QDomElement de2;
	
	//tree style
	//==========
	
	QDomElement de = result.createElement ("Treestyle");
	
	//write element count and styles
	de.setAttribute ("count", elementStyles.size());
        for (int i = 0; i < elementStyles.size(); i++) {
		elementStyles.at (i).toDomElement (result, de);
	}
	
	root.appendChild (de);
	
	
	//movie style
	//===========
	
	de = result.createElement ("Moviestyle");
	
	//write wavelength count and layout
	de.setAttribute ("WLLayout_count", wavelengthLayout.size());
        for (int i = 0; i < wavelengthLayout.size(); i++) {
		de2 = result.createElement ("WLLayout");
		de2.setAttribute ("Index", i);
		de2.setAttribute ("Value", wavelengthLayout.at (i));
		de.appendChild (de2);
	}
	
	de2 = result.createElement ("PicsPerRow");
	de2.setAttribute ("value", picsPerRow);
	de.appendChild (de2);
	
	de2 = result.createElement ("MovieExport_WavelengthVisible");
	de2.setAttribute ("value", (MovieExport_WavelengthVisible ? 1 : 0));
	de.appendChild (de2);
	
	de2 = result.createElement ("MovieExport_TimeboxPosition");
	de2.setAttribute ("value", MovieExport_TimeboxPosition);
	de.appendChild (de2);
	
	de2 = result.createElement ("MovieExport_TimeboxBackground");
	de2.setAttribute ("value", MovieExport_TimeboxBackground);
	de.appendChild (de2);
	
	de2 = result.createElement ("CellColor");
	de2.setAttribute ("color", GraphicAid::colorToString (cellColor));
	de.appendChild (de2);
	
	de2 = result.createElement ("CellNumberColor");
	de2.setAttribute ("color", GraphicAid::colorToString (cellNumberColor));
	de.appendChild (de2);
	
	de2 = result.createElement ("CellCircleThickness");
	de2.setAttribute ("value", cellCircleThickness);
	de.appendChild (de2);
	
	de2 = result.createElement ("CellPathColor");
	de2.setAttribute ("color", GraphicAid::colorToString (cellPathColor));
	de.appendChild (de2);
	
	de2 = result.createElement ("CellNumberFontSize");
	de2.setAttribute ("value", cellNumberFontSize);
	de.appendChild (de2);
	
	de2 = result.createElement ("CellPathThickness");
	de2.setAttribute ("value", cellPathThickness);
	de.appendChild (de2);
	
	de2 = result.createElement ("CellPathTimeInterval");
	de2.setAttribute ("value", cellPathTimeInterval);
	de.appendChild (de2);

	de2 = result.createElement ("ShowOutOfSyncBox");
	de2.setAttribute ("value", (showOutOfSyncBox ? 1 : 0));
	de.appendChild (de2);
	
    de2 = result.createElement ("WavelengthBoxColor");
    de2.setAttribute ("color", GraphicAid::colorToString (wavelengthBoxColor));
    de.appendChild (de2);

    de2 = result.createElement ("WavelengthBoxFontSize");
    de2.setAttribute ("value", wavelengthBoxFontSize);
    de.appendChild (de2);

    root.appendChild (de);
	
	
	//tracking keys
	//=============
	de = result.createElement ("Tracking_Keys");
	
	QMap<Q_INT8, TreeElementStyle>::const_iterator iter = trackingKeyMap.constBegin();
    for ( ; iter != trackingKeyMap.constEnd(); ++iter ) {
		de2 = result.createElement ("Key");
		de2.setAttribute ("value", iter.key());
		
		iter.data().toDomElement (result, de2);
		
		de.appendChild (de2);
    }
	
	root.appendChild (de);
	
	
	//various settings
	//================
	de = result.createElement ("Various_Settings");
	
	de2 = result.createElement ("AddUserSignToTTTFiles");
	de2.setAttribute ("value", (addUserSignToTTTFiles ? 1 : 0));
	de.appendChild (de2);
	
	de2 = result.createElement ("UseNewTTTFileFolderStructure");
	de2.setAttribute ("value", (useNewTTTFileFolderStructure ? 1 : 0));
	de.appendChild (de2);
	
	de2 = result.createElement ("ExportWavelength");
	de2.setAttribute ("value", exportWavelength);
	de.appendChild (de2);
	
	de2 = result.createElement ("PicturesToLoadBeforeStop");
	de2.setAttribute ("value", picturesToLoadBeforeStop);
	de.appendChild (de2);
	
	de2 = result.createElement ("PicturesToLoadAfterStop");
	de2.setAttribute ("value", picturesToLoadAfterStop);
	de.appendChild (de2);
	
	de2 = result.createElement ("LoadPicturesAsynchronous");
	de2.setAttribute ("value", (loadPicturesAsynchronous ? 1 : 0));
	de.appendChild (de2);
	
	de2 = result.createElement ("ShowTreeCountInPositionLayout");
	de2.setAttribute ("value", (showTreeCountInPositionLayout ? 1 : 0));
	de.appendChild (de2);
	
	de2 = result.createElement ("UseColorsForAllTracks");
	de2.setAttribute ("value", (useColorsForAllTracks ? 1 : 0));
	de.appendChild (de2);

	de2 = result.createElement("NumPicsToLoadByNextPosItem");
	de2.setAttribute ("value", numPicsToLoadByNextPosItem);
	de.appendChild (de2);

	de2 = result.createElement("TakeOverDisplaySettingsNextPosItem");
	de2.setAttribute ("value", (takeOverDisplaySettingsNextPosItem ? 1 : 0));
	de.appendChild (de2);

	de2 = result.createElement("LastTTTChangelogVersion");
	de2.setAttribute("value", lastTTTChangelogVersion);
	de.appendChild(de2);

	root.appendChild (de);
	
	
	//startup settings
	//================
	
	de = result.createElement ("Startup_Settings");
	
	de2 = result.createElement ("StartUpMoveToFirstFluor");
	de2.setAttribute ("value", (startUpMoveToFirstFluor ? 1 : 0));
	de.appendChild (de2);
	
	de2 = result.createElement ("StartUpBgCircleVisible");
	de2.setAttribute ("value", (startUpBgCircleVisible ? 1 : 0));
	de.appendChild (de2);
	
	de2 = result.createElement ("StartUpFullPicture");
	de2.setAttribute ("value", (startUpFullPicture ? 1 : 0));
	de.appendChild (de2);

	de2 = result.createElement ("StartUpAlternateNormalBgTracking");
	de2.setAttribute ("value", (startUpAlternateNormalBgTracking ? 1 : 0));
	de.appendChild (de2);
	
	root.appendChild (de);
	
	
	return result;
}

